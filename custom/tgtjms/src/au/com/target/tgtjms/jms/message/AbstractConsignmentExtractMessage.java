/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;


/**
 * @author vmuthura
 * 
 */
public abstract class AbstractConsignmentExtractMessage extends AbstractMessage
{
    private ConsignmentModel consignment;
    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;
    private ConsignmentSentToWarehouseReason reason;

    public AbstractConsignmentExtractMessage(final ConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason)
    {
        this.consignment = consignment;
        this.targetCustomerEmailResolutionService = targetCustomerEmailResolutionService;
        this.reason = reason;
    }



    /**
     * @return the targetCustomerEmailResolutionService
     */
    public TargetCustomerEmailResolutionService getTargetCustomerEmailResolutionService()
    {
        return targetCustomerEmailResolutionService;
    }

    /**
     * @param targetCustomerEmailResolutionService
     *            the targetCustomerEmailResolutionService to set
     */
    public void setTargetCustomerEmailResolutionService(
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService)
    {
        this.targetCustomerEmailResolutionService = targetCustomerEmailResolutionService;
    }

    /**
     * @return the reason
     */
    public ConsignmentSentToWarehouseReason getReason()
    {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final ConsignmentSentToWarehouseReason reason)
    {
        this.reason = reason;
    }



    /**
     * @return the consignment
     */
    public ConsignmentModel getConsignment() {
        return consignment;
    }



    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final ConsignmentModel consignment) {
        this.consignment = consignment;
    }



}