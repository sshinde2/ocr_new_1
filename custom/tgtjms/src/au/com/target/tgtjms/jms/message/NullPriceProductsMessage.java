/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import java.util.List;

import au.com.target.tgtjms.jms.converter.NullPriceProductsMessageConverter;
import au.com.target.tgtjms.jms.model.ProductCodeList;



/**
 * @author mmaki
 * 
 */
public class NullPriceProductsMessage extends AbstractMessage
{

    private ProductCodeList productCodes;

    public NullPriceProductsMessage(final List<String> productCodesList)
    {
        this.productCodes = new ProductCodeList(productCodesList);
    }

    /**
     * @return the productCodes
     */
    public ProductCodeList getProductCodes()
    {
        return productCodes;
    }

    /**
     * @param productCodes
     *            the productCodes to set
     */
    public void setProductCodes(final ProductCodeList productCodes)
    {
        this.productCodes = productCodes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtutility.jms.message.AbstractMessage#convertToMessagePayload()
     */
    @Override
    public String convertToMessagePayload()
    {
        return NullPriceProductsMessageConverter.getMessagePayload(this);
    }

}
