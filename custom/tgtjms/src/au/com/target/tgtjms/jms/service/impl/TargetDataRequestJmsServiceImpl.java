/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.NullPriceProductsMessage;
import au.com.target.tgtsale.pos.service.TargetDataRequestJmsService;


/**
 * @author fkratoch
 * 
 */
public class TargetDataRequestJmsServiceImpl implements TargetDataRequestJmsService
{
    private static final Logger LOG = Logger.getLogger(TargetDataRequestJmsServiceImpl.class);

    private static final String POS_REQUEST_PRICE_DATA = "posPriceData";
    private static final String POS_HOTCARD_DATA = "hotcardData";
    private static final String POS_LAYBY_CHANGES_DATA = "laybyChangesData";
    private static final String TRIGGER_SYNC_FEEDBACK = "catalogSyncComplete";

    private JmsMessageDispatcher messageDispatcher;

    private String senderPosQueueId;

    private String senderStepQueueId;

    private String nullPriceProductsQueueId;

    @Override
    public void requestProductPriceData()
    {
        LOG.info("Requesting price data from POS via ESB");
        messageDispatcher.send(senderPosQueueId, POS_REQUEST_PRICE_DATA);
    }

    @Override
    public void requestHotcardData()
    {
        LOG.info("Requesting hotcard data from POS via ESB");
        messageDispatcher.send(senderPosQueueId, POS_HOTCARD_DATA);
    }

    @Override
    public void requestLaybyChangesData()
    {
        LOG.info("Requesting layby changes data from POS via ESB");
        messageDispatcher.send(senderPosQueueId, POS_LAYBY_CHANGES_DATA);
    }

    @Override
    public void triggerCatalogSyncFeedback()
    {
        LOG.info("Catalog sync complete. Hence triggering the feedback to step via esb");
        messageDispatcher.send(senderStepQueueId, TRIGGER_SYNC_FEEDBACK);
    }

    @Override
    public void requestNullPriceProducts(final List<String> codes)
    {
        final NullPriceProductsMessage message = new NullPriceProductsMessage(codes);
        LOG.info("INFO-POS-NULLPRICE-REQUEST Sending null price product list to POS");
        messageDispatcher.send(nullPriceProductsQueueId, message);
    }

    /**
     * Sets the message dispatcher.
     *
     * @param messageDispatcher the message dispatcher to set
     */
    @Required
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher)
    {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * @param senderPosQueueId
     *            the senderPosQueueId to set
     */
    @Required
    public void setSenderPosQueueId(final String senderPosQueueId)
    {
        this.senderPosQueueId = senderPosQueueId;
    }

    /**
     * @param senderStepQueueId
     *            the senderStepQueueId to set
     */
    @Required
    public void setSenderStepQueueId(final String senderStepQueueId)
    {
        this.senderStepQueueId = senderStepQueueId;
    }

    /**
     * @param nullPriceProductsQueueId
     *            the nullPriceProductsQueueId to set
     */
    @Required
    public void setNullPriceProductsQueueId(final String nullPriceProductsQueueId)
    {
        this.nullPriceProductsQueueId = nullPriceProductsQueueId;
    }
}