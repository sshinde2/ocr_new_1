/**
 * 
 */
package au.com.target.tgtjms.jms.message;

/**
 * @author mmaki
 * 
 */
public abstract class AbstractMessage
{

    public abstract String convertToMessagePayload();

}
