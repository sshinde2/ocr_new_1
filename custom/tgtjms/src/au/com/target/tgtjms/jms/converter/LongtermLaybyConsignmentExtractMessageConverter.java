/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtjms.jms.message.ToysaleWarehouseConsignmentExtractMessage;
import au.com.target.tgtjms.jms.model.LongtermLaybyConsignmentExtract;
import au.com.target.tgtjms.jms.model.OrderItemExtract;


/**
 * @author fkratoch
 * 
 */
public final class LongtermLaybyConsignmentExtractMessageConverter {

    private static final Logger LOG = Logger.getLogger(LongtermLaybyConsignmentExtractMessageConverter.class);

    private static final String DEFAULT_CUSTOMER_CODE = "ANON";
    private static final String PURCHASE_OPTION_LONGTERM_LAYBY = "longtermlayby";
    private static final String ORDER_TYPE_LONGTERM_LAYBY = "O";


    private static JAXBContext longtermLaybyOrderExtractJaxbContext = null;

    static
    {
        try
        {
            longtermLaybyOrderExtractJaxbContext = JAXBContext.newInstance(LongtermLaybyConsignmentExtract.class);
        }
        catch (final JAXBException e)
        {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    private LongtermLaybyConsignmentExtractMessageConverter()
    {
    }


    public static String getMessagePayload(final ToysaleWarehouseConsignmentExtractMessage orderExtractMessage,
            final ConsignmentSentToWarehouseReason reason)
    {
        final ConsignmentModel consignment = orderExtractMessage.getConsignment();

        if (consignment == null || !(consignment instanceof TargetConsignmentModel))
        {
            throw new IllegalArgumentException("ConsignmentModel is null or not a target consignment");
        }
        final OrderModel order = (OrderModel)consignment.getOrder();
        if (order == null)
        {
            throw new IllegalArgumentException("OrderModel is null");
        }
        final String purchaseOption = order.getPurchaseOption().getCode();
        if (!PURCHASE_OPTION_LONGTERM_LAYBY.equalsIgnoreCase(purchaseOption))
        {
            final String errorMsg = "Purchase option " + purchaseOption
                    + " not allowed for long term layby order extract. Order code: "
                    + order.getCode();
            LOG.error(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }

        LOG.info("Converting to OrderExtractMessage, order " + order.getCode());

        final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
        final LongtermLaybyConsignmentExtract consignmentExtract = populateLongtermLaybyExtract(targetConsignment,
                order,
                reason);


        try
        {
            return marshalConsignmentExtractList(consignmentExtract);
        }
        catch (final JAXBException e)
        {
            LOG.error("Unable to marshal OrderExtractList");
            throw new MessageConversionException("Unable to marshal OrderExtractList", e);
        }
    }

    private static String marshalConsignmentExtractList(final LongtermLaybyConsignmentExtract orderExtract)
            throws JAXBException
    {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = longtermLaybyOrderExtractJaxbContext.createMarshaller();
        jaxbMarshaller.marshal(orderExtract, writer);
        return writer.toString();
    }

    private static LongtermLaybyConsignmentExtract populateLongtermLaybyExtract(
            final TargetConsignmentModel targetConsignment, final OrderModel order,
            final ConsignmentSentToWarehouseReason reason)
    {
        final LongtermLaybyConsignmentExtract consignmentExtract = new LongtermLaybyConsignmentExtract();

        final CustomerModel customer = (CustomerModel)order.getUser();
        final AddressModel address = order.getDeliveryAddress();

        consignmentExtract.setOrderNumber(order.getCode());
        consignmentExtract.setDeliveryDate(null);
        final String custCode = (customer.getCustomerID() == null)
                ? DEFAULT_CUSTOMER_CODE
                : customer.getCustomerID().toUpperCase();
        consignmentExtract.setCustomerCode(custCode);
        consignmentExtract.setCustomerName(getCustomerName(address));
        consignmentExtract.setShipToCode(custCode);
        consignmentExtract.setShipToLocName(null);
        consignmentExtract.setShipToLocAddr1(address.getStreetname() != null
                ? address.getStreetname().toUpperCase() : null);
        consignmentExtract.setShipToLocAddr2(address.getStreetnumber() != null
                ? address.getStreetnumber().toUpperCase()
                : null);
        consignmentExtract.setShipToCity(address.getTown() != null ? address.getTown().toUpperCase() : null);
        consignmentExtract.setShipToState(address.getDistrict() != null ? address.getDistrict().toUpperCase() : null);
        consignmentExtract.setShipToPostCode(address.getPostalcode() != null ? address.getPostalcode().toUpperCase()
                : null);
        consignmentExtract.setShipToCountry(address.getCountry() != null
                ? address.getCountry().getName().toUpperCase()
                : null);

        consignmentExtract.setOrderInstructions1(null);
        consignmentExtract.setOrderInstructions2(null);
        consignmentExtract.setOrderPurpose(reason.getCode());
        consignmentExtract.setOrderType(ORDER_TYPE_LONGTERM_LAYBY);
        if (null != targetConsignment.getTargetCarrier()) {
            consignmentExtract.setCarrier(targetConsignment.getTargetCarrier().getWarehouseCode());
        }
        consignmentExtract.setOrderItems(buildConsignmentItemsList(targetConsignment));

        return consignmentExtract;
    }

    private static String getCustomerName(final AddressModel address)
    {
        final StringBuilder str = new StringBuilder();
        if (address.getFirstname() != null)
        {
            str.append(address.getFirstname());
            str.append(' ');
        }
        if (address.getLastname() != null)
        {
            str.append(address.getLastname());
        }
        return str.toString().toUpperCase();
    }

    private static List<OrderItemExtract> buildConsignmentItemsList(final TargetConsignmentModel consignment) {

        final List<OrderItemExtract> items = new ArrayList<>();

        int cnt = 1;
        for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries())
        {
            final ProductModel product = entry.getOrderEntry().getProduct();
            final OrderItemExtract item = new OrderItemExtract();
            item.setOrderLineNum(Integer.valueOf(cnt));
            item.setItemEAN(product.getEan());
            item.setItemCode(product.getCode());
            item.setItemDescription(product.getName() != null ? product.getName().toUpperCase() : null);
            item.setQuantityOrdered(entry.getQuantity());
            items.add(item);

            cnt++;
        }
        return items;

    }
}
