/**
 * 
 */
package au.com.target.tgtjms.jms.gateway;

import javax.jms.Message;

import java.util.Map;

import org.springframework.jms.JmsException;

import au.com.target.tgtjms.jms.message.AbstractMessage;


/**
 * Encapsulates the logic related to sending and retrieving messages from the queue.
 *
 * @author fkratoch
 */
public interface JmsMessageDispatcher
{

    /**
     * Sends a text message to the given {@code queue}.
     *
     * @param queue the queue name
     * @param message the content of the text message
     * @throws JmsException if send operation fails
     */
    void send(String queue, String message) throws JmsException;

    /**
     * Sends a {@code message} to the given {@code queue}. Essentially, the
     * {@link AbstractMessage} will be converted to text and sent as a text message.
     *
     * @param queue the queue name
     * @param message the message
     * @throws JmsException if send operation fails
     */
    void send(final String queue, final AbstractMessage message) throws JmsException;

    /**
     * Sends a {@code message} with {@code headers} to the given {@code queue}.
     *
     * @param queue the queue name
     * @param message the message body
     * @param headers the message headers
     * @throws JmsException if send operation fails
     */
    void sendMessageWithHeaders(final String queue, final AbstractMessage message, final Map<String, String> headers)
            throws JmsException;

    /**
     * Sends a test message to the {@code requestQueue} and reads response from the
     * {@code responseQueue}.
     *
     * @param requestQueue the queue name to send message to
     * @param message the message body
     * @param responseQueue the queue name to retrieve a message from
     * @return the response message
     * @throws JmsException if send or receive operation fails
     */
    Message sendAndReceive(final String requestQueue, final String message, final String responseQueue)
            throws JmsException;
}
