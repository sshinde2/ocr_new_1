/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * @author mmaki
 * 
 */
@XmlRegistry
public class ObjectFactory
{

    public ConsignmentExtract createOrderExtractItem()
    {
        return new ConsignmentExtract();
    }

    public ShipAdvice createShipAdvice()
    {
        return new ShipAdvice();
    }

    public ProductCodeList createProductCodeList()
    {
        return new ProductCodeList();
    }
}
