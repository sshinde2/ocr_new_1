/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtsale.ist.dto.StockAdjustmentDto;


/**
 * @author Nandini
 *
 */
public class ISTMessageConverter {

    private static JAXBContext context = null;

    public ISTMessageConverter() throws JAXBException {
        context = JAXBContext.newInstance(StockAdjustmentDto.class);
    }

    /**
     * returns a xml representation of IST
     * 
     * @param stockAdjustmentInfo
     *            object to be marshalled
     * @return xml String
     * @throws MessageConversionException
     *             error when something goes wrong with marshalling
     */
    public String getMessagePayload(final StockAdjustmentDto stockAdjustmentInfo) throws MessageConversionException {
        try {
            return marshalISTExtract(stockAdjustmentInfo);
        }
        catch (final JAXBException e) {
            throw new MessageConversionException("Unable to marshal IST", e);
        }
    }

    /**
     * marshall the object
     * 
     * @param stockAdjustment
     *            of type StockAdjustmentDto
     * @return String representing the object
     * @throws JAXBException
     *             throws the exception when marshalling goes wrong
     */
    private static String marshalISTExtract(final StockAdjustmentDto stockAdjustment) throws JAXBException {
        final StringWriter writer = new StringWriter();
        final Marshaller jaxbMarshaller = context.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(stockAdjustment, writer);
        return writer.toString();
    }
}
