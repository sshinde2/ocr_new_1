/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mmaki
 * 
 */
@XmlRootElement(name = "integration-productCodeList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductCodeList implements Serializable
{
    private static final long serialVersionUID = 1L;

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "code")
    private List<String> productCodes;

    public ProductCodeList()
    {
        this.productCodes = new ArrayList<>();
    }

    public ProductCodeList(final List<String> productCodes)
    {
        this.productCodes = productCodes;
    }

    /**
     * @return the productCodes
     */
    public List<String> getProductCodes()
    {
        return productCodes;
    }

    /**
     * @param productCodes
     *            the productCodes to set
     */
    public void setProductCodes(final List<String> productCodes)
    {
        this.productCodes = productCodes;
    }

    public void addProductCode(final String productCode)
    {
        if (productCodes == null)
        {
            productCodes = new ArrayList<>();
        }
        productCodes.add(productCode);
    }

}
