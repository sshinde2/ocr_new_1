/**
 *
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * @author mmaki
 *
 */
@ManualTest
public class TargetOrderJmsServiceManualTest extends ServicelayerTransactionalTest {

    @Resource
    private SendToWarehouseProtocol esbFulfilmentProcessService;

    @Test
    public void testSendToOrderExtractOneEntry() {
        final OrderModel order = createOrder("789012", "Cust67890", "P4009_blue_S");
        final WarehouseModel warehouse = Mockito.mock(WarehouseModel.class);
        Mockito.when(warehouse.getCode()).thenReturn("FastlineWarehouse");

        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        Mockito.when(consignment.getOrder()).thenReturn(order);
        Mockito.when(consignment.getWarehouse()).thenReturn(warehouse);

        esbFulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW);
    }

    @Test
    public void testSendToOrderExtractThreeEntries() {
        final OrderModel order = createOrder("123456", "Cust12345", "P4000_red_M", "P4003_blue_L", "P4006_black_L");
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.CREATED);

        final WarehouseModel warehouse = Mockito.mock(WarehouseModel.class);
        Mockito.when(warehouse.getCode()).thenReturn("FastlineWarehouse");

        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        Mockito.when(consignment.getOrder()).thenReturn(order);
        Mockito.when(consignment.getWarehouse()).thenReturn(warehouse);

        esbFulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW);
    }

    @Test
    public void testSendToOrderExtractCancelledOrder() {
        final OrderModel order = createOrder("01010101", "Cust0101", "P4000_red_M");
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.CANCELLED);

        final WarehouseModel warehouse = Mockito.mock(WarehouseModel.class);
        Mockito.when(warehouse.getCode()).thenReturn("FastlineWarehouse");

        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        Mockito.when(consignment.getOrder()).thenReturn(order);
        Mockito.when(consignment.getWarehouse()).thenReturn(warehouse);

        esbFulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW);
    }

    private OrderModel createOrder(final String code, final String customerID, final String... productCodes) {
        final OrderModel order = Mockito.mock(OrderModel.class);
        Mockito.when(order.getCode()).thenReturn(code);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();

        for (final String productCode : productCodes) {
            final OrderEntryModel entry = Mockito.mock(OrderEntryModel.class);
            Mockito.when(entry.getQuantity()).thenReturn(Long.valueOf(1));
            entries.add(entry);

            final ProductTypeModel pt = Mockito.mock(ProductTypeModel.class);
            Mockito.when(pt.getBulky()).thenReturn(Boolean.TRUE);

            final AbstractTargetVariantProductModel product = Mockito.mock(AbstractTargetVariantProductModel.class);
            Mockito.when(product.getCode()).thenReturn(productCode);
            Mockito.when(product.getEan()).thenReturn(productCode + " ean");
            Mockito.when(product.getName()).thenReturn(productCode + " description");
            Mockito.when(product.getProductType()).thenReturn(pt);
            Mockito.when(entry.getProduct()).thenReturn(product);
        }
        Mockito.when(order.getEntries()).thenReturn(entries);

        final PurchaseOptionModel po = Mockito.mock(PurchaseOptionModel.class);
        Mockito.when(po.getCode()).thenReturn("buynow");
        Mockito.when(order.getPurchaseOption()).thenReturn(po);

        final TargetCustomerModel customer = Mockito.mock(TargetCustomerModel.class);
        Mockito.when(customer.getCustomerID()).thenReturn(customerID);
        Mockito.when(order.getUser()).thenReturn(customer);

        final AddressModel address = Mockito.mock(AddressModel.class);
        Mockito.when(address.getStreetname()).thenReturn("Victoria Street");
        Mockito.when(address.getStreetnumber()).thenReturn("23");
        Mockito.when(address.getFirstname()).thenReturn("John");
        Mockito.when(address.getLastname()).thenReturn("Doe");
        Mockito.when(address.getPostalcode()).thenReturn("3000");
        Mockito.when(address.getDistrict()).thenReturn("VIC");
        Mockito.when(address.getTown()).thenReturn("Melbourne");
        final CountryModel country = Mockito.mock(CountryModel.class);
        Mockito.when(country.getName()).thenReturn("Australia");
        Mockito.when(address.getCountry()).thenReturn(country);
        Mockito.when(order.getDeliveryAddress()).thenReturn(address);
        Mockito.when(order.getPaymentAddress()).thenReturn(address);

        return order;
    }
}