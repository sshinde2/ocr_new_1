/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtjms.jms.converter.ISTMessageConverter;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtsale.ist.dto.StockAdjustmentDto;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetInterStoreTransferServiceImplTest {

    private static final String INTER_STORE_TRANSFER = "ist";

    @InjectMocks
    private final TargetInterStoreTransferServiceImpl targetInterStoreTransferServiceImpl = new TargetInterStoreTransferServiceImpl();

    @Mock
    private ConsignmentModel consignmentModel;

    @Mock
    private TargetConsignmentModel targetConsignmentModel;

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private ISTMessageConverter istMessageConverter;

    @Mock
    private JmsMessageDispatcher messageDispatcher;

    @Mock
    private StockAdjustmentDto stockAdjustmentDto;

    @Mock
    private TargetConsignmentDao targetConsignmentDao;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private ConsignmentEntryModel entry;

    @Test(expected = IllegalArgumentException.class)
    public void testPublishInterStoreTransferInfoWithNullConsignmentCode() {
        targetInterStoreTransferServiceImpl.publishInterStoreTransferInfo(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPublishInterStoreTransferInfoWithNoConsignmentModel() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetConsignmentDao.getConsignmentBycode(INTER_STORE_TRANSFER)).willReturn(null);

        targetInterStoreTransferServiceImpl.publishInterStoreTransferInfo(INTER_STORE_TRANSFER);
    }

    @Test
    public void testPublishInterStoreTransferInfoNoWarehouse() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        final ConsignmentEntryModel consignmentEntryModel = new ConsignmentEntryModel();
        consignmentEntryModel.setConsignment(consignmentModel);
        conEntries.add(consignmentEntryModel);
        targetConsignmentModel.setConsignmentEntries(conEntries);

        final Collection<PointOfServiceModel> warehousePOSs = new ArrayList<>();
        warehousePOSs.add(targetPointOfServiceModel);
        given(targetConsignmentModel.getConsignmentEntries()).willReturn(conEntries);
        given(targetConsignmentDao.getConsignmentBycode(INTER_STORE_TRANSFER)).willReturn(targetConsignmentModel);
        given(consignmentModel.getConsignmentEntries()).willReturn(conEntries);
        given(consignmentModel.getWarehouse()).willReturn(warehouseModel);
        given(targetConsignmentDao.getConsignmentBycode(Mockito.anyString())).willReturn(targetConsignmentModel);
        given(consignmentModel.getWarehouse().getPointsOfService()).willReturn(warehousePOSs);
        targetInterStoreTransferServiceImpl.publishInterStoreTransferInfo(INTER_STORE_TRANSFER);
    }

    @Test
    public void testPublishInterStoreTransferInfo() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        final ConsignmentEntryModel consignmentEntryModel = new ConsignmentEntryModel();
        consignmentEntryModel.setConsignment(consignmentModel);
        conEntries.add(consignmentEntryModel);
        targetConsignmentModel.setConsignmentEntries(conEntries);

        final Collection<PointOfServiceModel> warehousePOSs = new ArrayList<>();
        warehousePOSs.add(targetPointOfServiceModel);
        given(targetConsignmentModel.getConsignmentEntries()).willReturn(conEntries);
        given(targetConsignmentDao.getConsignmentBycode(INTER_STORE_TRANSFER)).willReturn(targetConsignmentModel);
        given(consignmentModel.getConsignmentEntries()).willReturn(conEntries);
        given(targetConsignmentModel.getWarehouse()).willReturn(warehouseModel);
        given(targetConsignmentDao.getConsignmentBycode(Mockito.anyString())).willReturn(targetConsignmentModel);
        given(targetConsignmentModel.getWarehouse().getPointsOfService()).willReturn(warehousePOSs);

        targetInterStoreTransferServiceImpl.publishInterStoreTransferInfo(INTER_STORE_TRANSFER);
    }
}
