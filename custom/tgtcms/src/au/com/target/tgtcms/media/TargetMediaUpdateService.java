package au.com.target.tgtcms.media;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.impl.MediaUpdateServiceImpl;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.core.model.media.MediaModel;

import java.io.ByteArrayInputStream;


/**
 * Customise the default MediaUpdateService to pass the filename and contentType, <br/>
 * so svg media types will get file extension of .svg
 * 
 * @author sbryan6
 *
 */
public class TargetMediaUpdateService extends MediaUpdateServiceImpl {

    @Override
    public TypedObject updateMediaBinaryStream(final TypedObject typedObject, final byte[] byteStream,
            final String contentType, final String name)
    {
        TypedObject ret = null;

        de.hybris.platform.jalo.media.Media mediaObject = null;
        if (typedObject.getObject() instanceof MediaModel)
        {
            final MediaModel mediaModel = (MediaModel)typedObject.getObject();
            getModelService().refresh(mediaModel);

            mediaObject = (de.hybris.platform.jalo.media.Media)getModelService().getSource(mediaModel);

            // previously it called mediaObject.setData(byteStream);
            mediaObject.setData(new ByteArrayInputStream(byteStream), name, contentType);

            mediaModel.setMime(contentType);
            mediaModel.setRealFileName(name);
            getModelService().save(mediaModel);

            ret = UISessionUtils.getCurrentSession().getTypeService().wrapItem(mediaObject);
        }

        return ret;
    }
}
