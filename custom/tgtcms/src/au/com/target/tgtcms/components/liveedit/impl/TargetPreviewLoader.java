package au.com.target.tgtcms.components.liveedit.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSCatalogRestrictionModel;
import de.hybris.platform.cmscockpit.components.liveedit.impl.DefaultPreviewLoader;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.preview.TargetPreviewDataModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel;


/**
 * 
 * Extended version to populate {@link TargetPreviewDataModel} while previewing {@link BrandPageModel}
 * 
 */
public class TargetPreviewLoader extends DefaultPreviewLoader {

    @Override
    protected boolean loadRestrictionBasedValues(final PreviewDataModel previewCtx, final AbstractPageModel page)
            throws CMSItemNotFoundException {
        boolean oneRestrictionApplied = super.loadRestrictionBasedValues(previewCtx, page);
        if (!oneRestrictionApplied && previewCtx instanceof TargetPreviewDataModel && page instanceof BrandPageModel) {

            final TargetPreviewDataModel targetPreviewCtx = (TargetPreviewDataModel)previewCtx;

            final Collection<AbstractRestrictionModel> restrictions = new ArrayList<AbstractRestrictionModel>(
                    getRestrictionService().getRestrictionsForPage(page, CMSCatalogRestrictionModel._TYPECODE));
            restrictions.addAll(getRestrictionService().getRestrictionsForPage(page,
                    CMSBrandRestrictionModel._TYPECODE));

            if (CollectionUtils.isNotEmpty(restrictions)) {
                for (final AbstractRestrictionModel restriction : restrictions) {
                    if (oneRestrictionApplied && page.isOnlyOneRestrictionMustApply()) {
                        return oneRestrictionApplied;
                    }

                    if (restriction instanceof CMSBrandRestrictionModel) {
                        loadBrandRestrictionForBrandPage(targetPreviewCtx, (CMSBrandRestrictionModel)restriction);
                        oneRestrictionApplied = true;
                    }
                    else if (restriction instanceof CMSCatalogRestrictionModel) {
                        loadCatalogRestrictionForBrandPage(targetPreviewCtx, (CMSCatalogRestrictionModel)restriction);
                        oneRestrictionApplied = true;
                    }
                }
            }
        }
        return oneRestrictionApplied && page.isOnlyOneRestrictionMustApply();
    }

    /**
     * Populate the brand into preview context from the brand restriction
     * 
     * @param targetPreviewCtx
     * @param brandRestriction
     */
    protected void loadBrandRestrictionForBrandPage(final TargetPreviewDataModel targetPreviewCtx,
            final CMSBrandRestrictionModel brandRestriction) {
        final Collection<BrandModel> brands = brandRestriction.getBrands();
        if (CollectionUtils.isNotEmpty(brands)) {
            targetPreviewCtx.setPreviewBrand(brands.iterator().next());
        }
    }

    /**
     * Populate the catalog into preview context from the catalog restriction
     * 
     * @param targetPreviewCtx
     * @param catalogRestriction
     */
    protected void loadCatalogRestrictionForBrandPage(final TargetPreviewDataModel targetPreviewCtx,
            final CMSCatalogRestrictionModel catalogRestriction) {
        final Collection<CatalogModel> catalogs = catalogRestriction.getCatalogs();
        if (CollectionUtils.isNotEmpty(catalogs)) {
            targetPreviewCtx.setPreviewCatalog(catalogs.iterator().next());
        }
    }

}
