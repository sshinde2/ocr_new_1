/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.integration.dto.IntegrationAssetImportDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationMediaDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationMediasDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.data.MediaImageData;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AssetImportIntegrationFacadeImplTest {

    private static final String ASSET_ID = "1234";
    private static final String MEDIA_FORMAT_GRID = "grid";
    private static final String MEDIA_FORMAT_HERO = "hero";
 

    @Mock
    private MediaModel mediaModel;

    @Mock
    private MediaFormatModel mediaFormatModel;

    @Mock
    private MediaFolderModel mediaFolderModel;

    @Mock
    private MediaImageData mediaImageData;

    @Mock
    private TargetMediaImportService targetMediaImportService;

    @Mock
    private TargetProductImportUtil targetProductImportUtil;

    @Mock
    private ModelService mockModelService;

    @InjectMocks
    private final AssetImportIntegrationFacadeImpl facade = new AssetImportIntegrationFacadeImpl();

       private IntegrationAssetImportDto dto;


    @Before
    public void setup() throws IOException {
                dto = new IntegrationAssetImportDto();
           when(targetProductImportUtil.getActualImageLocation(Matchers.anyString())).thenReturn("/temp/image/" );
        }

        @Test
        public void testpopulatingModel() {
            dto.setAssetId(ASSET_ID);
            final IntegrationMediasDto mediasdto = new IntegrationMediasDto();
            final IntegrationMediaDto media1 = new IntegrationMediaDto();
            media1.setMediaFormat(MEDIA_FORMAT_GRID);
            media1.setMediaPath("/grid/4/6");
            mediasdto.addIntegrationMediaDto(media1);
            final IntegrationMediaDto media3 = new IntegrationMediaDto();
            media3.setMediaPath("/thumb/4/6");
            mediasdto.addIntegrationMediaDto(media3);
            dto.setMedias(mediasdto);
            final IntegrationMediaDto media4 = new IntegrationMediaDto();
            media4.setMediaFormat(MEDIA_FORMAT_HERO);
            mediasdto.addIntegrationMediaDto(media4);
            dto.setMedias(mediasdto);
            dto.setMime("image/jpeg");
            when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_GRID)).thenReturn(null);
            when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_HERO))
                    .thenReturn(new MediaFormatModel());
            given(targetMediaImportService.getOrCreateMediaModel(anyString())).willReturn(mediaModel);
            final IntegrationResponseDto actualResponse = new IntegrationResponseDto(dto.getAssetId());
            actualResponse.addMessage(String.format(AssetImportIntegrationFacadeImpl.ERROR_MEDIAFORMAT, MEDIA_FORMAT_GRID,
                    ASSET_ID));
            actualResponse.addMessage(String.format(AssetImportIntegrationFacadeImpl.ERROR_MEDIAFORMAT, null,
                    ASSET_ID));
            actualResponse.addMessage(String.format(AssetImportIntegrationFacadeImpl.INFO_MEDIAPATH, ASSET_ID));

            given(targetProductImportUtil.createResponse(anyBoolean(),anyList(), anyString()))
                    .willReturn(actualResponse);
            final IntegrationResponseDto response = facade.importTargetAssets(dto);
            assertThat(response).isNotNull();
            assertThat(response.isSuccessStatus()).isFalse();
            final List<String> messages = response.getMessages();

            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(3);
            assertThat(actualResponse.getMessages()).isEqualTo(messages);
        }

        @Test
        public void testpopulatingModelEmptyMedias() {
            dto.setAssetId(ASSET_ID);
            dto.setMime("image/jpeg");
            final IntegrationResponseDto expectedResponse = new IntegrationResponseDto(dto.getAssetId());
            expectedResponse.setSuccessStatus(false);
            expectedResponse.addMessage(String.format(
                    "ERR-ASSETIMPORT-NOMEDIA : No medias to import. Hence not processing the assets with id %s",
                    ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(), anyString()))
                    .willReturn(expectedResponse);
            final IntegrationResponseDto response = facade.importTargetAssets(dto);
            assertThat(response).isNotNull();
            assertThat(response.isSuccessStatus()).isFalse();
            final List<String> messages = response.getMessages();
            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);
            assertThat(messages).isEqualTo(expectedResponse.getMessages());

        }

        @Test
        public void testpopulatingModelEmptyMediasInList() {
            dto.setAssetId(ASSET_ID);
            dto.setMime("image/jpeg");
            final IntegrationMediasDto mediaObject = new IntegrationMediasDto();
            mediaObject.setMedia(new ArrayList<IntegrationMediaDto>());
            dto.setMedias(mediaObject);
            final IntegrationResponseDto expectedResponse = new IntegrationResponseDto(dto.getAssetId());
            expectedResponse.setSuccessStatus(false);
            expectedResponse.addMessage(String.format(
                    "ERR-ASSETIMPORT-NOMEDIA : No medias to import. Hence not processing the assets with id %s",
                    ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(), anyString()))
                    .willReturn(expectedResponse);
            final IntegrationResponseDto response = facade.importTargetAssets(dto);
            assertThat(response).isNotNull();
            assertThat(response.isSuccessStatus()).isFalse();
            final List<String> messages = response.getMessages();
            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);
            assertThat(expectedResponse.getMessages()).isEqualTo(messages);

        }

        @Test
        public void testpopulatingModelNullMediaList() {
            dto.setAssetId(ASSET_ID);
            dto.setMime("image/jpeg");
            final IntegrationMediasDto mediaObject = new IntegrationMediasDto();
            mediaObject.setMedia(null);
            dto.setMedias(mediaObject);
            final IntegrationResponseDto expectedResponse = new IntegrationResponseDto(dto.getAssetId());
            expectedResponse.setSuccessStatus(false);
            expectedResponse.addMessage(String.format(
                    "ERR-ASSETIMPORT-NOMEDIA : No medias to import. Hence not processing the assets with id %s",
                    ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(),anyString()))
                    .willReturn(expectedResponse);
            final IntegrationResponseDto response = facade.importTargetAssets(dto);
            assertThat(response).isNotNull();
            assertThat(response.isSuccessStatus()).isFalse();
            final List<String> messages = response.getMessages();
            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);
            assertThat(messages).isEqualTo(expectedResponse.getMessages());

        }


        @Test
        public void testImportModelFileNotFoundException() throws IOException {
            dto.setAssetId(ASSET_ID);
            final IntegrationMediasDto medias = new IntegrationMediasDto();
            final IntegrationMediaDto media1 = new IntegrationMediaDto();
            media1.setMediaFormat(MEDIA_FORMAT_GRID);
            final String mediaPath = "/grid/4/6/test.jpg";
            media1.setMediaPath(mediaPath);
            medias.addIntegrationMediaDto(media1);
            dto.setMedias(medias);
            dto.setMime("image/jpeg");
            final MediaFormatModel mediaFormat = new MediaFormatModel();
           when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(ASSET_ID);
           when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_GRID)).thenReturn(mediaFormat);
            given(targetMediaImportService.getOrCreateMediaModel(anyString())).willReturn(mediaModel);

            given(targetProductImportUtil.createImageData(anyString(), anyString(), anyString()))
                    .willThrow(new FileNotFoundException());

            final IntegrationResponseDto responsedto = new IntegrationResponseDto(dto.getAssetId());
            responsedto.setSuccessStatus(false);
            responsedto.addMessage(String.format(AssetImportIntegrationFacadeImpl.ERROR_MEDIAIMAGE, mediaPath, ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(), anyString()))
                    .willReturn(
                            responsedto);
            final IntegrationResponseDto responses = facade.importTargetAssets(dto);

            assertThat(responses).isNotNull();
            assertThat(responses.isSuccessStatus()).isFalse();

            final List<String> messages = responses.getMessages();
             assertThat(messages.size()).isEqualTo(1);
            final String response = messages.get(0);
            assertThat(response).startsWith(responsedto.getMessages().get(0));

        }

        @Test
        public void testImportModelMediaPathIncorrect() throws IOException {
            dto.setAssetId(ASSET_ID);
            final IntegrationMediasDto medias = new IntegrationMediasDto();
            final IntegrationMediaDto media1 = new IntegrationMediaDto();
            media1.setMediaFormat(MEDIA_FORMAT_GRID);
            final String mediaPath = "test";
            media1.setMediaPath(mediaPath);
            medias.addIntegrationMediaDto(media1);
            dto.setMedias(medias);
            dto.setMime("image/jpeg");
            final MediaFormatModel mediaFormat = new MediaFormatModel();
           when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(ASSET_ID);
           when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_GRID)).thenReturn(mediaFormat);
            given(targetMediaImportService.getOrCreateMediaModel(anyString())).willReturn(mediaModel);
            final IntegrationResponseDto responsedto = new IntegrationResponseDto(dto.getAssetId());
            responsedto.setSuccessStatus(false);
            responsedto.addMessage(String.format(AssetImportIntegrationFacadeImpl.INFO_MEDIAPATH, ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(), anyString()))
                    .willReturn(responsedto);
            final IntegrationResponseDto responses = facade.importTargetAssets(dto);
            assertThat(responses).isNotNull();
            assertThat(responses.isSuccessStatus()).isFalse();

            final List<String> messages = responses.getMessages();

            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);
            final String response = messages.get(0);
            assertThat(response).startsWith(responsedto.getMessages().get(0));

        }

        @Test
        public void testImportModelValid() throws IOException {
            dto.setAssetId(ASSET_ID);
            final IntegrationMediasDto mediasdto = new IntegrationMediasDto();
            final List<IntegrationMediaDto> medias = new ArrayList<>();
            final IntegrationMediaDto media1 = new IntegrationMediaDto();
            media1.setMediaFormat(MEDIA_FORMAT_GRID);
            media1.setMediaPath("/grid/4/6/test.jpg");
            medias.add(media1);
            mediasdto.setMedia(medias);
            dto.setMedias(mediasdto);
            dto.setMime("image/jpeg");
            final MediaFormatModel mediaFormat = new MediaFormatModel();
            when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(ASSET_ID);
            when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_GRID)).thenReturn(mediaFormat);
            given(targetMediaImportService.getOrCreateMediaModel(anyString())).willReturn(mediaModel);
            given(targetProductImportUtil.createImageData(anyString(), anyString(), anyString()))
                    .willReturn(mediaImageData);
            final IntegrationResponseDto responsedto = new IntegrationResponseDto(dto.getAssetId());
            responsedto.setSuccessStatus(true);
            responsedto
                    .addMessage(String.format(
                            "INFO-ASSETIMPORT-SUCESS : The asset with the id %s has been successfully created in hybris",
                            ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(), anyString()))
                    .willReturn(responsedto);
            final IntegrationResponseDto responses = facade.importTargetAssets(dto);
            assertThat(responses).isNotNull();
            assertThat(responses.isSuccessStatus()).isTrue();
            final List<String> messages = responses.getMessages();
            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);
            final String response = messages.get(0);
            assertThat(response).isEqualTo(responsedto.getMessages().get(0));

        }

        @Test
        public void testImportModelAmbigousMediaFormatModel() throws IOException {
            dto.setAssetId(ASSET_ID);
            final IntegrationMediasDto mediasdto = new IntegrationMediasDto();
            final List<IntegrationMediaDto> medias = new ArrayList<>();
            final IntegrationMediaDto media1 = new IntegrationMediaDto();
            media1.setMediaFormat(MEDIA_FORMAT_GRID);
            media1.setMediaPath("/grid/4/6/test.jpg");
            medias.add(media1);
            mediasdto.setMedia(medias);
            dto.setMedias(mediasdto);
            dto.setMime("image/jpeg");
           when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(ASSET_ID);
           when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_GRID)).thenThrow(
                    new AmbiguousIdentifierException("blah"));
            final IntegrationResponseDto responsedto = new IntegrationResponseDto(dto.getAssetId());
            responsedto.setSuccessStatus(false);
            responsedto
                    .addMessage(String.format(AssetImportIntegrationFacadeImpl.ERROR_AMBIGOUS, "Media Format", ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(), anyString()))
                    .willReturn(responsedto);
            final IntegrationResponseDto responses = facade.importTargetAssets(dto);

            assertThat(responses).isNotNull();
            assertThat(responses.isSuccessStatus()).isFalse();

            final List<String> messages = responses.getMessages();
            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);

            final String response = messages.get(0);
            Assert.assertEquals(responsedto.getMessages().get(0), response);
            assertThat(response).isEqualTo(responsedto.getMessages().get(0));
        }

        @Test
        public void testImportModelAmbigousMediaModel() throws IOException {
            dto.setAssetId(ASSET_ID);
            final IntegrationMediasDto mediasdto = new IntegrationMediasDto();
            final List<IntegrationMediaDto> medias = new ArrayList<>();
            final IntegrationMediaDto media1 = new IntegrationMediaDto();
            media1.setMediaFormat(MEDIA_FORMAT_GRID);
            media1.setMediaPath("/grid/4/6/test.jpg");
            medias.add(media1);
            mediasdto.setMedia(medias);
            dto.setMedias(mediasdto);
            dto.setMime("image/jpeg");
           when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(ASSET_ID);
           when(targetMediaImportService.getMediaFormatModel(MEDIA_FORMAT_GRID)).thenReturn(mediaFormatModel);
            given(targetMediaImportService.getOrCreateMediaModel(anyString())).willThrow(
                    new AmbiguousIdentifierException("blah"));
            final IntegrationResponseDto responsedto = new IntegrationResponseDto(dto.getAssetId());
            responsedto.setSuccessStatus(false);
            responsedto
                    .addMessage(String.format(AssetImportIntegrationFacadeImpl.ERROR_AMBIGOUS, "Media Model", ASSET_ID));
            given(targetProductImportUtil.createResponse(anyBoolean(), anyList(),anyString()))
                    .willReturn(responsedto);
            final IntegrationResponseDto responses = facade.importTargetAssets(dto);
            assertThat(responses).isNotNull();
            assertThat(responses.isSuccessStatus()).isFalse();

            final List<String> messages = responses.getMessages();

            assertThat(messages).isNotEmpty();
            assertThat(messages.size()).isEqualTo(1);

            final String response = messages.get(0);
            assertThat(response).isEqualTo(responsedto.getMessages().get(0));
        }

}
