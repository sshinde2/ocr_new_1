/**
 * 
 */
package au.com.target.tgtwsfacades.segments.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.interceptor.TargetCustomerValidateInterceptor;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.user.service.TargetUserService;
import au.com.target.tgtmarketing.segments.TargetUserSegmentService;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentImportResponseDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoRequestDto;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerSegmentImportFacadeImplTest {

    @Mock
    private TargetUserSegmentService targetUserSegmentService;

    @Mock
    private TargetUserService targetUserService;

    @Mock
    private ModelService modelService;

    @Mock
    private UserSegmentModel userSegmentWomens;

    @Mock
    private UserSegmentModel userSegmentMen;

    @Mock
    private UserSegmentModel userSegmentKids;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private TargetCustomerModel customerModel1;

    @Mock
    private UserSegmentModel userSegmentEntertainment;

    @Mock
    private PrincipalGroupModel groupModel;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private TargetCustomerValidateInterceptor targetCustomerValidateInterceptor;

    @Mock
    private Configuration cfg;

    @InjectMocks
    private final CustomerSegmentImportFacadeImpl facade = new CustomerSegmentImportFacadeImpl();

    private final Map<String, UserSegmentModel> userSegmentMap = new HashMap<>();
    private ArgumentCaptor<ArrayList> categoryCaptor = null;
    private ArgumentCaptor<HashSet> grpCategoryCaptor = null;


    @Before
    public void setup() {

        setupSegmentsInTheSystem();
        BDDMockito.given(targetUserSegmentService.getAllUserSegmentsMap()).willReturn(userSegmentMap);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(cfg);
        BDDMockito.given(
                Integer.valueOf(configurationService.getConfiguration().getInt(
                        "tgtwsfacades.customersegments.customerlist.size", 100)))
                .willReturn(Integer.valueOf(1));
        categoryCaptor = ArgumentCaptor.forClass(ArrayList.class);
        grpCategoryCaptor = ArgumentCaptor.forClass(HashSet.class);

    }

    private void setupSegmentsInTheSystem() {
        BDDMockito.given(userSegmentWomens.getUid()).willReturn("womens");
        BDDMockito.given(userSegmentMen.getUid()).willReturn("men");
        BDDMockito.given(userSegmentEntertainment.getUid()).willReturn("entertainment");
        userSegmentKids.setUid("kids");
        BDDMockito.given(userSegmentKids.getUid()).willReturn("kids");
        userSegmentMap.put("womens", userSegmentWomens);
        userSegmentMap.put("men", userSegmentMen);
        userSegmentMap.put("kids", userSegmentKids);
    }

    private CustomerSegmentInfoRequestDto createCustomerSegmentInfoDto(
            final String segment, final String... subscriptionIds) {
        final CustomerSegmentInfoRequestDto requestDto = new CustomerSegmentInfoRequestDto();
        final List<CustomerSegmentInfoDto> customerSegmentInfoList = new ArrayList<>();
        for (final String subscriptionId : subscriptionIds) {
            final CustomerSegmentInfoDto customerSegmentInfo = new CustomerSegmentInfoDto();
            customerSegmentInfo.setSubscriptionId(subscriptionId);
            customerSegmentInfo.setSegment(segment);
            customerSegmentInfoList.add(customerSegmentInfo);
        }
        requestDto.setCustomerSegments(customerSegmentInfoList);
        return requestDto;
    }

    private CustomerSegmentInfoDto createCustomerSegmentInfo(final String subscriptionId, final String segment) {
        final CustomerSegmentInfoDto customerSegmentInfo = new CustomerSegmentInfoDto();
        customerSegmentInfo.setSubscriptionId(subscriptionId);
        customerSegmentInfo.setSegment(segment);
        return customerSegmentInfo;
    }


    @Test
    public void testImportCustomerWhenPayLoadContainsNoCustomersAndSegments() {
        final CustomerSegmentImportResponseDto response = facade
                .importCustomerSegments(new CustomerSegmentInfoRequestDto());
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.FALSE);
    }

    @Test
    public void testImportCustomerWhenNoSegmentsCustomerInHybris() throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        BDDMockito.given(targetUserSegmentService.getAllUserSegmentsMap()).willReturn(null);
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "women", "1401"));
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.FALSE);
    }

    @Test
    public void testImportCustomerWithNoCustomerMatchingInHybris() throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        BDDMockito.given(targetUserSegmentService.getAllUserSegmentsMap()).willReturn(userSegmentMap);
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(null);
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "women", "1401"));
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.TRUE);
    }

    @Test
    public void testImportCustomerWithNoSegmentMatching() throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        BDDMockito.given(targetUserSegmentService.getAllUserSegmentsMap()).willReturn(userSegmentMap);
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "entertainment", "1401"));
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.TRUE);
    }

    @Test
    public void testImportCustomerSegmentsForCustomerWithNoExistingSegments()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(customerModel)).thenReturn(null);
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "womens", "1401"));
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.TRUE);
        BDDMockito.verify(modelService, Mockito.times(1)).saveAll(categoryCaptor.capture());
    }


    @Test
    public void testImportCustomerSegmentsForCustomerWithExistingSegmentsAndNotoFtypeuserSegments()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(customerModel)).thenReturn(
                Collections.singletonList(groupModel));
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "womens", "1401"));
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.TRUE);
        BDDMockito.verifyNoMoreInteractions(modelService);

    }

    @Test
    public void testImportCustomerSegmentsForCustomerWithExistingDiffererntUserSegments()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final ArrayList<CustomerSegmentInfoDto> infoList = new ArrayList<>();
        final List<PrincipalGroupModel> userList = new ArrayList<>();
        final CustomerSegmentInfoRequestDto requestDto = new CustomerSegmentInfoRequestDto();
        final TargetCustomerModel custModel1 = Mockito.mock(TargetCustomerModel.class);

        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1403")).willReturn(custModel1);

        userList.add(userSegmentEntertainment);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(customerModel)).thenReturn(
                userList);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(custModel1)).thenReturn(
                userList);
        final CustomerSegmentInfoDto info1 = createCustomerSegmentInfo("1403", "womens");
        infoList.add(info1);
        final CustomerSegmentInfoDto info2 = createCustomerSegmentInfo("1401", "kids");
        infoList.add(info2);
        requestDto.setCustomerSegments(infoList);

        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(requestDto);
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.TRUE);
        BDDMockito.verify(userSegmentEntertainment, Mockito.times(2)).getUid();
        BDDMockito.verify(userSegmentWomens, Mockito.times(1)).getUid();
        BDDMockito.verify(userSegmentKids, Mockito.times(1)).getUid();
        BDDMockito.verify(modelService, Mockito.times(2)).saveAll(categoryCaptor.capture());

    }

    @Test
    public void testImportCustomerSegmentsForCustomerWithExistingSameUserSegments()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {

        final List<PrincipalGroupModel> userList = new ArrayList<>();
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        userList.add(userSegmentMen);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(customerModel)).thenReturn(
                userList);
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "men", "1401"));
        Assert.assertEquals(Boolean.valueOf(response.isSuccess()), Boolean.TRUE);
        BDDMockito.verify(userSegmentMen, Mockito.times(2)).getUid();
        BDDMockito.verifyNoMoreInteractions(modelService);

    }

    @Test
    public void testUpdateCustomerUserSegmentWithSameUSerSegmetModel() {
        final HashSet<PrincipalGroupModel> grps = new HashSet<>();
        final UserGroupModel customergroup = Mockito.mock(UserGroupModel.class);
        BDDMockito.when(customergroup.getUid()).thenReturn("customergrp");
        grps.add(customergroup);
        grps.add(userSegmentEntertainment);
        BDDMockito.when(customerModel.getGroups()).thenReturn(grps);
        customerModel = facade.updateCustomerUserSegment(userSegmentEntertainment, userSegmentKids, customerModel);
        Assert.assertNotNull(customerModel.getGroups());
        BDDMockito.verify(customerModel).setGroups(grpCategoryCaptor.capture());
    }

    @Test
    public void testUpdateCustomerUserSegmentWithDiffUSerSegmetModel() {
        final HashSet<PrincipalGroupModel> grps = new HashSet<>();
        final UserGroupModel customergroup = Mockito.mock(UserGroupModel.class);
        BDDMockito.when(customergroup.getUid()).thenReturn("customergrp");
        grps.add(customergroup);
        grps.add(userSegmentEntertainment);
        BDDMockito.when(customerModel.getGroups()).thenReturn(grps);
        customerModel = facade.updateCustomerUserSegment(userSegmentMen, userSegmentKids, customerModel);
        BDDMockito.verify(customerModel).getGroups();
        BDDMockito.verifyNoMoreInteractions(customerModel);

    }

    @Test
    public void testUpdateCustomerValidationFailed() throws InterceptorException, TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final List<PrincipalGroupModel> userList = new ArrayList<>();
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        final List<CustomerModel> customerList = new ArrayList<>();
        customerList.add(customerModel);
        userList.add(userSegmentMen);
        Mockito.doThrow(new ModelSavingException("All failed")).when(modelService)
                .saveAll(customerList);
        Mockito.doThrow(new ModelSavingException("failed")).when(modelService)
                .save(customerModel);
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(createCustomerSegmentInfoDto(
                "men", "1401"));
        Assertions.assertThat(response.isSuccess()).isTrue();
        Mockito.verify(modelService, Mockito.times(customerList.size())).save(Mockito.any(CustomerModel.class));
        Mockito.verify(targetUserSegmentService).getAllUserSegmentsMap();
        Mockito.verify(targetUserSegmentService).getAllUserSegmentsForUser(customerModel);
        Mockito.verify(targetUserService).getUserBySubscriptionId("1401");
        Mockito.verify(customerModel).getSubscriberKey();
        Mockito.verify(modelService).saveAll(Mockito.anyCollection());
        Mockito.verify(customerModel).getGroups();
        Mockito.verify(customerModel).setGroups(Mockito.anySet());
        Mockito.verifyZeroInteractions(modelService);
        Mockito.verifyNoMoreInteractions(customerModel, targetUserSegmentService, targetUserService);

    }

    @Test
    public void testUpdateCustomerModelSaveFailedBetweenBatch() throws InterceptorException,
            TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {

        final List<PrincipalGroupModel> userList = new ArrayList<>();
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1401")).willReturn(customerModel);
        BDDMockito.given(targetUserService.getUserBySubscriptionId("1402")).willReturn(customerModel1);
        final HashSet<PrincipalGroupModel> grps = new HashSet<>();
        final UserGroupModel customergroup = Mockito.mock(UserGroupModel.class);
        BDDMockito.when(customergroup.getUid()).thenReturn("customergrp");
        grps.add(customergroup);
        grps.add(userSegmentEntertainment);
        BDDMockito.when(customerModel.getGroups()).thenReturn(grps);
        BDDMockito.when(customerModel1.getGroups()).thenReturn(grps);
        userList.add(userSegmentMen);
        Mockito.doThrow(new ModelSavingException("All failed")).doNothing().when(modelService)
                .saveAll(Mockito.anyCollection());
        Mockito.doThrow(new ModelSavingException("failed")).when(modelService)
                .save(customerModel);
        final CustomerSegmentInfoRequestDto customerSegmentInfoDto = createCustomerSegmentInfoDto(
                "men", "1401", "1402");
        final CustomerSegmentImportResponseDto response = facade.importCustomerSegments(customerSegmentInfoDto);
        Assertions.assertThat(response.isSuccess()).isTrue();

        Mockito.verify(targetUserSegmentService).getAllUserSegmentsMap();
        Mockito.verify(modelService).save(customerModel);
        Mockito.verify(targetUserService).getUserBySubscriptionId("1401");
        Mockito.verify(targetUserService).getUserBySubscriptionId("1402");
        Mockito.verify(targetUserSegmentService).getAllUserSegmentsForUser(customerModel);
        Mockito.verify(targetUserSegmentService).getAllUserSegmentsForUser(customerModel1);
        Mockito.verify(customerModel).getSubscriberKey();
        Mockito.verify(customerModel).getGroups();
        Mockito.verify(customerModel1).getGroups();
        Mockito.verify(customerModel).setGroups(Mockito.anySet());
        Mockito.verify(customerModel1).setGroups(Mockito.anySet());
        BDDMockito.verify(modelService, Mockito.times(2)).saveAll(Mockito.anyCollection());
        Mockito.verifyNoMoreInteractions(customerModel, targetUserSegmentService, targetUserService, modelService);
    }

    @Test
    public void testSaveallForException() throws InterceptorException,
            TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final CustomerModel customer1 = Mockito.mock(CustomerModel.class);
        final CustomerModel customer2 = Mockito.mock(CustomerModel.class);
        final List<CustomerModel> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        Mockito.doThrow(new ModelSavingException("All failed")).doNothing().when(modelService)
                .saveAll(Mockito.anyCollection());
        facade.saveAllCustomers(customerList);
        Mockito.verify(modelService, Mockito.times(customerList.size())).save(Mockito.any(CustomerModel.class));
    }

}
