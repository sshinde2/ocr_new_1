/**
 * 
 */
package au.com.target.tgtwsfacades.stockupdate.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.text.MessageFormat;
import java.util.Arrays;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateResponseDto;
import au.com.target.tgtwsfacades.integration.dto.StockUpdate;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateProductDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateResponseDto;


/**
 * @author mmaki
 * 
 */
@UnitTest
public class StockUpdateIntegrationFacadeTest {

    private static final String PRODUCT_CODE = "1234567";
    private static final String TRANSACTION_DATE = "20130417";
    private static final String TRANSACTION_NUMBER = "REC1234";
    private static final Integer TOTAL_QUANTITY = new Integer(219);
    private static final Integer TOTAL_QUANTITY_ZERO = new Integer(0);
    private static final String WAREHOUSE = "default";
    private static final String COMMENT = "Test comment";
    private static final String EXCEPTION_MESSAGE = "Exception was thrown";
    private static final Integer ADJUSTMENT = new Integer(-2);
    private static final Integer ADJUSTMENT_UP = new Integer(4);
    private static final String PRODUCT_EAN = "12345";

    @Mock
    private TargetStockService stockService;

    @Mock
    private ProductService productService;

    @Mock
    private WarehouseService warehouseService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetProductService targetProductService;

    @Spy
    @InjectMocks
    private final StockUpdateIntegrationFacadeImpl stockUpdateIntegrationFacade = new StockUpdateIntegrationFacadeImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUpdateStockNoAvailabilityGiven() {
        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_AVAILABILITY, response.getMessage());
    }

    @Test
    public void testUpdateStockWarehouseNotFound() {

        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willThrow(
                new UnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        Assert.assertEquals(
                MessageFormat.format(StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_WAREHOUSE, WAREHOUSE),
                response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockProductNotFoundPositiveAvailability() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willThrow(
                new UnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_PRODUCT_AVAILABLE, PRODUCT_CODE, TOTAL_QUANTITY,
                PRODUCT_EAN);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockProductNotFoundZeroAvailability() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willThrow(
                new UnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY_ZERO);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_PRODUCT, PRODUCT_CODE, PRODUCT_EAN);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY_ZERO, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockWithEmptyProductCodeAndEan() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(StringUtils.EMPTY);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_EMPTY_PRODUCT, PRODUCT_CODE);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockWithEmptyProductCode() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);

        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(targetProductService.getProductByEan(PRODUCT_EAN)).willReturn(productModel);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.OK, response.getStatusCode());

        Assert.assertNull(response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());
        Assert.assertEquals(PRODUCT_EAN, response.getItemPrimaryBarcode());

    }

    @Test
    public void testUpdateStockWithNoEANFoundException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);

        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(targetProductService.getProductByEan(PRODUCT_EAN)).willThrow(
                new TargetUnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);
        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_EAN, PRODUCT_EAN);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockWithAmbiguousEANFoundException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);

        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(targetProductService.getProductByEan(PRODUCT_EAN)).willThrow(
                new TargetAmbiguousIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);
        stockUpdateDto.setComment(COMMENT);
        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_MULTILPLE_EAN, PRODUCT_EAN, StringUtils.EMPTY,
                WAREHOUSE, TOTAL_QUANTITY, null, COMMENT);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockOk() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Mockito.verify(stockService).updateStockLevel(productModel, warehouseModel, TOTAL_QUANTITY.intValue(),
                COMMENT);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.OK, response.getStatusCode());
        Assert.assertNull(response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testUpdateStockException() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        BDDMockito.willThrow(new RuntimeException(EXCEPTION_MESSAGE)).given(stockService)
                .updateStockLevel(productModel, warehouseModel, TOTAL_QUANTITY.intValue(), COMMENT);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_OTHER, EXCEPTION_MESSAGE);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());

    }

    @Test
    public void testAdjustStockNoAdjustmentGiven() {
        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERR_STOCKADJUST_AVAILABILITY, response.getMessage());
    }

    @Test
    public void testAdjustStockWarehouseNotFound() {

        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willThrow(
                new UnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        Assert.assertEquals(
                MessageFormat.format(StockUpdateIntegrationFacadeImpl.ERR_STOCKADJUST_WAREHOUSE, WAREHOUSE),
                response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TRANSACTION_NUMBER, response.getTransactionNumber());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());

    }

    @Test
    public void testAdjustStockProductNotFound() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willThrow(
                new UnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKADJUST_PRODUCT, PRODUCT_CODE);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());

    }

    @Test
    public void testAdjustStockOk() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Mockito.verify(stockService).adjustActualAmount(productModel, warehouseModel, ADJUSTMENT.intValue(), COMMENT);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.OK, response.getStatusCode());
        Assert.assertNull(response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());

    }

    @Test
    public void testAdjustStockStockLevelNotFoundExceptionWhenAdjustmentDown() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(productModel.getCode()).willReturn(PRODUCT_CODE);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        BDDMockito.willThrow(new StockLevelNotFoundException(EXCEPTION_MESSAGE)).given(stockService)
                .adjustActualAmount(productModel, warehouseModel, ADJUSTMENT.intValue(), COMMENT);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_WAREHOUSE_FINDSTOCKLEVEL, PRODUCT_CODE);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());
    }

    @Test
    public void testAdjustStockStockLevelNotFoundExceptionWhenAdjustmentUp() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(productModel.getCode()).willReturn(PRODUCT_CODE);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        BDDMockito.willThrow(new StockLevelNotFoundException(EXCEPTION_MESSAGE)).given(stockService)
                .adjustActualAmount(productModel, warehouseModel, ADJUSTMENT_UP.intValue(), COMMENT);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT_UP);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_WAREHOUSE_FINDSTOCKLEVEL, PRODUCT_CODE);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT_UP, response.getAdjustmentQuantity());
    }

    @Test
    public void testAdjustStockException() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        BDDMockito.willThrow(new RuntimeException(EXCEPTION_MESSAGE)).given(stockService)
                .adjustActualAmount(productModel, warehouseModel, ADJUSTMENT.intValue(), COMMENT);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKADJUST_OTHER, EXCEPTION_MESSAGE);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());

    }

    @Test
    public void testAdjustStockStockLevelWithEmptyProductCodeAndEAN() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        final String expectedMsg = StockUpdateIntegrationFacadeImpl.ERR_STOCKADJUST_EMPTY_PRODUCT;
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());
    }

    @Test
    public void testAdjustStockStockWithNoEANFoundException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);

        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(targetProductService.getProductByEan(PRODUCT_EAN)).willThrow(
                new TargetUnknownIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);
        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.NOT_FOUND, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_EAN, PRODUCT_EAN);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());
    }

    @Test
    public void testAdjustStockStockWithAmbiguousEANException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);

        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(targetProductService.getProductByEan(PRODUCT_EAN)).willThrow(
                new TargetAmbiguousIdentifierException(""));

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(StringUtils.EMPTY);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setItemPrimaryBarcode(PRODUCT_EAN);
        stockUpdateDto.setComment(COMMENT);
        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.ERROR, response.getStatusCode());
        final String expectedMsg = MessageFormat.format(
                StockUpdateIntegrationFacadeImpl.ERR_STOCKUPDATE_MULTILPLE_EAN, PRODUCT_EAN, StringUtils.EMPTY,
                WAREHOUSE, null, ADJUSTMENT, COMMENT);
        Assert.assertEquals(expectedMsg, response.getMessage());
        Assert.assertEquals(StringUtils.EMPTY, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());
    }

    @Test
    public void testupdateStock() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        final StockUpdate stockUpdate = new StockUpdate();
        stockUpdate.setQuantity(10);
        stockUpdate.setProductCode(PRODUCT_CODE);
        stockUpdate.setWarehouseCode(WAREHOUSE);

        final IntegrationResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdate);

        Assert.assertNotNull(response);
        Assert.assertTrue(response.isSuccessStatus());

        Mockito.verify(stockService).adjustActualAmount(productModel, warehouseModel, 10, "");

    }

    @Test
    public void testupdateStockWithZero() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        final StockUpdate stockUpdate = new StockUpdate();
        stockUpdate.setQuantity(0);
        stockUpdate.setProductCode(PRODUCT_CODE);
        stockUpdate.setWarehouseCode(WAREHOUSE);

        final IntegrationResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdate);

        Assert.assertNotNull(response);
        Assert.assertTrue(response.isSuccessStatus());

        Mockito.verify(stockService).updateActualStockLevel(productModel, warehouseModel, 0, "");

    }

    @Test
    public void testUpdateStockOkWithColourVariant() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final TargetColourVariantProductModel colourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(colourVariant);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setTotalQuantityAvailable(TOTAL_QUANTITY);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdateDto);

        Mockito.verify(stockService).updateStockLevel(colourVariant, warehouseModel, TOTAL_QUANTITY.intValue(),
                COMMENT);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.OK, response.getStatusCode());
        Assert.assertNull(response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(TOTAL_QUANTITY, response.getTotalQuantityAvailable());
        Mockito.verify(targetProductService).setOnlineDateIfApplicable(colourVariant);

    }

    @Test
    public void testupdateStockWithColourVariant() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final TargetColourVariantProductModel colourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(colourVariant);

        final StockUpdate stockUpdate = new StockUpdate();
        stockUpdate.setQuantity(10);
        stockUpdate.setProductCode(PRODUCT_CODE);
        stockUpdate.setWarehouseCode(WAREHOUSE);

        final IntegrationResponseDto response = stockUpdateIntegrationFacade.updateStock(stockUpdate);

        Assert.assertNotNull(response);
        Assert.assertTrue(response.isSuccessStatus());

        Mockito.verify(stockService).adjustActualAmount(colourVariant, warehouseModel, 10, "");
        Mockito.verify(targetProductService).setOnlineDateIfApplicable(colourVariant);

    }

    @Test
    public void testAdjustStockOkWithColourVariant() {
        final WarehouseModel warehouseModel = Mockito.mock(WarehouseModel.class);
        final TargetColourVariantProductModel colourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(warehouseService.getWarehouseForCode(WAREHOUSE)).willReturn(warehouseModel);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(colourVariant);

        final IntegrationStockUpdateDto stockUpdateDto = new IntegrationStockUpdateDto();
        stockUpdateDto.setAdjustmentQuantity(ADJUSTMENT);
        stockUpdateDto.setItemCode(PRODUCT_CODE);
        stockUpdateDto.setTransactionDate(TRANSACTION_DATE);
        stockUpdateDto.setTransactionNumber(TRANSACTION_NUMBER);
        stockUpdateDto.setWarehouse(WAREHOUSE);
        stockUpdateDto.setComment(COMMENT);

        final IntegrationStockUpdateResponseDto response = stockUpdateIntegrationFacade.adjustStock(stockUpdateDto);

        Mockito.verify(stockService).adjustActualAmount(colourVariant, warehouseModel, ADJUSTMENT.intValue(), COMMENT);

        Assert.assertEquals(StockUpdateIntegrationFacadeImpl.OK, response.getStatusCode());
        Assert.assertNull(response.getMessage());
        Assert.assertEquals(PRODUCT_CODE, response.getItemCode());
        Assert.assertEquals(TRANSACTION_DATE, response.getTransactionDate());
        Assert.assertEquals(ADJUSTMENT, response.getAdjustmentQuantity());
        Mockito.verify(targetProductService).setOnlineDateIfApplicable(colourVariant);

    }

    @Test
    public void testUpdateStockBatchWithEmptyItems() {
        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.updateStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_ERROR, responseDto.getStatusCode());
        Assert.assertEquals("test number", responseDto.getTransactionNumber());
        Assert.assertEquals("test date", responseDto.getTransactionDate());
        Assert.assertEquals(
                "ERR-STOCKUPDATE-BATCHSIZE: Items in the payload are empty. transactionNumber=test number, transactionDate=test date, warehouse=test warehouse, comment=test comment",
                responseDto.getMessage());
    }

    @Test
    public void testUpdateStockBatchWithItemsExceedMaxBatchSize() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto3 = new TargetStockUpdateProductDto();
        productDto3.setItemCode("test product1");
        productDto3.setItemPrimaryBarcode("test EAN1");
        productDto3.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto3.setAdjustmentQuantity(Integer.valueOf(0));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2, productDto3));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.updateStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_EXCEED_MAX_BATCH_SIZE, responseDto.getStatusCode());
    }

    @Test
    public void testUpdateStockBatchWithAllErrorsInItems() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));

        final IntegrationStockUpdateResponseDto responseDto1 = new IntegrationStockUpdateResponseDto();
        responseDto1.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_ERROR);
        BDDMockito.doReturn(responseDto1).doReturn(responseDto1).when(
                stockUpdateIntegrationFacade).updateStock(Mockito.any(IntegrationStockUpdateDto.class));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.updateStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_ERROR, responseDto.getStatusCode());
    }

    @Test
    public void testUpdateStockBatchWithPartialErrorsInItems() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));

        final IntegrationStockUpdateResponseDto responseDto1 = new IntegrationStockUpdateResponseDto();
        responseDto1.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_ERROR);
        final IntegrationStockUpdateResponseDto responseDto2 = new IntegrationStockUpdateResponseDto();
        responseDto2.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_OK);
        BDDMockito.doReturn(responseDto1).doReturn(responseDto2).doReturn(responseDto1).when(
                stockUpdateIntegrationFacade).updateStock(Mockito.any(IntegrationStockUpdateDto.class));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.updateStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_ERROR, responseDto.getStatusCode());
    }

    @Test
    public void testUpdateStockBatchWithAllPassItems() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));

        final IntegrationStockUpdateResponseDto responseDto1 = new IntegrationStockUpdateResponseDto();
        responseDto1.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_OK);
        final IntegrationStockUpdateResponseDto responseDto2 = new IntegrationStockUpdateResponseDto();
        responseDto2.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_OK);
        BDDMockito.doReturn(responseDto1).doReturn(responseDto2).doReturn(responseDto1).when(
                stockUpdateIntegrationFacade).updateStock(Mockito.any(IntegrationStockUpdateDto.class));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.updateStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_OK, responseDto.getStatusCode());
    }

    @Test
    public void testAdjustStockBatchWithEmptyItems() {
        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.adjustStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_ERROR, responseDto.getStatusCode());
        Assert.assertEquals("test number", responseDto.getTransactionNumber());
        Assert.assertEquals("test date", responseDto.getTransactionDate());
        Assert.assertEquals(
                "ERR-STOCKUPDATE-BATCHSIZE: Items in the payload are empty. transactionNumber=test number, transactionDate=test date, warehouse=test warehouse, comment=test comment",
                responseDto.getMessage());
    }

    @Test
    public void testAdjustStockBatchWithItemsExceedMaxBatchSize() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto3 = new TargetStockUpdateProductDto();
        productDto3.setItemCode("test product1");
        productDto3.setItemPrimaryBarcode("test EAN1");
        productDto3.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto3.setAdjustmentQuantity(Integer.valueOf(0));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2, productDto3));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.adjustStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_EXCEED_MAX_BATCH_SIZE, responseDto.getStatusCode());
    }

    @Test
    public void testAdjustStockBatchWithAllErrorsInItems() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));

        final IntegrationStockUpdateResponseDto responseDto1 = new IntegrationStockUpdateResponseDto();
        responseDto1.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_ERROR);
        BDDMockito.doReturn(responseDto1).doReturn(responseDto1).when(
                stockUpdateIntegrationFacade).adjustStock(Mockito.any(IntegrationStockUpdateDto.class));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.adjustStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_ERROR, responseDto.getStatusCode());
    }

    @Test
    public void testAdjustStockBatchWithPartialErrorsInItems() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(0));
        productDto1.setAdjustmentQuantity(Integer.valueOf(100));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(0));
        productDto2.setAdjustmentQuantity(Integer.valueOf(100));

        final IntegrationStockUpdateResponseDto responseDto1 = new IntegrationStockUpdateResponseDto();
        responseDto1.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_ERROR);
        final IntegrationStockUpdateResponseDto responseDto2 = new IntegrationStockUpdateResponseDto();
        responseDto2.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_OK);
        BDDMockito.doReturn(responseDto1).doReturn(responseDto2).doReturn(responseDto1).when(
                stockUpdateIntegrationFacade).adjustStock(Mockito.any(IntegrationStockUpdateDto.class));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.adjustStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_ERROR, responseDto.getStatusCode());
    }

    @Test
    public void testAdjustStockBatchWithAllPassItems() {
        this.stockUpdateIntegrationFacade.setMaxBatchSize(2);
        final TargetStockUpdateProductDto productDto1 = new TargetStockUpdateProductDto();
        productDto1.setItemCode("test product1");
        productDto1.setItemPrimaryBarcode("test EAN1");
        productDto1.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto1.setAdjustmentQuantity(Integer.valueOf(0));
        final TargetStockUpdateProductDto productDto2 = new TargetStockUpdateProductDto();
        productDto2.setItemCode("test product1");
        productDto2.setItemPrimaryBarcode("test EAN1");
        productDto2.setTotalQuantityAvailable(Integer.valueOf(100));
        productDto2.setAdjustmentQuantity(Integer.valueOf(0));

        final IntegrationStockUpdateResponseDto responseDto1 = new IntegrationStockUpdateResponseDto();
        responseDto1.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_OK);
        final IntegrationStockUpdateResponseDto responseDto2 = new IntegrationStockUpdateResponseDto();
        responseDto2.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_OK);
        BDDMockito.doReturn(responseDto1).doReturn(responseDto2).doReturn(responseDto1).when(
                stockUpdateIntegrationFacade).adjustStock(Mockito.any(IntegrationStockUpdateDto.class));

        final TargetStockUpdateDto dto = new TargetStockUpdateDto();
        dto.setTransactionNumber("test number");
        dto.setTransactionDate("test date");
        dto.setComment("test comment");
        dto.setWarehouse("test warehouse");
        dto.setItems(Arrays.asList(productDto1, productDto2));
        final TargetStockUpdateResponseDto responseDto = stockUpdateIntegrationFacade.adjustStockBatch(dto);
        Assert.assertEquals(TargetStockUpdateResponseDto.STATUS_OK, responseDto.getStatusCode());
    }
}
