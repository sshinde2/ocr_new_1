/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentEntry;
import au.com.target.tgtwsfacades.instore.dto.consignments.Product;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;


/**
 * Test suite for {@link InstoreConsignmentEntryConverterTest}.
 * 
 * @author sbryan6
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InstoreConsignmentEntryConverterTest {

    @Mock
    private ConsignmentEntryModel mockConsignmentEntryModel;

    @Mock
    private AbstractOrderEntryModel mockAbstractOrderEntryModel;

    @Mock
    private ProductModel mockProductModel;

    @Mock
    private TargetProductModel productModel;

    @Mock
    private TargetSizeVariantProductModel mockTargetSizeVariantProductModel;

    @Mock
    private TargetColourVariantProductModel mockTargetColourVariantProductModel;

    @Mock
    private AbstractTargetVariantProductModel mockAbstractTargetVariantProductModel;

    @Mock
    private MediaContainerModel mockMediaContainerModel;

    @Mock
    private MediaModel mockMediaModelLarge;

    @Mock
    private MediaModel mockMediaModelThumb;

    @Mock
    private ColourModel mockColourModel;

    @Mock
    private ColourModel mockSwatchColourModel;

    @Mock
    private TargetMerchDepartmentModel mockTargetMerchDepartmentModel;

    @Mock
    private TargetCommercePriceService mockTargetCommercePriceService;

    @Mock
    private PriceRangeInformation mockPriceRangeInformation;


    private String imageContext;



    @InjectMocks
    private final InstoreConsignmentEntryConverter converter = new InstoreConsignmentEntryConverter();

    @Mock
    private Product mockProduct;

    @Before
    public void setup() {
        given(mockConsignmentEntryModel.getQuantity()).willReturn(new Long(123));
        given(mockConsignmentEntryModel.getOrderEntry()).willReturn(mockAbstractOrderEntryModel);
        given(mockConsignmentEntryModel.getOrderEntry().getProduct())
                .willReturn(mockProductModel);

        given(mockTargetColourVariantProductModel.getColour()).willReturn(mockColourModel);
        given(mockTargetColourVariantProductModel.getSwatch()).willReturn(mockSwatchColourModel);


    }


    @Test
    public void testColourVariantConsignmentEntryTestWithColurWithDisplayTrue() {
        given(mockAbstractOrderEntryModel.getBasePrice()).willReturn(Double.valueOf(23.00));
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);

        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getColourName()).willReturn("yellow");
        given(mockTargetColourVariantProductModel.getColour()).willReturn(mockColourModel);
        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isEqualTo("yellow");
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getPrice().intValue()).isEqualTo(2300);


    }

    @Test
    public void testColourVariantConsignmentEntryTestWithColurWithDisplayFalse() {
        given(mockAbstractOrderEntryModel.getBasePrice()).willReturn(Double.valueOf(23.45));
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);

        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.FALSE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");

        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isNull();
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getProduct().getPrice().intValue()).isEqualTo(2345);
        assertThat(ce.getQuantity()).isEqualTo("123");


    }

    @Test
    public void testColourVariantConsignmentEntryTestWithColurWithNullPrice() {
        given(mockAbstractOrderEntryModel.getBasePrice()).willReturn(null);
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);

        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.FALSE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");

        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isNull();
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getProduct().getPrice().intValue()).isEqualTo(0);
        assertThat(ce.getQuantity()).isEqualTo("123");


    }

    @Test
    public void testColourVariantConsignmentEntryTestWithColurWithZeroPrice() {
        given(mockAbstractOrderEntryModel.getBasePrice()).willReturn(Double.valueOf(0));
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);

        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.FALSE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");

        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isNull();
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getProduct().getPrice().intValue()).isEqualTo(0);
        assertThat(ce.getQuantity()).isEqualTo("123");


    }

    @Test
    public void testColourVariantConsignmentEntryTestWithSwatchColourSwatchDisplayTrue() {

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);

        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getSwatch().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getSwatchName()).willReturn("red");

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("blue");
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isEqualTo("red");
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");


    }

    @Test
    public void testColourVariantConsignmentEntryTestWithSwatchColourSwatchDisplayFalse() {

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);

        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getSwatch().getDisplay()).willReturn(Boolean.FALSE);
        given(mockTargetColourVariantProductModel.getSwatchName()).willReturn("red");

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("green");
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isEqualTo("green");
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");


    }

    @Test
    public void testColourVariantConsignmentEntryTestWithoutMedia() {

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn(null);
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn(null);
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getProduct().getThumbImageUrl()).isNull();
        assertThat(ce.getProduct().getLargeImageUrl()).isNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isEqualTo("white");
        assertThat(ce.getProduct().getSize()).isNull();
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");


    }

    @Test
    public void testSizeVariantConsignmentEntryTest() {
        given(mockAbstractOrderEntryModel.getBasePrice()).willReturn(Double.valueOf(23.0));
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetSizeVariantProductModel);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetSizeVariantProductModel.getName()).willReturn("productone");
        given(mockTargetSizeVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetSizeVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetSizeVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getCode()).willReturn("23");
        given(mockTargetSizeVariantProductModel.getSize()).willReturn("L");

        given(mockTargetSizeVariantProductModel.getBaseProduct())
                .willReturn(mockTargetColourVariantProductModel);

        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);


        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        final Product product = ce.getProduct();
        assertThat(product.getCode()).isEqualTo("123345");
        assertThat(product.getColour()).isNull();
        assertThat(product.getSize()).isEqualTo("L");
        assertThat(product.getDepartmentNumber()).isEqualTo("23");
        assertThat(product.getDepartment()).isEqualTo("Womens");
        assertThat(product.getName()).isEqualTo("productone");
        assertThat(product.getPrice()).isEqualTo(Integer.valueOf("2300"));
        assertThat(ce.getQuantity()).isEqualTo("123");
    }

    @Test
    public void testSizeVariantConsignmentEntryTestWithColourDisplayTrue() {
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetSizeVariantProductModel);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetSizeVariantProductModel.getName()).willReturn("productone");
        given(mockTargetSizeVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetSizeVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetSizeVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getCode()).willReturn("23");
        given(mockTargetSizeVariantProductModel.getSize()).willReturn("L");

        given(mockTargetSizeVariantProductModel.getBaseProduct())
                .willReturn(mockTargetColourVariantProductModel);

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");

        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);


        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isEqualTo("white");
        assertThat(ce.getProduct().getSize()).isEqualTo("L");
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");
    }

    @Test
    public void testSizeVariantConsignmentEntryTestWithColourDisplayFalse() {
        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetSizeVariantProductModel);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetSizeVariantProductModel.getName()).willReturn("productone");
        given(mockTargetSizeVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetSizeVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetSizeVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getCode()).willReturn("23");
        given(mockTargetSizeVariantProductModel.getSize()).willReturn("L");

        given(mockTargetSizeVariantProductModel.getBaseProduct())
                .willReturn(mockTargetColourVariantProductModel);

        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.FALSE);
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");

        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);


        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isNull();
        assertThat(ce.getProduct().getSize()).isEqualTo("L");
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");
    }

    @Test
    public void testPopulateSizeVariantConsignmentEntryTest() {
        //setup product details
        final Product product = new Product();

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetSizeVariantProductModel);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetSizeVariantProductModel.getName()).willReturn("productone");
        given(mockTargetSizeVariantProductModel.getEan()).willReturn("9311742000366");
        given(mockTargetSizeVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetSizeVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetSizeVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getCode()).willReturn("123");
        given(mockTargetSizeVariantProductModel.getBaseProduct())
                .willReturn(mockTargetColourVariantProductModel);

        given(mockTargetSizeVariantProductModel.getSize()).willReturn("L");
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        given(mockTargetColourVariantProductModel.getOnlineExclusive()).willReturn(Boolean.TRUE);
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);
        imageContext = "http://localhost";

        converter.setImageContext(imageContext);
        converter.populateProductForSizeVariation(mockTargetSizeVariantProductModel, product);
        assertThat(product.getCode()).isEqualTo("123345");
        assertThat(product.getBarcode()).isEqualTo("9311742000366");
        assertThat(product.getColour()).isNull();
        assertThat(product.getDepartment()).isEqualTo("Womens");
        assertThat(product.getDepartmentNumber()).isEqualTo("123");
        assertThat(product.getLargeImageUrl()).isEqualTo("http://localhost/large/picture");
        assertThat(product.getThumbImageUrl()).isEqualTo("http://localhost/thumb/picture");
        assertThat(product.getSize()).isEqualTo("L");
        assertThat(product.getOnlineExclusive()).isTrue();


    }


    @Test
    public void testPopulateSizeVariantConsignmentEntryTestWithNull() {

        //setup product details
        final Product product = null;
        converter.populateProductForSizeVariation(null, product);
        assertThat(product).isNull();




    }



    @Test
    public void testPopulateColourVariantConsignmentEntryTest() {
        //setup product details
        final Product product = new Product();

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetColourVariantProductModel);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetColourVariantProductModel.getName()).willReturn("productone");
        given(mockTargetColourVariantProductModel.getEan()).willReturn("9311742000366");
        given(mockTargetColourVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetColourVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetColourVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetColourVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetColourVariantProductModel.getMerchDepartment().getCode()).willReturn("123");

        given(mockTargetColourVariantProductModel.getOnlineExclusive()).willReturn(Boolean.TRUE);

        given(mockTargetColourVariantProductModel.getColour().getName()).willReturn("white");
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(
                Collections.singletonList(mockMediaContainerModel));
        imageContext = "http://localhost";
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(mockMediaModelLarge);
        medias.add(mockMediaModelThumb);
        given(mockMediaModelLarge.getCode()).willReturn(TargetMediaImportService.LARGE_MEDIA + "1234");
        given(mockMediaModelLarge.getURL()).willReturn("/large/picture");
        given(mockMediaModelThumb.getCode()).willReturn(TargetMediaImportService.THUMB_MEDIA + "1234");
        given(mockMediaModelThumb.getURL()).willReturn("/thumb/picture");
        given(mockMediaContainerModel.getMedias()).willReturn(medias);

        converter.setImageContext(imageContext);
        converter.populateProductForColourVariation(mockTargetColourVariantProductModel, product);
        assertThat(product.getCode()).isEqualTo("123345");
        assertThat(product.getBarcode()).isEqualTo("9311742000366");
        assertThat(product.getDepartment()).isEqualTo("Womens");
        assertThat(product.getDepartmentNumber()).isEqualTo("123");
        assertThat(product.getLargeImageUrl()).isEqualTo("http://localhost/large/picture");
        assertThat(product.getThumbImageUrl()).isEqualTo("http://localhost/thumb/picture");
        assertThat(product.getSize()).isNull();
        assertThat(product.getOnlineExclusive()).isTrue();
    }



    @Test
    public void testPopulateColourVariantConsignmentEntryTestWithNullPrice() {
        //setup product details
        final Product product = null;
        converter.populateProductForColourVariation(null, product);
        assertThat(product).isNull();

    }


    @Test
    public void testSizeVariantConsignmentEntryTestWithoutMedia() {

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                mockTargetSizeVariantProductModel);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn("123345");
        given(mockTargetSizeVariantProductModel.getName()).willReturn("productone");
        given(mockTargetSizeVariantProductModel.getPriceQuantity()).willReturn(new Double(23));
        given(mockTargetSizeVariantProductModel.getDepartment()).willReturn(new Integer(23));
        given(mockTargetSizeVariantProductModel.getMerchDepartment()).willReturn(
                mockTargetMerchDepartmentModel);
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getName()).willReturn("Womens");
        given(mockTargetSizeVariantProductModel.getMerchDepartment().getCode()).willReturn("23");

        given(mockTargetSizeVariantProductModel.getBaseProduct())
                .willReturn(mockTargetColourVariantProductModel);

        given(mockTargetSizeVariantProductModel.getSize()).willReturn("L");
        given(mockTargetColourVariantProductModel.getGalleryImages()).willReturn(null);

        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getProduct().getThumbImageUrl()).isNull();
        assertThat(ce.getProduct().getLargeImageUrl()).isNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
        assertThat(ce.getProduct().getCode()).isEqualTo("123345");
        assertThat(ce.getProduct().getColour()).isNull();
        assertThat(ce.getProduct().getSize()).isEqualTo("L");
        assertThat(ce.getProduct().getDepartmentNumber()).isEqualTo("23");
        assertThat(ce.getProduct().getDepartment()).isEqualTo("Womens");
        assertThat(ce.getProduct().getName()).isEqualTo("productone");
        assertThat(ce.getQuantity()).isEqualTo("123");
    }

    @Test
    public void testNullProductEntryTest() {

        given(mockConsignmentEntryModel.getOrderEntry().getProduct()).willReturn(
                null);
        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(mockConsignmentEntryModel);
        assertThat(ce).isNotNull();
        assertThat(ce.getProduct()).isNull();
        assertThat(ce.getQuantity()).isEqualTo("123");
    }

    @Test
    public void testNullConsignmentEntryModelEntryTest() {


        converter.setImageContext(imageContext);
        final ConsignmentEntry ce = converter.convert(null);
        assertThat(ce).isNull();

    }

    @Test
    public void testPopulateColourDetailsWithColour() {
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");
        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        final Product product = new Product();
        product.setColour("white");
        converter.populateColourDetails(mockTargetColourVariantProductModel, product);
        assertThat(product.getColour()).isEqualTo("white");

    }

    @Test
    public void testPopulateColourDetailsWithSwatchColour() {
        given(mockTargetColourVariantProductModel.getSwatch().getDisplay()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getSwatchName()).willReturn("red");
        given(mockTargetColourVariantProductModel.getColourName()).willReturn("white");
        given(mockTargetColourVariantProductModel.getColour()).willReturn(mockColourModel);
        given(mockTargetColourVariantProductModel.getColour().getDisplay()).willReturn(Boolean.TRUE);
        final Product product = new Product();
        product.setColour("white");
        converter.populateColourDetails(mockTargetColourVariantProductModel, product);
        assertThat(product.getColour()).isEqualTo("red");

    }

    @Test
    public void testPopulateColourDetailsWithNull() {

        final Product product = new Product();
        converter.populateColourDetails(null, product);
        assertThat(product.getColour()).isNull();

    }

    @Test
    public void testPopulateBaseProductAttributesWithSizeVariantNoFlags() {
        final Product product = new Product();
        final TargetSizeVariantProductModel prodModel = getMockSizeVariant(
                getMockColourVariant(getMockProduct(null, null)));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isNull();
        assertThat(product.getBulky()).isNull();
        assertThat(product.getMhd()).isNull();
    }

    @Test
    public void testPopulateBaseProductAttributesWithSizeVariantAllFlagsFalse() {
        final Product product = new Product();
        final TargetSizeVariantProductModel prodModel = getMockSizeVariant(getMockColourVariant(
                getMockProduct(Boolean.FALSE, createProductTypeMock(Boolean.FALSE, "home-delivery"))));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.FALSE);
        assertThat(product.getBulky()).isEqualTo(Boolean.FALSE);
        assertThat(product.getMhd()).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithSizeVariantAllFlagsTrue() {
        final Product product = new Product();
        final TargetSizeVariantProductModel prodModel = getMockSizeVariant(getMockColourVariant(
                getMockProduct(Boolean.TRUE, createProductTypeMock(Boolean.TRUE, "mhd"))));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.TRUE);
        assertThat(product.getBulky()).isEqualTo(Boolean.TRUE);
        assertThat(product.getMhd()).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithSizeVariantClearanceOnly() {
        final Product product = new Product();
        final TargetSizeVariantProductModel prodModel = getMockSizeVariant(getMockColourVariant(
                getMockProduct(Boolean.TRUE, createProductTypeMock(Boolean.FALSE, "home-delivery"))));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.TRUE);
        assertThat(product.getBulky()).isEqualTo(Boolean.FALSE);
        assertThat(product.getMhd()).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithSizeVariantBulkyOnly() {
        final Product product = new Product();
        final TargetSizeVariantProductModel prodModel = getMockSizeVariant(getMockColourVariant(
                getMockProduct(Boolean.FALSE, createProductTypeMock(Boolean.TRUE, "home-delivery"))));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.FALSE);
        assertThat(product.getBulky()).isEqualTo(Boolean.TRUE);
        assertThat(product.getMhd()).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithSizeVariantMHDOnly() {
        final Product product = new Product();
        final TargetSizeVariantProductModel prodModel = getMockSizeVariant(getMockColourVariant(
                getMockProduct(Boolean.FALSE, createProductTypeMock(Boolean.FALSE, "mhd"))));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.FALSE);
        assertThat(product.getBulky()).isEqualTo(Boolean.FALSE);
        assertThat(product.getMhd()).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithColourVariantNoFlags() {
        final Product product = new Product();
        final TargetColourVariantProductModel prodModel = getMockColourVariant(getMockProduct(null, null));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isNull();
        assertThat(product.getBulky()).isNull();
        assertThat(product.getMhd()).isNull();
    }

    @Test
    public void testPopulateBaseProductAttributesWithColourVariantAllFlagsFalse() {
        final Product product = new Product();
        final TargetColourVariantProductModel prodModel = getMockColourVariant(
                getMockProduct(Boolean.FALSE, createProductTypeMock(Boolean.FALSE, "home-delivery")));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.FALSE);
        assertThat(product.getBulky()).isEqualTo(Boolean.FALSE);
        assertThat(product.getMhd()).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithColourVariantAllFlagsTrue() {
        final Product product = new Product();
        final TargetColourVariantProductModel prodModel = getMockColourVariant(
                getMockProduct(Boolean.TRUE, createProductTypeMock(Boolean.TRUE, "mhd")));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.TRUE);
        assertThat(product.getBulky()).isEqualTo(Boolean.TRUE);
        assertThat(product.getMhd()).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithColourVariantClearanceOnly() {
        final Product product = new Product();
        final TargetColourVariantProductModel prodModel = getMockColourVariant(
                getMockProduct(Boolean.TRUE, createProductTypeMock(Boolean.FALSE, "home-delivery")));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.TRUE);
        assertThat(product.getBulky()).isEqualTo(Boolean.FALSE);
        assertThat(product.getMhd()).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithColourVariantBulkyOnly() {
        final Product product = new Product();
        final TargetColourVariantProductModel prodModel = getMockColourVariant(
                getMockProduct(Boolean.FALSE, createProductTypeMock(Boolean.TRUE, "home-delivery")));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.FALSE);
        assertThat(product.getBulky()).isEqualTo(Boolean.TRUE);
        assertThat(product.getMhd()).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testPopulateBaseProductAttributesWithColourVariantMHDOnly() {
        final Product product = new Product();
        final TargetColourVariantProductModel prodModel = getMockColourVariant(
                getMockProduct(Boolean.FALSE, createProductTypeMock(Boolean.FALSE, "mhd")));

        converter.populateBaseProductAttributes(prodModel, product);

        assertThat(product.getClearance()).isEqualTo(Boolean.FALSE);
        assertThat(product.getBulky()).isEqualTo(Boolean.FALSE);
        assertThat(product.getMhd()).isEqualTo(Boolean.TRUE);
    }

    private ProductTypeModel createProductTypeMock(final Boolean isBulky, final String code) {
        final ProductTypeModel mockProductType = mock(ProductTypeModel.class);

        given(mockProductType.getBulky()).willReturn(isBulky);
        given(mockProductType.getCode()).willReturn(code);

        return mockProductType;
    }

    private TargetProductModel getMockProduct(final Boolean clearance, final ProductTypeModel productType) {
        final TargetProductModel product = mock(TargetProductModel.class);

        given(product.getClearanceProduct()).willReturn(clearance);
        given(product.getProductType()).willReturn(productType);

        return product;
    }

    private TargetSizeVariantProductModel getMockSizeVariant(final ProductModel baseProduct) {
        final TargetSizeVariantProductModel sizeVaraint = mock(TargetSizeVariantProductModel.class);
        given(sizeVaraint.getBaseProduct()).willReturn(baseProduct);
        return sizeVaraint;
    }

    private TargetColourVariantProductModel getMockColourVariant(final ProductModel baseProduct) {
        final TargetColourVariantProductModel colourVaraint = mock(TargetColourVariantProductModel.class);
        given(colourVaraint.getBaseProduct()).willReturn(baseProduct);
        return colourVaraint;
    }

}
