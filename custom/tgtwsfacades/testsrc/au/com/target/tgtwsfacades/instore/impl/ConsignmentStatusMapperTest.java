/**
 * 
 */
package au.com.target.tgtwsfacades.instore.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtwsfacades.constants.TgtwsfacadesConstants;


/**
 * @author sbryan6
 *
 */
@UnitTest
public class ConsignmentStatusMapperTest {

    private final ConsignmentStatusMapperImpl mapper = new ConsignmentStatusMapperImpl();

    @Before
    public void setup() {

        mapper.setConsignmentStatusMap(createConsignmentStatsStatusMap());
    }

    @Test
    public void testStates() {

        assertThat(mapper.isOpen(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isTrue();
        assertThat(mapper.isCompleted(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isFalse();
        assertThat(mapper.isRejected(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isFalse();
        assertThat(mapper.isPicked(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isFalse();

        assertThat(mapper.isOpen(ConsignmentStatus.SENT_TO_WAREHOUSE)).isTrue();
        assertThat(mapper.isCompleted(ConsignmentStatus.SENT_TO_WAREHOUSE)).isFalse();
        assertThat(mapper.isRejected(ConsignmentStatus.SENT_TO_WAREHOUSE)).isFalse();
        assertThat(mapper.isPicked(ConsignmentStatus.SENT_TO_WAREHOUSE)).isFalse();

        assertThat(mapper.isOpen(ConsignmentStatus.SHIPPED)).isFalse();
        assertThat(mapper.isCompleted(ConsignmentStatus.SHIPPED)).isTrue();
        assertThat(mapper.isRejected(ConsignmentStatus.SHIPPED)).isFalse();
        assertThat(mapper.isPicked(ConsignmentStatus.SHIPPED)).isFalse();

        assertThat(mapper.isOpen(ConsignmentStatus.PICKED)).isFalse();
        assertThat(mapper.isCompleted(ConsignmentStatus.PICKED)).isFalse();
        assertThat(mapper.isRejected(ConsignmentStatus.PICKED)).isFalse();
        assertThat(mapper.isPicked(ConsignmentStatus.PICKED)).isTrue();

        assertThat(mapper.isOpen(ConsignmentStatus.CANCELLED)).isFalse();
        assertThat(mapper.isCompleted(ConsignmentStatus.CANCELLED)).isFalse();
        assertThat(mapper.isRejected(ConsignmentStatus.CANCELLED)).isTrue();

    }

    @Test
    public void testGetOFCStatus() {

        assertThat(mapper.getOFCStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isEqualTo(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN);
        assertThat(mapper.getOFCStatus(ConsignmentStatus.SENT_TO_WAREHOUSE)).isEqualTo(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN);
        assertThat(mapper.getOFCStatus(ConsignmentStatus.SHIPPED)).isEqualTo(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_COMPLETED);
        assertThat(mapper.getOFCStatus(ConsignmentStatus.CANCELLED)).isEqualTo(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_REJECTED);
        assertThat(mapper.getOFCStatus(ConsignmentStatus.PICKED)).isEqualTo(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_PICKED);
    }


    public static Map<String, List<ConsignmentStatus>> createConsignmentStatsStatusMap() {

        final Map<String, List<ConsignmentStatus>> consignmentStatsStatusMap = new HashMap<String, List<ConsignmentStatus>>();

        final List<ConsignmentStatus> list = new ArrayList<>();
        list.add(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        list.add(ConsignmentStatus.SENT_TO_WAREHOUSE);

        consignmentStatsStatusMap.put(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN,
                list);
        consignmentStatsStatusMap.put(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_INPROGRESS,
                Collections.singletonList(ConsignmentStatus.WAVED));
        consignmentStatsStatusMap.put(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_PICKED,
                Collections.singletonList(ConsignmentStatus.PICKED));
        consignmentStatsStatusMap.put(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_PACKED,
                Collections.singletonList(ConsignmentStatus.PACKED));
        consignmentStatsStatusMap.put(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_COMPLETED,
                Collections.singletonList(ConsignmentStatus.SHIPPED));
        consignmentStatsStatusMap.put(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_REJECTED,
                Collections.singletonList(ConsignmentStatus.CANCELLED));

        return consignmentStatsStatusMap;
    }



}
