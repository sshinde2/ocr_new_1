/**
 * 
 */
package au.com.target.tgtwsfacades.orderAck.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.helper.OrderConsignmentHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.Orders;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderAckIntegrationFacadeImplTest {

    private static final String ORDER_NUMBER_1 = "12345";
    private static final String ORDER_NUMBER_2 = "34567";

    @Mock
    private TargetFulfilmentService targetFulfilmentService;

    @Mock
    private OrderConsignmentHelper orderConsignmentHelper;

    @InjectMocks
    private final OrderAckIntegrationFacadeImpl orderAckFacade = new OrderAckIntegrationFacadeImpl();

    @Mock
    private TargetConsignmentModel con1;

    @Mock
    private TargetConsignmentModel con2;

    @Mock
    private OrderModel order1;

    @Mock
    private OrderModel order2;

    @Before
    public void setUp() {
        Mockito.when(orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(ORDER_NUMBER_1)).thenReturn(con1);
        Mockito.when(orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(ORDER_NUMBER_2)).thenReturn(con2);

        Mockito.when(con1.getOrder()).thenReturn(order1);
        Mockito.when(con2.getOrder()).thenReturn(order2);

        Mockito.when(order1.getCode()).thenReturn(ORDER_NUMBER_1);
        Mockito.when(order2.getCode()).thenReturn(ORDER_NUMBER_2);
    }

    @Test
    public void updateOrderAcknowledgementsHappyPath()
            throws FulfilmentException, ConsignmentStatusValidationException {
        final Orders orders = new Orders();
        orders.addOrder(ORDER_NUMBER_1);
        orders.addOrder(ORDER_NUMBER_2);

        final List<IntegrationResponseDto> responses = orderAckFacade.updateOrderAcknowledgements(orders);
        Assert.assertNotNull(responses);
        Assert.assertEquals(2, responses.size());

        for (final IntegrationResponseDto response : responses) {
            Assert.assertTrue(response.isSuccessStatus());
        }

        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_1, con1,
                LoggingContext.WAREHOUSE);
        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_2, con2,
                LoggingContext.WAREHOUSE);
        Mockito.verifyNoMoreInteractions(targetFulfilmentService);
    }

    @Test
    public void updateOrderAcknowledgementsThrowsException() throws Exception {
        final Orders orders = new Orders();
        orders.addOrder(ORDER_NUMBER_1);
        orders.addOrder(ORDER_NUMBER_2);
        Mockito.doThrow(new FulfilmentException("test exception")).when(targetFulfilmentService)
                .processAckForConsignment(ORDER_NUMBER_1, con1, LoggingContext.WAREHOUSE);
        final List<IntegrationResponseDto> responses = orderAckFacade.updateOrderAcknowledgements(orders);
        Assert.assertNotNull(responses);
        Assert.assertEquals(2, responses.size());
        for (final IntegrationResponseDto response : responses) {
            final String code = response.getCode();
            if (code.equals(ORDER_NUMBER_1)) {
                Assert.assertFalse(response.isSuccessStatus());
            }
            else {
                Assert.assertTrue(response.isSuccessStatus());
            }
        }

        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_1, con1,
                LoggingContext.WAREHOUSE);
        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_2, con2,
                LoggingContext.WAREHOUSE);
        Mockito.verifyNoMoreInteractions(targetFulfilmentService);
    }

    /**
     * Verifies that if order validation throws AmbiguousIdentifierException a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test
    public void updateOrderAcknowledgementsThrowsAmbiguousIdException() throws Exception {
        final Orders orders = new Orders();
        orders.addOrder(ORDER_NUMBER_1);
        orders.addOrder(ORDER_NUMBER_2);
        Mockito.doThrow(new AmbiguousIdentifierException("test ambiguous identifier exception"))
                .when(targetFulfilmentService)
                .processAckForConsignment(ORDER_NUMBER_1, con1, LoggingContext.WAREHOUSE);
        final List<IntegrationResponseDto> responses = orderAckFacade.updateOrderAcknowledgements(orders);
        Assert.assertNotNull(responses);
        Assert.assertEquals(2, responses.size());
        for (final IntegrationResponseDto response : responses) {
            final String code = response.getCode();
            if (code.equals(ORDER_NUMBER_1)) {
                Assert.assertFalse(response.isSuccessStatus());
            }
            else {
                Assert.assertTrue(response.isSuccessStatus());
            }
        }

        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_1, con1,
                LoggingContext.WAREHOUSE);
        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_2, con2,
                LoggingContext.WAREHOUSE);
        Mockito.verifyNoMoreInteractions(targetFulfilmentService);
    }

    /**
     * Verifies that if order validation throws UnknownIdentifierException a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test
    public void updateOrderAcknowledgementsThrowsUnknownIdException() throws Exception {
        final Orders orders = new Orders();
        orders.addOrder(ORDER_NUMBER_1);
        orders.addOrder(ORDER_NUMBER_2);
        Mockito.doThrow(new UnknownIdentifierException("test unknown identifier exception"))
                .when(targetFulfilmentService)
                .processAckForConsignment(ORDER_NUMBER_1, con1, LoggingContext.WAREHOUSE);
        final List<IntegrationResponseDto> responses = orderAckFacade.updateOrderAcknowledgements(orders);
        Assert.assertNotNull(responses);
        Assert.assertEquals(2, responses.size());
        for (final IntegrationResponseDto response : responses) {
            final String code = response.getCode();
            if (code.equals(ORDER_NUMBER_1)) {
                Assert.assertFalse(response.isSuccessStatus());
            }
            else {
                Assert.assertTrue(response.isSuccessStatus());
            }
        }

        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_1, con1,
                LoggingContext.WAREHOUSE);
        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_2, con2,
                LoggingContext.WAREHOUSE);
        Mockito.verifyNoMoreInteractions(targetFulfilmentService);
    }

    /**
     * Verifies that if a generic exception occurs a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test(expected = Exception.class)
    public void updateOrderAcknowledgementsThrowsGenericException() throws Exception {
        final Orders orders = new Orders();
        orders.addOrder(ORDER_NUMBER_1);
        orders.addOrder(ORDER_NUMBER_2);

        Mockito.doThrow(new Exception("test exception")).when(targetFulfilmentService)
                .processAckForConsignment(ORDER_NUMBER_1, con1, LoggingContext.WAREHOUSE);

        final List<IntegrationResponseDto> responses = orderAckFacade.updateOrderAcknowledgements(orders);
        Assert.assertNotNull(responses);
        Assert.assertEquals(2, responses.size());
        for (final IntegrationResponseDto response : responses) {
            final String code = response.getCode();
            if (code.equals(ORDER_NUMBER_1)) {
                Assert.assertFalse(response.isSuccessStatus());
            }
            else {
                Assert.assertTrue(response.isSuccessStatus());
            }
        }

        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_1, con1,
                LoggingContext.WAREHOUSE);
        Mockito.verify(targetFulfilmentService).processAckForConsignment(ORDER_NUMBER_2, con2,
                LoggingContext.WAREHOUSE);
        Mockito.verifyNoMoreInteractions(targetFulfilmentService);
    }
}
