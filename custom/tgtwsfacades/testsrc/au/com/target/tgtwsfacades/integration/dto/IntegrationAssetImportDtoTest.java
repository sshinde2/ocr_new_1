/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class IntegrationAssetImportDtoTest {

    @Test
    public void testMarshalAssets() throws JAXBException {
        final IntegrationAssetImportDto dto = new IntegrationAssetImportDto();
        dto.setAssetId("1234");
        dto.setMime("image-jpeg");
        final IntegrationMediasDto mediasdto = new IntegrationMediasDto();
        final List<IntegrationMediaDto> medias = new ArrayList<>();
        final IntegrationMediaDto media1 = new IntegrationMediaDto();
        media1.setMediaFormat("format1");
        media1.setMediaPath("blah");
        medias.add(media1);
        final IntegrationMediaDto media2 = new IntegrationMediaDto();
        media2.setMediaFormat("format2");
        media2.setMediaPath("blah2");
        medias.add(media1);
        mediasdto.setMedia(medias);
        dto.setMedias(mediasdto);
        final String xml = marshalType(dto);
        Assert.assertNotNull(xml);
        final IntegrationAssetImportDto assets = unMarshalProductTypes(xml);
        Assert.assertNotNull(assets);
    }

    private String marshalType(final IntegrationAssetImportDto types)
            throws JAXBException {
        final StringWriter writer = new StringWriter();
        final JAXBContext jaxbContext = JAXBContext.newInstance(IntegrationAssetImportDto.class);

        final Marshaller jaxbUnmarshaller = jaxbContext.createMarshaller();
        jaxbUnmarshaller.marshal(types, writer);
        return writer.toString();

    }

    private IntegrationAssetImportDto unMarshalProductTypes(final String xml)
            throws JAXBException {
        final JAXBContext jaxbContext = JAXBContext.newInstance(IntegrationAssetImportDto.class);

        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (IntegrationAssetImportDto)jaxbUnmarshaller.unmarshal(new StringReader(xml));
    }

}
