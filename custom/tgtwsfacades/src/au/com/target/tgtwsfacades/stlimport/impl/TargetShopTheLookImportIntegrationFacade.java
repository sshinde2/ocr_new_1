/**
 * 
 */
package au.com.target.tgtwsfacades.stlimport.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.media.TargetMediaContainerService;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.LookCollectionService;
import au.com.target.tgtmarketing.shopthelook.LookProductService;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookCollectionDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;
import au.com.target.tgtwsfacades.stlimport.ShopTheLookImportIntegrationFacade;
import au.com.target.tgtwsfacades.stlimport.TargetStlImportConstants;
import au.com.target.tgtwsfacades.stlimport.validators.TargetShopTheLookImportValidator;


/**
 * @author mgazal
 * 
 */
public class TargetShopTheLookImportIntegrationFacade implements ShopTheLookImportIntegrationFacade {

    private static final Logger LOG = Logger.getLogger(TargetShopTheLookImportIntegrationFacade.class);

    private List<TargetShopTheLookImportValidator> validators;

    private ModelService modelService;

    private CategoryService categoryService;

    private ShopTheLookService shopTheLookService;

    private LookCollectionService lookCollectionService;

    private LookService lookService;

    private LookProductService lookProductService;

    private TargetMediaImportService targetMediaImportService;

    private TargetMediaContainerService targetMediaContainerService;

    private TargetProductService targetProductService;

    private TargetProductImportUtil targetProductImportUtil;

    /**
     * to enable/disable media association (disabling can improve performance)
     */
    private boolean associateMedia = true;

    private DateTimeFormatter dateFormat;


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.LookCollectionImportIntegrationFacade#persistTargetLookCollection(au.com.target.tgtwsfacades.integration.dto.IntegrationLookCollectionDto)
     */
    @Override
    public IntegrationResponseDto persistTargetShopTheLook(final IntegrationShopTheLookDto dto) {

        final String code = dto.getCode();
        final String name = dto.getName();
        final IntegrationSupercategoryDto category = dto.getSupercategory();

        final IntegrationResponseDto response = new IntegrationResponseDto(code);
        response.setSuccessStatus(true);

        if (StringUtils.isBlank(code) || StringUtils.isBlank(name)
                || category == null || StringUtils.isBlank(category.getCode())) {

            response.setSuccessStatus(false);
            response.addMessage("One of required values is missing [Shop the Look Code, Name, Super Category]");
            return response;
        }

        synchronized (code.intern()) {
            try {
                return processTargetShopTheLook(dto, response);
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate products are getting created
                // we will only re-try to create product 1 more time
                LOG.warn(
                        "WARN_STL_IMPORT_EXCEPTION : Exception occured when saving a shop the look with the message : "
                                + mse.getMessage());
                if (mse.getMessage().toUpperCase().contains(TargetStlImportConstants.UNIQUE)) {
                    try {
                        LOG.warn(
                                "WARN_STL_IMPORT_EXCEPTION : A unique identifier exception has been thrown. Attempting to reimport the shop the look with code "
                                        + dto.getCode());
                        return processTargetShopTheLook(dto, response);
                    }
                    catch (final Exception e) {
                        return generateExceptionResponse(response, e);
                    }
                }
                else {
                    return generateExceptionResponse(response, mse);
                }
            }
            catch (final Exception e) {
                return generateExceptionResponse(response, e);
            }
        }
    }

    private IntegrationResponseDto generateExceptionResponse(final IntegrationResponseDto response, final Exception e) {
        final String message = MessageFormat.format(
                TargetStlImportConstants.LogMessages.ERR_STL_IMPORT_EXCEPTION, response.getCode());
        LOG.error(message, e);
        final String errMsg = e.getMessage() == null ? " An unexpected exception occured during the import" : " " + e
                .getMessage();
        response.setSuccessStatus(false);
        response.addMessage(message + errMsg);

        return response;
    }

    /**
     * @param dto
     * @param response
     * @return {@link IntegrationResponseDto}
     * @throws InstantiationException
     */
    private IntegrationResponseDto processTargetShopTheLook(
            final IntegrationShopTheLookDto dto,
            final IntegrationResponseDto response)
            throws InstantiationException {
        final String code = dto.getCode();
        final String name = dto.getName();

        if (!isImportDataValid(dto, response)) {
            if (LOG.isInfoEnabled()) {
                LOG.info(MessageFormat.format(TargetStlImportConstants.LogMessages.ERR_STL_PREIMPORT_VALIDATION,
                        code, name, response.getMessages()));
            }
            response.setSuccessStatus(false);
            return response;
        }

        LOG.info(MessageFormat.format(TargetStlImportConstants.LogMessages.INFO_STL_PREIMPORT_VALIDATION, code,
                name));

        createOrUpdateShopTheLook(dto);
        response.setSuccessStatus(true);
        return response;
    }

    /**
     * 
     * @param dto
     * @return Success / Failure flag.
     * @throws InstantiationException
     */
    private boolean isImportDataValid(final IntegrationShopTheLookDto dto, final IntegrationResponseDto response)
            throws InstantiationException {

        if (CollectionUtils.isNotEmpty(validators)) {
            for (final TargetShopTheLookImportValidator validator : validators) {
                final List<String> messages = validator.validate(dto);
                if (CollectionUtils.isNotEmpty(messages)) {
                    response.addMessages(messages);
                }
            }
        }

        return CollectionUtils.isEmpty(response.getMessages());
    }

    /**
     * @param dto
     * @return {@link TargetShopTheLookModel}
     */
    private TargetShopTheLookModel createOrUpdateShopTheLook(final IntegrationShopTheLookDto dto) {
        TargetShopTheLookModel shopTheLook = getExistingShopTheLook(dto.getCode());
        if (shopTheLook == null) {
            shopTheLook = createNewTargetShopTheLook(dto);
            try {
                populateShopTheLook(shopTheLook, dto);
                modelService.save(shopTheLook);
                LOG.info(MessageFormat.format(TargetStlImportConstants.LogMessages.INFO_NEW_SHOPTHELOOK,
                        dto.getCode(), dto.getName()));
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate models are getting created
                // we will only re-try to create model 1 more time
                if (mse.getMessage().toUpperCase().contains(TargetStlImportConstants.UNIQUE)) {
                    shopTheLook = getExistingShopTheLook(dto.getCode());
                    populateShopTheLook(shopTheLook, dto);
                    modelService.save(shopTheLook);
                }
                else {
                    throw mse;
                }
            }
        }
        else {
            populateShopTheLook(shopTheLook, dto);
            modelService.save(shopTheLook);
        }

        return shopTheLook;
    }

    /**
     * @param shopTheLook
     * @param dto
     */
    private void populateShopTheLook(final TargetShopTheLookModel shopTheLook,
            final IntegrationShopTheLookDto dto) {
        shopTheLook.setName(dto.getName());
        shopTheLook.setDescription(dto.getDescription());

        CategoryModel category = null;
        if (dto.getSupercategory() != null) {
            category = categoryService.getCategoryForCode(dto.getSupercategory().getCode());
        }
        shopTheLook.setCategory(category);
        if (!CollectionUtils.isEmpty(dto.getCollections())) {
            for (final IntegrationLookCollectionDto lookCollection : dto.getCollections()) {
                try {
                    createOrUpdateLookCollection(lookCollection, shopTheLook);
                }
                catch (final Exception e) {
                    LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.ERR_STL_IMPORT_LOOKCOLLECTION,
                            lookCollection.getCode(),
                            lookCollection.getName()), e);
                }
            }
        }
    }

    /**
     * 
     * @param code
     * @return existing collection or null
     */
    private TargetShopTheLookModel getExistingShopTheLook(final String code) {
        try {
            return shopTheLookService.getShopTheLookForCode(code);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.info(e.getMessage());
        }
        return null;
    }

    /**
     * 
     * @param dto
     * @return New TargetShopTheLookModel populated with basic information from dto
     */
    private TargetShopTheLookModel createNewTargetShopTheLook(final IntegrationShopTheLookDto dto) {
        final TargetShopTheLookModel shopTheLook = modelService.create(TargetShopTheLookModel.class);
        shopTheLook.setId(dto.getCode());
        return shopTheLook;
    }

    /**
     * 
     * @param dto
     * @throws InstantiationException
     */
    private void createOrUpdateLookCollection(final IntegrationLookCollectionDto dto,
            final TargetShopTheLookModel shopTheLook)
            throws InstantiationException {
        TargetLookCollectionModel lookCollection = getExistingCollection(dto.getCode());
        if (lookCollection == null) {
            lookCollection = createNewTargetLookCollection(dto);
            try {
                populateLookCollection(lookCollection, dto);
                lookCollection.setShopTheLook(shopTheLook);
                modelService.save(lookCollection);
                LOG.info(MessageFormat.format(TargetStlImportConstants.LogMessages.INFO_NEW_LOOKCOLLECTION,
                        dto.getCode(), dto.getName()));
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate models are getting created
                // we will only re-try to create model 1 more time
                if (mse.getMessage().toUpperCase().contains(TargetStlImportConstants.UNIQUE)) {
                    lookCollection = getExistingCollection(dto.getCode());
                    populateLookCollection(lookCollection, dto);
                    lookCollection.setShopTheLook(shopTheLook);
                    modelService.save(lookCollection);
                }
                else {
                    throw mse;
                }
            }
        }
        else {
            populateLookCollection(lookCollection, dto);
            lookCollection.setShopTheLook(shopTheLook);
            modelService.save(lookCollection);
        }
    }

    /**
     * 
     * @param code
     * @return existing collection or null
     */
    private TargetLookCollectionModel getExistingCollection(final String code) {
        try {
            return lookCollectionService.getLookCollectionForCode(code);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.info(e.getMessage());
        }
        return null;
    }

    private void populateLookCollection(final TargetLookCollectionModel lookCollection,
            final IntegrationLookCollectionDto dto) {
        lookCollection.setName(dto.getName());
        lookCollection.setDescription(dto.getDescription());

        if (!CollectionUtils.isEmpty(dto.getLooks())) {
            for (final IntegrationLookDto look : dto.getLooks()) {
                try {
                    createOrUpdateLook(look, lookCollection);
                }
                catch (final Exception e) {
                    LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.ERR_STL_IMPORT_LOOK,
                            look.getCode(),
                            look.getName()), e);
                }
            }
        }
    }


    /**
     * @param dto
     * @param lookCollection
     */
    private void createOrUpdateLook(final IntegrationLookDto dto,
            final TargetLookCollectionModel lookCollection) {
        TargetLookModel look = getExistingLook(dto.getCode());
        if (look == null) {
            look = createNewTargetLook(dto);
            try {
                populateLook(look, dto);
                look.setCollection(lookCollection);
                modelService.save(look);
                LOG.info(MessageFormat.format(TargetStlImportConstants.LogMessages.INFO_NEW_LOOK,
                        dto.getCode(), dto.getName()));
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate models are getting created
                // we will only re-try to create model 1 more time
                if (mse.getMessage().toUpperCase().contains(TargetStlImportConstants.UNIQUE)) {
                    look = getExistingLook(dto.getCode());
                    populateLook(look, dto);
                    look.setCollection(lookCollection);
                    modelService.save(look);
                }
                else {
                    throw mse;
                }
            }
        }
        else {
            populateLook(look, dto);
            look.setCollection(lookCollection);
            modelService.save(look);
        }

        // remove existing products
        if (CollectionUtils.isNotEmpty(look.getProducts())) {
            modelService.removeAll(look.getProducts());
        }

        final Set<TargetLookProductModel> lookProducts = new HashSet<>();
        if (!CollectionUtils.isEmpty(dto.getProducts())) {
            for (int i = 0; i < dto.getProducts().size(); i++) {
                try {
                    lookProducts.add(createOrUpdateLookProduct(dto.getProducts().get(i), look, i));
                }
                catch (final Exception e) {
                    LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.ERR_STL_IMPORT_LOOKPRODUCT,
                            dto.getProducts().get(i).getCode(),
                            look.getId()), e);
                }
            }
        }
        look.setProducts(lookProducts);
        modelService.save(look);
    }

    /**
     * @param look
     * @param dto
     */
    private void populateLook(final TargetLookModel look, final IntegrationLookDto dto) {
        look.setName(dto.getName());
        look.setDescription(dto.getDescription());
        look.setEnabled(TargetStlImportConstants.YES.equalsIgnoreCase(dto.getEnabled()));
        look.setInstagramUrl(dto.getInstagramURL());
        try {
            if (dto.getStartDate() != null) {
                look.setStartDate(dateFormat.parseDateTime(dto.getStartDate()).toDate());
            }
        }
        catch (final IllegalArgumentException e) {
            LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.WARN_STL_IMPORT_LOOK,
                    e.getMessage() + " for startDate", dto.getCode(), dto.getName()));
        }
        try {
            if (dto.getEndDate() != null) {
                look.setEndDate(dateFormat.parseDateTime(dto.getEndDate()).toDate());
            }
        }
        catch (final IllegalArgumentException e) {
            LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.WARN_STL_IMPORT_LOOK,
                    e.getMessage() + " for endDate", dto.getCode(), dto.getName()));
        }

        look.setTags(dto.getTags());

        if (associateMedia && StringUtils.isNotBlank(dto.getImages())) {
            final MediaContainerModel mediaContainer = targetMediaImportService.getMediaContainer(dto.getImages(),
                    targetProductImportUtil.getProductCatalogStagedVersion());
            if (mediaContainer != null) {
                final MediaContainerModel onlineMediaContainer = targetMediaImportService.getOrCreateMediaContainer(
                        mediaContainer.getQualifier(), targetProductImportUtil.getProductCatalogOnlineVersion());
                final List<MediaModel> onlineMedias = new ArrayList<>();
                for (final MediaModel media : mediaContainer.getMedias()) {
                    final MediaModel onlineMedia = targetMediaImportService.getOrCreateMediaModel(media.getCode(),
                            targetProductImportUtil.getProductCatalogOnlineVersion());
                    BeanUtils.copyProperties(media, onlineMedia, MediaModel.CATALOGVERSION, MediaModel.OWNER,
                            MediaModel.CREATIONTIME, MediaModel.MODIFIEDTIME);
                    modelService.save(onlineMedia);
                    onlineMedias.add(onlineMedia);
                }
                onlineMediaContainer.setMedias(Collections.EMPTY_SET);
                targetMediaContainerService.addMediaToContainer(onlineMediaContainer, onlineMedias);
                look.setImages(onlineMediaContainer);
            }
            else {
                LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.WARN_STL_IMPORT_LOOK,
                        "staged version of mediaContainer missing", dto.getCode(), dto.getName()));
            }
        }
    }

    private void checkSizeVariantProduct(final IntegrationLookProductDto dto, final TargetLookModel look) {
        try {
            final ProductModel product = targetProductService.getProductForCode(dto.getCode());
            if (product instanceof TargetSizeVariantProductModel) {
                dto.setCode(((TargetSizeVariantProductModel)product).getBaseProduct().getCode());
            }
        }
        catch (final AmbiguousIdentifierException | UnknownIdentifierException e) {
            LOG.warn(MessageFormat.format(TargetStlImportConstants.LogMessages.WARN_STL_IMPORT_LOOKPRODUCT,
                    dto.getCode(), look.getId()), e);
        }
    }

    /**
     * @param dto
     * @param look
     * @param i
     * @return {@link TargetLookProductModel}
     */
    private TargetLookProductModel createOrUpdateLookProduct(final IntegrationLookProductDto dto,
            final TargetLookModel look, final int i) {
        checkSizeVariantProduct(dto, look);

        TargetLookProductModel lookProduct = getExistingLookProduct(dto.getCode(), look);
        if (lookProduct == null) {
            lookProduct = createNewTargetLookProduct(dto, look);
            try {
                lookProduct.setPosition(i);
                modelService.save(lookProduct);
                LOG.info(MessageFormat.format(TargetStlImportConstants.LogMessages.INFO_NEW_LOOKPRODUCT,
                        dto.getCode(), look.getId()));
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate models are getting created
                // we will only re-try to create model 1 more time
                if (mse.getMessage().toUpperCase().contains(TargetStlImportConstants.UNIQUE)) {
                    lookProduct = getExistingLookProduct(dto.getCode(), look);
                    lookProduct.setPosition(i);
                    modelService.save(lookProduct);
                }
                else {
                    throw mse;
                }
            }
        }
        else {
            lookProduct.setPosition(i);
            modelService.save(lookProduct);
        }

        return lookProduct;
    }

    /**
     * @param dto
     * @param look
     * @return {@link TargetLookProductModel}
     */
    private TargetLookProductModel createNewTargetLookProduct(final IntegrationLookProductDto dto,
            final TargetLookModel look) {
        final TargetLookProductModel lookProduct = modelService.create(TargetLookProductModel.class);
        lookProduct.setProductCode(dto.getCode());
        lookProduct.setLook(look);
        return lookProduct;
    }

    /**
     * @param code
     * @param look
     * @return {@link TargetLookProductModel}
     */
    private TargetLookProductModel getExistingLookProduct(final String code, final TargetLookModel look) {
        try {
            return lookProductService.getLookProductForCodeAndLook(code, look);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.info(e.getMessage());
        }
        return null;
    }

    /**
     * @param dto
     * @return {@link TargetLookModel}
     */
    private TargetLookModel createNewTargetLook(final IntegrationLookDto dto) {
        final TargetLookModel look = modelService.create(TargetLookModel.class);
        look.setId(dto.getCode());
        return look;
    }

    /**
     * @param code
     * @return {@link TargetLookModel}
     */
    private TargetLookModel getExistingLook(final String code) {
        try {
            return lookService.getLookForCode(code);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.info(e.getMessage());
        }
        return null;
    }

    /**
     * 
     * @param dto
     * @return New TargetLookCollectionModel populated with basic information from dto
     */
    private TargetLookCollectionModel createNewTargetLookCollection(final IntegrationLookCollectionDto dto) {
        final TargetLookCollectionModel lookCollection = modelService.create(TargetLookCollectionModel.class);
        lookCollection.setId(dto.getCode());
        return lookCollection;
    }

    /**
     * @param associateMedia
     *            the associateMedia to set
     */
    public void setAssociateMedia(final boolean associateMedia) {
        this.associateMedia = associateMedia;
    }

    /**
     * @param dateFormat
     *            the dateFormat to set
     */
    @Required
    public void setDateFormat(final DateTimeFormatter dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * @param validators
     *            the validators to set
     */
    @Required
    public void setValidators(final List<TargetShopTheLookImportValidator> validators) {
        this.validators = validators;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param shopTheLookService
     *            the shopTheLookService to set
     */
    @Required
    public void setShopTheLookService(final ShopTheLookService shopTheLookService) {
        this.shopTheLookService = shopTheLookService;
    }

    /**
     * @param lookCollectionService
     *            the lookCollectionService to set
     */
    @Required
    public void setLookCollectionService(final LookCollectionService lookCollectionService) {
        this.lookCollectionService = lookCollectionService;
    }

    /**
     * @param lookService
     *            the lookService to set
     */
    @Required
    public void setLookService(final LookService lookService) {
        this.lookService = lookService;
    }

    /**
     * @param lookProductService
     *            the lookProductService to set
     */
    @Required
    public void setLookProductService(final LookProductService lookProductService) {
        this.lookProductService = lookProductService;
    }

    /**
     * @param targetMediaImportService
     *            the targetMediaImportService to set
     */
    @Required
    public void setTargetMediaImportService(final TargetMediaImportService targetMediaImportService) {
        this.targetMediaImportService = targetMediaImportService;
    }

    /**
     * @param targetMediaContainerService
     *            the mediaContainerService to set
     */
    @Required
    public void setTargetMediaContainerService(final TargetMediaContainerService targetMediaContainerService) {
        this.targetMediaContainerService = targetMediaContainerService;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param targetProductImportUtil
     *            the targetProductImportUtil to set
     */
    @Required
    public void setTargetProductImportUtil(final TargetProductImportUtil targetProductImportUtil) {
        this.targetProductImportUtil = targetProductImportUtil;
    }

}