/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;


import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.NumberUtils;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentEntry;
import au.com.target.tgtwsfacades.instore.dto.consignments.Product;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;


/**
 * Converter to generate DTOs for portal REST API from ConsignmentModels
 * 
 * @author sbryan6
 *
 */
public class InstoreConsignmentEntryConverter implements Converter<ConsignmentEntryModel, ConsignmentEntry> {

    protected static final Logger LOG = Logger.getLogger(InstoreConsignmentEntryConverter.class);

    private String imageContext;

    @Override
    public ConsignmentEntry convert(final ConsignmentEntryModel source, final ConsignmentEntry prototype)
            throws ConversionException {

        return convert(source);
    }


    @Override
    public ConsignmentEntry convert(final ConsignmentEntryModel source) throws ConversionException {

        if (null != source) {
            final ConsignmentEntry consignmentEntry = new ConsignmentEntry();
            if (null != source.getQuantity()) {
                consignmentEntry.setQuantity(source.getQuantity().toString());
                consignmentEntry.setPickedQty(ObjectUtils.toString(source.getShippedQuantity(), StringUtils.EMPTY));
            }
            if (null != source.getOrderEntry() && null != source.getOrderEntry().getProduct()) {

                final Product productDto = populateProduct(source.getOrderEntry());
                consignmentEntry.setProduct(productDto);
            }

            return consignmentEntry;
        }
        return null;
    }

    protected Product populateProduct(final AbstractOrderEntryModel abstractOrderEntryModel) {
        final ProductModel product = abstractOrderEntryModel.getProduct();
        Product productDto = null;
        if (null != product) {
            productDto = new Product();

            //setup price
            final Double price = abstractOrderEntryModel.getBasePrice();
            if (price != null) {
                final Long priceInCents = Long.valueOf(Math.round(price.doubleValue() * 100));
                productDto.setPrice(NumberUtils.convertNumberToTargetClass(priceInCents, Integer.class));
            }
            else {
                productDto.setPrice(Integer.valueOf(0));
                final AbstractOrderModel order = abstractOrderEntryModel.getOrder();
                String code = StringUtils.EMPTY;
                if (order != null) {
                    code = order.getCode();
                }
                LOG.warn("OFC-CONSIGNMENTCONVERTER : No price set on the order : " + code);
            }

            //setup product information
            if (product instanceof TargetColourVariantProductModel) {
                populateProductForColourVariation(product, productDto);
            }
            if (product instanceof TargetSizeVariantProductModel) {
                populateProductForSizeVariation(product, productDto);
            }
            populateBaseProductAttributes(product, productDto);

        }

        return productDto;

    }


    /**
     * Populates the following values from the base product:
     * <ul>
     * <li>ClearanceProduct</li>
     * <li>Bulky</li>
     * <li>MHD</li>
     * </ul>
     * 
     * @param product
     * @param productDto
     */
    protected void populateBaseProductAttributes(final ProductModel product, final Product productDto) {
        final ProductModel baseProduct = TargetProductDataHelper.getBaseProduct(product);
        if (baseProduct instanceof TargetProductModel) {
            final TargetProductModel productModel = (TargetProductModel)baseProduct;
            if (null != productModel.getClearanceProduct()) {
                productDto.setClearance(productModel.getClearanceProduct());
            }

            final ProductTypeModel productType = productModel.getProductType();
            if (productType != null) {
                if (productType.getBulky() != null) {
                    productDto.setBulky(productType.getBulky());
                }

                productDto.setMhd(Boolean.valueOf(TgtwebcoreConstants.ProductTypes.MHD.equals(productType.getCode())));
            }
        }
    }

    protected void populateProductForSizeVariation(final ProductModel product, final Product productDto) {

        final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)product;
        if (null != product) {
            populateCommonDetails(product, productDto);

            productDto.setSize(sizeVariant.getSize());

            if (null != sizeVariant.getMerchDepartment()) {
                productDto.setDepartmentNumber(sizeVariant.getMerchDepartment().getCode());
                productDto.setDepartment(sizeVariant.getMerchDepartment().getName());
            }



            //Get the media details from the colour variant.

            if (null != sizeVariant.getBaseProduct()) {

                final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)sizeVariant
                        .getBaseProduct();
                if (colourVariant != null) {
                    if (CollectionUtils.isNotEmpty(colourVariant.getGalleryImages())) {
                        final MediaContainerModel mediaCon = colourVariant.getGalleryImages().get(0);
                        if (null != mediaCon) {
                            populateImageDetails(colourVariant, productDto);
                        }
                    }
                    // populate colour details
                    populateColourDetails(colourVariant, productDto);

                }
            }
        }


    }

    protected void populateProductForColourVariation(final ProductModel product, final Product productDto) {

        final TargetColourVariantProductModel colVariant = (TargetColourVariantProductModel)product;

        if (null != product) {

            populateCommonDetails(product, productDto);

            populateColourDetails(colVariant, productDto);

            if (null != colVariant.getMerchDepartment()) {
                productDto.setDepartmentNumber(colVariant.getMerchDepartment().getCode());

                productDto.setDepartment(colVariant.getMerchDepartment().getName());
            }

            populateImageDetails(colVariant, productDto);
        }

    }

    private void populateImageDetails(final TargetColourVariantProductModel colVariant, final Product productDto) {
        if (CollectionUtils.isNotEmpty(colVariant.getGalleryImages())) {
            final MediaContainerModel mediaCon = colVariant.getGalleryImages().get(0);
            if (null != mediaCon) {
                final Collection<MediaModel> mediaModels = mediaCon.getMedias();
                if (CollectionUtils.isNotEmpty(mediaModels)) {
                    for (final MediaModel mediaModel : mediaModels) {
                        if (null != mediaModel && null != mediaModel.getURL()) {
                            if (mediaModel.getCode().startsWith(TargetMediaImportService.THUMB_MEDIA)) {
                                productDto.setThumbImageUrl(imageContext + mediaModel.getURL());
                            }

                            if (mediaModel.getCode().startsWith(TargetMediaImportService.LARGE_MEDIA)) {
                                productDto.setLargeImageUrl(imageContext + mediaModel.getURL());
                            }
                        }
                    }
                }

            }
        }


    }


    /**
     * @param colVariant
     *            Colour Variant
     * @param productDto
     *            product Dto
     */
    protected void populateColourDetails(final TargetColourVariantProductModel colVariant, final Product productDto) {

        if (null != colVariant) {
            if (StringUtils.isNotEmpty(colVariant.getSwatchName())
                    && null != colVariant.getSwatch()
                    && colVariant.getSwatch().getDisplay().booleanValue()) {
                productDto.setColour(colVariant.getSwatchName());
            }
            else if (StringUtils.isNotEmpty(colVariant.getColourName()) && null != colVariant.getColour()
                    && colVariant.getColour().getDisplay().booleanValue()) {
                productDto.setColour(colVariant.getColourName());
            }

            if (colVariant.getOnlineExclusive() != null) {
                productDto.setOnlineExclusive(colVariant.getOnlineExclusive());
            }
        }

    }


    private void populateCommonDetails(final ProductModel product, final Product productDto) {

        productDto.setCode(product.getCode());

        productDto.setName(product.getName());

        productDto.setBarcode(product.getEan());

    }


    /**
     * @param imageContext
     *            the imageContext to set
     */
    @Required
    public void setImageContext(final String imageContext) {
        this.imageContext = imageContext;
    }

}