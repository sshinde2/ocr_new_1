/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Customer object/
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Customer {

    private String firstName;
    private String lastName;


    /**
     * Constructor
     */
    public Customer() {

    }


    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }


    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

}
