/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcel;


/**
 * Convert parcel data to parcel information for a consignment.
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentParcelDTOConverter implements Converter<Parcel, ConsignmentParcelDTO> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public ConsignmentParcelDTO convert(final Parcel source) throws ConversionException {
        final ConsignmentParcelDTO consignmentParcel = new ConsignmentParcelDTO();
        return convert(source, consignmentParcel);
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object, java.lang.Object)
     */
    @Override
    public ConsignmentParcelDTO convert(final Parcel source, final ConsignmentParcelDTO target)
            throws ConversionException {
        try {
            Assert.notNull(source, "Parcel cannot be null");
            validateFields(source);
            target.setHeight(source.getHeight().toString());
            target.setLength(source.getLength().toString());
            target.setWidth(source.getWidth().toString());
            target.setWeight(source.getWeight().toString());
        }
        catch (final IllegalArgumentException e) {
            throw new ConversionException(e.getMessage());
        }

        return target;
    }

    private void validateFields(final Parcel parcel) {
        Assert.notNull(parcel.getHeight(), "Height of parcel is a mandatory attribute");
        Assert.notNull(parcel.getLength(), "Length of parcel is a mandatory attribute");
        Assert.notNull(parcel.getWidth(), "Width of parcel is a mandatory attribute");
        Assert.notNull(parcel.getWeight(), "Weight of parcel is a mandatory attribute");
    }
}
