/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.response.data;

import java.util.List;

import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;


/**
 * @author Nandini
 *
 */
public class ProductSohResponseData extends BaseResponseData {
    private List<ProductStockLevelInstore> products;

    /**
     * @return the products
     */
    public List<ProductStockLevelInstore> getProducts() {
        return products;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(final List<ProductStockLevelInstore> products) {
        this.products = products;
    }



}
