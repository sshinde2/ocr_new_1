/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;


/**
 * Converter to convert basic info from {@link TargetManifestModel} to {@link Manifest}.
 * 
 * @author Rahul
 *
 */
public class InstoreBasicManifestConverter implements Converter<TargetManifestModel, Manifest> {

    @Override
    public Manifest convert(final TargetManifestModel source, final Manifest prototype)
            throws ConversionException {
        return convert(source);
    }

    @Override
    public Manifest convert(final TargetManifestModel source) throws ConversionException {

        Assert.notNull(source, "TargetManifestModel cannot be null");

        final Manifest manifest = new Manifest();

        manifest.setCode(source.getCode());
        manifest.setDate(TargetDateUtil.getDateFormattedAsString(source.getDate()));
        manifest.setSent(source.isSent());

        return manifest;
    }

}
