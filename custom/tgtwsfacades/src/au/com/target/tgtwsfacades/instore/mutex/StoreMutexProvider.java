/**
 * 
 */
package au.com.target.tgtwsfacades.instore.mutex;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * Class to provide store level mutex for instore fulfilment actions
 * 
 * @author sbryan6
 *
 */
public class StoreMutexProvider {

    private final ConcurrentMap<Integer, Integer> storeMutexes = new ConcurrentHashMap<Integer, Integer>();

    /**
     * Return mutex for store.
     * 
     * @param storeNumber
     * @return mutex
     */
    public synchronized Object getMutex(final Integer storeNumber) {

        final Integer storeMutex = storeMutexes.get(storeNumber);
        if (storeMutex != null) {
            return storeMutex;
        }

        // Clone the storeNumber to avoid multiple references
        final Integer newMutex = new Integer(storeNumber.intValue());
        storeMutexes.put(newMutex, newMutex);
        return newMutex;
    }

}
