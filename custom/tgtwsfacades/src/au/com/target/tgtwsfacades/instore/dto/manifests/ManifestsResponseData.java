/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.manifests;

import java.util.List;

import au.com.target.tgtfacades.response.data.BaseResponseData;


/**
 * @author Vivek
 *
 */
public class ManifestsResponseData extends BaseResponseData {
    private List<Manifest> manifests;

    /**
     * @return the manifests
     */
    public List<Manifest> getManifests() {
        return manifests;
    }

    /**
     * @param manifests
     *            the manifests to set
     */
    public void setManifests(final List<Manifest> manifests) {
        this.manifests = manifests;
    }
}
