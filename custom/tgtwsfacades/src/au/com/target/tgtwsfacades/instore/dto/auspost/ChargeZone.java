/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.auspost;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author smishra1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ChargeZone {
    private String code;
    private String description;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }


}
