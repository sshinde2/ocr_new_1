/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;


/**
 * @author smudumba
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Consignment {

    private String code;
    private String orderNumber;
    private Long totalItems;
    private String date;
    private String status;
    private Customer customer;
    private List<ConsignmentEntry> entries;
    private String orderBarcodeNumber;
    private String orderBarcodeUrl;
    private Integer parcels;
    private Destination destination;
    private String pickDate;
    private String packDate;
    private String deliveryType;
    private Integer consignmentValueInCents;
    private Integer consignmentVersion;
    private boolean clearance;
    private boolean onlineExclusive;
    private boolean bulky;
    private boolean mhd;
    private ConsignmentRejectState rejectState;
    private String orderCreationDate;

    /**
     * Response object for consignment
     * 
     */
    public Consignment() {

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the totalItems
     */
    public Long getTotalItems() {
        return totalItems;
    }

    /**
     * @param totalItems
     *            the totalItems to set
     */
    public void setTotalItems(final Long totalItems) {
        this.totalItems = totalItems;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(final String date) {
        this.date = date;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    /**
     * @return the entries
     */
    public List<ConsignmentEntry> getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final List<ConsignmentEntry> entries) {
        this.entries = entries;
    }

    /**
     * @return the orderBarcodeNumber
     */
    public String getOrderBarcodeNumber() {
        return orderBarcodeNumber;
    }

    /**
     * @param orderBarcodeNumber
     *            the orderBarcodeNumber to set
     */
    public void setOrderBarcodeNumber(final String orderBarcodeNumber) {
        this.orderBarcodeNumber = orderBarcodeNumber;
    }

    /**
     * @return the orderBarcodeUrl
     */
    public String getOrderBarcodeUrl() {
        return orderBarcodeUrl;
    }

    /**
     * @param orderBarcodeUrl
     *            the orderBarcodeUrl to set
     */
    public void setOrderBarcodeUrl(final String orderBarcodeUrl) {
        this.orderBarcodeUrl = orderBarcodeUrl;
    }

    /**
     * @return the parcels
     */
    public Integer getParcels() {
        return parcels;
    }

    /**
     * @param parcels
     *            the parcels to set
     */
    public void setParcels(final Integer parcels) {
        this.parcels = parcels;
    }

    /**
     * @return the destination
     */
    public Destination getDestination() {
        return destination;
    }

    /**
     * @param destination
     *            the destination to set
     */
    public void setDestination(final Destination destination) {
        this.destination = destination;
    }

    /**
     * @return the pickDate
     */
    public String getPickDate() {
        return pickDate;
    }

    /**
     * @param pickDate
     *            the pickDate to set
     */
    public void setPickDate(final String pickDate) {
        this.pickDate = pickDate;
    }

    /**
     * @return the deliveryType
     */
    public String getDeliveryType() {
        return deliveryType;
    }

    /**
     * @param deliveryType
     *            the deliveryType to set
     */
    public void setDeliveryType(final String deliveryType) {
        this.deliveryType = deliveryType;
    }

    /**
     * @return the packDate
     */
    public String getPackDate() {
        return packDate;
    }

    /**
     * @param packDate
     *            the packDate to set
     */
    public void setPackDate(final String packDate) {
        this.packDate = packDate;
    }

    /**
     * @return the consignmentValueInCents
     */
    public Integer getConsignmentValueInCents() {
        return consignmentValueInCents;
    }

    /**
     * @param consignmentValueInCents
     *            the consignmentValueInCents to set
     */
    public void setConsignmentValueInCents(final Integer consignmentValueInCents) {
        this.consignmentValueInCents = consignmentValueInCents;
    }

    /**
     * @return the consignmentVersion
     */
    public Integer getConsignmentVersion() {
        return consignmentVersion;
    }

    /**
     * @param consignmentVersion
     *            the consignmentVersion to set
     */
    public void setConsignmentVersion(final Integer consignmentVersion) {
        this.consignmentVersion = consignmentVersion;
    }

    public boolean isClearance() {
        return clearance;
    }

    /**
     * @param clearance
     *            the clearance to set
     */
    public void setClearance(final boolean clearance) {
        this.clearance = clearance;
    }

    /**
     * @return the onlineExclusive
     */
    public boolean isOnlineExclusive() {
        return onlineExclusive;
    }

    /**
     * @param onlineExclusive
     *            the onlineExclusive to set
     */
    public void setOnlineExclusive(final boolean onlineExclusive) {
        this.onlineExclusive = onlineExclusive;
    }

    /**
     * @return the bulky
     */
    public boolean isBulky() {
        return bulky;
    }

    /**
     * @param bulky
     *            the bulky to set
     */
    public void setBulky(final boolean bulky) {
        this.bulky = bulky;
    }

    /**
     * @return the mhd
     */
    public boolean isMhd() {
        return mhd;
    }

    /**
     * @param mhd
     *            the mhd to set
     */
    public void setMhd(final boolean mhd) {
        this.mhd = mhd;
    }

    /**
     * @return the rejectState
     */
    public ConsignmentRejectState getRejectState() {
        return rejectState;
    }

    /**
     * @param rejectState
     *            the rejectState to set
     */
    public void setRejectState(final ConsignmentRejectState rejectState) {
        this.rejectState = rejectState;
    }

    /**
     * @return the orderCreationDate
     */
    public String getOrderCreationDate() {
        return orderCreationDate;
    }

    /**
     * @param orderCreationDate
     *            the orderCreationDate to set
     */
    public void setOrderCreationDate(final String orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

}
