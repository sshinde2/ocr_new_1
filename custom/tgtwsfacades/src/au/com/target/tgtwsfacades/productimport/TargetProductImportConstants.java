/**
 * 
 */
package au.com.target.tgtwsfacades.productimport;

/**
 * @author rsamuel3
 *
 */
public interface TargetProductImportConstants {

    public static final String VARIANT_CODE_SEPERATOR = "_";

    public static final String PRODUCT_INACTIVE = "inactive";
    public static final String PRODUCT_ACTIVE = "active";
    public static final String NOT_SOURCED = "NOT SOURCED";
    public static final String TARGET_COLOUR_VARIANT_PRODUCT_TYPE = "TargetColourVariantProduct";
    public static final String PRODUCT_TYPE_NORMAL = "normal";
    public static final String PRODUCT_TYPE_DIGITAL = "digital";
    public static final String PRODUCT_TYPE_PHYSICAL_GIFTCARD = "giftCard";
    public static final String NO_COLOUR = "No Colour";
    public static final String UNIQUE = "UNIQUE";
    public static final String TARGET_SIZE_VARIANT_PRODUCT_TYPE = "TargetSizeVariantProduct";
    public static final String VIDEO = "/video/";
    public static final String PRODUCT_PKG_DIMENSION_KEY = "PKG_";
    public static final String ARTICLE_STATUS_ONLINEEXCLUSIVE = "onlineexclusive";
    public static final String ARTICLE_STATUS_CLEARANCE = "clearance";
    public static final String ARTICLE_STATUS_HOTPRODUCT = "hotproduct";
    public static final String ARTICLE_STATUS_TARGETEXCLUSIVE = "targetexclusive";
    public static final String ARTICLE_STATUS_ESSENTIALS = "essentials";

    public static final String FEATURE_LICENSE = "license";
    public static final String FEATURE_GENDER = "gender";
    public static final String FEATURE_AGE_RANGE = "ageRange";
    public static final String FEATURE_EXPRESS_BY_FLAG = "productimport.expressbyflag";
    public static final String FEATURE_MOVE_BETWEEN_STYLEGROUPS = "moveBetweenStylegroups";
    public static final String BRAND_CODE_APPLE = "apple";
    public static final String FEATURE_FLUENT = "fluent";
    public static final String WAREHOUSE_INCOMM = "IncommWarehouse";

    public interface LogMessages {
        public static final String ERR_PREIMPORT_VALIDATION = "ERR-PRODUCTIMPORT-VALIDATION: Pre-import product validation failed for product with product code: {0}, name: {1}.  Reason(s): {2}";

        public static final String INFO_PREIMPORT_VALIDATION = "INFO-PRODUCTIMPORT: Pre-import product validation successful for product with product code: {0}, name: {1}";

        public static final String INFO_NEW_BASE_PRODUCT = "INFO-NEW-BASEPRODUCT: Successfully created new base product with code: {0}, name: {1}";

        public static final String INFO_EXISTING_BASE_PRODUCT = "INFO-EXISTING-BASEPRODUCT: Successfully updated existing base product with code: {0}, name: {1}";

        public static final String ERR_COLOUR_VARIATION = "ERR-PRODUCTIMPORT-COLOURVARIANT: Error creating/updating colour variant {2}, for base product with code: {0}, name: {1}";

        public static final String INFO_NEW_COLOUR_VARIATION = "INFO-NEW-COLOURVARIANT: Successfully created new colour variant {2}, for base product with code: {0}, name: {1}";

        public static final String INFO_EXISTING_COLOUR_VARIATION = "INFO-EXISTING-COLOURVARIANT: Successfully updated existing colour variant {2}, for base product with code: {0}, name: {1}";

        public static final String ERR_SIZE_VARIATION = "ERR-PRODUCTIMPORT-SIZEVARIANT: Error creating/updating size variant {3}, for base product with code: {0}, name: {1}, size: {2}";

        public static final String INFO_NEW_SIZE_VARIATION = "INFO-NEW-SIZEVARIANT: Successfully created new size variant {3}, for base product with code: {0}, name: {1}, size: {2}";

        public static final String INFO_EXISTING_SIZE_VARIATION = "INFO-EXISTING-SIZEVARIANT: Successfully updated existing size variant {3}, for base product with code: {0}, name: {1}, size: {2}";

        public static final String ERR_PRODUCTIMPORT_SOME_MEDIAMISSING = "ERR-PRODUCTIMPORT-MEDIAMISSING : There are a couple of media not available on hybris for product with ID {0}";

        public static final String ERR_PRODUCTIMPORT_MEDIAMISSING = "ERR-PRODUCTIMPORT-MEDIAMISSING : There was no media found on hybris for product with ID {0}";

        public static final String INFO_DIMENSION_INVALID_EVENT = "INFO-DIMENSION-INVALID-EVENT: For product with productCode={0}, baseProductCode={1}, reason=WeightOrDimensionIsNullOrNegative";

        public static final String INFO_DIGITIAL_PRODUCT_INVALID_EVENT = "INFO_DIGITIAL_PRODUCT_INVALID_EVENT: for product with productCode={0}, baseProduct={1}, reason=InvalidDimensionOrDigitalDeliveryNotAvailble";

        public static final String INFO_ERR_SV_CREATE_UPDATE = "ERR_SV_CREATE_UPDATE: The size variant product SVProductCode={0},CVProductCode={1} can't be created/updated because its color variant has price";

        public static final String ERR_PRODUCTIMPORT_EXCEPTION = "ERR-PRODUCTIMPORT-EXCEPTION: Unable to create/update product and/or product variant: {0}.";

        public static final String EAN_VALIDATION_FAILURE = "EAN validation failed for EAN: {2} for product with code: {0}, name: {1} hence EAN has been set to empty.";

        public static final String BULKY_PRODUCT_WITH_EXPRESS_DELIVERY_FAILURE_MESSAGE = "It is a bulky product with express-delivery enabled, aborting product/variant import for: {0}";

        public static final String INFO_WAREHOUSE_MISMATCH = "WAREHOUSE_MISMATCH: Mismatch in product warehouse information, productCode={0}, baseProductCode={1}, stockExistsInWarehouse={2}, newExpectedWarehouse={3}";

        public static final String ERR_PRODUCTIMPORT_BASE_PRODUCT = "ERR-PRODUCTIMPORT-BASE_PRODUCT: Base product feed to fluent failed for product with product code={0}, name={1}.";

        public static final String ERR_PRODUCTIMPORT_VARIANT_PRODUCT = "ERR-PRODUCTIMPORT-VARIANT_PRODUCT: Variant product feed to fluent failed for variant with code={0}";

    }
}
