/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author rsamuel3
 *
 */
public class TargetImportBaseProductValidator implements TargetProductImportValidator {

    private ProductTypeService productTypeService;


    private TargetProductImportUtil targetProductImportUtil;

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {

        final List<String> errorMessages = new ArrayList<>();
        final String variantCode = dto.getVariantCode();

        // product code
        if (StringUtils.isBlank(dto.getProductCode())) {
            errorMessages.add(MessageFormat.format(
                    "Missing 'PRODUCT CODE' (236245), aborting product/variant import for: {0}",
                    dto.getName()));
        }

        // brand
        if (StringUtils.isBlank(dto.getBrand())) {
            errorMessages.add(MessageFormat.format(
                    "Missing product/variant 'BRAND' (49135), aborting product/variant import for: {0}",
                    variantCode));
        }

        // product approval status
        if (StringUtils.isBlank(dto.getApprovalStatus())) {
            errorMessages.add(MessageFormat.format(
                    "Missing product/variant 'APPROVAL STATUS' (94267), aborting product/variant import for: {0}",
                    variantCode));
        }

        // available for layby
        if (StringUtils.isBlank(dto.getAvailableLayby())) {
            errorMessages
                    .add(MessageFormat
                            .format(
                                    "Missing 'AVAILABLE FOR LAYBY' flag (138956 - Toysale 7), aborting product/variant import for: {0}",
                                    variantCode));
        }

        // available for long-term layby
        if (StringUtils.isBlank(dto.getAvailableLongtermLayby())) {
            errorMessages
                    .add(MessageFormat
                            .format(
                                    "Missing 'AVAILABLE FOR LONGTERM LAYBY' flag (Attr_311962), aborting product/variant import for: {0}",
                                    variantCode));
        }

        // product Type
        if (StringUtils.isBlank(dto.getProductType())) {
            dto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_NORMAL);
        }

        // bulky product / correct product type
        ProductTypeModel typeByCode = null;
        try {
            typeByCode = productTypeService.getByCode(dto.getProductType());
        }
        catch (final ModelNotFoundException e) {
            errorMessages.add(MessageFormat.format(
                    "ProductType specified is not defined in hybris, aborting product/variant import for: {0}",
                    variantCode));
        }

        final String bulky = StringUtils.isNotBlank(dto.getBulky()) ? dto.getBulky() : StringUtils.EMPTY;

        if (bulky.equalsIgnoreCase("Y") && !targetProductImportUtil.isBulky(typeByCode)) {
            errorMessages.add(MessageFormat.format(
                    "Bulky product with incorrect product type, aborting product/variant import for: {0}",
                    variantCode));
        }
        else if (!bulky.equalsIgnoreCase("Y") && targetProductImportUtil.isBulky(typeByCode)) {
            errorMessages
                    .add(MessageFormat
                            .format(
                                    "Bulky product type set for a bulky flag that is not Y, aborting product/variant import for: {0}",
                                    variantCode));
        }

        // available for CNC
        if (StringUtils.isBlank(dto.getAvailableCnc())) {
            //            response.addMessage(MessageFormat.format(
            //                    "Missing 'AVAILABLE FOR CNC' flag (139044 - Toysale 6), aborting product/variant import for: {0}",
            //                    variationCode));
            dto.setAvailableCnc("Y");
        }

        // available for MHD
        if (StringUtils.isBlank(dto.getAvailableHomeDelivery())) {
            errorMessages.add(MessageFormat.format(
                    "Missing 'AVAILABLE FOR HOME DELIVERY' flag (279381), aborting product/variant import for: {0}",
                    variantCode));
        }

        // if available for ED, must be normal product
        if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableExpressDelivery()))
                && targetProductImportUtil.isBulky(typeByCode)) {
            errorMessages
                    .add(MessageFormat
                            .format(TargetProductImportConstants.LogMessages.BULKY_PRODUCT_WITH_EXPRESS_DELIVERY_FAILURE_MESSAGE,
                                    variantCode));
        }

        validateGiftcardProducts(dto, errorMessages, variantCode);

        return errorMessages;

    }

    /**
     * @param dto
     * @param errorMessages
     * @param variantCode
     */
    private void validateGiftcardProducts(final IntegrationProductDto dto,
            final List<String> errorMessages,
            final String variantCode) {
        //if digital gift card or physical gift card
        if (TargetProductImportConstants.PRODUCT_TYPE_DIGITAL.equals(dto.getProductType())
                || targetProductImportUtil.isProductPhysicalGiftcard(dto)) {

            //must have proper brand id
            if (StringUtils.isBlank(dto.getGiftcardBrandId())) {
                errorMessages.add(MessageFormat.format(
                        "Missing 'Brand ID' for gift card product, aborting product/variant import for: {0}",
                        variantCode));
            }

            //must have proper denomination
            if (dto.getDenominaton() == null) {
                errorMessages
                        .add(MessageFormat
                                .format(
                                        "Missing 'Denomination' for gift card product, aborting product / variant import for: {0}",
                                        variantCode));
            }
        }
    }

    /**
     * @return the targetProductImportUtil
     */

    /**
     * @param targetProductImportUtil
     *            the targetProductImportUtil to set
     */
    @Required
    public void setTargetProductImportUtil(final TargetProductImportUtil targetProductImportUtil) {
        this.targetProductImportUtil = targetProductImportUtil;
    }

    /**
     * @param productTypeService
     *            the productTypeService to set
     */
    @Required
    public void setProductTypeService(final ProductTypeService productTypeService) {
        this.productTypeService = productTypeService;
    }


}
