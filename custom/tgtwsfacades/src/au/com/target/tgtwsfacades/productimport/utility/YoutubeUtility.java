/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.utility;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author fkratoch
 * 
 */
public final class YoutubeUtility {
    private static final Logger LOG = Logger.getLogger(YoutubeUtility.class);

    /**
     * Regex pattern to match full YouTube URLs. Valid URL types are of the format
     * 'http://www.youtube.com/watch?v=<videoCode>'.<br />
     * 
     * URLs are considered valid with or without the following: 'http://', 'www.', country modifiers (.com.au, .co.uk,
     * etc), additional query string parameters
     */
    private static final Pattern VALID_FULL_YOUTUBE_URL_PATTERN = Pattern.compile(".*youtube.co.*/watch\\?v=.*");

    private static final Pattern VALID_FULL_EMBED_URL_PATTERN = Pattern.compile(".*youtube.co.*/v/.*");

    /**
     * Regex pattern to match short YouTube URLs. Valid URL types are of the format: youtu.be/<videoCode>.<br />
     * 
     * URLs are considered valid with or without the following: 'http://', 'www.'.
     */
    private static final Pattern VALID_SHORT_YOUTUBE_URL_PATTERN = Pattern.compile(".*youtu.be/.*");

    private static final Pattern VALID_EMBED_YOUTUBE_URL_PATTERN = Pattern.compile(".*youtube.co.*/embed/.*");

    private static final Pattern VALID_VERY_SHORT_YOUTUBE_URL_PATTERN = Pattern.compile(".*y2u.be/.*");


    private YoutubeUtility() {
        // avoid construction
    }

    public static String getVideoId(final String youtubeUrl) {
        if (StringUtils.isBlank(youtubeUrl)) {
            return "";
        }
        String videoId = null;

        if (VALID_FULL_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfter(youtubeUrl, "watch?v=");
            videoId = StringUtils.substringBefore(videoId, "&");
        }
        else if (VALID_SHORT_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()
                || VALID_VERY_SHORT_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
        }
        else if (VALID_FULL_EMBED_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfter(youtubeUrl, "/v/");
            videoId = StringUtils.substringBefore(videoId, "?");
        }
        else if (VALID_EMBED_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            if (!youtubeUrl.contains("?rel=0")) {
                videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
            }
            else {
                videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
                videoId = StringUtils.substringBefore(videoId, "?");
            }

        }
        else {
            LOG.info("Provided YouTube URL ('" + youtubeUrl + "') does not appear to be valid.");
            return "";
        }

        if (StringUtils.isBlank(videoId)) {
            LOG.info("Could not extract video code from provided YouTube URL: " + youtubeUrl + ". Check the format.");
            return "";
        }

        return videoId;
    }
}
