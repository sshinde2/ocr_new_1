/**
 * 
 */
package au.com.target.tgtwsfacades.consignment.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.consignment.ConsignmentIntegrationFacade;
import au.com.target.tgtwsfacades.integration.dto.Consignment;
import au.com.target.tgtwsfacades.integration.dto.Consignments;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author pthoma20
 * 
 */
public class ConsignmentIntegrationFacadeImpl implements ConsignmentIntegrationFacade {

    private static final Logger LOG = Logger.getLogger(ConsignmentIntegrationFacadeImpl.class);

    private static final String CONSIGNMENT_CANCEL = "CONSIGNMENT-CANCEL : When cancelling consignment={0} an error occured with the message={1}";

    private static final String CONSIGNMENT_COMPLETE = "CONSIGNMENT-COMPLETE : When completing consignment={0} an error occured with the message={1}";

    private static final String CONSIGNMENT_SHIP = "CONSIGNMENT-SHIP : When shipping consignment={0} an error occured with the message={1}";

    private TargetFulfilmentService targetFulfilmentService;

    @Override
    public List<IntegrationResponseDto> cancelConsignments(final Consignments consignments) {
        final List<IntegrationResponseDto> responses = new ArrayList<>();
        for (final Consignment consignment : consignments.getConsignments()) {
            final String consignmentId = consignment.getConsignmentId();
            final IntegrationResponseDto response = new IntegrationResponseDto(consignmentId);
            try {

                targetFulfilmentService.processCancelForConsignment(consignment.getConsignmentId(),
                        LoggingContext.WAREHOUSE);
                response.setSuccessStatus(true);
            }
            catch (final Exception e) {
                response.setSuccessStatus(false);
                final String message = MessageFormat.format(CONSIGNMENT_CANCEL, consignmentId, e.getMessage());
                LOG.error(message, e);
                response.addMessage(message);
            }
            responses.add(response);
        }
        return responses;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.consignment.ConsignmentIntegrationFacade#completeConsignments(au.com.target.tgtwsfacades.integration.dto.Consignments)
     */
    @Override
    public List<IntegrationResponseDto> completeConsignments(final Consignments consignments) {
        final List<IntegrationResponseDto> responses = new ArrayList<>();
        for (final Consignment consignment : consignments.getConsignments()) {
            final String consignmentId = consignment.getConsignmentId();
            final IntegrationResponseDto response = new IntegrationResponseDto(consignmentId);
            try {
                Assert.notNull(consignmentId, "Consignment id is null");
                Assert.notNull(consignment.getCarrier(), "Consignment carrier is null");
                Assert.notNull(consignment.getParcelCount(), "Consignment parcel count is null");
                Assert.notNull(consignment.getShipDate(), "Consignment ship date is null");

                if (consignment.getConsignmentEntries() != null) {
                    LOG.warn(MessageFormat.format(
                            CONSIGNMENT_COMPLETE, consignmentId,
                            "consignment entries present for complete consignmnet which will be ignored."));
                }
                targetFulfilmentService.processCompleteConsignment(consignmentId, consignment.getShipDate(),
                        consignment.getCarrier(),
                        consignment.getTrackingNumber(),
                        consignment.getParcelCount(), LoggingContext.WAREHOUSE);
                response.setSuccessStatus(true);
            }
            catch (final Exception e) {
                response.setSuccessStatus(false);
                final String message = MessageFormat.format(CONSIGNMENT_COMPLETE, consignmentId, e.getMessage());
                LOG.error(message, e);
                response.addMessage(message);
            }
            responses.add(response);
        }
        return responses;
    }

    @Override
    public List<IntegrationResponseDto> shipConsignments(final Consignments consignments) {

        final List<IntegrationResponseDto> responses = new ArrayList<>();
        for (final Consignment consignment : consignments.getConsignments()) {

            final String consignmentId = consignment.getConsignmentId();
            final String trackingNumber = consignment.getTrackingNumber();
            final IntegrationResponseDto response = new IntegrationResponseDto(consignmentId);
            try {
                targetFulfilmentService.processShipConsignment(consignmentId, trackingNumber, LoggingContext.WAREHOUSE);
                response.setSuccessStatus(true);
            }
            catch (final FulfilmentException e) {
                response.setSuccessStatus(false);
                final String message = MessageFormat.format(CONSIGNMENT_SHIP, consignmentId, e.getMessage());
                LOG.error(message, e);
                response.addMessage(message);
            }
            responses.add(response);
        }
        return responses;
    }

    /**
     * @return the targetFulfilmentService
     */
    public TargetFulfilmentService getTargetFulfilmentService() {
        return targetFulfilmentService;
    }

    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

}
