/**
 * 
 */
package au.com.target.tgtwsfacades.valuepack;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackDto;



/**
 * @author mmaki
 * 
 */
public interface ValuePackIntegrationFacade {


    IntegrationResponseDto updateValuePack(final IntegrationValuePackDto valuePackDto);
}
