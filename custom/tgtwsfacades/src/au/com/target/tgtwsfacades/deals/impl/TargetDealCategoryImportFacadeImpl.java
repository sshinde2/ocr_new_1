package au.com.target.tgtwsfacades.deals.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants.Catalog;
import au.com.target.tgtcore.constants.TgtCoreConstants.Seperators;
import au.com.target.tgtcore.model.MissingDealProductInfoModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtwsfacades.deals.TargetDealCategoryImportFacade;
import au.com.target.tgtwsfacades.deals.dto.DealProductDTO;
import au.com.target.tgtwsfacades.deals.dto.TargetDealCategoryDTO;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPrincipalDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * Default implementation of {@link TargetDealCategoryImportFacade}
 */
public class TargetDealCategoryImportFacadeImpl implements TargetDealCategoryImportFacade {

    private static final Logger LOG = Logger.getLogger(TargetDealCategoryImportFacadeImpl.class);

    private static final String PRODUCT_ASSIGNMENT_ERROR = "However, the following products could not be assigned to this category as they do not exist";

    private static final String UNIQUE = "UNIQUE";

    private ModelService modelService;

    private CategoryService categoryService;

    private CatalogVersionService catalogVersionService;

    private UserService userService;

    private ProductService productService;

    private CatalogVersionModel productCatalogOfflineVersion;

    @Override
    public IntegrationResponseDto persistTargetDealCategory(final TargetDealCategoryDTO categoryDTO) {

        final String categoryCode = categoryDTO.getCode();
        final String dealId = categoryDTO.getDealId();

        final IntegrationResponseDto response = new IntegrationResponseDto(categoryCode);
        response.setSuccessStatus(true);

        CategoryModel dealCategory = null;
        try {
            dealCategory = categoryService.getCategoryForCode(categoryCode);
            if (!(dealCategory instanceof TargetDealCategoryModel)) {
                response.setSuccessStatus(false);
                response.addMessage("Category " + categoryCode + " is not of type 'TargetDealCategory'");
                return response;
            }
        }
        catch (final UnknownIdentifierException uie) {
            LOG.warn("Creating Deal Category " + categoryCode + " as it does not already exist.");
            dealCategory = createDealCategory(categoryDTO);
        }
        final List<String> unknownProductCodes = assignProductsToDealCategory(categoryDTO, dealCategory);
        response.addMessage("Import of category " + categoryCode + " is successful");

        if (CollectionUtils.isNotEmpty(unknownProductCodes)) {

            persistMissingProducts(dealId, categoryCode, unknownProductCodes);

            response.addMessage(PRODUCT_ASSIGNMENT_ERROR);
            response.addMessage(StringUtils.join(unknownProductCodes, Seperators.COMMA));
        }
        return response;
    }

    /**
     * @param dealId
     * @param categoryCode
     * @param unknownProductCodes
     */
    private void persistMissingProducts(final String dealId, final String categoryCode,
            final List<String> unknownProductCodes) {
        final List<MissingDealProductInfoModel> missingProducts = new ArrayList<>();

        for (final String productCode : unknownProductCodes) {
            final MissingDealProductInfoModel missingProductInfo = modelService
                    .create(MissingDealProductInfoModel.class);
            missingProductInfo.setDealId(dealId);
            missingProductInfo.setProductCode(productCode);
            missingProductInfo.setDealCategoryCode(categoryCode);
            missingProducts.add(missingProductInfo);
        }

        modelService.saveAll(missingProducts);
    }

    private List<String> assignProductsToDealCategory(final TargetDealCategoryDTO categoryDTO,
            final CategoryModel dealCategory) {
        final List<ProductModel> productsToSave = new ArrayList<>();
        final List<DealProductDTO> dealProductDTOs = categoryDTO.getDealProducts();
        final List<String> unknownProductCodes = new ArrayList<>();
        for (final DealProductDTO dealProductDTO : dealProductDTOs) {
            ProductModel product = null;
            try {
                product = productService.getProductForCode(dealProductDTO.getCode());
            }
            catch (final UnknownIdentifierException uie) {
                unknownProductCodes.add(dealProductDTO.getCode());
                continue;
            }

            // retain existing category hierarchy
            final Set<CategoryModel> superCategories = new HashSet<CategoryModel>(product.getSupercategories());
            superCategories.add(dealCategory);
            product.setSupercategories(superCategories);
            productsToSave.add(product);
        }
        modelService.saveAll(productsToSave);
        return unknownProductCodes;
    }

    private CategoryModel createDealCategory(final TargetDealCategoryDTO categoryDTO) {
        CategoryModel dealCategory = modelService.create(TargetDealCategoryModel.class);
        dealCategory.setCode(categoryDTO.getCode());
        dealCategory.setName(categoryDTO.getName());
        dealCategory.setCatalogVersion(getOfflineProductCatalogVersion());
        dealCategory.setAllowedPrincipals(getAllowedPrincipals(categoryDTO.getAllowedPrincipals()));
        try {
            modelService.save(dealCategory);
        }
        catch (final ModelSavingException mse) {
            // this is in place because of:
            // Hybris issue SUP-9353 - Duplicate items are getting created
            if (mse.getMessage().toUpperCase().contains(UNIQUE)) {
                dealCategory = categoryService.getCategoryForCode(categoryDTO.getCode());
            }
            else {
                throw mse;
            }
        }
        return dealCategory;
    }

    private CatalogVersionModel getOfflineProductCatalogVersion() {
        if (null == productCatalogOfflineVersion) {
            productCatalogOfflineVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCTS,
                    Catalog.OFFLINE_VERSION);
        }
        return productCatalogOfflineVersion;
    }

    private List<PrincipalModel> getAllowedPrincipals(final List<IntegrationPrincipalDto> principals) {
        if (CollectionUtils.isEmpty(principals)) {
            return Collections.EMPTY_LIST;
        }
        final List<PrincipalModel> allowedPrincipals = new ArrayList<>();
        for (final IntegrationPrincipalDto principalDto : principals) {
            allowedPrincipals.add(userService.getUserGroupForUID(principalDto.getUid()));
        }
        return allowedPrincipals;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

}
