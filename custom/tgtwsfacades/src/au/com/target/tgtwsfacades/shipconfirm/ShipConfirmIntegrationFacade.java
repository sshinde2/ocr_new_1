package au.com.target.tgtwsfacades.shipconfirm;

import java.util.List;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.ShipConfirm;



/**
 * @author Olivier Lamy
 */
public interface ShipConfirmIntegrationFacade
{
    /**
     * Call the business process to update the order in the pick confirm
     * 
     * @param pickConfirmOrder
     * @return IntegrationResponse
     */
    List<IntegrationResponseDto> handleShipConfirm(ShipConfirm pickConfirmOrder);
}
