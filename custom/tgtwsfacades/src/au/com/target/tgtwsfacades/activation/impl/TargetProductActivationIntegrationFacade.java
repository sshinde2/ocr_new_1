/**
 * 
 */
package au.com.target.tgtwsfacades.activation.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtwsfacades.activation.ActivationIntegrationFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationActivationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author mmaki
 * 
 */
public class TargetProductActivationIntegrationFacade implements ActivationIntegrationFacade {


    /**
     * 
     */
    public static final String ERROR_SIZE_VARIANT = "Size variant does not exist";
    public static final String ERROR_COLOUR_VARIANT = "Colour variant does not exist";
    public static final String ERROR_NO_COLOUR_VARIANT = "NO COLOR colour variant does not exist";
    public static final String ERROR_STATUS = "ApprovalStatus code not supported";

    private static final String SIZE_VARIANT = "SIZE_VARIANT";
    private static final String COLOR_VARIANT = "COLOUR_VARIANT";

    private static final String PRODUCT_INACTIVE = "inactive";
    private static final String PRODUCT_ACTIVE = "active";
    private static final String NOT_SOURCED = "NOT SOURCED";

    private static final String VARIANT_CODE_SEPERATOR = "_";



    private static final Logger LOG = Logger.getLogger(TargetProductActivationIntegrationFacade.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private TargetPreviousPermanentPriceService targetPreviousPermanentPriceService;

    @Autowired
    private TargetProductService targetProductService;

    @Autowired
    private FluentSkuUpsertService fluentSkuUpsertService;

    @Autowired
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    public IntegrationResponseDto updateApprovalStatus(final IntegrationActivationDto dto) {

        final String variantCode = dto.getVariantCode();
        final IntegrationResponseDto response = new IntegrationResponseDto(variantCode);

        final ArticleApprovalStatus status = getApprovalStatus(dto.getApprovalStatus(), response);
        if (status == null) {
            addResponseErrorMessage(response, variantCode, ERROR_STATUS + ": " + dto.getApprovalStatus());
            return response;
        }

        if (status == ArticleApprovalStatus.UNAPPROVED || status == ArticleApprovalStatus.CHECK) {
            targetPreviousPermanentPriceService.updatePreviousPermanentPriceEndDate(variantCode);

        }
        if (status == ArticleApprovalStatus.APPROVED) {
            targetPreviousPermanentPriceService.createPreviousPermanentPrice(variantCode);

        }

        TargetColourVariantProductModel colourVariant = null;
        TargetSizeVariantProductModel sizeVariant;
        try {
            if (dto.getProductCode() == null) {
                // Update size variant

                sizeVariant = getExistingTargetSizeVariant(variantCode);

                if (sizeVariant == null) {
                    addResponseErrorMessage(response, variantCode, ERROR_SIZE_VARIANT);
                }
                else {
                    updateProduct(sizeVariant, status, response);
                    response.setAdditionalInfo(SIZE_VARIANT);

                    if (sizeVariant.getBaseProduct().getCode().contains(VARIANT_CODE_SEPERATOR)) {
                        updateFakeColourVariantStatus(sizeVariant.getBaseProduct(), response);
                    }
                }
            }
            else {
                //Update the colour variant and if it's a stylegroup with sizes only product, also the size variant

                sizeVariant = getExistingTargetSizeVariant(variantCode);

                // Check if size variant exists -> Stylegroup, sizes only product -> Update size variant and colour variant
                if (sizeVariant != null) {

                    updateProduct(sizeVariant, status, response);
                    response.setAdditionalInfo(SIZE_VARIANT);

                    final String colourVariantCode = sizeVariant.getBaseProduct().getCode();
                    colourVariant = getExistingTargetColourVariant(colourVariantCode);
                    if (colourVariant == null) {
                        addResponseErrorMessage(response, colourVariantCode, ERROR_NO_COLOUR_VARIANT);
                    }
                    else {
                        if (colourVariant.getCode().contains(VARIANT_CODE_SEPERATOR)) {
                            updateFakeColourVariantStatus(colourVariant, response);
                        }
                        else {
                            updateProduct(colourVariant, status, response);
                        }
                    }

                } // Otherwise update just the colour variant
                else {
                    colourVariant = getExistingTargetColourVariant(variantCode);
                    if (colourVariant == null) {
                        addResponseErrorMessage(response, variantCode, ERROR_COLOUR_VARIANT);
                    }
                    else {
                        if (colourVariant.getCode().contains(VARIANT_CODE_SEPERATOR)) {
                            updateFakeColourVariantStatus(colourVariant, response);
                        }
                        else {
                            updateProduct(colourVariant, status, response);
                        }
                        response.setAdditionalInfo(COLOR_VARIANT);
                    }
                }
            }
            syncToFluent(response, colourVariant, sizeVariant);
        }
        catch (final Exception e) {
            LOG.error("Unable to update product approvalStatus.", e);
            final String errMsg = e.getMessage() == null ? "Runtime exception occurred." : e.getMessage();
            addResponseErrorMessage(response, variantCode, errMsg);
        }

        return response;
    }

    /**
     * Sync sellableVariants that were affected by the activation request to fluent
     * 
     * @param response
     * @param colourVariant
     * @param sizeVariant
     */
    protected void syncToFluent(final IntegrationResponseDto response,
            final TargetColourVariantProductModel colourVariant, final TargetSizeVariantProductModel sizeVariant) {
        if (targetFeatureSwitchFacade.isFluentEnabled() && response.isSuccessStatus()) {
            for (final AbstractTargetVariantProductModel sellableVariant : getSellableVariants(colourVariant,
                    sizeVariant)) {
                fluentSkuUpsertService.upsertSku(sellableVariant);
            }
        }
    }

    /**
     * Get all sellable variants affected by the activation request
     * 
     * @param colourVariant
     * @param sizeVariant
     * @return list of {@link AbstractTargetVariantProductModel}
     */
    protected List<AbstractTargetVariantProductModel> getSellableVariants(
            final TargetColourVariantProductModel colourVariant, final TargetSizeVariantProductModel sizeVariant) {
        final List<AbstractTargetVariantProductModel> sellableVariants = new ArrayList<>();
        if (sizeVariant != null) {
            sellableVariants.add(sizeVariant);
        }
        else if (colourVariant != null) {
            if (CollectionUtils.isNotEmpty(colourVariant.getVariants())) {
                for (final VariantProductModel variant : colourVariant.getVariants()) {
                    sellableVariants.add((AbstractTargetVariantProductModel)variant);
                }
            }
            else {
                sellableVariants.add(colourVariant);
            }
        }
        return sellableVariants;
    }

    private void updateFakeColourVariantStatus(final ProductModel colourVariant,
            final IntegrationResponseDto response) {
        final Collection<VariantProductModel> variants = colourVariant.getVariants();
        if (variants != null && CollectionUtils.isNotEmpty(variants)) {
            // if at least one size variant is approved, colour variant has to be approved as well
            for (final VariantProductModel var : variants) {
                if (var.getApprovalStatus() == ArticleApprovalStatus.APPROVED) {
                    updateProduct((VariantProductModel)colourVariant, ArticleApprovalStatus.APPROVED, response);
                    return;
                }
            }

            updateProduct((VariantProductModel)colourVariant, ArticleApprovalStatus.UNAPPROVED, response);
        }
    }


    /**
     * @param product
     * @param status
     * @param response
     */
    private void updateProduct(final VariantProductModel product, final ArticleApprovalStatus status,
            final IntegrationResponseDto response) {
        product.setApprovalStatus(status);
        modelService.save(product);
        targetProductService.setOnlineDateIfApplicable(product);
        response.setSuccessStatus(true);
    }

    /**
     * Add error to response
     * 
     * @param response
     * @param code
     * @param errorMsg
     */
    private void addResponseErrorMessage(final IntegrationResponseDto response, final String code,
            final String errorMsg) {
        response.setSuccessStatus(false);
        response.addMessage(errorMsg);
        LOG.error(errorMsg + ": " + code);
    }

    private TargetColourVariantProductModel getExistingTargetColourVariant(final String code) {
        TargetColourVariantProductModel variant = null;

        try {
            final ProductModel product = productService.getProductForCode(code);
            if (product instanceof TargetColourVariantProductModel) {
                variant = (TargetColourVariantProductModel)product;
            }
        }
        catch (final UnknownIdentifierException e) {
            LOG.info("No TargetColourVariantProductModel found with code " + code);
        }

        return variant;
    }

    private TargetSizeVariantProductModel getExistingTargetSizeVariant(final String code) {
        TargetSizeVariantProductModel variant = null;

        try {
            final ProductModel product = productService.getProductForCode(code);
            if (product instanceof TargetSizeVariantProductModel) {
                variant = (TargetSizeVariantProductModel)product;
            }
        }
        catch (final UnknownIdentifierException e) {
            LOG.info("No TargetSizeVariantProductModel found with code " + code);
        }

        return variant;
    }

    private ArticleApprovalStatus getApprovalStatus(final String status, final IntegrationResponseDto response) {
        if (status.equalsIgnoreCase(PRODUCT_ACTIVE)) {
            return ArticleApprovalStatus.APPROVED;
        }
        else if (status.equalsIgnoreCase(PRODUCT_INACTIVE)) {
            return ArticleApprovalStatus.UNAPPROVED;
        }
        else if (status.equalsIgnoreCase(NOT_SOURCED)) {
            response.addMessage("'NOT SOURCED' product status found");
            return ArticleApprovalStatus.CHECK;
        }
        else {
            return null;
        }
    }

}
