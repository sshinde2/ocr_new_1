/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtwsfacades.constants;

/**
 * Global class for all Tgtwsfacades constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtwsfacadesConstants extends GeneratedTgtwsfacadesConstants {
    public static final String EXTENSIONNAME = "tgtwsfacades";

    public static final String CONSIGNMENT_STATUS_KEY_OPEN = "OPEN";
    public static final String CONSIGNMENT_STATUS_KEY_INPROGRESS = "INPROGRESS";
    public static final String CONSIGNMENT_STATUS_KEY_PICKED = "PICKED";
    public static final String CONSIGNMENT_STATUS_KEY_PACKED = "PACKED";
    public static final String CONSIGNMENT_STATUS_KEY_COMPLETED = "COMPLETED";
    public static final String CONSIGNMENT_STATUS_KEY_REJECTED = "REJECTED";

    public static final String CNC_NOTIFICATION_ACTION_PICKEDUP = "PickedUp";
    public static final String CNC_NOTIFICATION_ACTION_RETURNED_TO_FLOOR = "ReturnedToFloor";

    private TgtwsfacadesConstants() {
        //empty to avoid instantiating this constant class
    }

}
