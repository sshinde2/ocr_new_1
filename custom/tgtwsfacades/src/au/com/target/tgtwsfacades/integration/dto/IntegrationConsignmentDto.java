/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;



/**
 * @author pthoma20
 * 
 */
public class IntegrationConsignmentDto {

    private Consignments consignments;

    /**
     * @return the consignments
     */
    public Consignments getConsignments() {
        return consignments;
    }

    /**
     * @param consignments
     *            the consignments to set
     */
    public void setConsignments(final Consignments consignments) {
        this.consignments = consignments;
    }


}
