/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

/**
 * @author rsamuel3
 * 
 */
public class Order {
    private String orderNumber;

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
