/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;



/**
 * @author rsamuel3
 * 
 */
public class OrderAcknowlegment {

    private Orders orders;

    /**
     * @return the orders
     */
    public Orders getOrders() {
        return orders;
    }

    /**
     * @param orders
     *            the orders to set
     */
    public void setOrders(final Orders orders) {
        this.orders = orders;
    }


}
