/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;


/**
 * @author pthoma20
 *
 */
public class Consignments {

    private List<Consignment> consignments;

    /**
     * @return the consignments
     */
    public List<Consignment> getConsignments() {
        return consignments;
    }

    /**
     * @param consignments
     *            the consignments to set
     */
    public void setConsignments(final List<Consignment> consignments) {
        this.consignments = consignments;
    }
}
