/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationWideImagesDto {
    @XmlElement
    private List<String> wideImage;

    /**
     * @return the wideImage
     */
    public List<String> getWideImage() {
        return wideImage;
    }

    /**
     * @param wideImage
     *            the wideImage to set
     */
    public void setWideImage(final List<String> wideImage) {
        this.wideImage = wideImage;
    }

    public void addWideImage(final String wideImageDto) {
        if (this.wideImage == null) {
            this.wideImage = new ArrayList<>();
        }
        this.wideImage.add(wideImageDto);
    }
}
