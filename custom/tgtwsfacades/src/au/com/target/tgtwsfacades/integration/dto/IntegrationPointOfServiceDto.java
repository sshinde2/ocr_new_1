/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement(name = "integration-pointofservice")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationPointOfServiceDto {

    @XmlElement
    private String storeName;

    @XmlElement
    private Integer storeNumber;

    @XmlElement
    private String encodedName;

    @XmlElement
    private String description; // use for storing store name

    @XmlElement
    private String type;

    @XmlElement
    private Boolean closed;

    @XmlElement
    private Boolean acceptCNC;

    @XmlElement
    private Boolean acceptLayBy;

    @XmlElement
    private Double latitude;

    @XmlElement
    private Double longitude;

    @XmlElement
    private String baseStore;

    @XmlElement
    private IntegrationProductTypeData productTypes;

    @XmlElement(name = "integration-address")
    private IntegrationAddressDto address;

    @XmlElement(name = "integration-openingschedule")
    private IntegrationOpeningScheduleDto openingSchedule;

    @XmlElement
    private boolean hasOnlySchedule;

    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the encodedName
     */
    public String getEncodedName() {
        return encodedName;
    }

    /**
     * @param encodedName
     *            the encodedName to set
     */
    public void setEncodedName(final String encodedName) {
        this.encodedName = encodedName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the closed
     */
    public Boolean getClosed() {
        return closed;
    }

    /**
     * @param closed
     *            the closed to set
     */
    public void setClosed(final Boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the acceptCNC
     */
    public Boolean getAcceptCNC() {
        return acceptCNC;
    }

    /**
     * @param acceptCNC
     *            the acceptCNC to set
     */
    public void setAcceptCNC(final Boolean acceptCNC) {
        this.acceptCNC = acceptCNC;
    }

    /**
     * @return the acceptLayBy
     */
    public Boolean getAcceptLayBy() {
        return acceptLayBy;
    }

    /**
     * @param acceptLayBy
     *            the acceptLayBy to set
     */
    public void setAcceptLayBy(final Boolean acceptLayBy) {
        this.acceptLayBy = acceptLayBy;
    }

    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the address
     */
    public IntegrationAddressDto getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(final IntegrationAddressDto address) {
        this.address = address;
    }

    /**
     * @return the openingSchedule
     */
    public IntegrationOpeningScheduleDto getOpeningSchedule() {
        return openingSchedule;
    }

    /**
     * @param openingSchedule
     *            the openingSchedule to set
     */
    public void setOpeningSchedule(final IntegrationOpeningScheduleDto openingSchedule) {
        this.openingSchedule = openingSchedule;
    }

    /**
     * @return the baseStore
     */
    public String getBaseStore() {
        return baseStore;
    }

    /**
     * @param baseStore
     *            the baseStore to set
     */
    public void setBaseStore(final String baseStore) {
        this.baseStore = baseStore;
    }

    /**
     * @return the hasOnlySchedule
     */
    public boolean isHasOnlySchedule() {
        return hasOnlySchedule;
    }

    /**
     * @param hasOnlySchedule
     *            the hasOnlySchedule to set
     */
    public void setHasOnlySchedule(final boolean hasOnlySchedule) {
        this.hasOnlySchedule = hasOnlySchedule;
    }

    /**
     * @return the productTypes
     */
    public IntegrationProductTypeData getProductTypes() {
        return productTypes;
    }

    /**
     * @param productTypes
     *            the productTypes to set
     */
    public void setProductTypes(final IntegrationProductTypeData productTypes) {
        this.productTypes = productTypes;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

}
