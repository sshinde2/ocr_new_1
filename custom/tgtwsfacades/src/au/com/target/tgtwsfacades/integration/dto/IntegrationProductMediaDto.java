/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationProductMediaDto {

    @XmlElement
    private String primaryImage;

    @XmlElement
    private IntegrationSecondaryImagesDto secondaryImages;

    @XmlElement
    private String swatchImage;

    @XmlElement
    private IntegrationWideImagesDto wideImages;

    /**
     * @return the primaryImage
     */
    public String getPrimaryImage() {
        return primaryImage;
    }

    /**
     * @param primaryImage
     *            the primaryImage to set
     */
    public void setPrimaryImage(final String primaryImage) {
        this.primaryImage = primaryImage;
    }

    /**
     * @return the secondaryImages
     */
    public IntegrationSecondaryImagesDto getSecondaryImages() {
        return secondaryImages;
    }

    /**
     * @param secondaryImages
     *            the secondaryImages to set
     */
    public void setSecondaryImages(final IntegrationSecondaryImagesDto secondaryImages) {
        this.secondaryImages = secondaryImages;
    }

    /**
     * @return the swatchImage
     */
    public String getSwatchImage() {
        return swatchImage;
    }

    /**
     * @param swatchImage
     *            the swatchImage to set
     */
    public void setSwatchImage(final String swatchImage) {
        this.swatchImage = swatchImage;
    }

    /**
     * @return the wideImages
     */
    public IntegrationWideImagesDto getWideImages() {
        return wideImages;
    }

    /**
     * @param wideImages
     *            the wideImages to set
     */
    public void setWideImages(final IntegrationWideImagesDto wideImages) {
        this.wideImages = wideImages;
    }

}
