/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.Date;


/**
 * @author pthoma20
 *
 */
public class Consignment {

    private String pickConfirmFileName;

    private String consignmentId;

    private String trackingNumber;

    private String asnNumber;

    private String customerCode;

    private String shipToCode;

    private String carrier;

    private Integer parcelCount;

    private String totalWeight;

    private String totalVolume;

    private String manifestNumber;

    private Date shipDate;

    private ConsignmentEntries consignmentEntries;



    /**
     * @return the consignmentId
     */
    public String getConsignmentId() {
        return consignmentId;
    }

    /**
     * @param consignmentId
     *            the consignmentId to set
     */
    public void setConsignmentId(final String consignmentId) {
        this.consignmentId = consignmentId;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber
     *            the trackingNumber to set
     */
    public void setTrackingNumber(final String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the asnNumber
     */
    public String getAsnNumber() {
        return asnNumber;
    }

    /**
     * @param asnNumber
     *            the asnNumber to set
     */
    public void setAsnNumber(final String asnNumber) {
        this.asnNumber = asnNumber;
    }

    /**
     * @return the customerCode
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * @param customerCode
     *            the customerCode to set
     */
    public void setCustomerCode(final String customerCode) {
        this.customerCode = customerCode;
    }

    /**
     * @return the shipToCode
     */
    public String getShipToCode() {
        return shipToCode;
    }

    /**
     * @param shipToCode
     *            the shipToCode to set
     */
    public void setShipToCode(final String shipToCode) {
        this.shipToCode = shipToCode;
    }

    /**
     * @return the carrier
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * @param carrier
     *            the carrier to set
     */
    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }

    /**
     * @return the parcelCount
     */
    public Integer getParcelCount() {
        return parcelCount;
    }

    /**
     * @param parcelCount
     *            the parcelCount to set
     */
    public void setParcelCount(final Integer parcelCount) {
        this.parcelCount = parcelCount;
    }

    /**
     * @return the totalWeight
     */
    public String getTotalWeight() {
        return totalWeight;
    }

    /**
     * @param totalWeight
     *            the totalWeight to set
     */
    public void setTotalWeight(final String totalWeight) {
        this.totalWeight = totalWeight;
    }

    /**
     * @return the totalVolume
     */
    public String getTotalVolume() {
        return totalVolume;
    }

    /**
     * @param totalVolume
     *            the totalVolume to set
     */
    public void setTotalVolume(final String totalVolume) {
        this.totalVolume = totalVolume;
    }

    /**
     * @return the manifestNumber
     */
    public String getManifestNumber() {
        return manifestNumber;
    }

    /**
     * @param manifestNumber
     *            the manifestNumber to set
     */
    public void setManifestNumber(final String manifestNumber) {
        this.manifestNumber = manifestNumber;
    }

    /**
     * @return the consignmentEntries
     */
    public ConsignmentEntries getConsignmentEntries() {
        return consignmentEntries;
    }

    /**
     * @param consignmentEntries
     *            the consignmentEntries to set
     */
    public void setConsignmentEntries(final ConsignmentEntries consignmentEntries) {
        this.consignmentEntries = consignmentEntries;
    }

    /**
     * @return the shipDate
     */
    public Date getShipDate() {
        return shipDate;
    }

    /**
     * @param shipDate
     *            the shipDate to set
     */
    public void setShipDate(final Date shipDate) {
        this.shipDate = shipDate;
    }

    /**
     * @return the pickConfirmFileName
     */
    public String getPickConfirmFileName() {
        return pickConfirmFileName;
    }

    /**
     * @param pickConfirmFileName
     *            the pickConfirmFileName to set
     */
    public void setPickConfirmFileName(final String pickConfirmFileName) {
        this.pickConfirmFileName = pickConfirmFileName;
    }

}

