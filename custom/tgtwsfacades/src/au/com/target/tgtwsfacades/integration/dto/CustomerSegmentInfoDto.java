/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;


/**
 * @author pthoma20
 *
 */
@JsonRootName(value = "customerSegments")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerSegmentInfoDto {

    @JsonProperty
    private String subscriptionId;

    @JsonProperty
    private String segment;

    /**
     * @return the subscriptionId
     */
    public String getSubscriptionId() {
        return subscriptionId;
    }

    /**
     * @param subscriptionId
     *            the subscriptionId to set
     */
    public void setSubscriptionId(final String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     * @return the segment
     */
    public String getSegment() {
        return segment;
    }

    /**
     * @param segment
     *            the segment to set
     */
    public void setSegment(final String segment) {
        this.segment = segment;
    }


}
