$productCatalog=targetProductCatalog
$contentCatalog=targetContentCatalog
$classificationCatalog=TargetClassification
$catalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged]
$classCatalogVersion=catalogversion(catalog(id[default='TargetClassification']),version[default='1.0'])[unique=true,default='TargetClassification:1.0']
$classSystemVersion=systemVersion(catalog(id[default='TargetClassification']),version[default='1.0'])[unique=true]
$class=classificationClass(ClassificationClass.code,$classCatalogVersion)[unique=true]
$supercategories=source(code, $classCatalogVersion)[unique=true]
$categories=target(code, $catalogVersion)[unique=true]
$attribute=classificationAttribute(code,$classSystemVersion)[unique=true]
$unit=unit(code,$classSystemVersion)
$regulargroup=regulargroup
$customergroup=customergroup
$esbgroup=esbgroup
$lang=en

# Create Catalog
INSERT_UPDATE Catalog;id[unique=true];name[lang=$lang]
 ;targetProductCatalog;"Target Product Catalog"
 ;targetContentCatalog;"Target Content Catalog"
 
 # Create catalog version
INSERT_UPDATE CatalogVersion;catalog(id)[unique=true];version[unique=true];active;languages(isoCode)
;$productCatalog;Staged;false;$lang
;$contentCatalog;Staged;false;$lang

# Create Customer Group
INSERT_UPDATE Usergroup;uid[unique=true];groups(uid)[mode=append];locName[lang=en]
;$regulargroup;$customergroup;$esbgroup;Regular Group for Promotion

INSERT_UPDATE User;uid[unique=true];name;description;groups(uid)
;esb;ESB group user;Prinicpal to be used by ESB layer to interact with the Hybris restful services;esbgroup

# Create Categories
INSERT_UPDATE TargetProductCategory;code[unique=true];name[lang=$lang];description[lang=$lang];$catalogVersion;supercategories(code,$catalogVersion);allowedPrincipals(uid)[default='customergroup']
;CAT111;Category 1;;;;
;CAT222;Category 2;;;;

INSERT_UPDATE TargetMerchDepartment;code[unique=true];name[lang=$lang];expressDeliveryEligible;description[lang=$lang];$catalogVersion;supercategories(code,$catalogVersion);allowedPrincipals(uid)[default='customergroup']
;123;Kids Department;true;;;;
;221;Home Department;false;;;;

# Insert Classifications
INSERT_UPDATE ClassificationClass;$classCatalogVersion;code[unique=true];name[lang=$lang];supercategories(code,$classCatalogVersion);allowedPrincipals(uid)[default='customergroup']
;;CC-CAT111;Category 1;;
;;CC-CAT222;Category 2;;

# Insert Classification Attributes
INSERT_UPDATE ClassificationAttribute;$classSystemVersion;code[unique=true];name[lang=$lang];defaultAttributeValues(code,systemVersion(catalog(id),version))
;;license;License
;;gender;Gender
;;ageRange;Age Range

# Links ClassificationClasses to Categories
INSERT_UPDATE CategoryCategoryRelation;$categories;$supercategories
;CAT111;CC-CAT111;
 
INSERT_UPDATE ClassificationAttributeValue;code[unique=true,forceWrite=true,allownull=true];name[lang=en];$classSystemVersion
;boy;Boy;
;girl;Girl;
;unisex;Unisex;
 
INSERT_UPDATE ClassAttributeAssignment;$class;$attribute;position;$unit;attributeType(code[default=string]);multiValued[default=false];range[default=false];localized[default=true];attributeValues(code,$classSystemVersion)
;CC-CAT111;license;1;;string;false;false;
;CC-CAT111;gender;2;;enum;true;false;false;boy,girl,unisex
;CC-CAT111;ageRange;3;;number;false;true;false;

# Media
INSERT_UPDATE MediaContainer;$catalogVersion;qualifier[unique=true,allownull=true];
;;IMG123
;;IMG222
;;IMG-WIDE-001
;;IMG-WIDE-002

INSERT_UPDATE Media;$catalogVersion[unique=true];code[unique=true];realfilename;mime[default='image/jpg'];altText;mediaContainer($catalogVersion,qualifier);folder(qualifier)[default='images']
;;/hero/IMG123;hero-image.jpg;image/jpg;Hero Image;IMG123
;;/thumb/IMG123;thumb-image.jpg;image/jpg;Thumb Image;IMG123
;;/large/IMG123;large-image.jpg;image/jpg;Large Image;IMG123
;;/list/IMG123;list-image.jpg;image/jpg;List Image;IMG123
;;/grid/IMG123;grid-image.jpg;image/jpg;Grid Image;IMG123
;;/full/IMG123;full-image.jpg;image/jpg;Full Image;IMG123
;;/hero/IMG222;hero-image.jpg;image/jpg;Hero Image;IMG222
;;/thumb/IMG222;thumb-image.jpg;image/jpg;Thumb Image;IMG222
;;/large/IMG222;large-image.jpg;image/jpg;Large Image;IMG222
;;/list/IMG222;list-image.jpg;image/jpg;List Image;IMG222
;;/grid/IMG222;grid-image.jpg;image/jpg;Grid Image;IMG222
;;/wide/IMG-WIDE-001;wide-image1.jpg;image/jpg;Wide Image;IMG-WIDE-001
;;/wide/IMG-WIDE-002;wide-image2.jpg;image/jpg;Wide Image;IMG-WIDE-002

INSERT_UPDATE SizeType;code[unique=true];name
;babysize;BABYWEAR;

INSERT_UPDATE ProductType;code[unique=true];name;bulky;digital
;normal;Normal;false;false
;bulky1;Bulky1;true;false
;bulky2;Bulky2;true;false
;mhd;MHD;true;false
;digital;Digital;false;true

INSERT_UPDATE Warehouse;code[unique=true];name;vendor(code);default;integrationMethod(code)
;FastlineWarehouse;Fastline;Fastline;true;ESB
;IncommWarehouse;Incomm;Incomm;true;WEBMETHODS

INSERT_UPDATE TargetFeatureSwitch;name(code)[unique=true];enabled;comment;attrName
;productimport.expressbyflag;false;Consider express-delivery mode from flag in STEP;featureProductImportExpressByFlag