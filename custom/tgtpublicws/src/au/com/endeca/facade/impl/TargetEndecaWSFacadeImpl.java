/**
 * 
 */
package au.com.endeca.facade.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.endeca.facade.TargetEndecaWSFacade;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetDealProductsQueryBuilder;
import au.com.target.endeca.infront.querybuilder.TargetInstagramAndCatalogueQueryBuilder;
import au.com.target.endeca.infront.util.EndecaQueryConnecton;
import au.com.target.tgtfacades.product.data.TargetProductListerData;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;


/**
 * @author pthoma20
 *
 */
public class TargetEndecaWSFacadeImpl implements TargetEndecaWSFacade {

    private static final Logger LOG = Logger.getLogger(TargetEndecaWSFacadeImpl.class);

    private static final String FIELDS_REQUIRED_INSTAGRAM_CATALOGUE = "tgtpublicws.instagram.catalogues.selection.fieldlist";

    private EndecaQueryConnecton endecaQueryConnection;

    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    private TargetInstagramAndCatalogueQueryBuilder targetInstagramAndCatalogueQueryBuilder;

    private TargetDealProductsQueryBuilder targetDealProductsQueryBuilder;

    /**
     * This method takes in a list of productCodes for instagram and catalogue and queries endeca to get the product
     * information.
     * 
     * @param productCodes
     * @return List<TargetProductListerData>
     */
    @Override
    public List<TargetProductListerData> getProductsForInstagramAndCatalogue(final List<String> productCodes) {
        try {

            return getProductsFromEndeca(targetInstagramAndCatalogueQueryBuilder
                    .getQueryForFetchingAllProductsForInstagram(populateEndecaSearchStateData(productCodes)));
        }
        catch (final UrlENEQueryParseException e) {
            LOG.error("TGT-ENDECA-ERROR: Error while Parsing the for Instagram/Catalogue products", e);
        }
        return new ArrayList<>();
    }

    /*
     * (non-Javadoc)
     * @see au.com.endeca.facade.TargetEndecaWSFacade#getProductsForDealCategories(java.util.List)
     */
    @Override
    public List<TargetProductListerData> getProductsForDealCategories(final List<String> dealCategoryDimIds) {
        final List<TargetProductListerData> products = new ArrayList<>();

        if (CollectionUtils.isEmpty(dealCategoryDimIds)) {
            return products;
        }

        for (final String dealCategoryDimId : dealCategoryDimIds) {
            try {
                products.addAll(getProductsFromEndeca(targetDealProductsQueryBuilder
                        .getQueryForFetchingAllProductsForDealCategory(dealCategoryDimId)));
            }
            catch (final UrlENEQueryParseException e) {
                LOG.error("TGT-ENDECA-ERROR: Error while Parsing the deal category products", e);
            }
        }

        return products;
    }

    private List<TargetProductListerData> getProductsFromEndeca(final UrlENEQuery query) {
        ENEQueryResults queryResults = null;

        if (query == null) {
            return new ArrayList<>();
        }
        try {
            final HttpENEConnection connection = endecaQueryConnection.getConection();
            queryResults = connection.query(query);
        }
        catch (final ENEQueryException e) {
            LOG.error("TGT-ENDECA-ERROR: Error while querying endeca for products", e);
        }
        catch (final TargetEndecaException e) {
            LOG.error("TGT-ENDECA-ERROR: Error while getting connection to endeca for products", e);
        }

        return populateRecordInformation(queryResults);
    }

    private List<TargetProductListerData> populateRecordInformation(final ENEQueryResults queryResults) {
        final List<TargetProductListerData> targetEndecaProducts = new ArrayList<>();
        if (queryResults == null) {
            return targetEndecaProducts;
        }
        final Navigation navigation = queryResults.getNavigation();
        final AggrERecList aggregateERecs = navigation.getAggrERecs();
        if (aggregateERecs == null) {
            return null;
        }
        for (final Object eRecObject : aggregateERecs) {
            final AggrERec aggrERec = (AggrERec)eRecObject;
            final TargetProductListerData targetProduct = aggrERecToProductConverter.convert(aggrERec);
            targetEndecaProducts.add(targetProduct);

        }
        return targetEndecaProducts;
    }

    private EndecaSearchStateData populateEndecaSearchStateData(final List<String> productCodes) {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        endecaSearchStateData.setRecordFilterOptions(recFilterOptions);
        endecaSearchStateData.setProductCodes(productCodes);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_1);
        endecaSearchStateData.setFieldListConfig(FIELDS_REQUIRED_INSTAGRAM_CATALOGUE);
        return endecaSearchStateData;
    }



    /**
     * @param endecaQueryConnection
     *            the endecaQueryConnection to set
     */
    @Required
    public void setEndecaQueryConnection(final EndecaQueryConnecton endecaQueryConnection) {
        this.endecaQueryConnection = endecaQueryConnection;
    }


    /**
     * @param aggrERecToProductConverter
     *            the aggrERecToProductConverter to set
     */
    @Required
    public void setAggrERecToProductConverter(
            final AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter) {
        this.aggrERecToProductConverter = aggrERecToProductConverter;
    }


    /**
     * @param targetInstagramAndCatalogueQueryBuilder
     *            the targetInstagramAndCatalogueQueryBuilder to set
     */
    @Required
    public void setTargetInstagramAndCatalogueQueryBuilder(
            final TargetInstagramAndCatalogueQueryBuilder targetInstagramAndCatalogueQueryBuilder) {
        this.targetInstagramAndCatalogueQueryBuilder = targetInstagramAndCatalogueQueryBuilder;
    }

    /**
     * @param targetDealProductsQueryBuilder
     *            the targetDealProductsQueryBuilder to set
     */
    @Required
    public void setTargetDealProductsQueryBuilder(final TargetDealProductsQueryBuilder targetDealProductsQueryBuilder) {
        this.targetDealProductsQueryBuilder = targetDealProductsQueryBuilder;
    }

}
