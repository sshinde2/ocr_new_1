/**
 * 
 */
package au.com.target.tgtpublicws.navigation;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.navigation.AbstractMenuBuilder;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;


/**
 * @author rmcalave
 *
 */
public class MobileMenuBuilder extends AbstractMenuBuilder {

    /**
     * Creates the menu item.
     * 
     * @param navNode
     *            the nav node
     * @return the navigation menu item
     */
    public NavigationMenuItem createMenuItem(final CMSNavigationNodeModel navNode) {
        if (navNode == null || !navNode.isVisible()) {
            return null;
        }
        final Set<NavigationMenuItem> childMenuItems = createMenuItemsForChildren(navNode.getChildren());

        final List<CMSNavigationEntryModel> entries = navNode.getEntries();

        final String navNodeTitle = StringUtils.isNotBlank(navNode.getTitle()) ? navNode.getTitle() : navNode.getName();

        NavigationMenuItem menuItem = null;
        if (CollectionUtils.isNotEmpty(entries)) {
            for (final CMSNavigationEntryModel entry : entries) {
                menuItem = createNavigationMenuItem(entry, null, childMenuItems, navNodeTitle);

                if (menuItem != null) {
                    break;
                }
            }
        }

        if (menuItem == null && CollectionUtils.isNotEmpty(childMenuItems)) {
            menuItem = new NavigationMenuItem(navNodeTitle, null, childMenuItems, false, false, false);
        }

        if (menuItem != null && navNode.getStyle() != null) {
            menuItem.setStyle(navNode.getStyle().getCode());
        }

        return menuItem;
    }

    private Set<NavigationMenuItem> createMenuItemsForChildren(final List<CMSNavigationNodeModel> navigationNodes) {
        if (CollectionUtils.isEmpty(navigationNodes)) {
            return null;
        }

        final Set<NavigationMenuItem> menuItems = new LinkedHashSet<>();

        for (final CMSNavigationNodeModel navNode : navigationNodes) {
            if (!navNode.isVisible()) {
                continue;
            }

            if (NavigationNodeTypeEnum.SECTION.equals(navNode.getType()) || isValidEntryTypeInNavigationNode(navNode)) {
                final NavigationMenuItem menuItem = createMenuItem(navNode);

                if (menuItem != null) {
                    menuItems.add(menuItem);
                }
            }
            else {
                final Set<NavigationMenuItem> menuItemsForChildren = createMenuItemsForChildren(navNode.getChildren());

                if (CollectionUtils.isNotEmpty(menuItemsForChildren)) {
                    menuItems.addAll(menuItemsForChildren);
                }
            }
        }

        return menuItems;
    }
}
