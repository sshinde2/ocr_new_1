/**
 *
 */
package au.com.target.tgtpublicws.exceptions;



public class UnknownResourceException extends RuntimeException
{
    public UnknownResourceException(final String msg)
    {
        super(msg);
    }
}
