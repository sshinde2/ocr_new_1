/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtpublicws.cache;

import de.hybris.platform.core.Registry;

import java.io.IOException;
import java.lang.management.ManagementFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;

import net.sf.ehcache.management.ManagementService;


/**
 * {@link org.springframework.beans.factory.FactoryBean} that exposes a tenant-aware EhCache
 * {@link net.sf.ehcache.CacheManager} instance (independent or shared), configured from a specified config location.
 */
public class TenantAwareEhCacheManagerFactoryBean extends EhCacheManagerFactoryBean {
    private String cacheNamePrefix = "wsCache_";

    @Value("#{configurationService.configuration.getBoolean('tgtpublicws.ehcache.registerMbeans', false)}")
    private boolean registerEhcacheMbeans;

    @Override
    public void afterPropertiesSet() throws IOException {

        setCacheManagerName(cacheNamePrefix + Registry.getCurrentTenant().getTenantID());
        super.afterPropertiesSet();

        if (registerEhcacheMbeans) {
            ManagementService.registerMBeans(getObject(), ManagementFactory.getPlatformMBeanServer(), true, true, true,
                    true);
        }
    }

    public void setCacheNamePrefix(final String cacheNamePrefix) {
        this.cacheNamePrefix = cacheNamePrefix;
    }
}
