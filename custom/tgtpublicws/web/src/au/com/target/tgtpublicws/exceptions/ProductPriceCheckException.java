/**
 * 
 */
package au.com.target.tgtpublicws.exceptions;

/**
 * @author rmcalave
 *
 */
public class ProductPriceCheckException extends RuntimeException {

    public ProductPriceCheckException(final String errorType, final String failureReason, final String barcode,
            final String storeNumber) {
        super("Error while requesting product price from POS: errorType=" + errorType + ", failureReason="
                + failureReason + ", barcode=" + barcode + ", storeNumber=" + storeNumber);
    }

}
