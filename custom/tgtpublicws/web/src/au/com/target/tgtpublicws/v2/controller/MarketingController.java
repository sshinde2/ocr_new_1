/**
 * 
 */
package au.com.target.tgtpublicws.v2.controller;

import de.hybris.platform.commercewebservicescommons.cache.CacheControl;
import de.hybris.platform.commercewebservicescommons.cache.CacheControlDirective;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.marketing.TargetSocialMediaProductsFacade;
import au.com.target.tgtfacades.marketing.data.SocialMediaProductsListData;
import au.com.target.tgtpublicws.marketing.dto.TargetSocialMediaProductsListWsDTO;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/marketing")
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 300)
public class MarketingController extends BaseController {

    @Resource(name = "targetSocialMediaProductsFacade")
    private TargetSocialMediaProductsFacade targetSocialMediaProductsFacade;

    @RequestMapping(value = "/latest/{resultCount}", method = RequestMethod.GET)
    @Cacheable(value = "socialMediaProductsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,#resultCount,#fields)")
    @ResponseBody
    public TargetSocialMediaProductsListWsDTO getInstagramPostProducts(
            @PathVariable("resultCount") final int resultCount,
            @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields) {
        final SocialMediaProductsListData socialMediaProducts = targetSocialMediaProductsFacade
                .getSocialMediaProductsByCreationTimeDescending(resultCount);

        final TargetSocialMediaProductsListWsDTO socialMediaProductsList = dataMapper.map(socialMediaProducts,
                TargetSocialMediaProductsListWsDTO.class, fields);
        return socialMediaProductsList;
    }

}
