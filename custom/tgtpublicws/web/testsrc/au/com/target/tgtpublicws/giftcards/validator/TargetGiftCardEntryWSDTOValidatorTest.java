package au.com.target.tgtpublicws.giftcards.validator;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import au.com.target.tgtpublicws.delivery.dto.order.TargetGiftCardOrderEntryWsDTO;




@UnitTest
public class TargetGiftCardEntryWSDTOValidatorTest {

    private final TargetGiftCardEntryWSDTOValidator targetGiftCardEntryWSDTOValidator = new TargetGiftCardEntryWSDTOValidator();

    private TargetGiftCardOrderEntryWsDTO dto;
    private Errors errors;

    @Before
    public void setup() {
        dto = new TargetGiftCardOrderEntryWsDTO();
        errors = new BeanPropertyBindingResult(dto, "giftCardEntry");
    }

    @Test
    public void testSuccessfulValidation() {
        dto.setFirstName("hello");
        dto.setLastName("world");
        dto.setRecipientEmailAddress("hello@world.com");
        dto.setMessageText("This is hello world message");
        targetGiftCardEntryWSDTOValidator.validate(dto, errors);
        Assertions.assertThat(errors.getErrorCount()).isEqualTo(0);
    }

    @Test
    public void testFailureValidation() {
        dto.setMessageText("This is hello world message");
        final List<String> expectedErrors = Arrays.asList("giftCardEntry.firstName.blank",
                "giftCardEntry.lastName.blank",
                "giftCardEntry.recipientEmailAddress.blank");
        targetGiftCardEntryWSDTOValidator.validate(dto, errors);
        final List<ObjectError> objectErrors = errors.getAllErrors();
        Assertions.assertThat(errors.getErrorCount()).isEqualTo(3);
        for (final ObjectError error : objectErrors) {
            Assertions.assertThat(expectedErrors).contains(error.getCode());
        }
    }

    @Test
    public void testFailureValidationInvalidEmail() {
        final List<String> expectedErrors = Arrays.asList("giftCardEntry.recipientEmailAddress.invalid");
        dto.setFirstName("hello");
        dto.setLastName("world");
        dto.setRecipientEmailAddress("hello@");
        dto.setMessageText("This is hello world message");
        targetGiftCardEntryWSDTOValidator.validate(dto, errors);
        Assertions.assertThat(errors.getErrorCount()).isEqualTo(1);
        final List<ObjectError> objectErrors = errors.getAllErrors();
        for (final ObjectError error : objectErrors) {
            Assertions.assertThat(expectedErrors).contains(error.getCode());
        }
    }

}
