package au.com.target.tgtpublicws.v2.filter;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.web.savedrequest.Enumerator;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SessionInitialisingFilterTest {
    @Mock
    private StoreSessionFacade mockStoreSessionFacade;

    @InjectMocks
    private final SessionInitialisingFilter sessionInitialisingFilter = new SessionInitialisingFilter();

    @Test
    public void testDoFilterInternal() throws Exception {
        final Locale mockLocale = mock(Locale.class);
        final Enumeration<Locale> localeEnumeration = new Enumerator<Locale>(Collections.singletonList(mockLocale));

        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        given(mockRequest.getLocales()).willReturn(localeEnumeration);

        final HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        final FilterChain mockFilterChain = mock(FilterChain.class);

        sessionInitialisingFilter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

        verify(mockFilterChain).doFilter(mockRequest, mockResponse);

        final ArgumentCaptor<List> preferredLocalesCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockStoreSessionFacade).initializeSession(preferredLocalesCaptor.capture());

        assertThat(preferredLocalesCaptor.getValue()).containsExactly(mockLocale);
    }

}
