/**
 * 
 */
package au.com.target.tgtcore.deals.wrappers.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.result.PromotionOrderEntry;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;

import org.springframework.util.Assert;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * Wrapper to support calculating deals. This exists to overcome the following problems:
 * <ul>
 * <li>Jalo layer does not utilise the model classes from the service layer in hybris, this makes getting custom
 * attributes a little more awkward and unsafe</li>
 * <li>We want to leverage the POS logic for applying deals, but it assumes the product has ready access of the deal it
 * is in.</li>
 * 
 */
@SuppressWarnings("deprecation")
public class ProductWrapperImpl implements ProductWrapper {
    private final PromotionOrderEntry dealOrderEntry;
    private final AbstractTargetVariantProduct targetVariant;
    private final AbstractTargetVariantProductModel targetVariantModel;
    private final DealWrapper deal;

    private DealQualifierModel qualifierModel = null;
    private Boolean rewardProduct;
    private Long unitPrice = null;


    private DealItemTypeEnum type;
    private long dealMarkdown;
    private boolean inDeal = false;
    private int instance;
    private boolean dealRewardsPartiallyConsumed;

    private final ModelService modelService =
            (ModelService)Registry.getGlobalApplicationContext().getBean("modelService");

    /**
     * Constructor
     * 
     * @param dealOrderEntry
     *            order entry from the deal(promotion)
     * @param deal
     *            wrapped deal details
     */
    public ProductWrapperImpl(final PromotionOrderEntry dealOrderEntry, final DealWrapper deal) {
        Assert.notNull(dealOrderEntry);
        Assert.notNull(deal);
        this.dealOrderEntry = dealOrderEntry;
        this.deal = deal;
        final Product product = dealOrderEntry.getProduct(deal.getCtx());
        Assert.isInstanceOf(AbstractTargetVariantProduct.class, product);
        targetVariant = (AbstractTargetVariantProduct)product;
        targetVariantModel = modelService.toModelLayer(targetVariant);
    }

    /**
     * @return the qualifier model
     */
    @Override
    public DealQualifierModel getQualifierModel() {
        final AbstractDealModel dealModel = deal.getDealModel();
        if (qualifierModel != null || !(dealModel instanceof AbstractSimpleDealModel)) {
            return qualifierModel;
        }

        final AbstractSimpleDealModel simpleDealModel = (AbstractSimpleDealModel)dealModel;
        final List<DealQualifierModel> qualList = simpleDealModel.getQualifierList();

        final Collection<CategoryModel> dealCatPath = targetVariantModel.getSupercategories();

        for (final DealQualifierModel dealQualifierModel : qualList) {
            final List<TargetDealCategoryModel> dealCatList = dealQualifierModel.getDealCategory();
            for (final TargetDealCategoryModel targetDealCategoryModel : dealCatList) {
                if (dealCatPath.contains(targetDealCategoryModel)) {
                    // technically we are not limiting the variant to be in only one qualifying list, but this is a requirement for other systems within Target
                    qualifierModel = dealQualifierModel;
                    return qualifierModel;
                }
            }
        }

        return null;
    }

    /**
     * Price of a single unit.
     * 
     * @return price in cents
     */
    @Override
    public long getUnitSellPrice() {
        if (unitPrice == null) {
            final double dUnitCents = dealOrderEntry.getBasePrice(deal.getCtx()).doubleValue() * 100;
            unitPrice = Long.valueOf(Math.round(dUnitCents));
        }

        return unitPrice.longValue();
    }

    /**
     * @return the adjusted unit sell price
     */
    @Override
    public long getAdjustedUnitSellPrice() {
        return getUnitSellPrice() - dealMarkdown;
    }

    /**
     * Set the required details of a fired deal.
     * 
     * @param inst
     *            which instance of this deal does this entry belong to
     * @param dealType
     *            are we a reward or a qualifying item
     * @param saveAmt
     *            the amout we markdown this item for this deal
     */
    @Override
    public void setDealApplied(final int inst, final DealItemTypeEnum dealType,
            final long saveAmt) {
        type = dealType;
        dealMarkdown = saveAmt;
        instance = inst;
        inDeal = true;
    }

    /**
     * Remove all calculation that form part of a deal. The getItem method will still give details of the deal this item
     * is part off.
     */
    @Override
    public void clearDeal() {
        type = null;
        dealMarkdown = 0;
        instance = 0;
        inDeal = false;
    }

    /**
     * @return true if a deal has been applied to this item entry
     */
    @Override
    public boolean isInDeal() {
        return inDeal;
    }

    /**
     * @return the dealMarkdown
     */
    @Override
    public long getDealMarkdown() {
        return dealMarkdown;
    }

    /**
     * @return the product
     */
    @Override
    public Product getProduct() {
        return targetVariant;
    }

    /**
     * @return the type
     */
    @Override
    public DealItemTypeEnum getType() {
        return type;
    }

    /**
     * @return the instance
     */
    @Override
    public int getInstance() {
        return instance;
    }

    @Override
    public boolean isRewardProduct() {
        final AbstractDealModel dealModel = deal.getDealModel();
        if (rewardProduct != null) {
            return rewardProduct.booleanValue();
        }

        final AbstractSimpleDealModel simpleDealModel = (AbstractSimpleDealModel)dealModel;
        final List<TargetDealCategoryModel> dealCatList = simpleDealModel.getRewardCategory();

        final Collection<CategoryModel> dealCatPath = targetVariantModel.getSupercategories();

        for (final TargetDealCategoryModel targetDealCategoryModel : dealCatList) {
            if (dealCatPath.contains(targetDealCategoryModel)) {
                rewardProduct = Boolean.TRUE;
                return true;
            }
        }

        rewardProduct = Boolean.FALSE;
        return false;
    }


    /**
     * set the dealRewardsPartiallyConsumed to true
     */
    @Override
    public void setDealRewardsPartiallyConsumed() {
        this.dealRewardsPartiallyConsumed = true;
    }


    /**
     * @return the dealRewardsPartiallyConsumed
     */
    @Override
    public boolean getDealRewardsPartiallyConsumed() {
        return this.dealRewardsPartiallyConsumed;
    }


    /**
     * set the dealRewardsPartiallyConsumed to false
     */
    @Override
    public void clearDealRewardsPartiallyConsumed() {
        // YTODO Auto-generated method stub
        this.dealRewardsPartiallyConsumed = false;
    }
}
