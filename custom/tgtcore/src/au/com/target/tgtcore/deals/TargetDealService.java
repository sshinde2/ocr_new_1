package au.com.target.tgtcore.deals;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * 
 * Service API to manage {@link AbstractDealModel}
 * 
 */
public interface TargetDealService {

    /**
     * Find deal by dealId
     * 
     * @param dealId
     * @return AbstractDealModel
     */
    AbstractDealModel getDealForId(String dealId);

    /**
     * @return list of active deals
     */
    List<AbstractDealModel> getAllActiveDeals();

    /**
     * get deal by category
     * 
     * @param targetDealCategoryModel
     * @return AbstractSimpleDealModel
     */
    AbstractSimpleDealModel getDealByCategory(TargetDealCategoryModel targetDealCategoryModel);
}
