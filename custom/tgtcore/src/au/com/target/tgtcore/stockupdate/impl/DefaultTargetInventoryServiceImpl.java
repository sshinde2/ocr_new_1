/**
 * 
 */
package au.com.target.tgtcore.stockupdate.impl;

import au.com.target.tgtcore.stockupdate.TargetInventoryService;


/**
 * @author bhuang3
 * 
 */
public class DefaultTargetInventoryServiceImpl implements TargetInventoryService {

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.stockupdate.service.TargetCaInventoryService#updateInventoryItemQuantity(java.lang.String, int)
     */
    @Override
    public boolean updateInventoryItemQuantity(final String productCode, final int quantity) {
        return false;
    }

}
