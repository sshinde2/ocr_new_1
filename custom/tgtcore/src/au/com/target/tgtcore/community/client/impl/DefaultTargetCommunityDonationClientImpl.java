/**
 * 
 */
package au.com.target.tgtcore.community.client.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.community.client.TargetCommunityDonationClient;
import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;


/**
 * @author rmcalave
 * 
 */
public class DefaultTargetCommunityDonationClientImpl implements TargetCommunityDonationClient {

    private static final Logger LOG = Logger.getLogger(DefaultTargetCommunityDonationClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.community.client.TargetCommunityDonationClient#sendDonationRequestData(au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto)
     */
    @Override
    public boolean sendDonationRequestData(final TargetCommunityDonationRequestDto request) {
        LOG.info("sendDonationRequestData(TargetCommunityDonationRequestDto) :: default implementation");
        return true;
    }

}
