/**
 * 
 */
package au.com.target.tgtcore.community.dto;

/**
 * @author rmcalave
 * 
 */
public class TargetCommunityDonationRequestDto {
    private String eventName;

    private String orgName;

    private String abnOrAcn;

    private String orgFocus;

    private String contactName;

    private String role;

    private String orgStreet;

    private String orgCity;

    private String orgState;

    private String orgPostcode;

    private String orgCountry;

    private String email;

    private String phone;

    private String scope;

    private String reason;

    private String requestNotes;

    private String preferredMethodOfContact;

    private String storeNumber;

    private String storeState;

    /**
     * @return the eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * @param eventName
     *            the eventName to set
     */
    public void setEventName(final String eventName) {
        this.eventName = eventName;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName
     *            the orgName to set
     */
    public void setOrgName(final String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return the abnOrAcn
     */
    public String getAbnOrAcn() {
        return abnOrAcn;
    }

    /**
     * @param abnOrAcn
     *            the abnOrAcn to set
     */
    public void setAbnOrAcn(final String abnOrAcn) {
        this.abnOrAcn = abnOrAcn;
    }

    /**
     * @return the orgFocus
     */
    public String getOrgFocus() {
        return orgFocus;
    }

    /**
     * @param orgFocus
     *            the orgFocus to set
     */
    public void setOrgFocus(final String orgFocus) {
        this.orgFocus = orgFocus;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName
     *            the contactName to set
     */
    public void setContactName(final String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role
     *            the role to set
     */
    public void setRole(final String role) {
        this.role = role;
    }

    /**
     * @return the orgStreet
     */
    public String getOrgStreet() {
        return orgStreet;
    }

    /**
     * @param orgStreet
     *            the orgStreet to set
     */
    public void setOrgStreet(final String orgStreet) {
        this.orgStreet = orgStreet;
    }

    /**
     * @return the orgCity
     */
    public String getOrgCity() {
        return orgCity;
    }

    /**
     * @param orgCity
     *            the orgCity to set
     */
    public void setOrgCity(final String orgCity) {
        this.orgCity = orgCity;
    }

    /**
     * @return the orgState
     */
    public String getOrgState() {
        return orgState;
    }

    /**
     * @param orgState
     *            the orgState to set
     */
    public void setOrgState(final String orgState) {
        this.orgState = orgState;
    }

    /**
     * @return the orgPostcode
     */
    public String getOrgPostcode() {
        return orgPostcode;
    }

    /**
     * @param orgPostcode
     *            the orgPostcode to set
     */
    public void setOrgPostcode(final String orgPostcode) {
        this.orgPostcode = orgPostcode;
    }

    /**
     * @return the orgCountry
     */
    public String getOrgCountry() {
        return orgCountry;
    }

    /**
     * @param orgCountry
     *            the orgCountry to set
     */
    public void setOrgCountry(final String orgCountry) {
        this.orgCountry = orgCountry;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * @param scope
     *            the scope to set
     */
    public void setScope(final String scope) {
        this.scope = scope;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * @return the requestNotes
     */
    public String getRequestNotes() {
        return requestNotes;
    }

    /**
     * @param requestNotes
     *            the requestNotes to set
     */
    public void setRequestNotes(final String requestNotes) {
        this.requestNotes = requestNotes;
    }

    /**
     * @return the preferredMethodOfContact
     */
    public String getPreferredMethodOfContact() {
        return preferredMethodOfContact;
    }

    /**
     * @param preferredMethodOfContact
     *            the preferredMethodOfContact to set
     */
    public void setPreferredMethodOfContact(final String preferredMethodOfContact) {
        this.preferredMethodOfContact = preferredMethodOfContact;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeState
     */
    public String getStoreState() {
        return storeState;
    }

    /**
     * @param storeState
     *            the storeState to set
     */
    public void setStoreState(final String storeState) {
        this.storeState = storeState;
    }
}
