/**
 * 
 */
package au.com.target.tgtcore.community.client;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;


/**
 * @author rmcalave
 * 
 */
public interface TargetCommunityDonationClient {

    /**
     * Send the donation request data.
     * 
     * @param request
     *            The request data
     * @return true if successful, false if not
     */
    boolean sendDonationRequestData(TargetCommunityDonationRequestDto request);
}
