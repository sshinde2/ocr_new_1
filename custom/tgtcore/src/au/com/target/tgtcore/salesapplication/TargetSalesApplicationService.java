/**
 * 
 */
package au.com.target.tgtcore.salesapplication;

import de.hybris.platform.commerceservices.enums.SalesApplication;



/**
 * Service to manage operations related to sales application enum.
 * 
 * @author jjayawa1
 * 
 */
public interface TargetSalesApplicationService {

    /**
     * Set the current sales application enum
     * 
     * @param salesApplication
     */
    void setCurrentSalesApplication(SalesApplication salesApplication);

    /**
     * Get the current sales application enum
     * 
     * @return SalesApplication
     */
    SalesApplication getCurrentSalesApplication();
}
