package au.com.target.tgtcore.ordercancel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;


/**
 * Facade service to provide methods required when executing a cancel request.
 * 
 * 1) Modify order/consignment entries <br>
 * 2) Recalculate and refund the difference <br>
 * 3) Release the appropriate product stock <br>
 * 4) Update Order status accordingly <br>
 * 5) Update the order cancel record.
 * 
 */
public interface TargetCancelService {

    /**
     * Update order status based on cancel request
     * 
     * @param cancelRequestRecordEntry
     * @param order
     */
    void updateOrderStatus(OrderCancelRecordEntryModel cancelRequestRecordEntry, OrderModel order);

    /**
     * Update the status of all consignments attached to the order. If the request is not a partial cancel, sets the
     * status of all consignments to CANCELLED. If the request is for a partial cancel, adjusts or removes the
     * consignment entries accordingly.
     * 
     * @param cancelRequest
     */
    void updateConsignmentStatus(final OrderCancelRequest cancelRequest);

    /**
     * Modify order based on cancel request
     * 
     * @param cancelRequest
     * @param order
     * @throws OrderCancelException
     */
    void modifyOrderAccordingToRequest(OrderCancelRequest cancelRequest, OrderModel order) throws OrderCancelException;

    /**
     * Modify order and consignments based on cancel request
     * 
     * @param cancelRequest
     * @throws OrderCancelException
     * @see #modifyOrderAccordingToRequest(OrderCancelRequest, OrderModel)
     * @see #updateConsignmentStatus(OrderCancelRequest)
     */
    void modifyOrderAccordingToRequest(OrderCancelRequest cancelRequest) throws OrderCancelException;

    /**
     * Release stock based on cancel request
     * 
     * @param cancelRequest
     * @throws OrderCancelException
     */
    void releaseStock(OrderCancelRequest cancelRequest) throws OrderCancelException;

    /**
     * Fabricate a response for the order cancel request
     * 
     * @param request
     * @param success
     * @param message
     * @return OrderCancelResponse
     */
    OrderCancelResponse makeInternalResponse(OrderCancelRequest request, boolean success, String message);

    /**
     * Recalculate order and discounts on the order
     * 
     * @param order
     */
    void recalculateOrder(OrderModel order);

    /**
     * Update the order cancel request to flag it as done and mark partial if appropriate
     * 
     * @param orderCancelRequest
     * @throws OrderCancelRecordsHandlerException
     */
    void updateRecordEntry(OrderCancelRequest orderCancelRequest) throws OrderCancelRecordsHandlerException;

    /**
     * Determine whether we should extract the cancel to warehouse, based on current status of consignments
     * 
     * @param order
     * @return true if we should do an extract
     */
    boolean isExtractCancelToWarehouseRequired(OrderModel order);

    /**
     * recalculate as a result of this refund/cancel, if the order value goes up (ie deal broken) then apply a discount
     * to the order to bring it back to the same price
     * 
     * @param order
     *            order to be recalculated
     * @param cancelRequestRecordEntry
     *            cancel request record entry to be updated
     */
    void recalculateOrderPreventingIncrease(final OrderModel order, OrderCancelRecordEntryModel cancelRequestRecordEntry);

    /**
     * preview a recalculation as a result of this refund, if the order value goes up (ie deal broken) then apply a
     * discount to the order to bring it back to the same price
     * 
     * @param order
     *            order to be recalculated
     * @param deliveryCostRefund
     *            if this is for a preview what is the delivery cost we will be refunding
     */
    void previewOrderPreventingIncrease(final OrderModel order, final double deliveryCostRefund);

    /**
     * Perform a recalculation to determine if the value of the order will go up as a result of this cancellation
     * attempt.
     * 
     * @param order
     *            order to be recalculated
     * @return true if the order price goes up, false otherwise
     */
    boolean willCancellationIncreaseOrderValue(OrderModel order);

    /**
     * Update shipping amount for cancel.
     * 
     * @param order
     *            the order
     * @param cancelRequestRecordEntry
     *            the cancel request record entry
     */
    void updateShippingAmountForCancel(final OrderModel order,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry);

    /**
     * Releases reserved stock in for order from warehouse.
     * 
     * @param order
     */
    public void releaseAllStockForOrder(final OrderModel order);

}
