/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.denialstrategies.AbstractCancelDenialStrategy;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Disallow cancellation if there is any bus proc running on the order.
 * 
 * @author sbryan6
 *
 */
public class TargetBusProcCancelDenialStrategy extends AbstractCancelDenialStrategy implements
        OrderCancelDenialStrategy {

    private List<String> excludedBusinessProcesses;

    /**
     * Disallow cancellation if there is any business process(except excluded ones) running on the order.
     * 
     */
    @Override
    public OrderCancelDenialReason getCancelDenialReason(final OrderCancelConfigModel configuration,
            final OrderModel order,
            final PrincipalModel requestor,
            final boolean partialCancel, final boolean partialEntryCancel) {

        if (CollectionUtils.isNotEmpty(order.getOrderProcess())) {
            for (final OrderProcessModel process : order.getOrderProcess()) {

                // excluding configured processes from denying cancellation
                if (CollectionUtils.isEmpty(excludedBusinessProcesses)
                        || !excludedBusinessProcesses.contains(process.getProcessDefinitionName())) {

                    if (ProcessState.RUNNING.equals(process.getState())) {
                        return getReason();
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param excludedBusinessProcesses
     *            the excludedBusinessProcesses to set
     */
    @Required
    public void setExcludedBusinessProcesses(final List<String> excludedBusinessProcesses) {
        this.excludedBusinessProcesses = excludedBusinessProcesses;
    }

}
