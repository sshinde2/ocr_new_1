/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy;

import de.hybris.platform.core.model.order.OrderModel;



/**
 * @author rmcalave
 * 
 */
public interface VoucherCorrectionStrategy {

    /**
     * Correct vouchers that are attached to the order.
     * 
     * @param orderModel
     *            the order
     */
    void correctAppliedVouchers(OrderModel orderModel);

}