/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.denialstrategies.AbstractCancelDenialStrategy;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;
import de.hybris.platform.ticket.model.CsAgentGroupModel;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * CS-cockpit cancel button will be disabled based on cs-user group.
 * 
 * @author fkhan4
 *
 */
public class TargetCSUserGroupCancelDenialStrategy extends AbstractCancelDenialStrategy implements
        OrderCancelDenialStrategy {

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public OrderCancelDenialReason getCancelDenialReason(final OrderCancelConfigModel paramOrderCancelConfigModel,
            final OrderModel orderModel, final PrincipalModel requestor, final boolean partialCancel,
            final boolean partialEntryCancel) {

        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return null;
        }
        for (final PrincipalGroupModel groupModel : requestor.getGroups()) {
            if (groupModel instanceof CsAgentGroupModel) {
                final CsAgentGroupModel csAgentGroupModel = (CsAgentGroupModel)groupModel;
                if (null != csAgentGroupModel.getCancellationAllowed()
                        && BooleanUtils.isTrue(csAgentGroupModel.getCancellationAllowed())) {
                    return null;
                }
            }
        }

        return getReason();
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}
