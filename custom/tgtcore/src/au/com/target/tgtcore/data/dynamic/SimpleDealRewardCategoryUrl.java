/**
 *
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author rmcalave
 *
 */
public class SimpleDealRewardCategoryUrl extends AbstractSimpleDealCategoryUrl {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public String get(final AbstractSimpleDealModel model) {
        if (model == null) {
            return null;
        }

        final List<TargetDealCategoryModel> rewardCategories = model.getRewardCategory();

        if (CollectionUtils.isEmpty(rewardCategories)) {
            return null;
        }

        final TargetDealCategoryModel rewardCategory = rewardCategories.get(0);
        return getCategoryUrl(rewardCategory);
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.AbstractItemModel, java.lang.Object)
     */
    @Override
    public void set(final AbstractSimpleDealModel model, final String value) {
        throw new UnsupportedOperationException();
    }

}
