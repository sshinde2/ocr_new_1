/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author rsamuel3
 *
 */
public class ProductDisplayOnly extends
        AbstractDynamicAttributeHandler<Boolean, TargetProductModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public Boolean get(final TargetProductModel model) {
        return determineIfDisplayOnly(model);
    }

    /**
     * Go through the list of variants and if the variant has online products this flag is false
     * 
     * @param productModel
     *            Product
     * @return boolean true if all the variants are display only and false otherwise.
     */
    private Boolean determineIfDisplayOnly(final TargetProductModel productModel) {
        if (CollectionUtils.isEmpty(productModel.getVariants())) {
            return Boolean.FALSE;
        }
        for (final VariantProductModel colourVariant : productModel.getVariants()) {
            final Collection<VariantProductModel> sizeVariants = colourVariant.getVariants();

            // if it has size variants and any of the size variant is online only then exit with false
            if (CollectionUtils.isNotEmpty(sizeVariants) && areAnySizeVariantWithOnline(sizeVariants)) {
                return Boolean.FALSE;
            }
            // if size variant is empty then take colourvariant and check display only
            if (CollectionUtils.isEmpty(sizeVariants) && BooleanUtils.isFalse(isDisplayOnly(colourVariant))) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    private boolean areAnySizeVariantWithOnline(final Collection<VariantProductModel> sizeVariants) {
        for (final VariantProductModel sizeVariant : sizeVariants) {
            if (BooleanUtils.isFalse(isDisplayOnly(sizeVariant))) {
                return true;
            }
        }
        return false;
    }

    private Boolean isDisplayOnly(final VariantProductModel variant) {
        final AbstractTargetVariantProductModel tgtVariant = (AbstractTargetVariantProductModel)variant;
        return tgtVariant.getDisplayOnly() == null ? Boolean.FALSE : tgtVariant.getDisplayOnly();
    }
}
