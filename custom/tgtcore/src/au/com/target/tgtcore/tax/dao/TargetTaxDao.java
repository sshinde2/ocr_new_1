/**
 * 
 */
package au.com.target.tgtcore.tax.dao;

import de.hybris.platform.order.daos.TaxDao;

import au.com.target.tgtcore.model.TargetTaxModel;


/**
 * Extend TaxDao to add method to retrieve the default tax
 * 
 */
public interface TargetTaxDao extends TaxDao {

    /**
     * Return the default TargetTax, eg to use for delivery and layby
     * 
     * @return TargetTaxModel
     */
    TargetTaxModel getDefaultTax();
}
