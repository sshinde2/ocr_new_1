/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.delivery.dto.DeliveryCostEnumType;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;


/**
 * Delivery fees will be charged based on the items within the order. If there are no bulky items in the order there is
 * a flat fee (currently $9.00), if there is 1 or 2 bulky items in an order then a bulky fee is charged (currently
 * $49.00), if there is 3 or more bulky items in the order a extended (or double) bulky fee is charged (currently
 * $98.00)
 */
public class TargetFindDeliveryCostStrategyImpl implements TargetFindDeliveryCostStrategy {
    private static final Logger LOG = Logger.getLogger(TargetFindDeliveryCostStrategyImpl.class);
    private TargetDeliveryService targetDeliveryService;

    private FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy;


    @Override
    public PriceValue getDeliveryCost(final AbstractOrderModel order) {
        ServicesUtil.validateParameterNotNullStandardMessage("order", order);
        return new PriceValue(order.getCurrency().getIsocode(), calculateDeliveryCost(order).getDeliveryCost(),
                order.getNet().booleanValue());
    }

    /**
     * Recalculates the delivery cost for given cart or order.
     * 
     * @param abstractOrder
     *            the cart or the order to calculate the delivery cost for
     * @return the delivery cost
     */
    private DeliveryCostDto calculateDeliveryCost(final AbstractOrderModel abstractOrder) {
        final TargetZoneDeliveryModeModel deliveryMode = (TargetZoneDeliveryModeModel)abstractOrder.getDeliveryMode();
        final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
        deliveryCostDto.setType(DeliveryCostEnumType.NORMAL);
        try {
            if (abstractOrder instanceof OrderModel) {
                // is not a cart and initialDeliveryCost is not null, then this is an existing order is being recalculated
                final OrderModel order = (OrderModel)abstractOrder;

                if (order.getInitialDeliveryCost() != null) {
                    final double deliveryCost = deliveryCostOutstandingForOrder(order);
                    deliveryCostDto.setDeliveryCost(deliveryCost);
                    deliveryCostDto.setOriginalDeliveryCost(deliveryCost);
                    return deliveryCostDto;
                }
            }

            if (deliveryMode != null) {
                final AbstractTargetZoneDeliveryModeValueModel deliveryValue = abstractOrder
                        .getZoneDeliveryModeValue();
                return targetDeliveryService.getDeliveryCostForDeliveryType(deliveryValue, abstractOrder);
            }
        }
        catch (final Exception e) {
            LOG.error("Could not find deliveryCost for order [" + abstractOrder.getCode() + "], skipping...", e);
        }
        return deliveryCostDto;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy#getDeliveryCost(de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel, de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public double getDeliveryCost(final ZoneDeliveryModeModel deliveryMode, final AbstractOrderModel abstractOrder) {
        final ZoneDeliveryModeValueModel zoneDeliveryModeValueModel = targetDeliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(deliveryMode, abstractOrder);
        return targetDeliveryService.getDeliveryCostForDeliveryType(
                (AbstractTargetZoneDeliveryModeValueModel)zoneDeliveryModeValueModel, abstractOrder).getDeliveryCost();
    }

    @Override
    public double deliveryCostOutstandingForOrder(final OrderModel order) {

        // The amount outstanding for the order is the initial delivery cost minus any amount refunded so far
        final Double initialDeliveryCost = order.getInitialDeliveryCost();
        if (initialDeliveryCost == null) {
            LOG.warn("Initial Delivery Cost is null for order: " + order.getCode());
            return 0d;
        }

        final double refundedSoFarDeliveryCost = findOrderRefundedShippingStrategy.getRefundedShippingAmount(order);
        return initialDeliveryCost.doubleValue() - refundedSoFarDeliveryCost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resetDeliveryValue(final CartModel order) {
        final DeliveryModeModel deliveryMode = order.getDeliveryMode();
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            // ensure getting delivery mode value doesn't return the current one but clearing the current one
            order.setZoneDeliveryModeValue(null);
            try {
                final ZoneDeliveryModeValueModel newValue = targetDeliveryService
                        .getZoneDeliveryModeValueForAbstractOrderAndMode(
                                (TargetZoneDeliveryModeModel)deliveryMode, order);
                order.setZoneDeliveryModeValue((AbstractTargetZoneDeliveryModeValueModel)newValue);
            }
            catch (final TargetNoPostCodeException e) {
                LOG.debug(e);
            }
        }
    }

    @Override
    public void resetShipsterAttibutes(final AbstractOrderModel orderModel, final DeliveryCostDto deliveryCostDto) {
        if (orderModel instanceof CartModel) {
            orderModel.setAusPostDeliveryClubFreeDelivery(null);
            orderModel.setOriginalDeliveryCost(Double.valueOf(0d));
            if (DeliveryCostEnumType.SHIPSTERFREEDELIVERY.equals(deliveryCostDto.getType())) {
                orderModel.setAusPostDeliveryClubFreeDelivery(Boolean.TRUE);
                orderModel.setOriginalDeliveryCost(Double.valueOf(deliveryCostDto.getOriginalDeliveryCost()));
            }
            else {
                orderModel.setAusPostDeliveryClubFreeDelivery(Boolean.FALSE);
                orderModel.setOriginalDeliveryCost(Double.valueOf(deliveryCostDto.getOriginalDeliveryCost()));
            }
        }
    }

    @Override
    public DeliveryCostDto getDeliveryCostDto(final AbstractOrderModel order) {
        ServicesUtil.validateParameterNotNullStandardMessage("order", order);
        return calculateDeliveryCost(order);
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param findOrderRefundedShippingStrategy
     *            the findOrderRefundedShippingStrategy to set
     */
    @Required
    public void setFindOrderRefundedShippingStrategy(
            final FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy) {
        this.findOrderRefundedShippingStrategy = findOrderRefundedShippingStrategy;
    }


}
