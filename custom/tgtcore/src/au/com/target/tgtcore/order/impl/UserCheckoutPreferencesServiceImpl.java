/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.UserCheckoutPreferencesService;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;


/**
 * @author ajit
 *
 */
public class UserCheckoutPreferencesServiceImpl implements UserCheckoutPreferencesService {

    private static final Logger LOG = Logger.getLogger(UserCheckoutPreferencesServiceImpl.class);

    private ModelService modelService;
    private TargetPointOfServiceDao targetPointOfServiceDao;

    @Override
    public void saveUserPreferences(final OrderModel orderModel) {
        try {
            if (orderModel.getUser() instanceof TargetCustomerModel) {
                final TargetCustomerModel customer = (TargetCustomerModel)orderModel.getUser();
                if (!CustomerType.GUEST.equals(customer.getType())) {
                    final TargetZoneDeliveryModeModel deliveryModel = (TargetZoneDeliveryModeModel)orderModel
                            .getDeliveryMode();
                    customer.setPreferredDeliveryMode(deliveryModel);
                    modelService.save(customer);
                    if (deliveryModel.getIsDeliveryToStore().booleanValue()) {
                        final TargetAddressModel addressModel = (TargetAddressModel)orderModel.getDeliveryAddress();
                        if (addressModel != null) {
                            customer.setSavedCncStoreDetails(Collections.EMPTY_LIST);
                            modelService.save(customer);
                            final SavedCncStoreDetailsModel savedCncStoreDetailsModel = modelService
                                    .create(SavedCncStoreDetailsModel.class);
                            savedCncStoreDetailsModel.setFirstName(addressModel.getFirstname());
                            savedCncStoreDetailsModel.setLastName(addressModel.getLastname());
                            savedCncStoreDetailsModel.setPhoneNumber(addressModel.getPhone1());
                            if (addressModel.getTitle() != null) {
                                savedCncStoreDetailsModel.setTitle(addressModel.getTitle().getCode());
                            }
                            savedCncStoreDetailsModel
                                    .setStore(targetPointOfServiceDao.getPOSByStoreNumber(orderModel
                                            .getCncStoreNumber()));
                            savedCncStoreDetailsModel.setTargetCustomer(customer);
                            modelService.save(savedCncStoreDetailsModel);
                        }
                    }
                }
            }
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.info("Exception during getting POS details while persisting the preferred delivery mode for store No. & orderId : "
                    + orderModel.getCncStoreNumber() + " : " + orderModel.getCode());
            LOG.info("Exception message : " + e.getMessage());
        }
        catch (final ModelSavingException e) {
            LOG.info("Model saving exception while storing preferred CNC store & user details for order Id : "
                    + orderModel.getCode());
            LOG.info("Exception message : " + e.getMessage());
        }
        catch (final Exception e) {
            LOG.info("Exception while storing preferred CNC store & user details for order Id : "
                    + orderModel.getCode());
            LOG.info("Exception message : " + e.getMessage());
        }

    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetPointOfServiceDao
     *            the targetPointOfServiceDao to set
     */
    @Required
    public void setTargetPointOfServiceDao(final TargetPointOfServiceDao targetPointOfServiceDao) {
        this.targetPointOfServiceDao = targetPointOfServiceDao;
    }


}
