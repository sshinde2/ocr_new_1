/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.strategies.SubmitOrderStrategy;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.event.TargetSubmitOrderEvent;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;



public class TargetEventPublishingSubmitOrderStrategyImpl implements SubmitOrderStrategy {

    private EventService eventService;

    private ModelService modelService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void submitOrder(final OrderModel order) {
        final PaymentTransactionEntryModel ptem = getPaymentTransactionEntry(order);
        setOrderStatusForBasedOnReviewPte(order, ptem);
        eventService.publishEvent(new TargetSubmitOrderEvent(order, ptem,
                targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)));
    }

    /**
     * This will find the current payment entry - it will have REVIEW status and CAPTURE type
     * 
     * @param order
     * @return PaymentTransactionEntryModel for current payment
     */
    private PaymentTransactionEntryModel getPaymentTransactionEntry(final OrderModel order) {
        PaymentTransactionEntryModel paymentTransactionEntry = null;
        final List<PaymentTransactionModel> paymentTransactions = order.getPaymentTransactions();
        for (final PaymentTransactionModel transaction : paymentTransactions) {
            for (final PaymentTransactionEntryModel entry : transaction.getEntries()) {
                if (TransactionStatus.REVIEW.toString().equals(entry.getTransactionStatus())
                        && PaymentTransactionType.CAPTURE.equals(entry.getType())) {
                    paymentTransactionEntry = entry;
                    break;
                }
            }
        }
        return paymentTransactionEntry;
    }

    private void setOrderStatusForBasedOnReviewPte(final OrderModel order, final PaymentTransactionEntryModel ptem) {
        // Set order status depending on whether there is a payment in REVIEW status       
        if (ptem == null) {
            order.setStatus(OrderStatus.CREATED);
        }
        else {
            order.setStatus(OrderStatus.PENDING);
        }
        modelService.save(order);
    }

    /**
     * @param eventService
     *            the eventService to set
     */
    @Required
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}
