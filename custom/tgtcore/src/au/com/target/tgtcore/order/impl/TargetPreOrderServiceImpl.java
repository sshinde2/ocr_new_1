/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author gsing236
 *
 */
public class TargetPreOrderServiceImpl implements TargetPreOrderService {

    private TargetStockService stockService;

    private TargetSharedConfigService targetSharedConfigService;

    private TargetSalesApplicationService targetSalesApplicationService;

    private TargetOrderDao targetOrderDao;

    @Override
    public int getAvailablePreOrderQuantityForCurrentProduct(final ProductModel product) {
        return stockService.getStockLevelAmountFromPreOrderWarehouses(product);

    }

    @Override
    public boolean doesCartHavePreOrderProduct(final CartModel cartModel) {
        if (cartModel == null) {
            return false;
        }

        if (getPreOrderProductFromCart(cartModel) != null) {
            return true;
        }

        return false;
    }

    @Override
    public void updatePreOrderDetails(final CartModel cartModel) {

        final ProductModel productModel = getPreOrderProductFromCart(cartModel);

        if (productModel != null) {
            cartModel.setContainsPreOrderItems(Boolean.TRUE);
            final double preOrderDeposit = targetSharedConfigService.getDouble(
                    TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT, 10);
            cartModel.setPreOrderDepositAmount(Double.valueOf(preOrderDeposit));
            cartModel.setNormalSaleStartDateTime(TargetProductUtils.getNormalSaleStartDateTime(productModel));
        }

        else {
            cartModel.setContainsPreOrderItems(Boolean.FALSE);
            cartModel.setPreOrderDepositAmount(Double.valueOf(0d));
            cartModel.setNormalSaleStartDateTime(null);
        }

    }


    /**
     * Method will return pre-order product details if cart has an pre-order item in it or else return null.
     *
     * @param cartModel
     * @return ProductModel
     */
    private ProductModel getPreOrderProductFromCart(final CartModel cartModel) {
        final List<AbstractOrderEntryModel> cartEntries = cartModel.getEntries();
        if (CollectionUtils.isNotEmpty(cartEntries)) {
            for (final AbstractOrderEntryModel cartEntry : cartEntries) {
                if (TargetProductUtils.isProductOpenForPreOrder(cartEntry.getProduct())) {
                    return cartEntry.getProduct();
                }
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.order.TargetPreOrderService#getProductDisplayType(au.com.target.tgtcore.model.AbstractTargetVariantProductModel)
     */
    @Override
    public ProductDisplayType getProductDisplayType(final AbstractTargetVariantProductModel targetVariantProduct) {

        final TargetColourVariantProductModel colourVariant = TargetProductUtils
                .getColourVariantProduct(targetVariantProduct);
        if (BooleanUtils.isTrue(colourVariant.getPreview())) {
            return ProductDisplayType.COMING_SOON;
        }
        if (TargetProductUtils.isProductEmbargoed(colourVariant)) {
            if (TargetProductUtils.isProductOpenForPreOrder(colourVariant)
                    && !SalesApplication.KIOSK.equals(targetSalesApplicationService.getCurrentSalesApplication())) {
                return ProductDisplayType.PREORDER_AVAILABLE;
            }
            return ProductDisplayType.COMING_SOON;
        }
        return ProductDisplayType.AVAILABLE_FOR_SALE;
    }

    /**
     * @param stockService
     *            the stockService to set
     */
    @Required
    public void setStockService(final TargetStockService stockService) {
        this.stockService = stockService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */

    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

    /**
     * @return the targetOrderDao
     */
    public TargetOrderDao getTargetOrderDao() {
        return targetOrderDao;
    }

    /**
     * @param targetOrderDao
     *            the targetOrderDao to set
     */
    @Required
    public void setTargetOrderDao(final TargetOrderDao targetOrderDao) {
        this.targetOrderDao = targetOrderDao;
    }

    @Override
    public List<OrderModel> getParkedPreOrdersForReleaseDate(final Date releaseDate,
            final int daysToAdvance) {

        final Date normalSalesStartDate = getNormalSalesStartDate(releaseDate, daysToAdvance);
        return getTargetOrderDao().findOrdersBeforeNormalSalesStartDate(normalSalesStartDate, OrderStatus.PARKED);

    }

    /**
     * @param executionDate
     * @param daysToAdvance
     * @return normalSalesStartDate
     */
    private Date getNormalSalesStartDate(final Date executionDate, final int daysToAdvance) {

        final Date advancedDate = TargetDateUtil.addDays(executionDate, daysToAdvance);
        final Date normalSalesStartDate = TargetDateUtil.isWeekDay(advancedDate)
                ? advancedDate : TargetDateUtil.getNextWeekDate(advancedDate);

        return TargetDateUtil.getLastMilliSecondOfDate(normalSalesStartDate);

    }

}
