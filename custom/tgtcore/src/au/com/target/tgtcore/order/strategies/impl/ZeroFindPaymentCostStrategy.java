/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.strategies.calculation.FindPaymentCostStrategy;
import de.hybris.platform.util.PriceValue;


/**
 * 
 */
public class ZeroFindPaymentCostStrategy implements FindPaymentCostStrategy {
    private static final PriceValue ZERO_PRICE = new PriceValue("AUD", 0.0, false);

    /**
     * {@inheritDoc}
     */
    @Override
    public PriceValue getPaymentCost(final AbstractOrderModel order) {
        return ZERO_PRICE;
    }
}
