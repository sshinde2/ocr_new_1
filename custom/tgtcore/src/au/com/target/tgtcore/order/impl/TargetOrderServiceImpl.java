/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.impl.DefaultOrderService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtcore.order.strategies.ReplaceOrderDenialStrategy;
import au.com.target.tgtcore.order.strategies.UpdateAddressDenialStrategy;
import au.com.target.tgtcore.util.OrderHistoryHelper;


/**
 * @author asingh78
 * 
 */
public class TargetOrderServiceImpl extends DefaultOrderService implements TargetOrderService {

    protected static final String ADDRESS_UPDATE_NOTE = "Updated Delivery Address";

    private TargetOrderDao targetOrderDao;

    private DeliveryService deliveryService;

    private OrderHistoryHelper orderHistoryHelper;

    private List<UpdateAddressDenialStrategy> updateAddressDenialStrategies;

    private List<ReplaceOrderDenialStrategy> replaceOrderDenialStrategies;

    @Override
    public OrderModel findOrderModelForOrderId(final String orderId) {
        return getTargetOrderDao().findOrderModelForOrderId(orderId);
    }

    @Override
    public CartModel findCartModelForOrderId(final String orderId) {
        return getTargetOrderDao().findCartModelForOrderId(orderId);
    }

    @Override
    public OrderModel createOrderFromCart(final CartModel cart) throws InvalidCartException {
        final OrderModel order = super.createOrderFromCart(cart);
        order.setInitialDeliveryCost(order.getDeliveryCost());
        return order;
    }

    @Override
    public boolean isUpdateDeliveryAddressPossible(final OrderModel orderModel) {
        for (final UpdateAddressDenialStrategy uads : updateAddressDenialStrategies) {
            if (uads.isDenied(orderModel)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean updateDeliveryAddress(final OrderModel orderModel, final AddressModel addressModel) {
        validateParameterNotNull(orderModel, "Order model cannot be null");
        validateParameterNotNull(addressModel, "Address model cannot be null");
        getModelService().refresh(orderModel);

        final UserModel user = orderModel.getUser();
        getModelService().refresh(user);

        // Check that the update is allowed and the address model belongs to the same user as the cart
        if (isUpdateDeliveryAddressPossible(orderModel)
                && isValidDeliveryAddress(orderModel, addressModel)) {
            orderHistoryHelper.createHistorySnapshot(orderModel, addressModel, ADDRESS_UPDATE_NOTE);

            orderModel.setDeliveryAddress(addressModel);
            getModelService().save(orderModel);

            if (addressModel != null && !Boolean.TRUE.equals(addressModel.getShippingAddress())) {
                // Flag the address as a delivery address
                addressModel.setShippingAddress(Boolean.TRUE);
                getModelService().save(addressModel);
            }

            getModelService().refresh(orderModel);

            getTargetBusinessProcessService().startPOSAddressChangeProcess(orderModel);

            return true;
        }

        return false;
    }

    @Override
    public String createUniqueKeyForPaymentInitiation(final CartModel cartModel) {
        final String uniqueKey = getUniqueKey();

        // save the unique key in the cart
        cartModel.setUniqueKeyForPayment(uniqueKey);
        getModelService().save(cartModel);

        return cartModel.getUniqueKeyForPayment();
    }

    @Override
    public void updateOwner(final OrderModel orderModel, final UserModel userModel) {
        validateParameterNotNull(orderModel, "Order model cannot be null");
        validateParameterNotNull(userModel, "User model cannot be null");
        getModelService().refresh(orderModel);
        orderModel.setUser(userModel);
        getModelService().save(orderModel);
    }

    /**
     * 
     * @param orderModel
     * @return a boolean to indicate whether replace order is possible
     */
    @Override
    public boolean isReplacePossible(final OrderModel orderModel) {
        for (final ReplaceOrderDenialStrategy rods : replaceOrderDenialStrategies) {
            if (rods.isDenied(orderModel)) {
                return false;
            }
        }

        return true;

    }

    @Override
    public List<OrderModel> findOrdersForPartnerOrderIdAndSalesChannel(final String partnerOrderId,
            final String salesChannel) {
        return getTargetOrderDao().findOrdersForPartnerOrderIdAndSalesChannel(partnerOrderId, salesChannel);
    }

    @Override
    public OrderModel findLatestOrderForUser(final UserModel userModel) {
        return targetOrderDao.findLatestOrderForUser(userModel);
    }

    @Override
    public OrderModel findOrderByFluentId(final String fluentId) {
        return targetOrderDao.findOrderByFluentId(fluentId);
    }

    @Override
    public List<OrderModel> findPendingPreOrdersForProduct(final String productCode) {
        return getTargetOrderDao().findPendingPreOrdersForProduct(productCode);
    }

    /**
     * Get unique key
     * 
     * @return unique key
     */
    protected String getUniqueKey() {
        String uniqueKey = null;
        final SecureRandom secureRandom;
        final MessageDigest sha;
        final Random random;
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
            sha = MessageDigest.getInstance("SHA-1");
            final String randomNum = String.valueOf(secureRandom.nextInt());
            final byte[] result = sha.digest(randomNum.getBytes());
            uniqueKey = String.valueOf(Hex.encodeHex(result));
        }
        catch (final NoSuchAlgorithmException e) {
            // if secure key cannot be created fall back to a random number
            random = new Random();
            uniqueKey = String.valueOf(random.nextInt());
        }
        return uniqueKey;
    }

    protected boolean isValidDeliveryAddress(final OrderModel orderModel, final AddressModel addressModel) {
        if (addressModel != null && !isDeliveryToStore(orderModel)) {
            final List<AddressModel> supportedAddresses = deliveryService
                    .getSupportedDeliveryAddressesForOrder(orderModel, false);
            return supportedAddresses != null && supportedAddresses.contains(addressModel);
        }
        else {
            return true;
        }
    }

    private boolean isDeliveryToStore(final OrderModel orderModel) {
        if (orderModel.getDeliveryMode() != null
                && orderModel.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel deliveryMode = (TargetZoneDeliveryModeModel)orderModel.getDeliveryMode();
            return BooleanUtils.isTrue(deliveryMode.getIsDeliveryToStore());
        }
        return false;
    }

    /**
     * @return the targetOrderDao
     */
    protected TargetOrderDao getTargetOrderDao() {
        return targetOrderDao;
    }

    /**
     * @param targetOrderDao
     *            the targetOrderDao to set
     */
    @Required
    public void setTargetOrderDao(final TargetOrderDao targetOrderDao) {
        this.targetOrderDao = targetOrderDao;
    }

    /**
     * @param deliveryService
     *            the deliveryService to set
     */
    @Required
    public void setDeliveryService(final DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    /**
     * @param orderHistoryHelper
     *            the orderHistoryHelper to set
     */
    @Required
    public void setOrderHistoryHelper(final OrderHistoryHelper orderHistoryHelper) {
        this.orderHistoryHelper = orderHistoryHelper;
    }

    /**
     * @param updateAddressDenialStrategies
     *            the updateAddressDenialStrategies to set
     */
    @Required
    public void setUpdateAddressDenialStrategies(
            final List<UpdateAddressDenialStrategy> updateAddressDenialStrategies) {
        this.updateAddressDenialStrategies = updateAddressDenialStrategies;
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * 
     * @param replaceOrderDenialStrategies
     *            the replaceOrderDenialStrategy to set
     */
    @Required
    public void setReplaceOrderDenialStrategies(final List<ReplaceOrderDenialStrategy> replaceOrderDenialStrategies) {
        this.replaceOrderDenialStrategies = replaceOrderDenialStrategies;
    }

}
