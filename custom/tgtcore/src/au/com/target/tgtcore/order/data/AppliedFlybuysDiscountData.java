/**
 * 
 */
package au.com.target.tgtcore.order.data;

/**
 * @author vivek Represent data of Flybuys Discount that has been applied to an order.
 */
public class AppliedFlybuysDiscountData extends AppliedVoucherData {
    private Integer points;

    private String redeemCode;

    private String confirmationCode;

    /**
     * @return the points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(final Integer points) {
        this.points = points;
    }

    /**
     * @return the redeemCode
     */
    public String getRedeemCode() {
        return redeemCode;
    }

    /**
     * @param redeemCode
     *            the redeemCode to set
     */
    public void setRedeemCode(final String redeemCode) {
        this.redeemCode = redeemCode;
    }

    /**
     * @return the confirmationCode
     */
    public String getConfirmationCode() {
        return confirmationCode;
    }

    /**
     * @param confirmationCode
     *            the confirmationCode to set
     */
    public void setConfirmationCode(final String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }
}
