/**
 * 
 */
package au.com.target.tgtcore.order;

/**
 * @author bpottass
 *
 */
public class FluentOrderException extends RuntimeException {

    public FluentOrderException() {
        super();
    }

    public FluentOrderException(final String message) {
        super(message);
    }

    public FluentOrderException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
