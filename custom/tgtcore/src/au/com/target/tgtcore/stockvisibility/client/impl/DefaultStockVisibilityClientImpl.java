/**
 * 
 */
package au.com.target.tgtcore.stockvisibility.client.impl;

import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;


/**
 * @author rmcalave
 *
 */
public class DefaultStockVisibilityClientImpl implements StockVisibilityClient {

    private static final Logger LOG = Logger.getLogger(DefaultStockVisibilityClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient#lookupStockForItemsInStores(java.util.List, java.util.List)
     */
    @Override
    public StockVisibilityItemLookupResponseDto lookupStockForItemsInStores(final List<String> storeNumbers,
            final List<String> itemcodes) {
        LOG.info(
                "lookupStockForItemsInStores(List<String>, List<String>) default implementation");
        return new StockVisibilityItemLookupResponseDto();
    }

}
