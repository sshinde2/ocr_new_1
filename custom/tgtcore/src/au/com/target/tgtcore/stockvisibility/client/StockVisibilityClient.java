/**
 * 
 */
package au.com.target.tgtcore.stockvisibility.client;

import java.util.List;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;


/**
 * @author rmcalave
 *
 */
public interface StockVisibilityClient {
    /**
     * Lookup the available stock for the provided products in the provided stores.
     * 
     * @param storeNumbers
     *            a List of store numbers
     * @param itemcodes
     *            a list of items
     * @return the stock responses for the stores and items
     */
    StockVisibilityItemLookupResponseDto lookupStockForItemsInStores(List<String> storeNumbers,
            List<String> itemcodes);
}
