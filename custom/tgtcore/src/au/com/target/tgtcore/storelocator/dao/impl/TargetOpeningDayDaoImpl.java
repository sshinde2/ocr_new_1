/**
 * 
 */
package au.com.target.tgtcore.storelocator.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.storelocator.dao.TargetOpeningDayDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author rmcalave
 *
 */
public class TargetOpeningDayDaoImpl extends DefaultGenericDao<TargetOpeningDayModel> implements TargetOpeningDayDao {

    private static final Logger LOG = Logger.getLogger(TargetOpeningDayDaoImpl.class);

    public TargetOpeningDayDaoImpl() {
        super(TargetOpeningDayModel._TYPECODE);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.storelocator.dao.TargetOpeningDayDao#findTargetOpeningDayByScheduleAndId(de.hybris.platform.storelocator.model.OpeningScheduleModel, java.lang.String)
     */
    @Override
    public TargetOpeningDayModel findTargetOpeningDayByScheduleAndId(final OpeningScheduleModel schedule,
            final String id) throws TargetAmbiguousIdentifierException {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(TargetOpeningDayModel.TARGETOPENINGDAYID, id);
        params.put(TargetOpeningDayModel.OPENINGSCHEDULE, schedule);

        final List<TargetOpeningDayModel> targetOpeningDays = find(params);

        try {
            TargetServicesUtil.validateIfSingleResult(targetOpeningDays, TargetOpeningDayModel.class,
                    TargetOpeningDayModel.TARGETOPENINGDAYID, id);

            return targetOpeningDays.get(0);
        }
        catch (final TargetUnknownIdentifierException ex) {
            return null;
        }
        catch (final TargetAmbiguousIdentifierException ex) {
            LOG.warn("More than one " + TargetOpeningDayModel._TYPECODE + " found for id '" + id
                    + "' and schedule with code '" + schedule.getCode() + "'");
            throw ex;
        }
    }
}
