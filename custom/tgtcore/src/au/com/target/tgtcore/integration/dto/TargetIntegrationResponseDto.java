/**
 * 
 */
package au.com.target.tgtcore.integration.dto;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author rsamuel3
 *
 */
public class TargetIntegrationResponseDto {
    private boolean success;
    private String responseCode;
    private List<TargetIntegrationErrorDto> errorList;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the errorList
     */
    public List<TargetIntegrationErrorDto> getErrorList() {
        return errorList;
    }

    /**
     * @param errorList
     *            the errorList to set
     */
    public void setErrorList(final List<TargetIntegrationErrorDto> errorList) {
        this.errorList = errorList;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        final StringBuilder response = new StringBuilder();
        response.append("success=").append(success).append(", responseCode=").append(responseCode);
        if (CollectionUtils.isNotEmpty(getErrorList())) {
            for (final TargetIntegrationErrorDto errorCode : getErrorList()) {
                response.append(errorCode).append(",");
            }
        }
        return response.toString();
    }
}
