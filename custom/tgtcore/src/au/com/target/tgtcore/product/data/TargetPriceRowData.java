/**
 * 
 */
package au.com.target.tgtcore.product.data;

import de.hybris.platform.util.StandardDateRange;


/**
 * DTO for target price row for use when updating product prices.
 * 
 */
public class TargetPriceRowData {

    private double sellPrice;

    private double wasPrice;

    private StandardDateRange dateRange;

    private boolean promoEvent;

    /**
     * @return the sellPrice
     */
    public double getSellPrice() {
        return sellPrice;
    }

    /**
     * @param sellPrice
     *            the sellPrice to set
     */
    public void setSellPrice(final double sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * @return the wasPrice
     */
    public double getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final double wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @return the dateRange
     */
    public StandardDateRange getDateRange() {
        return dateRange;
    }

    /**
     * @param dateRange
     *            the dateRange to set
     */
    public void setDateRange(final StandardDateRange dateRange) {
        this.dateRange = dateRange;
    }

    /**
     * @return the promoEvent
     */
    public boolean isPromoEvent() {
        return promoEvent;
    }

    /**
     * @param promoEvent
     *            the promoEvent to set
     */
    public void setPromoEvent(final boolean promoEvent) {
        this.promoEvent = promoEvent;
    }




}
