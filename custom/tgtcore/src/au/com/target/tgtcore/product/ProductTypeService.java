package au.com.target.tgtcore.product;

import au.com.target.tgtcore.model.ProductTypeModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

/**
 * Service that works with {@link ProductTypeModel}.
 */
public interface ProductTypeService {

    /**
     * Returns the product type for a given {@code code}.
     *
     * @param code the product type code
     * @return the product type instance
     * @throws ModelNotFoundException if no record found
     * @throws AmbiguousIdentifierException if multiple records found
     */
    ProductTypeModel getByCode(String code) throws ModelNotFoundException, AmbiguousIdentifierException;
}
