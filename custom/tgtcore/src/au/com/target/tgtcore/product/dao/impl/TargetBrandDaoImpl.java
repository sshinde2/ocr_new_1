package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.dao.BrandDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * Default implementation of {@link BrandDao}
 */
public class TargetBrandDaoImpl extends DefaultGenericDao implements BrandDao {


    public TargetBrandDaoImpl() {
        super(BrandModel._TYPECODE);
    }

    @Override
    public BrandModel getBrandByCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);
        final Map<String, String> params = new HashMap<String, String>();
        params.put(BrandModel.CODE, code);
        final List<BrandModel> brands = find(params);
        TargetServicesUtil.validateIfSingleResult(brands, BrandModel.class, BrandModel.CODE, code);

        return brands.get(0);
    }

}
