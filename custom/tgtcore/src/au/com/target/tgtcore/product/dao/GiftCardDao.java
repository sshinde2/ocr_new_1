/**
 * 
 */
package au.com.target.tgtcore.product.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.GiftCardModel;


/**
 * @author smishra1
 *
 */
public interface GiftCardDao {
    /**
     * @param brandId
     * @return {@link GiftCardModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    GiftCardModel getGiftCardByBrandId(String brandId) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

}
