/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;


/**
 * @author rmcalave
 * 
 */
public class TargetProductDaoImpl extends DefaultProductDao implements TargetProductDao {

    private static final String ONLINE_DATE_ELIGIBLE_PRODUCTS_QUERY = "SELECT DISTINCT p.code " +
            "FROM products p " +
            "WHERE p.typepkstring IN (SELECT pk " +
            "    FROM composedtypes " +
            "    WHERE internalcode = 'TargetColourVariantProduct') " +
            "AND p.p_catalogversion = (SELECT cv.pk " +
            "    FROM CatalogVersions cv, Catalogs c " +
            "    WHERE cv.p_catalog = c.pk " +
            "    AND c.p_id = 'targetProductCatalog' " +
            "    AND cv.p_version = 'Staged') " +
            "AND p_approvalstatus = (SELECT pk " +
            "    FROM enumerationvalues " +
            "    WHERE typepkstring = (SELECT pk " +
            "        FROM composedtypes " +
            "        WHERE internalcode = 'ArticleApprovalStatus') " +
            "    AND code = 'approved') " +
            "AND p_onlinedate IS NULL " +
            "AND EXISTS (SELECT pr.pk " +
            "    FROM pricerows pr, FLUENT_STOCK_LEVELS fsl " +
            "    WHERE pr.p_product = p.pk " +
            "    AND fsl.skuRef = p.code " +
            "    AND pr.p_starttime IS NULL " +
            "    AND pr.p_endtime IS NULL " +
            "    AND (fsl.atsHD > 0 OR fsl.atsCC > 0 OR fsl.atsEd > 0) " +
            "    UNION " +
            "    SELECT pr.pk " +
            "    FROM pricerows pr, products sv, FLUENT_STOCK_LEVELS fsl " +
            "    WHERE pr.p_product = sv.pk " +
            "    AND sv.typepkstring IN (SELECT pk " +
            "        FROM composedtypes " +
            "        WHERE internalcode = 'TargetSizeVariantProduct') " +
            "    AND fsl.skuRef = sv.code " +
            "    AND sv.p_baseproduct = p.pk " +
            "    AND pr.p_starttime IS NULL " +
            "    AND pr.p_endtime IS NULL " +
            "    AND (fsl.atsHD > 0 OR fsl.atsCC > 0 OR fsl.atsEd > 0))";

    private JdbcTemplate jdbcTemplate;

    public TargetProductDaoImpl(final String typecode) {
        super(typecode);
    }

    @Override
    public List<ProductModel> findOnlineProductsByPrimarySuperCategory(final CategoryModel category) {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT {p:" + TargetProductModel.PK + "} FROM {" + TargetProductModel._TYPECODE);
        query.append(" AS p JOIN " + CategoryModel._TYPECODE + " AS c ON {c:" + CategoryModel.PK + "} = {p:"
                + TargetProductModel.PRIMARYSUPERCATEGORY + "}}");
        query.append(" WHERE {c:" + CategoryModel.PK + "} = ?" + CategoryModel._TYPECODE);

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(CategoryModel._TYPECODE, category);

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(ProductModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<TargetProductModel> findBulkyBoardProducts(final CatalogVersionModel catalogVersion) {
        Validate.notNull(catalogVersion, "catalogVersion must not be null");

        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {p:" + TargetProductModel.PK + "}");
        queryString.append(" FROM {" + TargetProductModel._TYPECODE + " as p}");
        queryString.append(" WHERE {p:" + TargetProductModel.BULKYBOARDPRODUCT + "} = ?bulkyBoardProduct");
        queryString.append(" AND {p:" + TargetProductModel.CATALOGVERSION + "} = ?catalogVersion");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString());
        query.addQueryParameter("catalogVersion", catalogVersion);
        query.addQueryParameter("bulkyBoardProduct", Boolean.TRUE);

        final SearchResult<TargetProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetColourVariantProductModel> findProductByMerchDepartment(
            final TargetMerchDepartmentModel targetMerchDepartmentModel,
            final int start, final int count) {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT {variantProducts:" + TargetColourVariantProductModel.PK + "}");
        query.append(" FROM {" + TargetColourVariantProductModel._TYPECODE + " AS variantProducts JOIN "
                + TargetProductModel._TYPECODE + " AS products ");
        query.append(" ON {variantProducts:"
                + TargetColourVariantProductModel.BASEPRODUCT + "}={products:" + TargetProductModel.PK + "}");
        query.append(" JOIN "
                + TargetMerchDepartmentModel._TYPECODE + " AS dep");
        query.append(" ON {products:" + TargetProductModel.MERCHDEPARTMENT + "} = {dep:"
                + TargetMerchDepartmentModel.PK + "} }");
        query.append(" WHERE {dep:" + TargetMerchDepartmentModel.PK + "} = ?" + TargetMerchDepartmentModel._TYPECODE);

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(TargetMerchDepartmentModel._TYPECODE, targetMerchDepartmentModel);
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(TargetColourVariantProductModel.class));
        searchQuery.setStart(start);
        searchQuery.setCount(count);
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.dao.TargetProductDao#findProductByEan(java.lang.String)
     */
    @Override
    public List<ProductModel> findProductByEan(final String ean) {
        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {p:" + ProductModel.PK + "}");
        queryString.append(" FROM {" + ProductModel._TYPECODE + " as p}");
        queryString.append(" WHERE {p:" + ProductModel.EAN + "} = ?ean");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString());
        query.addQueryParameter("ean", ean);

        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetProductModel> findProductsByOriginalCategoryNull(final CatalogVersionModel catalogversion,
            final int pageSize, final List<String> erroredProducts) {

        final Set<ArticleApprovalStatus> approvalStatus = new HashSet<>(
                Arrays.asList(ArticleApprovalStatus.APPROVED, ArticleApprovalStatus.UNAPPROVED));

        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {p:").append(TargetProductModel.PK).append("}");
        queryString.append(" FROM {").append(TargetProductModel._TYPECODE).append(" as p}");
        queryString.append(" WHERE {p:").append(TargetProductModel.ORIGINALCATEGORY).append("} IS NULL");
        queryString.append(" AND {p:").append(TargetProductModel.CATALOGVERSION).append("} = ?catalogVersion");
        queryString.append(" AND {p:").append(TargetProductModel.APPROVALSTATUS).append("} IN (?approvalStatus) ");
        if (CollectionUtils.isNotEmpty(erroredProducts)) {
            queryString.append(" AND {p:").append(TargetProductModel.CODE).append("} NOT IN (?erroredProducts) ");
        }
        queryString.append(" ORDER BY {p:").append(TargetProductModel.PK).append("}");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(queryString.toString());
        searchQuery.setStart(0);
        searchQuery.setCount(pageSize);
        searchQuery.setNeedTotal(true);
        searchQuery.addQueryParameter("catalogVersion", catalogversion);
        searchQuery.addQueryParameter("approvalStatus", approvalStatus);
        if (CollectionUtils.isNotEmpty(erroredProducts)) {
            searchQuery.addQueryParameter("erroredProducts", erroredProducts);
        }

        final SearchResult<TargetProductModel> result = getFlexibleSearchService().search(searchQuery);
        return result.getResult();
    }

    @Override
    public List<TargetProductModel> findProductsNotSyncedToFluent(final CatalogVersionModel catalogVersion,
            final int pageSize, final List<String> erroredProducts) {
        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {p:").append(TargetProductModel.PK).append("}").append(" FROM {")
                .append(TargetProductModel._TYPECODE).append(" as p LEFT OUTER JOIN ")
                .append(ProductFluentUpdateStatusModel._TYPECODE).append(" AS fus ON {p:")
                .append(TargetProductModel.CODE).append("} = {fus:").append(ProductFluentUpdateStatusModel.CODE)
                .append("}} WHERE ({fus:").append(ProductFluentUpdateStatusModel.CODE).append("} IS NULL OR {fus:")
                .append(ProductFluentUpdateStatusModel.LASTRUNSUCCESSFUL).append("} = 0) AND {p:")
                .append(TargetProductModel.CATALOGVERSION).append("} = ?catalogVersion");
        if (CollectionUtils.isNotEmpty(erroredProducts)) {
            queryString.append(" AND {p:").append(TargetProductModel.CODE).append("} NOT IN (?erroredProducts) ");
        }
        queryString.append(" ORDER BY {p:").append(TargetProductModel.PK).append("}");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(queryString.toString());
        searchQuery.setStart(0);
        searchQuery.setCount(pageSize);
        searchQuery.setNeedTotal(true);
        searchQuery.addQueryParameter("catalogVersion", catalogVersion);
        if (CollectionUtils.isNotEmpty(erroredProducts)) {
            searchQuery.addQueryParameter("erroredProducts", erroredProducts);
        }

        final SearchResult<TargetProductModel> result = getFlexibleSearchService().search(searchQuery);
        return result.getResult();
    }

    @Override
    public List<String> findProductsEligibleForNewOnlineDate() {
        return jdbcTemplate.queryForList(ONLINE_DATE_ELIGIBLE_PRODUCTS_QUERY, String.class);
    }

    /**
     * @param jdbcTemplate
     *            the jdbcTemplate to set
     */
    @Required
    public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
