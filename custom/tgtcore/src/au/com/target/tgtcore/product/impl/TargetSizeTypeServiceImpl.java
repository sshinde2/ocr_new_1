/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtcore.product.dao.TargetSizeTypeDao;


/**
 * The Class TargetSizeTypeServiceImpl.
 * 
 * @author nandini
 */
public class TargetSizeTypeServiceImpl implements TargetSizeTypeService {

    /** The size type dao. */
    private TargetSizeTypeDao sizeTypeDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.TargetSizeTypeService#findSizeTypeForCode(java.lang.String)
     */
    @Override
    public SizeTypeModel findSizeTypeForCode(final String code) {
        return sizeTypeDao.getSizeTypeByCode(code);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.TargetSizeTypeService#getDefaultSizeType()
     */
    @Override
    public SizeTypeModel getDefaultSizeType() {
        return sizeTypeDao.getDefaultSizeType();
    }

    /**
     * Sets the size type dao.
     * 
     * @param sizeTypeDao
     *            the new size type dao
     */
    @Required
    public void setSizeTypeDao(final TargetSizeTypeDao sizeTypeDao) {
        this.sizeTypeDao = sizeTypeDao;
    }
}
