/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;


/**
 * Strategy for determining whether an update to product tax is required
 * 
 */
public class ProductTaxGroupUpdateRequiredStrategy {

    private String ptgPrefix = "aus-gst-";

    /**
     * 
     * @param productModel
     * @param gstRate
     * 
     * @return true if an update to tax is required
     */
    public boolean isTaxGroupUpdateRequired(final ProductModel productModel, final int gstRate) {

        final ProductTaxGroup ptg = productModel.getEurope1PriceFactory_PTG();

        // If current ptg or its code is null then it should be updated
        if (ptg == null || ptg.getCode() == null) {
            return true;
        }

        return !isProductTaxGroupForRate(ptg, gstRate);
    }

    private boolean isProductTaxGroupForRate(final ProductTaxGroup ptg, final int rate) {

        // The tax group is associated with the given rate if the name is like 'prefix-[rate]'
        final boolean match = ptg.getCode().equalsIgnoreCase(ptgPrefix + rate);
        return match;
    }


    /**
     * @param ptgPrefix
     *            the ptgPrefix to set
     */
    public void setPtgPrefix(final String ptgPrefix) {
        this.ptgPrefix = ptgPrefix;
    }


}
