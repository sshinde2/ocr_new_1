package au.com.target.tgtcore.product;

import au.com.target.tgtcore.model.BrandModel;


/**
 * Service to manage {@link BrandModel}
 * 
 */
public interface BrandService {

    /**
     * @param brandName
     * @param createIfMissing
     * @return {@link BrandModel}
     */
    BrandModel getBrandForFuzzyName(String brandName, boolean createIfMissing);

    /**
     * Get the brand for a given code.
     * 
     * @param brandCode
     * @return {@link BrandModel}
     */
    BrandModel getBrandForCode(String brandCode);

}
