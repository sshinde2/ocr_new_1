/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.product.dao.TargetProductMerchDepartmentDao;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;


/**
 * @author pthoma20
 * 
 *         This class updates the department model passed to it and associates target product model with the
 *         department.Returns true if there is an update required.
 * 
 */
public class TargetProductDepartmentServiceImpl implements TargetProductDepartmentService {

    private static final Logger LOG = Logger.getLogger(TargetProductDepartmentServiceImpl.class);

    private CategoryService categoryService;

    private TargetProductMerchDepartmentDao targetProductMerchDepartmentDao;

    /**
     * This method will validate the department found. It will validate whether the product is already having same
     * department- No update required. If the product is having a different department associated - remove existing and
     * associate new department. If the product is having a no department - add the new department.
     * 
     * @param targetProductModel
     * @param targetMerchDepartmentModel
     * @return isUpdateRequired
     */
    @Override
    public boolean processMerchDeparment(final TargetProductModel targetProductModel,
            final TargetMerchDepartmentModel targetMerchDepartmentModel) {
        final Collection<CategoryModel> productSuperCategoriesModified = new ArrayList<>();
        if (null != targetProductModel.getSupercategories()) {
            productSuperCategoriesModified.addAll(targetProductModel.getSupercategories());
        }
        boolean isUpdateRequired = false;
        final TargetMerchDepartmentModel existingMerchDepCatModel = getExistingTargetMerchDep(productSuperCategoriesModified);

        //Check whether there exists a merchant department category
        if (existingMerchDepCatModel != null) {
            //check whether the merchant department category is different then associate to new one
            if (!StringUtils.equalsIgnoreCase(existingMerchDepCatModel.getCode(), targetMerchDepartmentModel.getCode())) {
                productSuperCategoriesModified.remove(existingMerchDepCatModel);
                productSuperCategoriesModified.add(targetMerchDepartmentModel);
                isUpdateRequired = true;
            }
        }
        //Add since there is no merchant department category
        else {
            productSuperCategoriesModified.add(targetMerchDepartmentModel);
            isUpdateRequired = true;
        }

        //Update MerchDepartment attribute
        if ((targetProductModel.getMerchDepartment() == null)
                || (!StringUtils.equalsIgnoreCase(targetProductModel.getMerchDepartment().getCode(),
                        targetMerchDepartmentModel.getCode()))) {
            targetProductModel.setMerchDepartment(targetMerchDepartmentModel);
            isUpdateRequired = true;
        }

        targetProductModel.setSupercategories(productSuperCategoriesModified);
        return isUpdateRequired;
    }

    /**
     * Retrieves the Department Category Model based on the department passed and the catalog version for which it is
     * required
     * 
     * @param productSuperCategories
     * @return existingMerchDepCatModel
     */
    private TargetMerchDepartmentModel getExistingTargetMerchDep(final Collection<CategoryModel> productSuperCategories) {
        TargetMerchDepartmentModel existingMerchDepCatModel = null;
        if (CollectionUtils.isNotEmpty(productSuperCategories)) {
            //Iterate to get the existing merchant department Category
            for (final CategoryModel productSuperCatModel : productSuperCategories) {
                if (productSuperCatModel instanceof TargetMerchDepartmentModel) {
                    existingMerchDepCatModel = (TargetMerchDepartmentModel)productSuperCatModel;
                    break;
                }
            }

        }
        return existingMerchDepCatModel;
    }

    /**
     * Retrieves the Department Category Model
     * 
     * @param catalogVersionModel
     * @param department
     * @return categoryModel
     */
    @Override
    public TargetMerchDepartmentModel getDepartmentCategoryModel(final CatalogVersionModel catalogVersionModel,
            final Integer department) {
        if (department == null) {
            return null;
        }
        TargetMerchDepartmentModel depCatModel = null;
        try {
            final CategoryModel categoryModel = categoryService.getCategoryForCode(
                    catalogVersionModel,
                    department.toString());
            if (categoryModel instanceof TargetMerchDepartmentModel) {
                depCatModel = (TargetMerchDepartmentModel)categoryModel;
            }
        }
        catch (final UnknownIdentifierException e) {
            LOG.error("Invalid Department or Department not Configured in Hybris  department " + department.toString());
            depCatModel = null;
        }
        return depCatModel;
    }

    @Override
    public List<TargetProductCountMerchDepRowData> findProductCountForEachMerchDep() {
        return targetProductMerchDepartmentDao.findProductCountForEachMerchDep();
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param targetProductMerchDepartmentDao
     *            the targetProductMerchDepartmentDao to set
     */
    @Required
    public void setTargetProductMerchDepartmentDao(final TargetProductMerchDepartmentDao targetProductMerchDepartmentDao) {
        this.targetProductMerchDepartmentDao = targetProductMerchDepartmentDao;
    }





}
