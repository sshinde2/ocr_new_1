/**
 * 
 */
package au.com.target.tgtcore.product.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.List;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;



/**
 * @author rmcalave
 * 
 */
public interface TargetProductDao extends ProductDao {

    List<ProductModel> findOnlineProductsByPrimarySuperCategory(CategoryModel category);

    /**
     * Retrieve all of the bulky board products (as indicted by the 'bulkyBoardProduct' flag) from the given catalog
     * version.
     * 
     * @param catalogVersion
     *            The catalog version to retrieve the products from
     * @return All the bulky board products in the given catalog version
     */
    List<TargetProductModel> findBulkyBoardProducts(CatalogVersionModel catalogVersion);

    /**
     * Retrieve the product by merchDepartment with start and count
     * 
     * @param targetMerchDepartmentModel
     * @return List<TargetProductModel>
     */
    List<TargetColourVariantProductModel> findProductByMerchDepartment(
            TargetMerchDepartmentModel targetMerchDepartmentModel,
            int start, int count);

    /**
     * Retrieve products by EAN
     * 
     * @param ean
     * @return TargetProductModel
     */
    List<ProductModel> findProductByEan(String ean);

    /**
     * Retrieve all the products whose original category is NULL and are in staged and approved/unapproved
     * 
     * 
     * @param catalogversion
     * @param pageSize
     * @param erroredProducts
     * @return list of TargetProductModel
     */
    List<TargetProductModel> findProductsByOriginalCategoryNull(CatalogVersionModel catalogversion,
            int pageSize, List<String> erroredProducts);

    /**
     * Retrieve products that haven't been synced to fluent
     * 
     * @param catalogVersion
     * @param pageSize
     * @param erroredProducts
     * @return list of TargetProductModel
     */
    List<TargetProductModel> findProductsNotSyncedToFluent(CatalogVersionModel catalogVersion, int pageSize,
            List<String> erroredProducts);

    /**
     * Retrieve approved colour variant products that have at least one sellable variant with:
     * <ul>
     * <li>active price</li>
     * <li>available to sell as per the feed in from fluent</li>
     * </ul>
     * 
     * @return list of product codes
     */
    List<String> findProductsEligibleForNewOnlineDate();

}