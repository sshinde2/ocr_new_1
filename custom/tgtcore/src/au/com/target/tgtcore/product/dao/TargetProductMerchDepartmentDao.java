/**
 * TargetProductMerchDepartmentDao
 */
package au.com.target.tgtcore.product.dao;

import java.util.List;

import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;


/**
 * @author bhuang3
 * 
 */
public interface TargetProductMerchDepartmentDao {

    /**
     * find the target colour variant product count for each merchDepartment. if the merch dep has no product, it won't
     * be in the list
     * 
     * @return List<TargetProductCountMerchDepRowData>
     */
    List<TargetProductCountMerchDepRowData> findProductCountForEachMerchDep();

}
