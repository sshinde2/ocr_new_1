/**
 * 
 */
package au.com.target.tgtcore.product.data;


/**
 * @author mjanarth
 *
 */
public enum ProductDisplayType {
    COMING_SOON("comingSoon"),

    PREORDER_AVAILABLE("preOrderAvailable"),

    AVAILABLE_FOR_SALE("availableForSale");

    private String type;


    /**
     * @param type
     *            - Product Display type
     */
    private ProductDisplayType(final String type)
    {
        this.type = type;
    }

    /**
     * @return - Product display type
     */
    public String getType()
    {
        return type;
    }
}
