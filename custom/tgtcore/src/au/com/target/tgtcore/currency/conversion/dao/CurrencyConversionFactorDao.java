/**
 * 
 */
package au.com.target.tgtcore.currency.conversion.dao;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.util.Date;

import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;


/**
 * @author Nandini
 *
 */
public interface CurrencyConversionFactorDao {

    /**
     * Method to fetch latest currency factor for the given date
     * 
     * @param date
     * @param source
     * @param target
     * @return CurrencyConversionFactorsModel
     */
    CurrencyConversionFactorsModel findLatestCurrencyFactor(Date date, CurrencyModel source, CurrencyModel target);
}
