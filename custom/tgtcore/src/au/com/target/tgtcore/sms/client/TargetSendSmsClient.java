/**
 * 
 */
package au.com.target.tgtcore.sms.client;

import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;


/**
 * @author mjanarth
 * 
 */
public interface TargetSendSmsClient {


    /**
     * Invoke the rest service exacttarget to send Sms
     * 
     * @param mobileNumber
     * @param message
     * @param timeZone
     * @return TargetSendSmsResponseDto
     */
    TargetSendSmsResponseDto sendSmsNotification(String mobileNumber, String message, String timeZone);

}
