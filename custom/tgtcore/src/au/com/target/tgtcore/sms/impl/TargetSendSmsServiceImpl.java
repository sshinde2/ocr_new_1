/**
 * 
 */
package au.com.target.tgtcore.sms.impl;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.acceleratorservices.process.email.context.EmailContextFactory;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.exceptions.BusinessProcessSmsException;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.sms.TargetSendSmsService;
import au.com.target.tgtcore.sms.client.TargetSendSmsClient;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtutility.format.PhoneFormat;
import au.com.target.tgtutility.util.TimeZoneUtils;


/**
 * @author bhuang3
 * 
 */
public class TargetSendSmsServiceImpl implements TargetSendSmsService {

    private static final String DEFAULT_TIME_ZONE = "+1000";

    private RendererService rendererService;

    private ProcessContextResolutionStrategy contextResolutionStrategy;

    private CMSEmailPageService cmsEmailPageService;

    private TargetPostCodeService targetPostCodeService;

    private TargetSendSmsClient targetSendSmsClient;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    private TargetPointOfServiceService targetPointOfServiceService;

    private String smsMessageToStoreForOpenOrders;

    private static final Logger LOG = Logger.getLogger(TargetSendSmsServiceImpl.class);

    @Override
    public TargetSendSmsResponseDto sendSmsForCncNotification(final BusinessProcessModel process,
            final String frontendTemplateName)
                    throws BusinessProcessSmsException {

        final String mobileNumber = getMobileNumberFromBusinessProcessModel(process);
        if (StringUtils.isEmpty(mobileNumber)) {
            throw new BusinessProcessSmsException("Failed to get mobile number or invalid mobile number from process "
                    + process.getCode());
        }
        final String smsMessage = generateSmsMessageFromVelocityTemplate(process, frontendTemplateName);
        if (StringUtils.isEmpty(smsMessage)) {
            throw new BusinessProcessSmsException("Failed to generate sms message from process "
                    + process.getCode());
        }
        final String timeZone = getTimeZoneFromBusinessProcessModel(process);
        return targetSendSmsClient.sendSmsNotification(mobileNumber, smsMessage,
                timeZone);
    }

    @Override
    public TargetSendSmsResponseDto sendSmsToStoreForOpenOrders(final BusinessProcessModel process)
            throws BusinessProcessSmsException {
        final SendSmsToStoreData smsStoreData = (SendSmsToStoreData)orderProcessParameterHelper
                .getSendSmsToStoreData(process);
        if (null == smsStoreData) {
            throw new BusinessProcessSmsException("No store sms data found for process: " + process.getCode());
        }

        final String phoneNumber = smsStoreData.getMobileNumber();
        final String storeNumber = smsStoreData.getStoreNumber();

        if (StringUtils.isEmpty(phoneNumber) || StringUtils.isEmpty(storeNumber)) {
            throw new BusinessProcessSmsException(
                    String.format(
                            "Invalid data found while sending the sms to store for open orders for process: %s. Got the phoneNumber as: %s and storeNumber as: %s",
                            process.getCode(), phoneNumber, storeNumber));
        }

        final TargetPointOfServiceModel pointOfService;
        try {
            pointOfService = targetPointOfServiceService
                    .getPOSByStoreNumber(Integer.valueOf(storeNumber));
        }
        catch (final NumberFormatException | TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            throw new BusinessProcessSmsException(String.format(
                    "No pointOfService found for storeNumber: %s while running the process: %s ", storeNumber,
                    process.getCode()));
        }

        if (StringUtils.isEmpty(smsMessageToStoreForOpenOrders)) {
            throw new BusinessProcessSmsException("Failed to generate sms message for process: " + process.getCode());
        }

        final String formattedMessage = MessageFormat.format(smsMessageToStoreForOpenOrders, storeNumber,
                String.valueOf(smsStoreData.getOpenOrders()));
        final String storeTimeZone = getTimeZoneFromPOS(pointOfService);

        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("Attempt to send an sms:{0}", formattedMessage));
        }

        return targetSendSmsClient.sendSmsNotification(PhoneFormat.validateAndformatPhoneNumberForSms(phoneNumber),
                formattedMessage, storeTimeZone);
    }

    @Override
    public String generateSmsMessageFromVelocityTemplate(final BusinessProcessModel process,
            final String frontendTemplateName) throws BusinessProcessSmsException {

        final CatalogVersionModel contentCatalogVersion = contextResolutionStrategy.getContentCatalogVersion(
                process);
        if (contentCatalogVersion == null) {
            throw new BusinessProcessSmsException("Error getting contentCatalogVersion for process "
                    + process.getCode());
        }
        final EmailPageModel emailPageModel = cmsEmailPageService.getEmailPageForFrontendTemplate(frontendTemplateName,
                contentCatalogVersion);
        if (emailPageModel == null) {
            throw new BusinessProcessSmsException("Could not retrieve email page model for " + frontendTemplateName
                    + " and process " + process.getCode());
        }
        final EmailPageTemplateModel emailPageTemplateModel = (EmailPageTemplateModel)emailPageModel
                .getMasterTemplate();
        if (emailPageTemplateModel == null) {
            throw new BusinessProcessSmsException("Could not retrieve email page template model for "
                    + frontendTemplateName
                    + " and process " + process.getCode());
        }
        final RendererTemplateModel smsTemplate = emailPageTemplateModel.getHtmlTemplate();
        if (smsTemplate == null) {
            throw new BusinessProcessSmsException("Failed to retrieve sms template");
        }
        final AbstractEmailContext smsContext = getEmailContextFactory().create(process, emailPageModel,
                smsTemplate);
        if (smsContext == null) {
            throw new BusinessProcessSmsException("Failed to create sms context");
        }
        else {
            final StringWriter smsMessage = new StringWriter();
            rendererService.render(smsTemplate, smsContext, smsMessage);
            return smsMessage.toString();
        }
    }

    /**
     * get the order Id from business process model
     * 
     * @param processModel
     * @return orderid if any error return null
     */
    private String getTimeZoneFromBusinessProcessModel(final BusinessProcessModel processModel) {
        if (processModel instanceof OrderProcessModel) {
            final OrderModel orderModel = ((OrderProcessModel)processModel).getOrder();
            if (orderModel != null && orderModel.getDeliveryAddress() != null) {
                final String postCode = orderModel.getDeliveryAddress().getPostalcode();
                if (StringUtils.isNotEmpty(postCode)) {
                    final PostCodeModel postCodeModel = targetPostCodeService.getPostCode(postCode);
                    if (postCodeModel != null && postCodeModel.getTimeZone() != null) {
                        return TimeZoneUtils.formatTimeZoneOffsetForSms(postCodeModel.getTimeZone());
                    }
                }
                final String state = orderModel.getDeliveryAddress().getDistrict();
                if (StringUtils.isNotEmpty(state)) {
                    final TimeZone timeZone = TimeZoneUtils.getTimeZoneFromState(state);
                    if (timeZone != null) {
                        return TimeZoneUtils.formatTimeZoneOffsetForSms(timeZone);
                    }
                }
            }
        }
        return DEFAULT_TIME_ZONE;
    }

    private String getTimeZoneFromPOS(final TargetPointOfServiceModel pos) {
        final AddressModel address = pos.getAddress();
        if (address != null) {
            final String postCode = address.getPostalcode();
            if (StringUtils.isNotEmpty(postCode)) {
                final PostCodeModel postCodeModel = targetPostCodeService.getPostCode(postCode);
                if (postCodeModel != null && postCodeModel.getTimeZone() != null) {
                    return TimeZoneUtils.formatTimeZoneOffsetForSms(postCodeModel.getTimeZone());
                }
            }
            final String state = pos.getAddress().getDistrict();
            if (StringUtils.isNotEmpty(state)) {
                final TimeZone timeZone = TimeZoneUtils.getTimeZoneFromState(state);
                if (timeZone != null) {
                    return TimeZoneUtils.formatTimeZoneOffsetForSms(timeZone);
                }
            }
        }
        return DEFAULT_TIME_ZONE;
    }

    /**
     * get mobile number from business process model
     * 
     * @param processModel
     * @return mobile number if any error or invaild number return null
     */
    private String getMobileNumberFromBusinessProcessModel(final BusinessProcessModel processModel) {
        if (processModel instanceof OrderProcessModel) {
            final OrderModel orderModel = ((OrderProcessModel)processModel).getOrder();
            if (orderModel != null && orderModel.getDeliveryAddress() != null) {
                final String phoneNumber = orderModel.getDeliveryAddress().getPhone1();
                return PhoneFormat.validateAndformatPhoneNumberForSms(phoneNumber);
            }
        }
        return null;
    }

    protected RendererService getRendererService() {
        return rendererService;
    }

    @Required
    public void setRendererService(final RendererService rendererService) {
        this.rendererService = rendererService;
    }

    public EmailContextFactory getEmailContextFactory() {
        throw new UnsupportedOperationException(
                "Please define a <lookup-method> for getEmailContextFactory() in tgtcore-spring.xml");
    }

    @Required
    public void setContextResolutionStrategy(final ProcessContextResolutionStrategy contextResolutionStrategy) {
        this.contextResolutionStrategy = contextResolutionStrategy;
    }

    @Required
    public void setCmsEmailPageService(final CMSEmailPageService cmsEmailPageService) {
        this.cmsEmailPageService = cmsEmailPageService;
    }

    @Required
    public void setTargetPostCodeService(final TargetPostCodeService targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }

    @Required
    public void setTargetSendSmsClient(final TargetSendSmsClient targetSendSmsClient) {
        this.targetSendSmsClient = targetSendSmsClient;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param smsMessageToStoreForOpenOrders
     *            the smsMessageToStoreForOpenOrders to set
     */
    @Required
    public void setSmsMessageToStoreForOpenOrders(final String smsMessageToStoreForOpenOrders) {
        this.smsMessageToStoreForOpenOrders = smsMessageToStoreForOpenOrders;
    }

}
