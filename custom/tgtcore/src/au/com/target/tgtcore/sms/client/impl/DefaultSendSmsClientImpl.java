/**
 * 
 */
package au.com.target.tgtcore.sms.client.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.sms.client.TargetSendSmsClient;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;


/**
 * @author mjanarth
 * 
 */
public class DefaultSendSmsClientImpl implements TargetSendSmsClient {

    private static final Logger LOG = Logger.getLogger(DefaultSendSmsClientImpl.class);


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.exacttarget.client.TargetSendSmsCncNotificationClient#sendSmsNotification(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public TargetSendSmsResponseDto sendSmsNotification(final String mobileNumber, final String message,
            final String timeZone) {
        LOG.info("sendSmsNotification(TargetSendSmsResponseDto) :: default implementation");
        return new TargetSendSmsResponseDto();
    }

}
