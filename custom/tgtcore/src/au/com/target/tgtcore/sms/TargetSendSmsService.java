/**
 * 
 */
package au.com.target.tgtcore.sms;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import au.com.target.tgtbusproc.exceptions.BusinessProcessSmsException;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;


/**
 * @author bhuang3
 * 
 */
public interface TargetSendSmsService {

    /**
     * send Sms Notification
     * 
     * @param process
     * @param frontendTemplateName
     * @throws BusinessProcessSmsException
     * @return responseDto
     */
    TargetSendSmsResponseDto sendSmsForCncNotification(final BusinessProcessModel process,
            final String frontendTemplateName)
            throws BusinessProcessSmsException;

    /**
     * get the rendererTemplate from templateName and generate the sms message from the template
     * 
     * @param process
     * @param frontendTemplateName
     * @return String sms message
     * @throws BusinessProcessSmsException
     */
    String generateSmsMessageFromVelocityTemplate(final BusinessProcessModel process, final String frontendTemplateName)
            throws BusinessProcessSmsException;


    /**
     * send Sms to store with number of open orders awaiting action
     * 
     * @param process
     * @return TargetSendSmsResponseDto
     * @throws BusinessProcessSmsException
     */
    TargetSendSmsResponseDto sendSmsToStoreForOpenOrders(BusinessProcessModel process)
            throws BusinessProcessSmsException;
}
