/**
 * 
 */
package au.com.target.tgtcore.fluent;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import java.util.Collection;
import java.util.Map;


/**
 * @author mgazal
 *
 */
public interface FluentStockService {

    /**
     * @param skuCode
     * @return {@link StockLevelStatus}
     */
    StockLevelStatus getProductOnlineStockStatus(String skuCode);

    /**
     * 
     * @param skuRefs
     * @return map of skuCode to ats values for sku & deliveryMode combination
     */
    Map<String, Integer> lookupPlaceOrderAts(final Collection<String> skuRefs, final String deliveryMode);
}
