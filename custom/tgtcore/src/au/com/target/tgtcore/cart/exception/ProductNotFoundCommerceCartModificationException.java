/**
 * 
 */
package au.com.target.tgtcore.cart.exception;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;


/**
 * Class for Product Commerce Cart Modification exception
 * 
 * @author pthoma20
 */
public class ProductNotFoundCommerceCartModificationException extends CommerceCartModificationException {

    public ProductNotFoundCommerceCartModificationException(final String message)
    {
        super(message);
    }


    public ProductNotFoundCommerceCartModificationException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

}
