/**
 * 
 */
package au.com.target.tgtcore.media.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.impl.DefaultMediaContainerDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.media.TargetMediaContainerDao;


/**
 * @author mgazal
 *
 */
public class TargetMediaContainerDaoImpl extends DefaultMediaContainerDao implements TargetMediaContainerDao {


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.media.TargetMediaContainerDao#findMediaContainersByQualifier(java.lang.String, de.hybris.platform.catalog.model.CatalogVersionModel)
     */
    @Override
    public List<MediaContainerModel> findMediaContainersByQualifier(final String qualifier,
            final CatalogVersionModel catalogVersion) {
        final Map params = new HashMap();
        params.put("qualifier", qualifier);
        params.put("catalogVersion", catalogVersion);

        final StringBuilder builder = new StringBuilder();
        builder.append("SELECT {").append("pk").append("} ");
        builder.append("FROM {").append("MediaContainer").append("} ");
        builder.append("WHERE {").append("qualifier").append("}=?qualifier ");
        builder.append("AND {").append("catalogVersion").append("}=?catalogVersion ");
        builder.append("ORDER BY {").append("pk").append("} ASC");
        final SearchResult result = getFlexibleSearchService().search(builder.toString(), params);
        return result.getResult();
    }

}
