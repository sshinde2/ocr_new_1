/**
 * 
 */
package au.com.target.tgtcore.media;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.MediaContainerDao;

import java.util.List;


/**
 * @author mgazal
 *
 */
public interface TargetMediaContainerDao extends MediaContainerDao {

    /**
     * Find all media containers for given qualifier and catalog version.
     * 
     * @param qualifier
     * @param catalogVersion
     * @return list of {@link MediaContainerModel}
     */
    List<MediaContainerModel> findMediaContainersByQualifier(String qualifier, CatalogVersionModel catalogVersion);
}
