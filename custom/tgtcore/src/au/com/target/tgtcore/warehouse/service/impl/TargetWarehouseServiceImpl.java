/**
 * 
 */
package au.com.target.tgtcore.warehouse.service.impl;

import de.hybris.platform.ordersplitting.impl.DefaultWarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * @author dcwillia
 * 
 */
public class TargetWarehouseServiceImpl extends DefaultWarehouseService implements TargetWarehouseService {

    private static final Logger LOG = Logger.getLogger(TargetWarehouseServiceImpl.class.getName());

    private TargetWarehouseDao targetWarehouseDao;

    @Override
    public WarehouseModel getWarehouseForPointOfService(final TargetPointOfServiceModel pointOfService) {
        ServicesUtil.validateParameterNotNullStandardMessage("pointOfService", pointOfService);
        final List<WarehouseModel> result = getTargetWarehouseDao().getWarehouseForPointOfService(
                pointOfService.getStoreNumber());

        if (CollectionUtils.isEmpty(result)) {
            return null;
        }

        return result.iterator().next();
    }


    @Override
    public WarehouseModel getDefaultOnlineWarehouse() {

        // Assume there is one default warehouse and return it
        WarehouseModel warehouseModel = null;
        final List<WarehouseModel> defWarehouses = getDefWarehouse();
        if (CollectionUtils.isNotEmpty(defWarehouses)) {
            warehouseModel = defWarehouses.get(0);
        }
        else {
            LOG.error("Default warehouse not found");
        }
        return warehouseModel;
    }

    @Override
    public boolean isStockAdjustmentRequired(final WarehouseModel warehouse) {
        Assert.notNull(warehouse, "Warehouse model can't be null");
        return warehouse.isNeedStockAdjustment();
    }

    @Override
    public WarehouseModel getWarehouseForFluentLocationRef(final String fluentLocationRef)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("fluentLocationRef", fluentLocationRef);
        final List<WarehouseModel> warehouses = getTargetWarehouseDao()
                .getWarehouseForFluentLocationRef(fluentLocationRef);
        TargetServicesUtil.validateIfSingleResult(warehouses, WarehouseModel.class, WarehouseModel.FLUENTLOCATIONREF,
                fluentLocationRef);
        return warehouses.get(0);
    }

    /**
     * @return the targetWarehouseDao
     */
    protected TargetWarehouseDao getTargetWarehouseDao() {
        return targetWarehouseDao;
    }

    @Required
    public void setTargetWarehouseDao(final TargetWarehouseDao targetWarehouseDao) {
        this.targetWarehouseDao = targetWarehouseDao;
        super.setWarehouseDao(targetWarehouseDao);
    }

}
