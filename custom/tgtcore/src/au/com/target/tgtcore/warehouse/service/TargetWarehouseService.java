/**
 * 
 */
package au.com.target.tgtcore.warehouse.service;

import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author dcwillia
 * 
 */
public interface TargetWarehouseService extends WarehouseService {

    /**
     * Retrieve the warehouse associated with the provided point of service.
     * 
     * @param pointOfService
     *            The point of service
     * @return The warehouse associated with the point of service
     */
    WarehouseModel getWarehouseForPointOfService(TargetPointOfServiceModel pointOfService);


    /**
     * Get the default warehouse used for online fulfilment.
     * 
     * @return default warehouse
     */
    WarehouseModel getDefaultOnlineWarehouse();

    /**
     * determines whether stock adjustment is needed or not.
     * 
     * @param warehouse
     * @return boolean
     */
    boolean isStockAdjustmentRequired(WarehouseModel warehouse);

    /**
     * Get the warehouse for given fluentLocationRef
     * 
     * @param fluentLocationRef
     * @return {@link WarehouseModel}
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    WarehouseModel getWarehouseForFluentLocationRef(String fluentLocationRef)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;
}
