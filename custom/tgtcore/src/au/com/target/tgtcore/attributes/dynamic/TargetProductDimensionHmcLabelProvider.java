/**
 * 
 */
package au.com.target.tgtcore.attributes.dynamic;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetProductDimensionsModel;


public class TargetProductDimensionHmcLabelProvider implements
        DynamicAttributeHandler<String, TargetProductDimensionsModel> {

    @Override
    public String get(final TargetProductDimensionsModel model) {
        Assert.notNull(model, "model cannot be null");

        final StringBuilder displayLabel = new StringBuilder();
        if (null != model.getDimensionCode()) {
            displayLabel.append("DimensionCode:").append(model.getDimensionCode());
        }
        if (null != model.getWeight()) {
            displayLabel.append("; Weight:").append(model.getWeight().toString());
        }
        if (null != model.getHeight()) {
            displayLabel.append("; Height:").append(model.getHeight().toString());
        }
        if (null != model.getLength()) {
            displayLabel.append("; Length:").append(model.getLength().toString());
        }
        if (null != model.getWidth()) {
            displayLabel.append("; Width:").append(model.getWidth().toString());
        }
        return displayLabel.toString();
    }


    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.AbstractItemModel, java.lang.Object)
     */
    @Override
    public void set(final TargetProductDimensionsModel model, final String value) {
        throw new UnsupportedOperationException();

    }

}
