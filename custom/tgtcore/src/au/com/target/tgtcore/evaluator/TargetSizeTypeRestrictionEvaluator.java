/**
 * 
 */
package au.com.target.tgtcore.evaluator;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetSizeTypeRestrictionModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author nandini
 * 
 */
public class TargetSizeTypeRestrictionEvaluator implements CMSRestrictionEvaluator<TargetSizeTypeRestrictionModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator#evaluate(de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel, de.hybris.platform.cms2.servicelayer.data.RestrictionData)
     */
    @Override
    public boolean evaluate(final TargetSizeTypeRestrictionModel sizeRestrictionModel,
            final RestrictionData restrictionData) {
        if (restrictionData == null) {
            return true;
        }

        if (restrictionData.hasProduct() && restrictionData.getProduct() instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariantProductModel = (TargetSizeVariantProductModel)restrictionData
                    .getProduct();

            // currently comparing with sizeType but will compare with sizeClassification once that is merged.
            return isProductSizeTypeInRestriction(sizeRestrictionModel, sizeVariantProductModel.getSizeType());
        }

        return false;
    }

    /**
     * Checks if is product size type in restriction.
     * 
     * @param sizeRestrictionModel
     *            the size restriction model
     * @param productSize
     *            the size type
     * @return true, if is product size type in restriction
     */
    private boolean isProductSizeTypeInRestriction(final TargetSizeTypeRestrictionModel sizeRestrictionModel,
            final String productSize) {
        for (final SizeTypeModel sizeType : sizeRestrictionModel.getSizes()) {
            if (StringUtils.equals(sizeType.getCode(), productSize)) {
                return true;
            }
        }
        return false;
    }
}