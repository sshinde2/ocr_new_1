/**
 * 
 */
package au.com.target.tgtcore.helper;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author bbaral1
 *
 */

public class ClickAndCollectOrderDetailsHelper {

    /**
     * Return orderStatus for the cnc order.
     * 
     * @param orderModel
     * @return String
     */
    public String getCustomOrderStatus(final OrderModel orderModel) {
        String orderStatus = StringUtils.EMPTY;
        if (isNonCncDeliveryMode(orderModel)) {
            return orderStatus;
        }

        if (isOrderInCreatedStatus(orderModel)) {
            orderStatus = TgtCoreConstants.CncOrderDetailsConstants.ORDER_PLACED;
        }
        else if (isOrderCompletedWithConsignmentPickedUpDate(orderModel)) {
            orderStatus = TgtCoreConstants.CncOrderDetailsConstants.ORDER_COMPLETED;
        }
        else if (isConsignmentInTerminalOrInPackedShippedState(orderModel)
                || isConsignmentInWavedPickedPackedShippedState(orderModel)) {
            orderStatus = TgtCoreConstants.CncOrderDetailsConstants.ORDER_INPROGRESS;
        }
        else if (isConsignmentConfirmedByWarehouse(orderModel)) {
            orderStatus = TgtCoreConstants.CncOrderDetailsConstants.ORDER_CONFIRMED;
        }
        else if (isOrderRefunded(orderModel)) {
            orderStatus = TgtCoreConstants.CncOrderDetailsConstants.ORDER_REFUNDED;
        }
        else if (isOrderReadyForPickup(orderModel)) {
            orderStatus = TgtCoreConstants.CncOrderDetailsConstants.READY_TO_COLLECT;
        }
        return orderStatus;
    }

    /**
     * Method will verify if the delivery mode of the placed order. Return true when mode is click-and-collect.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isCncDeliveryMode(final OrderModel orderModel) {
        return null != orderModel.getDeliveryMode()
                && TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT.equals(orderModel.getDeliveryMode().getCode());
    }

    /**
     * Method will verify to true if consignment is in packed/shipped or in terminal state.
     *
     * @param orderModel
     * @return boolean
     */
    private boolean isConsignmentInTerminalOrInPackedShippedState(final OrderModel orderModel) {
        return isConsignmentInPackedOrShippedStatus(orderModel)
                || isCompleteOrderConsignmentInPackedOrShippedStatus(orderModel)
                || (isConsignmentInTerminalState(orderModel)
                        && isConsignmentInCreatedWavedPickedState(orderModel));
    }

    /**
     * Method will verify if the delivery mode of the placed order. Return true when mode is click-and-collect.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isNonCncDeliveryMode(final OrderModel orderModel) {
        return !isCncDeliveryMode(orderModel);
    }

    /**
     * This check is for cnc order with invoiced status.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isNonCncNonInvoicedOrder(final OrderModel orderModel) {
        return isNonCncDeliveryMode(orderModel)
                || !OrderStatus.INVOICED.equals(orderModel.getStatus());
    }

    /**
     * Method will return true if the Cnc Order status is Created.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isOrderInCreatedStatus(final OrderModel orderModel) {
        return OrderStatus.CREATED.equals(orderModel.getStatus());
    }

    /**
     * Method will return true if the Cnc Order status is In Progress and any of the Consignment status is
     * Created/Confirmed by Warehouse/Sent to Warehouse.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isConsignmentConfirmedByWarehouse(final OrderModel orderModel) {
        return checkOrderAndConsignmentStatusExists(orderModel,
                Arrays.asList(ConsignmentStatus.CREATED, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                        ConsignmentStatus.SENT_TO_WAREHOUSE),
                OrderStatus.INPROGRESS);
    }

    /**
     * This method will return true if Consignment(s) in Packed/Shipped/Cancelled states.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isConsignmentInTerminalState(final OrderModel orderModel) {
        return checkOrderAndConsignmentStatusExists(orderModel,
                Arrays.asList(ConsignmentStatus.PACKED, ConsignmentStatus.SHIPPED),
                OrderStatus.INPROGRESS);
    }

    /**
     * This method will return true if Consignment(s) in Packed/Shipped/Cancelled states.
     *
     * @param orderModel
     * @return boolean
     */
    public boolean isConsignmentInCreatedWavedPickedState(final OrderModel orderModel) {
        return checkOrderAndConsignmentStatusExists(orderModel,
                Arrays.asList(ConsignmentStatus.CREATED, ConsignmentStatus.WAVED, ConsignmentStatus.PICKED),
                OrderStatus.INPROGRESS);
    }

    /**
     * This method will return true if Consignment(s) in Packed/Shipped/Picked/Waved states.
     *
     * @param orderModel
     * @return boolean
     */
    public boolean isConsignmentInWavedPickedPackedShippedState(final OrderModel orderModel) {
        return checkOrderAndConsignmentStatusExists(orderModel,
                Arrays.asList(ConsignmentStatus.WAVED, ConsignmentStatus.PICKED, ConsignmentStatus.PACKED,
                        ConsignmentStatus.SHIPPED),
                OrderStatus.INPROGRESS);
    }

    /**
     * This method will return true if any one of the Consignment(s) is in Packed/Shipped states.
     * 
     * @param orderModel
     * @return boolean
     */
    public boolean isConsignmentInPackedOrShippedStatus(final OrderModel orderModel) {
        return checkOrderAndConsignmentStatusExists(orderModel,
                Arrays.asList(ConsignmentStatus.PACKED, ConsignmentStatus.SHIPPED), OrderStatus.INVOICED);
    }

    /**
     * Method returns true if the order status is Completed and Consignment(s) Picked Up Date is available.
     *
     * @param orderModel
     * @return boolean
     */
    public boolean isOrderCompletedWithConsignmentPickedUpDate(final OrderModel orderModel) {
        if (!OrderStatus.COMPLETED.equals(orderModel.getStatus())) {
            return false;
        }
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        if (CollectionUtils.isEmpty(consignments)) {
            return false;
        }
        for (final ConsignmentModel consignmentModel : consignments) {
            if (consignmentModel instanceof TargetConsignmentModel) {
                final TargetConsignmentModel targetConsignmentModel = (TargetConsignmentModel)consignmentModel;
                if (targetConsignmentModel.getPickedUpDate() != null) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method returns true if the order status is Completed and any of the Consignment(s) with Ready to pick up date
     * available and no Picked Up Date.
     *
     * @param orderModel
     * @return boolean
     */
    public boolean isOrderReadyForPickup(final OrderModel orderModel) {
        if (!OrderStatus.COMPLETED.equals(orderModel.getStatus())) {
            return false;
        }
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        if (CollectionUtils.isEmpty(consignments)) {
            return false;
        }
        for (final ConsignmentModel consignmentModel : consignments) {
            if (consignmentModel instanceof TargetConsignmentModel) {
                final TargetConsignmentModel targetConsignmentModel = (TargetConsignmentModel)consignmentModel;
                if (targetConsignmentModel.getReadyForPickUpDate() != null
                        && targetConsignmentModel.getPickedUpDate() == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Method will return true when OrderStatus is Completed, TotalPrice is Zero, Ready for pick up date available and
     * Order Picked up date is not available.
     *
     * @param orderModel
     * @return boolean
     */
    public boolean isOrderRefunded(final OrderModel orderModel) {
        return isOrderReadyForPickup(orderModel) && isOrderSubTotalZero(orderModel);
    }

    /**
     * Method will check for order total. Will return true if the OrderTotal is Zero.
     * 
     * @param orderModel
     * @return boolean
     */
    private boolean isOrderSubTotalZero(final OrderModel orderModel) {
        final Double subTotal = orderModel.getSubtotal();
        return null != subTotal && 0 == BigDecimal.ZERO.compareTo(BigDecimal.valueOf(subTotal.doubleValue()));
    }

    /**
     * This method will return true if any one of the Consignment(s) is in Packed/Shipped states and ready for pick up
     * date not available for the completed order.
     *
     * @param orderModel
     * @return boolean
     */
    public boolean isCompleteOrderConsignmentInPackedOrShippedStatus(final OrderModel orderModel) {
        return !isOrderReadyForPickup(orderModel) && checkOrderAndConsignmentStatusExists(orderModel,
                Arrays.asList(ConsignmentStatus.PACKED, ConsignmentStatus.SHIPPED), OrderStatus.COMPLETED);
    }

    /**
     * Returns true if retrieved Consignment status order contains required status from the list.
     * 
     * @param orderModel
     * @param consignmentStatusList
     * @param orderStatus
     * @return boolean
     */
    private boolean checkOrderAndConsignmentStatusExists(final OrderModel orderModel,
            final List<ConsignmentStatus> consignmentStatusList, final OrderStatus orderStatus) {
        if (!orderStatus.equals(orderModel.getStatus())) {
            return false;
        }
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        if (CollectionUtils.isEmpty(consignments)) {
            return false;
        }
        for (final ConsignmentModel consignmentModel : consignments) {
            if (consignmentModel instanceof TargetConsignmentModel
                    && consignmentStatusList.contains(consignmentModel.getStatus())) {
                return true;
            }
        }
        return false;
    }

}
