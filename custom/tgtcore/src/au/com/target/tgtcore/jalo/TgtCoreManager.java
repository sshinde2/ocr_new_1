/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcore.jalo;

//CHECKSTYLE:OFF
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.promotions.jalo.AbstractPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.TargetDealResult;
import de.hybris.platform.promotions.jalo.TargetDealWithRewardResult;
import de.hybris.platform.promotions.result.PromotionException;

import java.util.HashMap;
import java.util.Map;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.setup.CoreSystemSetup;


//CHECKSTYLE:ON


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
@SuppressWarnings("deprecation")
public class TgtCoreManager extends GeneratedTgtCoreManager
{
    public static final TgtCoreManager getInstance()
    {
        final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
        return (TgtCoreManager)em.getExtension(TgtCoreConstants.EXTENSIONNAME);
    }


    /**
     * Create a new {@link TargetDealResult} instance.
     * 
     * @param ctx
     *            The session context
     * @param promotion
     *            The {@link AbstractPromotion} that created the {@link PromotionResult}
     * @param order
     *            The {@link AbstractOrder} that the {@link PromotionResult} is related to
     * @param certainty
     *            The certainty of firing in the range 0 to 1. 1.0 is fired, less than 1.0 is could fire.
     * @param instanceCount
     *            The Number of times this deal has been meet in this order.
     * @return the new {@link PromotionResult}
     */
    public TargetDealResult createTargetDealResult(final SessionContext ctx,
            final AbstractPromotion promotion,
            final AbstractOrder order, final float certainty, final int instanceCount) {
        if (promotion == null || order == null || certainty < 0.0F || certainty > 1.0F) {
            throw new PromotionException("Invalid attempt to create a promotion result");
        }

        if (instanceCount < 1) {
            throw new PromotionException("Invalid attempt to create a promotion result");
        }

        final Map parameters = new HashMap();
        parameters.put(TargetDealResult.PROMOTION, promotion);
        parameters.put(TargetDealResult.ORDER, order);
        parameters.put(TargetDealResult.CERTAINTY, Float.valueOf(certainty));
        parameters.put(TargetDealResult.INSTANCECOUNT, Integer.valueOf(instanceCount));
        final TargetDealResult promotionResult = super.createTargetDealResult(ctx, parameters);
        return promotionResult;
    }


    /**
     * This method created the TargetDealwithReward result
     * 
     * @param ctx
     * @param promotion
     * @param order
     * @return TargetDealWithRewardResult
     */

    public TargetDealWithRewardResult createTargetDealWithRewardResult(final SessionContext ctx,
            final AbstractPromotion promotion,
            final AbstractOrder order, final float certainty, final int instCount,
            final int numberOfRewards, final int maxNumberofRewards) {

        if (promotion == null || order == null) {
            throw new PromotionException("Invalid attempt to create a promotion result");
        }
        final Map parameters = new HashMap();
        parameters.put(TargetDealWithRewardResult.PROMOTION, promotion);
        parameters.put(TargetDealWithRewardResult.ORDER, order);
        parameters.put(TargetDealWithRewardResult.NUMBEROFREWARDSSOFAR, Integer.valueOf(numberOfRewards));
        parameters.put(TargetDealWithRewardResult.CERTAINTY, Float.valueOf(certainty));
        parameters.put(TargetDealWithRewardResult.INSTANCECOUNT, Integer.valueOf(instCount));
        parameters.put(TargetDealWithRewardResult.MAXNUMBEROFREWARDS, Integer.valueOf(maxNumberofRewards));
        final TargetDealWithRewardResult promotionResult = super.createTargetDealWithRewardResult(ctx, parameters);
        return promotionResult;
    }


}
