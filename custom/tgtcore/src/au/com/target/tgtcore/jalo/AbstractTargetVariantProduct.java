package au.com.target.tgtcore.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;

import org.apache.log4j.Logger;

import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtutility.util.StackTraceFormatter;



@SuppressWarnings("deprecation")
public abstract class AbstractTargetVariantProduct extends GeneratedAbstractTargetVariantProduct
{
    private static final Logger LOG = Logger.getLogger(AbstractTargetVariantProduct.class);

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    protected void doBeforeRemove(final SessionContext ctx, final java.util.Map removalCtx) {
        //Calling parent method
        super.doBeforeRemove(ctx, removalCtx);
        LOG.error(SplunkLogFormatter.formatMessage(
                "LOGGING FROM JALO-- Removing Product Variant (pk, code): (" + super.getPK().toString() + ","
                        + super.getCode()
                        + ") -->",
                TgtutilityConstants.ErrorCode.ERR_REMOVE_PRODUCT
                , StackTraceFormatter.stackTraceToString(Thread.currentThread().getStackTrace())));
    }
}
