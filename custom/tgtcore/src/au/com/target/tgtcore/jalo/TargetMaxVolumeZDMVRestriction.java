package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.util.ProductUtil;


@SuppressWarnings("deprecation")
public class TargetMaxVolumeZDMVRestriction extends GeneratedTargetMaxVolumeZDMVRestriction {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    /**
     * Evaluates to check if sum of volume of all the products in cart is less than the configured max volume.
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        final BigDecimal totalVolume = getTotalVolumeOfAbstractOrder(abstractOrder);
        if (totalVolume != null && totalVolume.doubleValue() <= getMaxVolumeAsPrimitive()) {
            return true;
        }
        return false;
    }

    /**
     * Evaluates to check if products volume is less than the configured max volume.
     */
    @Override
    public boolean evaluate(final Product product) {
        final ProductModel currentProductModel = getModelService().get(product);
        return evaluate(currentProductModel);
    }

    /**
     * Method to return total volume of all the products in cart is less than the configured max volume.
     * 
     * @param abstractOrder
     * @return maxDimension from various products, null if data is missing or invalid
     */
    private BigDecimal getTotalVolumeOfAbstractOrder(final AbstractOrderModel abstractOrder) {
        BigDecimal totalVolume = BigDecimal.ZERO;
        if (null != abstractOrder && CollectionUtils.isNotEmpty(abstractOrder.getEntries())) {
            for (final AbstractOrderEntryModel orderEntry : abstractOrder.getEntries()) {
                final String productCode = ProductUtil.getProductTypeCode(orderEntry.getProduct());
                if (ProductType.DIGITAL.equals(productCode)) {
                    continue;
                }
                final TargetProductDimensionsModel productPackageDimensions = getProductPackageDimensionsModel(
                        orderEntry.getProduct());
                if (doesProductHaveValidDimensions(productPackageDimensions)) {
                    totalVolume = totalVolume.add(calculateVolume(productPackageDimensions.getLength().doubleValue(),
                            productPackageDimensions.getWidth().doubleValue(), productPackageDimensions.getHeight()
                                    .doubleValue(),
                            orderEntry.getQuantity().doubleValue()));
                }
                else {
                    return null;
                }
            }
            return totalVolume;
        }
        return null;
    }

    /**
     * Method to calculate volume.
     * 
     * @param length
     * @param width
     * @param height
     * @param quantity
     * @return volume - Product of length, width, height and quantity.
     */
    private BigDecimal calculateVolume(final double length, final double width, final double height,
            final double quantity) {
        return BigDecimal.valueOf(length).multiply(BigDecimal.valueOf(width)).multiply(BigDecimal.valueOf(height))
                .multiply(BigDecimal.valueOf(quantity));
    }

    /**
     * Method to evaluate product model and its related variants.
     * 
     * @param currentProductModel
     * @return boolean
     */
    private boolean evaluate(final ProductModel currentProductModel) {
        final Collection<VariantProductModel> productVariants = currentProductModel.getVariants();
        if (CollectionUtils.isEmpty(productVariants)) {
            return evaluateProductsVolume(currentProductModel);
        }
        for (final ProductModel variant : currentProductModel.getVariants()) {
            if (!evaluate(variant)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to evaluate products volume, if dimensions are negative or missing then dimension is invalid and
     * evaluation fails
     * 
     * @param variant
     * @return boolean
     */
    private boolean evaluateProductsVolume(final ProductModel variant) {
        final TargetProductDimensionsModel productPackageDimensions = getProductPackageDimensionsModel(variant);
        if (!doesProductHaveValidDimensions(productPackageDimensions)) {
            return false;
        }
        return getProductsVolume(productPackageDimensions) <= getMaxVolumeAsPrimitive();
    }

    /**
     * Method to fetch TargetProductDimensionsModel
     * 
     * @param product
     * @return TargetProductDimensionsModel
     */
    private TargetProductDimensionsModel getProductPackageDimensionsModel(final ProductModel product) {
        if (product instanceof AbstractTargetVariantProductModel) {
            return ((AbstractTargetVariantProductModel)product).getProductPackageDimensions();
        }
        return null;
    }

    /**
     * Method to fetch products volume
     * 
     * @param productPackageDimensions
     * @return largestDimensionValue
     */
    private double getProductsVolume(final TargetProductDimensionsModel productPackageDimensions) {
        return calculateVolume(productPackageDimensions.getLength().doubleValue(), productPackageDimensions.getWidth()
                .doubleValue(), productPackageDimensions.getHeight().doubleValue(), 1.0).doubleValue();
    }

    /**
     * Method to check if product has valid dimensions, a valid dimension is a positive value.
     * 
     * @param productPackageDimensions
     * @return boolean
     */
    private boolean doesProductHaveValidDimensions(final TargetProductDimensionsModel productPackageDimensions) {
        if (productPackageDimensions != null && isDimensionValid(productPackageDimensions.getLength())
                && isDimensionValid(productPackageDimensions.getWidth())
                && isDimensionValid(productPackageDimensions.getHeight())) {
            return true;
        }
        return false;
    }

    /**
     * Method to check if the dimension is valid, valid is a positive number
     * 
     * @param dimension
     * @return boolean
     */
    private boolean isDimensionValid(final Double dimension) {
        if (ObjectUtils.compare(dimension, Double.valueOf(0.0)) < 0) {
            return false;
        }
        return true;
    }

    protected ModelService getModelService() {
        return this.modelService;
    }
}
