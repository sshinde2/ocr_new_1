package au.com.target.tgtcore.jalo;

//CHECKSTYLE:OFF OrderRestriction is used in comment link, but not in code
import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.voucher.jalo.OrderRestriction;
import de.hybris.platform.voucher.jalo.util.VoucherEntry;
import de.hybris.platform.voucher.jalo.util.VoucherEntrySet;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.util.RestrictionUtils;


//CHECKSTYLE:ON

@SuppressWarnings("deprecation")
public class TargetOrderRestriction extends GeneratedTargetOrderRestriction
{
    private static final Logger LOG = Logger.getLogger(TargetOrderRestriction.class);

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /**
     * Adapted from {@link OrderRestriction#isFulfilledInternal(AbstractOrder)}
     * 
     * @see de.hybris.platform.voucher.jalo.OrderRestriction#isFulfilledInternal(de.hybris.platform.jalo.order.AbstractOrder)
     */
    @Override
    protected boolean isFulfilledInternal(final AbstractOrder anOrder) {
        final Currency minimumOrderValueCurrency = getCurrency();
        final Currency currentOrderCurrency = anOrder.getCurrency();
        if (minimumOrderValueCurrency == null || currentOrderCurrency == null)
        {
            return false;
        }
        final double minimumTotal = minimumOrderValueCurrency.convert(currentOrderCurrency, getTotalAsPrimitive());
        try
        {
            anOrder.calculateTotals(false);
        }
        catch (final JaloPriceFactoryException e)
        {
            LOG.error("Failed to calculate totals for order while checking voucher eligibility", e);
        }

        final VoucherEntrySet applicableEntries = getApplicableEntriesInternal(anOrder);
        BigDecimal currentTotal = BigDecimal.ZERO;

        for (final Iterator<VoucherEntry> i = applicableEntries.iterator(); i.hasNext();) {
            final VoucherEntry entry = i.next();
            currentTotal = currentTotal.add(BigDecimal.valueOf(entry.getOrderEntry().getTotalPriceAsPrimitive()));
        }

        // we have to add the shipping and payment costs ?
        if (!isValueofgoodsonlyAsPrimitive())
        {
            currentTotal = currentTotal.add(BigDecimal.valueOf(anOrder.getDeliveryCosts()));
            currentTotal = currentTotal.add(BigDecimal.valueOf(anOrder.getPaymentCosts()));
        }

        if (isPositiveAsPrimitive())
        {
            return currentTotal.compareTo(BigDecimal.valueOf(minimumTotal)) >= 0;
        }
        else
        {
            return currentTotal.compareTo(BigDecimal.valueOf(minimumTotal)) <= 0;
        }
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.voucher.jalo.Restriction#getApplicableEntries(de.hybris.platform.jalo.order.AbstractOrder)
     */
    @Override
    public VoucherEntrySet getApplicableEntries(final AbstractOrder anOrder) {
        final VoucherEntrySet applicableEntries = new VoucherEntrySet();
        if (isFulfilled(anOrder)) {
            applicableEntries.addAll(getApplicableEntriesInternal(anOrder));
        }
        return applicableEntries;
    }

    private VoucherEntrySet getApplicableEntriesInternal(final AbstractOrder anOrder) {
        final Collection<Category> includedCategories = getIncludedCategories();
        final Collection<Category> excludedCategories = getExcludedCategories();
        final Collection<Product> includedProducts = getIncludedProducts();
        final Collection<Product> excludedProducts = getExcludedProducts();

        final VoucherEntrySet applicableEntries = new VoucherEntrySet();

        final List<AbstractOrderEntry> orderEntries = anOrder.getEntries();
        for (final AbstractOrderEntry orderEntry : orderEntries) {
            final Product orderEntryProduct = orderEntry.getProduct();

            boolean includedProduct = false;
            if (includedProducts.isEmpty() && includedCategories.isEmpty()) {
                includedProduct = true;
            }

            if (!includedProduct
                    && (!includedProducts.isEmpty() && RestrictionUtils.containsProduct(includedProducts,
                            orderEntryProduct))) {
                includedProduct = true;
            }

            if (!includedProduct
                    && (!includedCategories.isEmpty() && RestrictionUtils.containsProductCategory(orderEntryProduct,
                            includedCategories))) {
                includedProduct = true;
            }

            if (!includedProduct) {
                continue;
            }

            if (!excludedProducts.isEmpty() && RestrictionUtils.containsProduct(excludedProducts, orderEntryProduct)) {
                continue; // Product is excluded, so this entry doesn't count
            }

            if (!excludedCategories.isEmpty()
                    && RestrictionUtils.containsProductCategory(orderEntryProduct, excludedCategories)) {
                continue; // Product is in an excluded category, so this entry doesn't count
            }

            applicableEntries.add(new VoucherEntry(orderEntry, orderEntry.getQuantity().longValue(), orderEntry
                    .getUnit()));
        }

        return applicableEntries;
    }
}
