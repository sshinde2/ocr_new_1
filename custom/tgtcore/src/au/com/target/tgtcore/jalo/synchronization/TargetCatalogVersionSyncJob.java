package au.com.target.tgtcore.jalo.synchronization;

import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.cronjob.jalo.Job;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.servicelayer.event.EventService;

import au.com.target.tgtcore.event.CatalogVersionSyncCompletionEvent;


public class TargetCatalogVersionSyncJob extends GeneratedTargetCatalogVersionSyncJob
{

    @SuppressWarnings("deprecation")
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }


    @SuppressWarnings("deprecation")
    @Override
    protected CronJob.CronJobResult performCronJob(final CronJob cronJob) {
        try {
            return super.performCronJob(cronJob);
        }
        finally {
            if (shouldPublishSuccessfulCompletion(cronJob.getJob())) {
                getEventService().publishEvent(new CatalogVersionSyncCompletionEvent(cronJob.getEndTime()));
            }
        }
    }

    private boolean shouldPublishSuccessfulCompletion(final Job job) {
        if (!(job instanceof TargetCatalogVersionSyncJob)) {
            return false;
        }
        if (((TargetCatalogVersionSyncJob)job).isPublishCompletionAsPrimitive()) {
            return true;
        }
        return false;
    }

    /**
     * 
     */
    public EventService getEventService() {
        return (EventService)Registry.getApplicationContext().getBean("eventService");
    }

}
