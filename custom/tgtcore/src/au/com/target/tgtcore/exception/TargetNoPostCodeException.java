/**
 * 
 */
package au.com.target.tgtcore.exception;

/**
 * @author siddharam
 *
 */
public class TargetNoPostCodeException extends RuntimeException {

    public TargetNoPostCodeException(final String message)
    {
        super(message);
    }

    public TargetNoPostCodeException(final Throwable cause)
    {
        super(cause);
    }

    public TargetNoPostCodeException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
