/**
 * 
 */
package au.com.target.tgtcore.exception;

/**
 * Target UnknowIndentiferException. When the Flexible Query return empty result, this Exception will be thrown
 * 
 */
public class TargetUnknownIdentifierException extends Exception {

    public TargetUnknownIdentifierException(final String message)
    {
        super(message);
    }

    public TargetUnknownIdentifierException(final Throwable cause)
    {
        super(cause);
    }

    public TargetUnknownIdentifierException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
