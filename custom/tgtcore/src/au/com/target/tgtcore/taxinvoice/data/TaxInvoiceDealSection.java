/**
 * 
 */
package au.com.target.tgtcore.taxinvoice.data;

import java.util.ArrayList;
import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class TaxInvoiceDealSection {
    private List<TaxInvoiceLineItem> lineItems;

    private String dealMessage;

    /**
     * @return the lineItems
     */
    public List<TaxInvoiceLineItem> getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems
     *            the lineItems to set
     */
    public void setLineItems(final List<TaxInvoiceLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public void addLineItem(final TaxInvoiceLineItem lineItem) {
        if (this.lineItems == null) {
            this.lineItems = new ArrayList<>();
        }
        this.lineItems.add(lineItem);
    }

    /**
     * @return the dealMessage
     */
    public String getDealMessage() {
        return dealMessage;
    }

    /**
     * @param dealMessage
     *            the dealMessage to set
     */
    public void setDealMessage(final String dealMessage) {
        this.dealMessage = dealMessage;
    }
}
