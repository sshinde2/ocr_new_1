package au.com.target.tgtcore.taxinvoice.impl;

import de.hybris.platform.commons.jalo.CommonsManager;
import de.hybris.platform.commons.jalo.Document;
import de.hybris.platform.commons.jalo.Format;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.constants.TgtCoreConstants.Seperators;
import au.com.target.tgtcore.document.dao.TargetDocumentDao;
import au.com.target.tgtcore.enums.DocumentType;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.order.strategies.TaxInvoiceGenerationDenialStrategy;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtcore.taxinvoice.dao.TaxInvoiceDao;
import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * An Implementation of {@link TaxInvoiceService} using hybris formatter framework. Currently this implementation uses
 * Jalo version of the formatter framework as there is no ServiceLayer equivalent.
 * 
 */
public class TaxInvoiceServiceImpl extends AbstractBusinessService implements TaxInvoiceService {

    private static final Logger LOG = Logger.getLogger(TaxInvoiceServiceImpl.class);

    private String taxInvoiceTemplateCode;

    private TaxInvoiceDao taxInvoiceDao;

    private TargetDocumentDao targetDocumentDao;

    private PromotionsService promotionService;

    private List<TaxInvoiceGenerationDenialStrategy> taxInvoiceGenerationDenialStrategies;

    @Override
    public boolean isTaxInvoiceResendAllowed(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Order cannot be null");

        for (final TaxInvoiceGenerationDenialStrategy strategy : taxInvoiceGenerationDenialStrategies) {
            if (strategy.isDenied(orderModel)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public DocumentModel generateTaxInvoiceForOrder(final OrderModel orderModel) throws TaxInvoiceException {
        Assert.notNull(orderModel, "Order cannot be null");

        final DocumentModel existedTaxInvoice = this.getTaxInvoiceForOrder(orderModel);

        if (existedTaxInvoice != null) {
            return existedTaxInvoice;
        }


        final Order orderItem = getModelService().toPersistenceLayer(orderModel);

        final Format format = getTaxInvoiceFormatForOrder(orderItem);

        if (format == null) {
            throw new TaxInvoiceException("TaxInvoice template not found for order:" + orderModel.getCode());
        }

        try {
            final Document document = format.format(orderItem);

            final DocumentModel taxInvoice = getModelService().toModelLayer(document);
            final String code = (TgtCoreConstants.TAX_INVOICE_PREFIX + Seperators.HYPHEN + orderModel.getCode()).trim();
            taxInvoice.setCode(code);
            taxInvoice.setRealFileName(code + TgtCoreConstants.TAX_INVOICE_PDF_SUFFIX);
            taxInvoice.setDocumentType(DocumentType.INVOICE);
            getModelService().save(taxInvoice);

            return taxInvoice;
        }
        catch (final JaloBusinessException e) {
            LOG.error("Exception while generating the tax invoice for order:" + orderModel.getCode(), e);
            throw new TaxInvoiceException(e.getMessage(), e);
        }
    }

    /**
     * Call DAO to find the TargetValuePackType for the product
     * 
     * @param code
     *            - product code
     * @return ValuePackTypeModel
     */
    @Override
    public TargetValuePackTypeModel findTargetValuePackType(final String code) {
        return taxInvoiceDao.findTargetValuePackTypeForProductCode(code);
    }

    /**
     * get the relevant promotionsOrderResults for the order
     * 
     * @param orderModel
     * @return PromotionOrderResults
     */
    @Override
    public PromotionOrderResults getPromotionResults(final OrderModel orderModel) {
        return promotionService.getPromotionResults(orderModel);
    }




    /* (non-Javadoc)
     * @see au.com.target.tgtcore.taxinvoice.TaxInvoiceService#getTaxInvoiceForOrder(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public DocumentModel getTaxInvoiceForOrder(final OrderModel orderModel) {
        LOG.info(SplunkLogFormatter.formatMessage("Finding Tax Invoice for Order : " + orderModel.getCode(),
                ErrorCode.INFO_EMAIL_TAX_INVOICE));
        return targetDocumentDao.findDocumentModelBySourceItemAndDocumentType(orderModel, DocumentType.INVOICE);
    }

    protected Format getTaxInvoiceFormatForOrder(final Order orderItem) {
        final Collection<Format> formats = getCommonsManagerInstance().getFormatsForItem(orderItem);

        if (CollectionUtils.isEmpty(formats)) {
            return null;
        }

        for (final Format format : formats) {
            if (taxInvoiceTemplateCode.equals(format.getCode())) {
                return format;
            }
        }
        return null;
    }

    /**
     * @return the commons manager instance
     */
    protected CommonsManager getCommonsManagerInstance() {
        return CommonsManager.getInstance();
    }

    /**
     * @param taxInvoiceTemplateCode
     *            the taxInvoiceTemplateCode to set
     */
    @Required
    public void setTaxInvoiceTemplateCode(final String taxInvoiceTemplateCode) {
        this.taxInvoiceTemplateCode = taxInvoiceTemplateCode;
    }

    /**
     * @param taxInvoiceDao
     *            the taxInvoiceDao to set
     */
    @Required
    public void setTaxInvoiceDao(final TaxInvoiceDao taxInvoiceDao) {
        this.taxInvoiceDao = taxInvoiceDao;
    }

    /**
     * @param targetDocumentDao
     *            the targetDocumentDao to set
     */
    @Required
    public void setTargetDocumentDao(final TargetDocumentDao targetDocumentDao) {
        this.targetDocumentDao = targetDocumentDao;
    }

    /**
     * @param taxInvoiceGenerationDenialStrategies
     *            the taxInvoiceGenerationDenialStrategies to set
     */
    @Required
    public void setTaxInvoiceGenerationDenialStrategies(
            final List<TaxInvoiceGenerationDenialStrategy> taxInvoiceGenerationDenialStrategies) {
        this.taxInvoiceGenerationDenialStrategies = taxInvoiceGenerationDenialStrategies;
    }

    /**
     * @param promotionService
     *            the promotionService to set
     */
    @Required
    public void setPromotionService(final PromotionsService promotionService) {
        this.promotionService = promotionService;
    }

}
