package au.com.target.tgtcore.taxinvoice;

/**
 * Thrown to indicate that a business exception occured during TaxInvoice generation
 * 
 */
public class TaxInvoiceException extends Exception {


    /**
     * @param message
     */
    public TaxInvoiceException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public TaxInvoiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
