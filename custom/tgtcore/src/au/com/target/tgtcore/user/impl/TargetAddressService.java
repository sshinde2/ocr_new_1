package au.com.target.tgtcore.user.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.user.impl.DefaultAddressService;

import au.com.target.tgtcore.model.TargetAddressModel;


/**
 * Extend version to create addresses as instances of {@link TargetAddressModel}
 */
public class TargetAddressService extends DefaultAddressService {

    @Override
    public AddressModel createAddressForOwner(final ItemModel owner)
    {
        validateParameterNotNullStandardMessage("owner", owner);
        final AddressModel address = getModelService().create(TargetAddressModel.class);
        address.setOwner(owner);
        return address;
    }

}
