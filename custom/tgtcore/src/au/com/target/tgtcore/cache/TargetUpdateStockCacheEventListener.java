/**
 * 
 */
package au.com.target.tgtcore.cache;

import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.region.CacheRegion;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.constants.TgtCoreConstants;




/**
 * @author fkhan4
 *
 */
public class TargetUpdateStockCacheEventListener extends AbstractEventListener<TargetUpdateStockCacheEvent> {

    protected static final Logger LOG = Logger.getLogger(TargetUpdateStockCacheEventListener.class);
    @Autowired
    private CacheController cacheController;

    @Override
    public void onEvent(final TargetUpdateStockCacheEvent event) {
        final Collection<CacheRegion> cacheRegions = cacheController.getRegions();
        for (final CacheRegion region : cacheRegions) {
            if (TgtCoreConstants.ENTITY_CACHE_REGION.equals(region.getName())) {
                cacheController.clearCache(region);
                LOG.info(" :: Cleared entity cache region :: ");
                break;
            }
        }
    }

}