/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.util.Assert;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtutility.constants.TgtutilityConstants;


public class ReGenerateTaxInvoiceAction extends GenerateTaxInvoiceAction {


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.actions.GenerateTaxInvoiceAction#executeAction(de.hybris.platform.orderprocessing.model.OrderProcessModel)
     */
    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Order cannot be null");


        final boolean canResend = getTaxInvoiceService().isTaxInvoiceResendAllowed(orderModel);

        if (!canResend) {
            throw new BusinessProcessException(TgtutilityConstants.ErrorCode.ERR_TAXINVOICE,
                    "Can not generate TaxInvoice for Order: " + orderModel.getCode());
        }

        super.executeAction(process);
    }

}
