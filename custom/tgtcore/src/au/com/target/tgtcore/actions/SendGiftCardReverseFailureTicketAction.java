/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import au.com.target.tgtpayment.dto.GiftCardReversalData;


/**
 * @author htan3
 *
 */
public class SendGiftCardReverseFailureTicketAction extends SendCsAgentTicketAction {
    /**
     * @param process
     */
    @Override
    protected String generateTicketBodyText(final BusinessProcessModel process) {
        final GiftCardReversalData reversalData = (GiftCardReversalData)getOrderProcessParameterHelper()
                .getGiftCardReversalData(process);
        final String text = String
                .format("Gift card %s payment of $%s (receipt no: %s) could not be refunded." +
                        "Please manually confirm in IPG portal. Contact %s %s (%s %s) to refund the payment. cartId=%s",
                        reversalData.getMaskedCardNumber(),
                        reversalData.getAmount(), reversalData.getReceiptNumber(), reversalData.getFirstName(),
                        reversalData.getLastName(), reversalData.getMobile(), reversalData.getCustomerEmail(),
                        reversalData.getCartId());
        return text;
    }
}
