package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtutility.constants.TgtutilityConstants;


/**
 * Action responsible for generating Tax invoices for order.
 * 
 */
public class GenerateTaxInvoiceAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(GenerateTaxInvoiceAction.class);

    private TaxInvoiceService taxInvoiceService;
    private TargetSalesApplicationConfigService salesApplicationConfigService;





    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Order cannot be null");

        try {
            if (salesApplicationConfigService.isSuppressTaxInvoice(orderModel.getSalesApplication())) {
                LOG.info("Sales application is configured to suppress the tax invoice hence suppressing for order="
                        + orderModel.getCode() + ", salesApplication=" + orderModel.getSalesApplication().toString());
            }
            else {
                taxInvoiceService.generateTaxInvoiceForOrder(orderModel);
            }
        }
        catch (final TaxInvoiceException ex) {
            LOG.error("Error while generating tax invoice for order:" + orderModel.getCode());
            throw new BusinessProcessException(TgtutilityConstants.ErrorCode.ERR_TAXINVOICE, ex.getMessage(), ex);
        }


    }

    /**
     * @param taxInvoiceService
     *            the taxInvoiceService to set
     */
    @Required
    public void setTaxInvoiceService(final TaxInvoiceService taxInvoiceService) {
        this.taxInvoiceService = taxInvoiceService;
    }

    /**
     * @return the taxInvoiceService
     */
    public TaxInvoiceService getTaxInvoiceService() {
        return taxInvoiceService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}
