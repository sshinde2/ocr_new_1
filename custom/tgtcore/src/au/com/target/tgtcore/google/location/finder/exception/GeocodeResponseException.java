/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.exception;

/**
 * @author bpottass
 *
 */
public class GeocodeResponseException extends RuntimeException {

    /**
     * @param message
     *            Custom message
     */
    public GeocodeResponseException(final String message) {
        super(message);
    }

    /**
     * @param message
     *            Custom message
     * @param e
     *            Exception
     */
    public GeocodeResponseException(final String message, final Throwable e) {
        super(message, e);
    }

}