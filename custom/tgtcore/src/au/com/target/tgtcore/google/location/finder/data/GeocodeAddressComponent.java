/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.data;

import java.util.List;


/**
 * @author bbaral1
 *
 */
public class GeocodeAddressComponent {
    private String longName;
    private String shortName;
    private List<String> types;

    /**
     * @return the longName
     */
    public String getLongName() {
        return longName;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @return the types
     */
    public List<String> getTypes() {
        return types;
    }

    /**
     * @param longName
     *            the longName to set
     */
    public void setLongName(final String longName) {
        this.longName = longName;
    }

    /**
     * @param shortName
     *            the shortName to set
     */
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    /**
     * @param types
     *            the types to set
     */
    public void setTypes(final List<String> types) {
        this.types = types;
    }

}
