/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.client;

import java.io.IOException;
import java.net.URL;

import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;


/**
 * @author bbaral1
 *
 */
public interface TargetLocationFinderClient {

    /**
     * Method will fetch geocode response based on location/postcode/lalng value.
     *
     * @param url
     * @return GeocodingResponse
     * @throws IOException
     */
    GeocodeResponse geocodingResponse(final URL url) throws IOException;

}
