/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.service.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.GPS;

import java.io.IOException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.google.location.finder.client.TargetLocationFinderClient;
import au.com.target.tgtcore.google.location.finder.converter.GeocodeResponseToTargetGPSConverter;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.service.GeocodeResponseValidator;
import au.com.target.tgtcore.google.location.finder.service.TargetLocationFinderService;


/**
 * @author bbaral1
 *
 */
public class TargetLocationFinderServiceImpl implements TargetLocationFinderService {

    private TargetGeocodeLocationFinderHelper targetGeocodeLocationFinderHelper;

    private TargetLocationFinderClient targetLocationFinderClient;

    private GeocodeResponseToTargetGPSConverter responseConverter;

    private GeocodeResponseValidator validator;

    @Override
    public GeocodeResponse searchUserDeliveryLocationSuburb(final String locationOrPostCode, final Double latitude,
            final Double longitude) throws IOException {
        final URL url = targetGeocodeLocationFinderHelper.getUrl(locationOrPostCode, latitude, longitude);
        return targetLocationFinderClient.geocodingResponse(url);
    }

    @Override
    public GPS getTargetGPS(final AddressModel address)
            throws IOException {
        final URL url = targetGeocodeLocationFinderHelper.getUrl(address);
        final GeocodeResponse response = targetLocationFinderClient.geocodingResponse(url);
        validator.validate(response);
        return responseConverter.convert(response);
    }

    @Override
    public GPS getTargetGPS(final String postCode) throws IOException {
        final URL url = targetGeocodeLocationFinderHelper.getUrl(postCode);
        final GeocodeResponse response = targetLocationFinderClient.geocodingResponse(url);
        validator.validate(response);
        return responseConverter.convert(response);
    }

    /**
     * @return the targetLocationFinderClient
     */
    public TargetLocationFinderClient getTargetLocationFinderClient() {
        return targetLocationFinderClient;
    }

    /**
     * @param targetGeocodeLocationFinderHelper
     *            the targetGeocodeLocationFinderHelper to set
     */
    @Required
    public void setTargetGeocodeLocationFinderHelper(
            final TargetGeocodeLocationFinderHelper targetGeocodeLocationFinderHelper) {
        this.targetGeocodeLocationFinderHelper = targetGeocodeLocationFinderHelper;
    }

    /**
     * @param responseConverter
     *            the responseConverter to set
     */
    @Required
    public void setResponseConverter(final GeocodeResponseToTargetGPSConverter responseConverter) {
        this.responseConverter = responseConverter;
    }

    /**
     * @param targetLocationFinderClient
     *            the targetLocationFinderClient to set
     */
    @Required
    public void setTargetLocationFinderClient(final TargetLocationFinderClient targetLocationFinderClient) {
        this.targetLocationFinderClient = targetLocationFinderClient;
    }

    /**
     * @param validator
     *            the validator to set
     */
    @Required
    public void setValidator(final GeocodeResponseValidator validator) {
        this.validator = validator;
    }

}
