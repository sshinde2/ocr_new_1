/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.service;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.GPS;

import java.io.IOException;
import java.net.MalformedURLException;

import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;



/**
 * @author bbaral1
 *
 */
public interface TargetLocationFinderService {

    /**
     * Method will search for user delivery location/suburb based on location/postcode input parameter.
     * 
     * @param location
     * @param latitude
     * @param longitude
     * @return GeocodingResponse
     * @throws IOException
     */
    GeocodeResponse searchUserDeliveryLocationSuburb(final String location, final Double latitude,
            final Double longitude) throws IOException;

    /**
     * To get the latitude and longitude for the address using googles geocoding api
     * 
     * @param address
     * @return GPS with latitude and longitude for the given address
     * @throws IOException
     * @throws MalformedURLException
     */
    GPS getTargetGPS(AddressModel address) throws IOException;

    /**
     * GPS with latitude and longitude for first locality from given postcode
     * 
     * @param postCode
     * @return GPS
     * @throws IOException
     */
    GPS getTargetGPS(String postCode) throws IOException;

}
