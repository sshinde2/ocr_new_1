/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.client.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.log4j.Logger;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import au.com.target.tgtcore.google.location.finder.client.TargetLocationFinderClient;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;


/**
 * @author bbaral1
 *
 */
public class TargetLocationFinderClientImpl implements TargetLocationFinderClient {

    private static final Logger LOG = Logger.getLogger(TargetLocationFinderClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.google.location.finder.client.TargetLocationFinderClient#geocodingResponse(java.net.URL)
     */
    @Override
    public GeocodeResponse geocodingResponse(final URL url) throws IOException {
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        final GeocodeResponse geocodeResponse;
        try {
            geocodeResponse = gson.fromJson(bufferedReader, GeocodeResponse.class);
            LOG.info("INFO-GOOGLEAPI-LOOKUP-USING-GSON : Looked up Geocode API with query=" + url.getQuery());
        }
        finally {
            bufferedReader.close();
        }
        return geocodeResponse;
    }

}
