/**
 * 
 */
package au.com.target.tgtcore.featureswitch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.consignextractrecord.dao.ConsignmentExtractRecordDao;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;


/**
 * @author pratik
 *
 */
public class TargetFeatureSwitchServiceImpl implements TargetFeatureSwitchService {

    private TargetFeatureSwitchDao targetFeatureSwitchDao;

    private ConsignmentExtractRecordDao consignmentExtractRecordDao;

    /**
     * Method to fetch a switch for a particular feature, if the switch doesn't exist then that feature is considered as
     * enabled
     * 
     * @param featureName
     * @return true if switch doesn't exist and if exists it is enabled
     */
    @Override
    public boolean isFeatureEnabled(final String featureName) {
        final TargetFeatureSwitchModel featureSwitch = targetFeatureSwitchDao.getFeatureByName(featureName);
        return featureSwitch == null ? false : featureSwitch.getEnabled().booleanValue();
    }

    @Override
    public boolean includeBillingAddressInAccertifyFeed() {
        return isFeatureEnabled("accertify.include.billingAddress");
    }

    /**
     * Method is to return enabled features as a List. When includeOnlyUIEnabled is true, it restricts to list of
     * features to ones that are also UI enabled.
     * 
     * @param includeOnlyUIEnabled
     * @return list
     */
    @Override
    public List<TargetFeatureSwitchModel> getEnabledFeatures(final boolean includeOnlyUIEnabled) {
        return targetFeatureSwitchDao.getEnabledFeatures(includeOnlyUIEnabled);
    }

    @Override
    public void saveConsignmentExtractRecord(final TargetConsignmentModel consignment, final boolean consignmentAsId) {
        Assert.notNull(consignment, "Consignment cannot be null");
        consignmentExtractRecordDao.saveConsignmentExtractRecord(consignment, consignmentAsId);
    }

    @Override
    public ConsignmentExtractRecordModel getConsExtractRecordByConsignment(final TargetConsignmentModel consignment) {
        Assert.notNull(consignment, "Consignment cannot be null");
        return consignmentExtractRecordDao.getConsExtractRecordByConsignment(consignment);
    }

    @Override
    public boolean sendEmailWithBlackout() {
        return isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SEND_EMAIL_WITH_BLACKOUT);
    }

    /**
     * @param targetFeatureSwitchDao
     *            the targetFeatureSwitchDao to set
     */
    @Required
    public void setTargetFeatureSwitchDao(final TargetFeatureSwitchDao targetFeatureSwitchDao) {
        this.targetFeatureSwitchDao = targetFeatureSwitchDao;
    }

    @Override
    public List<TargetFeatureSwitchModel> getAllFeatures() {
        return targetFeatureSwitchDao.getAllFeatures();
    }

    /**
     * @param consignmentExtractRecordDao
     *            the consignmentExtractRecordDao to set
     */
    @Required
    public void setConsignmentExtractRecordDao(final ConsignmentExtractRecordDao consignmentExtractRecordDao) {
        this.consignmentExtractRecordDao = consignmentExtractRecordDao;
    }

}
