/**
 * 
 */
package au.com.target.tgtcore.flybuys.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.flybuys.client.FlybuysClient;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * The Class FlybuysDiscountServiceImpl.
 * 
 * @author Rahul
 */
public class FlybuysDiscountServiceImpl implements FlybuysDiscountService {

    private static final Logger LOG = Logger.getLogger(FlybuysDiscountServiceImpl.class);

    private static final String TRANSACTION_ID_TIMESTAMP_PATTERN = "yyyyMMddHHmm";

    private static final String TRANSACTION_ID_TIMESTAMP_WITH_SECONDS_PATTERN = TRANSACTION_ID_TIMESTAMP_PATTERN + "SS";

    private String storeNumber;

    private FlybuysClient flybuysClient;

    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    private ModelService modelService;

    private TargetVoucherService voucherService;


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#authenticateFlybuys(java.lang.String, java.util.Date, java.lang.String)
     */
    @Override
    public FlybuysAuthenticateResponseDto authenticateFlybuys(final String flybuysCardNumber, final Date dob,
            final String postCode) {

        if (StringUtils.isNotEmpty(flybuysCardNumber) && StringUtils.isNotEmpty(postCode)
                && dob != null) {
            final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = getFlybuysClient()
                    .flybuysAuthenticate(
                            flybuysCardNumber, dob, postCode);
            final FlybuysRedeemConfigModel flybuysRedeemConfigModel = flybuysRedeemConfigService
                    .getFlybuysRedeemConfig();

            if (null != flybuysRedeemConfigModel) {
                final BigDecimal configuredMinRedeemable = BigDecimal.valueOf(flybuysRedeemConfigModel
                        .getMinRedeemable().doubleValue());
                final BigDecimal configuredMaxRedeemable = BigDecimal.valueOf(flybuysRedeemConfigModel
                        .getMaxRedeemable().doubleValue());
                final Collection<FlybuysRedeemTierDto> allRedeemTiers = flybuysAuthenticateResponseDto.getRedeemTiers();
                final List<FlybuysRedeemTierDto> filteredTiersByConfig = new ArrayList<>();

                if (CollectionUtils.isNotEmpty(allRedeemTiers)) {
                    for (final FlybuysRedeemTierDto flybuysRedeemTier : allRedeemTiers) {
                        if (configuredMinRedeemable.compareTo(flybuysRedeemTier.getDollarAmt()) <= 0
                                && configuredMaxRedeemable.compareTo(flybuysRedeemTier.getDollarAmt()) >= 0) {
                            filteredTiersByConfig.add(flybuysRedeemTier);
                        }
                    }
                    flybuysAuthenticateResponseDto.setRedeemTiers(filteredTiersByConfig);
                }
            }

            return flybuysAuthenticateResponseDto;
        }

        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#consumeFlybuysPoints(au.com.target.tgtcore.model.FlybuysDiscountModel)
     */
    @Override
    public FlybuysConsumeResponseDto consumeFlybuysPoints(final OrderModel orderModel) {
        final FlybuysDiscountModel flybuysDiscountModel = voucherService.getFlybuysDiscountForOrder(orderModel);
        if (flybuysDiscountModel != null) {
            final String flybuysCardNumber = flybuysDiscountModel.getFlybuysCardNumber();
            final String securityToken = flybuysDiscountModel.getSecurityToken();
            final Integer points = orderModel.getFlybuysPointsConsumed();
            final String redeemCode = flybuysDiscountModel.getRedeemCode();

            if (points != null && flybuysDiscountModel.getValue() != null && StringUtils.isNotEmpty(securityToken)
                    && StringUtils.isNotEmpty(getStoreNumber())
                    && StringUtils.isNotEmpty(redeemCode)) {

                final FlybuysConsumeResponseDto responseDto = getFlybuysClient().consumeFlybuysPoints(
                        flybuysCardNumber,
                        securityToken, getStoreNumber(), points,
                        BigDecimal.valueOf(flybuysDiscountModel.getValue().doubleValue()), redeemCode,
                        getTransactionIdTimestamp(orderModel.getCode(), DateTime.now()));

                if (responseDto != null) {
                    flybuysDiscountModel.setConfirmationCode(responseDto.getConfimationCode());
                    modelService.save(flybuysDiscountModel);
                }

                return responseDto;
            }
            else {
                LOG.warn(SplunkLogFormatter.formatMessage(
                        "FlybuysDiscountService missing data in FlybuysDiscountModel",
                        TgtutilityConstants.ErrorCode.WARN_PAYMENT, flybuysDiscountModel.getCode()));
            }
        }
        return null;
    }


    /**
     * Gets the latest order modification record.
     * 
     * @param order
     *            the order
     * @return the latest order modification record
     */
    private OrderModificationRecordModel getLatestOrderModificationRecord(final OrderModel order) {
        final Set<OrderModificationRecordModel> modifications = order.getModificationRecords();
        if (CollectionUtils.isNotEmpty(modifications)) {
            final List<OrderModificationRecordModel> orderModificationRecordList = new ArrayList<>();
            orderModificationRecordList.addAll(modifications);
            Collections.sort(orderModificationRecordList, new Comparator<OrderModificationRecordModel>()
            {
                @Override
                public int compare(final OrderModificationRecordModel o1, final OrderModificationRecordModel o2)
                {
                    return o2.getCreationtime().compareTo(o1.getCreationtime());
                }
            });

            return orderModificationRecordList.get(0);
        }
        return null;
    }

    /**
     * Gets the latest order modification record entry.
     * 
     * @param order
     *            the order
     * @return the latest order modification record entry
     */
    private OrderModificationRecordEntryModel getLatestOrderModificationRecordEntry(final OrderModel order)
    {
        final OrderModificationRecordModel modificationModel = getLatestOrderModificationRecord(order);
        if (modificationModel == null) {
            return null;
        }
        final Collection<OrderModificationRecordEntryModel> entries = modificationModel
                .getModificationRecordEntries();
        if (CollectionUtils.isNotEmpty(entries)) {
            final List<OrderModificationRecordEntryModel> orderModificationRecordEntryModelList = new ArrayList<>();
            orderModificationRecordEntryModelList.addAll(entries);
            Collections.sort(orderModificationRecordEntryModelList,
                    new Comparator<OrderModificationRecordEntryModel>()
                    {
                        @Override
                        public int compare(final OrderModificationRecordEntryModel o1,
                                final OrderModificationRecordEntryModel o2)
                        {
                            return o2.getCreationtime().compareTo(o1.getCreationtime());
                        }
                    });

            return orderModificationRecordEntryModelList.get(0);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#refundFlybuysPoints(de.hybris.platform.core.model.order.OrderModel, java.math.BigDecimal)
     */
    @Override
    public FlybuysRefundResponseDto refundFlybuysPoints(final OrderModel order) {
        Assert.notNull(order, "order cannot not be null");
        final OrderModificationRecordEntryModel orderModificationRecordEntry = getLatestOrderModificationRecordEntry(order);
        if (orderModificationRecordEntry != null)
        {
            final Double refundedAmount = orderModificationRecordEntry.getRefundedFlybuysAmount();
            final Integer refundedPoints = orderModificationRecordEntry.getRefundedFlybuysPoints();
            if (null != refundedAmount && refundedAmount.compareTo(Double.valueOf(0d)) > 0
                    && null != refundedPoints && refundedPoints.compareTo(Integer.valueOf(0)) > 0) {

                final FlybuysDiscountModel flybuysDiscountModel = getVoucherService().getFlybuysDiscountForOrder(order);

                if (flybuysDiscountModel != null) {
                    final String flybuysCardNumber = flybuysDiscountModel.getFlybuysCardNumber();
                    final String securityToken = flybuysDiscountModel.getSecurityToken();

                    if (StringUtils.isNotEmpty(securityToken)
                            && StringUtils.isNotEmpty(getStoreNumber())) {
                        final DateTime now = DateTime.now();
                        final FlybuysRefundResponseDto responseDto = getFlybuysClient().refundFlybuysPoints(
                                flybuysCardNumber,
                                securityToken, getStoreNumber(), refundedPoints,
                                BigDecimal.valueOf(refundedAmount.doubleValue()), getTimestamp(now),
                                getTransactionIdTimestamp(order.getCode(), now));

                        if (responseDto != null) {

                            String messageKey = StringUtils.EMPTY;
                            if (responseDto.getResponse() != null
                                    && StringUtils.isNotEmpty(responseDto.getResponse().getMessageKey())) {
                                messageKey = responseDto.getResponse().getMessageKey();
                            }
                            final String responseMessage = responseDto.getResponseMessage() != null ? responseDto
                                    .getResponseMessage() : StringUtils.EMPTY;
                            final String confimationCode = responseDto.getConfimationCode() != null ? responseDto
                                    .getConfimationCode() : StringUtils.EMPTY;
                            LOG.info(MessageFormat
                                    .format("INFO-FLYBUYS-REFUND : The response code received from Flybuys is : {0} with a message {1} and a confirmation code of {2}",
                                            messageKey,
                                            responseMessage,
                                            confimationCode));
                            orderModificationRecordEntry.setFlybuysPointsRefundConfirmationCode(confimationCode);
                            modelService.save(orderModificationRecordEntry);
                        }
                        return responseDto;
                    }
                    else {
                        LOG.warn(SplunkLogFormatter.formatMessage(
                                "FlybuysDiscountService missing data in FlybuysDiscountModel",
                                TgtutilityConstants.ErrorCode.WARN_PAYMENT, flybuysDiscountModel.getCode()));
                    }
                }
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.TargetCancelService#getFlybuysPointsForRefund(au.com.target.tgtcore.model.FlybuysDiscountModel, java.lang.Double)
     */
    @Override
    public BigDecimal getFlybuysPointsToRefund(final FlybuysDiscountModel flybuysDiscount,
            final BigDecimal fbRefundAmount) {
        Assert.notNull(flybuysDiscount, "FlybuysDiscount cannot be null");
        Assert.notNull(fbRefundAmount, "fbRefundAmount cannot be null");

        if (null == flybuysDiscount.getValue() || null == flybuysDiscount.getPointsConsumed()) {
            return BigDecimal.ZERO;
        }
        final BigDecimal totalFBDiscountValue = BigDecimal.valueOf(flybuysDiscount.getValue().doubleValue());
        final BigDecimal totalFBPointsRedeemed = BigDecimal.valueOf(flybuysDiscount.getPointsConsumed().intValue());

        if (fbRefundAmount.compareTo(BigDecimal.ZERO) <= 0
                || totalFBDiscountValue.compareTo(BigDecimal.ZERO) <= 0
                || totalFBPointsRedeemed.compareTo(BigDecimal.ZERO) <= 0
                || fbRefundAmount.compareTo(totalFBDiscountValue) > 0) {
            return BigDecimal.ZERO;
        }
        return fbRefundAmount.multiply(totalFBPointsRedeemed).divide(totalFBDiscountValue);
    }

    /**
     * Gets the transaction id timestamp.
     * 
     * @param orderNumber
     *            the order number
     * @return the transaction id timestamp
     */
    protected String getTransactionIdTimestamp(final String orderNumber, final DateTime now) {
        return getStoreNumber() + now.toString(TRANSACTION_ID_TIMESTAMP_PATTERN) + orderNumber;
    }

    /**
     * Gets the timestamp.
     * 
     * @return the timestamp
     */
    protected String getTimestamp(final DateTime now) {
        return now.toString(TRANSACTION_ID_TIMESTAMP_WITH_SECONDS_PATTERN);
    }

    /**
     * @return the storeNumber
     */
    protected String getStoreNumber() {
        return storeNumber;
    }

    /**
     * Sets the store number.
     * 
     * @param storeNumber
     *            the storeNumber to set
     */
    @Required
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the flybuysClient
     */
    protected FlybuysClient getFlybuysClient() {
        return flybuysClient;
    }

    /**
     * Sets the flybuys client.
     * 
     * @param flybuysClient
     *            the flybuysClient to set
     */
    @Required
    public void setFlybuysClient(final FlybuysClient flybuysClient) {
        this.flybuysClient = flybuysClient;
    }

    /**
     * Sets the flybuys redeem config service.
     * 
     * @param flybuysRedeemConfigService
     *            the flybuysRedeemConfigService to set
     */
    @Required
    public void setFlybuysRedeemConfigService(final FlybuysRedeemConfigService flybuysRedeemConfigService) {
        this.flybuysRedeemConfigService = flybuysRedeemConfigService;
    }

    /**
     * Sets the model service.
     * 
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the voucherService
     */
    protected TargetVoucherService getVoucherService() {
        return voucherService;
    }

    /**
     * Sets the voucher service.
     * 
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final TargetVoucherService voucherService) {
        this.voucherService = voucherService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#setFlybuysRefundData(de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel, de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public void setFlybuysRefundDetailsInModificationEntry(
            final OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            final OrderModel orderModel, final Double orderTotalBeforeVoucherUpdate) {
        if (orderTotalBeforeVoucherUpdate.compareTo(Double.valueOf(0d)) < 0) {
            final FlybuysDiscountModel flybuysDiscount = voucherService.getFlybuysDiscountForOrder(orderModel);
            if (null != flybuysDiscount) {
                final BigDecimal totalPrice = BigDecimal.valueOf(orderTotalBeforeVoucherUpdate.doubleValue())
                        .negate();
                orderModificationRecordEntryModel.setRefundedFlybuysAmount(Double.valueOf(totalPrice.doubleValue()));
                final int refundedFlybuysPoints = getFlybuysPointsToRefund(
                        flybuysDiscount, totalPrice).intValue();
                orderModificationRecordEntryModel.setRefundedFlybuysPoints(Integer.valueOf(refundedFlybuysPoints));
                LOG.info(MessageFormat.format(
                        "INFO-FLYBUYS-REFUND : setting the refunded flybuys amount to {0} and refunded points to {1}",
                        String.valueOf(totalPrice.doubleValue()), Integer.valueOf(refundedFlybuysPoints)));
            }
        }
    }
}