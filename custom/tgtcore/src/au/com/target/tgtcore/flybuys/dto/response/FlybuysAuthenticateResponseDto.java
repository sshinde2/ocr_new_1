/**
 * 
 */
package au.com.target.tgtcore.flybuys.dto.response;


import java.util.Collection;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;


/**
 * @author cwijesu1
 * 
 */
public class FlybuysAuthenticateResponseDto {
    private FlybuysResponseType response;
    private String errorMessage;
    private String securityToken;
    private Integer availPoints;
    private Collection<FlybuysRedeemTierDto> redeemTiers;


    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage
     *            the errorMessage to set
     */
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the securityToken
     */

    public String getSecurityToken() {
        return securityToken;
    }

    /**
     * @param securityToken
     *            the securityToken to set
     */
    public void setSecurityToken(final String securityToken) {
        this.securityToken = securityToken;

    }

    /**
     * @return the availPoints
     */
    public Integer getAvailPoints() {
        return availPoints;
    }

    /**
     * @param availPoints
     *            the availPoints to set
     */
    public void setAvailPoints(final String availPoints) {
        this.availPoints = new Integer(availPoints);
    }

    /**
     * @return the redeemTiers
     */

    public Collection<FlybuysRedeemTierDto> getRedeemTiers() {

        return redeemTiers;
    }

    /**
     * @param redeemTiers
     *            the redeemTiers to set
     */
    public void setRedeemTiers(final Collection<FlybuysRedeemTierDto> redeemTiers) {
        this.redeemTiers = redeemTiers;
    }

    /**
     * @return the response
     */
    public FlybuysResponseType getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final FlybuysResponseType response) {
        this.response = response;
    }





}
