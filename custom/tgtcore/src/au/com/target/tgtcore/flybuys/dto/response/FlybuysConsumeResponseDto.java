/**
 * 
 */
package au.com.target.tgtcore.flybuys.dto.response;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;



/**
 * @author cwijesu1
 * 
 */
public class FlybuysConsumeResponseDto {
    private FlybuysResponseType response;
    private String confimationCode;
    private Integer availPoints;
    private Integer responseCode;

    /**
     * @return the responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * @return the response
     */
    public FlybuysResponseType getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final FlybuysResponseType response) {
        this.response = response;
    }

    /**
     * @return the confimationCode
     */
    public String getConfimationCode() {
        return confimationCode;
    }

    /**
     * @param confimationCode
     *            the confimationCode to set
     */
    public void setConfimationCode(final String confimationCode) {
        this.confimationCode = confimationCode;
    }

    /**
     * @return the availPoints
     */
    public Integer getAvailPoints() {
        return availPoints;
    }

    /**
     * @param availPoints
     *            the availPoints to set
     */
    public void setAvailPoints(final Integer availPoints) {
        this.availPoints = availPoints;
    }





}
