/**
 * 
 */
package au.com.target.tgtcore.flybuys.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;

import java.math.BigDecimal;
import java.util.Date;

import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.model.FlybuysDiscountModel;


/**
 * @author Rahul
 * 
 */
public interface FlybuysDiscountService {

    /**
     * Authenticate flybuys.
     * 
     * @param flybuysCardNumber
     * @param dob
     * @param postCode
     * @return FlybuysAuthenticateResponseDto
     */
    FlybuysAuthenticateResponseDto authenticateFlybuys(String flybuysCardNumber, Date dob, String postCode);

    /**
     * Consume flybuys points.
     * 
     * @param orderModel
     *            the order model
     * @return the flybuys consume response dto
     */
    FlybuysConsumeResponseDto consumeFlybuysPoints(OrderModel orderModel);

    /**
     * Refund flybuys points.
     * 
     * @param order
     *            the order
     * @return the flybuys refund response dto
     */
    FlybuysRefundResponseDto refundFlybuysPoints(OrderModel order);

    /**
     * Gets the flybuys points for flybuys refund amount.
     * 
     * @param flybuysDiscount
     *            the flybuys discount
     * @param fbRefundAmount
     *            the fb refund amount
     * @return the flybuys points for refund
     */
    BigDecimal getFlybuysPointsToRefund(FlybuysDiscountModel flybuysDiscount, BigDecimal fbRefundAmount);

    /**
     * Sets the flybuys refund details in modification entry.
     * 
     * @param orderModificationRecordEntryModel
     *            the order modification record entry model
     * @param orderModel
     *            the order model
     * @param orderTotalBeforeVoucherUpdate
     *            the order total before voucher updation
     */
    void setFlybuysRefundDetailsInModificationEntry(
            final OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            final OrderModel orderModel, final Double orderTotalBeforeVoucherUpdate);

}