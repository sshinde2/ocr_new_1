/**
 * 
 */
package au.com.target.tgtcore.jobs;

import de.hybris.platform.core.BeanShellUtils;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.DynamicMaintenanceJobPerformable;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import au.com.target.tgtcore.model.TargetDynamicMaintenanceCleanupJobModel;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import bsh.EvalError;
import bsh.Interpreter;


/**
 * TargetDynamicMaintenanceJobPerformable is an extended version of hybris given DynamicMaintenanceJobPerformable class
 * which is used to clean the business,emailMeassage and stocklevelHistory logs
 * 
 * @author siddharam
 *
 */
public class TargetDynamicMaintenanceJobPerformable extends DynamicMaintenanceJobPerformable {

    private static final Logger LOG = Logger.getLogger(TargetDynamicMaintenanceJobPerformable.class);
    private String searchCode = null;

    @Override
    public FlexibleSearchQuery getFetchQuery(final CronJobModel cronJob)
    {
        LOG.info("Parsing the Query for clean up Job Code=" + cronJob.getCode());
        this.checkJobDynamicParameter(cronJob);

        final Interpreter interpreter = setupBeanshellInterpreter(null, cronJob);
        FlexibleSearchQuery fsq = null;
        try
        {
            fsq = (FlexibleSearchQuery)interpreter.eval(searchCode);
            LOG.info("Number of items set for batch to delete is=" + fsq.getCount());
        }
        catch (final EvalError e)
        {
            LOG.error(
                    SplunkLogFormatter.formatMessage(
                            "Caught Evaluation exception in cronJobCode=" + cronJob.getCode()
                                    + " during evaluating long search code while cleaning up the entries  "
                                    + TgtutilityConstants.ErrorCode.ERR_CLEANUP_CRONJOB_INTERPRETER_EVAL, ""), e);
            throw new IllegalStateException(
                    "Could not evaluate beanshell's search code to clean up the entries", e);
        }
        return fsq;
    }


    private void checkJobDynamicParameter(final CronJobModel cronJob)
    {
        if (cronJob.getJob() instanceof TargetDynamicMaintenanceCleanupJobModel)
        {
            final TargetDynamicMaintenanceCleanupJobModel job = (TargetDynamicMaintenanceCleanupJobModel)cronJob
                    .getJob();
            if (job.getLongSearchScript() == null || job.getLongSearchScript().isEmpty())
            {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Long search code attribute value is missing for cronJobCode= " + cronJob.getCode()
                                + ",  This is mandatory   "
                                + TgtutilityConstants.ErrorCode.ERR_CLEANUP_CRONJOB_LONGSEARCH_VALUE_MISSING, ""));
                throw new IllegalArgumentException(
                        "Long search code attribute value is missing for TargetDynamicMaintenanceCleanupJobModel. This is mandatory");
            }
            searchCode = job.getLongSearchScript();
        }
        else
        {
            LOG.error(SplunkLogFormatter
                    .formatMessage(
                            "Invalid job instance found instead of TargetDynamicMaintenanceCleanupJobModel for cronJobCode="
                                    + cronJob.getCode()
                                    + " ,Cannot execute the beanshell search to clean up the entries  "
                                    + TgtutilityConstants.ErrorCode.ERR_CLEANUP_CRONJOB_INVALID_INSTANCE, ""));
            throw new IllegalStateException(
                    "The job must be a TargetDynamicMaintenanceCleanupJobModel. Cannot execute the beanshell search");
        }
    }


    /**
     * Returns the beanshell interpretor
     * 
     * @param elements
     * @param cronJob
     * @return Interpreter
     */

    private Interpreter setupBeanshellInterpreter(final List<ItemModel> elements, final CronJobModel cronJob)
    {
        final Interpreter interpreter = getInterpreter();
        try
        {
            interpreter.getNameSpace().importPackage("de.hybris.platform.servicelayer.search");
            interpreter.set("context", getApplicationContext());
            interpreter.set("modelService", modelService);
            interpreter.set("elements", elements);
        }
        catch (final EvalError e)
        {
            LOG.error(SplunkLogFormatter
                    .formatMessage(
                            "Error during setting up beanshell interpreter for CleanUp CronJob Code="
                                    + cronJob.getCode() + "   "
                                    + TgtutilityConstants.ErrorCode.ERR_CLEANUP_CRONJOB_INTERPRETER_SETUP, ""));
            throw new IllegalStateException("Caught eval exception during setup beanshell interpreter", e);
        }
        return interpreter;
    }


    /**
     * @return the ApplicationContext
     */
    protected ApplicationContext getApplicationContext() {
        return Registry.getApplicationContext();
    }

    /**
     * @return the Interpreter
     */
    protected Interpreter getInterpreter() {
        return BeanShellUtils.createInterpreter();
    }

}
