package au.com.target.tgtcore.jobs.exceptions;

/**
 * In the case of an abort Event by the user this exception is thrown for handling that scenario.
 * 
 * @author pthoma20
 * 
 */
public class CronJobAbortException extends Exception {

    public CronJobAbortException(final String msg) {
        super(msg);
    }

}