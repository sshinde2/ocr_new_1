/**
 * 
 */
package au.com.target.tgtcore.customer;

import de.hybris.platform.persistence.security.EJBCannotDecodePasswordException;
import de.hybris.platform.persistence.security.PasswordEncoder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StopWatch;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author rmcalave
 *
 */
public class TargetBcryptPasswordEncoder implements PasswordEncoder {

    private static final Logger LOG = Logger.getLogger(TargetBcryptPasswordEncoder.class);

    private BCryptPasswordEncoder bcryptPasswordEncoder;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    /* (non-Javadoc)
     * @see de.hybris.platform.persistence.security.PasswordEncoder#encode(java.lang.String, java.lang.String)
     */
    @Override
    public String encode(final String uid, final String plainPassword) {
        LOG.debug("Encoding password using bcrypt");

        StopWatch sw = null;
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING_TIMING)) {
            sw = new StopWatch();
            sw.start();
        }

        final String encodedPw = bcryptPasswordEncoder.encode(plainPassword);

        if (sw != null) {
            sw.stop();
            LOG.info("It took " + sw.getTotalTimeSeconds() + " seconds to encode the password using bcrypt");
        }

        return encodedPw;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.persistence.security.PasswordEncoder#check(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public boolean check(final String uid, final String encodedPassword, final String rawPassword) {
        LOG.debug("Checking password using bcrypt");

        StopWatch sw = null;
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING_TIMING)) {
            sw = new StopWatch();
            sw.start();
        }

        final boolean matches = bcryptPasswordEncoder.matches(rawPassword, encodedPassword);

        if (sw != null) {
            sw.stop();
            LOG.info("It took " + sw.getTotalTimeSeconds() + " seconds to check the password using bcrypt");
        }

        return matches;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.persistence.security.PasswordEncoder#decode(java.lang.String)
     */
    @Override
    public String decode(final String paramString) throws EJBCannotDecodePasswordException {
        throw new EJBCannotDecodePasswordException(null, "You cannot decode a bcrypt encoded password", 0);
    }

    /**
     * @param bcryptPasswordEncoder
     *            the bcryptPasswordEncoder to set
     */
    @Required
    public void setBcryptPasswordEncoder(final BCryptPasswordEncoder bcryptPasswordEncoder) {
        this.bcryptPasswordEncoder = bcryptPasswordEncoder;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}
