/**
 * 
 */
package au.com.target.tgtcore.stock.strategies.impl;

import de.hybris.platform.commerceservices.stock.strategies.CommerceAvailabilityCalculationStrategy;
import de.hybris.platform.commerceservices.stock.strategies.impl.CommerceStockLevelStatusStrategy;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author gsing236
 *
 */
public class PreOrderStockLevelStatusStrategy extends CommerceStockLevelStatusStrategy {

    private PreOrderAvailabilityCalculationStrategy preOrderAvailabilityCalculationStrategy;


    @Override
    protected CommerceAvailabilityCalculationStrategy getCommerceStockLevelCalculationStrategy() {
        return this.preOrderAvailabilityCalculationStrategy;
    }

    /**
     * @param preOrderAvailabilityCalculationStrategy
     *            the preOrderAvailabilityCalculationStrategy to set
     */
    @Required
    public void setPreOrderAvailabilityCalculationStrategy(
            final PreOrderAvailabilityCalculationStrategy preOrderAvailabilityCalculationStrategy) {
        this.preOrderAvailabilityCalculationStrategy = preOrderAvailabilityCalculationStrategy;
    }
}
