/**
 * 
 */
package au.com.target.tgtcore.stock.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.stock.TargetStockLevelStatusStrategy;


public class TargetStockLevelStatusStrategyImpl implements TargetStockLevelStatusStrategy {

    private int buffer = 0;

    private int lowStockThreshold = 8;

    private final int noStockThreshold = 0;

    private String lowStockThresholdConfigKey = null;

    private String noStockThresholdConfigKey = null;

    private TargetSharedConfigService targetSharedConfigService;

    @Override
    public StockLevelStatus checkStatus(final String soh) {
        StockLevelStatus stockLevel = StockLevelStatus.OUTOFSTOCK;
        if (StringUtils.isNumeric(soh)) {
            final int stocklevel = Integer.parseInt(soh) - buffer;
            if (stocklevel > targetSharedConfigService.getInt(lowStockThresholdConfigKey, lowStockThreshold)) {
                stockLevel = StockLevelStatus.INSTOCK;
            }
            else if (stocklevel > targetSharedConfigService.getInt(noStockThresholdConfigKey, noStockThreshold)) {
                stockLevel = StockLevelStatus.LOWSTOCK;
            }
        }
        return stockLevel;
    }

    public void setLowStockThreshold(final int lowStockThreshold) {
        this.lowStockThreshold = lowStockThreshold;
    }

    /**
     * @param noStockThresholdConfigKey
     *            the noStockThresholdConfigKey to set
     */
    public void setNoStockThresholdConfigKey(final String noStockThresholdConfigKey) {
        this.noStockThresholdConfigKey = noStockThresholdConfigKey;
    }

    public void setBuffer(final int buffer) {
        this.buffer = buffer;
    }

    public void setLowStockThresholdConfigKey(final String lowStockThresholdConfigKey) {
        this.lowStockThresholdConfigKey = lowStockThresholdConfigKey;
    }

    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

}
