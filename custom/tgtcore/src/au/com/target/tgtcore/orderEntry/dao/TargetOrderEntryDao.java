/**
 * 
 */
package au.com.target.tgtcore.orderEntry.dao;

import de.hybris.platform.core.model.order.OrderEntryModel;

import java.util.List;


/**
 * @author bhuang3
 *
 */
public interface TargetOrderEntryDao {

    /**
     * find order entry model with null productModel
     * 
     * @return orderEntryModel
     */
    public List<OrderEntryModel> findOrderEntryMissingProductModel();
}
