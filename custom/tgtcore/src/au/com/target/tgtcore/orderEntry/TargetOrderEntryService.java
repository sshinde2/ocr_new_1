/**
 * 
 */
package au.com.target.tgtcore.orderEntry;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;

import java.util.List;


/**
 * @author bhuang3
 *
 */
public interface TargetOrderEntryService {

    /**
     * find order entry model with null productModel
     * 
     * @return orderEntryModel
     */
    public List<OrderEntryModel> findOrderEntryMissingProductModel();

    /**
     * Get all product code from order entries
     * 
     * @param orderModel
     * @return list of the product code
     */
    List<String> getAllProductCodeFromOrderEntries(AbstractOrderModel orderModel);

    /**
     * get order entry model by product code
     * 
     * @param orderModel
     * @param productCode
     * @return AbstractOrderEntryModel
     */
    AbstractOrderEntryModel getOrderEntryByProductCode(AbstractOrderModel orderModel, String productCode);
}
