/**
 * 
 */
package au.com.target.tgtcore.orderEntry.strategies.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtcore.orderEntry.strategies.TargetFixMissingProductInOrderEntryStrategy;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author bhuang3
 *
 */
public class TargetFixMissingProductInOrderEntryStrategyImpl implements TargetFixMissingProductInOrderEntryStrategy {

    private static final Logger LOG = Logger.getLogger(TargetFixMissingProductInOrderEntryStrategyImpl.class);

    //pattern to match the value in quote
    private static final String PATTERN_VALUE_IN_QUOTE = "(?<=\")(.*?)(?=\")";

    private static final String ERROR_EMPTY_ORDERENTRY_INFO = "ERR-FIX-MISSING-PRODUCT-IN-ORDERENTRY: empty order entry info in orderEntry pk= {0} for order= {1}";

    private static final String ERROR_ORDERENTRY_INFO_MISMATCH_PATTERN = "ERR-FIX-MISSING-PRODUCT-IN-ORDERENTRY: unexpected order entry info patten in orderEntry pk= {0} for order= {1}";

    private static final String ERROR_ORDERENTRY_INFO_UNKONW_PRODUCT_CODE = "ERR-FIX-MISSING-PRODUCT-IN-ORDERENTRY: UnknownIdentifierException for productCode: {0} ";

    private static final String ERROR_ORDERENTRY_MULTIPLE_PRODUCT_CODE = "ERR-FIX-MISSING-PRODUCT-IN-ORDERENTRY: AmbiguousIdentifierException for productCode: {0}";

    private TargetOrderEntryService targetOrderEntryService;

    private TargetProductService targetProductServiceImpl;

    private CatalogVersionService catalogVersionService;

    private ModelService modelService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.orderEntry.strategies.TargetFixMissingProductInOrderEntryStrategy#fixMissingProductInOrderEntry()
     */
    @Override
    public void fixMissingProductInOrderEntry() {
        final List<OrderEntryModel> orderEntryMissingProduct = targetOrderEntryService
                .findOrderEntryMissingProductModel();
        for (final OrderEntryModel orderEntryModel : orderEntryMissingProduct) {
            final OrderModel orderModel = orderEntryModel.getOrder();
            String orderCode = StringUtils.EMPTY;
            if (orderModel != null) {
                orderCode = orderModel.getCode();
            }
            LOG.info("Try to fix the missing productModel orderEntry=" + orderEntryModel.getPk() + " for order="
                    + orderCode);
            final String orderEntryInfo = orderEntryModel.getInfo();
            if (StringUtils.isEmpty(orderEntryInfo)) {
                LOG.error(MessageFormat.format(ERROR_EMPTY_ORDERENTRY_INFO, orderEntryModel.getPk(), orderCode));
                continue;
            }
            final Pattern pattern = Pattern.compile(PATTERN_VALUE_IN_QUOTE);
            final Matcher matcher = pattern.matcher(orderEntryInfo);
            if (!matcher.find()) {
                //need the order entry info to match the pattern(productcode in the first quote) /product "productCode"...  /
                LOG.error(MessageFormat.format(ERROR_ORDERENTRY_INFO_MISMATCH_PATTERN, orderEntryModel.getPk(),
                        orderCode));
                continue;
            }
            final String productCode = matcher.group();

            ProductModel productModel = null;
            try {
                productModel = targetProductServiceImpl.getProductForCode(
                        catalogVersionService.getCatalogVersion(
                                TgtCoreConstants.Catalog.PRODUCTS,
                                TgtCoreConstants.Catalog.ONLINE_VERSION), productCode);
            }
            catch (final UnknownIdentifierException e) {
                LOG.error(MessageFormat.format(ERROR_ORDERENTRY_INFO_UNKONW_PRODUCT_CODE, productCode), e);
                continue;
            }
            catch (final AmbiguousIdentifierException e) {
                LOG.error(MessageFormat.format(ERROR_ORDERENTRY_MULTIPLE_PRODUCT_CODE, productCode), e);
                continue;
            }

            orderEntryModel.setProduct(productModel);
            modelService.save(orderEntryModel);

            LOG.info("Successfully fix the missing productModel= " + productModel.getCode() + " in orderEntry="
                    + orderEntryModel.getPk() + " for order="
                    + orderEntryModel.getOrder().getCode());
        }
    }

    /**
     * @return the targetOrderEntryService
     */
    protected TargetOrderEntryService getTargetOrderEntryService() {
        return targetOrderEntryService;
    }

    /**
     * @param targetOrderEntryService
     *            the targetOrderEntryService to set
     */
    @Required
    public void setTargetOrderEntryService(final TargetOrderEntryService targetOrderEntryService) {
        this.targetOrderEntryService = targetOrderEntryService;
    }

    /**
     * @return the targetProductServiceImpl
     */
    protected TargetProductService getTargetProductServiceImpl() {
        return targetProductServiceImpl;
    }

    /**
     * @param targetProductServiceImpl
     *            the targetProductServiceImpl to set
     */
    @Required
    public void setTargetProductServiceImpl(final TargetProductService targetProductServiceImpl) {
        this.targetProductServiceImpl = targetProductServiceImpl;
    }

    /**
     * @return the catalogVersionService
     */
    protected CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }



}
