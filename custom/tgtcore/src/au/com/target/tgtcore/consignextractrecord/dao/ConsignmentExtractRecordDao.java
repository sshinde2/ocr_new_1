/**
 * 
 */
package au.com.target.tgtcore.consignextractrecord.dao;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;


/**
 * @author pratik
 *
 */
public interface ConsignmentExtractRecordDao {

    /**
     * Method to fetch ConsignmentExtractRecordModel by consignment
     * 
     * @param consignment
     * @return ConsignmentExtractRecordModel
     */
    ConsignmentExtractRecordModel getConsExtractRecordByConsignment(TargetConsignmentModel consignment);

    /**
     * Method to save ConsignmentExtractRecord model
     * 
     * @param consignment
     * @param consignmentAsId
     */
    void saveConsignmentExtractRecord(TargetConsignmentModel consignment, boolean consignmentAsId);

}
