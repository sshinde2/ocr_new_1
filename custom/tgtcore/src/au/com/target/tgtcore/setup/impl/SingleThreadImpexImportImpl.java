/**
 * 
 */
package au.com.target.tgtcore.setup.impl;

import de.hybris.platform.commerceservices.setup.impl.DefaultSetupImpexService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;

import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.setup.SingleThreadImpexImport;


public class SingleThreadImpexImportImpl extends DefaultSetupImpexService implements SingleThreadImpexImport {

    private static final Logger LOG = Logger.getLogger(SingleThreadImpexImportImpl.class);

    @Override
    public void importImpexFileSingleThread(final String file, final boolean errorIfMissing,
            final boolean legacyMode, final int maxThreads)
    {
        importImpexFileSingleThread(file, errorIfMissing, legacyMode, maxThreads, true);
    }

    protected void importImpexFileSingleThread(final String file, final InputStream stream, final boolean legacyMode,
            final int maxThreads, final boolean dumpingEnabled)
    {

        final String message = "Importing [" + file + "]...";

        try
        {
            LOG.info(message);

            final ImportConfig importConfig = new ImportConfig();
            importConfig.setMaxThreads(maxThreads);
            importConfig.setScript(new StreamBasedImpExResource(stream, getFileEncoding()));
            importConfig.setLegacyMode(Boolean.valueOf(legacyMode));
            importConfig.setDumpingEnabled(dumpingEnabled);

            final ImportResult importResult = getImportService().importData(importConfig);
            if (importResult.isError())
            {
                LOG.error(message + " FAILED");
            }
        }
        catch (final Exception e)
        {
            LOG.error(message + " FAILED", e);
        }
    }

    @Override
    public void importImpexFileSingleThread(final String file, final boolean errorIfMissing, final boolean legacyMode,
            final int maxThreads,
            final boolean dumpingEnabled)
    {
        final InputStream resourceAsStream = getClass().getResourceAsStream(file);
        if (resourceAsStream == null)
        {
            if (errorIfMissing)
            {
                LOG.error("Importing [" + file + "]... ERROR (MISSING FILE)", null);
            }
            else
            {
                LOG.info("Importing [" + file + "]... SKIPPED (Optional File Not Found)");
            }
        }
        else
        {
            importImpexFileSingleThread(file, resourceAsStream, legacyMode, maxThreads, dumpingEnabled);

            // Try to import language specific impex files
            if (file.endsWith(getImpexExt()))
            {
                final String filePath = file.substring(0, file.length() - getImpexExt().length());

                final List<LanguageModel> languages = getCommonI18NService().getAllLanguages();
                for (final LanguageModel language : languages)
                {
                    final String languageFilePath = filePath + "_" + language.getIsocode() + getImpexExt();
                    final InputStream languageResourceAsStream = getClass().getResourceAsStream(languageFilePath);
                    if (languageResourceAsStream != null)
                    {
                        importImpexFile(languageFilePath, languageResourceAsStream, legacyMode);
                    }
                }
            }
        }
    }
}
