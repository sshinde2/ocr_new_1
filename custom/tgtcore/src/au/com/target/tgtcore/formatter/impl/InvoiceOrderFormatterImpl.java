/**
 * 
 */
package au.com.target.tgtcore.formatter.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.formatter.InvoiceFormatterHelper;
import au.com.target.tgtcore.formatter.InvoiceOrderFormatter;
import au.com.target.tgtcore.formatter.data.PaymentInfoData;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceItems;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtutility.format.PriceFormatter;


/**
 * @author dcwillia
 * 
 */
public class InvoiceOrderFormatterImpl implements InvoiceOrderFormatter {
    private static final String VOUCHER_LABEL = "VOUCHERS";
    private static final String FLYBUYS_REDEEMED_LABEL = " FLYBUYS POINTS";
    private static final DecimalFormat COMMA_FORMAT = new DecimalFormat("#,###");
    private static final String CREDIT_CARD = "creditcard";
    private static final String PIN_PAD = "pinpad";
    private static final String PAYPAL = "paypal";
    private static final String IPG = "ipg";
    private static final String GIFTCARD = "giftcard";
    private static final String AFTERPAY = "afterpay";
    private static final String ZIPPAY = "zippay";
    private static final String CREDIT_CARD_DESC = "Credit Card";

    private InvoiceFormatterHelper invoiceFormatterHelper;
    private TargetVoucherService targetVoucherService;
    private TargetBarCodeService targetBarCodeService;
    private TargetDiscountService targetDiscountService;
    private TypeService typeService;

    @Override
    public void addOrderDetails(final VelocityContext vContext, final OrderModel order) {
        vContext.put("dAddr2", getStreetNumber(order.getDeliveryAddress()));
        vContext.put("pAddr2", getStreetNumber(order.getPaymentAddress()));

        addEbayDetails(vContext, order);

        final TaxInvoiceItems lineItems = invoiceFormatterHelper.getTaxInvoiceItems(order);
        addDealAndLineItems(vContext, lineItems);

        addVouchers(vContext, lineItems.getVouchers(), order);

        addStoreBarcodes(vContext, order);

        addTotals(vContext, order, lineItems);

        // Refelect the correct Credit Card Type name
        addPaymentMethod(vContext, order);
        addPaymentInfo(vContext, order);

        addMaskedFlybuysCode(vContext, order);
    }

    /**
     * @param vContext
     * @param order
     */
    protected void addMaskedFlybuysCode(final VelocityContext vContext, final OrderModel order) {
        final String maskedFlybuysNumber = invoiceFormatterHelper.getMaskedFlybuysNumber(order.getFlyBuysCode());
        if (StringUtils.isNotBlank(maskedFlybuysNumber)) {
            vContext.put("maskedFlyBuysCode", maskedFlybuysNumber);
        }
    }

    private String getStreetNumber(final AddressModel address) {
        if (address == null) {
            return "";
        }

        return address.getStreetnumber() == null ? "" : address.getStreetnumber();
    }

    protected void addEbayDetails(final VelocityContext vContext, final OrderModel order) {
        final SalesApplication salesApplication = order.getSalesApplication();
        if (SalesApplication.EBAY.equals(salesApplication)) {
            vContext.put("isEbayOrder", Boolean.TRUE);
            vContext.put("ebayOrderId", order.getEBayOrderNumber());
        }
        else {
            vContext.put("isEbayOrder", Boolean.FALSE);
        }
    }

    protected void addDealAndLineItems(final VelocityContext vContext, final TaxInvoiceItems lineItems) {
        if (CollectionUtils.isNotEmpty(lineItems.getTaxInvoiceLineItems())) {
            vContext.put("lineItem", lineItems.getTaxInvoiceLineItems());
        }

        if (CollectionUtils.isNotEmpty(lineItems.getTaxInvoiceDealSections())) {
            vContext.put("deals", lineItems.getTaxInvoiceDealSections());
        }

        final double itemsSavings = lineItems.getItemsSavings();
        if (itemsSavings > 0.0d) {
            vContext.put("itemSavings", PriceFormatter.twoDecimalFormat(itemsSavings));
        }
    }

    protected void addVouchers(final VelocityContext vContext, final double vouchers, final OrderModel order) {
        final FlybuysDiscountModel flybuysDiscount = targetVoucherService.getFlybuysDiscountForOrder(order);

        if (vouchers > 0.0d) {
            vContext.put("vouchers", PriceFormatter.twoDecimalFormat(vouchers));

            if (flybuysDiscount == null) {
                vContext.put("voucherLabel", VOUCHER_LABEL);
            }
            else {
                final String flybuysPointsConsumed = COMMA_FORMAT.format(order.getFlybuysPointsConsumed());
                vContext.put("voucherLabel", flybuysPointsConsumed + FLYBUYS_REDEEMED_LABEL);
            }
        }
    }

    protected void addStoreBarcodes(final VelocityContext vContext, final OrderModel order) {
        final String barcode = targetBarCodeService.getPlainBarcodeWithoutCheckDigit(order.getCode());
        vContext.put("barcode", barcode);
    }

    protected void addTotals(final VelocityContext vContext, final OrderModel order, final TaxInvoiceItems lineItems) {
        final double tmdDisCount = targetDiscountService.getTotalTMDiscount(order);
        final double subTotal = order.getSubtotal().doubleValue();
        // set TMD total Discount
        // Business need 2 decimal Subtotal, Delivery Fee, Total Amount tax etc.
        // so need to use the twoDecialFormat to format first before set to context.
        if (tmdDisCount > 0) {
            vContext.put("tmdDiscount", PriceFormatter.twoDecimalFormat(tmdDisCount));
        }
        vContext.put("subTotal", PriceFormatter.twoDecimalFormat(subTotal));
        vContext.put("deliveryCost", PriceFormatter.twoDecimalFormat(order.getDeliveryCost().doubleValue()));
        vContext.put("totalPrice", PriceFormatter.twoDecimalFormat(order.getTotalPrice().doubleValue()));
        vContext.put("totalTax", PriceFormatter.twoDecimalFormat(order.getTotalTax().doubleValue()));
        final double totalSavings = tmdDisCount + lineItems.getItemsSavings() + lineItems.getVouchers();
        if (totalSavings > 0.0d) {
            vContext.put("totalSavings", PriceFormatter.twoDecimalFormat(totalSavings));
        }
    }

    private List<PaymentTransactionType> getCaptureAndRefundTransactionStatus() {
        final List<PaymentTransactionType> types = new ArrayList<>();
        types.add(PaymentTransactionType.CAPTURE);
        types.add(PaymentTransactionType.REFUND_FOLLOW_ON);
        types.add(PaymentTransactionType.REFUND_STANDALONE);
        return types;
    }

    private List<PaymentTransactionType> getRefundTransactionStatus() {
        final List<PaymentTransactionType> types = new ArrayList<>();
        types.add(PaymentTransactionType.REFUND_FOLLOW_ON);
        types.add(PaymentTransactionType.REFUND_STANDALONE);
        return types;
    }

    protected List<PaymentInfoData> getPayments(final OrderModel order) {
        final List<PaymentInfoData> payments = new ArrayList<>();

        final PaymentTransactionModel paymentTransactionModel = PaymentTransactionHelper.findCaptureTransaction(order);
        if (paymentTransactionModel == null || CollectionUtils.isEmpty(paymentTransactionModel.getEntries())) {
            return payments;
        }

        final List<PaymentTransactionType> transactionStatusConsideredInTaxInvoice = getCaptureAndRefundTransactionStatus();
        final List<PaymentTransactionType> transactionStatusForRefund = getRefundTransactionStatus();

        for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionModel.getEntries()) {

            if (transactionStatusConsideredInTaxInvoice.contains(paymentTransactionEntryModel.getType())
                    && TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(
                            paymentTransactionEntryModel.getTransactionStatus())) {

                final PaymentInfoData payment = new PaymentInfoData();
                if (transactionStatusForRefund.contains(paymentTransactionEntryModel.getType())) {
                    payment.setAmount("-" + PriceFormatter.twoDecimalFormat(paymentTransactionEntryModel.getAmount()
                            .doubleValue()));
                }
                else {
                    payment.setAmount(PriceFormatter.twoDecimalFormat(paymentTransactionEntryModel.getAmount()
                            .doubleValue()));
                }

                final PaymentInfoModel paymentInfo = order.getPaymentInfo();

                if (order.getPaymentMode() != null && StringUtils.isNotEmpty(order.getPaymentMode().getCode())) {
                    final String paymentMode = order.getPaymentMode().getCode().toLowerCase();

                    switch (paymentMode) {
                        case PIN_PAD:
                            addPinPadPaymentData((PinPadPaymentInfoModel)paymentInfo, order, payment);
                            break;

                        case CREDIT_CARD:
                            addCreditCardPaymentData((CreditCardPaymentInfoModel)paymentInfo,
                                    paymentTransactionEntryModel, payment);
                            break;

                        case PAYPAL:
                            addPayPalPaymentData((PaypalPaymentInfoModel)paymentInfo, order,
                                    paymentTransactionEntryModel, payment);
                            break;

                        case GIFTCARD: //IPG split payment marks payment mode as 'giftcard'
                        case IPG:
                            addIpgPaymentData(paymentTransactionEntryModel, payment);
                            break;

                        case AFTERPAY:
                            addPartnerPaymentData(order, paymentTransactionEntryModel, payment);
                            break;

                        case ZIPPAY:
                            addPartnerPaymentData(order, paymentTransactionEntryModel, payment);
                            // For zip, show the order receipt number for refund entries as well
                            payment.setReceiptNumber(((ZippayPaymentInfoModel)paymentInfo).getReceiptNo());
                            break;

                        default:
                            break;
                    }
                    payments.add(payment);
                }
            }
        }
        return payments;
    }

    private void addPinPadPaymentData(final PinPadPaymentInfoModel pinPadPaymentInfo, final OrderModel order,
            final PaymentInfoData invoicePaymentInfo) {
        invoicePaymentInfo.setPaymentMethod(order.getPaymentMode().getName());
        invoicePaymentInfo.setAccountId(pinPadPaymentInfo.getMaskedCardNumber());
        invoicePaymentInfo.setReceiptNumber(pinPadPaymentInfo.getRrn());
        invoicePaymentInfo.setPaymentDate(pinPadPaymentInfo.getCreationtime());
    }

    private void addCreditCardPaymentData(final CreditCardPaymentInfoModel creditCardPaymentInfo,
            final PaymentTransactionEntryModel paymentTransactionEntry, final PaymentInfoData invoicePaymentInfo) {
        invoicePaymentInfo.setPaymentMethod(typeService.getEnumerationValue(creditCardPaymentInfo.getType()).getName());
        invoicePaymentInfo.setAccountId(creditCardPaymentInfo.getNumber());
        invoicePaymentInfo.setReceiptNumber(paymentTransactionEntry.getReceiptNo());
        invoicePaymentInfo.setPaymentDate(paymentTransactionEntry.getCreationtime());
    }

    private void addPayPalPaymentData(final PaypalPaymentInfoModel payPalPaymentInfo, final OrderModel order,
            final PaymentTransactionEntryModel paymentTransactionEntry, final PaymentInfoData invoicePaymentInfo) {
        invoicePaymentInfo.setPaymentMethod(order.getPaymentMode().getName());
        invoicePaymentInfo.setAccountId(payPalPaymentInfo.getEmailId());
        invoicePaymentInfo.setReceiptNumber(paymentTransactionEntry.getReceiptNo());
        invoicePaymentInfo.setPaymentDate(paymentTransactionEntry.getCreationtime());
    }

    private void addIpgPaymentData(final PaymentTransactionEntryModel paymentTransactionEntry,
            final PaymentInfoData invoicePaymentInfo) {
        final PaymentInfoModel ipgPaymentInfo = paymentTransactionEntry.getIpgPaymentInfo();
        if (ipgPaymentInfo != null) {
            if (ipgPaymentInfo instanceof IpgCreditCardPaymentInfoModel) {
                final IpgCreditCardPaymentInfoModel ipgCCInfo = (IpgCreditCardPaymentInfoModel)ipgPaymentInfo;

                invoicePaymentInfo.setPaymentMethod(typeService.getEnumerationValue(ipgCCInfo.getType())
                        .getName());
                invoicePaymentInfo.setAccountId(ipgCCInfo.getNumber());
            }
            else if (ipgPaymentInfo instanceof IpgGiftCardPaymentInfoModel) {
                final IpgGiftCardPaymentInfoModel gcInfo = (IpgGiftCardPaymentInfoModel)ipgPaymentInfo;

                invoicePaymentInfo.setPaymentMethod(typeService.getEnumerationValue(gcInfo.getCardType())
                        .getName());
                invoicePaymentInfo.setAccountId(gcInfo.getMaskedNumber());
            }
        }
        else { // stand alone refund
            invoicePaymentInfo.setPaymentMethod(CREDIT_CARD_DESC);
            invoicePaymentInfo.setAccountId("");
        }
        invoicePaymentInfo.setReceiptNumber(paymentTransactionEntry.getReceiptNo());
        invoicePaymentInfo.setPaymentDate(paymentTransactionEntry.getCreationtime());
    }

    private void addPartnerPaymentData(final OrderModel order,
            final PaymentTransactionEntryModel paymentTransactionEntry, final PaymentInfoData invoicePaymentInfo) {
        invoicePaymentInfo.setPaymentMethod(order.getPaymentMode().getName());
        invoicePaymentInfo.setAccountId(null);
        invoicePaymentInfo.setReceiptNumber(paymentTransactionEntry.getReceiptNo());
        invoicePaymentInfo.setPaymentDate(paymentTransactionEntry.getCreationtime());
    }

    private void addPaymentInfo(final VelocityContext vContext, final OrderModel order) {
        final List<PaymentInfoData> payments = getPayments(order);
        vContext.put("payments", payments);
    }

    private void addPaymentMethod(final VelocityContext vContext, final OrderModel order) {
        if (order.getPaymentMode() != null && CREDIT_CARD.equalsIgnoreCase(order.getPaymentMode().getCode())) {
            final PaymentInfoModel paymentInfo = order.getPaymentInfo();
            if (paymentInfo instanceof CreditCardPaymentInfoModel) {
                final CreditCardPaymentInfoModel ccInfo = (CreditCardPaymentInfoModel)paymentInfo;
                vContext.put("paymentMethod", typeService.getEnumerationValue(ccInfo.getType()).getName());
            }
        }
        else if (order.getPaymentMode() != null && PIN_PAD.equalsIgnoreCase(order.getPaymentMode().getCode())) {
            final PaymentInfoModel paymentInfo = order.getPaymentInfo();
            if (paymentInfo instanceof PinPadPaymentInfoModel) {
                final PinPadPaymentInfoModel pinPadInfo = (PinPadPaymentInfoModel)paymentInfo;
                vContext.put("paymentMethod", typeService.getEnumerationValue(pinPadInfo.getCardType()).getName());
            }
        }
    }


    /**
     * @param invoiceFormatterHelper
     *            the invoiceFormatterHelper to set
     */
    @Required
    public void setInvoiceFormatterHelper(final InvoiceFormatterHelper invoiceFormatterHelper) {
        this.invoiceFormatterHelper = invoiceFormatterHelper;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    /**
     * @param targetBarCodeService
     *            the targetBarCodeService to set
     */
    @Required
    public void setTargetBarCodeService(final TargetBarCodeService targetBarCodeService) {
        this.targetBarCodeService = targetBarCodeService;
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }
}
