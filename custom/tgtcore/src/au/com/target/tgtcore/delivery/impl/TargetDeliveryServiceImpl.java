/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.delivery.impl.DefaultDeliveryService;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.ordercancel.OrderUtils;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeDao;
import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueDao;
import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueRestrictionStrategy;
import au.com.target.tgtcore.delivery.dao.impl.TargetCountryZoneDeliveryModeDao;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.delivery.dto.DeliveryCostEnumType;
import au.com.target.tgtcore.delivery.dto.DeliveryShipsterStatusCode;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.PostCodeAwareZoneModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.shipster.ShipsterConfigService;
import au.com.target.tgtcore.shipster.dao.ShipsterDao;
import au.com.target.tgtutility.util.TargetProductUtils;



/**
 * The Class TargetDeliveryServiceImpl.
 */
public class TargetDeliveryServiceImpl extends DefaultDeliveryService implements TargetDeliveryService {
    private TargetZoneDeliveryModeDao targetZoneDeliveryModeDao;
    private TargetZoneDeliveryModeValueDao targetZoneDeliveryModeValueDao;
    private FlexibleSearchService flexibleSearchService;
    private String baseStoreId;
    private TargetDiscountService targetDiscountService;
    private TargetZoneDeliveryModeValueRestrictionStrategy modeValueRestrictionStrategy;
    private TargetCountryZoneDeliveryModeDao countryZoneDeliveryModeDao;
    private ShipsterDao shipsterDao;
    private TargetFeatureSwitchService targetFeatureSwitchService;
    private ShipsterConfigService shipsterConfigService;


    /**
     * Extend the default to consider the date of the order.
     */
    @Override
    public List<DeliveryModeModel> getSupportedDeliveryModeListForOrder(final AbstractOrderModel abstractOrder) {
        validateParameterNotNull(abstractOrder, "abstractOrderModel cannot be null");

        final CountryModel country = getCountryForOrder(abstractOrder);
        final CurrencyModel currency = getCurrencyForOrder(abstractOrder);

        if (currency != null && country != null) {
            final List<DeliveryModeModel> allDeliveryModes = new ArrayList<DeliveryModeModel>(
                    countryZoneDeliveryModeDao
                            .findDeliveryModes(country, currency,
                                    abstractOrder.getNet()));

            final List<DeliveryModeModel> activeDeliveryModes = new ArrayList<>();
            for (final DeliveryModeModel deliveryModeModel : allDeliveryModes) {
                try {
                    if (deliveryModeModel instanceof ZoneDeliveryModeModel
                            && CollectionUtils.isNotEmpty(this.getAllApplicableDeliveryValuesForModeAndOrder(
                                    (ZoneDeliveryModeModel)deliveryModeModel, abstractOrder))) {
                        activeDeliveryModes.add(deliveryModeModel);
                    }
                }
                catch (final TargetNoPostCodeException e) {
                    activeDeliveryModes.add(deliveryModeModel);
                }
            }

            sortDeliveryModes(activeDeliveryModes, abstractOrder);
            return activeDeliveryModes;
        }
        return Collections.emptyList();
    }

    @Override
    protected void sortDeliveryModes(final List<DeliveryModeModel> activeDeliveryModes,
            final AbstractOrderModel abstractOrder) {
        super.sortDeliveryModes(activeDeliveryModes, abstractOrder);
    }


    @Override
    public Collection<ZoneDeliveryModeValueModel> getSupportedDeliveryModeValuesForOrder(
            final AbstractOrderModel order) {
        validateParameterNotNull(order, "abstractOrderModel cannot be null");

        final Collection<ZoneDeliveryModeValueModel> values = new ArrayList<>();

        // go through each of the supported delivery modes and get associated value
        // the values will then be sorted in the same way as the delivery modes
        for (final DeliveryModeModel deliveryMode : getSupportedDeliveryModeListForOrder(order)) {

            if (deliveryMode instanceof TargetZoneDeliveryModeModel) {

                final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;

                final ZoneDeliveryModeValueModel valueForModeAndOrder = getZoneDeliveryModeValueForAbstractOrderAndMode(
                        targetDeliveryMode, order);

                if (valueForModeAndOrder != null) {
                    values.add(valueForModeAndOrder);
                }
            }
        }
        return values;
    }


    /**
     * Extends the default to ensure we are using TargetZoneDeliveryModeValue and the value is currently active.<br/>
     * If order is null then the ZoneDeliveryModeValue is retrieved for the base currency and country.
     */
    @Override
    public ZoneDeliveryModeValueModel getZoneDeliveryModeValueForAbstractOrderAndMode(
            final ZoneDeliveryModeModel deliveryMode,
            final AbstractOrderModel abstractOrder) {
        validateParameterNotNull(deliveryMode, "deliveryMode model cannot be null");
        validateParameterNotNull(abstractOrder, "abstractOrder model cannot be null");

        final CountryModel country = getCountryForOrder(abstractOrder);
        final CurrencyModel currency = getCurrencyForOrder(abstractOrder);

        // if there is a abstract order and a subtotal then get the delivery mode value for delivery mode.
        // It returns a sorted list based on minimum value. Get the first one that is less then the subtotal
        if (country != null && currency != null && abstractOrder != null && abstractOrder.getSubtotal() != null) {

            final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = getAllApplicableDeliveryValuesForModeAndOrder(
                    deliveryMode, abstractOrder);

            if (CollectionUtils.isEmpty(deliveryModeValues)) {
                return null;
            }

            return deliveryModeValues.get(0);
        }

        // if there is no abstract order then get the delivery mode value based on currency and country
        else {
            for (final ZoneDeliveryModeValueModel deliveryModeValue : deliveryMode.getValues()) {
                if (deliveryModeValue.getCurrency().equals(currency)
                        && deliveryModeValue.getZone().getCountries().contains(country)) {
                    if (deliveryModeValue instanceof TargetZoneDeliveryModeValueModel
                            && (BooleanUtils
                                    .isTrue(((TargetZoneDeliveryModeValueModel)deliveryModeValue).getActive()))) {
                        return deliveryModeValue;
                    }
                    else if (deliveryModeValue instanceof RestrictableTargetZoneDeliveryModeValueModel
                            && modeValueRestrictionStrategy.isApplicableModeValue(
                                    (RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValue, abstractOrder)) {
                        return deliveryModeValue;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndOrder(
            final ZoneDeliveryModeModel zoneDeliveryMode, final AbstractOrderModel abstractOrder) {
        return getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryMode, abstractOrder, false);
    }

    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndOrder(
            final ZoneDeliveryModeModel zoneDeliveryMode, final AbstractOrderModel abstractOrder,
            final boolean ignorePostcodeException) {
        validateParameterNotNull(zoneDeliveryMode, "zoneDeliveryMode model cannot be null");
        validateParameterNotNull(abstractOrder, "abstractOrder model cannot be null");

        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = getDeliveryValuesSortedByPriority(
                zoneDeliveryMode);
        if (CollectionUtils.isEmpty(deliveryModeValues)) {
            return null;
        }

        // We consider TMD as a part of Cart Total while checking Threshold of Delivery Value. Hence adding it!
        final double valueOfGoods = getValueOfGoods(abstractOrder);

        // check to make sure the delivery mode is active
        final List<AbstractTargetZoneDeliveryModeValueModel> activeDeliveryModeValues = new ArrayList<>();

        for (final AbstractTargetZoneDeliveryModeValueModel value : deliveryModeValues) {
            if (value != null && Double.valueOf(valueOfGoods).compareTo(value.getMinimum()) >= 0) {
                if (isActive(value, abstractOrder, ignorePostcodeException)) {
                    activeDeliveryModeValues.add(value);
                }
            }
        }
        if (CollectionUtils.isEmpty(activeDeliveryModeValues)) {
            return null;
        }
        return activeDeliveryModeValues;
    }

    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
            final ZoneDeliveryModeModel zoneDeliveryMode, final AbstractOrderModel abstractOrder) {
        validateParameterNotNull(zoneDeliveryMode, "zoneDeliveryMode model cannot be null");
        validateParameterNotNull(abstractOrder, "abstractOrder model cannot be null");

        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = getDeliveryValuesSortedByPriority(
                zoneDeliveryMode);

        // check to make sure the delivery mode is active
        final List<AbstractTargetZoneDeliveryModeValueModel> activeDeliveryModeValues = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deliveryModeValues)) {

            for (final AbstractTargetZoneDeliveryModeValueModel value : deliveryModeValues) {
                if (isActive(value, abstractOrder)) {
                    activeDeliveryModeValues.add(value);
                }
            }
        }
        if (CollectionUtils.isEmpty(activeDeliveryModeValues)) {
            return null;
        }
        return activeDeliveryModeValues;
    }

    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndProduct(
            final ZoneDeliveryModeModel zoneDeliveryMode, final ProductModel productModel) {
        validateParameterNotNull(zoneDeliveryMode, "zoneDeliveryMode model cannot be null");

        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = targetZoneDeliveryModeValueDao
                .findDeliveryValuesSortedByPriorityForProduct(zoneDeliveryMode);
        if (CollectionUtils.isEmpty(deliveryModeValues)) {
            return null;
        }
        // check to make sure the delivery mode is active
        final List<AbstractTargetZoneDeliveryModeValueModel> activeDeliveryModeValues = new ArrayList<>();
        for (final AbstractTargetZoneDeliveryModeValueModel value : deliveryModeValues) {
            if (value instanceof TargetZoneDeliveryModeValueModel) {
                final TargetZoneDeliveryModeValueModel targetModeValueModel = (TargetZoneDeliveryModeValueModel)value;
                if (BooleanUtils.isTrue(targetModeValueModel.getActive())) {
                    activeDeliveryModeValues.add(targetModeValueModel);
                }
            }
            else if (value instanceof RestrictableTargetZoneDeliveryModeValueModel
                    && modeValueRestrictionStrategy.isApplicableModeValue(
                            (RestrictableTargetZoneDeliveryModeValueModel)value, productModel)) {
                activeDeliveryModeValues.add(value);
            }
        }

        if (CollectionUtils.isEmpty(activeDeliveryModeValues)) {
            return null;
        }
        return activeDeliveryModeValues;
    }

    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> getDeliveryValue(final ZoneModel zone,
            final CurrencyModel currency,
            final Double min,
            final ZoneDeliveryModeModel zoneDeliveryMode) {

        validateParameterNotNullStandardMessage("currency", currency);
        validateParameterNotNullStandardMessage("zone", zone);
        validateParameterNotNullStandardMessage("min", min);
        validateParameterNotNullStandardMessage("zoneDeliveryMode", zoneDeliveryMode);
        return targetZoneDeliveryModeValueDao
                .findDeliveryValues(zone,
                        currency, min, zoneDeliveryMode);
    }

    @Override
    public List<RegionModel> getRegionsForCountry(final CountryModel country) {

        final List<RegionModel> retList = new ArrayList<>();

        // First get all regions
        final List<RegionModel> listAllRegions = getCommonI18NService().getAllRegions();

        // If there are no regions then return
        if (CollectionUtils.isEmpty(listAllRegions)) {
            return retList;
        }

        // If no parameter then return list of all regions
        if (country == null) {
            return listAllRegions;
        }

        // Filter out all regions not for the given country
        for (final RegionModel state : listAllRegions) {
            if (state.getCountry().equals(country)) {
                retList.add(state);
            }
        }

        return retList;
    }

    @Override
    public Set<DeliveryModeModel> getVariantsDeliveryModesFromBaseProduct(final ProductModel productModel) {
        final VariantProductModel variant = (VariantProductModel)productModel;

        ProductModel product = variant;
        while (product instanceof VariantProductModel) {
            product = ((VariantProductModel)product).getBaseProduct();
        }
        if (product == null) {
            return Collections.EMPTY_SET;
        }

        final Set<DeliveryModeModel> productDeliveryModes = product.getDeliveryModes();

        if (TargetProductUtils.isProductOpenForPreOrder(productModel)) {
            return filterDeliveryModesPreOrder(productDeliveryModes);
        }
        return productDeliveryModes;
    }

    /**
     * This method will filter the express delivery mode for pre-order product
     * 
     * @param productDeliveryModes
     */
    private Set<DeliveryModeModel> filterDeliveryModesPreOrder(final Set<DeliveryModeModel> productDeliveryModes) {

        final Set<DeliveryModeModel> preOrderDeliveryModes = new HashSet<>();
        for (final DeliveryModeModel deliveryMode : productDeliveryModes) {
            if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
                final TargetZoneDeliveryModeModel zoneDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
                if (zoneDeliveryMode.isAvailableForPreOrder()) {
                    preOrderDeliveryModes.add(zoneDeliveryMode);
                }
            }
        }
        return preOrderDeliveryModes;
    }

    @Override
    public Collection<TargetZoneDeliveryModeModel> getAllApplicableDeliveryModesForProduct(
            final ProductModel productModel) {
        validateParameterNotNull(productModel, "Product Model cannot be null");

        final Collection<TargetZoneDeliveryModeModel> allDeliveryModes = getTargetZoneDeliveryModeDao()
                .findAllCurrentlyActiveDeliveryModesForProduct(productModel);

        if (CollectionUtils.isEmpty(allDeliveryModes)) {
            return allDeliveryModes;
        }

        final Collection<TargetZoneDeliveryModeModel> activeDeliveryModesForProduct = new ArrayList<>();
        for (final TargetZoneDeliveryModeModel deliveryModeModel : allDeliveryModes) {
            if (CollectionUtils.isNotEmpty(this.getAllApplicableDeliveryValuesForModeAndProduct(deliveryModeModel,
                    productModel))) {
                activeDeliveryModesForProduct.add(deliveryModeModel);
            }
        }
        return activeDeliveryModesForProduct;
    }

    @Override
    public DeliveryCostDto getDeliveryCostForDeliveryType(final AbstractTargetZoneDeliveryModeValueModel deliveryValue,
            final AbstractOrderModel order) {
        double shippingValue = 0.0;
        if (order instanceof CartModel && BooleanUtils.isTrue(((CartModel)order).getCockpitFreeDelivery())) {
            final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
            deliveryCostDto.setDeliveryCost(shippingValue);
            deliveryCostDto.setOriginalDeliveryCost(shippingValue);
            deliveryCostDto.setType(DeliveryCostEnumType.COCKPITFREEDELIVERY);
            return deliveryCostDto;
        }

        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
        if (deliveryValue != null && CollectionUtils.isNotEmpty(orderEntries) && OrderUtils.hasLivingEntries(order)) {

            // Just map each order entry direct to a ProductQuantity
            final List<ProductQuantity> prodQuantities = new ArrayList<>();

            for (final AbstractOrderEntryModel orderEntry : orderEntries) {
                final AbstractTargetVariantProductModel variant = (AbstractTargetVariantProductModel)orderEntry
                        .getProduct();

                final int qty = orderEntry.getQuantity() == null ? 1 : orderEntry.getQuantity().intValue();

                prodQuantities.add(new ProductQuantity(variant, qty));
            }

            shippingValue = getShippingValueFromTargetDeliveryZoneValue(deliveryValue, prodQuantities);
        }

        return calculateFinalDeliveryCost(deliveryValue, order, shippingValue);
    }

    private DeliveryCostDto calculateFinalDeliveryCost(
            final AbstractTargetZoneDeliveryModeValueModel deliveryValue,
            final AbstractOrderModel order, final double shippingValue) {
        final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
        if (targetFeatureSwitchService.isFeatureEnabled("shipster.available")
                && checkShipsterFreeDelivery(deliveryValue, order, shippingValue, deliveryCostDto)) {
            deliveryCostDto.setType(DeliveryCostEnumType.SHIPSTERFREEDELIVERY);
            deliveryCostDto.setOriginalDeliveryCost(shippingValue);
            deliveryCostDto.setDeliveryCost(0);
        }
        else {
            double recalcuatedShippingValue = shippingValue;
            if (BooleanUtils.isTrue(order.getContainsPreOrderItems())) {
                final Double maxPreOrderValue = ((TargetZoneDeliveryModeModel)deliveryValue.getDeliveryMode())
                        .getMaxPreOrderValue();
                if (maxPreOrderValue != null) {
                    recalcuatedShippingValue = maxPreOrderValue.doubleValue() < shippingValue
                            ? maxPreOrderValue.doubleValue() : shippingValue;
                }
            }
            deliveryCostDto.setType(DeliveryCostEnumType.NORMAL);
            deliveryCostDto.setDeliveryCost(recalcuatedShippingValue);
            deliveryCostDto.setOriginalDeliveryCost(shippingValue);
        }
        return deliveryCostDto;
    }

    private boolean checkShipsterFreeDelivery(final AbstractTargetZoneDeliveryModeValueModel deliveryValue,
            final AbstractOrderModel order, final double shippingValue, final DeliveryCostDto costDto) {
        if (deliveryValue == null) {
            return false;
        }
        final ZoneDeliveryModeModel deliveryMode = deliveryValue.getDeliveryMode();
        if (!(deliveryMode instanceof TargetZoneDeliveryModeModel)) {
            return false;
        }
        if (BooleanUtils.isFalse(order.getAusPostClubMember())) {
            costDto.setShipsterStatusCode(DeliveryShipsterStatusCode.ERR_SHIPSTER_NOT_SUBSCRIBED);
            return false;
        }
        final ShipsterConfigModel shipsterConfigModel = shipsterConfigService
                .getShipsterModelForDeliveryModes((TargetZoneDeliveryModeModel)deliveryMode);
        if (shipsterConfigModel == null) {
            return false;
        }
        if (BooleanUtils.isFalse(shipsterConfigModel.getActive())) {
            costDto.setShipsterStatusCode(DeliveryShipsterStatusCode.ERR_SHIPSTER_NOT_CONFIGURED);
            return false;
        }
        if (!checkOrderTotalThreshold(shipsterConfigModel, order)) {
            costDto.setShipsterStatusCode(DeliveryShipsterStatusCode.ERR_SHIPSTER_LOW_VALUE);
            return false;
        }
        if (!checkDeliveryCostThreshold(shipsterConfigModel, shippingValue)) {
            costDto.setShipsterStatusCode(DeliveryShipsterStatusCode.ERR_SHIPSTER_HIGH_DELIVERY);
            return false;
        }
        return true;
    }

    private boolean checkDeliveryCostThreshold(final ShipsterConfigModel shipsterConfigModel,
            final double shippingValue) {
        final Double maxShippingThreshold = shipsterConfigModel.getMaxShippingThresholdValue();
        if (maxShippingThreshold == null) {
            return false;
        }
        return shippingValue <= maxShippingThreshold.doubleValue();
    }

    private boolean checkOrderTotalThreshold(final ShipsterConfigModel shipsterConfigModel,
            final AbstractOrderModel order) {
        final Double minOrderTotal = shipsterConfigModel.getMinOrderTotalValue();
        if (minOrderTotal == null || order.getTotalPrice() == null) {
            return false;
        }
        final double orderTotal = order.getSubtotal().doubleValue()
                - (order.getTotalDiscounts() != null ? order.getTotalDiscounts().doubleValue() : 0d);
        return orderTotal >= minOrderTotal.doubleValue();
    }

    @Override
    public boolean containsBulkyProducts(final List<AbstractOrderEntryModel> orderEntryList) {

        if (CollectionUtils.isEmpty(orderEntryList)) {
            return false;
        }

        for (final AbstractOrderEntryModel entry : orderEntryList) {
            if (isEntryBulkyType(entry)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean consignmentContainsBulkyProducts(final Collection<ConsignmentEntryModel> consignmentEntry) {

        if (CollectionUtils.isEmpty(consignmentEntry)) {
            return false;
        }

        for (final ConsignmentEntryModel entry : consignmentEntry) {

            final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();
            if (isEntryBulkyType(orderEntry)) {
                return true;
            }
        }

        return false;
    }

    private boolean isEntryBulkyType(final AbstractOrderEntryModel orderEntry) {

        if (null == orderEntry || null == orderEntry.getProduct()) {
            return false;
        }

        final AbstractTargetVariantProductModel product = (AbstractTargetVariantProductModel)orderEntry
                .getProduct();

        if (null == product.getProductType() || BooleanUtils.isNotTrue(product.getProductType().getBulky())) {
            return false;
        }

        return true;
    }

    /**
     * Gets the shipping value from target delivery zone value.
     * 
     * @param deliveryValue
     *            the delivery value
     * @param prodQuantities
     *            the prod quantities
     * @return the shipping value from target delivery zone value
     */
    private double getShippingValueFromTargetDeliveryZoneValue(
            final AbstractTargetZoneDeliveryModeValueModel deliveryValue,
            final List<ProductQuantity> prodQuantities) {
        final double shippingValue;
        final int bulkyQty = getBulkyQuantity(prodQuantities);
        if (deliveryValue instanceof TargetZoneDeliveryModeValueModel) {
            shippingValue = getDeliveryCostGivenBulkyQuantity((TargetZoneDeliveryModeValueModel)deliveryValue,
                    bulkyQty);
        }
        else {
            shippingValue = deliveryValue.getValue().doubleValue();
        }
        return shippingValue;
    }

    /**
     * Calculate the delivery cost given the deliveryValue and quantity of bulky items
     * 
     * @param deliveryValue
     * @param bulkyQty
     * @return double
     */
    protected double getDeliveryCostGivenBulkyQuantity(final TargetZoneDeliveryModeValueModel deliveryValue,
            final int bulkyQty) {

        double shippingValue = 0.0;

        if (bulkyQty > 0) {
            if (bulkyQty > TgtCoreConstants.EXTENDED_BULKY_THRESHOLD) {
                shippingValue = deliveryValue.getExtendedBulky().doubleValue();
            }
            else {
                shippingValue = deliveryValue.getBulky().doubleValue();
            }
        }
        else {
            shippingValue = deliveryValue.getValue().doubleValue();
        }

        return shippingValue;
    }

    /**
     * Return the quantity of bulky items given the list of products and quantities
     * 
     * @param prodQuantities
     * @return quantity
     */
    protected int getBulkyQuantity(final List<ProductQuantity> prodQuantities) {
        Assert.notNull(prodQuantities, "prodQuantities should not be null");

        int bulkyQty = 0;

        for (final ProductQuantity prodQuantity : prodQuantities) {

            if (prodQuantity.product != null && prodQuantity.product.getProductType().getBulky().booleanValue()) {
                bulkyQty += prodQuantity.quantity;
            }
        }

        return bulkyQty;
    }

    // Checkstyle off since deliberately using a struct for ease
    // CHECKSTYLE:OFF
    protected class ProductQuantity {

        public ProductQuantity(final AbstractTargetVariantProductModel product, final int quantity) {
            this.product = product;
            this.quantity = quantity;
        }

        int quantity;
        AbstractTargetVariantProductModel product;
    }

    // CHECKSTYLE:ON

    @Override
    public List<AddressModel> getSupportedDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
            final boolean visibleAddressesOnly) {
        Assert.notNull(abstractOrder, "There must be a cart or order present");

        final List<AddressModel> addressesForOrder = super.getSupportedDeliveryAddressesForOrder(abstractOrder,
                visibleAddressesOnly);

        if (CollectionUtils.isNotEmpty(addressesForOrder)) {
            sortAddresses(addressesForOrder);
        }

        return addressesForOrder;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetDeliveryService#getAllApplicableDeliveryModesForOrder(AbstractOrderModel ,SalesApplication)
     */
    @Override
    public Collection<TargetZoneDeliveryModeModel> getAllApplicableDeliveryModesForOrder(
            final AbstractOrderModel abstractOrder, final SalesApplication salesApplication) {
        final Collection<TargetZoneDeliveryModeModel> allDeliveryModes = getTargetZoneDeliveryModeDao()
                .findAllCurrentlyActiveDeliveryModes(salesApplication);

        if (CollectionUtils.isEmpty(allDeliveryModes)) {
            return allDeliveryModes;
        }

        final Collection<TargetZoneDeliveryModeModel> activeDeliveryModes = new ArrayList<>();
        for (final TargetZoneDeliveryModeModel deliveryModeModel : allDeliveryModes) {
            try {
                if (CollectionUtils.isNotEmpty(this.getAllApplicableDeliveryValuesForModeAndOrder(deliveryModeModel,
                        abstractOrder))) {
                    activeDeliveryModes.add(deliveryModeModel);
                }
            }
            catch (final TargetNoPostCodeException ex) {
                activeDeliveryModes.add(deliveryModeModel);
            }

        }
        return activeDeliveryModes;
    }

    @Override
    public TargetZoneDeliveryModeModel getDefaultDeliveryMode() {
        return getTargetZoneDeliveryModeDao().getDefaultDeliveryMode();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetDeliveryService#getAllCurrentlyActiveDeliveryModes(SalesApplication)
     */
    @Override
    public Collection<TargetZoneDeliveryModeModel> getAllCurrentlyActiveDeliveryModes(
            final SalesApplication salesApplication) {
        return getTargetZoneDeliveryModeDao()
                .findAllCurrentlyActiveDeliveryModes(salesApplication);
    }

    private static class TargetAddressComparator extends AbstractComparator<AddressModel> {
        private static final TargetAddressComparator INSTANCE = new TargetAddressComparator();

        @Override
        protected int compareInstances(final AddressModel add1, final AddressModel add2) {
            final UserModel user = (UserModel)add1.getOwner();
            final AddressModel defaultAddress = user.getDefaultShipmentAddress();
            // If the address1 is the default address then return -1 if address2 is default return 1, so default address will be first
            // otherwise compare the creation time and sort by newest
            if (defaultAddress != null && defaultAddress.equals(add1)) {
                return -1;
            }

            if (defaultAddress != null && defaultAddress.equals(add2)) {
                return 1;
            }

            final int comp = compareValues(add2.getCreationtime(), add1.getCreationtime());
            return comp;
        }
    }

    protected List<AddressModel> sortAddresses(final List<AddressModel> addressesForOrder) {
        Collections.sort(addressesForOrder, TargetAddressComparator.INSTANCE);
        return addressesForOrder;
    }

    /**
     * Get currency from order otherwise try the default store currency
     * 
     * @param abstractOrder
     * @return currency
     */
    private CurrencyModel getCurrencyForOrder(final AbstractOrderModel abstractOrder) {

        CurrencyModel currency = null;

        if (abstractOrder != null) {
            currency = abstractOrder.getCurrency();
        }

        if (currency == null) {
            currency = getBaseStoreCurrency();
        }
        return currency;
    }

    /**
     * Get country from order otherwise try the default store country
     * 
     * @param abstractOrder
     * @return country
     */
    private CountryModel getCountryForOrder(final AbstractOrderModel abstractOrder) {

        CountryModel country = null;

        if (abstractOrder != null) {
            final AddressModel deliveryAddress = abstractOrder.getDeliveryAddress();
            if (deliveryAddress != null) {
                country = deliveryAddress.getCountry();
            }
        }

        if (country == null) {
            country = getBaseStoreCountry();
        }
        return country;
    }

    private CurrencyModel getBaseStoreCurrency() {
        final BaseStoreModel baseStore = getBaseStore();
        if (baseStore != null) {
            final Collection<CurrencyModel> currencies = baseStore.getCurrencies();
            if (currencies != null && currencies.size() == 1) {
                return currencies.iterator().next();
            }
        }
        return getAustralianDollar();
    }

    private CurrencyModel getAustralianDollar() {
        final CurrencyModel example = new CurrencyModel();
        example.setIsocode("AUD");
        final List<CurrencyModel> list = flexibleSearchService.getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    private CountryModel getBaseStoreCountry() {
        final BaseStoreModel baseStore = getBaseStore();
        if (baseStore != null) {
            final Collection<CountryModel> countries = baseStore.getDeliveryCountries();
            if (countries != null && countries.size() == 1) {
                return countries.iterator().next();
            }
        }

        return getAustralia();
    }

    private CountryModel getAustralia() {
        final CountryModel example = new CountryModel();
        example.setIsocode("AU");
        final List<CountryModel> list = flexibleSearchService.getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    private BaseStoreModel getBaseStore() {
        final BaseStoreModel example = new BaseStoreModel();
        example.setUid(baseStoreId);
        final List<BaseStoreModel> list = flexibleSearchService.getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Required
    public void setBaseStoreId(final String id) {
        this.baseStoreId = id;
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    /**
     * @param targetZoneDeliveryModeValueDao
     *            the targetZoneDeliveryModeValueDao to set
     */
    @Required
    public void setTargetZoneDeliveryModeValueDao(final TargetZoneDeliveryModeValueDao targetZoneDeliveryModeValueDao) {
        this.targetZoneDeliveryModeValueDao = targetZoneDeliveryModeValueDao;
    }

    /**
     * @return the targetZoneDeliveryModeDao
     */
    public TargetZoneDeliveryModeDao getTargetZoneDeliveryModeDao() {
        return targetZoneDeliveryModeDao;
    }

    /**
     * @param targetZoneDeliveryModeDao
     *            the targetZoneDeliveryModeDao to set
     */
    @Required
    public void setTargetZoneDeliveryModeDao(final TargetZoneDeliveryModeDao targetZoneDeliveryModeDao) {
        this.targetZoneDeliveryModeDao = targetZoneDeliveryModeDao;
    }

    /**
     * @param modeValueRestrictionStrategy
     *            the modeValueRestrictionStrategy to set
     */
    @Required
    public void setModeValueRestrictionStrategy(
            final TargetZoneDeliveryModeValueRestrictionStrategy modeValueRestrictionStrategy) {
        this.modeValueRestrictionStrategy = modeValueRestrictionStrategy;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetDeliveryService#isDeliveryModePostCodeCombinationValid(de.hybris.platform.core.model.order.delivery.DeliveryModeModel, au.com.target.tgtcore.jalo.PostCode)
     */
    @Override
    public boolean isDeliveryModePostCodeCombinationValid(final DeliveryModeModel deliveryMode, final String postcode,
            final AbstractOrderModel abstractOrder) {
        if (deliveryMode instanceof ZoneDeliveryModeModel) {
            final List<AbstractTargetZoneDeliveryModeValueModel> targetZoneDeliveryModeValueList = getAllApplicableDeliveryValuesForModeAndOrder(
                    (ZoneDeliveryModeModel)deliveryMode, abstractOrder, true);
            if (targetZoneDeliveryModeValueList != null) {
                for (final AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue : targetZoneDeliveryModeValueList) {
                    final ZoneModel zoneModel = targetZoneDeliveryModeValue.getZone();
                    if (zoneModel instanceof PostCodeAwareZoneModel) {
                        final PostCodeAwareZoneModel postCodeAwareZone = (PostCodeAwareZoneModel)zoneModel;
                        final Collection<PostCodeModel> postCodeModels = postCodeAwareZone.getPostCodes();
                        if (postCodeExistsInCollection(postCodeModels, postcode)) {
                            return true;
                        }
                    }
                    else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetDeliveryService#isDeliveryModePostCodeCombinationValid(de.hybris.platform.core.model.order.delivery.DeliveryModeModel, de.hybris.platform.core.model.user.AddressModel)
     */
    @Override
    public boolean isDeliveryModePostCodeCombinationValid(final DeliveryModeModel deliveryMode,
            final AddressModel address, final AbstractOrderModel abstractOrder) {
        final String postCode = address.getPostalcode();
        return isDeliveryModePostCodeCombinationValid(deliveryMode, postCode, abstractOrder);
    }

    private boolean postCodeExistsInCollection(final Collection<PostCodeModel> postCodeModels, final String postcode) {
        if (postCodeModels != null) {
            for (final PostCodeModel postCodeModel : postCodeModels) {
                if (postCodeModel.getPostCode().equals(postcode)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param countryZoneDeliveryModeDao
     *            the countryZoneDeliveryModeDao to set
     */
    @Required
    public void setCountryZoneDeliveryModeDao(final TargetCountryZoneDeliveryModeDao countryZoneDeliveryModeDao) {
        this.countryZoneDeliveryModeDao = countryZoneDeliveryModeDao;
    }

    protected boolean isActive(final AbstractTargetZoneDeliveryModeValueModel value,
            final AbstractOrderModel abstractOrder) {
        return isActive(value, abstractOrder, false);
    }

    protected boolean isActive(final AbstractTargetZoneDeliveryModeValueModel value,
            final AbstractOrderModel abstractOrder, final boolean ignorePostcodeException) {
        try {
            if (value instanceof TargetZoneDeliveryModeValueModel) {
                final TargetZoneDeliveryModeValueModel targetModeValueModel = (TargetZoneDeliveryModeValueModel)value;
                if (BooleanUtils.isTrue(targetModeValueModel.getActive())) {
                    return true;
                }

            }
            else if (value instanceof RestrictableTargetZoneDeliveryModeValueModel
                    && modeValueRestrictionStrategy.isApplicableModeValue(
                            (RestrictableTargetZoneDeliveryModeValueModel)value, abstractOrder)) {
                return true;
            }
        }
        catch (final TargetNoPostCodeException e) {
            if (!ignorePostcodeException) {
                throw e;
            }
        }
        return false;
    }

    /**
     * @param zoneDeliveryMode
     * @return deliveryModeValues
     */
    protected List<AbstractTargetZoneDeliveryModeValueModel> getDeliveryValuesSortedByPriority(
            final ZoneDeliveryModeModel zoneDeliveryMode) {
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = targetZoneDeliveryModeValueDao
                .findDeliveryValuesSortedByPriority(zoneDeliveryMode);

        if (CollectionUtils.isEmpty(deliveryModeValues)) {
            return null;
        }
        return deliveryModeValues;
    }

    /**
     * @param abstractOrder
     * @return double
     */
    protected double getValueOfGoods(final AbstractOrderModel abstractOrder) {
        // We consider TMD as a part of Cart Total while checking Threshold of Delivery Value. Hence adding it!
        double valueOfGoods = 0.0d;
        valueOfGoods = abstractOrder.getSubtotal().doubleValue()
                + targetDiscountService.getTotalTMDiscount(abstractOrder);
        valueOfGoods = getCommonI18NService().roundCurrency(valueOfGoods,
                abstractOrder.getCurrency().getDigits().intValue());
        return valueOfGoods;
    }

    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> getApplicableDeliveryValuesForModeAndProductWithThreshold(
            final ZoneDeliveryModeModel zoneDeliveryMode, final ProductModel productModel,
            final double minCostOfProduct) {
        final List<AbstractTargetZoneDeliveryModeValueModel> applicableDeliveryValuesForProduct = new ArrayList<>();
        final List<AbstractTargetZoneDeliveryModeValueModel> allDeliveryValuesForDeliveryMode = getDeliveryValuesSortedByPriority(
                zoneDeliveryMode);
        if (CollectionUtils.isNotEmpty(allDeliveryValuesForDeliveryMode)) {
            for (final AbstractTargetZoneDeliveryModeValueModel value : allDeliveryValuesForDeliveryMode) {
                if (isDeliveryFeeApplicableToProduct(productModel, minCostOfProduct, value)) {
                    applicableDeliveryValuesForProduct.add(value);
                }
            }
        }
        if (CollectionUtils.isEmpty(applicableDeliveryValuesForProduct)) {
            return null;
        }
        return applicableDeliveryValuesForProduct;
    }

    /**
     * Method to check if delivery fee is applicable to product, if products minimum cost is greater than or equal to
     * the threshold of the delivery value and the delivery value is applicable for the product
     * 
     * @param productModel
     * @param minCostOfProduct
     * @param value
     * @return boolean
     */
    private boolean isDeliveryFeeApplicableToProduct(final ProductModel productModel,
            final double minCostOfProduct, final AbstractTargetZoneDeliveryModeValueModel value) {
        return isValueOfTypeRestrictableTargetZoneDeliveryModeValue(value)
                && modeValueRestrictionStrategy.isApplicableModeValue(
                        (RestrictableTargetZoneDeliveryModeValueModel)value, productModel)
                && minCostOfProduct >= value.getMinimum().doubleValue();
    }

    /**
     * Method to check if delivery value is not null and of type 'RestrictableTargetZoneDeliveryModeValueModel'
     * 
     * @param value
     * @return boolean
     */
    private boolean isValueOfTypeRestrictableTargetZoneDeliveryModeValue(
            final AbstractTargetZoneDeliveryModeValueModel value) {
        return value != null && value instanceof RestrictableTargetZoneDeliveryModeValueModel;
    }

    @Override
    public boolean isFreeDeliveryApplicableOnProductForDeliveryMode(final ProductModel product,
            final double minCostOfProduct, final ZoneDeliveryModeModel zoneDeliveryMode) {
        final List<AbstractTargetZoneDeliveryModeValueModel> applicableDeliveryValuesForProduct = getApplicableDeliveryValuesForModeAndProductWithThreshold(
                zoneDeliveryMode, product, minCostOfProduct);
        return (CollectionUtils.isNotEmpty(applicableDeliveryValuesForProduct)
                && NumberUtils.compare(applicableDeliveryValuesForProduct.get(0).getValue().doubleValue(),
                        NumberUtils.DOUBLE_ZERO.doubleValue()) == 0);
    }

    @Override
    public Collection<ShipsterConfigModel> getShipsterModelForDeliveryModes(
            final Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModel) {
        if (CollectionUtils.isEmpty(targetZoneDeliveryModeModel)) {
            return Collections.emptyList();
        }
        return shipsterDao.getShipsterModelForDeliveryModes(targetZoneDeliveryModeModel);
    }

    /**
     * @param shipsterDao
     *            the shipsterModelDao to set
     */
    public void setShipsterDao(final ShipsterDao shipsterDao) {
        this.shipsterDao = shipsterDao;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param shipsterConfigService
     *            the shipsterConfigService to set
     */
    @Required
    public void setShipsterConfigService(final ShipsterConfigService shipsterConfigService) {
        this.shipsterConfigService = shipsterConfigService;
    }



}
