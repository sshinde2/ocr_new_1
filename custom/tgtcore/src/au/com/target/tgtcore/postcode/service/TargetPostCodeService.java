/**
 * 
 */
package au.com.target.tgtcore.postcode.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Collection;

import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.PostCodeModel;



/**
 * @author rmcalave
 * 
 */
public interface TargetPostCodeService {

    /**
     * Retrieve a {code}PostCodeModel{code} by post code.
     * 
     * @param code
     *            The post code to retrieve
     * @return The model representing the provided post code
     */
    PostCodeModel getPostCode(String code);

    /**
     * Method to find out if given post code belongs to given post code groups. Returns true if it, false otherwise.
     *
     * @param postCode
     *            the post code
     * @param pcGroups
     *            the pc groups
     * @return true, if successful
     */
    boolean doesPostCodeBelongsToGroups(String postCode, Collection<PostCodeGroupModel> pcGroups);

    /**
     * Get the post code from cart if it exists in cart or get it from session OR else empty
     * 
     * @param abstractOrder
     * @return String
     */
    String getPostalCodeFromCartOrSession(final AbstractOrderModel abstractOrder);

    /**
     * Get the post code group by post code
     * 
     * @param postcode
     * @return Collection<PostCodeGroupModel>
     */
    Collection<PostCodeGroupModel> getPostCodeGroupByPostcode(String postcode);

}