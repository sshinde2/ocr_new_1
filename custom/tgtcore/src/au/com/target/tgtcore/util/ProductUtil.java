/**
 * 
 */
package au.com.target.tgtcore.util;


import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;
import au.com.target.tgtcore.jalo.ProductType;
import au.com.target.tgtcore.jalo.TargetProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author Nandini
 *
 */
@SuppressWarnings({ "deprecation", "deprecation" })
public class ProductUtil {

    private ProductUtil() {
    }

    /**
     * Retrieves the product type code
     * 
     * @param orderEntryProduct
     * @return product type code
     */
    public static String getProductTypeCode(final Product orderEntryProduct) {
        if (orderEntryProduct instanceof TargetProduct) {
            final TargetProduct product = (TargetProduct)orderEntryProduct;
            final ProductType productType = product.getProductType();
            return productType != null ? productType.getCode() : null;
        }
        if (orderEntryProduct instanceof AbstractTargetVariantProduct) {
            final AbstractTargetVariantProduct variantProduct = (AbstractTargetVariantProduct)orderEntryProduct;
            return getProductTypeCode(variantProduct.getBaseProduct());
        }
        return null;
    }

    /**
     * Retrieves the product type code from the ProductModel
     * 
     * @param orderEntryPrdModel
     * @return product type code
     */
    public static String getProductTypeCode(final ProductModel orderEntryPrdModel) {
        if (orderEntryPrdModel instanceof TargetProductModel) {
            final TargetProductModel targetProductModel = (TargetProductModel)orderEntryPrdModel;
            final ProductTypeModel productType = targetProductModel.getProductType();
            return productType != null ? productType.getCode() : null;
        }
        if (orderEntryPrdModel instanceof AbstractTargetVariantProductModel) {
            final AbstractTargetVariantProductModel variantProductModel = (AbstractTargetVariantProductModel)orderEntryPrdModel;
            return getProductTypeCode(variantProductModel.getBaseProduct());
        }
        return null;
    }

    /**
     * verifies if product is digital giftcard
     * 
     * @param orderEntryPrdModel
     * @return boolean
     */
    public static boolean isProductTypeDigital(final ProductModel orderEntryPrdModel) {
        final String productTypeCode = getProductTypeCode(orderEntryPrdModel);
        return TgtCoreConstants.DIGITAL.equalsIgnoreCase(productTypeCode);
    }

    /**
     * verifies if product is physical giftcard.
     * 
     * @param orderEntryPrdModel
     * @return boolean
     */
    public static boolean isProductTypePhysicalGiftcard(final ProductModel orderEntryPrdModel) {
        final String productTypeCode = getProductTypeCode(orderEntryPrdModel);
        return TgtCoreConstants.PHYSICAL_GIFTCARD.equalsIgnoreCase(productTypeCode);
    }

}
