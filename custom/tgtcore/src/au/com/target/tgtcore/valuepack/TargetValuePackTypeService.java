package au.com.target.tgtcore.valuepack;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import au.com.target.tgtcore.model.TargetValuePackTypeModel;

/**
 * Service providing functions to work with {@link TargetValuePackTypeModel}.
 */
public interface TargetValuePackTypeService {

    /**
     * Returns the value pack type (a.k.a. product bundle) for a given {@code leadSku}.
     *
     * @param leadSku the lead SKU to search value pack type for
     * @return the value pack type
     * @throws ModelNotFoundException if no record found
     * @throws AmbiguousIdentifierException if multiple records found
     */
    TargetValuePackTypeModel getByLeadSku(String leadSku) throws ModelNotFoundException, AmbiguousIdentifierException;
}
