/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author rmcalave
 * 
 */
public class TargetSizeVariantProductValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetSizeVariantProductModel)) {
            return;
        }

        final TargetSizeVariantProductModel targetSizeVariantProduct = (TargetSizeVariantProductModel)model;

        if (Boolean.TRUE.equals(targetSizeVariantProduct.getBulkyBoardProduct())) {
            throw new InterceptorException("A size variant cannot be attached to a Bulky Board product.");
        }
    }

}
