package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtutility.util.StackTraceFormatter;


/**
 * Added for Logging the products which are getting removed along with trace to identify the action which causes the
 * removal
 * 
 */
public class TargetProductRemoveInterceptor implements RemoveInterceptor
{

    private static final Logger LOG = Logger.getLogger(TargetProductRemoveInterceptor.class);

    @Override
    public void onRemove(final Object model, final InterceptorContext ctx) throws InterceptorException
    {
        if (model instanceof TargetProductModel)
        {
            final ProductModel product = (ProductModel)model;

            LOG.error(SplunkLogFormatter.formatMessage(
                    "INTERCEPTOR -- Removing Product (pk, code): (" + product.getPk() + "," + product.getCode()
                            + ") -->",
                    TgtutilityConstants.ErrorCode.ERR_REMOVE_PRODUCT
                    , StackTraceFormatter.stackTraceToString(Thread.currentThread().getStackTrace())));
        }

    }
}
