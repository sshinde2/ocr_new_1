package au.com.target.tgtcore.interceptor;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * A {@link ValidateInterceptor} which ensures that a given {@link TargetProductModel} has one and only one
 * {@link TargetMerchDepartmentModel} as its super category
 */
public class TargetProductMerchDepartmentValidator implements ValidateInterceptor {


    private static final String MERCHANT_DEPARTMENT_CODE = ":merchantDepartmentCode=";

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (!(model instanceof TargetProductModel)) {
            return;
        }

        final TargetProductModel targetProductModel = (TargetProductModel)model;
        final Collection<CategoryModel> superCategories = targetProductModel.getSupercategories();

        if (CollectionUtils.isEmpty(superCategories)) {
            return;
        }

        boolean merchDepartmentFound = false;
        final StringBuilder merchantDepartMentCodes = new StringBuilder();

        for (final CategoryModel categoryModel : superCategories) {
            if (categoryModel instanceof TargetMerchDepartmentModel) {
                merchantDepartMentCodes.append(MERCHANT_DEPARTMENT_CODE).append(categoryModel.getCode());
                if (merchDepartmentFound) {
                    throw new InterceptorException(ErrorMessages.MERCH_DEPT_TOO_MANY
                            + merchantDepartMentCodes.toString());
                }
                merchDepartmentFound = true;
            }
        }
    }

    protected interface ErrorMessages {
        public String MERCH_DEPT_TOO_MANY = "'Target Product' must contain only one TargetMerchDepartment as super category.";
    }

}
