/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.order.exceptions.DeliveryModeInterceptorException;
import de.hybris.platform.order.strategies.deliveryzone.ZDMVConsistencyStrategy;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;


/**
 * Validator for {@link TargetZoneDeliveryModeValueModel}.<br/>
 * Replaces the hybris validator {@link TargetZoneDeliveryModeValueValidator} to consider valid to and valid from
 * fields.
 */
public class TargetZoneDeliveryModeValueValidator implements ValidateInterceptor {
    private ZoneDeliveryModeService zoneDeliveryModeService;
    private ZDMVConsistencyStrategy zdmvConsistencyStrategy;
    private TargetDeliveryService targetDeliveryService;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof TargetZoneDeliveryModeValueModel) {
            // 1. check if zones clash  (hybris logic)
            final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue = (TargetZoneDeliveryModeValueModel)model;
            final ZoneModel zone = zoneDeliveryModeValue.getZone();
            final ZoneDeliveryModeModel zoneDeliveryMode = zoneDeliveryModeValue.getDeliveryMode();
            final Collection<ZoneModel> existingZones =
                    zoneDeliveryModeService.getZonesForZoneDeliveryMode(zoneDeliveryMode);
            final Set<ZoneModel> zones = new HashSet<ZoneModel>(existingZones);
            if (zones.add(zone)) {
                final Map<CountryModel, Set<ZoneModel>> ambiguous =
                        zdmvConsistencyStrategy.getAmbiguousCountriesForZones(zones);
                if (!ambiguous.isEmpty()) {
                    throw new DeliveryModeInterceptorException("Illegal value for [" + zoneDeliveryMode.getCode()
                            + "] with zone [" + zone.getCode() + "] - its countries [" + ambiguous.keySet()
                            + "] would be mapped to more than one zone");
                }
            }
            final List<AbstractTargetZoneDeliveryModeValueModel> values = targetDeliveryService.getDeliveryValue(zone,
                    zoneDeliveryModeValue.getCurrency(), zoneDeliveryModeValue.getMinimum(), zoneDeliveryMode);
            if (CollectionUtils.isEmpty(values)) {
                return;
            }
            for (final AbstractTargetZoneDeliveryModeValueModel abstractTargetZoneDelValue : values) {
                if (abstractTargetZoneDelValue instanceof TargetZoneDeliveryModeValueModel) {
                    final TargetZoneDeliveryModeValueModel targetZoneDelValue = (TargetZoneDeliveryModeValueModel)abstractTargetZoneDelValue;
                    if (targetZoneDelValue.getPk().equals(zoneDeliveryModeValue.getPk())) {
                        continue;
                    }
                    if (isConflicted(zoneDeliveryModeValue, targetZoneDelValue)) {
                        throw new InterceptorException(
                                "You can't save another TargetZoneDeliveryModeValueModel " + zoneDeliveryModeValue
                                        + " overlapping with the current one " + targetZoneDelValue);
                    }
                }
            }
        }
    }

    private boolean isConflicted(final TargetZoneDeliveryModeValueModel newModel,
            final TargetZoneDeliveryModeValueModel existingModel) {
        final Date newFrom = newModel.getValidFrom();
        final Date newTo = newModel.getValidTo();
        final Date existingFrom = existingModel.getValidFrom();
        final Date existingTo = existingModel.getValidTo();

        // only valid if one ends on or before the other starts
        if (newTo != null && existingFrom != null) {
            if (!newTo.after(existingFrom)) {
                return false;
            }
        }

        if (existingTo != null && newFrom != null) {
            if (!existingTo.after(newFrom)) {
                return false;
            }
        }

        // all other combinations are a conflict
        return true;
    }

    @Required
    public void setZdmvConsistencyStrategy(final ZDMVConsistencyStrategy zdmvConsistencyStrategy) {
        this.zdmvConsistencyStrategy = zdmvConsistencyStrategy;

    }



    @Required
    public void setZoneDeliveryModeService(final ZoneDeliveryModeService zoneDeliveryModeService) {
        this.zoneDeliveryModeService = zoneDeliveryModeService;

    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }
}
