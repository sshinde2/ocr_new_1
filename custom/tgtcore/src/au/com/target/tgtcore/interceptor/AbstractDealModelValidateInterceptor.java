/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author rmcalave
 * 
 */
public class AbstractDealModelValidateInterceptor implements ValidateInterceptor {

    private int dealIdMinValue;
    private int dealIdMaxValue;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractDealModel)) {
            return;
        }

        final AbstractDealModel abstractDeal = (AbstractDealModel)model;

        try {
            final int code = Integer.parseInt(abstractDeal.getCode());

            if (code < dealIdMinValue || code > dealIdMaxValue) {
                throw new InterceptorException(ErrorMessages.DEAL_ID_INVALID + dealIdMaxValue);
            }
        }
        catch (final NumberFormatException ex) {
            throw new InterceptorException(ErrorMessages.DEAL_ID_INVALID + dealIdMaxValue); //NOPMD don't need to log the stack trace
        }

        final Date startDate = abstractDeal.getStartDate();
        final Date endDate = abstractDeal.getEndDate();
        if (startDate != null && endDate != null) {
            if (endDate.before(startDate)) {
                throw new InterceptorException(ErrorMessages.END_DATE_BEFORE_START_DATE);
            }
        }

        if (Boolean.TRUE.equals(abstractDeal.getEnabled())) {
            if (StringUtils.isBlank(abstractDeal.getDescription())) {
                throw new InterceptorException(ErrorMessages.DESCRIPTION_REQUIRED);
            }

            if (StringUtils.isBlank(abstractDeal.getMessageFired())) {
                throw new InterceptorException(ErrorMessages.FIRED_MESSAGE_REQUIRED);
            }

            if (StringUtils.isBlank(abstractDeal.getMessageCouldHaveFired())) {
                throw new InterceptorException(ErrorMessages.COULD_HAVE_FIRED_MESSAGE_REQUIRED);
            }
        }
    }

    protected interface ErrorMessages {
        public static final String DEAL_ID_INVALID = "Deal ID must be a positive integer not greater than ";
        public static final String END_DATE_BEFORE_START_DATE = "End date must not be before start date.";
        public static final String DESCRIPTION_REQUIRED = "A Description must be supplied if this deal is enabled.";
        public static final String FIRED_MESSAGE_REQUIRED = "A Fired Message must be supplied if this deal is enabled.";
        public static final String COULD_HAVE_FIRED_MESSAGE_REQUIRED = "A Could Have Fired Message must be supplied if this deal is enabled.";
    }


    /**
     * @param dealIdMinValue
     *            the dealIdMinValue to set
     */
    @Required
    public void setDealIdMinValue(final int dealIdMinValue) {
        this.dealIdMinValue = dealIdMinValue;
    }

    /**
     * @param dealIdMaxValue
     *            the dealIdMaxValue to set
     */
    @Required
    public void setDealIdMaxValue(final int dealIdMaxValue) {
        this.dealIdMaxValue = dealIdMaxValue;
    }
}
