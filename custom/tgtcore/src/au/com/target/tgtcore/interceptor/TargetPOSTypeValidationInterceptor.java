/**
 *
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Hook into interceptor framework to validate PointOfServiceType.
 *
 * @author jjayawa1
 *
 */
public class TargetPOSTypeValidationInterceptor implements ValidateInterceptor {

    private List<PointOfServiceTypeEnum> invalidPointOfServiceTypes = null;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object obj, final InterceptorContext ctx) throws InterceptorException {
        if (obj instanceof PointOfServiceModel) {
            final PointOfServiceModel pos = (PointOfServiceModel)obj;
            if (CollectionUtils.isNotEmpty(invalidPointOfServiceTypes)
                    && invalidPointOfServiceTypes.contains(pos.getType())) {
                throw new InterceptorException("We don't support point of service types "
                        + invalidPointOfServiceTypes.toString());
            }
        }
    }

    /**
     * @param invalidPointOfServiceTypes
     *            the invalidPointOfServiceTypes to set
     */
    @Required
    public void setInvalidPointOfServiceTypes(final List<PointOfServiceTypeEnum> invalidPointOfServiceTypes) {
        this.invalidPointOfServiceTypes = invalidPointOfServiceTypes;
    }
}
