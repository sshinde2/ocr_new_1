package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;


/**
 * Remove interceptor which removes the associated categories and breakpoints
 */
public class QuantityBreakDealModelRemoveInterceptor implements RemoveInterceptor {


    @Override
    public void onRemove(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof QuantityBreakDealModel)) {
            return;
        }

        final QuantityBreakDealModel quantityBreakDeal = (QuantityBreakDealModel)model;

        // remove the categories and break points
        final Collection<ItemModel> itemsToRemove = new ArrayList<>();
        itemsToRemove.addAll(quantityBreakDeal.getCategories());
        itemsToRemove.addAll(quantityBreakDeal.getBreakPoints());

        if (CollectionUtils.isNotEmpty(itemsToRemove)) {
            getModelService().removeAll(itemsToRemove);
        }

    }

    public ModelService getModelService() {
        throw new UnsupportedOperationException(
                "Please define a <lookup-method> for getModelService() in the spring configuration");
    }
}
