/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;


/**
 * @author Vivek
 *
 */
public class CurrencyConversionFactorsPrepareInterceptor implements PrepareInterceptor<CurrencyConversionFactorsModel> {

    private static final Logger LOG = Logger.getLogger(CurrencyConversionFactorsPrepareInterceptor.class);

    /*
     * Log whenever there is a new CurrencyConversionFactor added OR existing one is updated.
     */
    @Override
    public void onPrepare(final CurrencyConversionFactorsModel currencyConversionFactor, final InterceptorContext ctx)
            throws InterceptorException {

        if (null == currencyConversionFactor.getCurrencyConversionFactor()) {
            LOG.info("CurrencyConversionFactorUpdate: NULL conversion factor");
            return;
        }

        if (ctx.isNew(currencyConversionFactor)) {
            logNewConversionFactor(currencyConversionFactor);
        }
        else if (ctx.isModified(currencyConversionFactor, CurrencyConversionFactorsModel.CURRENCYCONVERSIONFACTOR)) {
            logUpdateConversionFactor(currencyConversionFactor);
        }
    }

    /**
     * @param currencyConversionFactor
     */
    protected void logNewConversionFactor(final CurrencyConversionFactorsModel currencyConversionFactor) {
        LOG.info("CurrencyConversionFactorUpdate: New Conversion Factor, code="
                + currencyConversionFactor.getCode() + ", conversionRate="
                + currencyConversionFactor.getCurrencyConversionFactor().toString());
    }

    /**
     * @param currencyConversionFactor
     */
    protected void logUpdateConversionFactor(final CurrencyConversionFactorsModel currencyConversionFactor) {
        LOG.info("CurrencyConversionFactorUpdate: Update Conversion Factor, code="
                + currencyConversionFactor.getCode() + ", conversionRate="
                + currencyConversionFactor.getCurrencyConversionFactor().toString());
    }

}
