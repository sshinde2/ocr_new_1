/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Date;

import au.com.target.tgtcore.model.TargetTimeZDMVRestrictionModel;


/**
 * @author Nandini
 * 
 */
public class TargetTimeValidateInterceptor implements ValidateInterceptor {

    /*
     * evaluates if we supplied either of the start or end date
     * otherwise it will not create the Target Time Restriction.
     * 
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof TargetTimeZDMVRestrictionModel) {
            final TargetTimeZDMVRestrictionModel targetTimeZDMVRestrictionModel = (TargetTimeZDMVRestrictionModel)model;
            final Date start = targetTimeZDMVRestrictionModel.getActiveFrom();
            final Date end = targetTimeZDMVRestrictionModel.getActiveUntil();

            if (start == null && end == null) {
                throw new InterceptorException("Either start date or end date must be filled.");
            }
        }
    }
}
