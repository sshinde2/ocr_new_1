/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetAnyProductCategoryZDMVRestrictionModel;


/**
 * @author Umesh
 * 
 */
public class TargetAnyProductCategoryZDMVRestrictionValidator implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof TargetAnyProductCategoryZDMVRestrictionModel) {
            final TargetAnyProductCategoryZDMVRestrictionModel restrictionModel = (TargetAnyProductCategoryZDMVRestrictionModel)model;

            if (CollectionUtils.isEmpty(restrictionModel.getIncludedProducts())
                    && CollectionUtils.isEmpty(restrictionModel.getIncludedCategories())
                    && null == restrictionModel.getProductType()) {
                throw new InterceptorException("Either products, categories or product type must be provided.");
            }
        }
    }
}
