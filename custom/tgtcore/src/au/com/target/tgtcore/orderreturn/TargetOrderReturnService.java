package au.com.target.tgtcore.orderreturn;

import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.enums.ReturnAction;
import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.returns.impl.DefaultReturnService;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;


/**
 * Target version to create {@link RefundEntryModel} with {@link ReturnStatus#RECEIVED} status
 * 
 */
public class TargetOrderReturnService extends DefaultReturnService {
    @Override
    public RefundEntryModel createRefund(final ReturnRequestModel request, final AbstractOrderEntryModel entry,
            final String notes, final Long expectedQuantity, final ReturnAction action, final RefundReason reason)
    {
        final RefundEntryModel returnsEntry = getModelService().create(RefundEntryModel.class);
        returnsEntry.setOrderEntry(entry);
        returnsEntry.setAction(action);
        returnsEntry.setNotes(notes);
        returnsEntry.setReason(reason);
        returnsEntry.setReturnRequest(request);
        returnsEntry.setExpectedQuantity(expectedQuantity);
        returnsEntry.setStatus(ReturnStatus.RECEIVED);
        getModelService().save(returnsEntry);
        return returnsEntry;
    }
}
