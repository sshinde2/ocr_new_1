/**
 * 
 */
package au.com.target.tgtcore.shipster.dao.impl;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.Collections;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.shipster.dao.ShipsterDao;


/**
 * @author pvarghe2
 *
 */
public class ShipsterDaoImpl extends DefaultGenericDao<ShipsterConfigModel>
        implements ShipsterDao {

    private static final String GET_SHIPSTER_MODEL = "SELECT {" + ShipsterConfigModel.PK
            + "} FROM {" + ShipsterConfigModel._TYPECODE + "} WHERE {" + ShipsterConfigModel.DELIVERYMODE
            + "} IN (?deliveryModes)";


    public ShipsterDaoImpl() {
        super(ShipsterConfigModel._TYPECODE);
    }

    @Override
    public Collection<ShipsterConfigModel> getShipsterModelForDeliveryModes(
            final Collection<TargetZoneDeliveryModeModel> deliveryModes) {
        final SearchResult<ShipsterConfigModel> result = getFlexibleSearchService().search(
                GET_SHIPSTER_MODEL,
                Collections.singletonMap("deliveryModes", deliveryModes));
        return result.getResult();
    }

}
