/**
 * 
 */
package au.com.target.tgtcore.sizeorder.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.sizeorder.dao.TargetProductSizeDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author mjanarth
 *
 */
public class TargetProductSizeDaoImpl implements TargetProductSizeDao {

    private static final String MAX_POSITION_QUERY = "select MAX({"
            + TargetProductSizeModel.POSITION
            + "}) from {" + TargetProductSizeModel._TYPECODE + "}"
            + "WHERE {" + TargetProductSizeModel.SIZEGROUP + "}=?sizeGroup ";


    private static final String PRODUCT_SIZE_QUERY = "select {"
            + TargetProductSizeModel.PK
            + "} from {" + TargetProductSizeModel._TYPECODE + "}"
            + "WHERE {" + TargetProductSizeModel.SIZEGROUP + "}=?sizeGroup "
            + "AND {" + TargetProductSizeModel.SIZE + "}=?size ";



    private FlexibleSearchService flexibleSearchService;


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.sizeorder.dao.TargetProductSizeDao#getMaxProductSizePosition()
     */
    @Override
    public Integer getMaxProductSizePosition(final TargetSizeGroupModel sizeGroup) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(MAX_POSITION_QUERY);
        query.addQueryParameter("sizeGroup", sizeGroup);
        query.setResultClassList(Arrays.asList(Integer.class));
        final SearchResult<Integer> searchResult = flexibleSearchService.search(query);
        return searchResult.getResult().get(0);
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.sizeorder.dao.TargetProductSizeDao#findTargetProductSize(java.lang.String, au.com.target.tgtcore.model.TargetSizeGroupModel)
     */
    @Override
    public TargetProductSizeModel findTargetProductSizeBySizeGroup(final String size,
            final TargetSizeGroupModel sizeGroup)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(PRODUCT_SIZE_QUERY);
        searchQuery.addQueryParameter("size", size);
        searchQuery.addQueryParameter("sizeGroup", sizeGroup);
        final List<TargetProductSizeModel> models = flexibleSearchService.<TargetProductSizeModel> search(searchQuery)
                .getResult();
        TargetServicesUtil.validateIfSingleResult(models, TargetProductSizeModel.class,
                TargetProductSizeModel.SIZEGROUP, size);
        return models.get(0);
    }
}
