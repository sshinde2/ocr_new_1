/**
 * 
 */
package au.com.target.tgtcore.sizeorder.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author mjanarth
 *
 */
public class TargetSizeGroupDaoImpl implements TargetSizeGroupDao {


    private static final String MAX_POSITION_QUERY = "select MAX({"
            + TargetSizeGroupModel.POSITION
            + "}) from {" + TargetSizeGroupModel._TYPECODE + "}";

    private static final String SIZEGROUP_CODE_QUERY = "SELECT {" + TargetSizeGroupModel.PK + "}"
            + "FROM {" + TargetSizeGroupModel._TYPECODE
            + "} WHERE {" + TargetSizeGroupModel.CODE + "}=?code";

    private FlexibleSearchService flexibleSearchService;



    /* (non-Javadoc)
     * @see au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao#findSizeGroupbyName(java.lang.String)
     */
    @Override
    public TargetSizeGroupModel findSizeGroupbyCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(SIZEGROUP_CODE_QUERY);
        searchQuery.addQueryParameter("code", code);
        final List<TargetSizeGroupModel> models = flexibleSearchService.<TargetSizeGroupModel> search(searchQuery)
                .getResult();
        TargetServicesUtil.validateIfSingleResult(models, TargetSizeGroupModel.class,
                TargetSizeGroupModel.CODE, code);
        return models.get(0);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao#getMaxSizeGroupPosition(java.lang.String)
     */
    @Override
    public Integer getMaxSizeGroupPosition() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(MAX_POSITION_QUERY);
        query.setResultClassList(Arrays.asList(Integer.class));
        final SearchResult<Integer> searchResult = flexibleSearchService.search(query);
        return searchResult.getResult().get(0);
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}
