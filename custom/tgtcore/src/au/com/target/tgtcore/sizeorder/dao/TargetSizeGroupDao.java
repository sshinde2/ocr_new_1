/**
 * 
 */
package au.com.target.tgtcore.sizeorder.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSizeGroupModel;


/**
 * @author mjanarth
 *
 */
public interface TargetSizeGroupDao {

    /**
     * 
     * @param code
     * @return TargetSizeGroupModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetSizeGroupModel findSizeGroupbyCode(String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * Retrieves the max(sizegroup) position
     * 
     */
    Integer getMaxSizeGroupPosition();
}
