/**
 * 
 */
package au.com.target.tgtcore.sizeorder.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;


/**
 * @author mjanarth
 *
 */
public interface TargetProductSizeDao {


    /**
     * Retrieves the max(position)
     * 
     * @param sizeGroup
     * @return position
     */
    Integer getMaxProductSizePosition(final TargetSizeGroupModel sizeGroup);

    /**
     * Retrieves the TargetProductSize based on the size and sizegroup
     * 
     * @param size
     * @param sizeGroup
     * @return TargetProductSizeModel
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    TargetProductSizeModel findTargetProductSizeBySizeGroup(String size, TargetSizeGroupModel sizeGroup)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;
}
