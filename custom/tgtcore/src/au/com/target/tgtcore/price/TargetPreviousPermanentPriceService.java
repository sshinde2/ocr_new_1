/**
 * 
 */
package au.com.target.tgtcore.price;

import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;



/**
 * @author knemalik
 * 
 */
public interface TargetPreviousPermanentPriceService {

    /**
     * Update the end date for previous permanent price record
     * 
     * @param productCode
     * 
     */
    public void updatePreviousPermanentPriceEndDate(String productCode);


    /**
     * Get the previous permanent price for the given product code
     * 
     * @param variantCode
     * 
     */
    public TargetPreviousPermanentPriceModel getPreviousPermanentPriceWithNullEndDate(String variantCode);

    /**
     * Creates or update the Previous Permanent Price
     * 
     * @param variantCode
     * @param price
     * @param promoEvent
     */
    public void createOrUpdatePermanentPrice(final String variantCode, final Double price, final boolean promoEvent);


    /**
     * create new previous permanent price record for given product code
     * 
     * @param productCode
     * 
     */
    public void createPreviousPermanentPrice(String productCode);

    /**
     * Cleanup TargetPreviousPermanentPriceRecord entries that are no longer required.
     * 
     * @return true if there could be more records, false if there are none
     */
    public boolean cleanupPreviousPermanentPriceRecords();
}
