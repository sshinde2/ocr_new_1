/**
 * 
 */
package au.com.target.tgtcore.price.data;



/**
 * Represents price range information for a product
 * 
 */
public class PriceRangeInformation {

    private static final double PRICE_EQUAL_TOLERANCE = 0.001;

    private final Double fromPrice;
    private final Double toPrice;
    private final Double fromWasPrice;
    private final Double toWasPrice;

    public PriceRangeInformation(final Double fromPrice, final Double toPrice, final Double fromWasPrice,
            final Double toWasPrice) {

        this.fromPrice = fromPrice;
        this.toPrice = toPrice;
        this.fromWasPrice = fromWasPrice;
        this.toWasPrice = toWasPrice;
    }

    /**
     * @return the fromPrice
     */
    public Double getFromPrice() {
        return fromPrice;
    }

    /**
     * @return the toPrice
     */
    public Double getToPrice() {
        return toPrice;
    }

    /**
     * @return the fromWasPrice
     */
    public Double getFromWasPrice() {
        return fromWasPrice;
    }

    /**
     * @return the toWasPrice
     */
    public Double getToWasPrice() {
        return toWasPrice;
    }

    public boolean sellPriceIsRange()
    {
        return pricesAreRange(fromPrice, toPrice);
    }

    public boolean wasPriceIsRange()
    {
        return pricesAreRange(fromWasPrice, toWasPrice);
    }

    /**
     * Are the given prices a range
     * 
     * @param price1
     * @param price2
     * @return true if the prices constitute a range
     */
    private boolean pricesAreRange(final Double price1, final Double price2)
    {
        // If either price is null then it isn't a range
        if (price1 == null || price2 == null)
        {
            return false;
        }

        // It's a range if the values are sufficiently different
        return Math.abs(price1.doubleValue() - price2.doubleValue()) > PRICE_EQUAL_TOLERANCE;
    }

}
