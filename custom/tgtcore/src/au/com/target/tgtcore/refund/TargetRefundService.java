/**
 * 
 */
package au.com.target.tgtcore.refund;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;


public interface TargetRefundService {

    /**
     * Performs refund for the given {@code request} and {@code order}.
     * 
     * @param request
     *            the refund request, containing items to process
     * @param order
     *            the source order
     */
    void apply(final ReturnRequestModel request, final OrderModel order);

    /**
     * Performs refund for the given {@code request} and {@code order} and {@code paymentInfo}.
     * 
     * @param request
     *            the refund request, containing items to process
     * @param order
     *            the source order
     * @param paymentInfo
     *            the credit card payment info
     * 
     * @param ipgNewRefundInfoDTO
     */
    void apply(final ReturnRequestModel request, final OrderModel order, final CreditCardPaymentInfoModel paymentInfo,
            final IpgNewRefundInfoDTO ipgNewRefundInfoDTO);

    /**
     * Performs check to find delivery cost is re-fundable irrespective of order entries only if order is in completed
     * and shipped status
     * 
     * @param order
     * @return boolean
     */
    boolean isDeliveryCostRefundable(final OrderModel order);

    /**
     * Checks the listed denial strategy to prevent refund and if all perform methods of the listed strategies returns
     * true than this method returns true
     * 
     * @param order
     * @param entry
     * @param returnQuantity
     * @return True/False
     */
    boolean isRefundable(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity);

}
