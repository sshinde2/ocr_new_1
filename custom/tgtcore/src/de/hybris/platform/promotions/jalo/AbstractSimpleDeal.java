package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.jalo.TgtCoreManager;


public abstract class AbstractSimpleDeal extends GeneratedAbstractSimpleDeal {
    //*************************************************************************
    protected abstract class MathDisc implements DealHelperDiscountableInterface {
        private final ProductWrapper item;
        private final long preDiscAmt;

        //*********************************************************
        MathDisc(final ProductWrapper item) {
            this.item = item;
            preDiscAmt = item.getUnitSellPrice() - item.getDealMarkdown();
        }

        //*********************************************************
        @Override
        public long getPreDiscAmt() {
            return preDiscAmt;
        }

        //*********************************************************
        @Override
        public long getDiscAmt() {
            return item.getDealMarkdown();
        }

        //*********************************************************
        public ProductWrapper getItem() {
            return item;
        }
    }

    private static Integer centsRoundingCutoff;

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /**
     * Calculate the certainty for the remaining items in this deal.
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @return how close we were to this deal being met (again), 0 not met at all, 1 fully met
     */
    protected float calcQtyCertainty(final List<ProductWrapper> wrappedProducts) {
        final List<DealQualifier> qualList = getQualifierList();

        long totMinQty = 0;
        long totSumQty = 0;
        for (final DealQualifier dealQualifier : qualList) {
            final long minQty = dealQualifier.getMinQtyAsPrimitive();
            if (minQty > 0)
            {
                totMinQty += minQty;
                long sumQty = 0;
                for (final ProductWrapper thisItem : wrappedProducts) {
                    if (thisItem.isInDeal()) {
                        continue;
                    }
                    final DealQualifierModel itemQualifer = thisItem.getQualifierModel();
                    if (itemQualifer != null && itemQualifer.getCode().equals(dealQualifier.getCode())) {
                        if (++sumQty == minQty) {
                            break;
                        }
                    }
                }
                totSumQty += sumQty;
            }
        }

        return (float)totSumQty / (float)totMinQty;
    }

    @Override
    protected List<PromotionResult> preApplyCheck(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts) {
        final List<DealQualifier> qualList = getQualifierList();
        // see if the qualifiers are met, this can save a
        // lot of calculation later for nothing.
        long minAmt, sumAmt; // note using cents here
        long minQty, sumQty;
        for (final DealQualifier dealQualifier : qualList) {

            minAmt = Math.round(dealQualifier.getMinAmtAsPrimitive() * 100);
            if (minAmt > 0) {
                sumAmt = 0;
                for (final ProductWrapper thisItem : wrappedProducts) {
                    final DealQualifierModel itemQualifer = thisItem.getQualifierModel();
                    if (itemQualifer != null && itemQualifer.getCode().equals(dealQualifier.getCode())) {
                        sumAmt += thisItem.getUnitSellPrice();
                    }
                }
                if ((minAmt - sumAmt) > getCentsRoundingCutoff()) {
                    return buildCouldHaveFiredResultList(wrappedProducts, wrappedDeal,
                            (float)sumAmt / (float)minAmt); // deal not met
                }
            }

            minQty = dealQualifier.getMinQtyAsPrimitive();
            if (minQty > 0)
            {
                sumQty = 0;
                for (final ProductWrapper thisItem : wrappedProducts) {
                    final DealQualifierModel itemQualifer = thisItem.getQualifierModel();
                    if (itemQualifer != null && itemQualifer.getCode().equals(dealQualifier.getCode())) {
                        sumQty++;
                    }
                }
                if (sumQty < minQty) {
                    return buildCouldHaveFiredResultList(wrappedProducts, wrappedDeal,
                            calcQtyCertainty(wrappedProducts)); // deal not met
                }
            }
        }

        // we got here, then it is worth doing some deeper evaluation of this deal
        return null;
    }

    /**
     * POS has concept of cents rounding when customers use cash. They put in a fudge factor to allow deals to apply if
     * they are close enough that they would be rounded if the customer used cash.
     * 
     * @return the cents rounding cutoff value in cents
     */
    protected static int getCentsRoundingCutoff() {
        if (centsRoundingCutoff == null) {
            centsRoundingCutoff = Integer.valueOf(Config.getInt("tgtcore.deals.centsroundingcutoff", 2));
        }

        return centsRoundingCutoff.intValue();
    }

    /**
     * Called to deep clone attributes of this instance
     * 
     * The values map contains all the attributes defined on this instance. The map will be used to initialize a new
     * instance of the Action that is a clone of this instance. This method can remove, replace or add to the Map of
     * attributes.
     * 
     * @param ctx
     *            The hybris context
     * @param values
     *            The map to write into
     */
    @Override
    protected void deepCloneAttributes(final SessionContext ctx, final Map values) {
        super.deepCloneAttributes(ctx, values);

        // Keep all existing attributes apart from BundlePrices, which we deep clone
        values.remove(AbstractSimpleDeal.QUALIFIERLIST);

        // Clone qualifier list
        values.put(AbstractSimpleDeal.QUALIFIERLIST, deepCloneQualifierList(ctx, getQualifierList(ctx)));
    }

    /**
     * @param ctx
     * @param qualifierList
     * @return collection of deep cloned deal qualifiers
     */
    private Collection<DealQualifier> deepCloneQualifierList(final SessionContext ctx,
            final Collection<DealQualifier> qualifierList) {
        final Collection<DealQualifier> dupQualifierList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(qualifierList)) {
            for (final DealQualifier qualifier : qualifierList) {
                final Map parameters = new HashMap();
                parameters.put(DealQualifier.CODE, qualifier.getCode(ctx));
                parameters.put(DealQualifier.MINAMT, qualifier.getMinAmt(ctx));
                parameters.put(DealQualifier.MINQTY, qualifier.getMinQty(ctx));
                parameters.put(DealQualifier.DEALCATEGORY, qualifier.getDealCategory(ctx));

                dupQualifierList.add(TgtCoreManager.getInstance().createDealQualifier(ctx, parameters));
            }
        }
        return dupQualifierList;
    }

    /**
     * Deal has been not been met but we have all qualifiers and no rewards added
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @param wrappedDeal
     *            the deal details wrapped in our class for convenience
     * @param certainty
     *            how close we were to this deal being met, 0 not met at all, 1 fully met
     * @param maxNUmberOfRewards
     *            the number of maximum number of rewards
     * @return the list of promotion results that this promotions creates
     */
    protected List<PromotionResult> buildCouldHaveFiredResultListForAllQualifiersButNoRewards(
            final List<ProductWrapper> wrappedProducts,
            final DealWrapper wrappedDeal, final float certainty, final int maxNUmberOfRewards) {
        final SessionContext ctx = wrappedDeal.getCtx();
        final PromotionEvaluationContext promoContext = wrappedDeal.getPromoContext();
        final List<PromotionResult> results = new ArrayList<>();
        promoContext.startLoggingConsumed(this);
        // our logic has broken products into individual items, no need for that now, and dealling with quantities should be more efficient
        consumePromotionOrderView(wrappedProducts, wrappedDeal, ctx);
        final PromotionResult result = TgtCoreManager.getInstance().createTargetDealWithRewardResult(ctx, this,
                promoContext.getOrder(), certainty, 0, 0, maxNUmberOfRewards);
        result.setConsumedEntries(ctx, promoContext.finishLoggingAndGetConsumed(this, false));
        results.add(result);
        return results;
    }

    /**
     * Deal has been met(all qualifiers and atleast one reward) now create the result list that hybris can deal with.
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @param wrappedDeal
     *            the deal details wrapped in our class for convenience
     * @param instanceCount
     *            how many times this deal has been applied in this order
     * @param numberofRewardsSofar
     *            Number of rewards in cart
     * @param maxNUmberOfRewards
     *            the number of maximum number of rewards
     * @return the list of promotion results that this promotions creates
     */
    protected List<PromotionResult> buildFiredResultListWithRewards(final List<ProductWrapper> wrappedProducts,
            final DealWrapper wrappedDeal, final int instanceCount, final int numberofRewardsSofar,
            final int maxNUmberOfRewards) {
        final List<PromotionResult> results = new ArrayList<>();
        final PromotionOrderView orderView = wrappedDeal.getOrderView();
        final SessionContext ctx = wrappedDeal.getCtx();
        final PromotionEvaluationContext promoContext = wrappedDeal.getPromoContext();
        final Currency currency = promoContext.getOrder().getCurrency();
        promoContext.startLoggingConsumed(this);
        final List<AbstractPromotionAction> actions = new ArrayList<>();
        createConsumedPromotionOrderEntries(wrappedProducts, orderView, currency, ctx, actions);
        final PromotionResult result = TgtCoreManager.getInstance().createTargetDealWithRewardResult(ctx, this,
                promoContext.getOrder(), 1.0F, instanceCount, numberofRewardsSofar, maxNUmberOfRewards);
        final List<PromotionOrderEntryConsumed> consumed = promoContext.finishLoggingAndGetConsumed(this, true);
        result.setConsumedEntries(ctx, consumed);
        result.setActions(ctx, actions);
        results.add(result);

        return results;
    }




}
