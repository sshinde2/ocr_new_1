package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.util.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.enums.DealRewardConsumeOrderEnum;


//*************************************************************************
// Buy and Get, where qualifier and reward list are the same.
// Example of this type of deal:
//          buy 2 shirts you get the 3rd free
//          buy a shoe and half off the 2nd shoe
//          buy a pair of pants and get a belt for $1
//
// All items in this deal are both qualifing and reward items, as there
//  can only be one reward list, this specifies there can only be one
//  qualifing list.
// The Qualifing record specifies the minimum quantity of item required to
//  achieve the reward.  (note this does not include the reward items)
// Reward types that are valid are:
//          DOLLAR_OFF_EACH
//          FIXED_DOLLAR_EACH
//          PERCENT_OFF_EACH
// The reward quantity is the maximum number of reward items for each
//  instance, if it is zero then there is no maximum number and all items
//  except the initial qualifing items are given the reward in a single
//  instance.
// If the reward is a DOLLAR_OFF_EACH, the final value of the item cannot
//  be negative, if the reward is more than the value of the item then the
//  item will given for free.
// If the reward is a FIXED_DOLLAR_EACH then we will only reduce the value
//  of an item not increase it.
// The quaulifing item(s) is/are the most expensive item(s), the reward
//  item(s) is/are the next most expensive after the qualifing item(s).
public class BuyGetSameListDeal extends GeneratedBuyGetSameListDeal {

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts) {
        final int instCnt = calculateDeals(wrappedProducts, wrappedDeal);

        // list of deal instances applied
        final List<PromotionResult> resultList;
        if (instCnt > 0) {
            resultList = buildFiredResultList(wrappedProducts, wrappedDeal, instCnt);
        }
        else {
            resultList = new ArrayList<>();
        }

        // product that could have met another deal if we had more products
        float certainty = calcQtyCertainty(wrappedProducts);
        if (certainty >= 1.0f) {
            certainty = .99f;
        }
        if (certainty > 0.0001) {
            resultList.addAll(buildCouldHaveFiredResultList(wrappedProducts, wrappedDeal, certainty));
        }
        return resultList;
    }

    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);
            final List<DealQualifier> qualList = getQualifierList();

            int qualCnt = 0;
            for (final DealQualifier dealQualifier : qualList) {
                qualCnt += dealQualifier.getMinQtyAsPrimitive();
            }

            if (promotionResult.getFired(ctx)) {
                final double totalDiscount = promotionResult.getTotalDiscount(ctx);

                final Object[] args = {
                        Double.valueOf(totalDiscount),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount),
                        ((TargetDealResult)promotionResult).getInstanceCount()
                };
                return formatMessage(this.getMessageFired(ctx), args, locale);
            }
            else if (promotionResult.getCouldFire(ctx)) {

                final int foundCnt = Math.round(qualCnt * promotionResult.getCertaintyAsPrimitive());

                final Object[] args = {
                        Integer.valueOf(qualCnt),
                        Integer.valueOf(foundCnt),
                        Integer.valueOf(qualCnt - foundCnt)
                };
                return formatMessage(this.getMessageCouldHaveFired(ctx), args, locale);
            }
        }
        return "";
    }

    /**
     * This method calculates the deal based on the config value default/pricelowtohigh
     * 
     * @param wrappedProducts
     * @param wrappedDeal
     * @return numberofDealInstance
     */
    protected int calculateDeals(final List<ProductWrapper> wrappedProducts, final DealWrapper wrappedDeal) {

        final ProductWrapper[] getList = getArrayOfSortedProducts(wrappedProducts);
        int instcnt = 0;
        final int buy = wrappedDeal.getQualifierModelList().get(0).getMinQty().intValue();
        int get = getRewardMaxQtyAsPrimitive();
        if (get == 0) {
            // setting get to the number of items means that all items
            // will be given as a reward on the first deal instance.
            get = wrappedProducts.size();
        }
        final DealRewardConsumeOrderEnum dealConfig = getDealConsumeOrderConfigValue(wrappedDeal);
        if (DealRewardConsumeOrderEnum.PRICELOWTOHIGH.equals(dealConfig)) {
            instcnt = calculateDealsLowtoHigh(buy, get, getList, wrappedDeal);
        }
        else {
            instcnt = calculateDealsDefault(buy, get, getList, wrappedDeal);
        }
        return instcnt;

    }

    /**
     * This method calculates deal using the default strategy
     * 
     * @param buy
     * @param get
     * @param getList
     * @param wrappedDeal
     * @return numberofDealInstance
     */
    protected int calculateDealsDefault(final int buy, final int get, final ProductWrapper[] getList,
            final DealWrapper wrappedDeal) {
        int instCnt = 0;
        final long dealAmt = Math.round(getRewardValueAsPrimitive() * 100);
        int innerCnt;
        int cnt = getList.length - 1;
        for (;;) // looping around until we've met all possible instances of this deal in the transaction
        {
            // check we have enough items to fulfill the deal
            if (cnt < buy) {
                break;
            }

            // we could at this point check if the adjust type is fixed dollar each
            // if it was we could check the next item in the get list and if
            // it was less than the deal amount we could return as all the
            // remaining instances will have no value, but the business decided
            // they wanted to see the deal on the receipt even if there is
            // no value to that deal.

            // we are here so we know we can apply another instance of this deal
            instCnt++;

            // Set the qualifing items as part of the deal, and
            // skip over them to ensure we don't give them as a reward
            for (innerCnt = 0; innerCnt < buy; innerCnt++, cnt--) {
                getList[cnt].setDealApplied(instCnt, DealItemTypeEnum.QUALIFIER, 0);
            }

            // update the reward items
            for (innerCnt = 0; (innerCnt < get) && (cnt >= 0); cnt--) {
                final boolean appliedReward = applyDealReward(wrappedDeal, cnt, instCnt, dealAmt, getList);
                if (appliedReward) {
                    innerCnt++;
                }
            }
        }
        return instCnt;
    }


    /**
     * This method calculates deal using the pricelowtohigh strategy
     * 
     * @param buy
     * @param get
     * @param getList
     * @param wrappedDeal
     * @return numberofinstances
     */
    protected int calculateDealsLowtoHigh(final int buy, final int get, final ProductWrapper[] getList,
            final DealWrapper wrappedDeal) {
        int instCnt = 0;
        final long dealAmt = Math.round(getRewardValueAsPrimitive() * 100);
        final int totalNumberofItems = getList.length;
        int qualifierIndex = getList.length - 1;
        int rewardIndex = 0;
        final int totalNumberofDealInstances = (totalNumberofItems / (buy + get));

        for (int dealCount = 0; dealCount < totalNumberofDealInstances; dealCount++) {
            instCnt++;
            //add qualifier
            for (int qualifierCounter = 0; qualifierCounter < buy; qualifierCounter++) {
                getList[qualifierIndex].setDealApplied(instCnt, DealItemTypeEnum.QUALIFIER, 0);
                qualifierIndex--;
            }
            //add reward
            for (int rewardCounter = 0; rewardCounter < get; rewardCounter++) {

                final boolean appliedReward = applyDealReward(wrappedDeal, rewardIndex, instCnt, dealAmt, getList);
                if (appliedReward) {
                    rewardIndex++;
                }
            }
        }
        return instCnt;
    }


    private boolean applyDealReward(final DealWrapper wrappedDeal, final int rewardIndex, final int instCnt,
            final long dealAmt, final ProductWrapper[] getList) {

        boolean appliedReward = false;

        switch (wrappedDeal.getDealType()) {
            case DOLLAR_OFF_EACH:
                if (dealAmt < getList[rewardIndex].getUnitSellPrice()) {
                    getList[rewardIndex].setDealApplied(instCnt, DealItemTypeEnum.REWARD, dealAmt);
                }
                else {
                    getList[rewardIndex].setDealApplied(instCnt, DealItemTypeEnum.REWARD,
                            getList[rewardIndex].getUnitSellPrice());
                }
                appliedReward = true;
                break;
            case FIXED_DOLLAR_EACH:
                if (dealAmt < getList[rewardIndex].getUnitSellPrice()) {
                    getList[rewardIndex].setDealApplied(instCnt, DealItemTypeEnum.REWARD,
                            getList[rewardIndex].getUnitSellPrice() - dealAmt);
                }
                else {
                    getList[rewardIndex].setDealApplied(instCnt, DealItemTypeEnum.REWARD, 0);
                }
                appliedReward = true;
                break;
            case PERCENT_OFF_EACH:
                final long tmp = DealHelper.calcDiscTruncated(getList[rewardIndex].getUnitSellPrice(), (int)dealAmt);
                getList[rewardIndex].setDealApplied(instCnt, DealItemTypeEnum.REWARD, tmp);
                appliedReward = true;
                break;
            default:
                break;
        }
        return appliedReward;
    }


}
