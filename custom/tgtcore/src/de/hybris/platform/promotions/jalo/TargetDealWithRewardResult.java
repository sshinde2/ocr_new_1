package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;


public class TargetDealWithRewardResult extends GeneratedTargetDealWithRewardResult {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    public boolean getCouldFire(final SessionContext ctx) {
        boolean couldFire = false;
        final float certainty = getCertainty(ctx).floatValue();
        if (certainty < 1.0f) {
            couldFire = true;
        }
        else if (getNumberofRewardsSofarAsPrimitive() == 0
                && Float.valueOf(certainty).compareTo(Float.valueOf(1)) == 0) {
            couldFire = true;
        }
        return couldFire;
    }

    @Override
    public boolean getFired(final SessionContext ctx) {
        boolean isFired = false;
        final float certainty = getCertainty(ctx).floatValue();
        if ((certainty >= 1.0) && (getNumberofRewardsSofarAsPrimitive() > 0)) {
            isFired = true;
        }
        return isFired;
    }

    /**
     * check if minimum required qualifiers are added and number of rewards in basket is lesser than max qty
     * 
     * @param ctx
     * @return couldhaveMoreRewards
     */
    public boolean getCouldhaveMoreRewards(final SessionContext ctx) {

        boolean couldhaveMoreRewards = false;
        final float certainty = getCertainty(ctx).floatValue();
        final int maxNumberOfRewards = getMaxNumberofRewardsAsPrimitive();
        if ((certainty >= 1.0) && (getNumberofRewardsSofarAsPrimitive() < maxNumberOfRewards)) {
            couldhaveMoreRewards = true;
        }
        return couldhaveMoreRewards;

    }

    /**
     * 
     * @return couldhaveMoreRewards
     */
    public boolean getCouldhaveMoreRewards() {
        return getCouldhaveMoreRewards(getSession().getSessionContext());
    }

}
