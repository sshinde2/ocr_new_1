/**
 * 
 */
package de.hybris.platform.promotions.jalo;

import java.math.BigInteger;


/**
 * Stolen direct from POS team TgtMath class
 */
public final class DealHelper {
    private static final BigInteger TEN_THOUSAND = BigInteger.valueOf(10000);

    private DealHelper() {
        // prevent construction
    }

    /**************************************************************************
     * Calculate the percentage of a number rounding upward to the next whole number, internally ensuring there is no
     * integer overflow.
     * 
     * @param num
     * @param perc
     *            percentage with 2 implied decimal places, ie 100 is 1%, 750 is 7.5%
     * @return discount to be given.
     */
    public static int calcDiscTruncated(final int num, final int perc) {
        return (int)calcDiscTruncated((long)num, perc);
    }

    /**************************************************************************
     * Calculate the percentage of a number rounding upward to the next whole number, internally ensuring there is no
     * integer overflow.
     * 
     * @param num
     * @param perc
     *            percentage with 2 implied decimal places, ie 100 is 1%, 750 is 7.5%
     * @return discount to be given.
     */
    public static long calcDiscTruncated(final long num, final int perc) {
        BigInteger bigVal = BigInteger.valueOf(Math.abs(num));
        bigVal = bigVal.multiply(BigInteger.valueOf(perc));
        final BigInteger[] divResult = bigVal.divideAndRemainder(TEN_THOUSAND);
        long retVal = divResult[0].longValue();
        if (divResult[1].longValue() != 0) {
            retVal++;
        }

        if (num < 0) {
            return -retVal;
        }
        else {
            return retVal;
        }
    }

    /**************************************************************************
     * Apportion a discount of an array of discountable elements, the apportionment is weighted to the amount of the
     * original item.
     * 
     * @param discArr
     *            array of discountable elements
     * @param discAmt
     *            amount to apportion to the discountable array
     * 
     * @throws IllegalArgumentException
     *             discArr and discAmt a mixture of positive and negative numbers
     */
    public static void apportionDiscount(final DealHelperDiscountableInterface[] discArr, final long discAmt)
            throws IllegalArgumentException {
        if (discArr.length == 0) {
            return;
        }

        int cnt;
        if (discAmt == 0) {
            for (cnt = 0; cnt < discArr.length; cnt++) {
                discArr[cnt].setDiscAmt(0);
            }
            return;
        }

        int spendAmt = 0;
        final boolean discNeg = (discAmt < 0);
        for (cnt = 0; cnt < discArr.length; cnt++) {
            final long thisAmt = discArr[cnt].getPreDiscAmt();
            spendAmt += thisAmt;
            if (discNeg) {
                if (thisAmt > 0) {
                    throw new IllegalArgumentException();
                }
            }
            else {
                if (thisAmt < 0) {
                    throw new IllegalArgumentException();
                }
            }
        }

        // now spread the savings around
        if (discAmt >= spendAmt) { // shouldn't happen, but would not suprise me
            for (cnt = 0; cnt < discArr.length; cnt++) {
                discArr[cnt].setDiscAmt(discArr[cnt].getPreDiscAmt());
            }
            return;
        }

        long saveAmt = discAmt;
        for (cnt = 0; cnt < discArr.length; cnt++) {
            long lTemp = discArr[cnt].getPreDiscAmt();
            lTemp *= discAmt;
            lTemp *= 10; // the *10, +5, /10 is for rounding purposes
            lTemp /= spendAmt;
            lTemp += 5;
            lTemp /= 10;
            if (lTemp > discArr[cnt].getPreDiscAmt()) {
                lTemp = discArr[cnt].getPreDiscAmt();
            }
            discArr[cnt].setDiscAmt(lTemp);
            saveAmt -= lTemp;
        }

        // Because rounding does not always work out perfect, apportion
        // the last few cents.
        int temp = 1;
        if (saveAmt < 0) {
            temp = -1;
        }
        for (cnt = 0; saveAmt != 0; cnt++) {
            if (cnt == discArr.length) {
                cnt = 0;
            }

            if (temp > 0) {// giving more discount
                // make sure we don't go negative
                if (discArr[cnt].getPreDiscAmt() - discArr[cnt].getDiscAmt() < 1) {
                    continue;
                }
            }
            else { // subtracting discount
                   // make sure we don't go negative
                if (discArr[cnt].getDiscAmt() < 1) {
                    continue;
                }
            }
            discArr[cnt].setDiscAmt(discArr[cnt].getDiscAmt() + temp);
            saveAmt -= temp;
        }
    }

}
