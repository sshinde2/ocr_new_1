package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.util.Helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.jalo.TgtCoreManager;


//*************************************************************************
// Qantity break
// Example of this type of deal:
//          buy 1 item get for $10.00, buy 2 items get each for $9.00, buy 3 get each for $8.00
public class QuantityBreakDeal extends GeneratedQuantityBreakDeal {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.jalo.AbstractDeal#preApplyCheck(au.com.target.tgtcore.deals.wrappers.DealWrapper, java.util.List)
     */
    @Override
    protected List<PromotionResult> preApplyCheck(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts) {
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.jalo.AbstractDeal#applyDeal(au.com.target.tgtcore.deals.wrappers.DealWrapper, java.util.List)
     */
    @Override
    protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal, final List<ProductWrapper> wrappedProducts) {
        // make sure we meet basic requirements for this deal
        final List<DealBreakPoint> breakPoints = getBreakPoints();
        if (breakPoints == null) {
            return new ArrayList<>();
        }
        final int validItems = wrappedProducts.size();
        if (validItems == 0) {
            return new ArrayList<>();
        }

        // find the best price we qualify for
        double price = -1.0;
        double breakPrice;
        int minQty = -1;
        for (final DealBreakPoint breakPoint : breakPoints) {
            final int reqQty = breakPoint.getQtyAsPrimitive();
            if (reqQty <= validItems) {
                breakPrice = breakPoint.getUnitPriceAsPrimitive();
                if ((price < 0) || (price > breakPrice)) {
                    price = breakPrice;
                }
            }

            if (minQty < 0 || minQty > reqQty) {
                minQty = reqQty;
            }
        }
        if (price < 0) {
            return buildCouldHaveFiredResultList(wrappedProducts, wrappedDeal, (float)validItems / (float)minQty);
        }

        final long unitPrice = Math.round(price * 100);

        // apply deal to qualifying items
        final int instCnt = 1;
        long curSell;
        for (final ProductWrapper thisItem : wrappedProducts) {
            breakPrice = price;
            curSell = thisItem.getUnitSellPrice();
            if (curSell > unitPrice) {
                thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, curSell - unitPrice);
            }
            else {
                thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, 0);
            }
        }

        return buildFiredResultList(wrappedProducts, wrappedDeal, instCnt);
    }

    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);

            if (promotionResult.getFired(ctx)) {
                final double totalDiscount = promotionResult.getTotalDiscount(ctx);
                final long consumed = promotionResult.getConsumedCount(true);

                // need to find which break point we are using
                DealBreakPoint breakPoint = null;
                for (final DealBreakPoint checkBreakPoint : getBreakPoints(ctx)) {
                    if (checkBreakPoint.getQtyAsPrimitive(ctx) <= consumed) {
                        if (breakPoint == null ||
                                breakPoint.getUnitPriceAsPrimitive(ctx) > checkBreakPoint.getUnitPriceAsPrimitive(ctx)) {
                            breakPoint = checkBreakPoint;
                        }
                    }
                }

                final double unitPrice = breakPoint == null ? 0d : breakPoint.getUnitPriceAsPrimitive(ctx);
                final Integer tierQty = breakPoint == null ? Integer.valueOf(0) : breakPoint.getQty(ctx);

                final Object[] args = {
                        Double.valueOf(totalDiscount),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount),
                        Double.valueOf(unitPrice),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, unitPrice),
                        tierQty,
                        Long.valueOf(consumed)
                };
                return formatMessage(this.getMessageFired(ctx), args, locale);
            }
            else if (promotionResult.getCouldFire(ctx)) {
                int minQty = -1;
                for (final DealBreakPoint breakPoint : getBreakPoints()) {
                    final int reqQty = breakPoint.getQtyAsPrimitive();
                    if (minQty < 0 || minQty > reqQty) {
                        minQty = reqQty;
                    }
                }

                final int foundAmt = Math.round(minQty * promotionResult.getCertaintyAsPrimitive());
                final int shortAmt = minQty - foundAmt;

                final Object[] args = {
                        Integer.valueOf(minQty),
                        Integer.valueOf(foundAmt),
                        Integer.valueOf(shortAmt)
                };
                return formatMessage(this.getMessageCouldHaveFired(ctx), args, locale);
            }
        }
        return "";
    }

    @Override
    protected void deepCloneAttributes(final SessionContext ctx, final Map values) {
        super.deepCloneAttributes(ctx, values);

        // Keep all existing attributes apart from the breakpoints which we deep clone
        values.remove(QuantityBreakDeal.BREAKPOINTS);

        // Clone deal break points
        values.put(QuantityBreakDeal.BREAKPOINTS, deepCloneBreakPoints(ctx, getBreakPoints(ctx)));
    }

    /**
     * @param ctx
     * @param breakPoints
     * @return collection of deep cloned breakpoints
     */
    private Collection<DealBreakPoint> deepCloneBreakPoints(final SessionContext ctx,
            final Collection<DealBreakPoint> breakPoints) {
        final Collection<DealBreakPoint> dupbreakPoints = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(breakPoints)) {
            for (final DealBreakPoint breakPoint : breakPoints) {
                final Map parameters = new HashMap();
                parameters.put(DealBreakPoint.CODE, breakPoint.getCode(ctx));
                parameters.put(DealBreakPoint.QTY, breakPoint.getQty(ctx));
                parameters.put(DealBreakPoint.UNITPRICE, breakPoint.getUnitPrice(ctx));

                dupbreakPoints.add(TgtCoreManager.getInstance().createDealBreakPoint(ctx, parameters));
            }
        }
        return dupbreakPoints;
    }

}
