package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.DealWrapper.DealType;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class SpendAndSaveDealTest {
    private class ProductWrapperTestImpl implements ProductWrapper {
        private boolean inDeal = false;

        private final AbstractTargetVariantProduct p;
        private final long unitPrice;
        private long markdown;
        private boolean rewardpartaillyConsumed = false;

        public ProductWrapperTestImpl(final AbstractTargetVariantProduct p, final long unitPrice) {
            this.p = p;
            this.unitPrice = unitPrice;
        }

        @Override
        public DealQualifierModel getQualifierModel() {
            return q1;
        }

        @Override
        public long getUnitSellPrice() {
            return unitPrice;
        }

        @Override
        public long getAdjustedUnitSellPrice() {
            return unitPrice - markdown;
        }

        @Override
        public void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt) {
            markdown = saveAmt;
            inDeal = true;
        }

        @Override
        public boolean isInDeal() {
            return inDeal;
        }

        @Override
        public long getDealMarkdown() {
            return markdown;
        }

        @Override
        public Product getProduct() {
            return p;
        }

        @Override
        public DealItemTypeEnum getType() {
            return null;
        }

        @Override
        public int getInstance() {
            return 0;
        }

        @Override
        public void clearDeal() {
            // these are not the droids you are looking for
        }

        @Override
        public boolean isRewardProduct() {
            return false;
        }


        @Override
        public void setDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = true;

        }


        @Override
        public boolean getDealRewardsPartiallyConsumed() {

            return rewardpartaillyConsumed;
        }


        @Override
        public void clearDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = false;
        }
    }

    private class DealWrapperTestImpl implements DealWrapper {
        private DealType dealType;

        @Override
        public AbstractDealModel getDealModel() {
            return dealModel;
        }

        @Override
        public List<DealQualifierModel> getQualifierModelList() {
            return qualList;
        }

        @Override
        public SessionContext getCtx() {
            return null;
        }

        @Override
        public PromotionEvaluationContext getPromoContext() {
            return null;
        }

        @Override
        public PromotionOrderView getOrderView() {
            return null;
        }

        @Override
        public DealType getDealType() {
            return dealType;
        }

        public void setDealType(final DealType dealType) {
            this.dealType = dealType;
        }
    }


    @Mock
    private SpendAndSaveDealModel dealModel;

    @Mock
    private DealQualifierModel q1;

    @Mock
    private AbstractTargetVariantProduct p1;
    @Mock
    private AbstractTargetVariantProduct p2;
    @Mock
    private AbstractTargetVariantProduct p3;
    @Mock
    private AbstractTargetVariantProduct p4;

    private boolean dealApplied = false;
    private final SpendAndSaveDeal deal = new SpendAndSaveDeal() {

        @Override
        public int getRewardMaxQtyAsPrimitive() {
            return dealModel.getRewardMaxQty().intValue();
        }

        @Override
        public double getRewardValueAsPrimitive() {
            return dealModel.getRewardValue().doubleValue();
        }

        @Override
        public double getRewardMinSaveAsPrimitive() {
            return dealModel.getRewardMinSave().doubleValue();
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }

        @Override
        protected java.util.List<PromotionResult> buildCouldHaveFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList, final DealWrapper dealWrapper,
                final float certainty) {
            return new ArrayList<>();
        }
    };

    private final List<DealQualifierModel> qualList = new ArrayList<>();
    private final DealWrapper wrappedDeal = new DealWrapperTestImpl();
    private final List<ProductWrapper> wrappedProducts = new ArrayList<>();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(dealModel.getQualifierList()).willReturn(qualList);
    }

    private long sumProductMarkdowns() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getDealMarkdown();
        }
        return retVal;
    }

    private long sumTotalPrice() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getAdjustedUnitSellPrice();
        }
        return retVal;
    }

    //**************************************************************************
    protected final int itemsWithValue(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getAdjustedUnitSellPrice() == amt) {
                ret++;
            }
        }

        return ret;
    }


    //**************************************************************************
    protected final int itemsWithMarkdown(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getDealMarkdown() == amt) {
                ret++;
            }
        }

        return ret;
    }

    //##################################################################################
    // Spend $20 and save 10% off
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend20DolSave10Pct() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffEnoughItemsForReward() throws Exception {
        setupDealSpend20DolSave10Pct();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(150, pw1.getDealMarkdown());
        Assert.assertEquals(150, pw2.getDealMarkdown());
        Assert.assertEquals(150, pw3.getDealMarkdown());
        Assert.assertEquals(4050, sumTotalPrice());
        Assert.assertEquals(450, sumProductMarkdowns());
    }

    //##################################################################################
    // Spend $20 and save $10 off
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend20DolSave10Dol() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_TOTAL);
    }

    @Test
    public void testApplyDealDolOffEnoughItemsForReward() throws Exception {
        setupDealSpend20DolSave10Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(2, itemsWithMarkdown(333));
        Assert.assertEquals(1, itemsWithMarkdown(334));
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    //##################################################################################
    // Spend $5 and save 2c off
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend5DolSave2Cents() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(5.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(0.02));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_TOTAL);
    }

    @Test
    public void testApplyDealCentsRoundingCheck() throws Exception {
        setupDealSpend5DolSave2Cents();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(2, itemsWithMarkdown(1));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(4498, sumTotalPrice());
        Assert.assertEquals(2, sumProductMarkdowns());
    }

}
