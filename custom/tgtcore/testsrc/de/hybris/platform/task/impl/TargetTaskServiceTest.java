/**
 * 
 */
package de.hybris.platform.task.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.util.config.ConfigIntf;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;




/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class })
public class TargetTaskServiceTest {


    @Before
    public void setup() {
        mockStatic(Registry.class);
        final Tenant tenant = mock(Tenant.class);
        final ConfigIntf conf = mock(ConfigIntf.class);
        given(tenant.getConfig()).willReturn(conf);
        willReturn(new Integer(2)).given(conf).getInt("tasks.shutdown.wait", 10);
        given(Registry.getCurrentTenantNoFallback()).willReturn(tenant);
    }

    @Test
    public void testGetTaskQueryWhenProcessNotToBeStalled() {
        final TargetTaskService targetTaskService = new TargetTaskService();
        final String getTaskQuery = targetTaskService.getTaskQuery(false);
        assertThat(getTaskQuery).doesNotContain("procs.p_state = :failedProcessState");

    }

    @Test
    public void testGetTaskQueryWhenProcessToBeStalled() {
        final TargetTaskService targetTaskService = new TargetTaskService();
        final String getTaskQuery = targetTaskService.getTaskQuery(true);
        assertThat(getTaskQuery).contains("procs.p_state = :failedProcessState");

    }

}
