/**
 * 
 */

package au.com.target.tgtcore.refund.denialstrategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSalesChannelRefundDenialStrategyTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private final TargetSalesChannelRefundDenialStrategy strategy = new TargetSalesChannelRefundDenialStrategy();

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    private OrderModel order;

    @Mock
    private AbstractOrderEntryModel entry;


    @Test
    public void testPerformExceptions() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order can't be null.");
        strategy.perform(null, entry, 0);
    }

    @Test
    public void testPerformDenied() {
        final SalesApplication salesApp = SalesApplication.TRADEME;
        Mockito.doReturn(Boolean.TRUE).when(salesApplicationConfigService).isRefundDenied(salesApp);
        BDDMockito.given(order.getSalesApplication()).willReturn(salesApp);

        Assert.assertFalse(strategy.perform(order, entry, 1));
    }

    @Test
    public void testPerformNotDenied() {
        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.EBAY);
        Assert.assertTrue(strategy.perform(order, entry, 1));
    }

}
