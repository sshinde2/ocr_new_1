package au.com.target.tgtcore.refund.denialstrategies;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPaymentTypeRefundDenailStrategyTest {

    private final TargetPaymentTypeRefundDenailStrategy targetPaymentTypeRefundDenailStrategy = new TargetPaymentTypeRefundDenailStrategy();

    @Test
    public void testPerformWithNoPaymentInfo() throws Exception {
        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(null);

        final boolean isReturnable = targetPaymentTypeRefundDenailStrategy.perform(mockOrder, null, 0);
        assertThat(isReturnable).isTrue();
    }

    @Test
    public void testPerformWithNonPinPadPaymentInfo() throws Exception {
        final CreditCardPaymentInfoModel mockPaymentInfo = mock(CreditCardPaymentInfoModel.class);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(mockPaymentInfo);

        final boolean isReturnable = targetPaymentTypeRefundDenailStrategy.perform(mockOrder, null, 0);
        assertThat(isReturnable).isTrue();
    }

    @Test
    public void testPerformWithNonBasicsCardPaymentType() throws Exception {
        final PinPadPaymentInfoModel mockPaymentInfo = mock(PinPadPaymentInfoModel.class);
        given(mockPaymentInfo.getCardType()).willReturn(CreditCardType.VISA);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(mockPaymentInfo);

        final boolean isReturnable = targetPaymentTypeRefundDenailStrategy.perform(mockOrder, null, 0);
        assertThat(isReturnable).isTrue();
    }

    @Test
    public void testPerformWithBasicsCardPaymentType() throws Exception {
        final PinPadPaymentInfoModel mockPaymentInfo = mock(PinPadPaymentInfoModel.class);
        given(mockPaymentInfo.getCardType()).willReturn(CreditCardType.BASICSCARD);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(mockPaymentInfo);

        final boolean isReturnable = targetPaymentTypeRefundDenailStrategy.perform(mockOrder, null, 0);
        assertThat(isReturnable).isFalse();
    }
}
