/**
 * 
 */
package au.com.target.tgtcore.refund.denialstrategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysDiscRefundDenialStrategyTest {

    @Mock
    private OrderModel order;

    @Mock
    private FlybuysDiscountModel flybuysDiscount;

    @Mock
    private TargetVoucherService targetVoucherService;

    @InjectMocks
    private final FlybuysDiscRefundDenialStrategy strategy = new FlybuysDiscRefundDenialStrategy();

    @Mock
    private AbstractOrderEntryModel entry;

    @Test
    public void testPerformOrderWithFbDisc() {
        BDDMockito.given(targetVoucherService.getFlybuysDiscountForOrder(order)).willReturn(flybuysDiscount);
        final boolean result = strategy.perform(order, entry, 1l);
        Assert.assertFalse(result);
    }

    @Test
    public void testPerformOrderWithoutFbDisc() {
        BDDMockito.given(targetVoucherService.getFlybuysDiscountForOrder(order)).willReturn(null);
        final boolean result = strategy.perform(order, entry, 1l);
        Assert.assertTrue(result);
    }
}
