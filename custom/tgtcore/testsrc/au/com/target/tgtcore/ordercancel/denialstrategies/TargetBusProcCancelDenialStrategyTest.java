/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author sbryan6
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetBusProcCancelDenialStrategyTest {

    private final TargetBusProcCancelDenialStrategy targetBusProcCancelDenialStrategy = new TargetBusProcCancelDenialStrategy();

    @Mock
    private OrderModel order;

    @Mock
    private OrderProcessModel notExcludedprocess;

    @Mock
    private OrderProcessModel excludedprocess;

    @Mock
    private OrderCancelDenialReason mockReason;

    private final List<String> excludedBusinessProcesses = Arrays.asList("excludedprocess");

    @Before
    public void setup() {
        targetBusProcCancelDenialStrategy.setReason(mockReason);
        targetBusProcCancelDenialStrategy.setExcludedBusinessProcesses(excludedBusinessProcesses);

        Mockito.when(notExcludedprocess.getProcessDefinitionName()).thenReturn("notExcludedprocess");
        Mockito.when(excludedprocess.getProcessDefinitionName()).thenReturn("excludedprocess");
    }

    @Test
    public void testNoProcesses() {
        Mockito.when(order.getOrderProcess()).thenReturn(Collections.EMPTY_SET);

        final OrderCancelDenialReason orderCancelDenialReason = targetBusProcCancelDenialStrategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testProcessNotRunning() {
        Mockito.when(order.getOrderProcess()).thenReturn(Collections.singleton(notExcludedprocess));
        Mockito.when(notExcludedprocess.getState()).thenReturn(ProcessState.SUCCEEDED);

        final OrderCancelDenialReason orderCancelDenialReason = targetBusProcCancelDenialStrategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testProcessIsRunningNotExcluded() {
        Mockito.when(order.getOrderProcess()).thenReturn(Collections.singleton(notExcludedprocess));
        Mockito.when(notExcludedprocess.getState()).thenReturn(ProcessState.RUNNING);

        final OrderCancelDenialReason orderCancelDenialReason = targetBusProcCancelDenialStrategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNotNull();
        assertThat(orderCancelDenialReason).isEqualTo(mockReason);
    }

    @Test
    public void testProcessIsRunningExcluded() {
        Mockito.when(order.getOrderProcess()).thenReturn(Collections.singleton(excludedprocess));
        Mockito.when(excludedprocess.getState()).thenReturn(ProcessState.RUNNING);

        final OrderCancelDenialReason orderCancelDenialReason = targetBusProcCancelDenialStrategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

}
