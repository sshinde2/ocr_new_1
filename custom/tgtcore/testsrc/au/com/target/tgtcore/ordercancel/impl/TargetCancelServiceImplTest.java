/**
 * 
 */
package au.com.target.tgtcore.ordercancel.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.order.price.Discount;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.ordercancel.OrderCancelResponse.ResponseStatus;
import de.hybris.platform.ordercancel.OrderStatusChangeStrategy;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.discountstrategy.DiscountValueCorrectionStrategy;
import au.com.target.tgtcore.stock.TargetStockService;


/**
 * Unit test for ({@link TargetCancelServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCancelServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @InjectMocks
    private final TargetCancelServiceImpl cancelService = new TargetCancelServiceImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @Mock
    private OrderStatusChangeStrategy completeCancelStatusChangeStrategy;

    @Mock
    private OrderStatusChangeStrategy partialCancelStatusChangeStrategy;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private WarehouseService warehouseService;

    @Mock
    private CalculationService calculationService;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private SearchRestrictionService searchRestrictionService;

    @Mock
    private TargetRefundPaymentService targetRefundPaymentService;

    @Mock
    private TargetDiscountService targetDiscountService;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private DiscountValueCorrectionStrategy discountValueCorrectionStrategy;

    @Mock
    private TargetOrderCancelRequest orderCancelRequest;

    @Mock
    private OrderCancelRecordEntryModel orderCancelRecordEntryModel;

    @Mock
    private OrderModel orderModel;
    private Order orderSource;

    @Mock
    private AbstractOrderEntryModel orderEntryModel;

    @Mock
    private OrderCancelEntry cancelEntry;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private DiscountModel discountModel;
    private Discount discountSource;

    @Mock
    private ProductTypeModel productType;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private AbstractTargetVariantProductModel abstractTargetVariantProductModel;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private TargetZoneDeliveryModeValueModel targetValueModel;

    @Mock
    private RestrictableTargetZoneDeliveryModeValueModel restrictableValueModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Before
    public void setUp() {
        BDDMockito.given(productModel.getName()).willReturn("I-Pod");
        BDDMockito.given(orderCancelRequest.getOrder()).willReturn(orderModel);
        BDDMockito.given(warehouseService.getDefWarehouse()).willReturn(Collections.singletonList(warehouseModel));

        when(targetDiscountService.createGlobalDiscountForOrder(Mockito.any(OrderModel.class), Mockito.anyDouble(),
                Mockito.anyString())).thenReturn(discountModel);

        orderSource = Mockito.mock(Order.class);
        when(modelService.getSource(orderModel)).thenReturn(orderSource);
        discountSource = Mockito.mock(Discount.class);
        when(modelService.getSource(discountModel)).thenReturn(discountSource);
        BDDMockito.given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(1));
    }

    @SuppressWarnings("boxing")
    public void setupOrderAndCancelEntryFullCancel() {
        setupOrderForCancel();
        BDDMockito.given(cancelEntry.getCancelQuantity()).willReturn(Long.valueOf(5));
        BDDMockito.given(cancelEntry.getOrderEntry()).willReturn(orderEntryModel);
        BDDMockito.given(orderCancelRequest.getEntriesToCancel()).willReturn(Collections.singletonList(cancelEntry));
    }

    @SuppressWarnings("boxing")
    public void setupOrderAndCancelEntryPartialCancel() {
        setupOrderForCancel();
        BDDMockito.given(cancelEntry.getCancelQuantity()).willReturn(Long.valueOf(3));
        BDDMockito.given(cancelEntry.getOrderEntry()).willReturn(orderEntryModel);
        BDDMockito.given(orderCancelRequest.getEntriesToCancel()).willReturn(Collections.singletonList(cancelEntry));
        BDDMockito.given(orderCancelRequest.isPartialCancel()).willReturn(true);
    }

    @SuppressWarnings("boxing")
    public void setupOrderForCancel() {

        BDDMockito.given(orderEntryModel.getProduct()).willReturn(productModel);
        BDDMockito.given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(5));
        BDDMockito.given(orderEntryModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(orderModel.getEntries()).willReturn(Collections.singletonList(orderEntryModel));
    }

    @Test
    public void testModifyFullCancelWithoutConsignments() throws OrderCancelException {

        setupOrderAndCancelEntryFullCancel();

        cancelService.modifyOrderAccordingToRequest(orderCancelRequest, orderModel);

        Mockito.verify(orderEntryModel).setQuantity(Long.valueOf(0));
        Mockito.verify(orderEntryModel).setQuantityStatus(OrderEntryStatus.DEAD);
        verify(discountValueCorrectionStrategy).correctDiscountValues(orderEntryModel, 5);
        Mockito.verify(modelService).save(orderEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testModifyFullCancelWithConsignments() throws OrderCancelException {

        setupOrderAndCancelEntryFullCancel();

        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        BDDMockito.given(orderModel.getConsignments()).willReturn(Collections.singleton(consignment));

        BDDMockito.given(orderCancelRequest.isPartialCancel()).willReturn(Boolean.FALSE);

        cancelService.modifyOrderAccordingToRequest(orderCancelRequest, orderModel);

        Mockito.verify(orderEntryModel).setQuantity(Long.valueOf(0));
        Mockito.verify(orderEntryModel).setQuantityStatus(OrderEntryStatus.DEAD);
        verify(discountValueCorrectionStrategy).correctDiscountValues(orderEntryModel, 5);
        Mockito.verify(modelService).save(orderEntryModel);
    }

    @Test
    public void testModifyPartialCancelWithoutConsignments() throws OrderCancelException {

        setupOrderAndCancelEntryPartialCancel();

        cancelService.modifyOrderAccordingToRequest(orderCancelRequest, orderModel);

        Mockito.verify(orderEntryModel).setQuantity(Long.valueOf(2));
        verify(discountValueCorrectionStrategy).correctDiscountValues(orderEntryModel, 5);
        Mockito.verify(modelService).save(orderEntryModel);
    }

    @Test
    public void testProcessPartialCancelRequestWithConsignments() throws OrderCancelException {

        setupOrderAndCancelEntryPartialCancel();

        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final ConsignmentEntryModel consignmentEntry = Mockito.mock(ConsignmentEntryModel.class);
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        BDDMockito.given(orderEntryModel.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        BDDMockito.given(orderModel.getConsignments()).willReturn(Collections.singleton(consignment));

        BDDMockito.given(modelService.clone(consignmentEntry)).willReturn(consignmentEntry);

        cancelService.modifyOrderAccordingToRequest(orderCancelRequest, orderModel);

        Mockito.verify(orderEntryModel).setQuantity(Long.valueOf(2));
        verify(discountValueCorrectionStrategy).correctDiscountValues(orderEntryModel, 5);
        Mockito.verify(modelService).save(orderEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testModifyInvalidQty() throws OrderCancelException {

        BDDMockito.given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(5));
        BDDMockito.given(orderModel.getEntries()).willReturn(Collections.singletonList(orderEntryModel));

        BDDMockito.given(cancelEntry.getCancelQuantity()).willReturn(Long.valueOf(6));
        BDDMockito.given(cancelEntry.getOrderEntry()).willReturn(orderEntryModel);
        BDDMockito.given(orderCancelRequest.getEntriesToCancel()).willReturn(Collections.singletonList(cancelEntry));
        BDDMockito.given(orderCancelRequest.isPartialCancel()).willReturn(true);

        expectedException.expect(OrderCancelException.class);
        expectedException.expectMessage("Trying to cancel ");

        cancelService.modifyOrderAccordingToRequest(orderCancelRequest, orderModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testMakeInternalResponsePartial() {

        setupOrderAndCancelEntryFullCancel();

        BDDMockito.given(orderCancelRequest.isPartialCancel()).willReturn(true);

        final OrderCancelResponse response = cancelService.makeInternalResponse(orderCancelRequest, true, "Hello");

        Assert.assertEquals(1, response.getEntriesToCancel().size());
        Assert.assertEquals(cancelEntry, response.getEntriesToCancel().get(0));
        Assert.assertEquals("Hello", response.getNotes());
        Assert.assertEquals(orderModel, response.getOrder());
        Assert.assertEquals(ResponseStatus.partial, response.getResponseStatus());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testMakeInternalResponseFull() {

        setupOrderAndCancelEntryFullCancel();

        BDDMockito.given(orderCancelRequest.isPartialCancel()).willReturn(false);

        final OrderCancelResponse response = cancelService.makeInternalResponse(orderCancelRequest, true, "Hello");

        Assert.assertEquals(1, response.getEntriesToCancel().size());
        Assert.assertEquals(cancelEntry, response.getEntriesToCancel().get(0));
        Assert.assertEquals("Hello", response.getNotes());
        Assert.assertEquals(orderModel, response.getOrder());
        Assert.assertEquals(ResponseStatus.full, response.getResponseStatus());
    }

    @Test
    public void testReleaseStock() throws OrderCancelException {

        setupOrderAndCancelEntryFullCancel();

        cancelService.releaseStock(orderCancelRequest);

        Mockito.verify(targetStockService).release(productModel, 5,
                "5 stock has been released for I-Pod");
    }

    @Test
    public void testUpdateRecordEntry() throws OrderCancelRecordsHandlerException {

        setupOrderAndCancelEntryFullCancel();

        final ArgumentCaptor<OrderCancelResponse> argumentCaptor = ArgumentCaptor.forClass(OrderCancelResponse.class);

        cancelService.updateRecordEntry(orderCancelRequest);

        Mockito.verify(orderCancelRecordsHandler).updateRecordEntry(argumentCaptor.capture());
    }

    @Test
    public void testRecalculateOrderForCalculationException() throws CalculationException {
        expectedException.expect(TargetOrderCancelException.class);
        expectedException.expectMessage("Cannot calculate order for refund");

        final OrderModel order = Mockito.mock(OrderModel.class);

        BDDMockito.willThrow(new CalculationException("cannot calculate order")).given(calculationService)
                .calculateTotals(order, false);

        cancelService.recalculateOrder(order);
    }

    @Test
    public void testRecalculateOrder() throws CalculationException {

        final OrderModel order = Mockito.mock(OrderModel.class);

        cancelService.recalculateOrder(order);

        Mockito.verify(calculationService).calculateTotals(order, false);
    }

    @Test
    public void testRecalculateOrderPreventingIncreaseGoDown() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(5.50));
        cancelService.recalculateOrderPreventingIncrease(orderModel, null);

        final InOrder inOrder = Mockito.inOrder(targetDiscountService, calculationService);
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
        inOrder.verify(targetDiscountService).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.eq(.5),
                Mockito.anyString());
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
    }

    @Test
    public void testRecalculateOrderPreventingIncreaseGoUp() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(4.60));
        cancelService.recalculateOrderPreventingIncrease(orderModel, null);

        verify(calculationService).calculateTotals(orderModel, false);
        verify(targetDiscountService, Mockito.times(0)).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.anyDouble(), Mockito.anyString());
        verifyNoMoreInteractions(calculationService);
    }

    @Test
    public void testRecalculateOrderPreventingIncreaseNoChange() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(5.00));
        cancelService.recalculateOrderPreventingIncrease(orderModel, null);

        verify(calculationService).calculateTotals(orderModel, false);
        verify(targetDiscountService, Mockito.times(0)).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.anyDouble(), Mockito.anyString());
        verifyNoMoreInteractions(calculationService);
    }

    @SuppressWarnings({ "boxing", "deprecation" })
    @Test
    public void testRecalculateOrderPreventingIncreaseNegativeNewValue() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(-2.00));
        final List<DiscountValue> discountValues = new ArrayList<DiscountValue>(1);
        final DiscountValue discountValue = Mockito.mock(DiscountValue.class);
        discountValues.add(discountValue);
        when(orderSource.getGlobalDiscountValues()).thenReturn(discountValues);
        when(discountValue.getAppliedValue()).thenReturn(Double.valueOf(3.00));

        cancelService.recalculateOrderPreventingIncrease(orderModel, null);

        verify(orderSource).removeGlobalDiscountValue(discountValue);
        final InOrder inOrder = Mockito.inOrder(targetDiscountService, calculationService);
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
        inOrder.verify(targetDiscountService).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.eq(1.00d), Mockito.anyString());
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
    }

    @Test
    public void testUpdateShippingAmountForCancelFullCancel() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        final List<AbstractOrderEntryModel> entryModels = Collections.emptyList();

        when(order.getEntries()).thenReturn(entryModels);
        when(order.getDeliveryCost()).thenReturn(Double.valueOf(9.0d));

        cancelService.updateShippingAmountForCancel(order, orderCancelRecordEntryModel);

        verify(order).setDeliveryCost(Double.valueOf(0.0d));
        verify(orderCancelRecordEntryModel).setRefundedShippingAmount(Double.valueOf(9.0d));
        verify(modelService).saveAll(order, orderCancelRecordEntryModel);
    }

    @SuppressWarnings("boxing")
    public void setUpCommonDataForUpdateShippingAmountForCancel() {
        final List<AbstractOrderEntryModel> entryModels = new ArrayList<>();
        entryModels.add(orderEntryModel);

        when(orderModel.getEntries()).thenReturn(entryModels);
        when(orderModel.getDeliveryCost()).thenReturn(Double.valueOf(30.0d));
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(45.00));
        when(orderModel.getSubtotal()).thenReturn(Double.valueOf(45.00));
        when(commonI18NService.roundCurrency(45.0d, 2)).thenReturn(45.0d);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(restrictableValueModel);

        when(targetValueModel.getValue()).thenReturn(9.0d);
        when(targetValueModel.getBulky()).thenReturn(20.0d);
        when(targetValueModel.getExtendedBulky()).thenReturn(30.0d);
        when(orderEntryModel.getProduct()).thenReturn(abstractTargetVariantProductModel);
        when(abstractTargetVariantProductModel.getProductType()).thenReturn(productType);
        when(productType.getBulky()).thenReturn(Boolean.TRUE);
        when(orderModel.getCurrency()).thenReturn(currencyModel);
        when(currencyModel.getDigits()).thenReturn(Integer.valueOf(2));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateShippingAmountForCancelPartialCancelRestrictableZoneDelValueChanged() {
        setupOrderAndCancelEntryPartialCancel();
        setUpCommonDataForUpdateShippingAmountForCancel();

        when(orderEntryModel.getQuantity()).thenReturn(1L);
        final RestrictableTargetZoneDeliveryModeValueModel newDeliveryModeValue = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        final ZoneDeliveryModeModel zoneDeliveryModeModel = Mockito.mock(ZoneDeliveryModeModel.class);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(restrictableValueModel);
        when(orderModel.getDeliveryMode()).thenReturn(zoneDeliveryModeModel);
        when(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, orderModel))
                .thenReturn(newDeliveryModeValue);
        when(newDeliveryModeValue.getValue()).thenReturn(Double.valueOf(9d));
        when(restrictableValueModel.getValue()).thenReturn(Double.valueOf(15d));
        when(commonI18NService.roundCurrency(21.0d, 2)).thenReturn(21.0d);

        cancelService.updateShippingAmountForCancel(orderModel, orderCancelRecordEntryModel);

        verify(orderModel).setZoneDeliveryModeValue(newDeliveryModeValue);
        verify(orderCancelRecordEntryModel).setRefundedShippingAmount(Double.valueOf(21d));
        verify(modelService).saveAll(orderModel, orderCancelRecordEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateShippingAmountForCancelPartialCancelRestrictableZoneDelValueNOTChanged() {
        setupOrderAndCancelEntryPartialCancel();
        setUpCommonDataForUpdateShippingAmountForCancel();

        when(orderEntryModel.getQuantity()).thenReturn(1L);
        final RestrictableTargetZoneDeliveryModeValueModel newDeliveryModeValue = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        final ZoneDeliveryModeModel zoneDeliveryModeModel = Mockito.mock(ZoneDeliveryModeModel.class);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(restrictableValueModel);
        when(orderModel.getDeliveryMode()).thenReturn(zoneDeliveryModeModel);
        when(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, orderModel))
                .thenReturn(newDeliveryModeValue);
        when(newDeliveryModeValue.getValue()).thenReturn(Double.valueOf(30d));
        when(restrictableValueModel.getValue()).thenReturn(Double.valueOf(30d));

        cancelService.updateShippingAmountForCancel(orderModel, orderCancelRecordEntryModel);

        verify(orderModel, Mockito.never()).setZoneDeliveryModeValue(newDeliveryModeValue);
        verify(orderCancelRecordEntryModel, Mockito.never()).setRefundedShippingAmount(Double.valueOf(6d));
        verify(modelService, Mockito.never()).saveAll(orderModel, orderCancelRecordEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateShippingAmountForCancelPartialCancelTargetZoneDelModeWithZeroBulky() {
        setupOrderAndCancelEntryPartialCancel();
        setUpCommonDataForUpdateShippingAmountForCancel();

        final ZoneDeliveryModeModel zoneDeliveryModeModel = Mockito.mock(ZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeValueModel newDeliveryModeValue = Mockito
                .mock(TargetZoneDeliveryModeValueModel.class);

        when(productType.getBulky()).thenReturn(Boolean.FALSE);
        when(orderEntryModel.getQuantity()).thenReturn(1L);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(targetValueModel);
        when(commonI18NService.roundCurrency(21.0d, 2)).thenReturn(21.0d);
        when(orderModel.getDeliveryMode()).thenReturn(zoneDeliveryModeModel);
        when(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(
                zoneDeliveryModeModel, orderModel))
                        .thenReturn(newDeliveryModeValue);

        cancelService.updateShippingAmountForCancel(orderModel, orderCancelRecordEntryModel);

        verify(orderModel).setDeliveryCost(Double.valueOf(9.0d));
        verify(orderCancelRecordEntryModel).setRefundedShippingAmount(Double.valueOf(21.0d));
        verify(modelService).saveAll(orderModel, orderCancelRecordEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateShippingAmountForCancelPartialCancelTargetZoneDelModeWithOneBulky() {
        setupOrderAndCancelEntryPartialCancel();
        setUpCommonDataForUpdateShippingAmountForCancel();

        final ZoneDeliveryModeModel zoneDeliveryModeModel = Mockito.mock(ZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeValueModel newDeliveryModeValue = Mockito
                .mock(TargetZoneDeliveryModeValueModel.class);

        when(orderEntryModel.getQuantity()).thenReturn(1L);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(targetValueModel);
        when(commonI18NService.roundCurrency(10.0d, 2)).thenReturn(10.0d);
        when(orderModel.getDeliveryMode()).thenReturn(zoneDeliveryModeModel);
        when(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(
                zoneDeliveryModeModel, orderModel))
                        .thenReturn(newDeliveryModeValue);

        cancelService.updateShippingAmountForCancel(orderModel, orderCancelRecordEntryModel);

        verify(orderModel, Mockito.never()).setZoneDeliveryModeValue(newDeliveryModeValue);
        verify(orderModel).setDeliveryCost(Double.valueOf(20.0d));
        verify(orderCancelRecordEntryModel).setRefundedShippingAmount(Double.valueOf(10.0d));
        verify(modelService).saveAll(orderModel, orderCancelRecordEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateShippingAmountForCancelPartialCancelTargetZoneDelModeWithThreeBulky() {
        setupOrderAndCancelEntryPartialCancel();
        setUpCommonDataForUpdateShippingAmountForCancel();

        when(orderEntryModel.getQuantity()).thenReturn(3L);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(targetValueModel);

        cancelService.updateShippingAmountForCancel(orderModel, orderCancelRecordEntryModel);

        verify(orderModel, Mockito.never()).setDeliveryCost(Double.valueOf(30.0d));
        verify(orderCancelRecordEntryModel, Mockito.never()).setRefundedShippingAmount(Double.valueOf(0.0d));
        verify(modelService, Mockito.never()).saveAll(orderModel, orderCancelRecordEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateShippingAmountForCancelPartialCancelWithNewTargetZDMV() {
        setupOrderAndCancelEntryPartialCancel();
        setUpCommonDataForUpdateShippingAmountForCancel();

        final ZoneDeliveryModeModel zoneDeliveryModeModel = Mockito.mock(ZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeValueModel newDeliveryModeValue = Mockito
                .mock(TargetZoneDeliveryModeValueModel.class);

        when(orderEntryModel.getQuantity()).thenReturn(1L);
        when(orderModel.getZoneDeliveryModeValue()).thenReturn(targetValueModel);
        when(commonI18NService.roundCurrency(10.0d, 2)).thenReturn(10.0d);
        when(commonI18NService.roundCurrency(30.0d, 2)).thenReturn(30.0d);
        when(orderModel.getDeliveryMode()).thenReturn(zoneDeliveryModeModel);
        when(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(
                zoneDeliveryModeModel, orderModel))
                        .thenReturn(newDeliveryModeValue);
        when(targetValueModel.getMinimum()).thenReturn(Double.valueOf(50.0d));


        cancelService.updateShippingAmountForCancel(orderModel, orderCancelRecordEntryModel);

        verify(orderModel).setZoneDeliveryModeValue(newDeliveryModeValue);
        verify(orderModel).setDeliveryCost(Double.valueOf(0.0d));
        verify(orderCancelRecordEntryModel).setRefundedShippingAmount(Double.valueOf(30.0d));
        verify(modelService).saveAll(orderModel, orderCancelRecordEntryModel);
    }

    @Test
    public void testPreviewOrderPreventingIncreaseGoDown() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(5.50));
        cancelService.previewOrderPreventingIncrease(orderModel, 0d);

        final InOrder inOrder = Mockito.inOrder(targetDiscountService, calculationService);
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
        inOrder.verify(targetDiscountService).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.eq(.5),
                Mockito.anyString());
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
    }

    @Test
    public void testPreviewOrderPreventingIncreaseGoUp() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(4.60));
        cancelService.previewOrderPreventingIncrease(orderModel, 0d);

        verify(calculationService).calculateTotals(orderModel, false);
        verify(targetDiscountService, Mockito.times(0)).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.anyDouble(), Mockito.anyString());
        verifyNoMoreInteractions(calculationService);
    }

    @Test
    public void testPreviewOrderPreventingIncreaseNoChange() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(5.00));
        cancelService.previewOrderPreventingIncrease(orderModel, 0d);

        verify(calculationService).calculateTotals(orderModel, false);
        verify(targetDiscountService, Mockito.times(0)).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.anyDouble(), Mockito.anyString());
        verifyNoMoreInteractions(calculationService);
    }

    @SuppressWarnings({ "boxing", "deprecation" })
    @Test
    public void testPreviewOrderPreventingIncreaseNegativeNewValue() throws CalculationException {
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf(5.00), Double.valueOf(2.00));
        final List<DiscountValue> discountValues = new ArrayList<DiscountValue>(1);
        final DiscountValue discountValue = Mockito.mock(DiscountValue.class);
        discountValues.add(discountValue);
        when(orderSource.getGlobalDiscountValues()).thenReturn(discountValues);
        when(discountValue.getAppliedValue()).thenReturn(Double.valueOf(20.00));

        cancelService.previewOrderPreventingIncrease(orderModel, 9d);

        verify(orderSource).removeGlobalDiscountValue(discountValue);
        final InOrder inOrder = Mockito.inOrder(targetDiscountService, calculationService);
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
        inOrder.verify(targetDiscountService).createGlobalDiscountForOrder(Mockito.any(OrderModel.class),
                Mockito.eq(13.00d), Mockito.anyString());
        inOrder.verify(calculationService).calculateTotals(orderModel, false);
    }

    @Test
    public void testReleaseAllStockForOrder() {
        setupOrderForCancel();
        cancelService.releaseAllStockForOrder(orderModel);
        Mockito.verify(targetStockService).release(productModel, 5,
                "5 stock has been released for I-Pod");
    }

    @Test
    public void testReleaseAllStockForOrderZeroQty() {
        setupOrderForCancel();
        BDDMockito.given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(0));
        cancelService.releaseAllStockForOrder(orderModel);
        Mockito.verifyZeroInteractions(targetStockService);
    }


    @Test
    public void testGetOnlineWarehouseWhenThereIsAWarehouse() {
        BDDMockito.given(warehouseService.getDefWarehouse()).willReturn(Collections.singletonList(warehouseModel));
        final WarehouseModel warehouse = cancelService.getOnlineWarehouse();
        Mockito.verify(warehouseService, Mockito.atLeastOnce()).getDefWarehouse();
        Assert.assertNotNull(warehouse);
    }

    @Test
    public void testGetOnlineWarehouseWhenThereIsNoWarehouse() {
        BDDMockito.given(warehouseService.getDefWarehouse()).willReturn(null);
        final WarehouseModel warehouse = cancelService.getOnlineWarehouse();
        Mockito.verify(warehouseService, Mockito.atLeastOnce()).getDefWarehouse();
        Assert.assertNull(warehouse);
    }

    @Test
    public void testAreAllPhysicalEntriesRemovedWhenNoEntries() {
        Mockito.when(orderModel.getEntries()).thenReturn(null);
        Assert.assertTrue(cancelService.areAllPhysicalEntriesCancelled(orderModel));
    }

    @Test
    public void testAreAllPhysicalEntriesRemovedWhenDeliveryModeNull() {
        Mockito.when(orderModel.getEntries()).thenReturn(Collections.singletonList(orderEntryModel));
        Mockito.when(orderEntryModel.getDeliveryMode()).thenReturn(null);
        Assert.assertFalse(cancelService.areAllPhysicalEntriesCancelled(orderModel));
    }

    @Test
    public void testAreAllPhysicalEntriesRemovedWhenNoDigitalProducts() {
        Mockito.when(orderModel.getEntries()).thenReturn(Collections.singletonList(orderEntryModel));
        Mockito.when(deliveryMode.getIsDigital()).thenReturn(Boolean.FALSE);
        Assert.assertFalse(cancelService.areAllPhysicalEntriesCancelled(orderModel));
    }

    @Test
    public void testAreAllPhysicalEntriesRemovedWhenOnlyDigitalProducts() {
        Mockito.when(orderModel.getEntries()).thenReturn(Collections.singletonList(orderEntryModel));
        Mockito.when(orderEntryModel.getDeliveryMode()).thenReturn(deliveryMode);
        Mockito.when(deliveryMode.getIsDigital()).thenReturn(Boolean.TRUE);
        Assert.assertTrue(cancelService.areAllPhysicalEntriesCancelled(orderModel));
    }

}
