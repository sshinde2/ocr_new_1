package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VoucherCorrectionStrategyImplTest {

    @InjectMocks
    private final VoucherCorrectionStrategy voucherCorrectionStrategyImpl = new VoucherCorrectionStrategyImpl();

    @Mock
    private TargetCancelService mockTargetCancelService;

    @Mock
    private FlybuysDiscountService mockFlybuysDiscountService;

    @Mock
    private TargetVoucherService mockTargetVoucherService;

    @Mock
    private OrderModel mockOrder;

    @Mock
    private CurrencyModel mockCurrency;

    @Mock
    private FlybuysDiscountModel mockFlybuys;

    @Captor
    private ArgumentCaptor<Double> orderTotalCaptor;

    @Captor
    private ArgumentCaptor<List> globalDiscountValuesCaptor;

    @Captor
    private ArgumentCaptor<List> discountsCaptor;

    @Before
    public void setUp() {
        given(mockCurrency.getIsocode()).willReturn("AUD");
    }

    @Test
    public void testCorrectAppliedVouchersWithNoVouchers() {
        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(0));

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder, never()).setTotalPrice(any(Double.class));
        verify(mockOrder, never()).setGlobalDiscountValues(any(List.class));
        verify(mockOrder, never()).setDiscounts(any(List.class));
        verifyZeroInteractions(mockTargetCancelService);
    }

    @Test
    public void testCorrectAppliedVouchersWithVoucherEqualToOrderTotal() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(10));

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();
        globalDiscountValues.add(createDiscountValueFromVoucher(mockVoucher));

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(-10));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        verify(mockTargetCancelService).recalculateOrder(mockOrder);

        final List capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).isEmpty();

        final List capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).isEmpty();
    }

    @Test
    public void testCorrectAppliedVouchersWithVoucherGreaterThanOrderTotal() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(20));

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();

        final DiscountValue discountValueForVoucher = createDiscountValueFromVoucher(mockVoucher);
        globalDiscountValues.add(discountValueForVoucher);

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(-10));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        verify(mockTargetCancelService).recalculateOrder(mockOrder);


        final List<DiscountModel> capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).hasSize(1);
        assertThat(capturedDiscounts).containsOnly(mockVoucher);


        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).hasSize(1);

        final DiscountValue discountValue = capturedGlobalDiscountValues.get(0);
        assertThat(discountValue.getCode()).isEqualTo(discountValueForVoucher.getCode());
        assertThat(discountValue.isAbsolute()).isEqualTo(discountValueForVoucher.isAbsolute());
        assertThat(discountValue.getCurrencyIsoCode()).isEqualTo(discountValueForVoucher.getCurrencyIsoCode());
        assertThat(discountValue.getValue()).isEqualTo(10d);
    }

    @Test
    public void testCorrectAppliedVouchersWithOrderTotalGreaterThanZero() {
        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(10));

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).getTotalPrice();
        verifyNoMoreInteractions(mockOrder);
    }

    @Test
    public void testCorrectAppliedVouchersWithNoDiscounts() {
        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(0));

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).getTotalPrice();
        verify(mockOrder).getDiscounts();
        verifyNoMoreInteractions(mockOrder);
    }

    @Test
    public void testCorrectAppliedVouchersWithOrderTotalZeroAndVoucher() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(20));

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();

        final DiscountValue discountValueForVoucher = createDiscountValueFromVoucher(mockVoucher);
        globalDiscountValues.add(discountValueForVoucher);

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(0));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        verify(mockTargetCancelService).recalculateOrder(mockOrder);


        final List<DiscountModel> capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).isEmpty();

        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).isEmpty();
    }

    @Test
    public void testCorrectAppliedVouchersWithOrderTotalZeroAndVoucherAndOtherDiscount() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(20));

        final DiscountModel mockDiscount = mock(DiscountModel.class);

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);
        discounts.add(mockDiscount);

        final DiscountValue discountValueForVoucher = createDiscountValueFromVoucher(mockVoucher);
        final DiscountValue discountValueForDiscount = new DiscountValue("Non Voucher Discount", 10d, true, "AUD");

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();
        globalDiscountValues.add(discountValueForVoucher);
        globalDiscountValues.add(discountValueForDiscount);

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(0));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        verify(mockTargetCancelService).recalculateOrder(mockOrder);


        final List<DiscountModel> capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).hasSize(1);
        assertThat(capturedDiscounts).containsOnly(mockDiscount);

        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).hasSize(1);
        assertThat(capturedGlobalDiscountValues).containsOnly(discountValueForDiscount);
    }

    @Test
    public void testCorrectAppliedVouchersWithVoucherGreaterThanOrderTotalAndOtherDiscount() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(20));

        final DiscountModel mockDiscount = mock(DiscountModel.class);

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);
        discounts.add(mockDiscount);

        final DiscountValue discountValueForVoucher = createDiscountValueFromVoucher(mockVoucher);
        final DiscountValue discountValueForDiscount = new DiscountValue("Non Voucher Discount", 10d, true, "AUD");

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();
        globalDiscountValues.add(discountValueForVoucher);
        globalDiscountValues.add(discountValueForDiscount);

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(-10));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());
        verify(mockTargetCancelService).recalculateOrder(mockOrder);
        verify(mockOrder, never()).setFlybuysPointsConsumed(Integer.valueOf(2000));
        verify(mockTargetVoucherService, never()).getFlybuysDiscountForOrder(mockOrder);
        verify(mockFlybuysDiscountService, never()).getFlybuysPointsToRefund(mockFlybuys, BigDecimal.valueOf(10d));

        final List<DiscountModel> capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).hasSize(2);
        assertThat(capturedDiscounts).containsOnly(mockVoucher, mockDiscount);


        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).hasSize(2);
        assertThat(capturedGlobalDiscountValues).contains(discountValueForDiscount);

        final DiscountValue discountValue = capturedGlobalDiscountValues.get(0);
        assertThat(discountValue.getCode()).isEqualTo(discountValueForVoucher.getCode());
        assertThat(discountValue.isAbsolute()).isEqualTo(discountValueForVoucher.isAbsolute());
        assertThat(discountValue.getCurrencyIsoCode()).isEqualTo(discountValueForVoucher.getCurrencyIsoCode());
        assertThat(discountValue.getValue()).isEqualTo(10d);
    }

    @Test
    public void testCorrectAppliedVouchersWithTwoVouchersOneGreaterThanOrderTotal() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(15));

        final VoucherModel mockVoucher2 = mock(VoucherModel.class);
        given(mockVoucher2.getCode()).willReturn("V2");
        given(mockVoucher2.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher2.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher2.getValue()).willReturn(Double.valueOf(25));

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);
        discounts.add(mockVoucher2);

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();

        final DiscountValue discountValueForVoucher = createDiscountValueFromVoucher(mockVoucher);
        final DiscountValue discountValueForVoucher2 = createDiscountValueFromVoucher(mockVoucher2);
        globalDiscountValues.add(discountValueForVoucher);
        globalDiscountValues.add(discountValueForVoucher2);

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(-20));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        verify(mockTargetCancelService).recalculateOrder(mockOrder);


        final List<DiscountModel> capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).hasSize(1);
        assertThat(capturedDiscounts).containsOnly(mockVoucher2);


        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).hasSize(1);

        final DiscountValue discountValue = capturedGlobalDiscountValues.get(0);
        assertThat(discountValue.getCode()).isEqualTo(discountValueForVoucher2.getCode());
        assertThat(discountValue.isAbsolute()).isEqualTo(discountValueForVoucher2.isAbsolute());
        assertThat(discountValue.getCurrencyIsoCode()).isEqualTo(discountValueForVoucher2.getCurrencyIsoCode());
        assertThat(discountValue.getValue()).isEqualTo(20d);
    }

    @Test
    public void testCorrectAppliedVouchersWithTwoVouchersBothGreaterThanOrderTotal() {
        final VoucherModel mockVoucher = mock(VoucherModel.class);
        given(mockVoucher.getCode()).willReturn("V1");
        given(mockVoucher.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher.getValue()).willReturn(Double.valueOf(25));

        final VoucherModel mockVoucher2 = mock(VoucherModel.class);
        given(mockVoucher2.getCode()).willReturn("V2");
        given(mockVoucher2.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockVoucher2.getCurrency()).willReturn(mockCurrency);
        given(mockVoucher2.getValue()).willReturn(Double.valueOf(25));

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockVoucher);
        discounts.add(mockVoucher2);

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();

        final DiscountValue discountValueForVoucher = createDiscountValueFromVoucher(mockVoucher);
        final DiscountValue discountValueForVoucher2 = createDiscountValueFromVoucher(mockVoucher2);
        globalDiscountValues.add(discountValueForVoucher);
        globalDiscountValues.add(discountValueForVoucher2);

        given(mockOrder.getTotalPrice()).willReturn(Double.valueOf(-20));
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        verify(mockTargetCancelService).recalculateOrder(mockOrder);


        final List<DiscountModel> capturedDiscounts = discountsCaptor.getValue();
        assertThat(capturedDiscounts).hasSize(1);
        assertThat(capturedDiscounts).containsOnly(mockVoucher);


        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).hasSize(1);

        final DiscountValue discountValue = capturedGlobalDiscountValues.get(0);
        assertThat(discountValue.getCode()).isEqualTo(discountValueForVoucher.getCode());
        assertThat(discountValue.isAbsolute()).isEqualTo(discountValueForVoucher.isAbsolute());
        assertThat(discountValue.getCurrencyIsoCode()).isEqualTo(discountValueForVoucher.getCurrencyIsoCode());
        assertThat(discountValue.getValue()).isEqualTo(5d);
    }

    @Test
    public void testCorrectAppliedVouchersWithFlybuysGreaterThanOrderTotal() {
        given(mockFlybuys.getCode()).willReturn("V1");
        given(mockFlybuys.getAbsolute()).willReturn(Boolean.TRUE);
        given(mockFlybuys.getCurrency()).willReturn(mockCurrency);
        given(mockFlybuys.getValue()).willReturn(Double.valueOf(30));
        given(mockFlybuys.getPointsConsumed()).willReturn(Integer.valueOf(1000));

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockFlybuys);

        final List<DiscountValue> globalDiscountValues = new ArrayList<>();
        globalDiscountValues.add(createDiscountValueFromVoucher(mockFlybuys));

        final Double orderTotalDouble = Double.valueOf(-10d);

        given(mockOrder.getTotalPrice()).willReturn(orderTotalDouble);
        given(mockOrder.getDiscounts()).willReturn(discounts);
        given(mockOrder.getGlobalDiscountValues()).willReturn(globalDiscountValues);
        given(mockOrder.getFlybuysPointsConsumed()).willReturn(Integer.valueOf(4000));

        BigDecimal fbRefundAmnt = BigDecimal.valueOf(orderTotalDouble.doubleValue()).negate();
        fbRefundAmnt = fbRefundAmnt.setScale(2, RoundingMode.HALF_UP);
        final BigDecimal flybuysPoints = BigDecimal.valueOf(2000d);

        given(mockTargetVoucherService.getFlybuysDiscountForOrder(mockOrder)).willReturn(mockFlybuys);
        Mockito.when(mockFlybuysDiscountService.getFlybuysPointsToRefund(mockFlybuys,
                fbRefundAmnt))
                .thenReturn(flybuysPoints);

        voucherCorrectionStrategyImpl.correctAppliedVouchers(mockOrder);

        verify(mockOrder).setFlybuysPointsConsumed(Integer.valueOf(2000));
        verify(mockTargetVoucherService).getFlybuysDiscountForOrder(mockOrder);
        verify(mockFlybuysDiscountService).getFlybuysPointsToRefund(mockFlybuys, fbRefundAmnt);
        verify(mockTargetCancelService).recalculateOrder(mockOrder);
        verify(mockOrder).setDiscounts(discountsCaptor.capture());
        verify(mockOrder).setGlobalDiscountValues(globalDiscountValuesCaptor.capture());

        final List<DiscountValue> capturedGlobalDiscountValues = globalDiscountValuesCaptor.getValue();
        assertThat(capturedGlobalDiscountValues).hasSize(1);

        final DiscountValue discountValue = capturedGlobalDiscountValues.get(0);
        assertThat(discountValue.getCode()).isEqualTo(mockFlybuys.getCode());
        assertThat(discountValue.getValue()).isEqualTo(20d);
    }

    private DiscountValue createDiscountValueFromVoucher(final VoucherModel voucher) {
        return new DiscountValue(voucher.getCode(), voucher.getValue().doubleValue(), voucher.getAbsolute()
                .booleanValue(), voucher.getCurrency().getIsocode());
    }
}
