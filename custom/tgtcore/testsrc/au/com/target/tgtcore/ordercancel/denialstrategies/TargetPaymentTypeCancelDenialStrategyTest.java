package au.com.target.tgtcore.ordercancel.denialstrategies;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPaymentTypeCancelDenialStrategyTest {

    @Mock
    private OrderCancelDenialReason mockReason;

    @InjectMocks
    private final TargetPaymentTypeCancelDenialStrategy targetPaymentTypeCancelDenialStrategy = new TargetPaymentTypeCancelDenialStrategy();

    @Test
    public void testGetCancelDenialReasonWithNoPaymentInfo() throws Exception {
        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(null);

        final OrderCancelDenialReason orderCancelDenialReason = targetPaymentTypeCancelDenialStrategy
                .getCancelDenialReason(null, mockOrder, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasonWithNonPinPadPaymentInfo() throws Exception {
        final CreditCardPaymentInfoModel mockPaymentInfo = mock(CreditCardPaymentInfoModel.class);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(mockPaymentInfo);

        final OrderCancelDenialReason orderCancelDenialReason = targetPaymentTypeCancelDenialStrategy
                .getCancelDenialReason(null, mockOrder, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasonWithNonBasicsCardPaymentType() throws Exception {
        final PinPadPaymentInfoModel mockPaymentInfo = mock(PinPadPaymentInfoModel.class);
        given(mockPaymentInfo.getCardType()).willReturn(CreditCardType.VISA);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(mockPaymentInfo);

        final OrderCancelDenialReason orderCancelDenialReason = targetPaymentTypeCancelDenialStrategy
                .getCancelDenialReason(null, mockOrder, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasonWithBasicsCardPaymentType() throws Exception {
        final PinPadPaymentInfoModel mockPaymentInfo = mock(PinPadPaymentInfoModel.class);
        given(mockPaymentInfo.getCardType()).willReturn(CreditCardType.BASICSCARD);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getPaymentInfo()).willReturn(mockPaymentInfo);

        final OrderCancelDenialReason orderCancelDenialReason = targetPaymentTypeCancelDenialStrategy
                .getCancelDenialReason(null, mockOrder, null, false, false);
        assertThat(orderCancelDenialReason).isEqualTo(mockReason);
    }
}
