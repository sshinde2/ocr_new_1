/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.fest.assertions.Delta;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link PromotionResultCorrectionStrategyImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FindConsumedEntryStrategyTest {

    private final FindConsumedEntryStrategyImpl strategy = new FindConsumedEntryStrategyImpl();

    @Mock
    private PromotionResultModel promotionResult;

    @Mock
    private AbstractOrderEntryModel orderEntry;


    @Before
    public void setup() {

        // Order entry has pk=1 
        Mockito.when(orderEntry.getPk()).thenReturn(PK.fromLong(1));
    }


    @Test
    public void testFindConsumedEntryGivenNullEntries() {

        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(null);
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNull();
    }

    @Test
    public void testFindConsumedEntryGivenEmptyEntries() {

        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(Collections.EMPTY_SET);
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNull();
    }

    @Test
    public void testFindConsumedEntryGivenNullOrderEntry() {

        // Given a list of entries not containing our test one
        final PromotionOrderEntryConsumedModel differentConsumedEntry = Mockito
                .mock(PromotionOrderEntryConsumedModel.class);
        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(getConsumedEntryList(differentConsumedEntry));

        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNull();
    }

    @Test
    public void testFindConsumedEntryGivenNotMatchingOrderEntry() {

        // Order entry is different 
        final AbstractOrderEntryModel differentOrderEntry = Mockito.mock(AbstractOrderEntryModel.class);
        Mockito.when(differentOrderEntry.getPk()).thenReturn(PK.fromLong(2));

        final TargetPromotionOrderEntryConsumedModel differentConsumedEntry =
                createMockConsumedEntry(differentOrderEntry, 1, 10);

        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(getConsumedEntryList(differentConsumedEntry));

        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNull();
    }

    @Test
    public void testFindConsumedEntryGivenMatchingOrderEntry() {

        // Given a list of entries containing our test one
        final TargetPromotionOrderEntryConsumedModel foundConsumedEntry =
                createMockConsumedEntry(orderEntry, 1, 10);
        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(getConsumedEntryList(foundConsumedEntry));

        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNotNull();
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isEqualTo(foundConsumedEntry);
    }

    @Test
    public void testFindConsumedEntryWithSamePriceMultipleInstances() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel1 =
                createMockConsumedEntry(orderEntry, 1, 10);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel2 =
                createMockConsumedEntry(orderEntry, 2, 10);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel3 =
                createMockConsumedEntry(orderEntry, 3, 10);


        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(
                getConsumedEntryList(mockTargetPromotionOrderEntryConsumedModel2,
                        mockTargetPromotionOrderEntryConsumedModel3, mockTargetPromotionOrderEntryConsumedModel1));

        // Expect the entry with highest instance number 
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNotNull();
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isEqualTo(
                mockTargetPromotionOrderEntryConsumedModel3);
    }

    @Test
    public void testFindConsumedEntryWithDifferentPrice() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel1 =
                createMockConsumedEntry(orderEntry, 1, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel2 =
                createMockConsumedEntry(orderEntry, 1, 25);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel3 =
                createMockConsumedEntry(orderEntry, 1, 25);


        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(
                getConsumedEntryList(mockTargetPromotionOrderEntryConsumedModel2,
                        mockTargetPromotionOrderEntryConsumedModel3, mockTargetPromotionOrderEntryConsumedModel1));

        // Expect the entry with highest price 
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNotNull();
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isEqualTo(
                mockTargetPromotionOrderEntryConsumedModel1);
    }

    @Test
    public void testFindConsumedEntryWithDifferentPricesAndInstances() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel1 =
                createMockConsumedEntry(orderEntry, 1, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel2 =
                createMockConsumedEntry(orderEntry, 1, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel3 =
                createMockConsumedEntry(orderEntry, 1, 25);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel4 =
                createMockConsumedEntry(orderEntry, 2, 60);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel5 =
                createMockConsumedEntry(orderEntry, 2, 60);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel6 =
                createMockConsumedEntry(orderEntry, 2, 30);

        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(
                getConsumedEntryList(
                        mockTargetPromotionOrderEntryConsumedModel1,
                        mockTargetPromotionOrderEntryConsumedModel2,
                        mockTargetPromotionOrderEntryConsumedModel3,
                        mockTargetPromotionOrderEntryConsumedModel4,
                        mockTargetPromotionOrderEntryConsumedModel5,
                        mockTargetPromotionOrderEntryConsumedModel6
                ));

        // Expect an entry with highest price 
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNotNull();
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry).getAdjustedUnitPrice())
                .isEqualTo(60, Delta.delta(0.1));
    }

    @Test
    public void testFindConsumedEntryWithSamePricesAndDifferentInstances() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel1 =
                createMockConsumedEntry(orderEntry, 1, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel2 =
                createMockConsumedEntry(orderEntry, 1, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel3 =
                createMockConsumedEntry(orderEntry, 1, 25);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel4 =
                createMockConsumedEntry(orderEntry, 2, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel5 =
                createMockConsumedEntry(orderEntry, 2, 50);
        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel6 =
                createMockConsumedEntry(orderEntry, 2, 30);

        Mockito.when(promotionResult.getConsumedEntries()).thenReturn(
                getConsumedEntryList(
                        mockTargetPromotionOrderEntryConsumedModel1,
                        mockTargetPromotionOrderEntryConsumedModel2,
                        mockTargetPromotionOrderEntryConsumedModel3,
                        mockTargetPromotionOrderEntryConsumedModel4,
                        mockTargetPromotionOrderEntryConsumedModel5,
                        mockTargetPromotionOrderEntryConsumedModel6
                ));

        // Expect an entry with highest price and instance
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isNotNull();
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry)).isInstanceOf(
                TargetPromotionOrderEntryConsumedModel.class);
        assertThat(strategy.findConsumedEntry(promotionResult, orderEntry).getAdjustedUnitPrice())
                .isEqualTo(50, Delta.delta(0.1));

        assertThat(
                ((TargetPromotionOrderEntryConsumedModel)strategy.findConsumedEntry(promotionResult, orderEntry))
                        .getInstance())
                .isEqualTo(2);
    }


    @Test
    public void testCreatePriceKeyNullEntry() {

        assertThat(strategy.createPriceKey(null)).isNull();
    }

    @Test
    public void testCreatePriceKeyNullPrice() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel =
                createMockConsumedEntry(orderEntry, 1, 0);
        given(mockTargetPromotionOrderEntryConsumedModel.getAdjustedUnitPrice()).willReturn(null);

        assertThat(strategy.createPriceKey(null)).isNull();
    }

    @Test
    public void testCreatePriceKeyWithPrice() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel =
                createMockConsumedEntry(orderEntry, 1, 40.51);

        assertThat(strategy.createPriceKey(mockTargetPromotionOrderEntryConsumedModel)).isNotNull();
        assertThat(strategy.createPriceKey(mockTargetPromotionOrderEntryConsumedModel))
                .isEqualTo(Integer.valueOf(4051));
    }

    @Test
    public void testCreatePriceKeyWithRoundedPrice() {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel =
                createMockConsumedEntry(orderEntry, 1, 40.516789);

        assertThat(strategy.createPriceKey(mockTargetPromotionOrderEntryConsumedModel)).isNotNull();
        assertThat(strategy.createPriceKey(mockTargetPromotionOrderEntryConsumedModel))
                .isEqualTo(Integer.valueOf(4051));
    }


    private Collection<PromotionOrderEntryConsumedModel> getConsumedEntryList(
            final PromotionOrderEntryConsumedModel... fromEntries) {

        final Collection<PromotionOrderEntryConsumedModel> mockEntries = new ArrayList<>();

        for (final PromotionOrderEntryConsumedModel fromEntry : fromEntries) {
            mockEntries.add(fromEntry);
        }

        return mockEntries;
    }

    private TargetPromotionOrderEntryConsumedModel createMockConsumedEntry(
            final AbstractOrderEntryModel mockorderEntry,
            final int instance, final double aup) {

        final TargetPromotionOrderEntryConsumedModel mockTargetPromotionOrderEntryConsumedModel = mock(TargetPromotionOrderEntryConsumedModel.class);
        given(mockTargetPromotionOrderEntryConsumedModel.getInstance()).willReturn(Integer.valueOf(instance));
        given(mockTargetPromotionOrderEntryConsumedModel.getOrderEntry()).willReturn(mockorderEntry);
        given(mockTargetPromotionOrderEntryConsumedModel.getAdjustedUnitPrice()).willReturn(Double.valueOf(aup));

        return mockTargetPromotionOrderEntryConsumedModel;
    }

}
