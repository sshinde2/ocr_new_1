/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author xiaofanlu
 *
 */
/**
 * Unit test for {@link OrderStatusReplaceOrderDenialStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderStatusReplaceOrderDenialStrategyTest {
    private final OrderStatusReplaceOrderDenialStrategy replaceOrderDenialStrategy = new OrderStatusReplaceOrderDenialStrategy();
    private OrderModel order;

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        order = Mockito.mock(OrderModel.class);
    }

    /**
     * test null order
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNullOrder() {
        replaceOrderDenialStrategy.isDenied(null);
    }


    @Test
    public void testIsDeniedForNotCompletedStatus() {
        final OrderStatus status = OrderStatus.CREATED;
        when(order.getStatus()).thenReturn(status);
        final boolean denied = replaceOrderDenialStrategy.isDenied(order);
        Assert.assertTrue(denied);
    }

    /**
     * 
     */
    @Test
    public void testIsDeniedForCompletedStatus() {
        final OrderStatus status = OrderStatus.COMPLETED;
        when(order.getStatus()).thenReturn(status);
        final boolean denied = replaceOrderDenialStrategy.isDenied(order);
        Assert.assertFalse(denied);
    }
}
