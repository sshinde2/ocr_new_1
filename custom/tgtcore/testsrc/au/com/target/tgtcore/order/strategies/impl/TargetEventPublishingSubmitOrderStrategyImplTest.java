/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.event.TargetSubmitOrderEvent;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetEventPublishingSubmitOrderStrategyImplTest {

    @Mock
    private EventService eventService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final TargetEventPublishingSubmitOrderStrategyImpl targetEventPublishingSubmitOrderStrategy = new TargetEventPublishingSubmitOrderStrategyImpl();

    @Mock
    private OrderModel order;

    @Test
    public void testSubmitOrder() {
        targetEventPublishingSubmitOrderStrategy.submitOrder(order);
        final ArgumentCaptor<AbstractEvent> eventCaptor = ArgumentCaptor.forClass(AbstractEvent.class);

        verify(eventService).publishEvent(eventCaptor.capture());
        final AbstractEvent event = eventCaptor.getValue();
        assertThat(event).isInstanceOf(TargetSubmitOrderEvent.class);
        assertThat(((TargetSubmitOrderEvent)event).isFluentOrder()).isFalse();
        assertThat(((TargetSubmitOrderEvent)event).getOrder()).isEqualTo(order);
    }

    @Test
    public void testSubmitFluentOrder() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        targetEventPublishingSubmitOrderStrategy.submitOrder(order);
        final ArgumentCaptor<AbstractEvent> eventCaptor = ArgumentCaptor.forClass(AbstractEvent.class);

        verify(eventService).publishEvent(eventCaptor.capture());
        final AbstractEvent event = eventCaptor.getValue();
        assertThat(event).isInstanceOf(TargetSubmitOrderEvent.class);
        assertThat(((TargetSubmitOrderEvent)event).isFluentOrder()).isTrue();
        assertThat(((TargetSubmitOrderEvent)event).getOrder()).isEqualTo(order);
    }
}
