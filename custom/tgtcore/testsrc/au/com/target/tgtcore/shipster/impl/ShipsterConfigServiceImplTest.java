/**
 * 
 */
package au.com.target.tgtcore.shipster.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.shipster.dao.ShipsterDao;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipsterConfigServiceImplTest {

    @InjectMocks
    private final ShipsterConfigServiceImpl shipsterConfigServiceImpl = new ShipsterConfigServiceImpl();

    @Mock
    private ShipsterDao shipsterDao;

    @Test
    public void testGetShipsterModelForDeliveryModes() {
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        final List<ShipsterConfigModel> shipsterConfigModelList = Arrays.asList(shipsterConfigModel);
        final TargetZoneDeliveryModeModel deliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(shipsterDao.getShipsterModelForDeliveryModes(Collections.singletonList(deliveryModeModel))).willReturn(
                shipsterConfigModelList);
        final ShipsterConfigModel result = shipsterConfigServiceImpl.getShipsterModelForDeliveryModes(deliveryModeModel);
        assertThat(result).isEqualTo(shipsterConfigModel);
    }

    @Test
    public void testGetShipsterModelForDeliveryModesWithNull() {
        final List<ShipsterConfigModel> shipsterConfigModelList = Collections.EMPTY_LIST;
        final TargetZoneDeliveryModeModel deliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(shipsterDao.getShipsterModelForDeliveryModes(Collections.singletonList(deliveryModeModel))).willReturn(
                shipsterConfigModelList);
        final ShipsterConfigModel result = shipsterConfigServiceImpl.getShipsterModelForDeliveryModes(deliveryModeModel);
        assertThat(result).isNull();
    }


}
