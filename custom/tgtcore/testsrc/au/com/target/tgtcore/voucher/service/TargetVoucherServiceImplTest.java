package au.com.target.tgtcore.voucher.service;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.SerialVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.dao.FlybuysRedeemConfigDao;
import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy;
import au.com.target.tgtcore.model.FlybuysDiscountModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVoucherServiceImplTest {

    private Collection<DiscountModel> appliedVouchers = null;
    private Collection<FlybuysDiscountModel> flybuysDiscounts = null;
    private Boolean redeemVoucherResult = null;
    private JaloPriceFactoryException redeemVoucherException = null;
    private JaloPriceFactoryException releaseVoucherException = null;

    @InjectMocks
    private final TargetVoucherServiceImpl voucherDiscountServiceImpl = new TargetVoucherServiceImpl() {
        @Override
        public Collection<DiscountModel> getAppliedVouchers(final AbstractOrderModel order) {
            return appliedVouchers;
        }

        @Override
        public Collection<FlybuysDiscountModel> getFlybuysDiscounts(final String code) {
            return flybuysDiscounts;
        }

        @Override
        public boolean redeemVoucher(final String voucherCode, final CartModel cart) throws JaloPriceFactoryException {
            if (redeemVoucherException != null) {
                throw redeemVoucherException;
            }
            else if (redeemVoucherResult != null) {
                return redeemVoucherResult.booleanValue();
            }
            else {
                return super.redeemVoucher(voucherCode, cart);
            }
        }

        @Override
        public void releaseVoucher(final String voucherCode, final CartModel cart) throws JaloPriceFactoryException {
            if (releaseVoucherException != null) {
                throw releaseVoucherException;
            }
        }

        @Override
        public VoucherModel getVoucher(final String voucherCode) {
            return voucherModel;
        }
    };

    @Mock
    private ModelService mockModelService;

    @Mock
    private CartModel mockCart;

    @Mock
    private CurrencyModel mockCurrency;

    @Mock
    private FlybuysDiscountModel flybuysDiscountModel;

    @Mock
    private FlybuysRedeemConfigDao flybuysRedeemConfigDao;

    @Mock
    private OrderModel orderModel;


    private DiscountModel voucher1;

    private VoucherModel voucherModel;

    @Before
    public void setUp() {
        given(mockCart.getCurrency()).willReturn(mockCurrency);
    }

    @Test
    public void testApplyFlybuysDiscountWithVouchersOnCart() {
        final String redeemCode = "awesomeRedeemCode";

        given(mockCart.getFlyBuysCode()).willReturn("6008943218616910");

        final VoucherModel mockVoucher = mock(VoucherModel.class);

        appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockVoucher);

        final boolean result = voucherDiscountServiceImpl.applyFlybuysDiscount(mockCart, redeemCode,
                BigDecimal.valueOf(10.00), Integer.valueOf(2000), "10j39s34psk9384fjmcks");

        assertThat(result).isFalse();
    }

    @Test
    public void testApplyFlybuysDiscountWithCartValueLessThanDiscountValue() {
        final BigDecimal discountValue = BigDecimal.valueOf(30.00);

        given(mockCart.getSubtotal()).willReturn(Double.valueOf(27.20));
        given(mockCart.getFlyBuysCode()).willReturn("6008943218616910");

        final boolean result = voucherDiscountServiceImpl.applyFlybuysDiscount(mockCart, "awesomeRedeemCode",
                discountValue, Integer.valueOf(6000), "10j39s34psk9384fjmcks");

        assertThat(result).isFalse();
    }

    @Test
    public void testApplyFlybuysDiscountWithExistingFlybuysDiscountForRedeemCode() throws JaloPriceFactoryException {
        final String orderNumber = "68475321";
        final String flybuysCardNumber = "";
        final String redeemCode = "awesomeRedeemCode";
        final BigDecimal voucherValue = BigDecimal.valueOf(10.00);
        final Integer pointsConsumed = Integer.valueOf(2000);
        final String securityToken = "10j39s34psk9384fjmcks";

        final String discountCode = (orderNumber + redeemCode).toUpperCase();

        given(mockCart.getCode()).willReturn(orderNumber);
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(27.20));
        given(mockCart.getFlyBuysCode()).willReturn(flybuysCardNumber);

        final FlybuysDiscountModel mockFlybuysDiscountModel = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscountModel.getCode()).willReturn(discountCode);

        flybuysDiscounts = Collections.singletonList(mockFlybuysDiscountModel);
        appliedVouchers = Collections.singletonList((DiscountModel)mockFlybuysDiscountModel);

        final boolean result = voucherDiscountServiceImpl.applyFlybuysDiscount(mockCart, redeemCode, voucherValue,
                pointsConsumed, securityToken);

        assertThat(result).isFalse();

        verify(mockModelService, never()).create(FlybuysDiscountModel.class);
    }

    @Test
    public void testApplyFlybuysDiscountWithNewFlybuysDiscount() throws JaloPriceFactoryException {
        final String orderNumber = "84352187";
        final String flybuysCardNumber = "";
        final String redeemCode = "awesomeRedeemCode";
        final BigDecimal voucherValue = BigDecimal.valueOf(10.00);
        final Integer pointsConsumed = Integer.valueOf(2000);
        final String securityToken = "10j39s34psk9384fjmcks";

        final String discountCode = (orderNumber + redeemCode).toUpperCase();

        given(mockCart.getCode()).willReturn(orderNumber);
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(27.20));
        given(mockCart.getFlyBuysCode()).willReturn(flybuysCardNumber);

        final FlybuysDiscountModel mockFlybuysDiscountModel = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscountModel.getCode()).willReturn(discountCode);
        given(mockModelService.create(FlybuysDiscountModel.class)).willReturn(mockFlybuysDiscountModel);
        redeemVoucherResult = Boolean.TRUE;

        final boolean result = voucherDiscountServiceImpl.applyFlybuysDiscount(mockCart, redeemCode, voucherValue,
                pointsConsumed, securityToken);

        assertThat(result).isTrue();

        verify(mockModelService).create(FlybuysDiscountModel.class);

        final InOrder inOrder = inOrder(mockFlybuysDiscountModel, mockModelService);

        inOrder.verify(mockFlybuysDiscountModel).setCode(discountCode);
        inOrder.verify(mockFlybuysDiscountModel).setFlybuysCardNumber(flybuysCardNumber);
        inOrder.verify(mockFlybuysDiscountModel).setRedeemCode(redeemCode);
        inOrder.verify(mockFlybuysDiscountModel).setValue(Double.valueOf(voucherValue.doubleValue()));
        inOrder.verify(mockFlybuysDiscountModel).setCurrency(mockCurrency);
        inOrder.verify(mockFlybuysDiscountModel).setPointsConsumed(pointsConsumed);
        inOrder.verify(mockFlybuysDiscountModel).setSecurityToken(securityToken);
        Mockito.verify(mockCart).setFlybuysPointsConsumed(pointsConsumed);
        Mockito.verify(mockCart).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testApplyFlybuysDiscountWithJaloPriceFactoryException() throws JaloPriceFactoryException {
        final String orderNumber = "84352187";
        final String flybuysCardNumber = "";
        final String redeemCode = "awesomeRedeemCode";
        final BigDecimal voucherValue = BigDecimal.valueOf(10.00);
        final Integer pointsConsumed = Integer.valueOf(2000);
        final String securityToken = "10j39s34psk9384fjmcks";

        final String discountCode = (orderNumber + redeemCode).toUpperCase();

        given(mockCart.getCode()).willReturn(orderNumber);
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(27.20));
        given(mockCart.getFlyBuysCode()).willReturn(flybuysCardNumber);

        final FlybuysDiscountModel mockFlybuysDiscountModel = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscountModel.getCode()).willReturn(discountCode);
        given(mockModelService.create(FlybuysDiscountModel.class)).willReturn(mockFlybuysDiscountModel);

        redeemVoucherException = new JaloPriceFactoryException("Exception!", 0);

        final boolean result = voucherDiscountServiceImpl.applyFlybuysDiscount(mockCart, redeemCode, voucherValue,
                pointsConsumed, securityToken);

        assertThat(result).isFalse();

        verify(mockModelService).create(FlybuysDiscountModel.class);

        final InOrder inOrder = inOrder(mockFlybuysDiscountModel);

        inOrder.verify(mockFlybuysDiscountModel).setCode(discountCode);
        inOrder.verify(mockFlybuysDiscountModel).setFlybuysCardNumber(flybuysCardNumber);
        inOrder.verify(mockFlybuysDiscountModel).setRedeemCode(redeemCode);
        inOrder.verify(mockFlybuysDiscountModel).setValue(Double.valueOf(voucherValue.doubleValue()));
        inOrder.verify(mockFlybuysDiscountModel).setCurrency(mockCurrency);
        inOrder.verify(mockFlybuysDiscountModel).setPointsConsumed(pointsConsumed);
        inOrder.verify(mockFlybuysDiscountModel).setSecurityToken(securityToken);

        verify(mockModelService, never()).refresh(mockCart);
        verify(mockCart, never()).setFlybuysPointsConsumed(pointsConsumed);
        Mockito.verify(mockCart, never()).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testApplyFlybuysDiscountWithNewFlybuysDiscountDidNotApply() throws JaloPriceFactoryException {
        final String orderNumber = "68475321";
        final String flybuysCardNumber = "";
        final String redeemCode = "awesomeRedeemCode";
        final BigDecimal voucherValue = BigDecimal.valueOf(10.00);
        final Integer pointsConsumed = Integer.valueOf(2000);
        final String securityToken = "10j39s34psk9384fjmcks";

        final String discountCode = (orderNumber + redeemCode).toUpperCase();

        given(mockCart.getSubtotal()).willReturn(Double.valueOf(27.20));
        given(mockCart.getFlyBuysCode()).willReturn(flybuysCardNumber);
        given(mockCart.getCode()).willReturn(orderNumber);

        final FlybuysDiscountModel mockFlybuysDiscountModel = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscountModel.getCode()).willReturn(redeemCode);
        given(mockModelService.create(FlybuysDiscountModel.class)).willReturn(mockFlybuysDiscountModel);

        redeemVoucherResult = Boolean.FALSE;

        final boolean result = voucherDiscountServiceImpl.applyFlybuysDiscount(mockCart, redeemCode, voucherValue,
                pointsConsumed, securityToken);

        assertThat(result).isFalse();

        verify(mockModelService).create(FlybuysDiscountModel.class);
        verify(mockModelService, never()).refresh(mockCart);

        final InOrder inOrder = inOrder(mockFlybuysDiscountModel);

        inOrder.verify(mockFlybuysDiscountModel).setCode(discountCode);
        inOrder.verify(mockFlybuysDiscountModel).setFlybuysCardNumber(flybuysCardNumber);
        inOrder.verify(mockFlybuysDiscountModel).setRedeemCode(redeemCode);
        inOrder.verify(mockFlybuysDiscountModel).setValue(Double.valueOf(voucherValue.doubleValue()));
        inOrder.verify(mockFlybuysDiscountModel).setCurrency(mockCurrency);
        inOrder.verify(mockFlybuysDiscountModel).setPointsConsumed(pointsConsumed);
        inOrder.verify(mockFlybuysDiscountModel).setSecurityToken(securityToken);
        verify(mockCart, never()).setFlybuysPointsConsumed(pointsConsumed);
        Mockito.verify(mockCart, never()).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testGetDiscountAllowedWithNullStrategy()
    {
        final List<FlybuysDenialStrategy> strategies = null;
        voucherDiscountServiceImpl.setDenialStrategies(strategies);
        assertThat(voucherDiscountServiceImpl.getFlybuysDiscountAllowed(mockCart)).isNull();
    }

    @Test
    public void testGetDiscountAllowedWithEmptyStrategy()
    {
        final List<FlybuysDenialStrategy> strategies = new LinkedList<>();
        voucherDiscountServiceImpl.setDenialStrategies(strategies);
        assertThat(voucherDiscountServiceImpl.getFlybuysDiscountAllowed(mockCart)).isNull();
    }

    @Test
    public void testGetDiscountAllowedWithOneStrategyThatAllows()
    {
        final List<FlybuysDenialStrategy> strategies = new LinkedList<>();
        final FlybuysDenialStrategy stategy0 = mock(FlybuysDenialStrategy.class);
        given(stategy0.isFlybuysDiscountAllowed(mockCart)).willReturn(null);
        strategies.add(stategy0);
        voucherDiscountServiceImpl.setDenialStrategies(strategies);
        assertThat(voucherDiscountServiceImpl.getFlybuysDiscountAllowed(mockCart)).isNull();
    }

    @Test
    public void testGetDiscountAllowedWithOneStrategyThatDenies()
    {
        final List<FlybuysDenialStrategy> strategies = new LinkedList<>();
        final FlybuysDiscountDenialDto deny0 = mock(FlybuysDiscountDenialDto.class);
        final FlybuysDenialStrategy stategy0 = mock(FlybuysDenialStrategy.class);
        given(stategy0.isFlybuysDiscountAllowed(mockCart)).willReturn(deny0);
        strategies.add(stategy0);
        voucherDiscountServiceImpl.setDenialStrategies(strategies);
        assertThat(voucherDiscountServiceImpl.getFlybuysDiscountAllowed(mockCart)).isNotNull();
    }

    @Test
    public void testGetDiscountAllowedWithMultipleStrategiesThatAllow()
    {
        final List<FlybuysDenialStrategy> strategies = new LinkedList<>();
        final FlybuysDenialStrategy stategy0 = mock(FlybuysDenialStrategy.class);
        given(stategy0.isFlybuysDiscountAllowed(mockCart)).willReturn(null);
        strategies.add(stategy0);
        final FlybuysDenialStrategy stategy1 = mock(FlybuysDenialStrategy.class);
        given(stategy1.isFlybuysDiscountAllowed(mockCart)).willReturn(null);
        strategies.add(stategy1);
        final FlybuysDenialStrategy stategy2 = mock(FlybuysDenialStrategy.class);
        given(stategy2.isFlybuysDiscountAllowed(mockCart)).willReturn(null);
        strategies.add(stategy2);
        voucherDiscountServiceImpl.setDenialStrategies(strategies);
        assertThat(voucherDiscountServiceImpl.getFlybuysDiscountAllowed(mockCart)).isNull();
    }

    @Test
    public void testGetDiscountAllowedWithMultipleStrategiesThatDeny()
    {
        final List<FlybuysDenialStrategy> strategies = new LinkedList<>();
        final FlybuysDiscountDenialDto deny0 = mock(FlybuysDiscountDenialDto.class);
        final FlybuysDenialStrategy stategy0 = mock(FlybuysDenialStrategy.class);
        given(stategy0.isFlybuysDiscountAllowed(mockCart)).willReturn(deny0);
        strategies.add(stategy0);
        final FlybuysDiscountDenialDto deny1 = mock(FlybuysDiscountDenialDto.class);
        final FlybuysDenialStrategy stategy1 = mock(FlybuysDenialStrategy.class);
        given(stategy1.isFlybuysDiscountAllowed(mockCart)).willReturn(deny1);
        strategies.add(stategy1);
        final FlybuysDiscountDenialDto deny2 = mock(FlybuysDiscountDenialDto.class);
        final FlybuysDenialStrategy stategy2 = mock(FlybuysDenialStrategy.class);
        given(stategy2.isFlybuysDiscountAllowed(mockCart)).willReturn(deny2);
        strategies.add(stategy2);
        voucherDiscountServiceImpl.setDenialStrategies(strategies);
        // need to ensure it returns the first
        assertThat(voucherDiscountServiceImpl.getFlybuysDiscountAllowed(mockCart)).isEqualTo(deny0);
    }

    @Test
    public void testRemoveFlybuysDiscountWithNoAppliedVouchers() {
        appliedVouchers = Collections.EMPTY_LIST;

        final boolean result = voucherDiscountServiceImpl.removeFlybuysDiscount(mockCart);
        assertThat(result).isTrue();

        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testRemoveFlybuysDiscountWithNonFlybuysDiscountVoucher() {
        final PromotionVoucherModel mockPromotionVoucher = mock(PromotionVoucherModel.class);

        final Collection<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockPromotionVoucher);
        appliedVouchers = discounts;

        final boolean result = voucherDiscountServiceImpl.removeFlybuysDiscount(mockCart);

        assertThat(result).isTrue();
    }

    @Test
    public void testRemoveFlybuysDiscountWithFlybuysDiscountSuccessfullyRemoved() throws JaloPriceFactoryException {
        final String voucherCode = "04582938REDEEMCODE";

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getCode()).willReturn(voucherCode);

        final Collection<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockFlybuysDiscount);
        appliedVouchers = discounts;

        final boolean result = voucherDiscountServiceImpl.removeFlybuysDiscount(mockCart);

        assertThat(result).isTrue();
        Mockito.verify(mockCart).setFlybuysPointsConsumed(null);
        Mockito.verify(mockCart).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testRemoveFlybuysDiscountWithFlybuysDiscountWithJaloPriceFactoryException()
            throws JaloPriceFactoryException {
        final String voucherCode = "04582938REDEEMCODE";

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getCode()).willReturn(voucherCode);

        final Collection<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockFlybuysDiscount);
        appliedVouchers = discounts;

        releaseVoucherException = new JaloPriceFactoryException("Exception!", 0);

        final boolean result = voucherDiscountServiceImpl.removeFlybuysDiscount(mockCart);

        assertThat(result).isFalse();
        verify(mockCart, never()).setFlybuysPointsConsumed(null);
    }

    public void testGetFlybuysDiscountForOrderWithNullDiscount()
    {
        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        appliedVouchers = null;
        final FlybuysDiscountModel discount = voucherDiscountServiceImpl.getFlybuysDiscountForOrder(mockOrder);
        Assert.assertEquals(null, discount);
    }

    @Test
    public void testGetFlybuysDiscountForOrderWithFlybuysDiscount()
    {

        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        appliedVouchers = new ArrayList<>();
        appliedVouchers.add(voucher1);
        appliedVouchers.add(flybuysDiscountModel);
        final FlybuysDiscountModel discount = voucherDiscountServiceImpl.getFlybuysDiscountForOrder(mockOrder);
        Assert.assertEquals(flybuysDiscountModel, discount);
    }

    @Test
    public void testGetFlybuysDiscountForOrderNotContainingFlybuysDiscount()
    {
        voucher1 = Mockito.mock(PromotionVoucherModel.class);
        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        appliedVouchers = new ArrayList<>();
        appliedVouchers.add(voucher1);
        final FlybuysDiscountModel discount = voucherDiscountServiceImpl.getFlybuysDiscountForOrder(mockOrder);
        Assert.assertEquals(null, discount);
    }

    @Test
    public void testIsVoucherEnabledOnlineWhenVoucherIsNull()
    {
        voucherModel = null;
        Assert.assertFalse(voucherDiscountServiceImpl.isVoucherEnabledOnline("NULLVOUCHER"));
    }

    @Test
    public void testIsVoucherEnabledOnlineWhenSerialVoucher()
    {
        voucherModel = Mockito.mock(SerialVoucherModel.class);
        Assert.assertTrue(voucherDiscountServiceImpl.isVoucherEnabledOnline("SERIALVOUCHER"));
    }

    @Test
    public void testIsVoucherEnabledOnlineWhenPromotionVoucherWithEnabledNull()
    {
        voucherModel = Mockito.mock(PromotionVoucherModel.class);
        Assert.assertFalse(voucherDiscountServiceImpl.isVoucherEnabledOnline("PROMOTIONVOUCHER-ENABLED-NULL"));
    }

    @Test
    public void testIsVoucherEnabledOnlineWhenPromotionVoucherWithEnabledFalse()
    {
        voucherModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(((PromotionVoucherModel)voucherModel).getEnabledOnline()).willReturn(Boolean.FALSE);
        Assert.assertFalse(voucherDiscountServiceImpl.isVoucherEnabledOnline("PROMOTIONVOUCHER-ENABLED-FALSE"));
    }

    @Test
    public void testIsVoucherEnabledOnlineWhenPromotionVoucherWithEnabledTrue()
    {
        voucherModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(((PromotionVoucherModel)voucherModel).getEnabledOnline()).willReturn(Boolean.TRUE);
        Assert.assertTrue(voucherDiscountServiceImpl.isVoucherEnabledOnline("PROMOTIONVOUCHER-ENABLED-TRUE"));
    }



}