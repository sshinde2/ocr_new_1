/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;


/**
 * @author gsing236
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PreOrderFlybuysDiscountDenialStrategyTest {

    @InjectMocks
    private final PreOrderFlybuysDiscountDenialStrategy strategy = new PreOrderFlybuysDiscountDenialStrategy();

    @Mock
    private OrderModel order;

    @Test
    public void testIsFlybuysDiscountAllowedWhenPreOrder() {

        willReturn(Boolean.TRUE).given(order).getContainsPreOrderItems();

        final FlybuysDiscountDenialDto flybuysDiscountAllowed = strategy.isFlybuysDiscountAllowed(order);

        assertThat(flybuysDiscountAllowed).isNotNull();
        assertThat(flybuysDiscountAllowed.isDenied()).isTrue();
        assertThat(flybuysDiscountAllowed.getReason()).isEqualTo("preOrder");
    }

    @Test
    public void testIsFlybuysDiscountAllowedWhenNormalOrder() {

        willReturn(Boolean.FALSE).given(order).getContainsPreOrderItems();

        final FlybuysDiscountDenialDto flybuysDiscountAllowed = strategy.isFlybuysDiscountAllowed(order);

        assertThat(flybuysDiscountAllowed).isNull();
    }
}
