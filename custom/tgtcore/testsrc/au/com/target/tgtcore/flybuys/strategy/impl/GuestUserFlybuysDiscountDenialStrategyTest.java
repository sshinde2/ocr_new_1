package au.com.target.tgtcore.flybuys.strategy.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.model.TargetCustomerModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GuestUserFlybuysDiscountDenialStrategyTest {

    @InjectMocks
    private final GuestUserFlybuysDiscountDenialStrategy stategy = new GuestUserFlybuysDiscountDenialStrategy();

    @Test
    public void testIsFlybuysDiscountAllowedNullOrder() {
        // return value is not defined and we don't care, so just make sure it does not throw an exception
        stategy.isFlybuysDiscountAllowed(null);
    }

    @Test
    public void testIsFlybuysDiscountAllowedGuestUser() {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(customer.getType()).willReturn(CustomerType.GUEST);
        final AbstractOrderModel order = mock(AbstractOrderModel.class);
        given(order.getUser()).willReturn(customer);
        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo("guest");
    }

    @Test
    public void testIsFlybuysDiscountAllowedNonGuestUser() {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(customer.getType()).willReturn(null);
        final AbstractOrderModel order = mock(AbstractOrderModel.class);
        given(order.getUser()).willReturn(customer);
        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }
}
