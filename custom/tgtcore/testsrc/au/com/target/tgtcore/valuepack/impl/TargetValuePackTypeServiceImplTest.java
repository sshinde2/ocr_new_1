package au.com.target.tgtcore.valuepack.impl;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.valuepack.TargetValuePackTypeService;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackTypeDao;

/**
 * Test suite for {@link TargetValuePackTypeServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetValuePackTypeServiceImplTest {

    private static final String SKU = "12345";

    @Mock
    private TargetValuePackTypeDao valuePackTypeDao;

    @InjectMocks
    private TargetValuePackTypeService valuePackTypeService = new TargetValuePackTypeServiceImpl();

    /**
     * Verifies that service layer object properly delegates the execution flow to DAO layer.
     */
    @Test
    public void testGetByChildSku() {
        valuePackTypeService.getByLeadSku(SKU);
        verify(valuePackTypeDao).getByLeadSku(SKU);
    }
}
