/**
 * 
 */
package au.com.target.tgtcore.manifest.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGenerateManifestCodeStrategyImplTest {

    @Mock
    private KeyGenerator keyGenerator;

    @Mock
    private Object object;

    @InjectMocks
    private final TargetGenerateManifestCodeStrategyImpl codeStrategyImpl = new TargetGenerateManifestCodeStrategyImpl();

    @Before
    public void setUp() {
        Mockito.when(keyGenerator.generate()).thenReturn(object);
    }

    @Test
    public void testGenerateManifestCode() {
        final String testCode = "000001";
        final String extectedManifestCode = "7032000001";
        Mockito.when(object.toString()).thenReturn(testCode);
        final String code = codeStrategyImpl.generateManifestCode(7032);
        Assert.assertNotNull(code);
        Assert.assertEquals(extectedManifestCode, code);
        Assert.assertTrue(code.startsWith("7032"));
        Assert.assertTrue(code.endsWith("000001"));
        Assert.assertEquals(10, code.length());
    }

    @Test
    public void testGenerateManifestCodeNull() {
        Mockito.when(keyGenerator.generate()).thenReturn(null);
        final String code = codeStrategyImpl.generateManifestCode(7032);
        Assert.assertNull(code);
    }
}
