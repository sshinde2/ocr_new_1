package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.regex.Pattern;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.PromotionVoucherModelValidateInterceptor.ErrorMessages;


@UnitTest
public class PromotionVoucherModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final PromotionVoucherModelValidateInterceptor promotionVoucherModelValidateInterceptor = new PromotionVoucherModelValidateInterceptor() {

        /* (non-Javadoc)
         * @see au.com.target.tgtcore.interceptor.PromotionVoucherModelValidateInterceptor#getVoucherValidationPattern()
         */
        @Override
        protected Pattern getVoucherValidationPattern() {
            return Pattern.compile("^[A-Z\\d-]{3,32}$"); // Override the pattern here to ensure the test tests that the pattern matches a specified pattern, not a specific pattern
        }

    };

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        promotionVoucherModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final VoucherModel mockModel = Mockito.mock(VoucherModel.class);

        promotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
    }

    @Test
    public void testOnValidateWithNullVoucherCode() throws InterceptorException {
        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(null);
        promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
    }

    @Test
    public void testOnValidateWithBlankVoucherCode() throws InterceptorException {
        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn("");
        promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
    }

    @Test
    public void testOnValidateWithNotMatchingVoucherCode() {
        final String invalidVoucher = "ABC-123-$#@";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(invalidVoucher);

        try {
            promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.VOUCHER_PATTERN_INVALID);
        }
    }

    @Test
    public void testOnValidateVoucherCodeEmptyAndOnlineEnabled() {
        final String invalidVoucher = "";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(invalidVoucher);
        BDDMockito.given(mockPromotionVoucher.getEnabledOnline()).willReturn(Boolean.TRUE);

        try {
            promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.VOUCHER_CODE_MANDATORY);
        }
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final String validVoucher = "HOMESALE-14";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(validVoucher);
        BDDMockito.given(mockPromotionVoucher.getEnabledOnline()).willReturn(Boolean.TRUE);

        promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
    }
}
