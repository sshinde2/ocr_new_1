package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link AbstractDealModelPrepareInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractDealModelPrepareInterceptorTest {

    @Spy
    private final AbstractDealModelPrepareInterceptor dealModelPrepareInterceptor = new AbstractDealModelPrepareInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private TimeService timeService;

    @Mock
    private AbstractDealModel dealModel;

    @Before
    public void setUp() {
        BDDMockito.willReturn(timeService).given(dealModelPrepareInterceptor).getTimeService();
    }

    @Test
    public void testOnPrepareForNullModel() throws InterceptorException {
        dealModelPrepareInterceptor.onPrepare(null, interceptorContext);
        Mockito.verifyNoMoreInteractions(interceptorContext);
    }

    @Test
    public void testOnPrepareForOtherModel() throws InterceptorException {
        dealModelPrepareInterceptor.onPrepare(new Object(), interceptorContext);
        Mockito.verifyNoMoreInteractions(interceptorContext);
    }

    @Test
    public void testOnPrepareForEndDateNotModified() throws InterceptorException {
        BDDMockito.given(Boolean.valueOf(interceptorContext.isModified(dealModel, AbstractDealModel.ENDDATE)))
                .willReturn(Boolean.FALSE);

        dealModelPrepareInterceptor.onPrepare(dealModel, interceptorContext);
        Mockito.verify(dealModel, Mockito.never()).getEndDate();
    }

    @Test
    public void testOnPrepareForEndDateModifiedToNull() throws InterceptorException {
        BDDMockito.given(Boolean.valueOf(interceptorContext.isModified(dealModel, AbstractDealModel.ENDDATE)))
                .willReturn(Boolean.TRUE);
        BDDMockito.given(dealModel.getEndDate()).willReturn(null);
        dealModelPrepareInterceptor.onPrepare(dealModel, interceptorContext);
        Mockito.verify(dealModel, Mockito.never()).setEndDate(Mockito.any(Date.class));
    }

    @Test
    public void testOnPrepareForEndDateModifiedToFuture() throws InterceptorException {
        BDDMockito.given(Boolean.valueOf(interceptorContext.isModified(dealModel, AbstractDealModel.ENDDATE)))
                .willReturn(Boolean.TRUE);

        final DateTime now = new DateTime();
        BDDMockito.given(timeService.getCurrentTime()).willReturn(now.toDate());

        final Date endDate = now.plusHours(1).toDate(); // one hour in the past

        BDDMockito.given(dealModel.getEndDate()).willReturn(endDate); // one hour in future
        dealModelPrepareInterceptor.onPrepare(dealModel, interceptorContext);
        Mockito.verify(dealModel, Mockito.never()).setEndDate(endDate);
    }

    @Test
    public void testOnPrepareForEndDateModifiedToPast() throws InterceptorException {
        BDDMockito.given(Boolean.valueOf(interceptorContext.isModified(dealModel, AbstractDealModel.ENDDATE)))
                .willReturn(Boolean.TRUE);

        final DateTime now = new DateTime();
        BDDMockito.given(timeService.getCurrentTime()).willReturn(now.toDate());

        final Date endDate = new DateTime().minusHours(1).toDate(); // one hour in the past

        BDDMockito.given(dealModel.getEndDate()).willReturn(endDate);
        dealModelPrepareInterceptor.onPrepare(dealModel, interceptorContext);
        Mockito.verify(dealModel).setEndDate(now.toDate());
    }
}
