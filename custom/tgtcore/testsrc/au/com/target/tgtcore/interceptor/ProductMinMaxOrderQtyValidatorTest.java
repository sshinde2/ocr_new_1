package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link ProductMinMaxOrderQtyValidator}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductMinMaxOrderQtyValidatorTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    private final ProductMinMaxOrderQtyValidator productValidator = new ProductMinMaxOrderQtyValidator();

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private ProductModel productModel;

    @Test
    public void testWithNullModel() throws InterceptorException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("null is not an instance of ProductModel");
        productValidator.onValidate(null, interceptorContext);
    }

    @Test
    public void testWithNonProductModel() throws InterceptorException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("String is not an instance of ProductModel");
        productValidator.onValidate("String", interceptorContext);
    }

    @Test
    public void testWithNullMinQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(null);
        BDDMockito.given(productModel.getMaxOrderQuantity()).willReturn(Integer.valueOf(2));
        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithNullMaxQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(2));
        BDDMockito.given(productModel.getMaxOrderQuantity()).willReturn(null);
        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithZeroMinQuantity() throws InterceptorException {
        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("MinOrderQuantity cannot be less than or equals zero.");

        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithZeroMaxQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(3));

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("MaxOrderQuantity cannot be less than or equals zero.");

        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithNegMinQuantity() throws InterceptorException {

        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(-1));

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("MinOrderQuantity cannot be less than or equals zero.");

        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithNegMaxQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(3));
        BDDMockito.given(productModel.getMaxOrderQuantity()).willReturn(Integer.valueOf(-1));

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("MaxOrderQuantity cannot be less than or equals zero.");

        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithEqualMinMaxQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(3));
        BDDMockito.given(productModel.getMaxOrderQuantity()).willReturn(Integer.valueOf(3));

        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithMinLessThanMaxQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(2));
        BDDMockito.given(productModel.getMaxOrderQuantity()).willReturn(Integer.valueOf(3));

        productValidator.onValidate(productModel, interceptorContext);
    }

    @Test
    public void testWithMinGreaterThanMaxQuantity() throws InterceptorException {
        BDDMockito.given(productModel.getMinOrderQuantity()).willReturn(Integer.valueOf(3));
        BDDMockito.given(productModel.getMaxOrderQuantity()).willReturn(Integer.valueOf(2));

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("MinOrderQuantity cannot be greater than MaxOrderQuantity");

        productValidator.onValidate(productModel, interceptorContext);
    }
}
