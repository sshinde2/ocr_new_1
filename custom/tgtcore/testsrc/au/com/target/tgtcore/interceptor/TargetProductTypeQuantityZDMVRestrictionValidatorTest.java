/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetProductTypeQuantityZDMVRestrictionModel;


/**
 * Test for {@link TargetProductTypeQuantityZDMVRestrictionValidator}.
 */
public class TargetProductTypeQuantityZDMVRestrictionValidatorTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetProductTypeQuantityZDMVRestrictionValidator productTypeQuantityZDMVRestrictionValidator;

    @Resource
    private ModelService modelService;

    @Resource
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Resource
    private CommonI18NService commonI18NService;

    private ZoneDeliveryModeModel dhlZoneDeliveryMode;
    private ZoneModel deZone;
    private CurrencyModel eurCurrency;
    private Collection<ProductTypeModel> productTypes;

    /**
     * Creates the core data, and necessary data for Restriction.
     */
    @Before
    public void setUp() throws Exception {
        createCoreData();
        importCsv("/tgtcore/test/testRestrictableDeliveryMode.impex", "windows-1252");
        dhlZoneDeliveryMode = (ZoneDeliveryModeModel)zoneDeliveryModeService.getDeliveryModeForCode("dhl");
        deZone = zoneDeliveryModeService.getZoneForCode("de");
        eurCurrency = commonI18NService.getCurrency("EUR");
        productTypes = getProductType();
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWithProductTypeNull() throws InterceptorException
    {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 6);
        final TargetProductTypeQuantityZDMVRestrictionModel restricationModel =
                modelService.create(TargetProductTypeQuantityZDMVRestrictionModel.class);
        restricationModel.setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        restricationModel.setMinQuantity(new Integer(1));
        restricationModel.setMaxQuantity(new Integer(2));
        productTypeQuantityZDMVRestrictionValidator.onValidate(restricationModel, null);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWithMinAndMaxQuantityIsNull() throws InterceptorException
    {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 6);
        final TargetProductTypeQuantityZDMVRestrictionModel restricationModel =
                modelService.create(TargetProductTypeQuantityZDMVRestrictionModel.class);
        restricationModel.setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        restricationModel.setProductType(productTypes);
        productTypeQuantityZDMVRestrictionValidator.onValidate(restricationModel, null);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWithMinQuantityValueIsGreaterThanMaxQuantityValue()
            throws InterceptorException
    {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 6);
        final TargetProductTypeQuantityZDMVRestrictionModel restricationModel =
                modelService.create(TargetProductTypeQuantityZDMVRestrictionModel.class);
        restricationModel.setMinQuantity(new Integer(5));
        restricationModel.setMaxQuantity(new Integer(1));
        restricationModel.setProductType(productTypes);
        restricationModel.setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        productTypeQuantityZDMVRestrictionValidator.onValidate(restricationModel, null);
    }

    @Test
    public void testOnValidateWithCorrectValues()
            throws InterceptorException
    {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 6);
        final TargetProductTypeQuantityZDMVRestrictionModel restricationModel =
                modelService.create(TargetProductTypeQuantityZDMVRestrictionModel.class);
        restricationModel.setMinQuantity(new Integer(1));
        restricationModel.setMaxQuantity(new Integer(2));
        restricationModel.setProductType(productTypes);
        restricationModel.setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        productTypeQuantityZDMVRestrictionValidator.onValidate(restricationModel, null);
    }

    private RestrictableTargetZoneDeliveryModeValueModel createZoneDeliveryModeValue(
            final CurrencyModel currency, final double min, final double value,
            final ZoneModel zone, final ZoneDeliveryModeModel zoneDeliveryMode, final int priority) {
        final RestrictableTargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                modelService.create(RestrictableTargetZoneDeliveryModeValueModel.class);
        zoneDeliveryModeValue.setCurrency(currency);
        zoneDeliveryModeValue.setMinimum(Double.valueOf(min));
        zoneDeliveryModeValue.setValue(Double.valueOf(value));
        zoneDeliveryModeValue.setZone(zone);
        zoneDeliveryModeValue.setDeliveryMode(zoneDeliveryMode);
        zoneDeliveryModeValue.setPriority(Integer.valueOf(priority));
        return zoneDeliveryModeValue;
    }

    private Collection<ProductTypeModel> getProductType()
    {
        final ProductTypeModel productType = modelService.create(ProductTypeModel.class);
        productType.setCode("bulky1");
        productType.setName("bulky1");
        final Collection<ProductTypeModel> cols = new ArrayList<>();
        cols.add(productType);
        return cols;
    }

}
