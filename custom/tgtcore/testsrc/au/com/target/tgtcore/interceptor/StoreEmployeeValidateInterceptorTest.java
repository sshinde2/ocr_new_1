/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.fest.util.Collections.list;
import static org.fest.util.Collections.set;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;


@UnitTest
public class StoreEmployeeValidateInterceptorTest {

    private final StoreEmployeeValidateInterceptor interceptor = new StoreEmployeeValidateInterceptor();

    @Mock
    private InterceptorContext interceptorContext;
    @Mock
    private StoreEmployeeModel storeUser;
    @Mock
    private EmployeeModel nonStoreUser;
    @Mock
    private TargetCommerceCheckoutService targetCommerceCheckoutService;
    @Mock
    private PrincipalGroupModel goodGroup;
    @Mock
    private PrincipalGroupModel goodGroupWithSpace;
    @Mock
    private PrincipalGroupModel badGroup;

    @Mock
    private PrincipalGroupModel storeemployeegroup;
    @Mock
    private PrincipalGroupModel storemanagergroup;

    @Mock
    private UserModel userModel;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        interceptor.setValidUserGroups(list("good", "good with space", "storeemployeegroup", "storemanagergroup"));
        given(goodGroup.getUid()).willReturn("good");
        given(goodGroupWithSpace.getUid()).willReturn("good with space");
        given(badGroup.getUid()).willReturn("bad");
        given(storeemployeegroup.getUid()).willReturn("storeemployeegroup");
        given(storemanagergroup.getUid()).willReturn("storemanagergroup");
    }

    @Test(expected = Exception.class)
    public void testGroupNull() throws InterceptorException {
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test(expected = Exception.class)
    public void testGroupEmpty() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(Collections.EMPTY_SET);
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test
    public void testGroupGood() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(goodGroup));
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testGroupBad() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(badGroup));
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test
    public void testGrouWithSpaces() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(goodGroupWithSpace));
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testGroupGoodAndBad() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(badGroup, goodGroup));
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testGroupMultipleGood() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(goodGroup, goodGroupWithSpace));
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testGroupMultipleBad() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(badGroup, badGroup));
        interceptor.onValidate(storeUser, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testNonStoreEmployeeInGroup() throws InterceptorException {
        given(nonStoreUser.getGroups()).willReturn(set(goodGroup));
        interceptor.onValidate(nonStoreUser, interceptorContext);
    }

    @Test
    public void testNonStoreEmployeeNotInGroup() throws InterceptorException {
        given(nonStoreUser.getGroups()).willReturn(Collections.EMPTY_SET);
        interceptor.onValidate(nonStoreUser, interceptorContext);
    }


    @Test(expected = InterceptorException.class)
    public void testStoreEmployeeAndStoreManagerNotInSameGroup() throws InterceptorException {
        given(storeUser.getGroups()).willReturn(set(storeemployeegroup, storemanagergroup));
        interceptor.onValidate(storeUser, interceptorContext);
    }


}
