/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;


@UnitTest
public class FlybuysValidateInterceptorTest {

    private final FlybuysValidateInterceptor interceptor = new FlybuysValidateInterceptor();

    @Mock
    private InterceptorContext interceptorContext;
    @Mock
    private CartModel cart;
    @Mock
    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        interceptor.setTargetCommerceCheckoutService(targetCommerceCheckoutService);
    }

    @Test
    public void testFlybuysEmpty() throws InterceptorException {
        interceptor.onValidate(cart, interceptorContext);
        Mockito.verify(targetCommerceCheckoutService, Mockito.times(0)).isFlybuysNumberValid(cart);
    }

    @Test(expected = InterceptorException.class)
    public void testInvalidFlybuys() throws InterceptorException {
        BDDMockito.given(cart.getFlyBuysCode()).willReturn("1111");
        interceptor.onValidate(cart, interceptorContext);
    }
}
