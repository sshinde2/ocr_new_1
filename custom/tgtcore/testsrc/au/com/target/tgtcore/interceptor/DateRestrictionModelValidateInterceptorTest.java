/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.ProductRestrictionModel;
import de.hybris.platform.voucher.model.RestrictionModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.DateRestrictionModelValidateInterceptor.ErrorMessages;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class DateRestrictionModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final DateRestrictionModelValidateInterceptor dateRestrictionModelValidateInterceptor = new DateRestrictionModelValidateInterceptor();


    @Test
    public void testDateRestrictionSavingOneRestriction() throws InterceptorException {
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final VoucherModel voucherModel = Mockito.mock(VoucherModel.class);
        BDDMockito.given(voucherModel.getRestrictions()).willReturn(restrictions);
        final DateRestrictionModel dateRestrictionModelBeingSaved = new DateRestrictionModel();
        dateRestrictionModelBeingSaved.setVoucher(voucherModel);
        dateRestrictionModelValidateInterceptor.onValidate(dateRestrictionModelBeingSaved, null);
    }

    @Test
    public void testDateRestrictionSavingNewDateRestrictionWithAnotherTypeOfRestriction() throws InterceptorException {
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final ProductRestrictionModel productRestrictionModel = new ProductRestrictionModel();
        restrictions.add(productRestrictionModel);
        final VoucherModel voucherModel = Mockito.mock(VoucherModel.class);
        BDDMockito.given(voucherModel.getRestrictions()).willReturn(restrictions);
        final DateRestrictionModel dateRestrictionModelBeingSaved = new DateRestrictionModel();
        dateRestrictionModelBeingSaved.setVoucher(voucherModel);
        dateRestrictionModelValidateInterceptor.onValidate(dateRestrictionModelBeingSaved, null);
    }

    @Test
    public void testDateRestrictionWhenAnotherDateRestrictionIsPresent() {
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final DateRestrictionModel duplicateDateRestriction = new DateRestrictionModel();
        restrictions.add(duplicateDateRestriction);
        final VoucherModel voucherModel = Mockito.mock(VoucherModel.class);
        BDDMockito.given(voucherModel.getRestrictions()).willReturn(restrictions);
        final DateRestrictionModel dateRestrictionModelBeingSaved = new DateRestrictionModel();
        dateRestrictionModelBeingSaved.setVoucher(voucherModel);
        try {
            dateRestrictionModelValidateInterceptor.onValidate(dateRestrictionModelBeingSaved, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_MULTIPLE_DATE_RESTRICTION_VOUCHER);
        }
    }

    @Test
    public void testDateRestrictionWhenModifyingSameDateRestriction() throws InterceptorException {
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final DateRestrictionModel dateRestrictionModelBeingSaved = new DateRestrictionModel();
        restrictions.add(dateRestrictionModelBeingSaved);
        final VoucherModel voucherModel = Mockito.mock(VoucherModel.class);
        BDDMockito.given(voucherModel.getRestrictions()).willReturn(restrictions);
        dateRestrictionModelBeingSaved.setStartDate(new Date());
        dateRestrictionModelBeingSaved.setVoucher(voucherModel);
        dateRestrictionModelValidateInterceptor.onValidate(dateRestrictionModelBeingSaved, null);
    }

}
