/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetAddressModel;


/**
 * Test for {@link TargetPhoneNumberPrepareInterceptor}
 * 
 */
@UnitTest
public class TargetPhoneNumberPrepareInterceptorTest {
    private TargetPhoneNumberPrepareInterceptor interceptor;

    @Before
    public void setUp() {
        interceptor = new TargetPhoneNumberPrepareInterceptor();
    }


    @Test
    public void testNullPhone() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone1(null);
        interceptor.onPrepare(model, null);
        Assert.assertNull(model.getPhone1());
    }

    @Test
    public void testEmptyPhone() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone1("");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("", model.getPhone1());
    }

    @Test
    public void testTranslationRules() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();

        model.setPhone1("123456789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("123456789", model.getPhone1());

        model.setPhone1("1234567abcd89");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("123456789", model.getPhone1());

        model.setPhone1("+123456789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+123456789", model.getPhone1());

        model.setPhone1("+123456abcd789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+123456789", model.getPhone1());

        model.setPhone1("023456789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+6123456789", model.getPhone1());

        model.setPhone1("023456abcd789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+6123456789", model.getPhone1());

        model.setPhone1("  +123456789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+123456789", model.getPhone1());

        model.setPhone1("12345+6789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("123456789", model.getPhone1());

        model.setPhone1("++123456789");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+123456789", model.getPhone1());

        model.setPhone1("(03) 5246 2000");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+61352462000", model.getPhone1());

        model.setPhone1("+610812345678");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+61812345678", model.getPhone1());

        model.setPhone1("+610512345678");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+61512345678", model.getPhone1());

        model.setPhone1("(+61)414141414");
        interceptor.onPrepare(model, null);
        Assert.assertEquals("+61414141414", model.getPhone1());
    }
}
