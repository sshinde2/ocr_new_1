package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.interceptor.TargetProductMerchDepartmentValidator.ErrorMessages;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Unit test for {@link TargetProductMerchDepartmentValidator}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductMerchDepartmentValidatorTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    private final TargetProductMerchDepartmentValidator merchDepartmentValidator = new TargetProductMerchDepartmentValidator();

    @Mock
    private InterceptorContext interceptorContext;

    @Test
    public void testValidateForNonTargetProductModel() throws InterceptorException {
        final ItemModel itemModel = Mockito.mock(ItemModel.class);
        merchDepartmentValidator.onValidate(itemModel, interceptorContext);

        Mockito.verifyZeroInteractions(itemModel);
    }

    @Test
    public void testValidateWithTooManyMerchDeptAssigned() throws InterceptorException {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);
        final CategoryModel categoryModel = Mockito.mock(CategoryModel.class);
        final TargetMerchDepartmentModel merchDepartment1 = Mockito.mock(TargetMerchDepartmentModel.class);
        final TargetMerchDepartmentModel merchDepartment2 = Mockito.mock(TargetMerchDepartmentModel.class);

        Mockito.when(productModel.getSupercategories()).thenReturn(
                Arrays.asList(categoryModel, merchDepartment1, merchDepartment2));

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.MERCH_DEPT_TOO_MANY);

        merchDepartmentValidator.onValidate(productModel, interceptorContext);
    }

}
