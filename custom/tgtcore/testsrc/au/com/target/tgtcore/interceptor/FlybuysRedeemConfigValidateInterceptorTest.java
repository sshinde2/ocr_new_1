/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author Rahul
 * 
 */
@UnitTest
public class FlybuysRedeemConfigValidateInterceptorTest {

    @Mock
    private FlybuysRedeemConfigModel configModel;

    private FlybuysRedeemConfigValidateInterceptor configValidateInterceptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        configValidateInterceptor = new FlybuysRedeemConfigValidateInterceptor();
    }

    @Test
    public void testValidateNullValues() throws InterceptorException {
        Mockito.when(configModel.getMinRedeemable()).thenReturn(null);
        Mockito.when(configModel.getMaxRedeemable()).thenReturn(null);
        Mockito.when(configModel.getMinCartValue()).thenReturn(null);

        configValidateInterceptor.onValidate(configModel, null);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateInvalidRedeemValues() throws InterceptorException {
        Mockito.when(configModel.getMinRedeemable()).thenReturn(Double.valueOf(20d));
        Mockito.when(configModel.getMaxRedeemable()).thenReturn(Double.valueOf(10d));

        configValidateInterceptor.onValidate(configModel, null);
        Assert.fail();
    }

    @Test(expected = InterceptorException.class)
    public void testValidateInvalidCartValues() throws InterceptorException {
        Mockito.when(configModel.getMinRedeemable()).thenReturn(Double.valueOf(10d));
        Mockito.when(configModel.getMaxRedeemable()).thenReturn(Double.valueOf(20d));
        Mockito.when(configModel.getMinCartValue()).thenReturn(Double.valueOf(5d));

        configValidateInterceptor.onValidate(configModel, null);
        Assert.fail();
    }

    @Test
    public void testValidateValidValues() throws InterceptorException {
        Mockito.when(configModel.getMinRedeemable()).thenReturn(Double.valueOf(10d));
        Mockito.when(configModel.getMaxRedeemable()).thenReturn(Double.valueOf(20d));
        Mockito.when(configModel.getMinCartValue()).thenReturn(Double.valueOf(15d));

        configValidateInterceptor.onValidate(configModel, null);
    }

}
