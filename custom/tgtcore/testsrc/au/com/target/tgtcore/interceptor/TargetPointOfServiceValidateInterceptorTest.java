/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


@UnitTest
public class TargetPointOfServiceValidateInterceptorTest {

    private final TargetPointOfServiceValidateInterceptor interceptor = new TargetPointOfServiceValidateInterceptor();

    @Mock
    private InterceptorContext interceptorContext;
    @Mock
    private TargetPointOfServiceModel model;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnValidateStoreNumberEmpty() throws InterceptorException {
        interceptor.onValidate(model, interceptorContext);
    }

    @Test
    public void testOnValidateStoreNumberTargetGeelong() throws InterceptorException {
        BDDMockito.given(model.getStoreNumber()).willReturn(Integer.valueOf(5001));
        interceptor.onValidate(model, interceptorContext);
    }

    @Test
    public void testOnValidateStoreNumberTargetCountryBacchusMarsh() throws InterceptorException {
        BDDMockito.given(model.getStoreNumber()).willReturn(Integer.valueOf(6558));
        interceptor.onValidate(model, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateStoreNumberNegative() throws InterceptorException {
        BDDMockito.given(model.getStoreNumber()).willReturn(Integer.valueOf(-1));
        interceptor.onValidate(model, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateStoreNumberTooHigh() throws InterceptorException {
        BDDMockito.given(model.getStoreNumber()).willReturn(Integer.valueOf(10000));
        interceptor.onValidate(model, interceptorContext);
    }

    @Test
    public void testOnValidateStoreNumberMax() throws InterceptorException {
        BDDMockito.given(model.getStoreNumber()).willReturn(Integer.valueOf(9999));
        interceptor.onValidate(model, interceptorContext);
    }
}
