/**
 * 
 */
package au.com.target.tgtcore.jalo;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyString;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetPostCodeGroupZDMVRestrictionModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPostCodeGroupZDMVRestrictionTest {

    public class MyTargetPostCodeGroupZDMVRestriction extends TargetPostCodeGroupZDMVRestriction {
        @Override
        protected TargetPostCodeGroupZDMVRestrictionModel getRestrictionModel() {
            final TargetPostCodeGroupZDMVRestrictionModel targetPostCodeGroupZDMVRestrictionModel = new TargetPostCodeGroupZDMVRestrictionModel();
            final PostCodeGroupModel postCodeGroupModel = new PostCodeGroupModel();
            final Collection<PostCodeGroupModel> postCodeGroups1 = new HashSet<>();

            final Collection<PostCodeModel> postCodes = new HashSet<>();
            final PostCodeModel postCodeModel = new PostCodeModel();
            postCodeModel.setPostCode("3001");
            postCodes.add(postCodeModel);
            postCodeGroupModel.setPostCodes(postCodes);
            postCodeGroups1.add(postCodeGroupModel);
            targetPostCodeGroupZDMVRestrictionModel.setPostCodeGroup(postCodeGroups1);
            return targetPostCodeGroupZDMVRestrictionModel;
        }
    }

    @Mock
    private MyTargetPostCodeGroupZDMVRestriction targetPostCodeGroupZDMVRestriction;

    @Mock
    private ModelService modelService;


    @Mock
    private AbstractOrderModel abstractOrder;

    @Mock
    private AbstractOrderModel abstractOrderModel;


    @Mock
    private TargetPostCodeService targetPostCodeService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        given(Boolean.valueOf(targetPostCodeGroupZDMVRestriction.evaluate(abstractOrder))).willCallRealMethod();
        given(targetPostCodeGroupZDMVRestriction.getRestrictionModel()).willCallRealMethod();
        given(targetPostCodeGroupZDMVRestriction.getModelService()).willReturn(modelService);
        given(targetPostCodeGroupZDMVRestriction.getTargetPostCodeService()).willReturn(targetPostCodeService);
    }

    @Test
    public void testEvaluatePostCode() {
        given(Boolean.valueOf(targetPostCodeService.doesPostCodeBelongsToGroups(anyString(),
                anyCollectionOf(PostCodeGroupModel.class)))).willReturn(Boolean.TRUE);
        given(targetPostCodeService.getPostalCodeFromCartOrSession(abstractOrder)).willReturn("3001");
        assertThat(targetPostCodeGroupZDMVRestriction.evaluate(abstractOrder)).isTrue();
    }

    @Test
    public void testEvaluatePostCodeFromCartForUnknownCode() {
        given(Boolean.valueOf(targetPostCodeService.doesPostCodeBelongsToGroups(anyString(),
                anyCollectionOf(PostCodeGroupModel.class)))).willReturn(Boolean.FALSE);
        given(targetPostCodeService.getPostalCodeFromCartOrSession(abstractOrder)).willReturn("3007");
        assertThat(targetPostCodeGroupZDMVRestriction.evaluate(abstractOrder)).isFalse();
    }

    @Test(expected = TargetNoPostCodeException.class)
    public void testEvaluatePostCodeFromCartForUnAllocatedPC() {
        given(Boolean.valueOf(targetPostCodeService.doesPostCodeBelongsToGroups(anyString(),
                anyCollectionOf(PostCodeGroupModel.class)))).willReturn(Boolean.FALSE);
        given(targetPostCodeService.getPostalCodeFromCartOrSession(abstractOrderModel)).willReturn(StringUtils.EMPTY);
        targetPostCodeGroupZDMVRestriction.evaluate(abstractOrder);
    }
}