/**
 * 
 */
package au.com.target.tgtcore.media;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * @author htan3
 *
 */
@UnitTest
public class TargetMimeServiceTest {

    private TargetMimeService mimeService = new TargetMimeService();

    @Mock
    private Set supportedMimeTypes;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mimeService = spy(mimeService);
        doReturn(supportedMimeTypes).when(mimeService).getSupportedMimeTypes();
    }

    @SuppressWarnings("boxing")
    @Test
    public void getBestMimeFromOverride() {
        when(supportedMimeTypes.contains("text/html")).thenReturn(true);
        assertEquals("text/html", mimeService.getBestMime("test.txt", null, "text/html"));
    }

}