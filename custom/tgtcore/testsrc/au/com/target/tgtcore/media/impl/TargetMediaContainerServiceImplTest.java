/**
 * 
 */
package au.com.target.tgtcore.media.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.media.TargetMediaContainerDao;
import au.com.target.tgtcore.media.TargetMediaContainerService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMediaContainerServiceImplTest {

    @Mock
    private TargetMediaContainerDao targetMediaContainerDao;

    @InjectMocks
    private final TargetMediaContainerService targetMediaContainerService = new TargetMediaContainerServiceImpl();

    @Test
    public void testGetMediaContainerForQualifier() {
        final CatalogVersionModel catalogVersion = new CatalogVersionModel();
        final String qualifier = "MC01";
        final List<MediaContainerModel> mediaContainers = new ArrayList<>();
        mediaContainers.add(new MediaContainerModel());

        Mockito.when(targetMediaContainerDao.findMediaContainersByQualifier(qualifier, catalogVersion))
                .thenReturn(mediaContainers);

        final MediaContainerModel mediaContainer = targetMediaContainerService
                .getMediaContainerForQualifier(qualifier, catalogVersion);
        Assert.assertNotNull(mediaContainer);
        Assert.assertEquals(mediaContainers.get(0), mediaContainer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMediaContainerForQualifierWithNullQualifier() {
        targetMediaContainerService.getMediaContainerForQualifier(null, new CatalogVersionModel());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMediaContainerForQualifierWithNullCatalogVersion() {
        targetMediaContainerService.getMediaContainerForQualifier("MC01", null);
    }

}
