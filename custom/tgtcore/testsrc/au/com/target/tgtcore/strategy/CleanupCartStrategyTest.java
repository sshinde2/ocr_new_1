/**
 * 
 */
package au.com.target.tgtcore.strategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.model.CronJobModel;

import org.junit.Assert;
import org.junit.Test;


/**
 * Test for {@link CleanupCartStrategy}
 * 
 */
@UnitTest
public class CleanupCartStrategyTest {

    private final CleanupCartStrategy strategy = new CleanupCartStrategy();

    private final String expectedQuery = "SELECT {PK} "
            + "FROM { " + CartModel._TYPECODE + "} WHERE {modifiedtime} < ?age";


    @Test(expected = IllegalArgumentException.class)
    public void testAgeNull() {
        strategy.setCartAge(null);
        strategy.createFetchQuery(new CronJobModel());
    }

    @Test
    public void testQueryEqualsExpected() {
        strategy.setCartAge(new Integer(30));
        Assert.assertEquals(expectedQuery, strategy.createFetchQuery(new CronJobModel()).getQuery());
    }
}
