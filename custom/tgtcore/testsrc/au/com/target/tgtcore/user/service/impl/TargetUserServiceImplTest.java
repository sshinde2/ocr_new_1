/**
 * 
 */
package au.com.target.tgtcore.user.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.user.dao.TargetUserDao;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetUserServiceImplTest {

    @Mock
    private TargetUserDao targetUserDao;

    @InjectMocks
    private final TargetUserServiceImpl service = new TargetUserServiceImpl();

    @Test
    public void testGetUserBySubscriptId() throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        service.getUserBySubscriptionId("1502");
        Mockito.verify(targetUserDao).findUserBySubscriptionId("1502");
    }

    @Test
    public void testGetUserBySubscriptIdWithNull() throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetCustomerModel customer = service.getUserBySubscriptionId(null);
        Assert.assertNull(customer);
        Mockito.verifyNoMoreInteractions(targetUserDao);
    }

}
