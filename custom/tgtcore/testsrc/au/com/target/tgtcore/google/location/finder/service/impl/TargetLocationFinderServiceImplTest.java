/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.GPS;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.google.location.finder.client.TargetLocationFinderClient;
import au.com.target.tgtcore.google.location.finder.converter.GeocodeResponseToTargetGPSConverter;
import au.com.target.tgtcore.google.location.finder.data.GeocodeAddressComponent;
import au.com.target.tgtcore.google.location.finder.data.GeocodeGeometry;
import au.com.target.tgtcore.google.location.finder.data.GeocodeLocation;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResultData;
import au.com.target.tgtcore.google.location.finder.exception.GeocodeResponseException;
import au.com.target.tgtcore.google.location.finder.service.GeocodeResponseValidator;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;


/**
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLocationFinderServiceImplTest {

    private static final String URL_SIGNATURE_FOR_LOCATION_PARAM = "https://maps/api/geocode/json?address=Carnegie&components=country:AU&client=target&signature=cryptic_string";

    private static final String URL_SIGNATURE_FOR_POSTCODE_PARAM = "https://maps/api/geocode/json?address=3245&components=country:AU&client=target&signature=cryptic_string";

    private static final String URL_SIGNATURE_FOR_ADDRESS_PARAM = "https://maps/api/geocode/json?address=12+Thompson+Road++3215+North+Geelong+VIC+AU&components=country:AU&client=target&signature=cryptic_string";

    private static final String URL_LATLNG_SIGNATURE = "https://maps/api/geocode/json?latlng=-33.4444,144.2222&client=target&signature=cryptic_string";

    private static final String STATUS_OK = "OK";

    private static final String POLITICAL = "POLITICAL";

    private static final String LOCALITY = "LOCALITY";

    private static final String CARNEGIE = "Carnegie";

    private static final Double LATITUDE = Double.valueOf(-33.4444);

    private static final Double LONGITUDE = Double.valueOf(144.2222);

    private static final String POST_CODE = "3245";

    private static final String GEOCODING_RESPONSE = "Geocoding Response Data";

    private static final String GEOCODING_RESULTS = "As Geocoding Results";

    private static final String GEOCODING_STATUS = "Geocoding Status";

    @Mock
    private AddressModel address;

    @Mock
    private TargetLocationFinderClient targetLocationFinderClientMock;

    @Mock
    private TargetGeocodeLocationFinderHelper targetGeocodeLocationFinderHelper;

    @Mock
    private GeocodeResponseToTargetGPSConverter responseConverter;

    @Mock
    private GeocodeResponseValidator validator;

    @Mock
    private TargetGPS mockGPS;

    @Mock
    private GeocodeResponse mockGeocodeResponse;

    @Mock
    private GeocodeResultData resultData;

    @Mock
    private GeocodeGeometry geometry;

    @InjectMocks
    private final TargetLocationFinderServiceImpl targetLocationFinderServiceImpl = new TargetLocationFinderServiceImpl();

    /**
     * Method will verify user delivery location by placing request with location name.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationSuburbWithLocation() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_LOCATION_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(CARNEGIE, null, null)).willReturn(url);
        //given(targetGeolocationFinderUrlBuilder.getLocationUrlSuburb(CARNEGIE))

        final GeocodeResponse geocodingResponse = createDeliveryLocationData();
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(geocodingResponse);
        final GeocodeResponse geocodingActualResponse = targetLocationFinderServiceImpl
                .searchUserDeliveryLocationSuburb(CARNEGIE, null, null);
        assertThat(geocodingActualResponse).as(GEOCODING_RESPONSE).isEqualTo(geocodingResponse);
        assertThat(geocodingActualResponse.getStatus()).as(GEOCODING_STATUS).isEqualTo(STATUS_OK);
        assertThat(geocodingActualResponse.getResults()).as(GEOCODING_RESULTS)
                .isEqualTo(geocodingResponse.getResults());
    }

    /**
     * Method will verify user delivery location by placing request with postcode.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationSuburbWithPostCode() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_POSTCODE_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(POST_CODE, null, null)).willReturn(url);
        final GeocodeResponse geocodingResponse = createDeliveryLocationData();
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(geocodingResponse);
        final GeocodeResponse geocodingActualResponse = targetLocationFinderServiceImpl
                .searchUserDeliveryLocationSuburb(POST_CODE, null, null);
        assertThat(geocodingActualResponse).as(GEOCODING_RESPONSE).isEqualTo(geocodingResponse);
        assertThat(geocodingActualResponse.getStatus()).as(GEOCODING_STATUS).isEqualTo(STATUS_OK);
        assertThat(geocodingActualResponse.getResults()).as(GEOCODING_RESULTS)
                .isEqualTo(geocodingResponse.getResults());
    }

    /**
     * Method will verify user delivery location by placing request for the location with no response.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationSuburbNoResponseForGivenLocation() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_LOCATION_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(CARNEGIE, null, null)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(null);
        final GeocodeResponse geocodingActualResponse = targetLocationFinderServiceImpl
                .searchUserDeliveryLocationSuburb(CARNEGIE, null, null);
        assertThat(geocodingActualResponse).as(GEOCODING_RESPONSE).isNull();
    }

    /**
     * Method will verify user delivery location by placing request for the postcode with no response.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationSuburbNoResponseForGivenPostcode() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_POSTCODE_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(POST_CODE, null, null)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(null);
        final GeocodeResponse geocodingActualResponse = targetLocationFinderServiceImpl
                .searchUserDeliveryLocationSuburb(POST_CODE, null, null);
        assertThat(geocodingActualResponse).as(GEOCODING_RESPONSE).isNull();
    }

    /**
     * Method will verify user delivery location by placing request with coordinates.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationSuburbWithCoordinates() throws IOException {
        final URL url = new URL(URL_LATLNG_SIGNATURE);
        given(targetGeocodeLocationFinderHelper.getUrl(null, LATITUDE, LONGITUDE)).willReturn(url);
        final GeocodeResponse geocodingResponse = createDeliveryLocationData();
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(geocodingResponse);
        final GeocodeResponse geocodingActualResponse = targetLocationFinderServiceImpl
                .searchUserDeliveryLocationSuburb(null, LATITUDE, LONGITUDE);
        assertThat(geocodingActualResponse).as(GEOCODING_RESPONSE).isEqualTo(geocodingResponse);
        assertThat(geocodingActualResponse.getStatus()).as(GEOCODING_STATUS).isEqualTo(STATUS_OK);
        assertThat(geocodingActualResponse.getResults()).as(GEOCODING_RESULTS)
                .isEqualTo(geocodingResponse.getResults());
    }

    /**
     * Method will verify user delivery location by placing request with coordinates with no response.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationSuburbNoResponse() throws IOException {
        final URL url = new URL(URL_LATLNG_SIGNATURE);
        given(targetGeocodeLocationFinderHelper.getUrl(null, LATITUDE, LONGITUDE)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(null);
        final GeocodeResponse geocodingActualResponse = targetLocationFinderServiceImpl
                .searchUserDeliveryLocationSuburb(null, LATITUDE, LONGITUDE);
        assertThat(geocodingActualResponse).as(GEOCODING_RESPONSE).isNull();
    }

    @Test
    public void testGetTargetGPS() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_ADDRESS_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(address)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(mockGeocodeResponse);
        doNothing().when(validator).validate(mockGeocodeResponse);

        given(mockGeocodeResponse.getResults()).willReturn(Arrays.asList(resultData));
        doNothing().when(validator).validate(mockGeocodeResponse);
        doReturn(mockGPS).when(responseConverter).convert(mockGeocodeResponse);
        final GPS targetGPS = targetLocationFinderServiceImpl.getTargetGPS(address);

        assertThat(targetGPS).isEqualTo(mockGPS);
        verify(targetGeocodeLocationFinderHelper).getUrl(address);
        verify(targetLocationFinderClientMock).geocodingResponse(url);
        verify(validator).validate(mockGeocodeResponse);
        verify(responseConverter).convert(mockGeocodeResponse);
    }


    @Test(expected = IOException.class)
    public void testGetTargetGPSException() throws IOException {
        given(targetGeocodeLocationFinderHelper.getUrl(address)).willThrow(new MalformedURLException());
        targetLocationFinderServiceImpl.getTargetGPS(address);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTargetGPSIllegalArgumentException() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_ADDRESS_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(address)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(mockGeocodeResponse);
        doThrow(new IllegalArgumentException()).when(validator).validate(mockGeocodeResponse);
        targetLocationFinderServiceImpl.getTargetGPS(address);
    }

    @Test(expected = GeocodeResponseException.class)
    public void testGetTargetGPSValidatorException() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_ADDRESS_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(address)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(mockGeocodeResponse);
        doThrow(new GeocodeResponseException("test")).when(validator).validate(mockGeocodeResponse);
        targetLocationFinderServiceImpl.getTargetGPS(address);
    }


    @Test
    public void testGetTargetGPSForPostCode() throws IOException {
        final URL url = new URL(URL_SIGNATURE_FOR_ADDRESS_PARAM);
        given(targetGeocodeLocationFinderHelper.getUrl(POST_CODE)).willReturn(url);
        given(targetLocationFinderClientMock.geocodingResponse(url)).willReturn(mockGeocodeResponse);
        doNothing().when(validator).validate(mockGeocodeResponse);
        doReturn(mockGPS).when(responseConverter).convert(mockGeocodeResponse);
        final GPS targetGPS = targetLocationFinderServiceImpl.getTargetGPS(POST_CODE);

        assertThat(targetGPS).isEqualTo(mockGPS);
        verify(targetGeocodeLocationFinderHelper).getUrl(POST_CODE);
        verify(targetLocationFinderClientMock).geocodingResponse(url);
        verify(validator).validate(mockGeocodeResponse);
        verify(responseConverter).convert(mockGeocodeResponse);
    }

    /**
     * Method will set geoCoding mock data.
     * 
     * @return GeocodingResponse
     */
    private GeocodeResponse createDeliveryLocationData() {
        final GeocodeResponse geocodingResponse = new GeocodeResponse();
        geocodingResponse.setStatus(STATUS_OK);
        final List<GeocodeResultData> results = new ArrayList<>();
        final GeocodeResultData geocodingResultData = new GeocodeResultData();
        final List<GeocodeAddressComponent> addressComponents = new ArrayList<>();
        final GeocodeAddressComponent geocoderAddressComponent = new GeocodeAddressComponent();
        final List<String> types = new ArrayList<>();
        types.add(POLITICAL);
        types.add(LOCALITY);
        geocoderAddressComponent.setTypes(types);
        geocoderAddressComponent.setLongName(CARNEGIE);
        geocoderAddressComponent.setShortName(CARNEGIE);
        addressComponents.add(geocoderAddressComponent);
        geocodingResultData.setAddressComponents(addressComponents);

        final GeocodeLocation location = new GeocodeLocation();
        location.setLat(-33.4444);
        location.setLng(144.2222);
        final GeocodeGeometry geometry = new GeocodeGeometry();
        geometry.setLocation(location);
        geocodingResultData.setGeometry(geometry);
        final String[] postcodeLocalities = { CARNEGIE };
        geocodingResultData.setPostcodeLocalities(postcodeLocalities);
        results.add(geocodingResultData);
        geocodingResponse.setResults(results);
        return geocodingResponse;
    }

}
