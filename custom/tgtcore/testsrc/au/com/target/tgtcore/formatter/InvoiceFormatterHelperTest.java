/**
 * 
 */
package au.com.target.tgtcore.formatter;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetDealResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.enums.DealTypeEnum;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceDealSection;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceItems;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceLineItem;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InvoiceFormatterHelperTest {

    /**
     * 
     */
    private static final String TMD_MESSAGE = "Team Member Discount of {0} Applied";

    private static final String DEAL_MESSAGE = "This item(s) qualifies for {0} - You have saved {1}";
    /**
     * 
     */
    private static final String PROMOTION_DESC = "This is a dress deal";

    /**
     * 
     */
    private static final String PROMOTION_CODE = "12345";
    /**
     * 
     */
    private static final String P12346_NAME = "Fantastic Shoes";
    /**
     * 
     */
    private static final String P12345_NAME = "Fantastic Jeans";
    /**
     * 
     */
    private static final String PRODUCT_P12346 = "P12346";
    /**
     * 
     */
    private static final String PRODUCT_P12345 = "P12345";
    private static final String PRODUCT_P12347 = "P12347";
    private static final String P12347_NAME = "Fantastic T-Shirt";
    private static final String PRODUCT_P12348 = "P12348";
    private static final String P12348_NAME = "Fantastic Belt";

    @Mock
    private AbstractOrderEntryModel entry;

    @Mock
    private TaxInvoiceLineItem taxInvoiceLineItem;

    @Mock
    private ProductModel product;

    @Mock
    private OrderModel order;

    @Mock
    private TargetValuePackTypeModel targetValuePackTypeModel;

    @Mock
    private TargetValuePackModel targetValuePackModel;

    @Mock
    private TypeService typeService;

    @Mock
    private TaxInvoiceService taxInvoiceService;

    @InjectMocks
    @Spy
    private final InvoiceFormatterHelper invoiceFormatterHelper = new InvoiceFormatterHelper();

    @Before
    public void setUp() {
        invoiceFormatterHelper.setTaxInvoiceTMDMessage(TMD_MESSAGE);
        invoiceFormatterHelper.setTaxInvoiceDealMessage(DEAL_MESSAGE);
        BDDMockito.doReturn(taxInvoiceLineItem).when(invoiceFormatterHelper).createTaxInvoiceItem(entry, null);
    }

    @Test
    public void testGetLineItemsForTaxInvoiceNormalItem() {
        BDDMockito.given(entry.getProduct()).willReturn(product);
        BDDMockito.given(product.getCode()).willReturn("test");

        BDDMockito.given(taxInvoiceService.findTargetValuePackType("test")).willReturn(null);
        BDDMockito.given(order.getEntries()).willReturn(Collections.singletonList(entry));
        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(order);
        final List<TaxInvoiceLineItem> results = taxInvoiceItems
                .getTaxInvoiceLineItems();
        Assert.assertEquals(1, results.size());
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    @Test
    public void testGetLineItemsForTaxInvoiceBundleItem() {
        final List<TargetValuePackModel> list2 = new ArrayList<>();
        list2.add(targetValuePackModel);
        BDDMockito.given(order.getEntries()).willReturn(Collections.singletonList(entry));
        BDDMockito.given(taxInvoiceService.findTargetValuePackType("test")).willReturn(null);
        BDDMockito.given(targetValuePackModel.getValuePackDealType()).willReturn(DealTypeEnum.BUYGET);
        BDDMockito.given(targetValuePackTypeModel.getValuePackListQualifier()).willReturn(list2);
        BDDMockito.given(entry.getProduct()).willReturn(product);
        BDDMockito.given(product.getCode()).willReturn("test");
        BDDMockito.given(taxInvoiceService.findTargetValuePackType("test")).willReturn(
                targetValuePackTypeModel);

        final EnumerationValueModel buyGet = Mockito.mock(EnumerationValueModel.class);
        BDDMockito.given(buyGet.getName()).willReturn("Buy Get");
        BDDMockito.given(typeService.getEnumerationValue(DealTypeEnum.BUYGET)).willReturn(buyGet);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(order);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceDealSections()).isNull();
        final List<TaxInvoiceLineItem> taxInvoiceLineItems = taxInvoiceItems.getTaxInvoiceLineItems();
        Assertions.assertThat(taxInvoiceLineItems).isNotNull().hasSize(1);
        final TaxInvoiceLineItem lineItem = taxInvoiceLineItems.get(0);
        Assertions.assertThat(lineItem.getBundleType()).isNotNull().isNotEmpty().isEqualTo("Buy Get");
        Assertions.assertThat(lineItem.getBundles()).isNotNull().isNotEmpty().hasSize(1);
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * all entries consumed by the deal
     */
    @Test
    public void testGetTaxInvoiceItemsAllEntriesConsumedByDeal() {
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $15.00";
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
        Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

        final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
        Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem1.getBundles()).isNull();
        Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(2L);
        Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$10.00");

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(15.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * all entries consumed by the deal and TMD Promotion
     */
    @Test
    public void testGetTaxInvoiceItemsAllEntriesConsumedByDealWithTMDPromotion() {
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);

        final TargetDealResultModel promoResult2 = new TargetDealResultModel();
        final TMDiscountProductPromotionModel promotionMode2 = new TMDiscountProductPromotionModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries2 = new ArrayList<>();
        promotionMode2.setCode(PROMOTION_CODE);
        promotionMode2.setDescription(TMD_MESSAGE);
        promotionMode2.setPercentageDiscount(new Double(5));
        promoResult2.setPromotion(promotionMode2);
        promoResult2.setCertainty(Float.valueOf(1.0f));
        promoResult2.setInstanceCount(Integer.valueOf(1));
        promoResult2.setCustom("0");
        promoResult2.setOrder(orderModel);
        promoResult2.setConsumedEntries(consumedEntries2);

        promoResults.add(promoResult2);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $15.00";
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$4.75");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(4.75d);
        Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");
        final String tmdMessage = "Team Member Discount of $0.25 Applied";
        Assertions.assertThat(lineItem.getPromotionMessage()).isNotNull().isNotEmpty().isEqualTo(tmdMessage);

        final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
        Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem1.getBundles()).isNull();
        Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(2L);
        Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$10.00");

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(15.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * all entries by the deal with TMD Promotion
     */
    @Test
    public void testGetTaxInvoiceItemsAllEntriesByDealWithTMDPromotion() {
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TMDiscountProductPromotionModel promotionModel = new TMDiscountProductPromotionModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(TMD_MESSAGE);
        promotionModel.setPercentageDiscount(new Double(5));
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        promoResult1.setCustom("0");
        promoResult1.setOrder(orderModel);

        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNotNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        Assertions.assertThat(dealSection.getDealMessage()).isNull();
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$9.50");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(9.5d);
        final String tmdMessage = "Team Member Discount of $0.50 Applied";
        Assertions.assertThat(lineItem.getDiscount()).isNull();
        Assertions.assertThat(lineItem.getPromotionMessage()).isNotNull().isNotEmpty().isEqualTo(tmdMessage);

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(0.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * all entries consumed by the deal
     */
    @Test
    public void testGetTaxInvoiceItemsAllEntriesConsumedByDealMultipleInstances() {
        final OrderModel orderModel = createOrderModel(2L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(2, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry4 = populatePromotionalResults(2, 1L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult1);
        consumedEntry4.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        consumedEntries.add(consumedEntry3);
        consumedEntries.add(consumedEntry4);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult1.setInstanceCount(Integer.valueOf(2));
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(2);
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $10.00";
            Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
            final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
            Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
            final TaxInvoiceLineItem lineItem = lineItems.get(0);
            Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

            final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
            Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
            Assertions.assertThat(lineItem1.getBundles()).isNull();
            Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
            Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$5.00");
        }

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(20.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * all entries consumed by the deal
     */
    @Test
    public void testGetTaxInvoiceItemsAllEntriesConsumedByDealWithEmptyVoucherDiscounts() {
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        orderModel.setGlobalDiscountValues(Collections.EMPTY_LIST);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $15.00";
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
        Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

        final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
        Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem1.getBundles()).isNull();
        Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(2L);
        Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$10.00");

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(15.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * all entries consumed by the deal
     */
    @Test
    public void testGetTaxInvoiceItemsAllEntriesConsumedByDealWith2VoucherDiscounts() {
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        orderModel.setTotalDiscounts(Double.valueOf(17.53d));

        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $15.00";
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
        Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

        final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
        Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem1.getBundles()).isNull();
        Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(2L);
        Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$10.00");

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(15.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(17.53d);
    }

    /**
     * all entries except one consumed by the deal
     */
    @Test
    public void testAllEntriesExcept1ConsumedByDeal() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        Mockito.when(tmdDiscountPromotionModel.getPercentageDiscount()).thenReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);

        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $10.00";
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
        Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

        final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
        Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem1.getBundles()).isNull();
        Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$5.00");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
        Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$5.00");

        final List<TaxInvoiceLineItem> taxInvoiceLineItems = taxInvoiceItems.getTaxInvoiceLineItems();
        Assertions.assertThat(taxInvoiceLineItems).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceLineItem lineItem2 = taxInvoiceLineItems.get(0);
        Assertions.assertThat(lineItem2.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem2.getBundles()).isNull();
        Assertions.assertThat(lineItem2.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem2.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem2.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem2.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem2.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(lineItem2.getDiscount()).isNull();

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(10.00d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * of the 2 deals...one with 1 quantity and other with 2 we have 1 and 1 in a deal and the second of the second
     * entry in TMD
     */
    @Test
    public void testExcept2ConsumedByDeal1ConsumedByTMD() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        Mockito.when(tmdDiscountPromotionModel.getPercentageDiscount()).thenReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));

        final TargetDealResultModel promoResult2 = new TargetDealResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        promoResult2.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));
        promoResult2.setPromotion(tmdDiscountPromotionModel);

        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        consumedEntries1.add(consumedEntry3);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(2);
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
            Assertions.assertThat(lineItems).isNotNull().isNotEmpty();
            if (lineItems.size() == 2) {
                final String message = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $10.00";
                Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
                final TaxInvoiceLineItem lineItem = lineItems.get(0);
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
                Assertions.assertThat(lineItem.getBundles()).isNull();
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
                Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
                Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
                Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
                Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
                Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

                final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
                Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
                Assertions.assertThat(lineItem1.getBundles()).isNull();
                Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
                Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(1L);
                Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
                Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$5.00");
                Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
                Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$5.00");
            }
            else {
                final String tmdMessage = "Team Member Discount of $5.00 Applied";
                Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().contains(tmdMessage);
                final TaxInvoiceLineItem lineItem3 = lineItems.get(0);
                Assertions.assertThat(lineItem3.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
                Assertions.assertThat(lineItem3.getBundles()).isNull();
                Assertions.assertThat(lineItem3.getProductName()).isNotEmpty().contains(P12346_NAME);
                Assertions.assertThat(lineItem3.getQuantity()).isNotNull().isEqualTo(1L);
                Assertions.assertThat(lineItem3.getUnitPrice()).isNotNull().contains("$10.00");
                Assertions.assertThat(lineItem3.getTotalPrice()).isNotNull().contains("$5.00");
                Assertions.assertThat(lineItem3.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
                Assertions.assertThat(lineItem3.getDiscount()).isNull();
            }

        }
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(10.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * Of the 3 products 2 are consumed by the deal and 1 is not
     */
    @Test
    public void testGetTaxInvoice3Products2ConsumedByDeal1NotConsumed() {

        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));

        final TargetDealResultModel promoResult2 = new TargetDealResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        promoResult2.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE + 1);
        promotionModel.setDescription(PROMOTION_DESC + 1);
        promoResult2.setPromotion(promotionModel);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promotionModel1.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);

        final String message1 = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $5.00";
        final String message2 = "This item(s) qualifies for " + PROMOTION_DESC + 1 + " - You have saved $5.00";

        final List<TaxInvoiceLineItem> normalInvoiceItems = taxInvoiceItems.getTaxInvoiceLineItems();
        Assertions.assertThat(normalInvoiceItems).isNotNull().hasSize(1);
        final TaxInvoiceLineItem normalInvoiceItem = normalInvoiceItems.get(0);
        Assertions.assertThat(normalInvoiceItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12347);
        Assertions.assertThat(normalInvoiceItem.getBundles()).isNull();
        Assertions.assertThat(normalInvoiceItem.getProductName()).isNotEmpty().contains(P12347_NAME);
        Assertions.assertThat(normalInvoiceItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(normalInvoiceItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(normalInvoiceItem.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(normalInvoiceItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(normalInvoiceItem.getDiscount()).isNull();

        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(2);
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
            Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(1);
            Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty();
            final TaxInvoiceLineItem lineItem = lineItems.get(0);
            if (dealSection.getDealMessage().equals(message1)) {
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
            }
            else {
                Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message2);
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12346_NAME);
            }
            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");
        }
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(10.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * Of the 3 products 2 are consumed by the deal and 1 is not
     */
    @Test
    public void testGetTaxInvoice3Products2ConsumedByDeal1PartiallyConsumed() {

        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));

        final TargetDealResultModel promoResult2 = new TargetDealResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        promoResult2.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));

        final TargetDealResultModel promoResult3 = new TargetDealResultModel();
        promoResult3.setCertainty(Float.valueOf(0.5f));
        promoResult3.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries2 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(2));


        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE + 1);
        promotionModel.setDescription(PROMOTION_DESC + 1);
        promoResult2.setPromotion(promotionModel);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promotionModel1.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel1);
        promoResult3.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult2);
        consumedEntry3.setPromotionResult(promoResult3);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry2);
        consumedEntries2.add(consumedEntry3);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResult3.setConsumedEntries(consumedEntries2);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        promoResults.add(promoResult3);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);

        final String message1 = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $5.00";
        final String message2 = "This item(s) qualifies for " + PROMOTION_DESC + 1 + " - You have saved $5.00";

        final List<TaxInvoiceLineItem> normalInvoiceItems = taxInvoiceItems.getTaxInvoiceLineItems();
        Assertions.assertThat(normalInvoiceItems).isNotNull().hasSize(1);
        final TaxInvoiceLineItem normalInvoiceItem = normalInvoiceItems.get(0);
        Assertions.assertThat(normalInvoiceItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12347);
        Assertions.assertThat(normalInvoiceItem.getBundles()).isNull();
        Assertions.assertThat(normalInvoiceItem.getProductName()).isNotEmpty().contains(P12347_NAME);
        Assertions.assertThat(normalInvoiceItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(normalInvoiceItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(normalInvoiceItem.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(normalInvoiceItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(normalInvoiceItem.getDiscount()).isNull();

        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(2);
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
            Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(1);
            Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty();
            final TaxInvoiceLineItem lineItem = lineItems.get(0);
            if (dealSection.getDealMessage().equals(message1)) {
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
            }
            else {
                Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message2);
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12346_NAME);
            }
            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");
        }
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(10.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * of the 4 products 2 are consumed by seperate deals and 2 by TMD
     */
    @Test
    public void testGetTaxInvoice4Products2ConsumedByDealN2ByTMD() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        Mockito.when(tmdDiscountPromotionModel.getPercentageDiscount()).thenReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 1L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));

        final TargetDealResultModel promoResult2 = new TargetDealResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        promoResult2.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));

        final TargetDealResultModel promoResult3 = new TargetDealResultModel();
        promoResult3.setCertainty(Float.valueOf(1.0f));
        promoResult3.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries2 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(2));
        consumedEntries2.add(consumedEntry3);
        promoResult3.setConsumedEntries(consumedEntries2);
        promoResult3.setPromotion(tmdDiscountPromotionModel);

        final TargetDealResultModel promoResult4 = new TargetDealResultModel();
        promoResult4.setCertainty(Float.valueOf(1.0f));
        promoResult4.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries3 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry4 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(3));
        consumedEntries3.add(consumedEntry4);
        promoResult4.setConsumedEntries(consumedEntries3);
        promoResult4.setPromotion(tmdDiscountPromotionModel);


        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE + 1);
        promotionModel.setDescription(PROMOTION_DESC + 1);
        promoResult2.setPromotion(promotionModel);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promotionModel1.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        promoResults.add(promoResult3);
        promoResults.add(promoResult4);
        orderModel.setAllPromotionResults(promoResults);

        final String message1 = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $5.00";
        final String message2 = "This item(s) qualifies for This is a dress deal1 - You have saved $5.00";

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);

        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();

        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(4);
        int tmdCount = 0;
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            if (dealSection.getDealMessage().equals(TMD_MESSAGE)) {
                tmdCount++;
                validatedTMD(dealSection);
            }
            else {
                final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
                Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(1);
                Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty();
                final TaxInvoiceLineItem lineItem3 = lineItems.get(0);
                if (dealSection.getDealMessage().equals(message1)) {
                    Assertions.assertThat(lineItem3.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
                    Assertions.assertThat(lineItem3.getProductName()).isNotEmpty().contains(P12345_NAME);
                    Assertions.assertThat(lineItem3.getDiscount()).isNotNull().isEqualTo("$5.00");
                }
                else if (dealSection.getDealMessage().equals(message2)) {
                    Assertions.assertThat(lineItem3.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
                    Assertions.assertThat(lineItem3.getProductName()).isNotEmpty().contains(P12346_NAME);
                }
                Assertions.assertThat(lineItem3.getBundles()).isNull();
                Assertions.assertThat(lineItem3.getQuantity()).isNotNull().isEqualTo(1L);
                Assertions.assertThat(lineItem3.getUnitPrice()).isNotNull().contains("$10.00");
                Assertions.assertThat(lineItem3.getTotalPrice()).isNotNull().contains("$5.00");
                Assertions.assertThat(lineItem3.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            }
        }
        Assertions.assertThat(tmdCount).isEqualTo(0);
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(10.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * of the 4 products 2 are consumed by seperate deals and 2 by TMD
     */
    @Test
    public void testBuy2Get1Free() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        Mockito.when(tmdDiscountPromotionModel.getPercentageDiscount()).thenReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 1L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setInstanceCount(Integer.valueOf(1));
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        consumedEntry1.setAdjustedUnitPrice(Double.valueOf(10.0d));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));
        consumedEntry2.setAdjustedUnitPrice(Double.valueOf(10.0d));
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(2));
        consumedEntry3.setAdjustedUnitPrice(Double.valueOf(0.0d));
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        consumedEntries.add(consumedEntry3);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promotionModel1.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult1);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final String message1 = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $10.00";

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);

        final List<TaxInvoiceLineItem> normalLineItems = taxInvoiceItems.getTaxInvoiceLineItems();
        Assertions.assertThat(normalLineItems).isNotNull().hasSize(1);

        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message1);

        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(3);
        for (final TaxInvoiceLineItem lineItem : lineItems) {
            Assertions.assertThat(lineItem.getProductCode()).isNotEmpty();
            if (lineItem.getProductCode().equals(PRODUCT_P12345)) {
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
                Assertions.assertThat(lineItem.getDiscount()).isNull();
                Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$10.00");
                Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
            }
            else if (lineItem.getProductCode().equals(PRODUCT_P12346)) {
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12346_NAME);
                Assertions.assertThat(lineItem.getDiscount()).isNull();
                Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$10.00");
                Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
            }
            else {
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12347);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12347_NAME);
                Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$10.00");
                Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$0.00");
                Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(0d);
            }

            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        }
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(10.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * @param dealSection
     *            deal Section of the invoice
     */
    public void validatedTMD(final TaxInvoiceDealSection dealSection) {
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(1);
        for (final TaxInvoiceLineItem lineItem : lineItems) {
            Assertions.assertThat(lineItem.getProductCode()).isNotEmpty();
            if (lineItem.getProductCode().equals(PRODUCT_P12347)) {
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12347_NAME);
            }
            else {
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12348);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12348_NAME);
            }
            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem.getDiscount()).isNull();
        }
    }

    /**
     * when the same deal is struck twice
     */
    @Test
    public void testGetTaxInvoiceSameDealStruckTwiceOneOutsideDeal() {
        final OrderModel orderModel = createOrderModel(2L, 2L, 1L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(2));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(1));

        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(2, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry4 = populatePromotionalResults(2, 1L, orderModel
                .getEntries().get(1));

        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        promoResult1.setPromotion(promotionModel);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult1);
        consumedEntry4.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        consumedEntries.add(consumedEntry3);
        consumedEntries.add(consumedEntry4);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        final List<TaxInvoiceLineItem> taxInvoiceLineItems = taxInvoiceItems.getTaxInvoiceLineItems();
        Assertions.assertThat(taxInvoiceLineItems).isNotNull().isNotEmpty().hasSize(1);
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(2);
        final String message1 = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $10.00";
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
            Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
            Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message1);
            final TaxInvoiceLineItem lineItem = lineItems.get(0);
            Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

            final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
            Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
            Assertions.assertThat(lineItem1.getBundles()).isNull();
            Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
            Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(1L);
            Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$5.00");
            Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
            Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$5.00");

        }
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(20.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    /**
     * test case with multiple deals
     */
    @Test
    public void testMultipleDeals() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        Mockito.when(tmdDiscountPromotionModel.getPercentageDiscount()).thenReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(2L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(0));

        final TargetDealResultModel promoResult2 = new TargetDealResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        promoResult2.setInstanceCount(Integer.valueOf(1));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(1));
        promoResult2.setConsumedEntries(consumedEntries1);

        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(PROMOTION_DESC);
        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE + 1);
        promotionModel1.setDescription(PROMOTION_DESC + 1);
        promoResult1.setPromotion(promotionModel);
        promoResult2.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry3);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        orderModel.setAllPromotionResults(promoResults);

        final String message1 = "This item(s) qualifies for " + PROMOTION_DESC + " - You have saved $10.00";
        final String message2 = "This item(s) qualifies for " + PROMOTION_DESC + 1 + " - You have saved $10.00";

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(2);
        for (final TaxInvoiceDealSection dealSection : dealSections) {
            final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
            Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(1);
            Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty();
            final TaxInvoiceLineItem lineItem = lineItems.get(0);
            if (dealSection.getDealMessage().equals(message1)) {
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
            }
            else {
                Assertions.assertThat(dealSection.getDealMessage()).isEqualTo(message2);
                Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
                Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12346_NAME);
            }
            Assertions.assertThat(lineItem.getBundles()).isNull();
            Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(2L);
            Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$10.00");
            Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
            Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$10.00");
        }
        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(20.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    @Test
    public void testGetTaxInvoiceItemsAllEntriesConsumedByDealWithHTMLTagsInDealMessage() {
        final String promotionDescription = "This message has <strong>tags</strong> in it. Click <a href=\"/d/d1234c1\" title=\"here\">here</a> for more information";
        final String cleanPromotionDescription = "This message has tags in it. Click here for more information";

        final OrderModel orderModel = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final TargetDealResultModel promoResult1 = new TargetDealResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 1L, orderModel
                .getEntries().get(0));
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 2L, orderModel
                .getEntries().get(1));
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promotionModel.setDescription(promotionDescription);
        promoResult1.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult1.setInstanceCount(Integer.valueOf(1));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        orderModel.setAllPromotionResults(promoResults);

        final TaxInvoiceItems taxInvoiceItems = invoiceFormatterHelper.getTaxInvoiceItems(orderModel);
        Assert.assertNotNull(taxInvoiceItems);
        Assertions.assertThat(taxInvoiceItems.getTaxInvoiceLineItems()).isNull();
        final List<TaxInvoiceDealSection> dealSections = taxInvoiceItems.getTaxInvoiceDealSections();
        Assertions.assertThat(dealSections).isNotNull().isNotEmpty().hasSize(1);
        final TaxInvoiceDealSection dealSection = dealSections.get(0);
        final String message = "This item(s) qualifies for " + cleanPromotionDescription + " - You have saved $15.00";
        Assertions.assertThat(dealSection.getDealMessage()).isNotNull().isNotEmpty().isEqualTo(message);
        final List<TaxInvoiceLineItem> lineItems = dealSection.getLineItems();
        Assertions.assertThat(lineItems).isNotNull().isNotEmpty().hasSize(2);
        final TaxInvoiceLineItem lineItem = lineItems.get(0);
        Assertions.assertThat(lineItem.getProductCode()).isNotEmpty().contains(PRODUCT_P12345);
        Assertions.assertThat(lineItem.getBundles()).isNull();
        Assertions.assertThat(lineItem.getProductName()).isNotEmpty().contains(P12345_NAME);
        Assertions.assertThat(lineItem.getQuantity()).isNotNull().isEqualTo(1L);
        Assertions.assertThat(lineItem.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem.getTotalPrice()).isNotNull().contains("$5.00");
        Assertions.assertThat(lineItem.getTotalPriceAsDouble()).isNotNull().isEqualTo(5d);
        Assertions.assertThat(lineItem.getDiscount()).isNotNull().isEqualTo("$5.00");

        final TaxInvoiceLineItem lineItem1 = lineItems.get(1);
        Assertions.assertThat(lineItem1.getProductCode()).isNotEmpty().contains(PRODUCT_P12346);
        Assertions.assertThat(lineItem1.getBundles()).isNull();
        Assertions.assertThat(lineItem1.getProductName()).isNotEmpty().contains(P12346_NAME);
        Assertions.assertThat(lineItem1.getQuantity()).isNotNull().isEqualTo(2L);
        Assertions.assertThat(lineItem1.getUnitPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPrice()).isNotNull().contains("$10.00");
        Assertions.assertThat(lineItem1.getTotalPriceAsDouble()).isNotNull().isEqualTo(10d);
        Assertions.assertThat(lineItem1.getDiscount()).isNotNull().isEqualTo("$10.00");

        Assertions.assertThat(taxInvoiceItems.getItemsSavings()).isEqualTo(15.0d);
        Assertions.assertThat(taxInvoiceItems.getVouchers()).isEqualTo(0d);
    }

    @Test
    public void testGetMaskedFlybuysNumber() {
        final String maskedFlybuysNumber = invoiceFormatterHelper.getMaskedFlybuysNumber("6008943218616910");
        assertThat(maskedFlybuysNumber).isEqualTo("600*********6910");
    }

    @Test
    public void testGetMaskedFlybuysNumberEmpty() {
        final String maskedFlybuysNumber = invoiceFormatterHelper.getMaskedFlybuysNumber(null);
        assertThat(maskedFlybuysNumber).isNull();
    }

    /**
     * create an orderModel
     * 
     * @param quantity1
     *            - for entry 1
     * @param quantity2
     *            - for entry 2
     * @param quantity3
     *            - for entry 3
     * @param quantity4
     *            - for entry 4
     * @return orderModel with all the relevant entries
     */
    private OrderModel createOrderModel(final long quantity1, final long quantity2, final long quantity3,
            final long quantity4) {
        final OrderModel orderModel = new OrderModel();
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final OrderEntryModel entry1 = new OrderEntryModel();
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        Mockito.when(productModel.getCode()).thenReturn(PRODUCT_P12345);
        Mockito.when(productModel.getName()).thenReturn(P12345_NAME);
        entry1.setEntryNumber(Integer.valueOf(0));
        entry1.setQuantity(Long.valueOf(quantity1));
        entry1.setBasePrice(Double.valueOf(10d));
        entry1.setTotalPrice(Double.valueOf(5d));
        entry1.setProduct(productModel);
        entries.add(entry1);
        final OrderEntryModel entry2 = new OrderEntryModel();
        final ProductModel productModel1 = Mockito.mock(ProductModel.class);
        Mockito.when(productModel1.getCode()).thenReturn(PRODUCT_P12346);
        Mockito.when(productModel1.getName()).thenReturn(P12346_NAME);
        entry2.setEntryNumber(Integer.valueOf(1));
        entry2.setQuantity(Long.valueOf(quantity2));
        entry2.setBasePrice(Double.valueOf(10d));
        entry2.setProduct(productModel1);
        entries.add(entry2);

        if (quantity3 > 0) {
            final OrderEntryModel entry3 = new OrderEntryModel();
            final ProductModel productModel2 = Mockito.mock(ProductModel.class);
            Mockito.when(productModel2.getCode()).thenReturn(PRODUCT_P12347);
            Mockito.when(productModel2.getName()).thenReturn(P12347_NAME);
            entry3.setEntryNumber(Integer.valueOf(1));
            entry3.setQuantity(Long.valueOf(quantity2));
            entry3.setBasePrice(Double.valueOf(10d));
            entry3.setProduct(productModel2);
            entries.add(entry3);
        }

        if (quantity4 > 0) {
            if (quantity3 > 0) {
                final OrderEntryModel entry4 = new OrderEntryModel();
                final ProductModel productModel3 = Mockito.mock(ProductModel.class);
                Mockito.when(productModel3.getCode()).thenReturn(PRODUCT_P12348);
                Mockito.when(productModel3.getName()).thenReturn(P12348_NAME);
                entry4.setEntryNumber(Integer.valueOf(1));
                entry4.setQuantity(Long.valueOf(quantity2));
                entry4.setBasePrice(Double.valueOf(10d));
                entry4.setProduct(productModel3);
                entries.add(entry4);
            }
        }

        orderModel.setEntries(entries);


        return orderModel;

    }


    /**
     * @param instance
     *            instance variable
     * @param quantity
     *            quantity consumed
     * @param abstractOrderEntryModel
     *            -order entry model
     * @return TargetPromotionOrderEntryConsumedModel
     */
    private TargetPromotionOrderEntryConsumedModel populatePromotionalResults(final int instance, final long quantity,
            final AbstractOrderEntryModel abstractOrderEntryModel) {
        final TargetPromotionOrderEntryConsumedModel entryConsumed = new TargetPromotionOrderEntryConsumedModel();
        entryConsumed.setAdjustedUnitPrice(Double.valueOf(5.0d));
        entryConsumed.setDealItemType(DealItemTypeEnum.REWARD);
        entryConsumed.setInstance(Integer.valueOf(instance));
        entryConsumed.setQuantity(Long.valueOf(quantity));
        entryConsumed.setOrderEntry(abstractOrderEntryModel);
        return entryConsumed;
    }

}
