package au.com.target.tgtcore.data.dynamic;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantPreviewDynamicAttributeHandlerTest {
    private final VariantPreviewDynamicAttributeHandler variantPreviewDynamicAttributeHandler = new VariantPreviewDynamicAttributeHandler();

    @Mock
    private TargetProductModel mockTargetProductModel;

    @Mock
    private TargetColourVariantProductModel mockTargetColourVariantProductModel;

    @Mock
    private TargetSizeVariantProductModel mockTargetSizeVariantProductModel;

    @Before
    public void setUp() {
        given(mockTargetColourVariantProductModel.getBaseProduct()).willReturn(mockTargetProductModel);
        given(mockTargetSizeVariantProductModel.getBaseProduct()).willReturn(mockTargetColourVariantProductModel);
    }

    @Test
    public void testGetTrueFromColourVariant() {
        given(mockTargetProductModel.getPreview()).willReturn(Boolean.TRUE);
        final Boolean result = variantPreviewDynamicAttributeHandler.get(mockTargetColourVariantProductModel);

        assertThat(result).isTrue();
    }

    @Test
    public void testGetFalseColourVariant() {
        given(mockTargetProductModel.getPreview()).willReturn(Boolean.FALSE);
        final Boolean result = variantPreviewDynamicAttributeHandler.get(mockTargetColourVariantProductModel);

        assertThat(result).isFalse();
    }

    @Test
    public void testGetNullColourVariant() {
        given(mockTargetProductModel.getPreview()).willReturn(null);
        final Boolean result = variantPreviewDynamicAttributeHandler.get(mockTargetSizeVariantProductModel);

        assertThat(result).isNull();
    }

    @Test
    public void testGetTrueFromSizeVariant() {
        given(mockTargetProductModel.getPreview()).willReturn(Boolean.TRUE);
        final Boolean result = variantPreviewDynamicAttributeHandler.get(mockTargetSizeVariantProductModel);

        assertThat(result).isTrue();
    }

    @Test
    public void testGetFalseSizeVariant() {
        given(mockTargetProductModel.getPreview()).willReturn(Boolean.FALSE);
        final Boolean result = variantPreviewDynamicAttributeHandler.get(mockTargetSizeVariantProductModel);

        assertThat(result).isFalse();
    }

    @Test
    public void testGetNullSizeVariant() {
        given(mockTargetProductModel.getPreview()).willReturn(null);
        final Boolean result = variantPreviewDynamicAttributeHandler.get(mockTargetColourVariantProductModel);

        assertThat(result).isNull();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSet() {
        variantPreviewDynamicAttributeHandler.set(mockTargetColourVariantProductModel, Boolean.TRUE);
    }
}
