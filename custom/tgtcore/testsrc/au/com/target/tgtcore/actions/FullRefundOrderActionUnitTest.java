/**
 * 
 */
package au.com.target.tgtcore.actions;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.task.RetryLaterException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.service.TargetPaymentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FullRefundOrderActionUnitTest {

    private static final BigDecimal AMOUNT_PAID_SO_FAR = new BigDecimal(100).setScale(2);

    @InjectMocks
    private final FullRefundOrderAction refundOrderAction = new FullRefundOrderAction();

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private PaymentTransactionModel paymentTransaction;


    @Before
    public void setup() {

        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order))
                .willReturn(Double.valueOf(AMOUNT_PAID_SO_FAR.doubleValue()));
        given(orderProcess.getOrder()).willReturn(order);

        paymentTransaction = new PaymentTransactionModel();
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();

        final PaymentTransactionEntryModel captureTransaction = new PaymentTransactionEntryModel();
        captureTransaction.setType(PaymentTransactionType.CAPTURE);
        captureTransaction.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        captureTransaction.setPaymentTransaction(paymentTransaction);

        paymentTransaction.setEntries(asList(captureTransaction));
        paymentTransactions.add(paymentTransaction);

        given(order.getPaymentTransactions()).willReturn(paymentTransactions);
    }

    @Test
    public void testExecuteActionZeroPaymentTransactions() throws RetryLaterException, Exception {

        given(order.getPaymentTransactions()).willReturn(ImmutableList.<PaymentTransactionModel> of());

        final Transition resultTransition = refundOrderAction
                .executeAction(orderProcess);

        assertEquals(Transition.NOK, resultTransition);
    }

    @Test
    public void testExecuteActionWithPaymentTransactionsWithFullRefundSuccess() throws RetryLaterException, Exception {

        final PaymentTransactionEntryModel refundEntry = new PaymentTransactionEntryModel();
        final List<PaymentTransactionEntryModel> refundList = new ArrayList<PaymentTransactionEntryModel>(
                Arrays.asList(refundEntry));
        given(targetPaymentService.refundFollowOn(paymentTransaction, AMOUNT_PAID_SO_FAR))
                .willReturn(refundList);
        given(targetPaymentService
                .findRefundedAmountAfterRefundFollowOn(refundList)).willReturn(AMOUNT_PAID_SO_FAR);

        final Transition resultTransition = refundOrderAction.executeAction(orderProcess);

        assertEquals(Transition.OK, resultTransition);
    }

    @Test
    public void testExecuteActionWithPaymentTransactionsWithPartialRefundSuccess() throws RetryLaterException,
            Exception {
        final BigDecimal refundedAmount = BigDecimal.TEN;
        final BigDecimal refundedAmountRemains = AMOUNT_PAID_SO_FAR.subtract(refundedAmount);
        final PaymentTransactionEntryModel refundEntry = new PaymentTransactionEntryModel();
        final List<PaymentTransactionEntryModel> refundList = new ArrayList<PaymentTransactionEntryModel>(
                Arrays.asList(refundEntry));
        given(targetPaymentService.refundFollowOn(paymentTransaction, AMOUNT_PAID_SO_FAR))
                .willReturn(refundList);
        given(targetPaymentService
                .findRefundedAmountAfterRefundFollowOn(refundList)).willReturn(refundedAmount);

        final Transition resultTransition = refundOrderAction.executeAction(orderProcess);
        verify(orderProcessParameterHelper)
                .setRefundedAmountInfo(
                        orderProcess,
                        MessageFormat
                                .format("Total amount need to be refunded={0}. System has already succeeded to refund amount={1} automatically, another amount={2} need to be refunded manually.",
                                        AMOUNT_PAID_SO_FAR, refundedAmount, refundedAmountRemains));
        verify(orderProcessParameterHelper).setRefundPaymentEntryList(orderProcess, refundList);
        assertEquals(Transition.NOK, resultTransition);
    }

    @Test
    public void testExecuteActionWithPaymentTransactionsWithFullRefundFailure() throws RetryLaterException,
            Exception {
        final BigDecimal refundedAmount = BigDecimal.ZERO;
        final PaymentTransactionEntryModel refundEntry = new PaymentTransactionEntryModel();
        final List<PaymentTransactionEntryModel> refundList = new ArrayList<PaymentTransactionEntryModel>(
                Arrays.asList(refundEntry));
        given(targetPaymentService.refundFollowOn(paymentTransaction, AMOUNT_PAID_SO_FAR))
                .willReturn(refundList);
        given(targetPaymentService
                .findRefundedAmountAfterRefundFollowOn(refundList)).willReturn(refundedAmount);

        final Transition resultTransition = refundOrderAction.executeAction(orderProcess);
        verify(orderProcessParameterHelper)
                .setRefundedAmountInfo(
                        orderProcess,
                        MessageFormat
                                .format("System failed to refund automatically, refund amount={0} need to be refunded manually.",
                                        AMOUNT_PAID_SO_FAR));
        verify(orderProcessParameterHelper).setRefundPaymentEntryList(orderProcess, refundList);
        assertEquals(Transition.NOK, resultTransition);
    }

    @Test
    public void testExecuteActionWithPaymentTransactionsWithException() throws RetryLaterException,
            Exception {
        final PaymentTransactionEntryModel refundEntry = new PaymentTransactionEntryModel();
        refundEntry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        given(targetPaymentService.refundFollowOn(paymentTransaction, AMOUNT_PAID_SO_FAR))
                .willThrow(new PaymentException("test exception"));
        final Transition resultTransition = refundOrderAction.executeAction(orderProcess);
        assertEquals(Transition.NOK, resultTransition);
    }

    /**
     * Verifies that {@link Transition#NOK} is returned when payment gateway replies with a REJECTED response code.
     * 
     * @throws Exception
     *             if any exception occurs
     */
    @Test
    public void testExecuteActionAndGetTransactionRejected() throws Exception {
        final PaymentTransactionEntryModel refundEntry = new PaymentTransactionEntryModel();
        refundEntry.setTransactionStatus(TransactionStatus.REJECTED.toString());
        final List<PaymentTransactionEntryModel> refundList = new ArrayList<PaymentTransactionEntryModel>(
                asList(refundEntry));
        given(targetPaymentService.refundFollowOn(paymentTransaction, AMOUNT_PAID_SO_FAR))
                .willReturn(refundList);

        final Transition resultTransition = refundOrderAction.executeAction(orderProcess);

        verify(orderProcessParameterHelper).setRefundPaymentEntryList(orderProcess, refundList);
        assertEquals(Transition.NOK, resultTransition);
    }
}
