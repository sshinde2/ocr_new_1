/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;


/**
 *
 */
@UnitTest
public class SendCsAgentTicketActionTest {
    @Test
    public void testExecuteActionWithOrderProcess() throws Exception {
        final SendCsAgentTicketAction action = new SendCsAgentTicketAction();
        action.setHeadline("fred");
        action.setSubject("john");
        action.setText("dave");
        final TargetTicketBusinessService targetTicketBusinessService = Mockito.mock(TargetTicketBusinessService.class);
        action.setTargetTicketBusinessService(targetTicketBusinessService);
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        action.executeAction(process);
        Mockito.verify(process).getOrder();
        Mockito.verify(targetTicketBusinessService, Mockito.times(1)).createCsAgentTicket("fred", "john", "dave", null,
                null);
    }

    @Test
    public void testExecuteActionWithBusinessProcess() throws Exception {
        final SendCsAgentTicketAction action = new SendCsAgentTicketAction();
        action.setHeadline("fred");
        action.setSubject("john");
        action.setText("dave");
        final TargetTicketBusinessService targetTicketBusinessService = Mockito.mock(TargetTicketBusinessService.class);
        action.setTargetTicketBusinessService(targetTicketBusinessService);
        final BusinessProcessModel process = Mockito.mock(BusinessProcessModel.class);
        action.executeAction(process);

        Mockito.verify(targetTicketBusinessService, Mockito.times(1)).createCsAgentTicket("fred", "john", "dave", null,
                null);
    }

    @Test
    public void testExecuteActionWithOrder() throws Exception {
        final SendCsAgentTicketAction action = new SendCsAgentTicketAction();
        action.setHeadline("fred");
        action.setSubject("john");
        action.setText("dave");
        final TargetTicketBusinessService targetTicketBusinessService = Mockito.mock(TargetTicketBusinessService.class);
        action.setTargetTicketBusinessService(targetTicketBusinessService);
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(process.getOrder()).willReturn(order);
        BDDMockito.given(order.getCode()).willReturn("123456");
        action.executeAction(process);
        Mockito.verify(targetTicketBusinessService, Mockito.times(1))
                .createCsAgentTicket("fred", "john", "dave for order=123456", order, null);
    }

    @Test
    public void testExecuteActionWithOrderWithGroup() throws Exception {
        final SendCsAgentTicketAction action = new SendCsAgentTicketAction();
        action.setHeadline("fred");
        action.setSubject("john");
        action.setText("dave");
        action.setGroup("group");
        final TargetTicketBusinessService targetTicketBusinessService = Mockito.mock(TargetTicketBusinessService.class);
        action.setTargetTicketBusinessService(targetTicketBusinessService);
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(process.getOrder()).willReturn(order);
        BDDMockito.given(order.getCode()).willReturn("123456");
        action.executeAction(process);
        Mockito.verify(targetTicketBusinessService, Mockito.times(1))
                .createCsAgentTicket("fred", "john", "dave for order=123456", order, "group");
    }
}
