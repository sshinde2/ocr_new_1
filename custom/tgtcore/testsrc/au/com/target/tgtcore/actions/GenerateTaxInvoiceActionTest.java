package au.com.target.tgtcore.actions;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;


/**
 * Unit Test for {@link GenerateTaxInvoiceAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GenerateTaxInvoiceActionTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private TaxInvoiceService taxInvoiceService;

    @InjectMocks
    private final GenerateTaxInvoiceAction generateTaxInvoiceAction = new GenerateTaxInvoiceAction();

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Before
    public void setUp() {
        BDDMockito.given(Boolean.valueOf(salesApplicationConfigService.isSuppressTaxInvoice(SalesApplication.TRADEME)))
                .willReturn(Boolean.TRUE);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
    }

    @Test
    public void testWithNullOrder() throws RetryLaterException, Exception {

        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Order cannot be null");
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(null);

        generateTaxInvoiceAction.executeAction(orderProcessModel);
    }

    @Test(expected = BusinessProcessException.class)
    public void testExpectionWithValidOrder() throws RetryLaterException, Exception {

        BDDMockito.given(taxInvoiceService.generateTaxInvoiceForOrder(orderModel)).willThrow(
                new TaxInvoiceException("Some Expection"));

        generateTaxInvoiceAction.executeAction(orderProcessModel);

        Mockito.verify(taxInvoiceService).generateTaxInvoiceForOrder(orderModel);
    }

    @Test
    public void testWithValidOrder() throws RetryLaterException, Exception {

        generateTaxInvoiceAction.executeAction(orderProcessModel);

        Mockito.verify(taxInvoiceService).generateTaxInvoiceForOrder(orderModel);
    }


    @Test
    public void testWithTradeMeOrder() throws RetryLaterException, Exception {

        BDDMockito.given(orderModel.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        generateTaxInvoiceAction.executeAction(orderProcessModel);

        verify(taxInvoiceService, times(0)).generateTaxInvoiceForOrder(orderModel);
    }

    @Test
    public void testWithWebOrder() throws RetryLaterException, Exception {

        BDDMockito.given(orderModel.getSalesApplication()).willReturn(SalesApplication.WEB);
        generateTaxInvoiceAction.executeAction(orderProcessModel);

        verify(taxInvoiceService, times(1)).generateTaxInvoiceForOrder(orderModel);
    }
}
