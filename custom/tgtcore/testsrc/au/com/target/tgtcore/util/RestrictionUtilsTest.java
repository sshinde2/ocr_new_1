/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetProductRestrictionModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class RestrictionUtilsTest {

    @Test
    public void testStartDateEndWithMultipleDateRestrictions() {
        final Set<RestrictionModel> restrictions = new LinkedHashSet<>();
        final DateTime startDate1 = new DateTime().minusDays(10);
        final DateTime endDate1 = new DateTime().plusDays(20);
        restrictions.add(createDateRestriction(startDate1, endDate1));
        final DateTime startDate2 = new DateTime().minusDays(5);
        final DateTime endDate2 = new DateTime().plusDays(30);
        restrictions.add(createDateRestriction(startDate2, endDate2));
        restrictions.add(new TargetProductRestrictionModel());
        final RestrictionStartDateEndDateResult restrictionStartDateEndDateResult = RestrictionUtils
                .findStartAndEndDateBasedOnRestriction(restrictions);
        Assertions.assertThat(startDate1).isEqualTo(restrictionStartDateEndDateResult.getStartDate());
        Assertions.assertThat(endDate1).isEqualTo(restrictionStartDateEndDateResult.getEndDate());
    }

    @Test
    public void testStartDateEndWithSingleDateRestrictions() {
        final DateTime startDate = new DateTime().minusDays(10);
        final DateTime endDate = new DateTime().plusDays(20);
        final Set<RestrictionModel> restrictions = new HashSet<>();
        restrictions.add(createDateRestriction(startDate, endDate));
        final RestrictionStartDateEndDateResult restrictionStartDateEndDateResult = RestrictionUtils
                .findStartAndEndDateBasedOnRestriction(restrictions);
        Assertions.assertThat(startDate).isEqualTo(restrictionStartDateEndDateResult.getStartDate());
        Assertions.assertThat(endDate).isEqualTo(restrictionStartDateEndDateResult.getEndDate());

    }

    @Test
    public void testFindApplicableStartEndDateWhenNoRestrictionsPresent() {
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final RestrictionStartDateEndDateResult restrictionStartDateEndDateResult = RestrictionUtils
                .findStartAndEndDateBasedOnRestriction(restrictions);
        Assertions.assertThat(restrictionStartDateEndDateResult).isNull();
    }

    private DateRestrictionModel createDateRestriction(final DateTime startDate, final DateTime endDate) {
        final DateRestrictionModel dateRestrictionModel = new DateRestrictionModel();
        dateRestrictionModel.setStartDate(startDate.toDate());
        dateRestrictionModel.setEndDate(endDate.toDate());
        return dateRestrictionModel;
    }
}
