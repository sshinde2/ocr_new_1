/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author pthoma20
 * 
 */
public class TargetProductMockHelper {

    private final CatalogVersionModel catalogVersionModel;

    /**
     * 
     * @param catalogVersionModel
     */
    public TargetProductMockHelper(final CatalogVersionModel catalogVersionModel) {
        this.catalogVersionModel = catalogVersionModel;
    }

    /**
     * This method will take department, baseproductcode, noof size variants , no of colour variants required and return
     * a target product. The first colour product will have code - baseCode_C1. The corresponding child size variants
     * will be having basecode_c1_S1, basecode_c1_S2 etc Similarly the next color variant product will have basecode_c2
     * and so on.
     * 
     * @param variantdepartment
     * @param baseProductCode
     * @param noOfSizeVariants
     * @param noOfColourVariants
     * @param existingDepartment
     * @return tgtProductModel
     */
    public TargetProductModel createTargetProduct(final Integer variantdepartment,
            final String baseProductCode, final int noOfSizeVariants, final int noOfColourVariants,
            final Integer existingDepartment, final Boolean existingDepExpEligble, final Boolean isBulky) {

        final Collection<VariantProductModel> colourCollection = new ArrayList<>();
        for (int j = 1; j <= noOfColourVariants; j++) {

            final Collection<VariantProductModel> sizeCollection = new ArrayList<>();
            for (int i = 1; i <= noOfSizeVariants; i++) {
                final TargetSizeVariantProductModel tgtSizeVariantModel = new TargetSizeVariantProductModel();
                tgtSizeVariantModel.setDepartment(variantdepartment);
                tgtSizeVariantModel.setCode(baseProductCode + "_C" + j + "_S" + i);
                sizeCollection.add(tgtSizeVariantModel);
            }
            final TargetColourVariantProductModel tgtColorVariantModel = new TargetColourVariantProductModel();
            tgtColorVariantModel.setCode(baseProductCode + "_C" + j);

            tgtColorVariantModel.setVariants(sizeCollection);
            if (noOfSizeVariants <= 0) {
                tgtColorVariantModel.setDepartment(variantdepartment);
            }

            colourCollection.add(tgtColorVariantModel);
        }

        final TargetProductModel tgtProductModel = new TargetProductModel();
        tgtProductModel.setCode(baseProductCode);
        tgtProductModel.setVariants(colourCollection);

        if (null != existingDepartment) {
            final Collection<CategoryModel> categoryModelList = new ArrayList<>();
            categoryModelList.add(createDepartmentCategoryModel(existingDepartment.toString(), existingDepExpEligble));
            tgtProductModel.setSupercategories(categoryModelList);
        }

        tgtProductModel.setProductType(createProductTypeModel(isBulky));
        tgtProductModel.setDeliveryModes(createDeliveryModeList(null));
        tgtProductModel.setCatalogVersion(catalogVersionModel);
        return tgtProductModel;

    }

    private Set<DeliveryModeModel> createDeliveryModeList(final String deliveryMode) {

        final Set<DeliveryModeModel> delModList = new HashSet();
        if (deliveryMode != null) {
            delModList.add(createDeliveryModeModel(deliveryMode));
        }
        return delModList;
    }

    /**
     * Gets the code and creates a department category
     * 
     * @param categoryName
     * @return categoryModel
     */
    public TargetMerchDepartmentModel createDepartmentCategoryModel(final String categoryName,
            final Boolean isExpressDelivery) {
        final TargetMerchDepartmentModel categoryModel = new TargetMerchDepartmentModel();
        categoryModel.setCode(categoryName);
        categoryModel.setExpressDeliveryEligible(isExpressDelivery);
        return categoryModel;
    }

    /**
     * Gets the code and creates a department category
     * 
     * @param isBulky
     * @return categoryModel
     */
    private ProductTypeModel createProductTypeModel(final Boolean isBulky) {
        final ProductTypeModel pdtModel = new ProductTypeModel();
        pdtModel.setBulky(isBulky);
        return pdtModel;
    }

    /**
     * Gets the code and creates a delivery mode
     * 
     * @param deliveryModeCode
     * @return deliveryModeModel
     */
    public DeliveryModeModel createDeliveryModeModel(final String deliveryModeCode) {
        final DeliveryModeModel deliveryModeModel = new DeliveryModeModel();
        deliveryModeModel.setCode(deliveryModeCode);
        return deliveryModeModel;
    }

}
