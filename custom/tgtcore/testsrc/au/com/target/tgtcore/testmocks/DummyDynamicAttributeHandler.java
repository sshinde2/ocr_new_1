/**
 * 
 */
package au.com.target.tgtcore.testmocks;

import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;


/**
 * 
 */
public class DummyDynamicAttributeHandler extends AbstractDynamicAttributeHandler<Object, AbstractItemModel> {
    // Just use the defaults, which implemented is in the Abstract as throw a UnsupportedOperationException
}
