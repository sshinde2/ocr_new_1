package au.com.target.tgtcore.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.dao.BrandDao;


@UnitTest
public class TargetBrandServiceUnitTest {

    @Mock
    private BrandDao brandDao;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final TargetBrandServiceImpl brandService = new TargetBrandServiceImpl();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        final BrandModel brandModel = new BrandModel();
        BDDMockito.given(modelService.create(BrandModel.class)).willReturn(brandModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBrandForNullName() {
        brandService.getBrandForFuzzyName(null, false);
    }

    @Test
    public void testGetExistingBrandForName() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final BrandModel expected = new BrandModel();
        expected.setName("Apple");
        BDDMockito.given(brandDao.getBrandByCode(BDDMockito.anyString())).willReturn(expected);
        final BrandModel result = brandService.getBrandForFuzzyName("Apple", false);

        Assert.assertNotNull(result);
        Assert.assertEquals(expected.getName(), result.getName());
    }

    @Test
    public void testGetNonExistingBrandForNameAllowingCreate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final BrandModel expected = new BrandModel();
        expected.setName("Apple");

        BDDMockito.given(brandDao.getBrandByCode(BDDMockito.anyString())).willThrow(
                new TargetUnknownIdentifierException("Can't find brand with name"));
        BDDMockito.given(modelService.create(BrandModel.class)).willReturn(expected);
        final BrandModel result = brandService.getBrandForFuzzyName("Apple", true);

        Assert.assertNotNull(result);
        Assert.assertEquals(expected.getName(), result.getName());
    }


    @Test
    public void testGetNonExistingBrandForName() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final BrandModel expected = new BrandModel();
        expected.setName("Apple");

        BDDMockito.given(brandDao.getBrandByCode(BDDMockito.anyString())).willThrow(
                new TargetUnknownIdentifierException("Can't find brand with name"));
        final BrandModel result = brandService.getBrandForFuzzyName("Apple", false);

        Assert.assertNull(result);
    }

    @Test
    public void testGetBrandForDuplicateName() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final BrandModel expected = new BrandModel();
        expected.setName("Apple");

        BDDMockito.given(brandDao.getBrandByCode(BDDMockito.anyString())).willThrow(
                new TargetAmbiguousIdentifierException("Found more than one brand with name"));
        final BrandModel result = brandService.getBrandForFuzzyName("Apple", false);

        Assert.assertNull(result);
    }

    @Test
    public void testOnPrepareRequiresChange() throws Exception {
        final BrandModel brand = brandService.createBrand("LEGO");
        Assert.assertNotNull(brand);
        Assert.assertEquals("LEGO", brand.getName());
        Assert.assertEquals("lego", brand.getCode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOnPrepareNull() throws Exception {
        brandService.getBrandForFuzzyName(null, true);
    }

    @Test
    public void testOnPrepareWithSpace() throws Exception {
        final BrandModel brand = brandService.createBrand(" Fisher Price ");
        Assert.assertNotNull(brand);
        Assert.assertEquals(" Fisher Price ", brand.getName());
        Assert.assertEquals("fisherprice", brand.getCode());
    }

    @Test
    public void testOnPrepareFancyChars() throws Exception {
        final BrandModel brand = brandService.createBrand("'Lego�'");
        Assert.assertNotNull(brand);
        Assert.assertEquals("'Lego�'", brand.getName());
        Assert.assertEquals("lego", brand.getCode());
    }

    @Test
    public void testOnPrepareSpaceAtStartAfterFancyChar() throws Exception {
        final BrandModel brand = brandService.createBrand(" ' Disney - ");
        Assert.assertNotNull(brand);
        Assert.assertEquals(" ' Disney - ", brand.getName());
        Assert.assertEquals("disney", brand.getCode());
    }
}
