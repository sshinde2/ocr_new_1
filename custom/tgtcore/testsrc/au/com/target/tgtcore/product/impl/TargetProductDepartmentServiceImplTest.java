/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.dao.TargetProductMerchDepartmentDao;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;
import au.com.target.tgtcore.util.TargetProductMockHelper;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDepartmentServiceImplTest {
    @Mock
    private CategoryService categoryService;

    @Mock
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Mock
    private TargetProductMerchDepartmentDao targetProductMerchDepartmentDao;

    @InjectMocks
    private final TargetProductDepartmentServiceImpl targetProductDepartmentService = new TargetProductDepartmentServiceImpl();

    private final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

    private TargetProductMockHelper targetProductMockHelper;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        targetProductMockHelper = new TargetProductMockHelper(catalogVersionModel);
        when(zoneDeliveryModeService.getDeliveryModeForCode("express-delivery")).thenReturn(
                targetProductMockHelper.createDeliveryModeModel("express-delivery"));

    }

    /**
     * Test case where a department is retrieved correctly.
     */
    @Test
    public void getDepartmentCategoryModelPositiveTest() {
        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetMerchDepartmentModel targetMerchDepModelExpected = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                targetMerchDepModelExpected);

        final TargetMerchDepartmentModel targetMerchDepModelResult = targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersionModel,
                        new Integer(457));


        Assert.assertEquals(targetMerchDepModelExpected.getCode(), targetMerchDepModelResult.getCode());

    }

    /**
     * Test case where department is not found and a null gets returned.
     */
    @Test
    public void getDepartmentCategoryModelNegativeTest() {
        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetMerchDepartmentModel targetMerchDepModelExpected = null;

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                targetMerchDepModelExpected);

        final TargetMerchDepartmentModel targetMerchDepModelResult = targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersionModel,
                        new Integer(457));


        Assert.assertEquals(null, targetMerchDepModelResult);

    }

    /**
     * Test case where department is not found and a null gets returned.
     */
    @Test
    public void getDepartmentCategoryModelNegativeExceptionTest() {
        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final UnknownIdentifierException unexp = Mockito
                .mock(UnknownIdentifierException.class);
        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenThrow(unexp);

        final TargetMerchDepartmentModel targetMerchDepModelResult = targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersionModel,
                        new Integer(457));


        Assert.assertEquals(null, targetMerchDepModelResult);

    }

    /**
     * Test case where there are 3 size variants and 2 colour variants with department set at the size variant level. Is
     * department update required should return true;
     */
    @Test
    public void positiveDepartmentUpdateTest() {
        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetProductModel targetProductModel = targetProductMockHelper.createTargetProduct(
                new Integer(457),
                "DM1", 3, 2,
                null,
                Boolean.TRUE,
                Boolean.FALSE);

        final TargetMerchDepartmentModel targetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                targetMerchDepModel);

        targetProductModel.setMerchDepartment(targetMerchDepModel);

        Assert.assertTrue(targetProductDepartmentService
                .processMerchDeparment(targetProductModel, targetMerchDepModel));

        final List<String> listOfAllCategories = new ArrayList<>();
        for (final CategoryModel catModel : targetProductModel.getSupercategories()) {
            listOfAllCategories.add(catModel.getCode());
        }
        Assert.assertTrue(listOfAllCategories.contains("457"));

    }

    /**
     * Test case where there are 2 colour variants with department "657". The TargetProduct already has a department
     * 457. The new department 657 should get updated. department update required should return true;
     */
    @Test
    public void positiveDepartmentChangeUpdateTest() {
        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetProductModel targetProductModel = targetProductMockHelper.createTargetProduct(
                new Integer(657),
                "DM1", 3, 2,
                new Integer(457),
                Boolean.TRUE,
                Boolean.FALSE);

        final TargetMerchDepartmentModel existingtargetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);
        targetProductModel.setMerchDepartment(existingtargetMerchDepModel);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                existingtargetMerchDepModel);

        final TargetMerchDepartmentModel newtargetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("657",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "657")).thenReturn(
                newtargetMerchDepModel);

        Assert.assertTrue(targetProductDepartmentService
                .processMerchDeparment(targetProductModel, newtargetMerchDepModel));

        final List<String> listOfAllCategories = new ArrayList<>();
        for (final CategoryModel catModel : targetProductModel.getSupercategories()) {
            listOfAllCategories.add(catModel.getCode());
        }
        Assert.assertTrue(listOfAllCategories.contains("657"));

    }

    /**
     * Test case where there are 2 colour variants with department "457". The target Product is also having the
     * department 457. It should not update the existing row.
     */
    @Test
    public void negativeDepartmentNotChanged() {
        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetProductModel targetProductModel = targetProductMockHelper.createTargetProduct(
                new Integer(457),
                "DM1", 3, 2,
                new Integer(457),
                Boolean.TRUE,
                Boolean.FALSE);

        final TargetMerchDepartmentModel existingtargetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                existingtargetMerchDepModel);

        final TargetMerchDepartmentModel newtargetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                newtargetMerchDepModel);

        targetProductModel.setMerchDepartment(newtargetMerchDepModel);

        Assert.assertFalse(targetProductDepartmentService
                .processMerchDeparment(targetProductModel, newtargetMerchDepModel));

    }

    @Test
    public void testFindProductCountForEachMerchDep() {
        final List<TargetProductCountMerchDepRowData> expected = new ArrayList<>();
        when(targetProductMerchDepartmentDao.findProductCountForEachMerchDep()).thenReturn(
                expected);
        final List<TargetProductCountMerchDepRowData> result = targetProductDepartmentService
                .findProductCountForEachMerchDep();
        Assert.assertEquals(expected, result);
    }

}
