/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.product.dao.TargetProductDimensionsDao;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDimensionsServiceImplTest {
    @Mock
    private CategoryService categoryService;


    @Mock
    private TargetProductDimensionsDao targetProductDimensionsDao;

    @InjectMocks
    private final TargetProductDimensionsServiceImpl targetProductDimensionsService = new TargetProductDimensionsServiceImpl();

    @Mock
    private CatalogVersionModel catalogVersionModel;



    /**
     * Test case where a TargetProductDimensions is retrieved correctly.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void getDepartmentCategoryModelPositiveTest() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductDimensionsModel targetProductDimensionsModel = new TargetProductDimensionsModel();

        targetProductDimensionsModel.setDimensionCode("PKG_P1000");
        targetProductDimensionsModel.setWeight(Double.valueOf(10.0));
        targetProductDimensionsModel.setHeight(Double.valueOf(12.0));
        targetProductDimensionsModel.setLength(Double.valueOf(13.0));

        when(targetProductDimensionsDao.getTargetProductDimensionByCode(catalogVersionModel, "PKG_P1000")).thenReturn(
                targetProductDimensionsModel);

        final TargetProductDimensionsModel targetProductDimensionsModelResult = targetProductDimensionsService
                .getProductDimensionsForCode(catalogVersionModel, "PKG_P1000");

        Assert.assertEquals(targetProductDimensionsModel.getDimensionCode(),
                targetProductDimensionsModelResult.getDimensionCode());
    }

    /**
     * Test case where TargetProductDimensions is not found and a null gets returned.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void getDepartmentCategoryModelNegativeTest() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        when(targetProductDimensionsDao.getTargetProductDimensionByCode(catalogVersionModel, "PKG_P1000")).thenReturn(
                null);

        final TargetProductDimensionsModel targetProductDimensionsModelResult = targetProductDimensionsService
                .getProductDimensionsForCode(catalogVersionModel, "PKG_P1000");

        Assert.assertEquals(null, targetProductDimensionsModelResult);

    }

    /**
     * Test case where TargetProductDimensions is not found and an unknownidentifier exception is thrown.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void getDepartmentCategoryModelNegativeExceptionTest() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetUnknownIdentifierException unexp = Mockito
                .mock(TargetUnknownIdentifierException.class);
        when(targetProductDimensionsDao.getTargetProductDimensionByCode(catalogVersionModel, "PKG_P1000")).thenThrow(
                unexp);

        final TargetProductDimensionsModel targetProductDimensionsModelResult = targetProductDimensionsService
                .getProductDimensionsForCode(catalogVersionModel, "PKG_P1000");

        Assert.assertEquals(null, targetProductDimensionsModelResult);

    }


}
