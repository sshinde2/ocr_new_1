/**
 * 
 */
package au.com.target.tgtfluent.hmc.action;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.hmc.HMCHelper;
import de.hybris.platform.hmc.util.action.ActionEvent;
import de.hybris.platform.hmc.util.action.ActionResult;
import de.hybris.platform.hmc.util.action.ItemAction;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.servicelayer.model.ModelService;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author bhuang3
 *
 */
public abstract class AbstractFluentFeedAction<I extends Item, M extends ItemModel> extends ItemAction {

    protected static final FluentUpdateStatusService FLUENT_UPDATE_STATUS_SERVICE = (FluentUpdateStatusService)Registry
            .getApplicationContext().getBean(
                    "fluentUpdateStatusService");

    private static final ModelService MODEL_SERVICE = (ModelService)Registry.getApplicationContext().getBean(
            "modelService");

    private static final TargetFeatureSwitchService TARGET_FEATURE_SWITCH_SERVICES = (TargetFeatureSwitchService)Registry
            .getApplicationContext().getBean("targetFeatureSwitchService");

    private static final String FEATURE_SWITCH_OFF = "Fluent feature swith off";

    @Override
    public ActionResult perform(final ActionEvent actionEvent) throws JaloBusinessException {
        if (!TARGET_FEATURE_SWITCH_SERVICES.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return new ActionResult(1, FEATURE_SWITCH_OFF, false);
        }
        final Item item = getItem(actionEvent);
        if (item == null) {
            return new ActionResult(1, HMCHelper.getLocalizedString("action.notcreatedyet"), false);
        }
        if (!(canChange(item))) {
            return new ActionResult(1, HMCHelper.getLocalizedString("action.cannotchangetype"), false);
        }
        final I currentItem = (I)item;

        final M currentModel = MODEL_SERVICE.get(currentItem);

        return executeAction(currentModel);
    }

    protected abstract ActionResult executeAction(final M currentModel);

    /**
     * @return the fluentUpdateStatusService
     */
    public static FluentUpdateStatusService getFluentUpdateStatusService() {
        return FLUENT_UPDATE_STATUS_SERVICE;
    }

}
