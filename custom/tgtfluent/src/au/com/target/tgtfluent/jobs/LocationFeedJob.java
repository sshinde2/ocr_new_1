/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfluent.exception.FluentFeedException;
import au.com.target.tgtfluent.service.FluentFeedService;


/**
 * @author mgazal
 *
 */
public class LocationFeedJob extends AbstractJobPerformable<CronJobModel> {

    private FluentFeedService fluentFeedService;

    private TargetPointOfServiceService targetPointOfServiceService;

    @Override
    public PerformResult perform(final CronJobModel paramT) {
        final List<TargetPointOfServiceModel> openStores = targetPointOfServiceService.getAllStoresForFluent();
        try {
            fluentFeedService.feedLocation(openStores);
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
        catch (final FluentFeedException e) {
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }
    }

    /**
     * @param fluentFeedService
     *            the fluentFeedService to set
     */
    @Required
    public void setFluentFeedService(final FluentFeedService fluentFeedService) {
        this.fluentFeedService = fluentFeedService;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

}
