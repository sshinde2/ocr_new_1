/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author fkhan4
 *
 */
public class UpdateProductOnlineDateJob extends AbstractJobPerformable<CronJobModel> {
    private static final Logger LOG = Logger.getLogger(UpdateProductOnlineDateJob.class);
    private TargetProductService targetProductService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel arg0) {
        try {
            targetProductService.setOnlineDateForApplicableProducts();
        }
        catch (final Exception ex) {
            LOG.error("Failed to update product online date", ex);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    @Override
    public boolean isAbortable() {
        return true;
    }
    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

}
