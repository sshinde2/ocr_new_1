/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfluent.service.ProductFeedService;


/**
 * @author mgazal
 *
 */
public class ProductFeedJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(ProductFeedJob.class);

    private ProductFeedService productFeedService;

    @Override
    public PerformResult perform(final CronJobModel paramT) {
        try {
            productFeedService.feedProducts();
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
        catch (final Exception e) {
            LOG.error("FLUENT PRODUCT_BATCH_FEED", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }
    }

    @Override
    public boolean isAbortable() {
        return true;
    }

    /**
     * @param productFeedService
     *            the productFeedService to set
     */
    @Required
    public void setProductFeedService(final ProductFeedService productFeedService) {
        this.productFeedService = productFeedService;
    }
}
