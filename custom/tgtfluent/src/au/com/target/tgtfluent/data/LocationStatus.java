/**
 * 
 */
package au.com.target.tgtfluent.data;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author mgazal
 *
 */
public enum LocationStatus {
    ACTIVE, INACTIVE;

    public static LocationStatus get(final TargetPointOfServiceModel pointOfServiceModel) {
        if (Boolean.TRUE.equals(pointOfServiceModel.getClosed())) {
            return LocationStatus.INACTIVE;
        }
        return ACTIVE;
    }
}
