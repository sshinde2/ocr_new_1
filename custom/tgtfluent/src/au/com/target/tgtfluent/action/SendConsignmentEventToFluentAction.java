/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;


/**
 * @author cbi
 *
 */
public class SendConsignmentEventToFluentAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendConsignmentEventToFluentAction.class);

    private static final String FLUENT_FEATURE_SWITCH_OFF_NOT_FLUENT_ORDER = "FLUENT_FEATURE_SWITCH = OFF OR NOT A FLUENT ORDER.";

    private static final String FLUENT_ORDER_FAILED = "FLUENT_ORDER_FAILED: ";

    private static final String FLUENT_CONSIGNMENTS_IS_NULL = "FLUENT_CONSIGNMENTS_IS_NULL";

    private String eventName;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentFulfilmentService fluentFulfilmentService;

    @Override
    public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
        if (!(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotEmpty(orderProcessModel.getOrder().getFluentId()))) {
            LOG.info(FLUENT_FEATURE_SWITCH_OFF_NOT_FLUENT_ORDER);
            return;
        }
        try {
            final OrderModel orderModel = orderProcessModel.getOrder();
            final List<TargetConsignmentModel> consignments = getShippedAndNotEGiftCardConsignmentsForOrder(orderModel);
            if (CollectionUtils.isEmpty(consignments)) {
                LOG.warn(FLUENT_CONSIGNMENTS_IS_NULL + " WITH EVENT NAME " + eventName);
                return;
            }
            for (final TargetConsignmentModel consignment : consignments) {
                fluentFulfilmentService.sendConsignmentEventToFluent(consignment, eventName, false);
            }
        }
        catch (final FluentFulfilmentException e) {
            LOG.error(FLUENT_ORDER_FAILED + " WITH EVENT NAME " + eventName, e);
        }
    }

    /**
     * @param fluentFulfilmentService
     *            the fluentFulfilmentService to set
     */
    public void setFluentFulfilmentService(final FluentFulfilmentService fluentFulfilmentService) {
        this.fluentFulfilmentService = fluentFulfilmentService;
    }

    /**
     * 
     * @param orderModel
     * @return List<TargetConsignmentModel>
     */
    private List<TargetConsignmentModel> getShippedAndNotEGiftCardConsignmentsForOrder(final OrderModel orderModel) {
        if (CollectionUtils.isEmpty(orderModel.getConsignments())) {
            return Collections.emptyList();
        }
        else {
            final List<TargetConsignmentModel> activeOnes = new ArrayList<>();
            for (final ConsignmentModel cons : orderModel.getConsignments()) {
                if (cons instanceof TargetConsignmentModel
                        && ConsignmentStatus.SHIPPED.equals(cons.getStatus())
                        && !checkIsDigital(cons)
                        && checkIsDeliveryToStore(cons)) {
                    activeOnes.add((TargetConsignmentModel)cons);
                }
            }
            return activeOnes;
        }

    }

    private boolean checkIsDeliveryToStore(final ConsignmentModel cons) {
        boolean isDeliveryToStore = false;
        if (cons != null && cons.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
            isDeliveryToStore = BooleanUtils.isTrue(((TargetZoneDeliveryModeModel)cons.getDeliveryMode())
                    .getIsDeliveryToStore());
        }
        return isDeliveryToStore;

    }

    private boolean checkIsDigital(final ConsignmentModel cons) {
        boolean isDigital = false;
        if (cons != null && cons.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
            isDigital = BooleanUtils.isTrue(((TargetZoneDeliveryModeModel)cons.getDeliveryMode()).getIsDigital());
        }
        return isDigital;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param eventName
     *            the eventName to set
     */
    @Required
    public void setEventName(final String eventName) {
        this.eventName = eventName;
    }


}
