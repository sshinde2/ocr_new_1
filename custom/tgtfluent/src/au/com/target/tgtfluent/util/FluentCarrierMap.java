/**
 * 
 */
package au.com.target.tgtfluent.util;

import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;

import au.com.target.tgtfluent.constants.TgtFluentConstants.DeliveryMode;
import au.com.target.tgtfluent.constants.TgtFluentConstants.FluentCarrierName;
import au.com.target.tgtfluent.constants.TgtFluentConstants.TargetCarrierName;


/**
 * @author bpottass
 *
 */
public final class FluentCarrierMap {

    public static final String STORE = "store";

    public static final String NON_STORE = "non-store";

    private static final MultiKeyMap FLUENT_TO_HYBRIS_CARRIER_MAP = new MultiKeyMap();


    private FluentCarrierMap() {

    }

    static {
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.AUSTRALIA_POST, NON_STORE, DeliveryMode.HOME_DELIVERY),
                TargetCarrierName.AUSTRALIA_POST);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.AUSTRALIA_POST, NON_STORE, DeliveryMode.EBAY_HOME_DELIVERY),
                TargetCarrierName.AUSTRALIA_POST_EBAY);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.AUSTRALIA_POST, STORE, DeliveryMode.EBAY_HOME_DELIVERY),
                TargetCarrierName.AUSTRALIA_POST_EBAY_INSTORE_HD);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.AUSTRALIA_POST, STORE, DeliveryMode.EBAY_CLICK_COLLECT),
                TargetCarrierName.AUSTRALIA_POST_EBAY_INSTORE_CNC);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.AUSTRALIA_POST, STORE, DeliveryMode.CLICK_AND_COLLECT),
                TargetCarrierName.AUSTRALIA_POST_INSTORE_CNC);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.AUSTRALIA_POST, STORE, DeliveryMode.HOME_DELIVERY),
                TargetCarrierName.AUSTRALIA_POST_INSTORE_HD);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(new MultiKey(FluentCarrierName.NA, STORE, DeliveryMode.CLICK_AND_COLLECT),
                TargetCarrierName.NULL_CNC);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(new MultiKey(FluentCarrierName.NA, NON_STORE, DeliveryMode.DIGITAL_GIFT_CARD),
                TargetCarrierName.NULL_DIG_GIFT);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(new MultiKey(FluentCarrierName.NA, NON_STORE, DeliveryMode.EBAY_CLICK_COLLECT),
                TargetCarrierName.NULL_EBAY_CNC);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.STAR_TRACK, NON_STORE, DeliveryMode.EXPRESS_DELIVERY),
                TargetCarrierName.STAR_TRACK);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.STAR_TRACK, NON_STORE, DeliveryMode.EBAY_EXPRESS_DELIVERY),
                TargetCarrierName.STAR_TRACK_EBAY);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(new MultiKey(FluentCarrierName.TOLL, NON_STORE, DeliveryMode.HOME_DELIVERY),
                TargetCarrierName.TOLL);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.TOLL_CC, NON_STORE, DeliveryMode.CLICK_AND_COLLECT),
                TargetCarrierName.TOLL_CNC);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.TOLL, NON_STORE, DeliveryMode.EBAY_HOME_DELIVERY),
                TargetCarrierName.TOLL_EBAY);
        FLUENT_TO_HYBRIS_CARRIER_MAP.put(
                new MultiKey(FluentCarrierName.TOLL_CC, NON_STORE, DeliveryMode.EBAY_CLICK_COLLECT),
                TargetCarrierName.TOLL_EBAY_CNC);
    }


    public static final String getTargetCarrierName(final String fluentCarrierName, final String location,
            final String deliveryModeCode) {
        if (FLUENT_TO_HYBRIS_CARRIER_MAP.containsKey(fluentCarrierName, location, deliveryModeCode)) {
            return FLUENT_TO_HYBRIS_CARRIER_MAP.get(new MultiKey(fluentCarrierName, location, deliveryModeCode))
                    .toString();
        }
        else {
            throw new IllegalArgumentException("Invalid keys, no value for the combination of fluentCarrierName= "
                    + fluentCarrierName + "location= "
                    + location + "deliveryModeCode= " + deliveryModeCode);
        }
    }

    public static final String getFluentCarrierName(final String targetCarrierName) {
        if (FLUENT_TO_HYBRIS_CARRIER_MAP.containsValue(targetCarrierName)) {
            final Set<MultiKeyMap.Entry> entrySet = FLUENT_TO_HYBRIS_CARRIER_MAP.entrySet();
            MultiKey keys = null;
            for (final MultiKeyMap.Entry entry : entrySet) {
                if (targetCarrierName.equals(entry.getValue())) {
                    keys = (MultiKey)entry.getKey();
                    break;
                }
            }
            return keys != null ? (String)keys.getKey(0) : null;
        }
        else {
            throw new IllegalArgumentException("no value found for the targetCarrierName= "
                    + targetCarrierName);
        }
    }
}
