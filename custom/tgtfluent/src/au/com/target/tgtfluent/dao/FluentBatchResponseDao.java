/**
 * 
 */
package au.com.target.tgtfluent.dao;

import java.util.Collection;
import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;


/**
 * @author mgazal
 *
 */
public interface FluentBatchResponseDao {

    /**
     * Find the FluentBatchResponseModel for given typecode, jobId and batchId
     * 
     * @param typecode
     * @param jobId
     * @param batchId
     * @return {@link AbstractFluentBatchResponseModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    AbstractFluentBatchResponseModel find(String typecode, String jobId, String batchId)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

    /**
     * Find all FluentBatchResponseModel for given typecode and statuses
     * 
     * @param typecode
     * @param statuses
     * @return List of {@link AbstractFluentBatchResponseModel}
     */
    List<AbstractFluentBatchResponseModel> find(String typecode, Collection<FluentBatchStatus> statuses);
}
