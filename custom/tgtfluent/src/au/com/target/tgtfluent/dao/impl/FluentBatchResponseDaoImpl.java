/**
 * 
 */
package au.com.target.tgtfluent.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfluent.dao.FluentBatchResponseDao;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;


/**
 * @author mgazal
 *
 */
public class FluentBatchResponseDaoImpl extends DefaultGenericDao<AbstractFluentBatchResponseModel>
        implements FluentBatchResponseDao {

    public FluentBatchResponseDaoImpl() {
        super(AbstractFluentBatchResponseModel._TYPECODE);
    }

    @Override
    public AbstractFluentBatchResponseModel find(final String typecode, final String jobId, final String batchId)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final FlexibleSearchQuery query = createFlexibleSearchQuery(typecode, jobId, batchId);
        final FlexibleSearchService fss = getFlexibleSearchService();
        final SearchResult searchResult = fss.search(query);
        final List<AbstractFluentBatchResponseModel> result = searchResult.getResult();
        TargetServicesUtil.validateIfSingleResult(result, AbstractFluentBatchResponseModel.class,
                AbstractFluentBatchResponseModel.JOBID + "," + AbstractFluentBatchResponseModel.BATCHID,
                jobId + "," + batchId);
        return result.get(0);
    }

    @Override
    public List<AbstractFluentBatchResponseModel> find(final String typecode,
            final Collection<FluentBatchStatus> statuses) {
        final FlexibleSearchQuery query = createFlexibleSearchQuery(typecode, statuses);
        final SearchResult searchResult = getFlexibleSearchService().search(query);
        final List<AbstractFluentBatchResponseModel> results = searchResult.getResult();
        return results;
    }

    /**
     * @param typecode
     * @param jobId
     * @param batchId
     * @return {@link FlexibleSearchQuery}
     */
    private FlexibleSearchQuery createFlexibleSearchQuery(final String typecode, final String jobId,
            final String batchId) {
        final StringBuilder builder = createQueryString(typecode);
        builder.append("WHERE {c:").append(AbstractFluentBatchResponseModel.JOBID).append("} = ?")
                .append(AbstractFluentBatchResponseModel.JOBID);
        builder.append(" AND {c:").append(AbstractFluentBatchResponseModel.BATCHID).append("} = ?")
                .append(AbstractFluentBatchResponseModel.BATCHID);

        final FlexibleSearchQuery query = new FlexibleSearchQuery(builder.toString());
        query.addQueryParameter(AbstractFluentBatchResponseModel.JOBID, jobId);
        query.addQueryParameter(AbstractFluentBatchResponseModel.BATCHID, batchId);
        query.setNeedTotal(true);
        return query;
    }

    /**
     * @param typecode
     * @param statuses
     * @return {@link StringBuilder}
     */
    private FlexibleSearchQuery createFlexibleSearchQuery(final String typecode,
            final Collection<FluentBatchStatus> statuses) {
        final StringBuilder builder = createQueryString(typecode);
        builder.append("WHERE {c:").append(AbstractFluentBatchResponseModel.STATUS).append("} IN (?")
                .append(AbstractFluentBatchResponseModel.STATUS).append(")");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(builder.toString());
        query.addQueryParameter(AbstractFluentBatchResponseModel.STATUS, statuses);
        return query;
    }

    /**
     * @param typecode
     * @return {@link StringBuilder}
     */
    private StringBuilder createQueryString(final String typecode) {
        final StringBuilder builder = new StringBuilder();
        builder.append("SELECT {c:").append("pk").append("} ");
        builder.append("FROM {").append(typecode).append(" AS c} ");
        return builder;
    }
}
