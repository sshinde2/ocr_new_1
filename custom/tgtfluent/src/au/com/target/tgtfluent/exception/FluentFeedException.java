/**
 * 
 */
package au.com.target.tgtfluent.exception;

/**
 * @author mgazal
 *
 */
public class FluentFeedException extends RuntimeException {

    public FluentFeedException() {
        super();
    }

    public FluentFeedException(final String message) {
        super(message);
    }


    public FluentFeedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
