/**
 * 
 */
package au.com.target.tgtfluent.client;

import java.util.Map;

import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.FulfilmentOptionRequest;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkusResponse;


/**
 * @author mgazal
 *
 */
public interface FluentClient {

    /**
     * Creates a job record for storing a series of batches.
     * 
     * @param job
     * @return {@link FluentResponse}
     */
    FluentResponse createJob(Job job);

    /**
     * Retrieve details of the Job.
     * 
     * @param jobId
     * @return {@link FluentResponse}
     */
    FluentResponse viewJob(String jobId);

    /**
     * Create a Batch in the Fluent Retail system
     * 
     * @param jobId
     * @param batch
     * 
     * @return {@link FluentResponse}
     */
    FluentResponse createBatch(String jobId, Batch batch);

    /**
     * Retrieve details of the Batch
     * 
     * @param jobId
     * @param batchId
     * @param queryParams
     * @return {@link BatchResponse}
     */
    BatchResponse viewBatch(String jobId, String batchId, Map<String, String> queryParams);

    /**
     * @param location
     * @return {@link FluentResponse}
     */
    FluentResponse createLocation(Location location);

    /**
     * @param locationId
     *            The unique identifier assigned by Fluent Retail
     * @param location
     * @return {@link FluentResponse}
     */
    FluentResponse updateLocation(String locationId, Location location);

    /**
     * Create Product
     * 
     * @param product
     * @return {@link FluentResponse}
     */
    FluentResponse createProduct(Product product);

    /**
     * Update Product
     * 
     * @param fluentId
     * @param product
     * @return {@link FluentResponse}
     */
    FluentResponse updateProduct(String fluentId, Product product);

    /**
     * Search Product
     * 
     * @param id
     * @return {@link ProductsResponse}
     */
    ProductsResponse searchProduct(String id);

    /**
     * Create a SKU in the Fluent Retail system
     * 
     * @param productId
     * @param sku
     * @return {@link FluentResponse}
     */
    FluentResponse createSku(String productId, Sku sku);

    /**
     * Edit the SKU information
     * 
     * @param productId
     * @param skuId
     * @param sku
     * @return {@link FluentResponse}
     */
    FluentResponse updateSku(String productId, String skuId, Sku sku);

    /**
     * Retrieve a list of SKUs matching the search criteria
     * 
     * @param skuRef
     * @return {@link SkusResponse}
     */
    SkusResponse searchSkus(String skuRef);

    /**
     * Create Category
     * 
     * @param category
     * @return {@link FluentResponse}
     */
    FluentResponse createCategory(Category category);

    /**
     * Update Category
     * 
     * @param fluentId
     * @param category
     * @return {@link FluentResponse}
     */
    FluentResponse updateCategory(final String fluentId, Category category);

    /**
     * Search Category
     * 
     * @param categoryRef
     * @return {@link CategoryResponse}
     */
    CategoryResponse searchCategory(String categoryRef);

    /**
     * @param fulfilmentOptionRequest
     * @return {@link FluentResponse}
     */
    FluentResponse createFulfilmentOption(FulfilmentOptionRequest fulfilmentOptionRequest);

    /**
     * 
     * @param createOrderRequest
     * @return {@link FluentResponse}
     */
    FluentResponse createOrder(Order createOrderRequest);

    /**
     * Sends events to the Fluent platform in a synchronous manner
     * 
     * @param event
     * @return {@link FluentResponse}
     */
    FluentResponse eventSync(Event event);

    /**
     * Retrieve details of all fulfilments within an order by fluent order ID
     * 
     * @param fluentOrderId
     * @return {@link FulfilmentsResponse}
     */
    FulfilmentsResponse retrieveFulfilmentsByOrder(String fluentOrderId);

    /**
     * Retrieve the fluent order details.
     * 
     * @param fluentOrderId
     * @return {@link FulfilmentsResponse}
     */
    FluentResponse viewOrder(String fluentOrderId);

}
