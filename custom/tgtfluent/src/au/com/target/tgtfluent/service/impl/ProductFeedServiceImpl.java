/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.SkuFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentProductUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;
import au.com.target.tgtfluent.service.ProductFeedService;


/**
 * @author mgazal
 *
 */
public class ProductFeedServiceImpl extends AbstractBusinessService implements ProductFeedService {

    private static final Logger LOG = Logger.getLogger(ProductFeedServiceImpl.class);

    private static final String BATCH_SIZE = "tgtfluent.productFeed.batchSize";

    private static final String SUB_BATCH_SIZE = "tgtfluent.productFeed.sub.batchSize";

    private TargetSharedConfigService targetSharedConfigService;

    private TargetProductDao targetProductDao;

    private CatalogVersionService catalogVersionService;

    private FluentProductUpsertService fluentProductUpsertService;

    private FluentSkuUpsertService fluentSkuUpsertService;

    private FluentUpdateStatusService fluentUpdateStatusService;

    @Override
    public void feedProducts() {
        final CatalogVersionModel stagedProductCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);

        int noOfProductFetched = 0;
        final int batchSize = targetSharedConfigService.getInt(BATCH_SIZE, 10000);
        final int subBatchSize = targetSharedConfigService.getInt(SUB_BATCH_SIZE, 100);
        List<TargetProductModel> products;

        final List<String> erroredProducts = new ArrayList<>();
        LOG.info("FLUENT PRODUCT_BATCH_FEED: Start");
        do {
            final int pageSize = Math.min(batchSize - noOfProductFetched, subBatchSize);
            products = targetProductDao.findProductsNotSyncedToFluent(stagedProductCatalog, pageSize, erroredProducts);
            if (CollectionUtils.isEmpty(products)) {
                LOG.warn("FLUENT PRODUCT_BATCH_FEED: no products left to sync to fluent");
                break;
            }
            else {
                LOG.info("FLUENT PRODUCT_BATCH_FEED: syncing " + products.size() + " products");
            }
            noOfProductFetched += products.size();

            sendProducts(products, erroredProducts);
        }
        while (noOfProductFetched < batchSize && products.size() == subBatchSize);
        LOG.info("FLUENT PRODUCT_BATCH_FEED: End, noOfProductFetched=" + noOfProductFetched + ", erroredProducts="
                + erroredProducts);
    }

    /**
     * @param products
     * @param erroredProducts
     */
    protected void sendProducts(final List<TargetProductModel> products, final List<String> erroredProducts) {
        for (final TargetProductModel product : products) {
            try {
                fluentProductUpsertService.upsertProduct(product);
                final ProductFluentUpdateStatusModel fluentUpdateStatus = fluentUpdateStatusService
                        .getFluentUpdateStatus(ProductFluentUpdateStatusModel.class,
                                ProductFluentUpdateStatusModel._TYPECODE, product.getCode());
                if (fluentUpdateStatus != null && fluentUpdateStatus.isLastRunSuccessful()) {
                    sendSkus(product);
                }
                else {
                    LOG.warn("FLUENT PRODUCT_BATCH_FEED: failed for product:" + product.getCode());
                    erroredProducts.add(product.getCode());
                }
            }
            catch (final Exception e) {
                LOG.warn("FLUENT PRODUCT_BATCH_FEED: failed for product:" + product.getCode(), e);
                erroredProducts.add(product.getCode());
            }
        }
    }

    /**
     * @param product
     */
    protected void sendSkus(final TargetProductModel product) {
        final List<AbstractTargetVariantProductModel> sellableVariants = new ArrayList<>();
        collectSellableVariantsNotSyncedToFluent(product, sellableVariants);
        for (final AbstractTargetVariantProductModel variantProduct : sellableVariants) {
            try {
                fluentSkuUpsertService.upsertSku(variantProduct);
                final SkuFluentUpdateStatusModel fluentUpdateStatus = fluentUpdateStatusService.getFluentUpdateStatus(
                        SkuFluentUpdateStatusModel.class, SkuFluentUpdateStatusModel._TYPECODE,
                        variantProduct.getCode());
                if (fluentUpdateStatus == null || !fluentUpdateStatus.isLastRunSuccessful()) {
                    LOG.warn("FLUENT PRODUCT_BATCH_FEED: failed for variant:" + variantProduct.getCode());
                }
            }
            catch (final Exception e) {
                LOG.warn("FLUENT PRODUCT_BATCH_FEED: failed for variant:" + variantProduct.getCode(), e);
            }
        }
    }

    /**
     * Collect all sellableVariants for given product which have not been synced to fluent
     * 
     * @param product
     * @param sellableVariants
     */
    protected void collectSellableVariantsNotSyncedToFluent(final ProductModel product,
            final List<AbstractTargetVariantProductModel> sellableVariants) {
        for (final VariantProductModel variantProduct : product.getVariants()) {
            if (CollectionUtils.isNotEmpty(variantProduct.getVariants())) {
                collectSellableVariantsNotSyncedToFluent(variantProduct, sellableVariants);
            }
            else if (variantProduct instanceof AbstractTargetVariantProductModel) {
                final SkuFluentUpdateStatusModel fluentUpdateStatus = fluentUpdateStatusService.getFluentUpdateStatus(
                        SkuFluentUpdateStatusModel.class, SkuFluentUpdateStatusModel._TYPECODE,
                        variantProduct.getCode());
                if (fluentUpdateStatus == null || !fluentUpdateStatus.isLastRunSuccessful()) {
                    sellableVariants.add((AbstractTargetVariantProductModel)variantProduct);
                }
            }
        }
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param targetProductDao
     *            the targetProductDao to set
     */
    @Required
    public void setTargetProductDao(final TargetProductDao targetProductDao) {
        this.targetProductDao = targetProductDao;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param fluentProductUpsertService
     *            the fluentProductUpsertService to set
     */
    @Required
    public void setFluentProductUpsertService(final FluentProductUpsertService fluentProductUpsertService) {
        this.fluentProductUpsertService = fluentProductUpsertService;
    }

    /**
     * @param fluentSkuUpsertService
     *            the fluentSkuUpsertService to set
     */
    @Required
    public void setFluentSkuUpsertService(final FluentSkuUpsertService fluentSkuUpsertService) {
        this.fluentSkuUpsertService = fluentSkuUpsertService;
    }

    /**
     * @param fluentUpdateStatusService
     *            the fluentUpdateStatusService to set
     */
    @Required
    public void setFluentUpdateStatusService(final FluentUpdateStatusService fluentUpdateStatusService) {
        this.fluentUpdateStatusService = fluentUpdateStatusService;
    }
}
