/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchAction;
import au.com.target.tgtfluent.data.BatchRecordStatus;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.BatchStatus;
import au.com.target.tgtfluent.data.EntityType;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.JobResponse;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.exception.FluentFeedException;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentFeedServiceImplTest {

    @Mock
    private Converter<TargetPointOfServiceModel, Location> locationConverter;

    @Mock
    private FluentClient fluentClient;

    @Mock
    private ModelService modelService;

    @InjectMocks
    @Spy
    private final FluentFeedServiceImpl fluentFeedService = new FluentFeedServiceImpl();

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private Location location;

    @Mock
    private FluentResponse fluentResponse;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    private final Map<String, String> queryParams = new HashMap<>();

    @Before
    public void setup() {
        fluentFeedService.setBatchCount("500");
        queryParams.put("count", "500");
        given(locationConverter.convert(targetPointOfServiceModel)).willReturn(location);
        given(targetPointOfServiceModel.getStoreNumber()).willReturn(Integer.valueOf(1234));
    }

    @Test
    public void testFeedLocationBatch() {
        final List<TargetPointOfServiceModel> pointOfServiceModels = new ArrayList<>();
        final TargetPointOfServiceModel pointOfServiceModel = mock(TargetPointOfServiceModel.class);
        pointOfServiceModels.add(pointOfServiceModel);
        final ArgumentCaptor<Job> jobCaptor = ArgumentCaptor.forClass(Job.class);
        final FluentResponse jobResponse = mock(JobResponse.class);
        given(jobResponse.getId()).willReturn("1234");
        given(fluentClient.createJob(jobCaptor.capture())).willReturn(jobResponse);
        given(locationConverter.convert(pointOfServiceModel)).willReturn(location);
        final ArgumentCaptor<Batch> batchCaptor = ArgumentCaptor.forClass(Batch.class);
        final FluentResponse batchResponse = mock(FluentResponse.class);
        given(batchResponse.getId()).willReturn("4321");
        final ArgumentCaptor<String> jobIdCaptor = ArgumentCaptor.forClass(String.class);
        given(fluentClient.createBatch(jobIdCaptor.capture(), batchCaptor.capture())).willReturn(batchResponse);
        final LocationFluentBatchResponseModel fluentBatchResponse = mock(LocationFluentBatchResponseModel.class);
        given(modelService.create(LocationFluentBatchResponseModel.class)).willReturn(fluentBatchResponse);

        fluentFeedService.feedLocation(pointOfServiceModels);
        assertThat(jobIdCaptor.getValue()).isEqualTo("1234");
        final Job job = jobCaptor.getValue();
        assertThat(job.getName()).startsWith("LOCATION_");
        final Batch batch = batchCaptor.getValue();
        assertThat(batch.getAction()).isEqualTo(BatchAction.UPSERT);
        assertThat(batch.getEntityType()).isEqualTo(EntityType.LOCATION);
        assertThat(batch.getEntities()).hasSize(1);
        assertThat(batch.getEntities()).containsExactly(location);
        verify(fluentBatchResponse).setJobId("1234");
        verify(fluentBatchResponse).setBatchId("4321");
        verify(fluentBatchResponse).setStatus(FluentBatchStatus.PENDING);
        verify(modelService).save(fluentBatchResponse);
    }

    @Test(expected = FluentFeedException.class)
    public void testFeedLocationJobFail() {
        final List<TargetPointOfServiceModel> pointOfServiceModels = new ArrayList<>();
        final TargetPointOfServiceModel pointOfServiceModel = mock(TargetPointOfServiceModel.class);
        pointOfServiceModels.add(pointOfServiceModel);
        final FluentResponse jobResponse = mock(JobResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        errors.add(error);
        given(jobResponse.getErrors()).willReturn(errors);
        given(fluentClient.createJob(Mockito.any(Job.class))).willReturn(jobResponse);

        fluentFeedService.feedLocation(pointOfServiceModels);
    }

    @Test(expected = FluentFeedException.class)
    public void testFeedLocationBatchFail() {
        final List<TargetPointOfServiceModel> pointOfServiceModels = new ArrayList<>();
        final TargetPointOfServiceModel pointOfServiceModel = mock(TargetPointOfServiceModel.class);
        pointOfServiceModels.add(pointOfServiceModel);
        final FluentResponse jobResponse = mock(JobResponse.class);
        given(jobResponse.getId()).willReturn("1234");
        given(fluentClient.createJob(Mockito.any(Job.class))).willReturn(jobResponse);
        given(locationConverter.convert(pointOfServiceModel)).willReturn(location);
        final FluentResponse batchResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        errors.add(error);
        given(batchResponse.getErrors()).willReturn(errors);
        given(fluentClient.createBatch(Mockito.anyString(), Mockito.any(Batch.class))).willReturn(batchResponse);
        final LocationFluentBatchResponseModel fluentBatchResponse = mock(LocationFluentBatchResponseModel.class);
        given(modelService.create(LocationFluentBatchResponseModel.class)).willReturn(fluentBatchResponse);

        fluentFeedService.feedLocation(pointOfServiceModels);
    }

    @Test
    public void testCheckBatchStatusPending() {
        final AbstractFluentBatchResponseModel batchResponseModel = mock(AbstractFluentBatchResponseModel.class);
        given(batchResponseModel.getJobId()).willReturn("1");
        given(batchResponseModel.getBatchId()).willReturn("1");
        final BatchResponse batchResponse = mock(BatchResponse.class);
        given(batchResponse.getStatus()).willReturn(BatchStatus.PENDING);
        given(fluentClient.viewBatch("1", "1", queryParams)).willReturn(batchResponse);

        fluentFeedService.checkBatchStatus(batchResponseModel);
        verify(batchResponseModel).setStatus(FluentBatchStatus.PENDING);
        verify(modelService).save(batchResponseModel);
    }

    @Test
    public void testCheckBatchStatusRunning() {
        final AbstractFluentBatchResponseModel batchResponseModel = mock(AbstractFluentBatchResponseModel.class);
        given(batchResponseModel.getJobId()).willReturn("1");
        given(batchResponseModel.getBatchId()).willReturn("1");
        final BatchResponse batchResponse = mock(BatchResponse.class);
        given(batchResponse.getStatus()).willReturn(BatchStatus.RUNNING);
        given(fluentClient.viewBatch("1", "1", queryParams)).willReturn(batchResponse);

        fluentFeedService.checkBatchStatus(batchResponseModel);
        verify(batchResponseModel).setStatus(FluentBatchStatus.RUNNING);
        verify(modelService).save(batchResponseModel);
    }

    @Test
    public void testCheckBatchStatusComplete() {
        final AbstractFluentBatchResponseModel batchResponseModel = mock(AbstractFluentBatchResponseModel.class);
        given(batchResponseModel.getJobId()).willReturn("1");
        given(batchResponseModel.getBatchId()).willReturn("1");
        final BatchResponse batchResponse = mock(BatchResponse.class);
        given(batchResponse.getStatus()).willReturn(BatchStatus.COMPLETE);
        final List<BatchRecordStatus> recordStatuses = new ArrayList<>();
        final BatchRecordStatus recordStatus1 = mock(BatchRecordStatus.class);
        given(recordStatus1.getResponseCode()).willReturn("200");
        recordStatuses.add(recordStatus1);
        final BatchRecordStatus recordStatus2 = mock(BatchRecordStatus.class);
        given(recordStatus2.getResponseCode()).willReturn("400");
        recordStatuses.add(recordStatus2);
        final BatchRecordStatus recordStatus3 = mock(BatchRecordStatus.class);
        given(recordStatus3.getResponseCode()).willReturn("300");
        recordStatuses.add(recordStatus3);
        given(batchResponse.getResults()).willReturn(recordStatuses);
        given(fluentClient.viewBatch("1", "1", queryParams)).willReturn(batchResponse);

        fluentFeedService.checkBatchStatus(batchResponseModel);
        verify(batchResponseModel).setStatus(FluentBatchStatus.COMPLETE);
        verify(fluentFeedService, never()).logRecordStatus(Level.ERROR, batchResponse, recordStatus1);
        verify(fluentFeedService).logRecordStatus(Level.ERROR, batchResponse, recordStatus2);
        verify(fluentFeedService).logRecordStatus(Level.WARN, batchResponse, recordStatus3);
        verify(modelService).save(batchResponseModel);
    }
}
