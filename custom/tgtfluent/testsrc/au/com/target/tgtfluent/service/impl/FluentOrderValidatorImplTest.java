package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderItem;


/**
 * @author bpottass
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentOrderValidatorImplTest {

    @Mock
    private OrderModel orderModel;

    @Mock
    private Order fluentOrder;

    @Mock
    private OrderItem fluentOrderItem;

    @Mock
    private ConsignmentModel consignmentModel;

    @Mock
    private ConsignmentModel consignmentModel2;

    @Mock
    private AbstractOrderEntryModel orderEntryModel;

    @Mock
    private AbstractOrderEntryModel orderEntryModel2;

    @Mock
    private ConsignmentEntryModel consignmentEntryModel;

    @Mock
    private ConsignmentEntryModel consignmentEntryModel2;

    @InjectMocks
    private final FluentOrderValidatorImpl fluentOrderValidator = new FluentOrderValidatorImpl();

    @Test()
    public void testValidateShippedConsignment() {
        willReturn(ConsignmentStatus.SHIPPED).given(consignmentModel).getStatus();
        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel));
        assertThat(fluentOrderValidator.validateIfAnyConsignmentsAreOpen(orderModel)).isFalse();
    }

    @Test()
    public void testValidateOpenConsignment() {
        willReturn(ConsignmentStatus.PICKED).given(consignmentModel).getStatus();
        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel));
        assertThat(fluentOrderValidator.validateIfAnyConsignmentsAreOpen(orderModel)).isTrue();
    }

    @Test()
    public void testValidateAnyConsignmentsOpenWhenNoConsignmentsArePresent() {
        given(orderModel.getConsignments()).willReturn(null);
        assertThat(fluentOrderValidator.validateIfAnyConsignmentsAreOpen(orderModel)).isFalse();
    }

    @Test()
    public void testValidateAnyConsignmentsOpenWhenConsignmentsAreCancelledAndShipped() {
        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel, consignmentModel2));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignmentModel2.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        assertThat(fluentOrderValidator.validateIfAnyConsignmentsAreOpen(orderModel)).isFalse();
    }

    @Test
    public void testValidateIfAnyItemInOrderIsNotShippedForAllItemsShipped() {
        given(orderModel.getEntries()).willReturn(ImmutableList.of(orderEntryModel, orderEntryModel2));
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(2));
        given(orderEntryModel2.getQuantity()).willReturn(Long.valueOf(3));

        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel, consignmentModel2));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignmentModel2.getStatus()).willReturn(ConsignmentStatus.SHIPPED);

        given(consignmentModel.getConsignmentEntries()).willReturn(ImmutableSet.of(consignmentEntryModel));
        given(consignmentModel2.getConsignmentEntries()).willReturn(ImmutableSet.of(consignmentEntryModel2));

        given(consignmentEntryModel.getShippedQuantity()).willReturn(Long.valueOf(2));
        given(consignmentEntryModel2.getShippedQuantity()).willReturn(Long.valueOf(3));

        assertThat(fluentOrderValidator.validateIfAnyItemInOrderIsNotShipped(orderModel)).isFalse();
    }

    @Test
    public void testValidateIfAnyItemInOrderIsNotShippedForAllItemsShippedWithACancelledConsignment() {
        given(orderModel.getEntries()).willReturn(ImmutableList.of(orderEntryModel, orderEntryModel2));
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(2));
        given(orderEntryModel2.getQuantity()).willReturn(Long.valueOf(3));

        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel, consignmentModel2));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignmentModel2.getStatus()).willReturn(ConsignmentStatus.CANCELLED);

        given(consignmentModel.getConsignmentEntries())
                .willReturn(ImmutableSet.of(consignmentEntryModel, consignmentEntryModel2));

        given(consignmentEntryModel.getShippedQuantity()).willReturn(Long.valueOf(2));
        given(consignmentEntryModel2.getShippedQuantity()).willReturn(Long.valueOf(3));

        assertThat(fluentOrderValidator.validateIfAnyItemInOrderIsNotShipped(orderModel)).isFalse();
    }

    @Test
    public void testValidateIfAnyItemInOrderIsNotShippedForAllItemsNotShipped() {
        given(orderModel.getEntries()).willReturn(ImmutableList.of(orderEntryModel, orderEntryModel2));
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(2));
        given(orderEntryModel2.getQuantity()).willReturn(Long.valueOf(4));

        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel, consignmentModel2));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignmentModel2.getStatus()).willReturn(ConsignmentStatus.SHIPPED);

        given(consignmentModel.getConsignmentEntries()).willReturn(ImmutableSet.of(consignmentEntryModel));
        given(consignmentModel2.getConsignmentEntries()).willReturn(ImmutableSet.of(consignmentEntryModel2));

        given(consignmentEntryModel.getShippedQuantity()).willReturn(Long.valueOf(2));
        given(consignmentEntryModel2.getShippedQuantity()).willReturn(Long.valueOf(3));

        assertThat(fluentOrderValidator.validateIfAnyItemInOrderIsNotShipped(orderModel)).isTrue();

    }

    @Test
    public void testValidateIfAnyItemInOrderIsNotShippedForAllItemsNotShippedAndHasCancelledConsignment() {
        given(orderModel.getEntries()).willReturn(ImmutableList.of(orderEntryModel, orderEntryModel2));
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(2));
        given(orderEntryModel2.getQuantity()).willReturn(Long.valueOf(4));

        given(orderModel.getConsignments()).willReturn(ImmutableSet.of(consignmentModel, consignmentModel2));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignmentModel2.getStatus()).willReturn(ConsignmentStatus.CANCELLED);

        given(consignmentModel.getConsignmentEntries()).willReturn(ImmutableSet.of(consignmentEntryModel));

        given(consignmentEntryModel.getShippedQuantity()).willReturn(Long.valueOf(2));

        assertThat(fluentOrderValidator.validateIfAnyItemInOrderIsNotShipped(orderModel)).isTrue();

    }

    @Test
    public void testIsOrderDataInvalidForOrderIdNull() {
        given(fluentOrder.getOrderId()).willReturn(null);
        assertThat(fluentOrderValidator.isOrderDataInValid(fluentOrder)).isTrue();
    }

    @Test
    public void testIsOrderDataInvalidForOrderStatusNull() {
        given(fluentOrder.getOrderId()).willReturn("123");
        given(fluentOrder.getItems()).willReturn(ImmutableList.of(fluentOrderItem));
        assertThat(fluentOrderValidator.isOrderDataInValid(fluentOrder)).isTrue();
    }

    @Test
    public void testIsOrderDataInvalidForValidOrder() {
        given(fluentOrder.getOrderId()).willReturn("123");
        given(fluentOrder.getItems()).willReturn(ImmutableList.of(fluentOrderItem));
        given(fluentOrder.getStatus()).willReturn("SHIPPED");
        assertThat(fluentOrderValidator.isOrderDataInValid(fluentOrder)).isFalse();
    }

}
