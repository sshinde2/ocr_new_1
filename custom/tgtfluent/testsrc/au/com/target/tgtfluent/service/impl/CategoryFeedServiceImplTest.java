package au.com.target.tgtfluent.service.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryFeedServiceImplTest {

    @Mock
    private FluentCategoryUpsertService fluentCategoryUpsertService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @Mock
    private TargetCategoryService targetCategoryService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @InjectMocks
    private final CategoryFeedServiceImpl categoryFeedServiceImpl = new CategoryFeedServiceImpl();


    @Test
    public void testFeedCategoriesToFluentWhenCategoriesHasNoFluentUpdateStatus() {
        createCategoryStructure(false, Boolean.FALSE);
        categoryFeedServiceImpl.feedCategoriesToFluent();
        verify(fluentCategoryUpsertService, times(8)).upsertCategory(Mockito.any(TargetProductCategoryModel.class));
    }

    @Test
    public void testFeedCategoriesToFluentWhenAllCategoriesSynchedToFluent() {
        createCategoryStructure(true, Boolean.TRUE);
        categoryFeedServiceImpl.feedCategoriesToFluent();
        verify(fluentCategoryUpsertService, never()).upsertCategory(Mockito.any(TargetProductCategoryModel.class));
    }

    @Test
    public void testFeedCategoriesToFluentWhenLastRunForCategoriesNotSuccessful() {
        createCategoryStructure(true, Boolean.FALSE);
        categoryFeedServiceImpl.feedCategoriesToFluent();
        verify(fluentCategoryUpsertService, times(8)).upsertCategory(Mockito.any(TargetProductCategoryModel.class));
    }

    private void createCategoryStructure(final boolean hasUpdateStatus, final Boolean lastRunSuccessful) {

        //8.Root/5.Baby/3.Babywear/1.Bodysuits
        //Root/Baby/Babywear/2.Dresses
        //Root/Baby/4.Feeding
        //Root/7.Kids/6.Shoes

        final CategoryModel categoryBodySuits = createTargetProductCategoryMock("bodysuits1", null, hasUpdateStatus,
                lastRunSuccessful);
        final CategoryModel categoryDresses = createTargetProductCategoryMock("dresses2", null, hasUpdateStatus,
                lastRunSuccessful);
        final List<CategoryModel> subCategoriesBabywear = new ArrayList();
        subCategoriesBabywear.add(categoryBodySuits);
        subCategoriesBabywear.add(categoryDresses);

        final CategoryModel categoryBabywear = createTargetProductCategoryMock("babywear3", subCategoriesBabywear,
                hasUpdateStatus,
                lastRunSuccessful);
        final CategoryModel categoryFeeding = createTargetProductCategoryMock("feeding4", null, hasUpdateStatus,
                lastRunSuccessful);

        final List<CategoryModel> subCategoriesBaby = new ArrayList();
        subCategoriesBaby.add(categoryBabywear);
        subCategoriesBaby.add(categoryFeeding);
        final CategoryModel categoryBaby = createTargetProductCategoryMock("baby5", subCategoriesBaby, hasUpdateStatus,
                lastRunSuccessful);

        final CategoryModel categoryShoes = createTargetProductCategoryMock("shoes6", null, hasUpdateStatus,
                lastRunSuccessful);
        final List<CategoryModel> subCategoriesKids = new ArrayList();
        subCategoriesKids.add(categoryShoes);

        final CategoryModel categoryKids = createTargetProductCategoryMock("kids7", subCategoriesKids, hasUpdateStatus,
                lastRunSuccessful);

        final List<CategoryModel> subCategoriesRoot = new ArrayList();
        subCategoriesRoot.add(categoryBaby);
        subCategoriesRoot.add(categoryKids);

        final CategoryModel rootCategory = createTargetProductCategoryMock("AP01", subCategoriesRoot, hasUpdateStatus,
                lastRunSuccessful);

        final CatalogVersionModel offlineProductCatalog = mock(CatalogVersionModel.class);

        willReturn(offlineProductCatalog).given(catalogVersionService).getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        willReturn(rootCategory).given(targetCategoryService)
                .getCategoryForCode(offlineProductCatalog, TgtFluentConstants.Category.ALL_PRODUCTS);

    }

    private CategoryModel createTargetProductCategoryMock(final String code, final List<CategoryModel> subCategories,
            final boolean hasUpdatedStatus, final Boolean lastRunSuccessful) {
        final CategoryModel category = mock(TargetProductCategoryModel.class);
        given(category.getCode()).willReturn(code);
        given(category.getCategories()).willReturn(subCategories);
        if (hasUpdatedStatus) {
            final CategoryFluentUpdateStatusModel updateStatus = createCategoryFluentUpdateStatusMock(code);
            willReturn(lastRunSuccessful).given(updateStatus).isLastRunSuccessful();
        }
        return category;
    }

    private CategoryFluentUpdateStatusModel createCategoryFluentUpdateStatusMock(final String code) {
        final CategoryFluentUpdateStatusModel updateStatusModel = mock(CategoryFluentUpdateStatusModel.class);
        willReturn(updateStatusModel).given(fluentUpdateStatusService).getFluentUpdateStatus(
                CategoryFluentUpdateStatusModel.class,
                CategoryFluentUpdateStatusModel._TYPECODE, code);
        return updateStatusModel;
    }

}
