/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.exception.ProductFluentFeedException;
import au.com.target.tgtfluent.model.FluentUpdateFailureCauseModel;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentProductUpsertServiceImplTest {

    @Mock
    protected FluentClient fluentClient;

    @Mock
    private ModelService modelService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @Mock
    private Converter<TargetProductModel, Product> converter;

    @InjectMocks
    @Spy
    private final FluentProductUpsertServiceImpl fluentProductUpsertServiceImpl = new FluentProductUpsertServiceImpl();

    @Mock
    private TargetProductModel productModel;

    @Mock
    private Product product;

    @Mock
    private ProductFluentUpdateStatusModel productUpdateStatus;

    @Mock
    private FluentUpdateFailureCauseModel updateFailureCause;

    private final String fluentId = "Test fluentId";

    @Before
    public void setup() {
        given(converter.convert(productModel)).willReturn(product);
        given(fluentUpdateStatusService.getFluentUpdateStatus(ProductFluentUpdateStatusModel.class,
                ProductFluentUpdateStatusModel._TYPECODE, productModel.getCode())).willReturn(productUpdateStatus);
        final List<FluentUpdateFailureCauseModel> failureCauses = mock(List.class);
        given(productUpdateStatus.getFailureCauses()).willReturn(failureCauses);
        given(modelService.create(FluentUpdateFailureCauseModel.class))
                .willReturn(updateFailureCause);
    }

    @Test
    public void testUpsertProductWithoutAnyCategory() {
        given(converter.convert(productModel)).willThrow(
                new ProductFluentFeedException("NO SUPER CATEGORIES", "Product doesn't have any super categories"));

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("NO SUPER CATEGORIES");
        verify(updateFailureCause).setMessage("Product doesn't have any super categories");
        verify(productUpdateStatus).setLastRunSuccessful(false);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, productUpdateStatus));
        verifyZeroInteractions(fluentClient);
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductCreateSuccess() {
        given(productModel.getFluentId()).willReturn(StringUtils.EMPTY);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createProduct(product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).createProduct(product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(true);
        verify(productUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(productModel, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductCreateFail() {
        given(productModel.getFluentId()).willReturn(StringUtils.EMPTY);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Disaster");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createProduct(product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).createProduct(product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(false);
        verify(productUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Disaster");
        verify(updateFailureCause).setUpdateStatus(productUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductUpdateOnConflict() {
        given(product.getProductRef()).willReturn("P01");
        given(productModel.getFluentId()).willReturn(StringUtils.EMPTY);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("409");
        given(error.getMessage())
                .willReturn("product reference W200000 already exists for this retailer");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createProduct(product)).willReturn(fluentResponse);
        final ProductsResponse productsResponse = mock(ProductsResponse.class);
        final List<Product> products = new ArrayList<>();
        final Product productResult = mock(Product.class);
        given(productResult.getProductId()).willReturn(fluentId);
        products.add(productResult);
        given(productsResponse.getResults()).willReturn(products);
        given(fluentClient.searchProduct("P01")).willReturn(productsResponse);
        fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.updateProduct(fluentId, product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).createProduct(product);
        verify(fluentClient).searchProduct("P01");
        verify(fluentClient).updateProduct(fluentId, product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(true);
        verify(productUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(productModel, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductCreateAndUpdateFail() {
        given(product.getProductRef()).willReturn("P01");
        given(productModel.getFluentId()).willReturn(StringUtils.EMPTY);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        List<Error> errors = new ArrayList<>();
        Error error = mock(Error.class);
        given(error.getCode()).willReturn("409");
        given(error.getMessage())
                .willReturn("product reference W200000 already exists for this retailer");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createProduct(product)).willReturn(fluentResponse);
        final ProductsResponse productsResponse = mock(ProductsResponse.class);
        final List<Product> products = new ArrayList<>();
        final Product productResult = mock(Product.class);
        given(productResult.getProductId()).willReturn(fluentId);
        products.add(productResult);
        given(productsResponse.getResults()).willReturn(products);
        given(fluentClient.searchProduct("P01")).willReturn(productsResponse);
        fluentResponse = mock(FluentResponse.class);
        errors = new ArrayList<>();
        error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Disaster");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateProduct(fluentId, product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).createProduct(product);
        verify(fluentClient).searchProduct("P01");
        verify(fluentClient).updateProduct(fluentId, product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(false);
        verify(productUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Disaster");
        verify(updateFailureCause).setUpdateStatus(productUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductUpdateSuccess() {
        given(productModel.getFluentId()).willReturn(fluentId);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.updateProduct(fluentId, product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).updateProduct(fluentId, product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(true);
        verify(productUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(productModel, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductCreateOnNotFound() {
        given(productModel.getFluentId()).willReturn(fluentId);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("404");
        given(error.getMessage()).willReturn("Object of type [Sku] was not found using id : [67]");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateProduct(fluentId, product)).willReturn(fluentResponse);
        fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createProduct(product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).updateProduct(fluentId, product);
        verify(fluentClient).createProduct(product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(true);
        verify(productUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(productModel, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertProductUpdateAndCreateFail() {
        given(productModel.getFluentId()).willReturn(fluentId);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        List<Error> errors = new ArrayList<>();
        Error error = mock(Error.class);
        given(error.getCode()).willReturn("404");
        given(error.getMessage()).willReturn("Object of type [Sku] was not found using id : [67]");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateProduct(fluentId, product)).willReturn(fluentResponse);
        fluentResponse = mock(FluentResponse.class);
        errors = new ArrayList<>();
        error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Disaster");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createProduct(product)).willReturn(fluentResponse);

        fluentProductUpsertServiceImpl.upsertProduct(productModel);
        verify(fluentClient).updateProduct(fluentId, product);
        verify(fluentClient).createProduct(product);
        verifyNoMoreInteractions(fluentClient);
        verify(productUpdateStatus).setLastRunSuccessful(false);
        verify(productUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Disaster");
        verify(updateFailureCause).setUpdateStatus(productUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, productUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }
}
