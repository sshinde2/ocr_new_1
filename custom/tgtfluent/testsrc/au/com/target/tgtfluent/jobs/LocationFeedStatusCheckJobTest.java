/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;
import au.com.target.tgtfluent.service.FluentBatchResponseService;
import au.com.target.tgtfluent.service.FluentFeedService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LocationFeedStatusCheckJobTest {

    @Mock
    private FluentBatchResponseService fluentBatchResponseService;

    @Mock
    private FluentFeedService fluentFeedService;

    @InjectMocks
    private final LocationFeedStatusCheckJob locationFeedStatusCheckJob = new LocationFeedStatusCheckJob();

    @Test
    public void testPerform() {
        final List<AbstractFluentBatchResponseModel> batchRespones = new ArrayList<>();
        final AbstractFluentBatchResponseModel batchResponse1 = mock(AbstractFluentBatchResponseModel.class);
        batchRespones.add(batchResponse1);
        final AbstractFluentBatchResponseModel batchResponse2 = mock(AbstractFluentBatchResponseModel.class);
        batchRespones.add(batchResponse2);
        given(fluentBatchResponseService.find(LocationFluentBatchResponseModel._TYPECODE,
                Arrays.asList(FluentBatchStatus.PENDING, FluentBatchStatus.RUNNING))).willReturn(batchRespones);
        final CronJobModel cronJob = mock(CronJobModel.class);

        final PerformResult result = locationFeedStatusCheckJob.perform(cronJob);
        verify(fluentFeedService).checkBatchStatus(batchResponse1);
        verify(fluentFeedService).checkBatchStatus(batchResponse2);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }
}
