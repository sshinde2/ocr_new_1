/**
 * 
 */
package au.com.target.tgtfluent.action;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentCancelOrderActionTest {

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private FluentOrderService fluentOrderService;

    @InjectMocks
    private final FluentCancelOrderAction fluentCancelOrderAction = new FluentCancelOrderAction();

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Before
    public void setup() {
        willReturn(orderModel).given(orderProcessModel).getOrder();
    }

    @Test
    public void testExecuteAction() throws RetryLaterException, Exception {
        fluentCancelOrderAction.executeAction(orderProcessModel);
        verifyNoMoreInteractions(fluentOrderService);
    }

    @Test
    public void testExecuteActionWithFluent() throws RetryLaterException, Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderModel.getFluentId()).willReturn("123");
        fluentCancelOrderAction.executeAction(orderProcessModel);
        verify(fluentOrderService).cancelOrder(orderModel);
    }

    @Test(expected = FluentOrderException.class)
    public void testExecuteActionWithFluentFail() throws RetryLaterException, Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        willThrow(new FluentOrderException()).given(fluentOrderService).cancelOrder(orderModel);
        given(orderModel.getFluentId()).willReturn("123");
        fluentCancelOrderAction.executeAction(orderProcessModel);
    }

    @Test
    public void testExecuteActionWithFluentOnAndFluentIDBlank() throws RetryLaterException, Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderModel.getFluentId()).willReturn("");
        fluentCancelOrderAction.executeAction(orderProcessModel);
        verifyNoMoreInteractions(fluentOrderService);
    }
}
