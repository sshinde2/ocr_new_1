package au.com.target.tgtfluent.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckIfFluentOrderActionTest {


    @InjectMocks
    private final CheckIfFluentOrderAction checkIfFluentOrderAction = new CheckIfFluentOrderAction();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;



    @Test
    public void testExecuteActionWithNullOrder() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");

        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        given(orderProcessModel.getOrder()).willReturn(null);

        checkIfFluentOrderAction.execute(process);
    }


    @Test
    public void testExecuteActionWithFluentOrderReturnsTrue() throws Exception {
        given(process.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn("123");
        final String result = checkIfFluentOrderAction.execute(process);
        assertThat(result).isEqualTo("TRUE");
    }


    @Test
    public void testExecuteActionWithNonFluentOrderReturnsFalse() throws Exception {
        given(process.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn("");
        final String result = checkIfFluentOrderAction.execute(process);
        assertThat(result).isEqualTo("FALSE");
    }

    @Test
    public void testExecuteActionWithNullFluentIdReturnsFalse() throws Exception {
        given(process.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn(null);
        final String result = checkIfFluentOrderAction.execute(process);
        assertThat(result).isEqualTo("FALSE");
    }


}
