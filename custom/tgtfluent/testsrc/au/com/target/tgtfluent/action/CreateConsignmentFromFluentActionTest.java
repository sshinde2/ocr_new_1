/**
 * 
 */
package au.com.target.tgtfluent.action;



import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CreateConsignmentFromFluentActionTest {

    @InjectMocks
    private final CreateConsignmentFromFluentAction action = new CreateConsignmentFromFluentAction();

    @Mock
    private FluentFulfilmentService fluentFulfilmentService;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Before
    public void setup() {
        given(process.getOrder()).willReturn(order);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternal() throws RetryLaterException, Exception {
        doThrow(new FluentFulfilmentException("test error")).when(fluentFulfilmentService).createConsignmentsByOrderId(
                order);
        action.executeInternal(process);
    }

    @Test
    public void testExecuteInternalFailed() throws RetryLaterException, Exception {
        doThrow(new NullPointerException("test error")).when(fluentFulfilmentService).createConsignmentsByOrderId(
                order);
        final String result = action.executeInternal(process);
        assertThat(result).isEqualTo("NOK");
    }

    @Test
    public void testExecuteInternalSucceed() throws RetryLaterException, Exception {
        doNothing().when(fluentFulfilmentService).createConsignmentsByOrderId(
                order);
        final String result = action.executeInternal(process);
        assertThat(result).isEqualTo("OK");
    }
}
