/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySet;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtcore.orderEntry.impl.TargetOrderEntryServiceImpl;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.dao.FluentCarrierDao;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Article;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentConsignmentReversePopulatorTest {

    @InjectMocks
    @Spy
    private final FluentConsignmentReversePopulator fluentConsignmentReversePopulator = new FluentConsignmentReversePopulator();

    @Spy
    private final TargetOrderEntryService targetOrderEntryService = new TargetOrderEntryServiceImpl();

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private ModelService modelService;

    @Mock
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private AbstractOrderEntryModel orderEntryModel;

    @Mock
    private TargetSizeVariantProductModel productModel;

    private final String productCode = "P4025_blue_M";

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private ConsignmentEntryModel consignmentEntryModel;

    @Mock
    private AddressModel shippingAddress;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private VendorModel vendorModel;

    @Mock
    private ConsignmentStatus consignmentStatus;

    @Mock
    private FluentCarrierDao fluentCarrierDao;

    @Mock
    private DeliveryModeModel deliveryModeModel;

    @Mock
    private TargetCarrierModel targetCarrierModel;

    private final String orderId = "testOrderId";

    private final String fulfilmentId = "789456";

    private final String deliveryType = "STANDARD";

    private final String deliveryTypeExpress = "EXPRESS";


    private final String createdOn = "2018-04-23T23:40:50.763+0000";

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private final String status = "REJECTED";

    private final String fulfilmentType = "HD_PFDC";

    private final String fulfilmentTypeCnc = "CC_PFS";

    private final Long fulfilmentItemId = Long.valueOf(987654321l);

    private final String orderItemRef = "P4025_blue_M";

    private final Integer requestedQty = Integer.valueOf(1);

    private final Integer confirmedQty = Integer.valueOf(0);

    private final Integer filledQty = Integer.valueOf(0);

    private final Integer rejectedQty = Integer.valueOf(1);

    private final String skuRef = "P4025_blue_M";

    @Before
    public void setup() {
        fluentConsignmentReversePopulator.setTargetOrderEntryService(targetOrderEntryService);
        given(modelService.create(ConsignmentEntryModel.class)).willReturn(consignmentEntryModel);
        given(productModel.getCode()).willReturn(productCode);
        given(consignment.getOrder()).willReturn(orderModel);
        given(consignment.getStatus()).willReturn(consignmentStatus);
        given(consignmentStatus.getCode()).willReturn("REJECTED");
        given(consignment.getWarehouse()).willReturn(warehouseModel);
        given(warehouseModel.getVendor()).willReturn(vendorModel);
        given(vendorModel.getCode()).willReturn("Target");
        given(orderModel.getCode()).willReturn(orderId);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(orderEntryModel.getDeliveryAddress()).willReturn(shippingAddress);
        given(orderModel.getDeliveryAddress()).willReturn(shippingAddress);
        given(orderModel.getEntries()).willReturn(ImmutableList.of(orderEntryModel));
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.WEB);
        given(orderModel.getFluentId()).willReturn(orderId);
        given(consignment.getTargetCarrier()).willReturn(targetCarrierModel);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).hasOnlyDigitalDeliveryMode(productModel);
    }

    @Test
    public void testPopulateBasicCancellation() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verfy warehouse
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignment).setRejectReason(ConsignmentRejectReason.ZERO_PICK_BY_STORE);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.HOME_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentCancellation() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("1234"));

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verfy status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CANCELLED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.HOME_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentExpiredCancellation() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setStatus("EXPIRED");
        fulfilment.setFromAddress(createAddress("1234"));

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CANCELLED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.HOME_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
        // verify reason
        verify(consignment).setRejectReason(ConsignmentRejectReason.EXPIRED);
    }

    @Test
    public void testPopulateConsignmentFastlineHomeDelivery() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("fastlineWarehouse"));
        fulfilment.setStatus("CREATED");

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.HOME_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentFastlineDeliveryToStore() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("fastlineWarehouse"));
        fulfilment.setToAddress(createAddress("1234"));
        fulfilment.setStatus("CREATED");
        fulfilment.setFulfilmentType(fulfilmentTypeCnc);

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.CLICK_AND_COLLECT);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentFastlineExpressDelivery() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("fastlineWarehouse"));
        fulfilment.setToAddress(createAddress(""));
        fulfilment.setStatus("CREATED");
        fulfilment.setDeliveryType(deliveryTypeExpress);

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.EXPRESS_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentStoreHomeDelivery() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("1234"));
        fulfilment.setToAddress(createAddress(""));
        fulfilment.setStatus("CREATED");

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.HOME_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentStoreToStoreDelivery() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("1234"));
        fulfilment.setToAddress(createAddress("4567"));
        fulfilment.setStatus("CREATED");
        fulfilment.setFulfilmentType(fulfilmentTypeCnc);

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify warehouse
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.CLICK_AND_COLLECT);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentStoreToStoreDeliveryWithEbay() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.EBAY);
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("1234"));
        fulfilment.setToAddress(createAddress("4567"));
        fulfilment.setStatus("CREATED");
        fulfilment.setFulfilmentType(fulfilmentTypeCnc);

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.EBAY_CLICK_COLLECT);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentStoreHomeDeliveryWithEbay() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.EBAY);
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("1234"));
        fulfilment.setToAddress(createAddress(""));
        fulfilment.setStatus("CREATED");

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.EBAY_HOME_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentFastlineExpressDeliveryWithEbay() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.EBAY);
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setFromAddress(createAddress("fastlineWarehouse"));
        fulfilment.setToAddress(createAddress(""));
        fulfilment.setStatus("CREATED");
        fulfilment.setDeliveryType(deliveryTypeExpress);

        fluentConsignmentReversePopulator.populate(fulfilment, consignment);
        // verify status
        verify(consignment, never()).setCode(String.valueOf(fulfilmentId));
        verify(consignment, atLeastOnce()).setStatus(ConsignmentStatus.CREATED);
        // verify entries
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(modelService).create(ConsignmentEntryModel.class);
        verify(consignmentEntryModel).setConsignment(consignment);
        verify(consignmentEntryModel).setOrderEntry(orderEntryModel);
        verify(consignmentEntryModel).setQuantity(Long.valueOf(requestedQty.intValue()));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(filledQty.intValue()));
        // delivery mode
        verify(zoneDeliveryModeService).getDeliveryModeForCode(TgtFluentConstants.DeliveryMode.EBAY_EXPRESS_DELIVERY);
        // shipping address
        verify(targetOrderEntryService, atLeastOnce()).getOrderEntryByProductCode(orderModel, orderItemRef);
        verify(consignment).setShippingAddress(shippingAddress);
    }

    @Test
    public void testPopulateConsignmentEntriesForUpdate() {
        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();
        consignmentEntries.add(mock(ConsignmentEntryModel.class));
        willReturn(consignmentEntries).given(consignment).getConsignmentEntries();
        final Fulfilment fulfilment = mock(Fulfilment.class);
        final List<FulfilmentItem> items = mock(List.class);
        willReturn(items).given(fulfilment).getItems();
        willDoNothing().given(fluentConsignmentReversePopulator).updateConsignmentEntries(items, consignment,
                orderModel);

        fluentConsignmentReversePopulator.populateConsignmentEntries(items, consignment, orderModel);
        verify(consignment, never()).setConsignmentEntries(anySet());
    }

    @Test
    public void testPopulateShippingAddress() {
        willReturn(shippingAddress).given(consignment).getShippingAddress();
        final Fulfilment fulfilment = mock(Fulfilment.class);

        fluentConsignmentReversePopulator.populateShippingAddress(orderModel, fulfilment, consignment);
        verify(consignment, never()).setShippingAddress(any(AddressModel.class));
    }


    @Test
    public void testUpdateConsignmentEntries() {
        willReturn(Sets.newHashSet(consignmentEntryModel)).given(consignment).getConsignmentEntries();
        final FulfilmentItem fulfilmentItem1 = mock(FulfilmentItem.class);
        final FulfilmentItem fulfilmentItem2 = mock(FulfilmentItem.class);
        final List<FulfilmentItem> items = Arrays.asList(fulfilmentItem1, fulfilmentItem2);
        willDoNothing().given(fluentConsignmentReversePopulator).updateConsignmentEntry(any(FulfilmentItem.class),
                any(AbstractOrderModel.class), any(TargetConsignmentModel.class));

        fluentConsignmentReversePopulator.updateConsignmentEntries(items, consignment, orderModel);
        verify(fluentConsignmentReversePopulator).updateConsignmentEntry(fulfilmentItem1, orderModel, consignment);
        verify(fluentConsignmentReversePopulator).updateConsignmentEntry(fulfilmentItem2, orderModel, consignment);
    }

    @Test
    public void testUpdateConsignmentEntry() {
        willReturn(Sets.newHashSet(consignmentEntryModel)).given(consignment).getConsignmentEntries();
        final FulfilmentItem fulfilmentItem = mock(FulfilmentItem.class);
        willReturn(Integer.valueOf(3)).given(fulfilmentItem).getRequestedQty();
        willReturn(Integer.valueOf(1)).given(fulfilmentItem).getFilledQty();
        willReturn("productCode").given(fulfilmentItem).getOrderItemRef();
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        willReturn(orderEntry).given(targetOrderEntryService).getOrderEntryByProductCode(orderModel, "productCode");
        willReturn(orderEntry).given(consignmentEntryModel).getOrderEntry();

        fluentConsignmentReversePopulator.updateConsignmentEntry(fulfilmentItem, orderModel, consignment);

        verify(consignmentEntryModel, never()).setQuantity(any(Long.class));
        verify(consignmentEntryModel).setShippedQuantity(Long.valueOf(1));
        verify(modelService).save(consignmentEntryModel);
    }

    @Test
    public void testCarrierPopulationForCreatedStatus()
            throws ParseException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        fulfilment.setStatus("CREATED");
        this.setFulfilmentData(fulfilment);

        fluentConsignmentReversePopulator.populateCarrier(fulfilment, consignment);

        verify(fluentCarrierDao, never()).getTargetCarrierByCode(any(String.class));
        verify(consignment, never()).setTargetCarrier(any(TargetCarrierModel.class));
        verify(consignment, never()).setCarrier(any(String.class));
        verify(consignment, never()).setTrackingID(any(String.class));
        verify(consignment, never()).setParcelCount(any(Integer.class));
    }

    @Test
    public void testCarrierPopulationForTerminalStatus()
            throws ParseException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.CREATED);

        fluentConsignmentReversePopulator.populateCarrier(fulfilment, consignment);

        verify(fluentCarrierDao, never()).getTargetCarrierByCode(any(String.class));
        verify(consignment, never()).setTargetCarrier(any(TargetCarrierModel.class));
        verify(consignment, never()).setCarrier(any(String.class));
        verify(consignment, never()).setTrackingID(any(String.class));
        verify(consignment, never()).setParcelCount(any(Integer.class));
    }

    @Test
    public void testCarrierPopulationForSentToWarehouseStatus()
            throws ParseException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setCarrierName("AUSTRALIA_POST");
        fulfilment.setCarrierTrackingId("carrierTrackingId");
        fulfilment.setParcelCount(1);
        fulfilment.setStatus("AWAITING_WAVE");

        given(consignment.getTargetCarrier()).willReturn(null);
        given(consignment.getDeliveryMode()).willReturn(deliveryModeModel);
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        willReturn(targetCarrierModel).given(fluentCarrierDao).getTargetCarrierByCode("AustraliaPostInstoreHD");
        given(targetCarrierModel.getWarehouseCode()).willReturn("AustraliaPostInstoreHD");

        fluentConsignmentReversePopulator.populateCarrier(fulfilment, consignment);

        verify(consignment).setTargetCarrier(targetCarrierModel);
        verify(consignment).setCarrier("AustraliaPostInstoreHD");
        verify(consignment).setTrackingID("carrierTrackingId");
        verify(consignment).setParcelCount(Integer.valueOf(1));
    }

    @Test
    public void testCarrierPopulationForSentToWarehouseStatusForShipit()
            throws ParseException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setCarrierName("AUSTRALIA_POST");
        fulfilment.setParcelCount(1);
        fulfilment.setStatus("AWAITING_WAVE");
        fulfilment.setCarrierIntegrationTrackingId("shipitTrackingId");

        given(consignment.getTargetCarrier()).willReturn(null);
        given(consignment.getDeliveryMode()).willReturn(deliveryModeModel);
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        willReturn(targetCarrierModel).given(fluentCarrierDao).getTargetCarrierByCode("AustraliaPostInstoreHD");
        given(targetCarrierModel.getWarehouseCode()).willReturn("AustraliaPostInstoreHD");

        fluentConsignmentReversePopulator.populateCarrier(fulfilment, consignment);

        verify(consignment).setTargetCarrier(targetCarrierModel);
        verify(consignment).setCarrier("AustraliaPostInstoreHD");
        verify(consignment).setCarrierIntegratorTrackingId("shipitTrackingId");
        verify(consignment).setParcelCount(Integer.valueOf(1));
    }

    @Test
    public void testCarrierPopulationForSentToWarehouseStatusAndCarrierNameNull()
            throws ParseException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setCarrierName(null);
        fulfilment.setStatus("AWAITING_WAVE");

        fluentConsignmentReversePopulator.populateCarrier(fulfilment, consignment);

        verify(fluentCarrierDao, never()).getTargetCarrierByCode(any(String.class));
        verify(consignment, never()).setTargetCarrier(any(TargetCarrierModel.class));
        verify(consignment, never()).setCarrier(any(String.class));
        verify(consignment).setTrackingID(any(String.class));
        verify(consignment).setParcelCount(any(Integer.class));
    }

    @Test
    public void testCarrierPopulationForShipped()
            throws ParseException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setCarrierName("AUSTRALIA_POST");
        fulfilment.setCarrierTrackingId("carrierTrackingId");
        fulfilment.setParcelCount(1);
        fulfilment.setStatus("SHIPPED");

        fluentConsignmentReversePopulator.populateCarrier(fulfilment, consignment);

        verify(fluentCarrierDao, never()).getTargetCarrierByCode(any(String.class));
        verify(consignment, never()).setTargetCarrier(any(TargetCarrierModel.class));
        verify(consignment, never()).setCarrier(any(String.class));
        verify(consignment).setTrackingID("carrierTrackingId");
        verify(consignment).setParcelCount(Integer.valueOf(1));
    }

    @Test(expected = ConversionException.class)
    public void testConsignmentStatusDateForNullStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(null);
        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
    }

    @Test
    public void testPopulateConsignmentStatusDateForSentToWarehouseStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);

        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
        verify(consignment).setSentToWarehouseDate(any(Date.class));
    }

    @Test
    public void testPopulateConsignmentStatusDateForWavedStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.WAVED);

        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
        verify(consignment).setWavedDate(any(Date.class));
    }

    @Test
    public void testPopulateConsignmentStatusDateForPickedStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.PICKED);

        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
        verify(consignment).setPickConfirmDate(any(Date.class));
    }

    @Test
    public void testPopulateConsignmentStatusDateForPackedStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.PACKED);

        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
        verify(consignment).setPackedDate(any(Date.class));
    }

    @Test
    public void testPopulateConsignmentStatusDateForShippedStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.SHIPPED);

        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
        verify(consignment).setShippingDate(any(Date.class));
        verify(consignment).setShipConfReceived(Boolean.TRUE);
    }

    @Test
    public void testPopulateConsignmentStatusDateForCancelledStatus() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);

        fluentConsignmentReversePopulator.populateConsignmentStatusDate(fulfilment, consignment);
        verify(consignment).setCancelDate(any(Date.class));
    }

    @Test
    public void testPopulateRejectReasonForSystemExpired() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setStatus("SYSTEM_EXPIRED");

        fluentConsignmentReversePopulator.populateRejectReason(fulfilment, consignment);
        verify(consignment).setRejectReason(ConsignmentRejectReason.SYSTEM_EXPIRED);
    }

    @Test
    public void testPopulateRejectReasonForRejected() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setStatus("REJECTED");

        given(consignment.getWarehouse()).willReturn(warehouseModel);
        given(warehouseModel.getVendor()).willReturn(vendorModel);
        given(vendorModel.getCode()).willReturn("Fastline");

        fluentConsignmentReversePopulator.populateRejectReason(fulfilment, consignment);
        verify(consignment).setRejectReason(ConsignmentRejectReason.ZERO_PICK_BY_WAREHOUSE);
    }

    @Test
    public void testPopulateRejectReasonForSystemRejected() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setStatus("SYSTEM_REJECTED");

        fluentConsignmentReversePopulator.populateRejectReason(fulfilment, consignment);
        verify(consignment).setRejectReason(ConsignmentRejectReason.SYSTEM_REJECT);
    }

    @Test
    public void testPopulateRejectReasonForCancelled() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        this.setFulfilmentData(fulfilment);
        fulfilment.setStatus("CANCELLED_NO_STOCK");

        fluentConsignmentReversePopulator.populateRejectReason(fulfilment, consignment);
        verify(consignment).setRejectReason(ConsignmentRejectReason.UNIT_OUT_OF_STOCK_AT_LOCATION);
    }

    @Test
    public void testArticleCreationWhenArticleDoesNotExist() {
        given(consignment.getParcelsDetails()).willReturn(null);

        final ConsignmentParcelModel consignmentParcelModel = new ConsignmentParcelModel();
        given(modelService.create(ConsignmentParcelModel.class)).willReturn(consignmentParcelModel);

        final Fulfilment fulfilmentWithArticles = new Fulfilment();
        fulfilmentWithArticles.setArticles(Arrays.asList(createNewArticle("par1", "10", "20", "30", "40")));


        fluentConsignmentReversePopulator.populateArticle(fulfilmentWithArticles, consignment);
        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(modelService, times(1)).saveAll(modelSaveCaptor.capture());
        final List<List> savedArticlesWrapper = modelSaveCaptor.getAllValues();
        final List<ConsignmentParcelModel> savedArticles = savedArticlesWrapper.get(0);
        verifyConsignmentParcelResult(savedArticles.get(0), "par1", "10", "20",
                "30", "40");

    }

    private Article createNewArticle(final String articleId, final String weight, final String length,
            final String width, final String height) {
        final Article article = new Article();
        article.setArticleId(articleId);
        article.setWeight(weight);
        article.setLength(length);
        article.setWidth(width);
        article.setHeight(height);

        return article;
    }

    private void verifyConsignmentParcelResult(final ConsignmentParcelModel consignmentParcelModel,
            final String parcelId, final String weight, final String length,
            final String width, final String height) {
        assertThat(parcelId).isEqualTo(consignmentParcelModel.getParcelId());
        assertThat(Double.valueOf(weight)).isEqualTo(consignmentParcelModel.getActualWeight());
        assertThat(Double.valueOf(height)).isEqualTo(consignmentParcelModel.getHeight());
        assertThat(Double.valueOf(width)).isEqualTo(consignmentParcelModel.getWidth());
        assertThat(Double.valueOf(length)).isEqualTo(consignmentParcelModel.getLength());
    }

    @Test
    public void testSkipArticleCreationWhenArticleDoesExist() {
        final ConsignmentParcelModel consignmentParcelModel = new ConsignmentParcelModel();
        given(modelService.create(ConsignmentParcelModel.class)).willReturn(consignmentParcelModel);
        final ConsignmentParcelModel articlePresent1 = mock(ConsignmentParcelModel.class);
        given(articlePresent1.getParcelId()).willReturn("par1");
        final ConsignmentParcelModel articlePresent2 = mock(ConsignmentParcelModel.class);
        given(articlePresent2.getParcelId()).willReturn("par0");

        final HashSet<ConsignmentParcelModel> articles = new HashSet<>();
        articles.add(articlePresent1);
        articles.add(articlePresent2);

        given(consignment.getParcelsDetails()).willReturn(articles);

        final Fulfilment fulfilmentWithArticles = new Fulfilment();
        fulfilmentWithArticles.setArticles(Arrays.asList(createNewArticle("par1", "10", "20", "30", "40"),
                createNewArticle("par2", "110", "120", "130", "140"), createNewArticle("par0", "1", "2", "3", "4")));


        fluentConsignmentReversePopulator.populateArticle(fulfilmentWithArticles, consignment);
        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(modelService, times(1)).saveAll(modelSaveCaptor.capture());
        final List<List> savedArticlesWrapper = modelSaveCaptor.getAllValues();
        final List<ConsignmentParcelModel> savedArticles = savedArticlesWrapper.get(0);
        final ConsignmentParcelModel article1 = savedArticles.get(0);
        verifyConsignmentParcelResult(article1, "par2", "110", "120", "130", "140");

    }

    @Test
    public void testSkipArticleCreationWhenNoArticlesPresent() {
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        given(consignment.getParcelsDetails()).willReturn(null);
        final Fulfilment fulfilmentWithArticles = new Fulfilment();
        fulfilmentWithArticles.setArticles(null);
        fluentConsignmentReversePopulator.populateArticle(fulfilmentWithArticles, consignmentModel);
        verify(modelService, times(0)).saveAll(any(ConsignmentParcelModel.class));
    }


    private void setFulfilmentData(final Fulfilment fulfilment) throws ParseException {
        fulfilment.setOrderId(orderId);
        fulfilment.setFulfilmentId(fulfilmentId);
        fulfilment.setDeliveryType(deliveryType);
        fulfilment.setCreatedOn(sdf.parse(createdOn));
        fulfilment.setStatus(status);
        fulfilment.setFulfilmentType(fulfilmentType);
        fulfilment.setOccurredOn(new Date(1239876543L));
        fulfilment.setCarrierName("AUSTRALIA_POST");

        final FulfilmentItem item = new FulfilmentItem();
        item.setFulfilmentItemId(fulfilmentItemId);
        item.setOrderItemRef(orderItemRef);
        item.setRequestedQty(requestedQty);
        item.setConfirmedQty(confirmedQty);
        item.setFilledQty(filledQty);
        item.setRejectedQty(rejectedQty);
        item.setSkuRef(skuRef);
        fulfilment.setItems(ImmutableList.of(item));
    }

    private Address createAddress(final String locationRef) {
        final Address address = new Address();
        address.setLocationRef(locationRef);
        return address;
    }
}
