package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.exception.CategoryFluentFeedException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryPopulatorTest {

    @Mock
    private TargetProductCategoryModel rootCategory;

    @Mock
    private TargetProductCategoryModel subCategory;

    @Mock
    private TargetMerchDepartmentModel department;

    private final CategoryPopulator categoryPopulator = new CategoryPopulator();

    @Before
    public void setUp() throws Exception {
        given(rootCategory.getCode()).willReturn("Cat0");
        given(rootCategory.getName()).willReturn("Top Level");
        given(rootCategory.getFluentId()).willReturn("1");
        given(rootCategory.getSupercategories()).willReturn(null);

        given(subCategory.getCode()).willReturn("Cat1");
        given(subCategory.getName()).willReturn("Mens");
        given(subCategory.getFluentId()).willReturn("2");

        final List<CategoryModel> categoryList = Arrays.asList(rootCategory, department);
        given(subCategory.getSupercategories()).willReturn(categoryList);
    }


    @Test
    public void testPopulateRootCategory() {

        final Category target = new Category();
        categoryPopulator.populate(rootCategory, target);

        assertThat(target.getCategoryRef()).isEqualTo("Cat0");
        assertThat(target.getName()).isEqualTo("Top Level");
        assertThat(target.getId()).isEqualTo("1");
        assertThat(target.getParentCategoryId()).isNull();
    }


    @Test
    public void testPopulateSubCategory() {

        final Category target = new Category();
        categoryPopulator.populate(subCategory, target);
        assertThat(target.getCategoryRef()).isEqualTo("Cat1");
        assertThat(target.getName()).isEqualTo("Mens");
        assertThat(target.getId()).isEqualTo("2");
        assertThat(target.getParentCategoryId()).isEqualTo("1");
    }


    @Test(expected = CategoryFluentFeedException.class)
    public void testPopulateSubCategoryWithParentFluentIdNull() {
        given(rootCategory.getFluentId()).willReturn(null);
        final Category target = new Category();
        categoryPopulator.populate(subCategory, target);
    }

}
