/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;

import java.util.Locale;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtcore.model.AddressModificationRecordModel;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Locale.class })
public class TgtCsOrderModificationRecordDiscriptionColumnTest {

    private final TgtCsOrderModificationRecordDiscriptionColumn column = new TgtCsOrderModificationRecordDiscriptionColumn();

    @Mock
    private Locale locale;

    @Before
    public void setup() {
        PowerMockito.mockStatic(Locale.class, Mockito.RETURNS_MOCKS);
    }

    @Test
    public void testItemValueWithNoModel() throws ValueHandlerException {
        Assert.assertEquals(StringUtils.EMPTY, column.getItemValue(null, locale));
    }

    @Test
    public void testItemValueForRefund() throws ValueHandlerException {
        final OrderReturnRecordModel model = Mockito.mock(OrderReturnRecordModel.class);
        Assert.assertEquals("Refund", column.getItemValue(model, locale));
    }

    @Test
    public void testItemValueForCancel() throws ValueHandlerException {
        final OrderCancelRecordModel model = Mockito.mock(OrderCancelRecordModel.class);
        Assert.assertEquals("Cancel", column.getItemValue(model, locale));
    }

    @Test
    public void testItemValueForAddressChange() throws ValueHandlerException {
        final AddressModificationRecordModel model = Mockito.mock(AddressModificationRecordModel.class);
        Assert.assertEquals("Address change", column.getItemValue(model, locale));
    }
}
