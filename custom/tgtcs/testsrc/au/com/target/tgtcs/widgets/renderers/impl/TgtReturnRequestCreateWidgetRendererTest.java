/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.BaseType;
import de.hybris.platform.cockpit.model.meta.ObjectTemplate;
import de.hybris.platform.cockpit.model.meta.ObjectType;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.SystemService;
import de.hybris.platform.cockpit.services.config.ColumnConfiguration;
import de.hybris.platform.cockpit.services.config.EditorConfiguration;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.config.EditorSectionConfiguration;
import de.hybris.platform.cockpit.services.config.impl.PropertyColumnConfiguration;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.cockpit.session.UIBrowserArea;
import de.hybris.platform.cockpit.session.UICockpitPerspective;
import de.hybris.platform.cockpit.session.UISession;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.ListWidgetModel;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.CockpitUiConfigLoader;
import de.hybris.platform.cscockpit.utils.PropertyEditorHelper;
import de.hybris.platform.cscockpit.utils.PropertyRendererHelper;
import de.hybris.platform.cscockpit.widgets.controllers.ReturnsController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.ReturnRequestCreateWidgetRenderer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.components.orderdetail.controllers.AddPaymentWindowController;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.impl.TargetReturnsControllerImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author Nandini
 * 
 */


@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, UISessionUtils.class, CockpitUiConfigLoader.class, Messagebox.class })
public class TgtReturnRequestCreateWidgetRendererTest {

    private static final String EXPECTED_QTY_PROPERTY_QUALIFIER = "ReturnEntry.expectedQuantity";

    @InjectMocks
    @Spy
    private final TgtReturnRequestCreateWidgetRenderer returnRequest = new TgtReturnRequestCreateWidgetRenderer();

    @Mock
    private HtmlBasedComponent rootContainer;

    @Mock
    private DefaultListWidgetModel defaultListWidgetModel;

    @Mock
    private ReturnsController returnsController;

    @Mock
    private InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> inputWidget;

    @Mock
    private List<ObjectValueContainer> objectValue;

    @Mock
    private Map<TypedObject, Long> returnableOrderEntries;

    @Mock
    private ListWidgetModel<?> listWidget;

    @Mock
    private PropertyDescriptor propertyDescriptor;

    @Mock
    private Event event;

    @Mock
    private TargetReturnsControllerImpl targetController;

    @Mock
    private ObjectValueContainer container;

    @Mock
    private Set<TypedObject> typedObjects;
    @Mock
    private TypedObject typedObject;
    @Mock
    private TypeService typeService;

    @Mock
    private ObjectType objectType;

    @Mock
    private ObjectTemplate objectTemplate;

    @Mock
    private UISession uiSession;

    @Mock
    private UICockpitPerspective uiCockpitPerspective;

    @Mock
    private UIBrowserArea uiBrowserArea;

    @Mock
    private ReturnRequestCreateWidgetRenderer returnRequestCreateWidgetRenderer;

    @Mock
    private PropertyEditorHelper propertyEditorHelper;

    @Mock
    private EditorConfiguration editorConfiguration;

    @Mock
    private EditorRowConfiguration editorRowConfiguration;

    @Mock
    private AbstractOrderEntryModel orderEntryModel;
    @Mock
    private OrderModel orderModel;

    @Mock
    private BaseType baseType;

    @Mock
    private PropertyRendererHelper propertyRendererHelper;

    @Mock
    private SystemService systemService;

    @Mock
    private PropertyColumnConfiguration propertyColumnConfiguration;

    @Mock
    private List<ObjectValueContainer> returnObjectValueContainers;

    @Mock
    private ObjectValueHolder objectValueHolder;

    @Mock
    private EditorSectionConfiguration editorSectionConfiguration;
    @Mock
    private TargetReturnsControllerImpl targetReturnsControllerImpl;
    @Mock
    private AddPaymentWindowController addPaymentWindowController;
    @Mock
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    @Mock
    private Window window;
    @Mock
    private Executions executions;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        PowerMockito.mockStatic(UISessionUtils.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(CockpitUiConfigLoader.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(CollectionUtils.class, Mockito.RETURNS_MOCKS);
        returnRequest.setListConfigurationCode("listconfig");
        returnRequest.setListConfigurationType("configtype");

        given(UISessionUtils.getCurrentSession()).willReturn(uiSession);
        given(uiSession.getCurrentPerspective()).willReturn(uiCockpitPerspective);
        given(uiSession.isUsingTestIDs()).willReturn(true);
        given(uiCockpitPerspective.getBrowserArea()).willReturn(uiBrowserArea);
        final List<EditorRowConfiguration> editorRowConfigurations = new ArrayList<>();
        editorRowConfigurations.add(editorRowConfiguration);

        final List<EditorSectionConfiguration> sectionConfigs = new ArrayList<>();
        sectionConfigs.add(editorSectionConfiguration);


        given(editorSectionConfiguration.getSectionRows()).willReturn(editorRowConfigurations);
        given(editorConfiguration.getSections()).willReturn(sectionConfigs);

        given(CockpitUiConfigLoader.getEditorConfiguration(uiSession, "listconfig", "configtype")).willReturn(
                editorConfiguration);
        returnRequest.setPropertyEditorHelper(propertyEditorHelper);
        returnRequest.setPropertyRendererHelper(propertyRendererHelper);
        returnRequest.setAddPaymentWindowController(addPaymentWindowController);
        returnRequest.setTgtCsCardPaymentFacade(tgtCsCardPaymentFacade);
    }

    @Test
    public void testCreateInternalNullContainer() {
        final HtmlBasedComponent component = returnRequest.createContentInternal(inputWidget,
                null);
        Assert.assertNotNull(component);
        Assert.assertEquals(1, component.getChildren().size());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateInternalValid() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        given(inputWidget.getWidgetController()).willReturn(targetReturnsControllerImpl);
        given(inputWidget.getWidgetController().canReturn()).willReturn(Boolean.TRUE);
        given(targetReturnsControllerImpl.getCurrentOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(order);
        final HtmlBasedComponent component = returnRequest.createContentInternal(inputWidget,
                rootContainer);
        Mockito.verify(tgtCsCardPaymentFacade).clearOldPayment();
        Assert.assertNotNull(component);
        Assert.assertEquals(2, component.getChildren().size());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateInternalWithPaymentButton() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        given(inputWidget.getWidgetController()).willReturn(targetReturnsControllerImpl);
        given(inputWidget.getWidgetController().canReturn()).willReturn(Boolean.TRUE);
        given(targetReturnsControllerImpl.getCurrentOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(order);
        final HtmlBasedComponent component = returnRequest.createContentInternal(inputWidget,
                rootContainer);
        Mockito.verify(tgtCsCardPaymentFacade).clearOldPayment();
        final Div div = (Div)component.getChildren().get(1);

        Assert.assertNotNull(component);
        Assert.assertEquals(2, div.getChildren().size());
    }

    @Test(expected = Exception.class)
    public void testHandleReturnRequestCreateEvent() throws Exception {
        given(inputWidget.getWidgetModel()).willReturn(defaultListWidgetModel);
        given(inputWidget.getWidgetController()).willReturn(targetController);
        returnRequest.handleReturnRequestCreateEvent(inputWidget, event, objectValue);
        Mockito.verify(targetController).setAdditionalRequestParams(
                container);
        Mockito.verify(returnRequest).handleReturnRequestCreateEvent(inputWidget, event, objectValue);
    }

    @Test
    public void testRenderListbox() {
        final Listbox listBox = setUpListBoxData();
        returnRequest.renderListbox(listBox, inputWidget);

        Assert.assertNotNull(defaultListWidgetModel);
        Assert.assertNotNull(returnableOrderEntries);
        Mockito.verify(returnsController).getReturnableOrderEntries();
    }

    protected Listbox setUpListBoxData() {
        returnRequest.setListConfigurationCode("listconfig");
        returnRequest.setListConfigurationType("configtype");
        returnRequest.setProductInfoConfigurationCode("productInfoConfigurationCode");
        final Set<TypedObject> orderEntries = new HashSet<>();
        orderEntries.add(typedObject);
        final Set<String> langISO = new HashSet<>();
        langISO.add("AUS");
        final List<ColumnConfiguration> columnConfigurations = new ArrayList<>();
        columnConfigurations.add(propertyColumnConfiguration);
        final Set<PropertyDescriptor> propertyDescriptors = new HashSet<>();
        propertyDescriptors.add(propertyDescriptor);
        final Listbox listBox = new Listbox();
        given(inputWidget.getWidgetModel()).willReturn(defaultListWidgetModel);
        given(inputWidget.getWidgetController()).willReturn(returnsController);
        given(typeService.getObjectTemplate("objectTemplate")).willReturn(objectTemplate);
        given(inputWidget.getWidgetController().getReturnableOrderEntries()).willReturn(returnableOrderEntries);
        given(CockpitUiConfigLoader.getAllVisibleColumnConfigurations(uiSession, "listconfig", "configtype"))
                .willReturn(columnConfigurations);
        given(returnableOrderEntries.keySet()).willReturn(orderEntries);
        given(typedObject.getObject()).willReturn(orderEntryModel);
        given(orderEntryModel.getEntryNumber()).willReturn(Integer.valueOf(1));
        given(typedObject.getType()).willReturn(baseType);
        given(baseType.getCode()).willReturn("configtype");
        given(typeService.getObjectType("configtype")).willReturn(objectType);
        given(objectType.getPropertyDescriptors()).willReturn(propertyDescriptors);
        given(systemService.getAvailableLanguageIsos()).willReturn(langISO);
        given(propertyColumnConfiguration.getPropertyDescriptor()).willReturn(propertyDescriptor);
        given(propertyDescriptor.getQualifier()).willReturn(EXPECTED_QTY_PROPERTY_QUALIFIER);
        given(container.getValue(propertyDescriptor, null)).willReturn(objectValueHolder);

        given(editorRowConfiguration.getPropertyDescriptor()).willReturn(propertyDescriptor);
        given(Boolean.valueOf(returnsController.canReturn())).willReturn(Boolean.TRUE);
        return listBox;
    }

    @Test
    public void testRenderListboxWhenCannotReturn() {
        final Listbox listBox = setUpListBoxData();
        given(inputWidget.getWidgetController()).willReturn(returnsController);
        given(Boolean.valueOf(returnsController.canReturn())).willReturn(Boolean.FALSE);
        returnRequest.renderListbox(listBox, inputWidget);

        Assert.assertNotNull(defaultListWidgetModel);
        Assert.assertNotNull(returnableOrderEntries);
        Mockito.verify(returnsController, Mockito.times(0)).getReturnableOrderEntries();
    }

    @Test
    public void testRenderListboxWhenZeroEntries() {
        final Listbox listBox = setUpListBoxData();
        given(inputWidget.getWidgetController()).willReturn(returnsController);
        given(returnsController.getReturnableOrderEntries()).willReturn(null);
        returnRequest.renderListbox(listBox, inputWidget);

        Assert.assertNotNull(defaultListWidgetModel);
        Assert.assertNotNull(returnableOrderEntries);
        Mockito.verify(returnsController, Mockito.times(1)).getReturnableOrderEntries();
    }

    @Test
    public void testIsExpectedQtyPropertyDescriptorWithNull() {
        Assert.assertFalse(returnRequest.isExpectedQtyPropertyDescriptor(null));
    }

    @Test
    public void testIsExpectedQtyPropertyDescriptor() {
        given(propertyDescriptor.getQualifier()).willReturn(EXPECTED_QTY_PROPERTY_QUALIFIER);

        Assert.assertTrue(returnRequest.isExpectedQtyPropertyDescriptor(propertyDescriptor));
    }

    @Test
    public void testPrepareFixedColums() {
        final Listhead listHead = new Listhead();
        returnRequest.prepareFixedColums(inputWidget, listHead);

        Mockito.verify(inputWidget, Mockito.times(9)).getWidgetTitle();
    }

    @Test
    public void testFillFixedColumns() {
        given(typedObject.getObject()).willReturn(orderEntryModel);
        given(orderEntryModel.getEntryNumber()).willReturn(Integer.valueOf(1));
        given(typedObject.getType()).willReturn(baseType);
        given(baseType.getCode()).willReturn("configtype");
        final Listitem row = new Listitem();
        returnRequest.fillFixedColumns(returnableOrderEntries, typedObject, row);

        Assert.assertEquals(row.getChildren().size(), 5);
    }

    @Test
    public void testHandleRefundPaymentPopupeEventWidgetNull() throws InterruptedException {
        returnRequest.handleRefundPaymentPopupeEvent(null);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateRefundPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupeEventWidgetDifferentController() throws InterruptedException {
        given(inputWidget.getWidgetController()).willReturn(returnsController);
        returnRequest.handleRefundPaymentPopupeEvent(inputWidget);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateRefundPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupeEventWidgetNoTypedObject() throws InterruptedException {
        given(inputWidget.getWidgetController()).willReturn(targetReturnsControllerImpl);
        given(targetReturnsControllerImpl.getCurrentOrder()).willReturn(null);
        returnRequest.handleRefundPaymentPopupeEvent(inputWidget);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateRefundPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupeEventWidgetWithNoOrder() throws InterruptedException {
        given(inputWidget.getWidgetController()).willReturn(targetReturnsControllerImpl);
        given(targetReturnsControllerImpl.getCurrentOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(null);
        returnRequest.handleRefundPaymentPopupeEvent(inputWidget);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateRefundPaymentForm(orderModel,
                window);
    }

    @Test(expected = NullPointerException.class)
    public void testHandleRefundPaymentPopupWindowNull() throws InterruptedException {
        given(inputWidget.getWidgetController()).willReturn(targetReturnsControllerImpl);
        given(targetReturnsControllerImpl.getCurrentOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(orderModel);
        given(returnRequest.getPaymentWindow(orderModel)).willReturn(window);
        Mockito.doNothing().when(addPaymentWindowController).populateRefundPaymentForm(orderModel, window);
        returnRequest.handleRefundPaymentPopupeEvent(inputWidget);
        Mockito.verify(window, Mockito.times(1)).doModal();
        Assert.fail();
    }
}
