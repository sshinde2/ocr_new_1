/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.label.LabelService;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.models.impl.OrderItemWidgetModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.FormatFactory;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.DiscountValue;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderDetailsOrderTotalsWidgetRendererTest {

    @InjectMocks
    private final TargetOrderDetailsOrderTotalsWidgetRenderer renderer = new TargetOrderDetailsOrderTotalsWidgetRenderer();

    @Mock
    private TargetVoucherService targetVoucherService;

    @Mock
    private Widget<OrderItemWidgetModel, OrderController> widget;

    @Mock
    private TypedObject typedObject;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private TypeService typeService;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private FormatFactory formatFactory;

    @Mock
    private LabelService labelService;

    @Mock
    private SessionService sessionService;


    @Before
    public void setup()
    {
        final NumberFormat currencyInstance = new DecimalFormat();
        final Currency currency = Currency.getInstance("AUD");
        currencyInstance.setCurrency(currency);

        when(typedObject.getObject()).thenReturn(order);
        when(sessionService.executeInLocalView(Mockito.any(SessionExecutionBody.class))).thenReturn(currencyInstance);
    }

    @Test
    public void testRenderOrderDetailWhenOrderIsNull()
    {
        when(typedObject.getObject()).thenReturn(null);
        renderer.renderOrderDetail(widget, typedObject, null);
    }

    @Test
    public void testRenderOrderDetailWithVoucher()
    {
        when(targetVoucherService.getFlybuysDiscountForOrder(order)).thenReturn(null);
        renderer.renderOrderDetail(widget, typedObject, null);
    }

    @Test
    public void testRenderOrderDetailWithFlybuys()
    {
        final FlybuysDiscountModel flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);
        when(targetVoucherService.getFlybuysDiscountForOrder(order)).thenReturn(flybuysDiscountModel);
        when(order.getGlobalDiscountValues()).thenReturn(getGlobalDiscounts());

        renderer.renderOrderDetail(widget, typedObject, null);
    }

    private List<DiscountValue> getGlobalDiscounts() {
        final List<DiscountValue> discountValues = new ArrayList<>();
        final DiscountValue value = new DiscountValue("", 10d, true, 10d, "AUD");
        discountValues.add(value);
        return discountValues;
    }

    @Test
    public void testFlybuysPointsConsumedWithNoFlybuys() {
        when(order.getFlybuysPointsConsumed()).thenReturn(null);

        assertFalse(renderer.isFlybuysPointsConsumed(order));
    }

    @Test
    public void testFlybuysPointsConsumedWithZeroFlybuys() {
        when(order.getFlybuysPointsConsumed()).thenReturn(Integer.valueOf(0));

        assertFalse(renderer.isFlybuysPointsConsumed(order));
    }

    @Test
    public void testFlybuysPointsConsumedWithSomeFlybuys() {
        when(order.getFlybuysPointsConsumed()).thenReturn(Integer.valueOf(10));

        assertTrue(renderer.isFlybuysPointsConsumed(order));
    }
}
