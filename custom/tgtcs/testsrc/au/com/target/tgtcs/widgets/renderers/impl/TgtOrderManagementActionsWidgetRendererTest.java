/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UIBrowserArea;
import de.hybris.platform.cockpit.session.UICockpitPerspective;
import de.hybris.platform.cockpit.session.UISession;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultItemWidgetModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;
import de.hybris.platform.ordercancel.CancelDecision;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.ApplicationContext;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Messagebox;

import com.google.common.collect.ImmutableSet;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.refund.TargetRefundService;



/**
 * Unit Test for {@link TgtOrderManagementActionsWidgetRenderer}
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, UISessionUtils.class, Messagebox.class })
public class TgtOrderManagementActionsWidgetRendererTest {

    private static final String ORDER_CANCEL_SERVICE = "orderCancelService";
    private static final String USER_SERVICE_BEAN_NAME = "userService";
    private static final String RETURN_SERVICE = "returnService";
    private static final String REFUND_SERVICE = "refundService";

    @Mock
    private Widget<DefaultItemWidgetModel, OrderManagementActionsWidgetController> widget;

    @Mock
    private HtmlBasedComponent rootContainer;
    @Mock
    private OrderManagementActionsWidgetController widgetController;
    @Mock
    private UserService userService;
    @Mock
    private ApplicationContext ctx;
    @Mock
    private ReturnService returnService;
    @Mock
    private OrderCancelService orderCancelService;
    @Mock
    private UISession uiSession;
    @Mock
    private UICockpitPerspective uiCockpitPerspective;
    @Mock
    private UIBrowserArea uiBrowserArea;
    @Mock
    private OrderModel order;
    @Mock
    private TypedObject typedObject;
    @Mock
    private UserModel user;
    @Mock
    private Map returnable;
    @Mock
    private CancelDecision cancelable;
    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TgtOrderManagementActionsWidgetRenderer renderer;

    @Mock
    private TargetRefundService targetRefundService;

    @Before
    public void setUp() {

        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(UISessionUtils.class, Mockito.RETURNS_MOCKS);
        given(Registry.getApplicationContext()).willReturn(ctx);
        given(ctx.getBean(USER_SERVICE_BEAN_NAME)).willReturn(userService);
        given(ctx.getBean(ORDER_CANCEL_SERVICE)).willReturn(orderCancelService);
        given(ctx.getBean(RETURN_SERVICE)).willReturn(returnService);
        given(ctx.getBean(REFUND_SERVICE)).willReturn(targetRefundService);
        given(UISessionUtils.getCurrentSession()).willReturn(uiSession);
        given(uiSession.getCurrentPerspective()).willReturn(uiCockpitPerspective);
        willReturn(Boolean.TRUE).given(uiSession).isUsingTestIDs();
        given(uiCockpitPerspective.getBrowserArea()).willReturn(uiBrowserArea);
        given(widget.getWidgetController()).willReturn(widgetController);
        given(widgetController.getOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(order);
        given(userService.getCurrentUser()).willReturn(user);
        renderer = new TgtOrderManagementActionsWidgetRenderer();
        renderer.setTargetFeatureSwitchService(targetFeatureSwitchService);
        final List<ConsignmentStatus> cancelDeniedConsignmentStates = Collections
                .singletonList(ConsignmentStatus.SHIPPED);
        renderer.setCancelDeniedConsignmentStates(cancelDeniedConsignmentStates);
    }

    @Test
    public void testCreateContentInternal() {
        final HtmlBasedComponent component = renderer.createContentInternal(widget, rootContainer);
        assertThat(component).isNotNull();
        verify(returnService).getAllReturnableEntries(order);
        verify(orderCancelService, never()).isCancelPossible(order, user, true, true);
        verify(orderCancelService).isCancelPossible(order, user, false, false);
    }

    @Test
    public void testCreateContentInternalWithFeatureOn() {
        given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CSCOCKPIT_CANCEL_BUTTON))).willReturn(
                                Boolean.TRUE);
        final HtmlBasedComponent component = renderer.createContentInternal(widget, rootContainer);
        assertThat(component).isNotNull();
        verify(returnService).getAllReturnableEntries(order);
        verify(orderCancelService).isCancelPossible(order, user, true, true);
        verify(orderCancelService).isCancelPossible(order, user, false, false);
    }

    @Test
    public void testIsRefundAllowedWithNullOrder() {
        assertThat(renderer.isRefundAllowed(null)).isFalse();
    }

    @Test
    public void testIsRefundAllowedWithNullOrderEntries() {
        given(returnService.getAllReturnableEntries(order)).willReturn(null);
        assertThat(renderer.isRefundAllowed(order)).isFalse();
    }

    @Test
    public void testIsRefundAllowedWithEmptyOrderEntries() {
        given(returnService.getAllReturnableEntries(order)).willReturn(returnable);
        willReturn(Boolean.TRUE).given(returnable).isEmpty();
        assertThat(renderer.isRefundAllowed(order)).isFalse();
    }

    @Test
    public void testIsRefundAllowed() {
        given(returnService.getAllReturnableEntries(order)).willReturn(returnable);
        willReturn(Boolean.FALSE).given(returnable).isEmpty();
        assertThat(renderer.isRefundAllowed(order)).isTrue();
    }

    @Test
    public void testIsPartialCancelPossibleWithOrderNull() {
        assertThat(renderer.isPartialCancelPossible(null)).isFalse();
    }

    @Test
    public void testIsPartialCancelPossibleWithOrderVersionId() {
        given(order.getVersionID()).willReturn("V1234");
        assertThat(renderer.isPartialCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsPartialCancelPossibleWithCancelDecisionNull() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(null);
        assertThat(renderer.isPartialCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsPartialCancelPossibleWithCancelAllowedFalse() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(cancelable);
        willReturn(Boolean.FALSE).given(cancelable).isAllowed();
        assertThat(renderer.isPartialCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsPartialCancelPossibleWithCancelAllowedTrue() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(cancelable);
        willReturn(Boolean.TRUE).given(cancelable).isAllowed();
        assertThat(renderer.isPartialCancelPossible(order)).isTrue();
    }

    @Test
    public void testIsFullCancelPossibleWithOrderNull() {
        assertThat(renderer.isPartialCancelPossible(null)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleWithOrderVersionId() {
        given(order.getVersionID()).willReturn("V1234");
        assertThat(renderer.isPartialCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleWithCancelDecisionNull() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        given(orderCancelService.isCancelPossible(order, user, false, false)).willReturn(null);
        assertThat(renderer.isPartialCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleWithCancelAllowedFalse() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        given(orderCancelService.isCancelPossible(order, user, false, false)).willReturn(cancelable);
        willReturn(Boolean.FALSE).given(cancelable).isAllowed();
        assertThat(renderer.isPartialCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleWithCancelAllowedTrue() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(cancelable);
        willReturn(Boolean.TRUE).given(cancelable).isAllowed();
        assertThat(renderer.isPartialCancelPossible(order)).isTrue();
    }

    @Test
    public void testIsRefundAllowedForDeliveryCost() {
        given(Boolean.valueOf(targetRefundService.isDeliveryCostRefundable(order))).willReturn(Boolean.TRUE);
        assertThat(renderer.isRefundAllowed(order)).isTrue();
    }

    @Test
    public void testIsFullCancelPossibleWithCancelAllowedTrueFeatureSwitchOn() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CSCOCKPIT_CANCEL_BUTTON);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(cancelable);
        willReturn(Boolean.TRUE).given(cancelable).isAllowed();
        assertThat(renderer.isFullCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleFluentOnWithGiftCard() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        final ConsignmentEntryModel conEntryModel = mock(ConsignmentEntryModel.class);
        final TargetProductModel productModel = mock(TargetProductModel.class);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        final ProductTypeModel productTypeModel = mock(ProductTypeModel.class);
        final ConsignmentModel conModel = mock(ConsignmentModel.class);
        given(productTypeModel.getCode()).willReturn("digital");
        given(productModel.getProductType()).willReturn(productTypeModel);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(conEntryModel.getOrderEntry()).willReturn(orderEntryModel);
        given(conModel.getConsignmentEntries()).willReturn(ImmutableSet.of(conEntryModel));
        given(conModel.getOrder()).willReturn(order);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(cancelable);
        willReturn(Boolean.TRUE).given(cancelable).isAllowed();
        assertThat(renderer.isFullCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleFluentOnWithPickedConsignment() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        final ConsignmentEntryModel conEntryModel = mock(ConsignmentEntryModel.class);
        final TargetProductModel productModel = mock(TargetProductModel.class);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        final ProductTypeModel productTypeModel = mock(ProductTypeModel.class);
        final ConsignmentModel conModel = mock(ConsignmentModel.class);
        given(conModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(productTypeModel.getCode()).willReturn("normal");
        given(productModel.getProductType()).willReturn(productTypeModel);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(conEntryModel.getOrderEntry()).willReturn(orderEntryModel);
        given(conModel.getConsignmentEntries()).willReturn(ImmutableSet.of(conEntryModel));
        given(conModel.getOrder()).willReturn(order);
        given(order.getConsignments()).willReturn(ImmutableSet.of(conModel));
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderCancelService.isCancelPossible(order, user, true, true)).willReturn(cancelable);
        willReturn(Boolean.TRUE).given(cancelable).isAllowed();
        assertThat(renderer.isFullCancelPossible(order)).isFalse();
    }

    @Test
    public void testIsFullCancelPossibleFluentOnWithOpenConsignment() {
        given(order.getVersionID()).willReturn(StringUtils.EMPTY);
        final ConsignmentEntryModel conEntryModel = mock(ConsignmentEntryModel.class);
        final TargetProductModel productModel = mock(TargetProductModel.class);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        final ProductTypeModel productTypeModel = mock(ProductTypeModel.class);
        final ConsignmentModel conModel = mock(ConsignmentModel.class);
        given(conModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(productTypeModel.getCode()).willReturn("normal");
        given(productModel.getProductType()).willReturn(productTypeModel);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(conEntryModel.getOrderEntry()).willReturn(orderEntryModel);
        given(conModel.getConsignmentEntries()).willReturn(ImmutableSet.of(conEntryModel));
        given(conModel.getOrder()).willReturn(order);
        given(order.getConsignments()).willReturn(ImmutableSet.of(conModel));
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderCancelService.isCancelPossible(order, user, false, false)).willReturn(cancelable);
        willReturn(Boolean.TRUE).given(cancelable).isAllowed();
        assertThat(renderer.isFullCancelPossible(order)).isTrue();
    }
}
