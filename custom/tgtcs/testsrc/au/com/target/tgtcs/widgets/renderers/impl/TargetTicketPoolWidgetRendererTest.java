/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.core.Registry;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.widgets.controllers.search.SearchCommandController;
import de.hybris.platform.cscockpit.widgets.models.impl.TextSearchWidgetModel;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zhtml.Span;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;

import au.com.target.tgtcs.csagent.service.TargetCsAgentGroupService;




/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, UISessionUtils.class, Listbox.class })
public class TargetTicketPoolWidgetRendererTest {

    @Mock
    private TargetCsAgentGroupService targetCsAgentGroupService;

    @Mock
    private ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget;
    @Mock
    private TextSearchWidgetModel widgetModel;

    @Mock
    private SearchCommandController<DefaultCsTextSearchCommand> controller;

    @Mock
    private DefaultCsTextSearchCommand searchCommand;

    @InjectMocks
    private final TargetTicketPoolWidgetRenderer renderer = new TargetTicketPoolWidgetRenderer();

    @Before
    public void setup() {

        widget.setWidgetModel(widgetModel);
        Mockito.when(widget.getWidgetModel()).thenReturn(widgetModel);
        Mockito.when(widget.getWidgetController()).thenReturn(controller);
        Mockito.when(widgetModel.getSearchCommand()).thenReturn(searchCommand);
        Mockito.when(controller.getDefaultSearchCommand()).thenReturn(searchCommand);
        PowerMockito.mockStatic(UISessionUtils.class, Mockito.RETURNS_MOCKS);

    }

    @Test
    public void testSetSelectedIndexForCustomGroup() {
        final Listbox box = new Listbox();
        box.appendItem("test", "test");
        box.appendItem("test1", "test1");
        renderer.setSelectedIndexForCustomGroup(box, "test1");
        Assert.assertEquals(box.getSelectedIndex(), 1);
    }

    @Test
    public void testSetSelectedIndexForCustomGroup1() {
        final Listbox box = new Listbox();
        box.appendItem("testaa", "testaa");
        box.appendItem("test1", "test1");
        renderer.setSelectedIndexForCustomGroup(box, "testaa");
        Assert.assertEquals(box.getSelectedIndex(), 0);
    }

    @Test
    public void testSetSelectedIndexForCustomGroup2() {
        final Listbox box = new Listbox();
        box.appendItem("Hello", "Hi");
        box.appendItem("test1", "test1");
        renderer.setSelectedIndexForCustomGroup(box, "testaa");
        Assert.assertEquals(box.getSelectedIndex(), -1);
    }

    @Ignore
    @Test
    public void testCreateSearchPane() {
        final HtmlBasedComponent component = renderer.createSearchPane(widget);
        Assert.assertNotNull(component);
        Assert.assertEquals(component.getChildren().size(), 3);
        final Span agentSpan = (Span)component.getChildren().get(0);
        final Span grpSpan = (Span)component.getChildren().get(1);
        final Span ticketSpan = (Span)component.getChildren().get(2);
        Assert.assertNotNull(agentSpan);
        Assert.assertNotNull(grpSpan);
        Assert.assertNotNull(ticketSpan);
        final Listbox agentbox = (Listbox)agentSpan.getChildren().get(1);
        Assert.assertNotNull(agentbox);
        final Listbox grpbox = (Listbox)grpSpan.getChildren().get(1);
        Assert.assertNotNull(grpbox);
        final Listbox ticketbox = (Listbox)ticketSpan.getChildren().get(1);
        Assert.assertNotNull(ticketbox);

    }

    @Ignore
    @Test
    public void testCreateDropDown() {
        Mockito.when(targetCsAgentGroupService.getAllCustomerAgentGroupUid()).thenReturn(
                Collections.singletonList("test"));
        final Listbox box = renderer.createSearchGroupField(widget, new Div());
        Assert.assertNotNull(box);
    }
}
