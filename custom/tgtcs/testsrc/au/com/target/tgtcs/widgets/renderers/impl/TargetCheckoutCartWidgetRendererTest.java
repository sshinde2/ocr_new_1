/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CheckoutController;
import de.hybris.platform.cscockpit.widgets.models.impl.CheckoutCartWidgetModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcs.widgets.controllers.TargetBasketController;


/**
 * Unit Test for {@link TargetCheckoutCartWidgetRenderer}
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Messagebox.class })
public class TargetCheckoutCartWidgetRendererTest {

    @Mock
    private ListboxWidget<CheckoutCartWidgetModel, CheckoutController> widget;
    @Mock
    private CheckoutController checkoutController;
    @Mock
    private TargetBasketController basketController;
    @Mock
    private Event event;
    @Mock
    private TypedObject item;
    @Mock
    private Longbox qtyInput;
    private final Long value = Long.valueOf(10L);
    private final TargetCheckoutCartWidgetRenderer testInstance = new TargetCheckoutCartWidgetRenderer();

    @Before
    public void setUp() {
        mockStatic(Messagebox.class);
        MockitoAnnotations.initMocks(this);
        when(checkoutController.getBasketController()).thenReturn(basketController);
        when(widget.getWidgetController()).thenReturn(checkoutController);
        when(item.getObject()).thenReturn(new Object());
        when(qtyInput.getValue()).thenReturn(value);
    }

    @Test
    public void testHandleCartEntryQtyChangeEventInvoceSetQuantityToSku() throws CommerceCartModificationException {
        testInstance.handleCartEntryQtyChangeEvent(widget, event, item, qtyInput);
        verify(basketController).setQuantityToSku(item, value.longValue(), null);
        verify(basketController).dispatchEvent(null, widget, null);
    }

    @Test
    public void testHandleCartEntryQtyChangeEventWhenSetQuantityToSkuThrowException()
            throws CommerceCartModificationException, InterruptedException {
        doThrow(new CommerceCartModificationException("exception")).when(basketController)
                .setQuantityToSku(item, value.longValue(), null);
        testInstance.handleCartEntryQtyChangeEvent(widget, event, item, qtyInput);
        verifyStatic();
        Messagebox.show("exception", LabelUtils.getLabel(widget, "unableToSaveQty"),
                Messagebox.OK, Messagebox.ERROR);
    }
}
