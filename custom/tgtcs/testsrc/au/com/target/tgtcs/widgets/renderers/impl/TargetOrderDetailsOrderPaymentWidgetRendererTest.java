/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.models.impl.OrderItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.details.WidgetDetailRenderer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zk.ui.api.HtmlBasedComponent;

import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderDetailsOrderPaymentWidgetRendererTest {

    @InjectMocks
    private final TargetOrderDetailsOrderPaymentWidgetRenderer paymentRenderer = new TargetOrderDetailsOrderPaymentWidgetRenderer();

    @Mock
    private TargetPaymentService paymentService;

    @Mock
    private OrderItemWidgetModel itemWidget;

    @Mock
    private OrderController orderController;

    @Mock
    private HtmlBasedComponent component;

    @Mock
    private TypeService typeService;

    @Mock
    private TypedObject typedObject;

    @Mock
    private OrderModel orderModel;

    @Mock
    private PaymentInfoModel paymentInfo;

    @Mock
    private Widget widget;

    @Mock
    private IpgPaymentInfoModel ipgPaymentInfo;

    @Mock
    private WidgetDetailRenderer widgetRenderer;

    @Mock
    private AddressModel billingAddress;

    @Mock
    private IpgCreditCardPaymentInfoModel creditCardInfo;

    @Before
    public void setup() {
        when(widget.getWidgetModel()).thenReturn(itemWidget);
        when(itemWidget.getOrder()).thenReturn(typedObject);
        when(typedObject.getObject()).thenReturn(orderModel);

        final List<PaymentInfoModel> successfulPayment = new ArrayList<>();
        successfulPayment.add(creditCardInfo);
        when(paymentService.getSuccessfulPayments(orderModel)).thenReturn(successfulPayment);
    }

    @Test
    public void testcreateContentInternalWithNullPaymentInfo() {
        when(orderModel.getPaymentInfo()).thenReturn(null);
        paymentRenderer.createContentInternal(widget, component);
        Mockito.verifyZeroInteractions(typeService);
        Mockito.verifyZeroInteractions(paymentService);
    }

    @Test
    public void testcreateContentInternalWithNonIPG() {
        when(orderModel.getPaymentInfo()).thenReturn(paymentInfo);
        paymentRenderer.createContentInternal(widget, component);
        Mockito.verify(typeService).wrapItem(paymentInfo);
        Mockito.verifyZeroInteractions(paymentService);
    }

    @Test
    public void testcreateContentInternalWithSingleCardIPG() {
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfo);
        when(ipgPaymentInfo.getIpgPaymentTemplateType()).thenReturn(IpgPaymentTemplateType.CREDITCARDSINGLE);
        when(ipgPaymentInfo.getBillingAddress()).thenReturn(billingAddress);
        paymentRenderer.createContentInternal(widget, component);
        Mockito.verify(typeService).wrapItem(creditCardInfo);
        Mockito.verify(paymentService).getSuccessfulPayments(orderModel);
    }


    @Test
    public void testcreateContentInternalWithMultiCardIPG() {
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfo);
        when(ipgPaymentInfo.getIpgPaymentTemplateType()).thenReturn(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        when(ipgPaymentInfo.getBillingAddress()).thenReturn(billingAddress);
        paymentRenderer.createContentInternal(widget, component);
        Mockito.verify(typeService, times(0)).wrapItem(ipgPaymentInfo);
        Mockito.verifyZeroInteractions(paymentService);
    }

    @Test
    public void testcreateContentInternalWithNullTemplateIPG() {
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfo);
        when(ipgPaymentInfo.getIpgPaymentTemplateType()).thenReturn(null);
        when(ipgPaymentInfo.getBillingAddress()).thenReturn(billingAddress);
        paymentRenderer.createContentInternal(widget, component);
        Mockito.verify(typeService, times(0)).wrapItem(ipgPaymentInfo);
        Mockito.verifyZeroInteractions(paymentService);
    }

}
