/**
 * 
 */
package au.com.target.tgtcs.strategies.impl;

import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UIBrowserArea;
import de.hybris.platform.cockpit.session.UICockpitPerspective;
import de.hybris.platform.cockpit.widgets.browsers.WidgetBrowserModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetBasketControllerImpl;


/**
 * Unit Test for {@link TargetDeliveryModeCostRetrievalStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDeliveryModeCostRetrievalStrategyTest {

    @Mock
    private TargetDeliveryModeCostRetrievalStrategy testInstance;

    /**
     * 
     */
    @Test
    public void testGetCard() {
        final List<TargetBasketController> controllers = new ArrayList<>();
        final TargetBasketControllerImpl bk1 = mock(TargetBasketControllerImpl.class);
        final WidgetBrowserModel bm1 = mock(WidgetBrowserModel.class);
        when(bm1.getBrowserCode()).thenReturn("bk1");
        when(bk1.getCheckoutBrowser()).thenReturn(bm1);
        final WidgetBrowserModel bm2 = mock(WidgetBrowserModel.class);
        final TargetBasketControllerImpl bk2 = mock(TargetBasketControllerImpl.class);
        when(bm2.getBrowserCode()).thenReturn("bk2");
        when(bk2.getCheckoutBrowser()).thenReturn(bm2);
        final TypedObject cart1 = mock(TypedObject.class);
        final TypedObject cart2 = mock(TypedObject.class);
        when(bk1.getCart()).thenReturn(cart1);
        when(bk2.getCart()).thenReturn(cart2);
        controllers.add(bk1);
        controllers.add(bk2);

        final UICockpitPerspective perspective = mock(UICockpitPerspective.class);
        final UIBrowserArea brawserArea = mock(UIBrowserArea.class);
        final WidgetBrowserModel brawserModel = mock(WidgetBrowserModel.class);
        when(brawserModel.getBrowserCode()).thenReturn("bk2");
        when(brawserArea.getFocusedBrowser()).thenReturn(brawserModel);
        when(perspective.getBrowserArea()).thenReturn(brawserArea);

        when(testInstance.getCurrentPerspective()).thenReturn(perspective);

        when(testInstance.getBasketControllers()).thenReturn(controllers);

        doCallRealMethod().when(testInstance).getCart();

        final TypedObject cart = testInstance.getCart();
        Assert.assertSame(cart, cart2);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetDeliveryCostForDeliveryMode() {
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        final TargetFindDeliveryCostStrategy deliveryService = mock(TargetFindDeliveryCostStrategy.class);

        final TypedObject cartObject = mock(TypedObject.class);
        final CartModel cart = mock(CartModel.class);

        when(cartObject.getObject()).thenReturn(cart);

        when(testInstance.getCart()).thenReturn(cartObject);
        when(testInstance.getTargetFindDeliveryCostStrategy()).thenReturn(deliveryService);
        when(Double.valueOf(deliveryService.getDeliveryCost(deliveryMode, cart))).thenReturn(Double.valueOf(11D));


        doCallRealMethod().when(testInstance).getDeliveryCostForDeliveryMode(deliveryMode);
        final BigDecimal value = testInstance.getDeliveryCostForDeliveryMode(deliveryMode);
        Assert.assertEquals(value, BigDecimal.valueOf(11D));
    }

    @Test
    public void testGetDeliveryCostForDeliveryModeWhenNoPostcodeEntered() {
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        final TargetFindDeliveryCostStrategy deliveryService = mock(TargetFindDeliveryCostStrategy.class);

        final TypedObject cartObject = mock(TypedObject.class);
        final CartModel cart = mock(CartModel.class);

        when(cartObject.getObject()).thenReturn(cart);

        when(testInstance.getCart()).thenReturn(cartObject);
        when(testInstance.getTargetFindDeliveryCostStrategy()).thenReturn(deliveryService);
        when(Double.valueOf(deliveryService.getDeliveryCost(deliveryMode, cart))).thenThrow(
                new TargetNoPostCodeException("Exception occured"));

        doCallRealMethod().when(testInstance).getDeliveryCostForDeliveryMode(deliveryMode);
        final BigDecimal value = testInstance.getDeliveryCostForDeliveryMode(deliveryMode);
        Assert.assertNull(value);
    }
}
