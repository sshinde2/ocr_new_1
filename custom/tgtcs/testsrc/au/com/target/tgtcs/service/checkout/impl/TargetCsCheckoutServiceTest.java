/**
 * 
 */
package au.com.target.tgtcs.service.checkout.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.zkoss.spring.SpringUtil;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TargetCSOrderFacade;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;



/**
 * Unit Test for {@link TargetCsCheckoutService}
 * 
 */

@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, SpringUtil.class })
@PowerMockIgnore("javax.management.*")
public class TargetCsCheckoutServiceTest {

    @Mock
    private CartModel masterCartModel;
    @Mock
    private TargetCommerceCheckoutService targetCommerceCheckoutService;
    @Mock
    private TargetCustomerAccountService targetCustomerAccountService;
    @Mock
    private ModelService modelService;
    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;
    @Mock
    private TargetCSOrderFacade orderFacade;
    @Mock
    private TargetCommerceCartService commerceCartService;
    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfig;
    @Mock
    private CommerceOrderResult commerceOrderResult;
    @Mock
    private SessionService sessionService;
    @Mock
    private TargetPaymentService targetPaymentService;
    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private TargetCsCheckoutService targetCsCheckoutService;
    private MessageSource messageSource;
    private I18NService i18Service;


    /**
     * set up some initial values for test
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(SpringUtil.class, Mockito.RETURNS_MOCKS);
        MockitoAnnotations.initMocks(this);

        when(
                Boolean.valueOf(orderFacade.isOrderAvailableForDeliveryMode(any(AbstractOrderModel.class),
                        any(DeliveryModeModel.class))))
                                .thenReturn(Boolean.TRUE);

        targetCsCheckoutService = new TargetCsCheckoutService();
        targetCsCheckoutService.setCustomerAccountService(targetCustomerAccountService);
        targetCsCheckoutService.setCommerceCheckoutService(targetCommerceCheckoutService);
        targetCsCheckoutService.setModelService(modelService);
        targetCsCheckoutService.setTargetPurchaseOptionHelper(targetPurchaseOptionHelper);
        targetCsCheckoutService.setDeliveryAddressRequired(true);
        targetCsCheckoutService.setPaymentAddressRequired(true);
        targetCsCheckoutService.setDeliveryModeRequired(true);
        targetCsCheckoutService.setOrderFacade(orderFacade);
        targetCsCheckoutService.setSessionService(sessionService);
        targetCsCheckoutService.setTargetPaymentService(targetPaymentService);
        targetCsCheckoutService.setFindOrderTotalPaymentMadeStrategy(findOrderTotalPaymentMadeStrategy);
        messageSource = mock(MessageSource.class);
        i18Service = mock(I18NService.class);
        given(SpringUtil.getBean("messageSource")).willReturn(messageSource);
        given(SpringUtil.getBean("i18nService")).willReturn(i18Service);

        when(masterCartModel.getPurchaseOption()).thenReturn(null);
        targetCsCheckoutService.setCommerceCartService(commerceCartService);

    }

    /**
     * test cart null
     */
    @Test
    public void testInvalidCart() {
        try {
            targetCsCheckoutService.validateCartForCheckout(null);
        }
        catch (final ValidationException e) {
            assertTrue("expected result", e.getResourceMessages().size() == 1);
        }
    }

    /**
     * test an empty cart, exception expected
     * 
     * @throws ValidationException
     */
    @Test(expected = ValidationException.class)
    public void testEmptyCart() throws ValidationException {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        when(masterCartModel.getEntries()).thenReturn(entries);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        targetCsCheckoutService.validateCartForCheckout(masterCartModel);

    }

    /**
     * test a valid cart, validation should pass
     * 
     * @throws ValidationException
     */
    @Test
    public void testValidCart() throws ValidationException {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        final PaymentInfoModel paymentInfo = new PaymentInfoModel();
        final UserModel user = new UserModel();
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        final AddressModel address = new AddressModel();
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);

        targetCsCheckoutService.validateCartForCheckout(masterCartModel);

    }

    /**
     * test checkout
     * 
     * @throws Exception
     */
    @SuppressWarnings("boxing")
    @Test
    public void testCheckout() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        final PaymentInfoModel paymentInfo = new PaymentInfoModel();
        final UserModel user = new UserModel();
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        final AddressModel address = new AddressModel();
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, null)).thenReturn(true);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);

        targetCsCheckoutService.doCheckout(masterCartModel);

        verify(targetCommerceCheckoutService).beforePlaceOrder(masterCartModel, null);
    }

    /**
     * This test checks if ValidationException is thrown if cart has products that are not eligible for delivery mode in
     * this cart.
     * 
     * @throws ValidationException
     */
    @Test(expected = ValidationException.class)
    public void testInvalidDeliveryModeCart() throws ValidationException {
        when(
                Boolean.valueOf(orderFacade.isOrderAvailableForDeliveryMode(any(AbstractOrderModel.class),
                        any(DeliveryModeModel.class))))
                                .thenReturn(Boolean.FALSE);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));

        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);

        final PaymentInfoModel paymentInfo = new PaymentInfoModel();
        final UserModel user = new UserModel();
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);

        final AddressModel address = new AddressModel();
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);

        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);

        targetCsCheckoutService.validateCartForCheckout(masterCartModel);
    }


    /**
     * test checkout
     * 
     * @throws Exception
     */
    @SuppressWarnings("boxing")
    @Test(expected = ValidationException.class)
    public void testCheckoutForIPGWithoutQueryTransactionResult() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        final AddressModel address = new AddressModel();
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, null)).thenReturn(true);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);

        targetCsCheckoutService.doCheckout(masterCartModel);

        Mockito.verify(targetPaymentService, Mockito.times(0)).createPaymentTransactionEntry(any(OrderModel.class));
    }

    @SuppressWarnings("boxing")
    @Test(expected = ValidationException.class)
    public void testCheckoutForIPGWithOrderTotalAmountMissmatch() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        final AddressModel address = new AddressModel();
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(targetCommerceCheckoutService.shouldPaymentBeReversed(masterCartModel, null)).thenReturn(Boolean.TRUE);
        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, null)).thenReturn(Boolean.TRUE);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);

        targetCsCheckoutService.doCheckout(masterCartModel);

        Mockito.verify(targetPaymentService, Mockito.times(0)).createPaymentTransactionEntry(any(OrderModel.class));
    }

    /**
     * test checkout
     * 
     * @throws Exception
     */
    @SuppressWarnings("boxing")
    @Test
    public void testCheckoutForIPG() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final List<PaymentTransactionEntryModel> list = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        final AddressModel address = new AddressModel();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, paymentTransactionModel))
                .thenReturn(true);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = new TargetQueryTransactionDetailsResult();
        queryTransactionDetailsResult.setSuccess(true);
        when(sessionService.getAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY))
                .thenReturn(queryTransactionDetailsResult);
        list.add(paymentTransactionEntryModel);
        given(paymentTransactionModel.getEntries()).willReturn(list);
        when(targetPaymentService.createTransactionWithQueryResult(masterCartModel, queryTransactionDetailsResult))
                .thenReturn(paymentTransactionModel);

        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);
        targetCsCheckoutService.doCheckout(masterCartModel);
        verify(targetPaymentService).createTransactionWithQueryResult(masterCartModel,
                queryTransactionDetailsResult);
        verify(targetCommerceCheckoutService).beforePlaceOrder(masterCartModel, paymentTransactionModel);
    }

    @SuppressWarnings("boxing")
    @Test(expected = ValidationException.class)
    public void testCheckoutForIPGWithFailedPayment() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final List<PaymentTransactionEntryModel> list = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        final AddressModel address = new AddressModel();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, paymentTransactionModel))
                .thenReturn(false);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = new TargetQueryTransactionDetailsResult();
        when(sessionService.getAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY))
                .thenReturn(queryTransactionDetailsResult);
        list.add(paymentTransactionEntryModel);
        given(paymentTransactionModel.getEntries()).willReturn(list);
        when(targetPaymentService.createTransactionWithQueryResult(masterCartModel, queryTransactionDetailsResult))
                .thenReturn(paymentTransactionModel);

        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);

        targetCsCheckoutService.doCheckout(masterCartModel);

        verify(targetPaymentService).createTransactionWithQueryResult(masterCartModel,
                queryTransactionDetailsResult);
        verify(targetCommerceCheckoutService).beforePlaceOrder(masterCartModel, paymentTransactionModel);
    }

    @SuppressWarnings("boxing")
    @Test(expected = ValidationException.class)
    public void testCheckoutForIPGWithInsufficientStock() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final List<PaymentTransactionEntryModel> list = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        final AddressModel address = new AddressModel();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, paymentTransactionModel))
                .thenThrow(new InsufficientStockLevelException("test"));
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = new TargetQueryTransactionDetailsResult();
        when(sessionService.getAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY))
                .thenReturn(queryTransactionDetailsResult);
        list.add(paymentTransactionEntryModel);
        given(paymentTransactionModel.getEntries()).willReturn(list);
        when(targetPaymentService.createTransactionWithQueryResult(masterCartModel, queryTransactionDetailsResult))
                .thenReturn(paymentTransactionModel);

        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);

        targetCsCheckoutService.doCheckout(masterCartModel);

        verify(targetPaymentService).createTransactionWithQueryResult(masterCartModel,
                queryTransactionDetailsResult);
        verify(modelService).remove(paymentTransactionModel);
        verify(targetCommerceCheckoutService).beforePlaceOrder(masterCartModel, paymentTransactionModel);
    }

    @SuppressWarnings("boxing")
    @Test(expected = ValidationException.class)
    public void testCheckoutForIPGWithTotalAmountMissmatch() throws Exception {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final List<PaymentTransactionEntryModel> list = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        final AddressModel address = new AddressModel();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);

        when(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, paymentTransactionModel))
                .thenReturn(true);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = new TargetQueryTransactionDetailsResult();
        queryTransactionDetailsResult.setSuccess(true);
        when(sessionService.getAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY))
                .thenReturn(queryTransactionDetailsResult);
        list.add(paymentTransactionEntryModel);
        given(paymentTransactionModel.getEntries()).willReturn(list);
        when(targetPaymentService.createTransactionWithQueryResult(masterCartModel, queryTransactionDetailsResult))
                .thenReturn(paymentTransactionModel);

        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);

        when(
                targetCommerceCheckoutService.shouldPaymentBeReversed(masterCartModel,
                        queryTransactionDetailsResult.getCardResults()))
                                .thenReturn(true);

        targetCsCheckoutService.doCheckout(masterCartModel);
        verify(targetPaymentService).reversePayment(Mockito.any(CartModel.class), Mockito.anyList());
        verify(targetPaymentService, Mockito.times(0)).createTransactionWithQueryResult(masterCartModel,
                queryTransactionDetailsResult);
        verify(targetCommerceCheckoutService, Mockito.times(0)).beforePlaceOrder(masterCartModel,
                paymentTransactionModel);
    }

    @Test
    public void testCheckoutForCallCenterSalesApp() throws Exception {
        mockCorrectCartInfo();

        targetCsCheckoutService.doCheckout(masterCartModel);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(masterCartModel);
        checkoutParameter.setSalesApplication(SalesApplication.CALLCENTER);
        verify(targetCommerceCheckoutService).placeOrder(Mockito.refEq(checkoutParameter));
    }

    @Test
    public void testCheckoutForB2BSalesApp() throws Exception {
        mockCorrectCartInfo();
        when(sessionService.getAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR)).thenReturn(Boolean.TRUE);

        targetCsCheckoutService.doCheckout(masterCartModel);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(masterCartModel);
        checkoutParameter.setSalesApplication(SalesApplication.B2B);
        verify(targetCommerceCheckoutService).placeOrder(Mockito.refEq(checkoutParameter));
    }

    /**
     * Method to populate and mock cart with correct information
     * 
     * @throws InsufficientStockLevelException
     * @throws InvalidCartException
     */
    private void mockCorrectCartInfo() throws InsufficientStockLevelException, InvalidCartException {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final List<PaymentTransactionEntryModel> list = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfo = new IpgPaymentInfoModel();
        final UserModel user = new UserModel();
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        final DeliveryModeModel deliveryMode = new DeliveryModeModel();
        final AddressModel address = new AddressModel();
        when(masterCartModel.getTotalPrice()).thenReturn(Double.valueOf(10.0D));
        entries.add(entry);
        when(masterCartModel.getEntries()).thenReturn(entries);
        user.setUid("testid");
        paymentInfo.setUser(user);
        when(masterCartModel.getUser()).thenReturn(user);
        when(masterCartModel.getPaymentInfo()).thenReturn(paymentInfo);
        when(masterCartModel.getDeliveryAddress()).thenReturn(address);
        when(masterCartModel.getPaymentAddress()).thenReturn(address);
        when(masterCartModel.getDeliveryMode()).thenReturn(deliveryMode);
        when(Boolean.valueOf(targetCommerceCheckoutService.beforePlaceOrder(masterCartModel, paymentTransactionModel)))
                .thenReturn(Boolean.TRUE);
        when(targetCommerceCheckoutService.placeOrder(Mockito.any(CommerceCheckoutParameter.class)))
                .thenReturn(commerceOrderResult);
        when(Boolean.valueOf(commerceCartService.validateCartPurchaseAmount(masterCartModel))).thenReturn(Boolean.TRUE);

        when(Boolean.valueOf(targetCommerceCheckoutService.isCaptureOrderSuccessful(masterCartModel)))
                .thenReturn(Boolean.TRUE);

        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = new TargetQueryTransactionDetailsResult();
        queryTransactionDetailsResult.setSuccess(true);
        when(sessionService.getAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY))
                .thenReturn(queryTransactionDetailsResult);
        list.add(paymentTransactionEntryModel);
        given(paymentTransactionModel.getEntries()).willReturn(list);
        when(targetPaymentService.createTransactionWithQueryResult(masterCartModel, queryTransactionDetailsResult))
                .thenReturn(paymentTransactionModel);

        final OrderModel orderModel = new OrderModel();
        when(commerceOrderResult.getOrder()).thenReturn(orderModel);
    }
}
