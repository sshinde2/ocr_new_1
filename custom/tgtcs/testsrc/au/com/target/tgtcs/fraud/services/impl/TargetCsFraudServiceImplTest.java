/**
 *
 */
package au.com.target.tgtcs.fraud.services.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcs.service.fraud.impl.TargetCsFraudServiceImpl;
import au.com.target.tgtfraud.strategy.impl.TargetFraudReportCreationStrategyImpl;


/**
 * @author mjanarth
 *
 */

@UnitTest

public class TargetCsFraudServiceImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private UserService userService;
    @Mock
    private UserModel user;

    @Mock
    private TargetFraudReportCreationStrategyImpl targetFraudReportCreationStrategy;

    @InjectMocks
    private final TargetCsFraudServiceImpl service = new TargetCsFraudServiceImpl();


    /**
     *
     */
    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);
        Mockito.when(userService.getCurrentUser()).thenReturn(user);
        Mockito.when(user.getUid()).thenReturn("admin");

    }

    @Test
    public void testAddFraudReportToOrderWithEmptyNote() throws Exception {
        final OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getCode()).thenReturn("123456");
        service.addFraudReportToOrder(orderModel, FraudStatus.OK,"" );
        Mockito.verify(targetFraudReportCreationStrategy,Mockito.times(1)).createFraudReport(Mockito.any(FraudServiceResponse.class),Mockito.any(OrderModel.class),
                (Mockito.any(FraudStatus.class)));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddFraudReportToOrderWithNullOrder() throws Exception {
        final OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getCode()).thenReturn("123456");
        service.addFraudReportToOrder(null, FraudStatus.OK,"" );
        Mockito.verify(targetFraudReportCreationStrategy,Mockito.never()).createFraudReport(Mockito.any(FraudServiceResponse.class),Mockito.any(OrderModel.class),
                (Mockito.any(FraudStatus.class)));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddFraudReportToOrderWithNullFraudStatys() throws Exception {
        final OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getCode()).thenReturn("123456");
        service.addFraudReportToOrder(orderModel, null,"" );
        Mockito.verify(targetFraudReportCreationStrategy,Mockito.never()).createFraudReport(Mockito.any(FraudServiceResponse.class),Mockito.any(OrderModel.class),
                (Mockito.any(FraudStatus.class)));

    }




}
