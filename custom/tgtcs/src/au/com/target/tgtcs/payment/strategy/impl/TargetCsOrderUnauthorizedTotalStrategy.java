/**
 * 
 */
package au.com.target.tgtcs.payment.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cscockpit.services.payment.strategy.impl.DefaultCsOrderUnauthorizedTotalStrategy;
import de.hybris.platform.cscockpit.utils.SafeUnbox;

import java.math.BigDecimal;

import au.com.target.tgtpayment.util.PriceCalculator;


public class TargetCsOrderUnauthorizedTotalStrategy extends DefaultCsOrderUnauthorizedTotalStrategy {

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.services.payment.strategy.impl.DefaultCsOrderUnauthorizedTotalStrategy#getUnauthorizedTotal(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public double getUnauthorizedTotal(final AbstractOrderModel order) {
        double amount = 0.0D;
        if (order != null)
        {
            final BigDecimal amountThatWeHaveCapturedOrThinkWeCanAuth = getTransactionViableAuthAmountOrCapturedAmount(order);
            final double totalPrice = SafeUnbox.toDouble(order.getTotalPrice());
            final BigDecimal outstandingTotal = PriceCalculator.subtract(
                    Double.valueOf(amountThatWeHaveCapturedOrThinkWeCanAuth.doubleValue()),
                    Double.valueOf(totalPrice));

            amount = outstandingTotal.doubleValue();

        }
        if (order instanceof CartModel
                && order.getAmountCurrentPayment() != null
                && order.getPurchaseOptionConfig() != null
                && order.getPurchaseOptionConfig().getAllowPaymentDues().booleanValue()) {

            final double currentPay = SafeUnbox.toDouble(order.getAmountCurrentPayment());
            if (amount > currentPay) {
                return currentPay;
            }

        }
        return amount;
    }

    /* for test
     * @see de.hybris.platform.cscockpit.services.payment.strategy.impl.DefaultCsOrderUnauthorizedTotalStrategy#getTransactionViableAuthAmountOrCapturedAmount(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    protected BigDecimal getTransactionViableAuthAmountOrCapturedAmount(final AbstractOrderModel order) {
        return super.getTransactionViableAuthAmountOrCapturedAmount(order);
    }




}
