/**
 * 
 */
package au.com.target.tgtcs.model.editor.impl;

import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.impl.AbstractUIEditor;
import de.hybris.platform.cockpit.model.editor.impl.DefaultTextUIEditor;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cscockpit.model.editor.validators.CockpitValidator;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Constraint;
import org.zkoss.zul.impl.InputElement;


/**
 * email format validation
 */
public class EmailConstraintTextUIEditor extends DefaultTextUIEditor {

    private static final Logger LOG = Logger.getLogger(EmailConstraintTextUIEditor.class);
    private static final String COCKPIT_VALIDATOR_PARAM = "cockpitValidatorClass";
    private static final String COCKPIT_VALIDATOR_FAIL_LABEL = "cockpitValidatorFailLabel";
    private static final String COCKPIT_VALIDATOR_NOT_ALLOWED = "tgtcscockpit.cockpitvalidation.not_allowed";


    @Override
    protected AbstractUIEditor.CancelButtonContainer createViewComponentInternal(final InputElement editorView,
            final EditorListener listener, final Map<String, ? extends Object> parameters) {

        final Object validatorParam = parameters.get(COCKPIT_VALIDATOR_PARAM);
        if ((validatorParam instanceof String)) {
            try {
                final Class validator = Class.forName((String)validatorParam);
                final Object validatorObj = validator.newInstance();

                if ((validatorObj instanceof CockpitValidator)) {
                    final CockpitValidator cockpitValidator = (CockpitValidator)validatorObj;
                    final Object propParam = parameters.get("attributeQualifier");
                    if ((propParam instanceof String)) {
                        final PropertyDescriptor propertyDescriptor = getTypeService().getPropertyDescriptor(
                                (String)propParam);
                        final Object i3FailParam = parameters.get(COCKPIT_VALIDATOR_FAIL_LABEL);
                        String i3FailLabel = null;
                        if ((i3FailParam instanceof String)) {
                            i3FailLabel = (String)i3FailParam;
                        }
                        cockpitValidator.initialize(editorView, propertyDescriptor,
                                StringUtils.isBlank(i3FailLabel) ? COCKPIT_VALIDATOR_NOT_ALLOWED
                                        : i3FailLabel);
                        if ((propParam instanceof String)) {
                            editorView.setConstraint(new CockpitValidatorConstraint(cockpitValidator));
                        }
                    }
                }
                else {
                    LOG.warn("Validation can not be performed. Reason: Cockpit validator could not be created.");
                }
            }
            catch (final ClassNotFoundException ex) {
                LOG.error("Validation can not be performed. Reason: Cockpit validator with class '"
                        + validatorParam +
                        "' could not be loaded.", ex);
            }
            catch (final ClassCastException ex) {
                LOG.error("Validation can not be performed. Reason: Unexpected validator type.", ex);
            }
            catch (final Exception ex) {
                LOG.error("Validation can not be performed.", ex);
            }
        }
        return super.createViewComponentInternal(editorView, listener, parameters);
    }

    private TypeService getTypeService() {
        return UISessionUtils.getCurrentSession().getTypeService();
    }


    private static class CockpitValidatorConstraint implements Constraint {

        private final CockpitValidator cockpitValidator;

        public CockpitValidatorConstraint(final CockpitValidator validator) {
            this.cockpitValidator = validator;
        }

        @Override
        public void validate(final Component cmp, final Object value)
                throws WrongValueException {
            try {
                this.cockpitValidator.validate(value);
            }
            catch (final WrongValueException ex) {
                throw ex;
            }
            catch (final Exception e) {
                EmailConstraintTextUIEditor.LOG.error(
                        "An unknown error occurred while trying to validate editor value.", e);
            }
        }
    }

}
