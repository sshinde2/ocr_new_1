/**
 * 
 */
package au.com.target.tgtcs.facade;

import de.hybris.platform.core.model.order.OrderModel;

import org.zkoss.zul.Window;


/**
 * The Interface AddPaymentFormFacade.
 * 
 * @author umesh
 */
public interface TgtCsAddPaymentFormFacade {

    /**
     * refresh window by recreating the payment form and filling in the values.
     * 
     * @param window
     * @param order
     */
    void loadFormContent(final OrderModel order, final Window window);

    /**
     * Populate payment form.
     * 
     * @param window
     * @param order
     */
    void populateRefundForm(final OrderModel order, final Window window);

    /**
     * Populate new payment form.
     * 
     * @param order
     * @param window
     */
    void populateCancelForm(final OrderModel order, final Window window);

    /**
     * refresh window by recreating the payment form and filling in the values.
     * 
     * @param window
     * @param order
     */
    void loadIpgFormContent(final OrderModel order, final Window window);

}
