/**
 * 
 */
package au.com.target.tgtcs.facade;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.math.BigDecimal;
import java.util.List;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Execution;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;


/**
 * The Interface TgtCsCardPaymentFacade.
 * 
 * @author umesh
 */
public interface TgtCsCardPaymentFacade {

    /**
     * Handle refund payment form response.
     * 
     * @param execution
     *            the execution
     * @return the list
     */
    List<String> handleRefundPaymentFormResponse(final Execution execution);

    /**
     * Clear old payment.
     */
    void clearOldPayment();

    /**
     * Processes the refund.
     * 
     * @param request
     * @param orderModel
     * 
     */
    void processRefund(ReturnRequestModel request, OrderModel orderModel);

    /**
     * Handle payment form response.
     * 
     * @param execution
     *            the execution
     * @return the list
     */
    List<String> handleCancelPaymentFormResponse(Execution execution);

    /**
     * Gets the suggested amount for payment option.
     * 
     * @return the suggested amount for payment option
     */
    double getSuggestedAmountForPaymentOption(OrderModel order);

    /**
     * Gets the new payment.
     * 
     * @return the new payment
     */
    PaymentInfoModel getNewPayment();

    /**
     * Method to create hosted session token request.
     * 
     * @param cart
     * @param amount
     * @param paymentMode
     * @param cancelUrl
     * @param returnUrl
     * @param pct
     * @param sessionId
     * @return HostedSessionTokenRequest
     */
    HostedSessionTokenRequest createHostedSessionTokenRequest(final CartModel cart, final BigDecimal amount,
            final PaymentModeModel paymentMode, final String cancelUrl, final String returnUrl,
            final PaymentCaptureType pct, final String sessionId, final IpgPaymentTemplateType ipgPaymentTemplateType);

    /**
     * create ipg payment info
     * 
     * @param component
     */
    void createPaymentInfoModelForIpg(AbstractComponent component);

    /**
     * Get ipg New refund info Dto
     * 
     * @return IpgNewRefundInfoDTO
     */
    IpgNewRefundInfoDTO getIpgNewRefundInfoDTO();
}
