/**
 * 
 */
package au.com.target.tgtcs.util;

import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.c2l.CountryModel;


/**
 * @author Rahul
 * 
 */
public class CountryComparator extends AbstractComparator<CountryModel> {

    public static final CountryComparator INSTANCE = new CountryComparator();

    @Override
    protected int compareInstances(final CountryModel country1, final CountryModel country2)
    {
        int result = (country1.getName() != null && country2.getName() != null) ? country1.getName()
                .compareToIgnoreCase(
                        country2.getName()) : BEFORE;
        if (EQUAL == result)
        {
            result = country1.getIsocode().compareToIgnoreCase(country2.getIsocode());
        }
        return result;
    }

}
