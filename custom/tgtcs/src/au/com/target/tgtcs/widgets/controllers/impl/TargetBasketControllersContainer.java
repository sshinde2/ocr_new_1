/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcs.widgets.controllers.TargetBasketController;


/**
 *
 */
public class TargetBasketControllersContainer {

    private List<TargetBasketController> basketControllers;

    /**
     * @return the basketControllers
     */
    public List<TargetBasketController> getBasketControllers() {
        return basketControllers;
    }

    /**
     * @param basketControllers
     *            the basketControllers to set
     */
    @Required
    public void setBasketControllers(final List<TargetBasketController> basketControllers) {
        this.basketControllers = basketControllers;
    }


}
