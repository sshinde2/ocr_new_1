/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.ColumnConfiguration;
import de.hybris.platform.cockpit.widgets.impl.DefaultListboxWidget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractConfigurableCsListboxWidgetRenderer;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listitem;

import au.com.target.tgtcs.widgets.models.impl.TargetAccertifyReportListWidgetModel;


/**
 * @author umesh
 * 
 */
public class TargetAccertifyHistoryRenderer
        extends
        AbstractConfigurableCsListboxWidgetRenderer<DefaultListboxWidget<TargetAccertifyReportListWidgetModel, OrderController>> {
    protected static final String CSS_ACCERTIFY_HISTORY = "tgtcsAccertifyHistory";

    @Override
    protected HtmlBasedComponent createContentInternal(
            final DefaultListboxWidget<TargetAccertifyReportListWidgetModel, OrderController> widget,
            final HtmlBasedComponent rootContainer)
    {
        final Div content = new Div();
        content.setSclass("csOrderHistory");

        final Listbox listBox = new Listbox();
        listBox.setParent(content);
        listBox.setSclass("csWidgetListbox");

        renderListbox(listBox, widget, rootContainer);

        return content;
    }

    @Override
    protected void renderListbox(final Listbox listBox,
            final DefaultListboxWidget<TargetAccertifyReportListWidgetModel, OrderController> widget,
            final HtmlBasedComponent rootContainer)
    {
        final TargetAccertifyReportListWidgetModel widgetModel = widget.getWidgetModel();
        if (CollectionUtils.isNotEmpty(widgetModel.getItems()))
        {
            final Listhead header = new Listhead();
            header.setParent(listBox);

            final List<ColumnConfiguration> columns = getColumnConfigurations();
            populateHeaderRow(widget, header, columns);
            for (final TypedObject history : widget.getWidgetModel().getItems()) {
                renderAccertifyHistory(widget, history, listBox, columns);
            }
        }
        else
        {
            final Listitem row = new Listitem();
            row.setParent(listBox);
            final Listcell cell = new Listcell(LabelUtils.getLabel(widget, "noEntries", new Object[0]));
            cell.setParent(row);
        }
    }

    protected void renderAccertifyHistory(
            final DefaultListboxWidget<TargetAccertifyReportListWidgetModel, OrderController> widget,
            final TypedObject history, final Listbox listBox, final List<ColumnConfiguration> columns)
    {
        final Listitem row = new Listitem();
        row.setParent(listBox);

        populateDataRow(widget, row, columns, history);
    }
}
