package au.com.target.tgtcs.widgets.controllers.search.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cscockpit.services.search.CsSearchCommand;
import de.hybris.platform.cscockpit.services.search.CsSearchResult;
import de.hybris.platform.cscockpit.services.search.SearchException;
import de.hybris.platform.cscockpit.widgets.controllers.search.impl.DefaultSearchCommandController;

import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;


public class TargetSearchCommandController<SC extends CsSearchCommand> extends DefaultSearchCommandController {

    protected static final String REFINE_POPUP_TITLE_KEY = "cscockpit.widget.basket.search.refinePopup.title";
    protected static final String REFINE_POPUP_MESSAGE_KEY = "cscockpit.widget.basket.search.refinePopup.message";

    private static final Logger LOG = Logger.getLogger(TargetSearchCommandController.class);

    @Override
    protected void doSearchInternal() throws SearchException {
        if (getSearchCommand() != null) {
            CsSearchResult<SC, ? extends ItemModel> currentSearchResult = null;
            try {
                // Run the search
                currentSearchResult = getSearchService().search(getSearchCommand(), getPageable());
            }
            catch (final Exception ex) {
                LOG.error("Failed to search", ex);

                // Reset the current search
                reset();
            }

            if (currentSearchResult == null) {
                try {
                    final String message = Labels.getLabel(REFINE_POPUP_MESSAGE_KEY);
                    final String title = Labels.getLabel(REFINE_POPUP_TITLE_KEY);
                    Messagebox.show(message, title, Messagebox.OK,
                            Messagebox.INFORMATION);
                }
                catch (final InterruptedException e) {
                    LOG.debug("System Exception: ", e);
                    Thread.currentThread().interrupt();
                }
            }
            else {
                super.doSearchInternal();
            }
        }
    }
}
