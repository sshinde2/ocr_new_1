package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PostPopupAction;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.AddressCustomerController;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCallContextController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetBasketControllersContainer;
import au.com.target.tgtverifyaddr.data.AddressData;


/**
 * @author Dell
 * 
 */
public class ConfirmNewAddressWidgetRenderer extends AbstractCsWidgetRenderer {

    private static final Logger LOG = Logger.getLogger(ConfirmNewAddressWidgetRenderer.class);
    private List<AddressData> addresses;
    private PopupWidgetHelper popupWidgetHelper;
    private ObjectValueContainer addressContainer;
    private String configurationTypeCode;
    private TargetBasketControllersContainer targetBasketControllersContainer;

    /**
     * @return the popupWidgetHelper
     */
    public PopupWidgetHelper getPopupWidgetHelper() {
        return popupWidgetHelper;
    }

    /**
     * @param popupWidgetHelper
     *            the popupWidgetHelper to set
     */
    @Required
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper) {
        this.popupWidgetHelper = popupWidgetHelper;
    }

    /**
     * @param inputtedAddressContainer
     *            the inputtedAddressContainer to set
     */
    public void setInputtedAddressContainer(final ObjectValueContainer inputtedAddressContainer) {
        this.addressContainer = inputtedAddressContainer;
    }

    /**
     * @param configurationTypeCode
     *            the configurationTypeCode to set
     */
    public void setConfigurationTypeCode(final String configurationTypeCode) {
        this.configurationTypeCode = configurationTypeCode;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer#createContentInternal(de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zk.ui.api.HtmlBasedComponent)
     */
    @Override
    protected HtmlBasedComponent createContentInternal(final Widget widget, final HtmlBasedComponent rootContainer) {
        final Div content = new Div();
        final Radiogroup radiogroupAddress = new Radiogroup();
        for (final AddressData data : addresses) {
            final Radio radio = new Radio(data.getPartialAddress());
            radio.setParent(radiogroupAddress);
        }
        radiogroupAddress.setOrient("vertical");
        final Radio radio = new Radio(LabelUtils.getLabel(widget,
                TgtcsConstants.ENTERED_ADDRESS_RADIO_BUTTON_KEY_POSTFIX));
        radio.setParent(radiogroupAddress);
        radiogroupAddress.setSelectedIndex(0);
        content.appendChild(radiogroupAddress);
        final Button buttonSelect = new Button(LabelUtils.getLabel(widget, TgtcsConstants.SELECT_BUTTON_KEY_POSTFIX));
        buttonSelect.addEventListener("onClick", new EventListener()
        {
            @Override
            public void onEvent(final Event event) throws Exception
            {
                ConfirmNewAddressWidgetRenderer.this.handleSelectAddressClickEvent(widget, event);
            }
        });
        content.appendChild(buttonSelect);
        return content;
    }


    /**
     * @param potentialAddresses
     *            the potential addresses to set
     */
    public void setPotentialAddresses(final List<AddressData> potentialAddresses) {
        this.addresses = potentialAddresses;
    }

    protected void handleSelectAddressClickEvent(final Widget widget, final Event event) throws Exception {
        // get selected address
        final Radiogroup radiogroupAddress = (Radiogroup)event.getTarget().getPreviousSibling();
        // format address to insert into database
        final String addressString = radiogroupAddress.getSelectedItem().getLabel();
        final String enteredAddressLabel = LabelUtils
                .getLabel(widget, TgtcsConstants.ENTERED_ADDRESS_RADIO_BUTTON_KEY_POSTFIX);
        if (!addressString.equals(enteredAddressLabel)) {
            ((AddressCustomerController)widget.getWidgetController()).updateAddressByQASFormattedAddress(
                    addressContainer, addressString);
        }
        try {
            final TypedObject address = ((CustomerController)widget.getWidgetController()).createNewCustomerAddress(
                    addressContainer,
                    configurationTypeCode);
            // auto select new address if adding address in checkout tab
            if (((TargetCallContextController)widget.getWidgetController()).isAddAddressInCheckoutTab()) {
                Listbox addressSelect = null;
                for (final TargetBasketController targetBasketController : getTargetBasketControllersContainer()
                        .getBasketControllers()) {
                    addressSelect = targetBasketController.getAddressSelectComponent();
                    if (addressSelect != null) {
                        break;
                    }
                }
                final Set<Listitem> selectedItems = new HashSet<>();
                final Listitem item = new Listitem();
                item.setValue(address);
                selectedItems.add(item);
                Events.postEvent(new SelectEvent(Events.ON_SELECT, addressSelect, selectedItems));
            }
            else {
                widget.getWidgetController().dispatchEvent(null, this, Collections.<String, Object> emptyMap());
            }
            final PostPopupAction postPopupAction = getPopupWidgetHelper().getPostPopupAction();
            if (postPopupAction != null)
            {
                postPopupAction.execute(address);
            }

            widget.getWidgetModel().notifyListeners();
            getPopupWidgetHelper().dismissCurrentPopup();
        }
        catch (final ValueHandlerException ex) {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "unableToCreateAddress"), Messagebox.OK,
                    Messagebox.ERROR);
            LOG.debug("unable to create item", ex);
        }
    }

    /**
     * @return the targetBasketControllersContainer
     */
    public TargetBasketControllersContainer getTargetBasketControllersContainer() {
        return targetBasketControllersContainer;
    }

    /**
     * @param targetBasketControllersContainer
     *            the targetBasketControllersContainer to set
     */
    @Required
    public void setTargetBasketControllersContainer(
            final TargetBasketControllersContainer targetBasketControllersContainer) {
        this.targetBasketControllersContainer = targetBasketControllersContainer;
    }


}
