/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.events.impl.ItemChangedEvent;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.impl.DefaultWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultItemWidgetModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CheckoutController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.PlaceOrderWidgetRenderer;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Collections;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.TargetCardPaymentController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;


/**
 * Target extension to {@link PlaceOrderWidgetRenderer}.
 * 
 */
public class TargetPlaceOrderWidgetRenderer extends PlaceOrderWidgetRenderer {

    private static final String B2B_ORDER_BTN_LABEL = "b2bOrderBtn";
    private static final String B2B_ORDER_BTN_SCLASS = "csPlaceOrderB2BCheckbox";

    private SessionService sessionService;

    @Override
    protected Component createSummary(final DefaultWidget<DefaultItemWidgetModel, CheckoutController> widget) {
        final Div summary = new Div();
        summary.setSclass("csPlaceOrderSummary");

        final PaymentInfoModel paymentInfo = ((TargetCardPaymentController)widget.getWidgetController())
                .getCurrentPaymentInfoModel();
        final CartModel cartModel = ((TargetCheckoutController)widget.getWidgetController())
                .getCheckoutCartByMasterCart();
        final String labelText;

        if (paymentInfo == null
                || !paymentInfo.getUser().getUid().equals(cartModel.getUser().getUid())
                || (paymentInfo instanceof CreditCardPaymentInfoModel && ((CreditCardPaymentInfoModel)paymentInfo)
                        .getSubscriptionId() == null)) {

            labelText = LabelUtils.getLabel(widget, "noPaymentsAdded", new Object[0]);
        }
        else {
            labelText = LabelUtils.getLabel(widget, "paymentsAdded", new Object[] { Integer.valueOf(1) });
        }

        final Label pmSummary = new Label(labelText);
        pmSummary.setSclass("csPaymentMethodSummaryValue");

        pmSummary.setParent(summary);

        return summary;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.PlaceOrderWidgetRenderer#handlePlaceOrderEvent(de.hybris.platform.cockpit.widgets.impl.DefaultWidget, org.zkoss.zk.ui.event.Event)
     */
    @Override
    protected void handlePlaceOrderEvent(final DefaultWidget<DefaultItemWidgetModel, CheckoutController> widget,
            final Event event)
                    throws Exception {
        try {
            final TypedObject order = (widget
                    .getWidgetController()).placeOrder();
            if (order == null) {
                return;
            }
            UISessionUtils.getCurrentSession().sendGlobalEvent(
                    new ItemChangedEvent(widget, (widget
                            .getWidgetController()).getBasketController()
                                    .getCart(),
                            null,
                            ItemChangedEvent.ChangeType.REMOVED));

            getCallContextController().setCurrentOrder(order);
            getCallContextController().dispatchEvent(null, null, Collections.EMPTY_MAP);

            widget.getWidgetController().dispatchEvent(
                    null, widget, null);

            getWidgetHelper().closeBrowser(getCheckoutBrowser());
            getSessionService().removeAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR);
        }
        catch (final Exception ex) {
            widget.getWidgetController().getBasketController().dispatchEvent(null, widget, null);
            widget.getWidgetController().dispatchEvent(
                    null, widget, null);
            Messagebox.show(ex.getMessage(),
                    Labels.getLabel(TgtcsConstants.FAILED_PLACE_ORDER, new Object[0]), 1,
                    "z-msgbox z-msgbox-error");
        }
    }

    @Override
    protected HtmlBasedComponent createContentInternal(
            final DefaultWidget<DefaultItemWidgetModel, CheckoutController> widget,
            final HtmlBasedComponent rootContainer) {
        final Div content = (Div)super.createContentInternal(widget, rootContainer);

        final Checkbox b2bSalesChannelCheckbox = new Checkbox(LabelUtils.getLabel(widget, B2B_ORDER_BTN_LABEL));
        b2bSalesChannelCheckbox.setParent(content);
        b2bSalesChannelCheckbox.setSclass(B2B_ORDER_BTN_SCLASS);
        b2bSalesChannelCheckbox
                .setChecked(BooleanUtils
                        .isTrue((Boolean)getSessionService().getAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR)));
        b2bSalesChannelCheckbox.addEventListener(Events.ON_CHECK, new EventListener() {

            @Override
            public void onEvent(final Event checkEvent) throws Exception {
                getSessionService().setAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR,
                        Boolean.valueOf(b2bSalesChannelCheckbox.isChecked()));
            }
        });
        return content;
    }

    /**
     * @return the sessionService
     */
    protected SessionService getSessionService() {
        return sessionService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }
}
