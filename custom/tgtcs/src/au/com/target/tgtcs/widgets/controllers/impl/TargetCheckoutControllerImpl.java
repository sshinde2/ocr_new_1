/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.exceptions.PaymentException;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCheckoutController;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Execution;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetInsufficientStockLevelException;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.dto.TgtHostedPaymentFormData;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCardPaymentController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * Implementation of {@link TargetCheckoutController}
 * 
 */
public class TargetCheckoutControllerImpl extends DefaultCheckoutController implements TargetCheckoutController,
        TargetCardPaymentController {
    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String VISA = "Visa";
    private static final String DINERS_CLUB = "diners_club";
    private static final String MASTERCARD = "mastercard";
    private static final String AMEX = "amex";
    private static final String DISCOVER = "DISCOVER";
    private static final String JCB = "JCB";
    private static final String AMOUNT = "amount";
    private static final String NAME_ON_CARD = "nameOnCard";
    private static final String CARD_TYPE = "cardType";
    private static final String GATEWAY_CARD_SCHEME = "gatewayCardScheme";
    private static final String GATEWAY_CARD_NUMBER = "gatewayCardNumber";
    private static final String GATEWAY_CARD_EXPIRY_DATE_MONTH = "gatewayCardExpiryDateMonth";
    private static final String GATEWAY_CARD_EXPIRY_DATE_YEAR = "gatewayCardExpiryDateYear";
    private static final String GATEWAY_CARD_SECURITY_CODE = "gatewayCardSecurityCode";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String STREET1 = "street1";
    private static final String STREET2 = "street2";
    private static final String CITY = "city";
    private static final String POSTAL_CODE = "postalCode";
    private static final String COUNTRY = "country";
    private static final String STATE = "state";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String GATEWAY_FORM_RESPONSE = "gatewayFormResponse";
    private static final String CARDTYPE_VISA = "visa";
    private static final String CARDTYPE_AMEX = "amex";
    private static final String CARDTYPE_MASTER = "master";
    private static final String CARDTYPE_DINERS = "diners";
    private static final String AMOUNT_REQUIRED = "amount is required";
    private static final String SST = "SST";
    private static final String SESSION_ID = "SessionId";
    private static final String CREDITCARD = "creditcard";

    private static final Logger LOG = Logger.getLogger(TargetCheckoutControllerImpl.class);

    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;

    private TargetPaymentService targetPaymentService;

    private PaymentModeService paymentModeService;

    private PaymentInfoModel currentPaymentInfoModel;

    private PaymentsInProgressService paymentsInProgressService;

    private TargetLaybyCartService targetLaybyCartService;

    private TargetIpgPaymentMethod paymentMethod;

    private SessionService sessionService;

    private String currentPaymentModeCode;

    /**
     * 
     * @param paymentsInProgressService
     *            the payment in progress service
     */
    @Required
    public void setPaymentsInProgressService(final PaymentsInProgressService paymentsInProgressService) {
        this.paymentsInProgressService = paymentsInProgressService;
    }

    /**
     * 
     * @return PaymentsInProgressService
     */
    protected PaymentsInProgressService getPaymentsInProgressService() {
        return paymentsInProgressService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.TargetCheckoutController#saveFlyBuysCode(de.hybris.platform.cockpit.model.meta.TypedObject) //NOPMD
     */
    @Override
    public void saveFlyBuysCode(final String flyBuysCode) throws ValueHandlerException {

        final CartModel cartModel = getCheckoutCartByMasterCart();

        if (cartModel != null) {
            boolean isFlybuysNumberValid = false;
            // set fly buys code to empty if invalid code
            try {
                isFlybuysNumberValid = targetLaybyCommerceCheckoutService.setFlybuysNumber(cartModel, flyBuysCode);
            }
            catch (final ModelSavingException e) {
                String message = e.getMessage();
                if ((e.getCause() instanceof InterceptorException)) {
                    final String interceptorMessage = e.getCause().getMessage();
                    message = (interceptorMessage != null) && (interceptorMessage.contains("]:"))
                            ? StringUtils.substringAfter(interceptorMessage, "]:") : interceptorMessage;
                }

                throw new ValueHandlerException(message, e);
            }
            if (!isFlybuysNumberValid) {
                final String message = "Invalid flybuys number " + flyBuysCode;
                throw new ValueHandlerException(message, new Exception(message));
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartModel getCheckoutCartByMasterCart() {
        return ((TargetBasketController)getBasketController()).getMasterCart();
    }

    @Override
    public void canCreatePayments() throws ValidationException {
        getCsCheckoutService().canCreatePayments(getCheckoutCartByMasterCart());
    }

    @Override
    public double getSuggestedAmountForPaymentOption() {
        return getCsCheckoutService().getUnauthorizedTotal(getCheckoutCartByMasterCart());
    }

    @Override
    public TypedObject getDefaultPaymentAddress() {
        final CartModel cart = getCheckoutCartByMasterCart();
        if (cart != null) {
            final AddressModel paymentAddress = cart.getPaymentAddress();
            if (paymentAddress != null) {
                return getCockpitTypeService().wrapItem(paymentAddress);
            }
        }
        return null;
    }

    /**
     * Set current payment information
     * 
     * @param paymentInfoModel
     *            paymentInfo Model
     */
    protected void setCurrentPaymentInfo(final PaymentInfoModel paymentInfoModel) {
        this.currentPaymentInfoModel = paymentInfoModel;
    }

    @Override
    public PaymentInfoModel getCurrentPaymentInfoModel() {
        return currentPaymentInfoModel;
    }

    @Override
    public List<TypedObject> getValidPaymentTransactions() {
        return getCockpitTypeService().wrapItems(
                getCsCheckoutService().getValidPaymentTransactions(getCheckoutCartByMasterCart()));
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.TargetCheckoutController#preLoadFlyBuysCode(de.hybris.platform.cockpit.widgets.Widget) //NOPMD
     */
    @Override
    public String preLoadFlyBuysCode() throws ValueHandlerException {

        final CartModel cartModel = getCheckoutCartByMasterCart();
        if (cartModel != null && StringUtils.isNotBlank(cartModel.getFlyBuysCode())) {
            return cartModel.getFlyBuysCode();
        }
        // fly buys code will be gotten from customer if it is valid
        final TypedObject customer = getCallContextController().getCurrentCustomer();
        if (customer != null && customer.getObject() instanceof TargetCustomerModel) {
            final TargetCustomerModel customerModel = TypeUtils.unwrapItem(customer, TargetCustomerModel.class);
            final String customerFlybuysCode = customerModel.getFlyBuysCode();

            if (cartModel != null && StringUtils.isNotBlank(customerFlybuysCode)) {
                cartModel.setFlyBuysCode(customerFlybuysCode);
                final boolean isFlybuysNumberValid = targetLaybyCommerceCheckoutService.isFlybuysNumberValid(cartModel);
                if (isFlybuysNumberValid) {
                    saveFlyBuysCode(customerFlybuysCode);
                    return customerModel.getFlyBuysCode();
                }
            }
        }
        return StringUtils.EMPTY;
    }

    @Override
    public List<TypedObject> getPreviousPaymentInfos() {
        final CartModel cart = getCheckoutCartByMasterCart();
        if (cart != null) {
            final UserModel user = cart.getUser();
            if (user != null) {
                return getCockpitTypeService().wrapItems(
                        getCsCardPaymentService().getPreviousCreditCardPaymentInfos(user));
            }
        }

        return null;
    }

    /**
     * @param targetLaybyCommerceCheckoutService
     *            the targetLaybyCommerceCheckoutService to set
     */
    @Required
    public void setTargetLaybyCommerceCheckoutService(
            final TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService) {
        this.targetLaybyCommerceCheckoutService = targetLaybyCommerceCheckoutService;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @param paymentModeService
     *            the paymentModeService to set
     */
    @Required
    public void setPaymentModeService(final PaymentModeService paymentModeService) {
        this.paymentModeService = paymentModeService;
    }

    /**
     * save billing address
     * 
     * @param billingAddress
     *            billing address
     * @param billingInfo
     *            billing info
     */
    private void saveBillingAddress(final TargetAddressModel billingAddress,
            final BillingInfo billingInfo) {

        billingAddress.setFirstname(billingInfo.getFirstName());
        billingAddress.setLastname(billingInfo.getLastName());
        billingAddress.setLine1(billingInfo.getStreet1());
        billingAddress.setLine2(billingInfo.getStreet2());
        billingAddress.setTown(billingInfo.getCity());
        billingAddress.setDistrict(billingInfo.getState());
        billingAddress.setPostalcode(billingInfo.getPostalCode());
        billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
        billingAddress.setPhone1(billingInfo.getPhoneNumber());
        billingAddress.setBillingAddress(Boolean.TRUE);
    }

    /**
     * handle payment form submit response. the form is submitted to TNS server in order to generate tns form session.
     * if the session is created successfully, the token will be generated in this call and either payment or refund
     * will happend using the new credit card information
     * 
     * @param execution
     *            the current execution
     * @return list of messages
     */
    public List<String> handlePaymentFormResponse(final Execution execution) {
        final ArrayList<String> results = new ArrayList<>();
        results.add(FAILURE); //by default, we assume the following operation will fails
        if (execution != null) {
            final TgtHostedPaymentFormData data = createTargetHostedPaymentFormData(execution);
            final CartModel cartModel = getCheckoutCartByMasterCart();
            final String operationType = execution.getParameter("paymentType");
            int operationTypeValue = 0; //default, just make token
            try {
                operationTypeValue = Integer.parseInt(operationType);
            }
            catch (final NumberFormatException e) {
                LOG.error(e);
            }
            validate(data, operationTypeValue);
            cleanupTNSFieldValues(data);
            if (data.getMessages().size() == 0) {
                //everything is ok, no validation issue
                final CardInfo cardInfo = new CardInfo();
                final PaymentOption paymentOption = new PaymentOption(cardInfo);
                paymentOption.setAmount(Double.parseDouble(execution.getParameter("amount")));
                cardInfo.setCardHolderFullName(execution.getParameter("nameOnCard"));
                final String cardname = execution.getParameter("gatewayCardScheme");
                cardInfo.setCardType(getCreditCardType(cardname));
                cardInfo.setCardNumber(execution.getParameter("gatewayCardNumber"));
                cardInfo.setExpirationMonth(Integer.valueOf(execution.getParameter("gatewayCardExpiryDateMonth")));
                cardInfo.setExpirationYear(Integer.valueOf(execution.getParameter("gatewayCardExpiryDateYear")));
                cardInfo.setCv2Number(execution.getParameter("gatewayCardSecurityCode"));
                final BillingInfo billingInfo = createBillingInfo(execution);
                cardInfo.setBillingInfo(billingInfo);
                final String paymentSessionId = execution.getParameter("paymentSessionId");
                String savedToken = null;
                try {
                    savedToken = targetPaymentService.tokenize(paymentSessionId,
                            paymentModeService.getPaymentModeForCode("creditcard"));
                }
                catch (final Exception e) {
                    LOG.error(e);
                }
                if (savedToken != null) {
                    execution.setAttribute("paymentToken", savedToken);
                    final CreditCardPaymentInfoModel cardPaymentInfoModel = createCreditCardPaymentInfoModel(cardInfo,
                            savedToken);
                    if (operationTypeValue == TgtcsAddPaymentWindowController.PAYMENT_OPERATION_TYPE_TOKENIZE) {
                        results.set(0, SUCCESS);
                        results.add("New payment option is added successfully.");
                        setCurrentPaymentInfo(cardPaymentInfoModel);
                    }
                    else { //do capture
                        final BigDecimal totalAmount = BigDecimal.valueOf(paymentOption.getAmount());
                        //do capture
                        final PaymentTransactionEntryModel paymentTransactionModelEntry = targetPaymentService.capture(
                                cartModel,
                                cardPaymentInfoModel, totalAmount, cartModel.getCurrency(),
                                PaymentCaptureType.PLACEORDER);
                        if (paymentTransactionModelEntry == null) {
                            LOG.error("targe cs cockpit", new PaymentException(
                                    "paymentTransactionModelEntry from payment service is null"));
                            try {
                                paymentsInProgressService.removeInProgressPayment(cartModel);
                            }
                            catch (final Exception e) {
                                LOG.info(e); //error may happen if there are more than one payment for the same order found
                            }
                            results.add("payment failed: paymentTransactionModelEntry from payment service is null");
                        }
                        // If it was rejected then immediately return failure, otherwise we will continue and check the capture later if needed.
                        else if (TransactionStatus.REJECTED.toString().equals(
                                paymentTransactionModelEntry.getTransactionStatus())) {
                            try {
                                paymentsInProgressService.removeInProgressPayment(cartModel);
                            }
                            catch (final Exception e) {
                                LOG.info(e); //error may happen if there are more than one payment for the same order found
                            }
                            results.add("Failed to capture, transaction is rejected by TNS payment provider");
                        }
                        else if (TransactionStatus.REVIEW.toString().equals(
                                paymentTransactionModelEntry.getTransactionStatus())) {
                            results.set(0, SUCCESS);
                            results.add("transaction went though and it is under Review status now");
                        }
                        else if (TransactionStatus.ERROR.toString().equals(
                                paymentTransactionModelEntry.getTransactionStatus())) {
                            try {
                                paymentsInProgressService.removeInProgressPayment(cartModel);
                            }
                            catch (final Exception e) {
                                LOG.info(e); //error may happen if there are more than one payment for the same order found
                            }
                            results.add("Failed to capture, Error happens with transaction");
                        }
                        else {
                            results.set(0, SUCCESS);
                            results.add("payment is successful, transaction reference number" + ":"
                                    + paymentTransactionModelEntry.getReceiptNo()
                                    + "<br> You can close the window now.");
                        }
                    } //end of do capture
                } // end if savedToken != null
                else { // token can not generated
                    results.add("Failed to generate token by TNS payment provider");
                }
            } // no  validation messages
            else { // has validation errors
                for (final Message message : data.getMessages()) {
                    results.add(message.getMessageText());
                }
            }
        } // end of execuation is not null
        return results;
    }


    /**
     * Method to handle IPGPayment form response
     * 
     * @param execution
     * @return List<String>
     */
    public List<String> handleIPGPaymentFormResponse(final Execution execution) {
        final ArrayList<String> results = new ArrayList<>();
        final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest = new TargetQueryTransactionDetailsRequest();
        TargetQueryTransactionDetailsResult queryTransactionDetailsResult = null;
        try {

            targetQueryTransactionDetailsRequest.setSessionId(execution.getParameter(SESSION_ID));
            targetQueryTransactionDetailsRequest.setSessionToken(execution.getParameter(SST));
            queryTransactionDetailsResult = paymentMethod
                    .queryTransactionDetails(targetQueryTransactionDetailsRequest);

            if (queryTransactionDetailsResult.isSuccess()) {
                final IpgPaymentInfoModel ipgPayment = new IpgPaymentInfoModel();
                saveInitialIpgPaymentInfo(execution, ipgPayment);
                setCurrentPaymentInfo(ipgPayment);
                getSessionService().setAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY,
                        queryTransactionDetailsResult);
                results.add(SUCCESS + " : Card payment reserved successfully");

            }
            else {
                createFailureResult(results, queryTransactionDetailsResult);
            }
        }
        catch (final Exception exception) {
            createFailureResult(results, queryTransactionDetailsResult);
            LOG.error(exception.getMessage() + "::Cart Id :" + getCheckoutCartByMasterCart().getCode());
        }
        return results;
    }

    /**
     * @param results
     * @param queryTransactionDetailsResult
     */
    private void createFailureResult(final ArrayList<String> results,
            final TargetQueryTransactionDetailsResult queryTransactionDetailsResult) {
        String failureDescription = "Adding Payment unsuccessful";
        results.add(FAILURE);
        if (queryTransactionDetailsResult != null
                && StringUtils.isNotEmpty(queryTransactionDetailsResult.getErrorDescription())) {
            failureDescription = queryTransactionDetailsResult.getErrorDescription();
        }
        results.add("error description:" + failureDescription);
    }

    /**
     * @param execution
     * @param ipgPayment
     */
    private void saveInitialIpgPaymentInfo(final Execution execution, final IpgPaymentInfoModel ipgPayment) {
        ipgPayment.setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        ipgPayment.setSessionId(execution.getParameter(SESSION_ID));
        ipgPayment.setToken(execution.getParameter(SST));
        final CartModel cartModel = getCheckoutCartByMasterCart();
        ipgPayment.setUser(cartModel.getUser());
        ipgPayment.setBillingAddress(cartModel.getPaymentAddress());
        cartModel.setPaymentInfo(ipgPayment);
        ipgPayment.setCode(cartModel.getUser().getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        getModelService().save(ipgPayment);
        getModelService().save(cartModel);

    }

    /**
     * according to card type code, get a credit card type object
     * 
     * @param creditCardType
     *            the credit card type
     * @return a CreditCardType Object
     */
    private CreditCardType getCreditCardType(final String creditCardType) {
        if (AMEX.equalsIgnoreCase(creditCardType)) {
            return CreditCardType.AMEX;
        }
        else if (MASTERCARD.equalsIgnoreCase(creditCardType)) {
            return CreditCardType.MASTER;
        }
        else if (DINERS_CLUB.equalsIgnoreCase(creditCardType)) {
            return CreditCardType.DINERS;
        }
        else if (VISA.equalsIgnoreCase(creditCardType)) {
            return CreditCardType.VISA;
        }
        else if (DISCOVER.equalsIgnoreCase(creditCardType)) {
            return null; // no CreditCardType.DISCOVER exist
        }
        else if (JCB.equalsIgnoreCase(creditCardType)) {
            return null; // no CreditCardType.JCB
        }
        return null;

    }

    /**
     * 
     * @param data
     *            target hosted form data
     * @param operationTypeValue
     *            operation type: 0 or 1
     */
    private void validate(final TgtHostedPaymentFormData data, final int operationTypeValue) {
        final List<Message> messages = new ArrayList<>();
        validateBillingAddress(data, messages);
        if (operationTypeValue == TgtcsAddPaymentWindowController.PAYMENT_OPERATION_TYPE_CAPTURE) {
            try {
                validateAmount(Double.parseDouble(data.getAmount()), messages);
            }
            catch (final NumberFormatException e) {
                messages.add(new Message(Message.ERROR, AMOUNT_REQUIRED, AMOUNT));
            }
        }
        validateCardInfoGeneral(data, messages);
        validateTNSFields(data, messages);

        data.setMessages(messages);
    }

    /**
     * 
     * @param data
     *            target hosted payment data
     * @param messages
     *            a list of error messages
     * 
     */
    private void validateCardInfoGeneral(final TgtHostedPaymentFormData data, final List<Message> messages) {
        // Validate Cardholder name
        if (StringUtils.isBlank(data.getCardHolderFullName())) {
            messages.add(new Message(Message.ERROR, "Card Holder Full Name Required", "nameOnCard"));
        }

        if (StringUtils.isBlank(data.getCardType())) {
            messages.add(new Message(Message.ERROR, "Card Type Required", "cardType"));
        }

        //add validation to only accept visa, amex, master and diners_club on this site
        if (!CARDTYPE_VISA.equalsIgnoreCase(data.getCardType()) && !CARDTYPE_AMEX.equalsIgnoreCase(data.getCardType())
                && !CARDTYPE_MASTER.equalsIgnoreCase(data.getCardType())
                && !CARDTYPE_DINERS.equalsIgnoreCase(data.getCardType())) {
            messages.add(new Message(Message.ERROR, "Card Type Unsupported", "cardType"));
        }
    }

    /**
     * 
     * @param data
     *            target hosted payment data
     * @param messages
     *            a list of error messages
     * 
     */
    private void validateBillingAddress(final TgtHostedPaymentFormData data, final List<Message> messages) {
        if (StringUtils.isBlank(data.getFirstName())) {
            messages.add(new Message(Message.ERROR, "First Name Required", "firstName"));
        }
        if (StringUtils.isBlank(data.getLastName())) {
            messages.add(new Message(Message.ERROR, "Last Name Required", "lastName"));
        }
        if (StringUtils.isBlank(data.getStreetAddress())) {
            messages.add(new Message(Message.ERROR, "Street Required", "street1"));
        }
        if (StringUtils.isBlank(data.getCity())) {
            messages.add(new Message(Message.ERROR, "City/Suburb Required", "city"));
        }
        if (StringUtils.isBlank(data.getState())) {
            messages.add(new Message(Message.ERROR, "State Required", "state"));
        }
        if (StringUtils.isBlank(data.getCountry())) {
            messages.add(new Message(Message.ERROR, "Country Required", "country"));
        }
        if (StringUtils.isBlank(data.getPostalCode())) {
            messages.add(new Message(Message.ERROR, "Postal Code Required", "postalCode"));
        }
    }

    /**
     * 
     * @param amount
     *            amount
     * @param messages
     *            a list of error messages
     */
    private void validateAmount(final double amount, final List<Message> messages) {

        final CartModel cartModel = getCheckoutCartByMasterCart();
        final double roundedAmount = getCommonI18NService().roundCurrency(amount,
                cartModel.getCurrency().getDigits().intValue());
        final BigDecimal amountToCapture = BigDecimal.valueOf(roundedAmount);

        if (amountToCapture.compareTo(BigDecimal.ZERO) <= 0) {
            messages.add(new Message(Message.ERROR, "amount must not be zero", "amount"));
        }

        //also amountToCapture can not be bigger than what is unauthorized
        if (roundedAmount > getSuggestedAmountForPaymentOption()) {
            messages.add(new Message(Message.ERROR, "amount can not be greater than uncaptured amount:"
                    + getSuggestedAmountForPaymentOption(), "amount"));
        }
    }

    /**
     * 
     * @param fieldName
     *            field name
     * @param value
     *            field value
     * @param label
     *            field label
     * @param messages
     *            a list of error messages
     */
    private void validateTNSField(final String fieldName, final String value, final String label,
            final List<Message> messages) {
        if (!StringUtils.isBlank(value)) {
            if (value.startsWith("1~")) {
                messages.add(new Message(Message.ERROR, label + " Required", fieldName));
            }
            else if (value.startsWith("2~")) {
                messages.add(new Message(Message.ERROR, "Invalid " + label, fieldName));
            }
        }
    }

    /**
     * 
     * @param data
     *            target hosted form data
     * @param messages
     *            a list of error messages
     */
    private void validateTNSFields(final TgtHostedPaymentFormData data, final List<Message> messages) {
        validateTNSField("gatewayCardNumber", data.getCardNumber(), "Card Number", messages);
        validateTNSField("gatewayCardExpiryDateMonth", data.getCardExpiryMonth(), "Card Expiry Month", messages);
        validateTNSField("gatewayCardExpiryDateYear", data.getCardExpiryYear(), "Card Expiry Year", messages);
        validateTNSField("gatewayCardSecurityCode", data.getCardSecurityCode(), "Card Security Code", messages);

        final String cardType = data.getCardScheme();

        if (!DINERS_CLUB.equalsIgnoreCase(cardType)
                && data.getCardSecurityCode() != null && "".equals(data.getCardSecurityCode())) {
            messages.add(new Message(Message.ERROR, "Card Security Code Required", "gatewayCardSecurityCode"));
        }

        if (StringUtils.isBlank(data.getGatewayFormResponse())) {
            messages.add(new Message(Message.ERROR, "Missing Gateway Form Response", "gatewayFormResponse"));
        }
        else if (data.getGatewayFormResponse().startsWith("2~")) {
            messages.add(new Message(
                    Message.ERROR,
                    "Form Session Id Invalid. "
                            + "To submit another payment, please close the current payment window and open a new one. ",
                    "gatewayFormResponse"));
            LOG.info("Session Key has expired - getting new one");
        }
        else if (data.getGatewayFormResponse().startsWith("3~")) {
            messages.add(new Message(Message.ERROR, "TNS Field Errors", "gatewayFormResponse"));
        }
        else if (data.getGatewayFormResponse().startsWith("4~")) {
            messages.add(new Message(Message.ERROR, "TNS System Error", "gatewayFormResponse"));
        }
    }

    /**
     * 
     * @param data
     *            target hosted payment form data
     */
    private void cleanupTNSFieldValues(final TgtHostedPaymentFormData data) {
        data.setCardExpiryMonth(cleanupTNSFieldValue(data.getCardExpiryMonth()));
        data.setCardExpiryYear(cleanupTNSFieldValue(data.getCardExpiryYear()));
        data.setCardNumber(cleanupTNSFieldValue(data.getCardNumber()));
        data.setCardSecurityCode(cleanupTNSFieldValue(data.getCardSecurityCode()));
    }

    /**
     * 
     * @param value
     *            raw value
     * @return cleaned up value
     */
    private String cleanupTNSFieldValue(final String value) {
        if (!StringUtils.isBlank(value) && (value.startsWith("1~")
                || value.startsWith("2~") || value.startsWith("3~"))) {
            return value.substring(2);
        }
        return value;
    }

    /**
     * 
     * @param execution
     *            the current execution
     * @return target hosted payment form data
     */
    private TgtHostedPaymentFormData createTargetHostedPaymentFormData(final Execution execution) {
        final TgtHostedPaymentFormData data = new TgtHostedPaymentFormData();
        data.setAmount(execution.getParameter(AMOUNT));
        data.setCardHolderFullName(execution.getParameter(NAME_ON_CARD));
        data.setCardType(execution.getParameter(CARD_TYPE));
        data.setCardScheme(execution.getParameter(GATEWAY_CARD_SCHEME));
        data.setCardNumber(execution.getParameter(GATEWAY_CARD_NUMBER));
        data.setCardExpiryMonth(execution.getParameter(GATEWAY_CARD_EXPIRY_DATE_MONTH));
        data.setCardExpiryYear(execution.getParameter(GATEWAY_CARD_EXPIRY_DATE_YEAR));
        data.setCardSecurityCode(execution.getParameter(GATEWAY_CARD_SECURITY_CODE));
        data.setFirstName(execution.getParameter(FIRST_NAME));
        data.setLastName(execution.getParameter(LAST_NAME));
        data.setStreetAddress(execution.getParameter(STREET1));
        data.setStreetAddress2(execution.getParameter(STREET2));
        data.setCity(execution.getParameter(CITY));
        data.setPostalCode(execution.getParameter(POSTAL_CODE));
        data.setCountry(execution.getParameter(COUNTRY));
        data.setState(execution.getParameter(STATE));
        data.setPhoneNumber(execution.getParameter(PHONE_NUMBER));
        data.setGatewayFormResponse(execution.getParameter(GATEWAY_FORM_RESPONSE));
        return data;
    }

    /**
     * 
     * @param execution
     *            the current execution
     * @return BillingInfo
     */
    private BillingInfo createBillingInfo(final Execution execution) {
        final BillingInfo billingInfo = new BillingInfo();

        billingInfo.setFirstName(execution.getParameter(FIRST_NAME));
        billingInfo.setLastName(execution.getParameter(LAST_NAME));
        billingInfo.setStreet1(execution.getParameter(STREET1));
        billingInfo.setStreet2(execution.getParameter(STREET2));
        billingInfo.setCity(execution.getParameter(CITY));
        billingInfo.setPostalCode(execution.getParameter(POSTAL_CODE));
        billingInfo.setCountry(getCountryIsoCode(execution.getParameter(COUNTRY)));
        billingInfo.setState(execution.getParameter(STATE));
        billingInfo.setPhoneNumber(execution.getParameter(PHONE_NUMBER));
        return billingInfo;
    }

    private String getCountryIsoCode(final String countryName) {
        final List<CountryModel> countries = getCommonI18NService().getAllCountries();
        for (final CountryModel country : countries) {
            if (country.getName().equals(countryName)) {
                return country.getIsocode();
            }
        }
        return null;
    }

    /**
     * 
     * @param cardInfo
     *            card info
     * @param savedToken
     *            payment token
     * @return CreditCardPaymentInfoModel
     */
    private CreditCardPaymentInfoModel createCreditCardPaymentInfoModel(final CardInfo cardInfo,
            final String savedToken) {
        final CartModel cartModel = getCheckoutCartByMasterCart();

        final CreditCardPaymentInfoModel cardPaymentInfoModel = getModelService().create(
                CreditCardPaymentInfoModel.class);
        cardPaymentInfoModel.setCode(cartModel.getUser().getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        cardPaymentInfoModel.setUser(cartModel.getUser());

        cardPaymentInfoModel.setSubscriptionId(savedToken);

        cardPaymentInfoModel.setNumber(cardInfo.getCardNumber());
        cardPaymentInfoModel.setType(cardInfo.getCardType());
        cardPaymentInfoModel.setCcOwner(cardInfo.getCardHolderFullName());
        cardPaymentInfoModel.setValidToMonth(String.valueOf(cardInfo.getExpirationMonth()));
        cardPaymentInfoModel.setValidToYear(String.valueOf(cardInfo.getExpirationYear()));
        cardPaymentInfoModel.setSaved(false);
        final TargetAddressModel billingAddress = getModelService().create(TargetAddressModel.class);
        saveBillingAddress(billingAddress, cardInfo.getBillingInfo());
        billingAddress.setOwner(cardPaymentInfoModel);
        cardPaymentInfoModel.setBillingAddress(billingAddress);
        return cardPaymentInfoModel;
    }

    @Override
    public TypedObject placeOrder() throws ValidationException {
        final CartModel checkoutCart = getCheckoutCartByMasterCart();

        if (currentPaymentInfoModel != null
                && currentPaymentInfoModel.getUser().getUid().equals(checkoutCart.getUser().getUid())) {
            checkoutCart.setPaymentInfo(currentPaymentInfoModel);
            String paymentModeCode = CREDITCARD;
            if (currentPaymentInfoModel instanceof IpgPaymentInfoModel) {
                paymentModeCode = getCurrentPaymentModeCode();
            }
            targetLaybyCommerceCheckoutService.setPaymentMode(checkoutCart,
                    paymentModeService.getPaymentModeForCode(paymentModeCode));
        }

        final ImpersonationContext context = createImpersonationContext(checkoutCart);

        try {
            final OrderModel order = (OrderModel)getImpersonationService().executeInContext(context,
                    new ImpersonationService.Executor() {
                        @Override
                        public OrderModel execute()
                                throws ValidationException {
                            return getCsCheckoutService().doCheckout(checkoutCart);
                        }
                    });

            if (order != null) {
                targetLaybyCartService.removeSessionCart();
                return getCockpitTypeService().wrapItem(order);
            }
        }
        catch (final ValidationException e) {
            if (e.getCause() instanceof TargetInsufficientStockLevelException) {
                final AbstractOrderEntryModel cartEntry = ((TargetInsufficientStockLevelException)e.getCause())
                        .getCartEntry();
                try {
                    updateOrderEntryQuantity(cartEntry);
                }
                catch (final CommerceCartModificationException ex) {
                    LOG.warn(ex.getMessage());
                }
            }
            throw e;
        }
        catch (final Throwable e) {
            LOG.error(e.getMessage());
        }
        return null;

    }

    private void updateOrderEntryQuantity(final AbstractOrderEntryModel cartEntry)
            throws CommerceCartModificationException {
        final TypedObject wEntry = getCockpitTypeService().wrapItem(cartEntry);
        ((TargetBasketController)getBasketController()).setQuantityToSku(wEntry, cartEntry.getQuantity()
                .longValue(), null);
    }

    /**
     * @param targetLaybyCartService
     *            the targetLaybyCartService to set
     */
    @Required
    public void setTargetLaybyCartService(final TargetLaybyCartService targetLaybyCartService) {
        this.targetLaybyCartService = targetLaybyCartService;
    }

    /**
     * @return the sessionService
     */
    public SessionService getSessionService() {
        return sessionService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }


    /**
     * @param paymentMethod
     *            the paymentMethod to set
     */
    @Required
    public void setPaymentMethod(final TargetIpgPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return the currentPaymentModeCode
     */
    public String getCurrentPaymentModeCode() {
        return currentPaymentModeCode;
    }

    /**
     * @param currentPaymentModeCode
     *            the currentPaymentMode to set
     */
    @Override
    public void setCurrentPaymentModeCode(final String currentPaymentModeCode) {
        this.currentPaymentModeCode = currentPaymentModeCode;
    }


}
