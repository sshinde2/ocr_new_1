/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.cockpit.widgets.models.ListWidgetModel;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.ObjectGetValueUtils;
import de.hybris.platform.cscockpit.widgets.controllers.ReturnsController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.RefundConfirmationWidgetRenderer;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.refund.TargetOrderRefundException;
import au.com.target.tgtcs.components.orderdetail.utils.DiscountUtil;


/**
 * @author Nandini
 * 
 */
public class TgtRefundConfirmationWidgetRenderer extends RefundConfirmationWidgetRenderer {
    private static final String FAILED_TO_RENDER_RETURN_ENTRIES_LIST = "failed to render return entries list";
    private static final String WIDTH_BASE_PRICE = "80px";
    private static final String WIDTH_QTY = "40px";
    private static final String WIDTH_REF_QTY = "40px";
    private static final String WIDTH_DISCOUNT = "40px";
    private static final String WIDTH_TOTAL_PRICE = "80px";
    private static final String WIDTH_PRODUCT = "300px";
    private static final String ABSTRACT_ORDER_ENTRY_QUANTITY = "AbstractOrderEntry.quantity";
    private static final String ABSTRACT_ORDER_ENTRY_TOTAL_PRICE = "AbstractOrderEntry.totalPrice";
    private static final String ABSTRACT_ORDER_ENTRY_BASE_PRICE = "AbstractOrderEntry.basePrice";
    private static final String LISTBOX_ROW_ITEM = "listbox-row-item";
    private static final String TOTAL_PRICE = "totalPrice";
    private static final String DISCOUNT = "discount";
    private static final String REF_QTY = "refQty";
    private static final String QTY = "qty";
    private static final String BASE_PRICE = "basePrice";
    private static final String PRODUCT = "product";
    private static final Logger LOG = Logger.getLogger(TgtRefundConfirmationWidgetRenderer.class);

    @Override
    protected void renderListbox(final Listbox listBox,
            final ListboxWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget,
            final HtmlBasedComponent rootContainer) {
        final ListWidgetModel widgetModel = widget.getWidgetModel();
        if (widgetModel == null) {
            return;
        }
        final TypedObject refundOrder = widget.getWidgetController().getRefundOrderPreview();
        if (refundOrder == null) {
            return;
        }
        final OrderModel refundOrderModel = (OrderModel)refundOrder.getObject();

        if ((refundOrderModel.getEntries() == null) || (refundOrderModel.getEntries().isEmpty())) {
            return;
        }
        try {
            final List columns = getColumnConfigurations();
            Listheader listheader;
            if (CollectionUtils.isNotEmpty(columns)) {
                final Listhead headRow = new Listhead();
                headRow.setParent(listBox);
                final Listheader colProductHeader = new Listheader(
                        LabelUtils.getLabel(widget, PRODUCT, new Object[0]));
                colProductHeader.setWidth(WIDTH_PRODUCT);
                colProductHeader.setParent(headRow);
                listheader = new Listheader(LabelUtils.getLabel(widget, BASE_PRICE, new Object[0]));
                listheader.setWidth(WIDTH_BASE_PRICE);
                headRow.appendChild(listheader);
                listheader = new Listheader(LabelUtils.getLabel(widget, QTY, new Object[0]));
                listheader.setWidth(WIDTH_QTY);
                headRow.appendChild(listheader);
                listheader = new Listheader(LabelUtils.getLabel(widget, REF_QTY, new Object[0]));
                listheader.setWidth(WIDTH_REF_QTY);
                headRow.appendChild(listheader);
                listheader = new Listheader(LabelUtils.getLabel(widget, DISCOUNT, new Object[0]));
                listheader.setWidth(WIDTH_DISCOUNT);
                headRow.appendChild(listheader);
                listheader = new Listheader(LabelUtils.getLabel(widget, TOTAL_PRICE, new Object[0]));
                listheader.setWidth(WIDTH_TOTAL_PRICE);
                headRow.appendChild(listheader);
            }
            final List<TypedObject> items = getCockpitTypeService().wrapItems(refundOrderModel.getEntries());
            for (final TypedObject item : items) {
                final Listitem row = new Listitem();
                row.setParent(listBox);
                row.setSclass(LISTBOX_ROW_ITEM);
                final Listcell productCell = new Listcell();
                productCell.setParent(row);
                final Div productDiv = new Div();
                productDiv.setParent(productCell);
                getPropertyRendererHelper().buildPropertyValuesFromColumnConfigs(item, columns, productDiv);
                final PropertyDescriptor basePricePD = getCockpitTypeService().getPropertyDescriptor(
                        ABSTRACT_ORDER_ENTRY_BASE_PRICE);
                final PropertyDescriptor totalPricePD = getCockpitTypeService().getPropertyDescriptor(
                        ABSTRACT_ORDER_ENTRY_TOTAL_PRICE);
                final PropertyDescriptor qtyPD = getCockpitTypeService()
                        .getPropertyDescriptor(ABSTRACT_ORDER_ENTRY_QUANTITY);
                final ObjectValueContainer valueContainer = getValueContainer(item,
                        Arrays.asList(new PropertyDescriptor[] { basePricePD, totalPricePD, qtyPD }));
                final CurrencyModel cartCurrencyModel = refundOrderModel.getCurrency();
                final NumberFormat currencyInstance = (NumberFormat)getSessionService().executeInLocalView(
                        new SessionExecutionBody() {
                            @Override
                            public Object execute() {
                                TgtRefundConfirmationWidgetRenderer.this.getCommonI18NService().setCurrentCurrency(
                                        cartCurrencyModel);
                                return TgtRefundConfirmationWidgetRenderer.this.getFormatFactory()
                                        .createCurrencyFormat();
                            }
                        });
                final Double basePriceValue = ObjectGetValueUtils.getDoubleValue(valueContainer, basePricePD);
                final String basePriceString = (basePriceValue != null) ? currencyInstance.format(basePriceValue)
                        : StringUtils.EMPTY;
                row.appendChild(new Listcell(basePriceString));
                final Long qty = ObjectGetValueUtils.getLongValue(valueContainer, qtyPD);
                final String qtyString = (qty != null) ? qty.toString() : StringUtils.EMPTY;
                row.appendChild(new Listcell(qtyString));
                final Long refQty = getRefundQTy((OrderEntryModel)item.getObject());
                final String refQtyString = (refQty != null) ? refQty.toString() : StringUtils.EMPTY;
                row.appendChild(new Listcell(refQtyString));
                row.appendChild(new Listcell(
                        String.valueOf(DiscountUtil.getDiscount((OrderEntryModel)item.getObject()))));
                final Double totalPriceValue = ObjectGetValueUtils.getDoubleValue(valueContainer, totalPricePD);
                final String totalPriceString = (totalPriceValue != null) ? currencyInstance.format(totalPriceValue)
                        : StringUtils.EMPTY;
                row.appendChild(new Listcell(totalPriceString));
            }
        }
        catch (final Exception e) {
            LOG.error(FAILED_TO_RENDER_RETURN_ENTRIES_LIST, e);
        }
    }

    /**
     * 
     * @param oem
     *            The OrderEntryModel
     * @return expected quantity
     */
    protected Long getRefundQTy(final OrderEntryModel oem) {
        final List<ReturnEntryModel> rel = oem.getOrder().getReturnRequests().get(0).getReturnEntries();
        for (final ReturnEntryModel rem : rel) {
            if (rem.getOrderEntry() == oem) {
                return rem.getExpectedQuantity();
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.RefundConfirmationWidgetRenderer#handleRefundConfirmedEvent(de.hybris.platform.cockpit.widgets.ListboxWidget, org.zkoss.zk.ui.event.Event)
     */
    @Override
    protected void handleRefundConfirmedEvent(
            final ListboxWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget, final Event event)
            throws Exception
    {
        try {
            final TypedObject returnRequest = widget.getWidgetController().createRefundRequest();
            if (returnRequest != null)
            {
                if (getPopupWidgetHelper().getCurrentPopup() != null)
                {
                    final List<Component> children = getPopupWidgetHelper().getCurrentPopup().getParent().getChildren();
                    for (final Component c : children)
                    {
                        if (!(c instanceof Window)) {
                            continue;
                        }
                        Events.postEvent(new Event("onClose", c));
                    }
                }

                final ReturnRequestModel returnRequestModel = (ReturnRequestModel)returnRequest.getObject();
                Messagebox.show(LabelUtils.getLabel(widget, "rmaNumber", new Object[] { returnRequestModel.getRMA() }),
                        LabelUtils.getLabel(widget, "rmaNumberTitle", new Object[0]), 1,
                        "z-msgbox z-msgbox-information");


                widget.getWidgetController().dispatchEvent(null, widget, null);
            }
            else
            {
                Messagebox.show(LabelUtils.getLabel(widget, "error", new Object[0]),
                        LabelUtils.getLabel(widget, "failed", new Object[0]), 1,
                        "z-msgbox z-msgbox-error");
            }
        }
        catch (final TargetOrderRefundException e)
        {
            Messagebox.show(
                    e.getMessage()
                            + ((e.getCause() == null) ? "" : new StringBuilder(" - ").append(e.getCause().getMessage())
                                    .toString()),
                    LabelUtils.getLabel(widget, "failed", new Object[0]), 1, "z-msgbox z-msgbox-error");
        }

    }



}
