/**
 * 
 */
package au.com.target.tgtcs.widgets.models.impl;

import de.hybris.platform.cscockpit.widgets.models.impl.ShippingCartAddressWidgetModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;


/**
 * @author Roman_Skrypal
 * 
 */
public class TargetShippingCartAddressWidgetModel extends ShippingCartAddressWidgetModel {

    private final Map<String, Double> deliveryCost = new HashedMap();

    private List<String> supportedDeliveryModesForOrder = new ArrayList<>();

    public boolean setDeliveryCost(final Map<String, Double> delivery) {
        boolean changed = false;
        for (final String code : delivery.keySet()) {
            if (deliveryCost.get(code) == null || !deliveryCost.get(code).equals(delivery.get(code))) {
                deliveryCost.put(code, delivery.get(code));
                changed = true;
            }
        }
        return changed;
    }


    public boolean setSupportedDeliveryModesForOrder(final List<String> deliveryModes) {
        if (CollectionUtils.isEqualCollection(deliveryModes, supportedDeliveryModesForOrder)) {
            return false;
        }
        else {
            supportedDeliveryModesForOrder = deliveryModes;
        }
        return true;
    }
}
