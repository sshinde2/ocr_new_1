package au.com.target.tgtcs.widgets.renderers.utils.edit;

import de.hybris.platform.cockpit.model.editor.EditorHelper;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.session.impl.CreateContext;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.CssUtils;
import de.hybris.platform.cscockpit.utils.PropertyEditorHelper;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.PojoPropertyRendererUtil;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;


public final class TargetPojoPropertyRendererUtil {
    protected static final String CSS_MANDATORY = "mandatory";
    private static final String CSS_EDITOR_WIDGET_EDITOR_LABEL = "editorWidgetEditorLabel";


    private TargetPojoPropertyRendererUtil() {
    }

    public static void renderEditor(final org.zkoss.zk.ui.HtmlBasedComponent parent,
            final PropertyDescriptor propertyDescriptor,
            final EditorRowConfiguration rowConfig, final TypedObject item,
            final ObjectValueContainer objectValueContainer,
            final Widget widget, final boolean createLabel)
    {
        if (rowConfig != null && rowConfig.isVisible())
        {
            final Hbox hbox = new Hbox();
            hbox.setParent(parent);
            hbox.setWidth("96%");
            hbox.setWidths("9em, none");

            if (createLabel)
            {
                final Label propLabel = new Label(getPropertyDescriptorName(propertyDescriptor));
                propLabel.setParent(hbox);
                propLabel.setSclass(CSS_EDITOR_WIDGET_EDITOR_LABEL);
                if (propertyDescriptor.getOccurence().equals(PropertyDescriptor.Occurrence.REQUIRED)) {
                    propLabel.setValue(propLabel.getValue() + "*");
                    propLabel.setSclass(CssUtils.combine(propLabel.getSclass(), CSS_MANDATORY));
                }
            }

            final CreateContext ctx = new CreateContext(null, item, propertyDescriptor, null);
            ctx.setExcludedTypes(EditorHelper.parseTemplateCodes(rowConfig.getParameter("excludeCreateTypes"),
                    UISessionUtils
                            .getCurrentSession().getTypeService()));
            ctx.setAllowedTypes(EditorHelper.parseTemplateCodes(rowConfig.getParameter("restrictToCreateTypes"),
                    UISessionUtils
                            .getCurrentSession().getTypeService()));

            final Map<String, Object> params = new HashMap<String, Object>();
            params.putAll(rowConfig.getParameters());
            params.put("createContext", ctx);
            params.put(EditorHelper.EVENT_SOURCE, widget);

            PojoPropertyRendererUtil.renderSingleEditor(null, propertyDescriptor, hbox, objectValueContainer, true,
                    rowConfig
                            .getEditor(), params, null, false, new PropertyEditorHelper.EditorInputListener(
                            objectValueContainer,
                            propertyDescriptor, null));
        }
    }

    private static String getPropertyDescriptorName(final PropertyDescriptor propertyDescriptor)
    {
        if (propertyDescriptor != null)
        {
            final String text = propertyDescriptor.getName();
            if (text == null || text.isEmpty())
            {
                // Fallback to qualifier instead of name
                return "[" + propertyDescriptor.getQualifier() + "]";
            }
            return text;
        }
        return "";
    }
}
