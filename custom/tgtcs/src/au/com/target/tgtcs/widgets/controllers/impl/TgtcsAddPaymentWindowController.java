package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.services.payment.CsCardPaymentService;
import de.hybris.platform.cscockpit.services.payment.strategy.CsOrderUnauthorizedTotalStrategy;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.zkoss.zhtml.Form;
import org.zkoss.zhtml.Input;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.util.CountryComparator;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * a controller class that will create the add payment dialog
 * 
 */
public class TgtcsAddPaymentWindowController {

    public static final int PAYMENT_OPERATION_TYPE_TOKENIZE = 0;
    public static final int PAYMENT_OPERATION_TYPE_CAPTURE = 1;
    private static final Logger LOG = Logger.getLogger(TgtcsAddPaymentWindowController.class);
    private static final String PAYMENT_FORM_RESULT_ZUL = "/tgtcshostedPaymentFormResultFor";
    private static final String ZUL_FILE_EXTENSION = ".zul";
    private static final String IPG_PAYMENT_FORM_RESULT_ZUL = "/tgtcshostedIpgPaymentForm.zul";
    private static final String IPG_PAYMENT_FEATURE = "payment.ipg.cscockpit";
    private static final String IPG = "ipg";
    private static final String GIFTCARD = "giftcard";
    protected final Window window;
    private final AbstractOrderModel order;
    private final int paymentType;
    private final IpgPaymentTemplateType ipgPaymentTemplateType;
    private final PaymentInfoModel currentPaymentInfo;
    private final CsCardPaymentService csCardPaymentService;
    private final DeliveryService deliveryService;
    private final CommonI18NService commonI18NService;


    private final TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    private final TargetFeatureSwitchService targetFeatureSwitchService;
    private final TargetOrderService targetOrderService;
    private final PaymentModeService paymentModeService;
    private final TargetPaymentService targetPaymentService;
    private final ConfigurationService configurationService;
    private final TargetCheckoutFacade targetCheckoutFacade;
    private final TargetCheckoutController targetCheckoutController;

    /**
     * 
     * @param window
     *            the payment window
     * @param order
     *            the current order in cart
     * @param paymentType
     *            payment type 0 or 1
     * @param paymentInfo
     *            the existing payment info model
     */
    public TgtcsAddPaymentWindowController(final Window window, final AbstractOrderModel order, final int paymentType,
            final PaymentInfoModel paymentInfo, final IpgPaymentTemplateType ipgPaymentTemplateType) {
        this.window = window;
        this.order = order;
        this.paymentType = paymentType;
        this.currentPaymentInfo = paymentInfo;
        this.ipgPaymentTemplateType = ipgPaymentTemplateType;
        final ApplicationContext ctx = de.hybris.platform.core.Registry.getApplicationContext();
        this.csCardPaymentService = (CsCardPaymentService)ctx.getBean("csCardPaymentService");
        this.deliveryService = (DeliveryService)ctx.getBean("deliveryService");
        this.commonI18NService = (CommonI18NService)ctx.getBean("commonI18NService");
        this.targetFeatureSwitchService = (TargetFeatureSwitchService)ctx.getBean("targetFeatureSwitchService");
        this.targetOrderService = (TargetOrderService)ctx.getBean("orderService");
        this.paymentModeService = (PaymentModeService)ctx.getBean("paymentModeService");
        this.targetPaymentService = (TargetPaymentService)ctx.getBean("targetPaymentService");
        this.configurationService = (ConfigurationService)ctx.getBean("configurationService");
        this.tgtCsCardPaymentFacade = (TgtCsCardPaymentFacade)ctx.getBean("tgtCsCardPaymentFacade");
        this.targetCheckoutFacade = (TargetCheckoutFacade)ctx.getBean("targetCheckoutFacade");
        this.targetCheckoutController = (TargetCheckoutController)ctx.getBean("csCheckoutController");
    }


    /**
     * 
     * @param window
     * @param order
     * @param paymentType
     * @param csCardPaymentService
     * @param deliveryService
     * @param paymentInfo
     * @param commonI18NService
     * @param targetFeatureSwitchService
     * @param targetOrderService
     * @param paymentModeService
     * @param targetPaymentService
     * @param configurationService
     */
    public TgtcsAddPaymentWindowController(final Window window, final OrderModel order, final int paymentType,
            final CsCardPaymentService csCardPaymentService, final DeliveryService deliveryService,
            final PaymentInfoModel paymentInfo, final CommonI18NService commonI18NService,
            final TargetFeatureSwitchService targetFeatureSwitchService, final TargetOrderService targetOrderService,
            final PaymentModeService paymentModeService, final TargetPaymentService targetPaymentService,
            final ConfigurationService configurationService, final TgtCsCardPaymentFacade tgtCsCardPaymentFacade,
            final TargetCheckoutFacade targetCheckoutFacade, final IpgPaymentTemplateType ipgPaymentTemplateType,
            final TargetCheckoutController targetCheckoutController) {
        this.window = window;
        this.order = order;
        this.paymentType = paymentType;
        this.ipgPaymentTemplateType = ipgPaymentTemplateType;
        this.csCardPaymentService = csCardPaymentService;
        this.deliveryService = deliveryService;
        this.currentPaymentInfo = paymentInfo;
        this.commonI18NService = commonI18NService;
        this.targetFeatureSwitchService = targetFeatureSwitchService;
        this.targetOrderService = targetOrderService;
        this.paymentModeService = paymentModeService;
        this.targetPaymentService = targetPaymentService;
        this.configurationService = configurationService;
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
        this.targetCheckoutFacade = targetCheckoutFacade;
        this.targetCheckoutController = targetCheckoutController;
    }

    /**
     * refresh window by recreating the payment form and filling in the values
     */
    public void refreshWindow() {
        if (getTargetFeatureSwitchService().isFeatureEnabled(IPG_PAYMENT_FEATURE)) {
            final Div formcontainer = (Div)getWindow().getFellow("hostedPaymentFormContainer");
            formcontainer.setStyle("border:5px");

            final String iFrameUrl = getFrameUrl();
            if (StringUtils.isNotEmpty(iFrameUrl)) {
                final Iframe iframe = createIframe();
                iframe.setId("ipgIFramePaymentId");
                iframe.setName("ipgIFrame");
                if (IpgPaymentTemplateType.GIFTCARDMULTIPLE.equals(getIpgPaymentTemplateType())) {
                    iframe.setWidth("620px");
                    iframe.setHeight("745px");
                }
                else if (IpgPaymentTemplateType.CREDITCARDSINGLE.equals(getIpgPaymentTemplateType())) {
                    iframe.setWidth("620px");
                    iframe.setHeight("720px");
                }
                else {
                    iframe.setWidth("600px");
                    iframe.setHeight("600px");
                }
                iframe.setParent(formcontainer);
                iframe.setSrc(iFrameUrl);
            }
            else {
                LOG.info("ERROR: Unable to make payment at this moment.");
            }
        }
        else {
            createForm();
            fillWindowContent();
        }
    }

    /**
     * Method to fetch sessionId and token and then return the ipgUrl to render the iFrame
     * 
     * @return frameUrl
     */
    protected String getFrameUrl() {
        final String csCockpitBaseUrl = getConfigurationService().getConfiguration().getString("tgtcscockpit.baseurl");
        final String returnURL = csCockpitBaseUrl + IPG_PAYMENT_FORM_RESULT_ZUL;

        final PaymentModeModel paymentMode = getPaymentModeService().getPaymentModeForCode(IPG);
        if (IpgPaymentTemplateType.CREDITCARDSINGLE.equals(getIpgPaymentTemplateType())) {
            getTargetCheckoutController().setCurrentPaymentModeCode(IPG);
        }
        else if (IpgPaymentTemplateType.GIFTCARDMULTIPLE.equals(getIpgPaymentTemplateType())) {
            getTargetCheckoutController().setCurrentPaymentModeCode(GIFTCARD);
        }

        if (getOrder() instanceof CartModel) {
            String sessionId = ((CartModel)getOrder()).getUniqueKeyForPayment();
            if (StringUtils.isEmpty(sessionId)) {
                sessionId = getTargetOrderService().createUniqueKeyForPaymentInitiation((CartModel)getOrder());
            }
            final HostedSessionTokenRequest hostedSessionTokenRequest = getTgtCsCardPaymentFacade()
                    .createHostedSessionTokenRequest((CartModel)getOrder(),
                            BigDecimal.valueOf(getOrder().getTotalPrice().doubleValue()), paymentMode, returnURL,
                            returnURL, PaymentCaptureType.PLACEORDER, sessionId, getIpgPaymentTemplateType());
            final TargetCreateSubscriptionResult targetCreateSubscriptionResult = getTargetPaymentService()
                    .getHostedSessionToken(hostedSessionTokenRequest);
            final String token = targetCreateSubscriptionResult != null
                    ? targetCreateSubscriptionResult.getRequestToken()
                    : StringUtils.EMPTY;
            return getTargetCheckoutFacade().createIpgUrl(sessionId, token);
        }
        return StringUtils.EMPTY;

    }

    /**
     * Method to create new Iframe object.
     * 
     * @return iFrame object
     */
    protected Iframe createIframe() {
        return new Iframe();
    }

    /**
     * create the form
     */
    protected void createForm() {
        final Div formcontainer = (Div)window.getFellow("hostedPaymentFormContainer");
        final Form form = new Form();
        form.setId("paymentForm");
        form.setParent(formcontainer);
        createFormDoubleBox(form, "Amount $", "amountReadonly");

        cardDetails(form);
        billingAddress(form);
        returnURL(form);

        createFormButton(form, "action", "Add New Payment");
        window.setWidth("750px");

    }

    /**
     * Card details
     * 
     * @param form
     *            the form
     */
    private void cardDetails(final Form form) {
        createFormTextBox(form, "Card holder full name*", "nameOnCard", "300px");
        createFormSelect(form, "Card type *", "cardType", false);
        createFormTextBox(form, "Card Number *", "gatewayCardNumber", "150px");
        createFormIntBox(form, "Card Expiry Month *", "gatewayCardExpiryDateMonth");
        createFormIntBox(form, "Card Expiry Year *", "gatewayCardExpiryDateYear");
        createFormTextBox(form, "Card Security Code *", "gatewayCardSecurityCode", "30px");
    }

    /**
     * Billing address
     * 
     * @param form
     *            the form
     */
    private void billingAddress(final Form form) {
        createFormTextBox(form, "First name *", "firstName", "150px");
        createFormTextBox(form, "Last name *", "lastName", "150px");
        createFormTextBox(form, "Street Address *", "street1", "500px");
        createFormTextBox(form, "Street Address 2", "street2", "500px");
        createFormTextBox(form, "City/Suburb *", "city", "150px");
        createFormTextBox(form, "State *", "state", "150px");
        createFormTextBox(form, "Postal code *", "postalCode", "50px");
        createFormSelect(form, "Country *", "country", true);
        createFormTextBox(form, "Phone number", "phoneNumber", "50px");
    }

    /**
     * Return URL
     * 
     * @param form
     *            the form
     */
    private void returnURL(final Form form) {
        createFormHiddenField(form, "gatewayReturnURL");
        createFormHiddenField(form, "paymentSessionId");
        createFormHiddenField(form, "tnsaction");
        createFormHiddenField(form, "orderID");
        createFormHiddenField(form, "paymentType");
        createFormHiddenField(form, "amount");
    }

    private void createFormDoubleBox(final AbstractComponent parent, final String labelText, final String fieldName) {

        final Div div = new Div();
        div.setSclass("editorWidgetEditor");
        div.setStyle("padding-bottom:0;");
        div.setHeight("100%");
        div.setWidth("100%");
        parent.appendChild(div);

        final Hbox hbox = new Hbox();
        hbox.setWidth("100%");
        hbox.setWidths("9em, none");
        hbox.setStyle("height:35px;");
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass("editorWidgetEditorLabel");
        label.setValue(labelText);
        hbox.appendChild(label);

        final Doublebox doublebox = new Doublebox();
        doublebox.setSclass("textEditor");
        doublebox.setName(fieldName);
        doublebox.setId(fieldName);
        doublebox.setMaxlength(20);
        doublebox.setWidth("100px");
        if (doublebox.getName().equalsIgnoreCase("amountReadonly")) {
            doublebox.setDisabled(true);
        }

        hbox.appendChild(doublebox);
    }

    private void createFormIntBox(final AbstractComponent parent, final String labelText, final String fieldName) {

        final Div div = new Div();
        div.setSclass("editorWidgetEditor");
        div.setStyle("padding-bottom:0;");
        div.setHeight("100%");
        div.setWidth("100%");
        parent.appendChild(div);

        final Hbox hbox = new Hbox();
        hbox.setWidth("100%");
        hbox.setWidths("9em, none");
        hbox.setStyle("height:35px;");
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass("editorWidgetEditorLabel");
        label.setValue(labelText);
        hbox.appendChild(label);

        final Intbox intbox = new Intbox();
        intbox.setSclass("textEditor");
        intbox.setName(fieldName);
        intbox.setId(fieldName);
        intbox.setWidth("40px");
        intbox.setMaxlength(10);

        hbox.appendChild(intbox);
    }


    private void createFormTextBox(final AbstractComponent parent, final String labelText, final String fieldName,
            final String width) {

        final Div div = new Div();
        div.setSclass("editorWidgetEditor");
        div.setStyle("padding-bottom:0;");
        div.setHeight("100%");
        div.setWidth("100%");
        parent.appendChild(div);

        final Hbox hbox = new Hbox();
        hbox.setWidth("100%");
        hbox.setWidths("9em, none");
        hbox.setStyle("height:35px;");
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass("editorWidgetEditorLabel");
        label.setValue(labelText);
        hbox.appendChild(label);

        final Textbox textbox = new Textbox();
        textbox.setSclass("textEditor");
        textbox.setType("text");
        textbox.setWidth(width);
        textbox.setName(fieldName);
        textbox.setId(fieldName);

        if (textbox.getName().equals("street1") || textbox.getName().equals("street2")) {
            textbox.setMaxlength(40);
        }
        else {
            textbox.setMaxlength(200);
        }
        hbox.appendChild(textbox);
    }

    private void createFormSelect(final AbstractComponent parent, final String labelText, final String fieldName,
            final boolean isReadOnly) {

        final Div div = new Div();
        div.setSclass("editorWidgetEditor");
        div.setStyle("padding-bottom:0;");
        div.setHeight("100%");
        div.setWidth("100%");
        parent.appendChild(div);

        final Hbox hbox = new Hbox();
        hbox.setWidth("100%");
        hbox.setWidths("9em, none");

        hbox.setStyle("height:35px;");
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass("editorWidgetEditorLabel");
        label.setValue(labelText);
        hbox.appendChild(label);

        final Combobox select = new Combobox();
        select.setReadonly(isReadOnly);
        select.setId(fieldName);
        select.setName(fieldName);
        hbox.appendChild(select);

    }

    private void createFormHiddenField(final AbstractComponent parent, final String fieldName) {
        final Input hiddenInput = new Input();
        hiddenInput.setDynamicProperty("type", "hidden");
        hiddenInput.setDynamicProperty("name", fieldName);
        hiddenInput.setId(fieldName);
        //hiddenInput.setValue(fieldValue);
        parent.appendChild(hiddenInput);
    }

    private void createFormButton(final AbstractComponent parent, final String fieldName, final String fieldValue) {
        final Input button = new Input();
        button.setDynamicProperty("type", "submit");
        button.setDynamicProperty("name", fieldName);
        button.setStyle(
                "cursor: pointer;font-family: Arial,Helvetica,sans-serif;background-color: rgb(32, 43, 74);color: rgb(255, 255, 255);background-image: url(\"images/button_back.gif\");background-repeat: repeat-x;border: 0px none;");
        button.setValue(fieldValue);
        parent.appendChild(button);
    }

    /**
     * 
     * @return a list of credit card types
     */
    protected List<CreditCardType> getAvailableCardTypes() {
        return csCardPaymentService.getSupportedCardSchemes(order);
    }

    protected void fillWindowContent() {

        fillUpCardTypes();
        fillUpCountries();
        populateform();

    }

    /**
     * Fills up card type and countries
     */
    private void fillUpCardTypes() {
        final Combobox cardtypecombo = (Combobox)window.getFellow("cardType");
        final List<CreditCardType> types = getAvailableCardTypes();

        for (final CreditCardType item : types) {
            final Comboitem comboItem = new Comboitem();
            cardtypecombo.appendChild(comboItem);
            comboItem.setValue(item.getCode());
            comboItem.setLabel(item.name());

        }
    }

    /**
     * Fills up countries
     */
    private void fillUpCountries() {
        final Combobox countrycombo = (Combobox)window.getFellow("country");
        final List<CountryModel> countries = commonI18NService.getAllCountries();
        for (final CountryModel item : countries) {
            final Comboitem comboItem = new Comboitem();
            countrycombo.appendChild(comboItem);
            comboItem.setValue(item.getIsocode());
            comboItem.setLabel(item.getName());

        }
    }

    /**
     * populate the form
     */
    protected void populateform() {
        final double amount = getSuggestedAmountForPaymentOption();
        final Doublebox amountbox = (Doublebox)window.getFellow("amountReadonly");
        amountbox.setValue(amount);
        ((Input)window.getFellow("amount")).setValue(String.valueOf(amount));

        final Form form = (Form)window.getFellow("paymentForm");
        //add new payment
        if (currentPaymentInfo == null
                || !currentPaymentInfo.getUser().getUid().equals(order.getUser().getUid())
                || (currentPaymentInfo instanceof CreditCardPaymentInfoModel
                        && ((CreditCardPaymentInfoModel)currentPaymentInfo)
                                .getSubscriptionId() == null)) {

            ((Intbox)window.getFellow("gatewayCardExpiryDateMonth")).setValue(Integer.valueOf(java.util.Calendar
                    .getInstance()
                    .get(
                            java.util.Calendar.MONTH)
                    + 1));
            ((Intbox)window.getFellow("gatewayCardExpiryDateYear")).setValue(Integer.valueOf(java.util.Calendar
                    .getInstance()
                    .get(
                            java.util.Calendar.YEAR)));
            final AddressModel addressModel = order.getPaymentAddress();
            if (addressModel != null) {
                ((Textbox)window.getFellow("firstName")).setValue(addressModel.getFirstname());
                ((Textbox)window.getFellow("lastName")).setValue(addressModel.getLastname());
                ((Textbox)window.getFellow("street1")).setValue(addressModel.getLine1());
                ((Textbox)window.getFellow("street2")).setValue(addressModel.getLine2());
                ((Textbox)window.getFellow("city")).setValue(addressModel.getTown());
                ((Textbox)window.getFellow("postalCode")).setValue(addressModel.getPostalcode());
                setSelectedCountry(addressModel.getCountry().getIsocode());
                ((Textbox)window.getFellow("phoneNumber")).setValue(addressModel.getPhone1());
                ((Textbox)window.getFellow("state")).setValue(addressModel.getDistrict());
            }
        }
        else { //edit payment
            final CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel)currentPaymentInfo;
            ((Intbox)window.getFellow("gatewayCardExpiryDateMonth")).setValue(Integer.valueOf(String
                    .valueOf(paymentInfo
                            .getValidToMonth())));
            ((Intbox)window.getFellow("gatewayCardExpiryDateYear")).setValue(Integer.valueOf(String.valueOf(paymentInfo
                    .getValidToYear())));
            ((Textbox)window.getFellow("firstName")).setValue(paymentInfo.getBillingAddress().getFirstname());
            ((Textbox)window.getFellow("lastName")).setValue(paymentInfo.getBillingAddress().getLastname());
            ((Textbox)window.getFellow("street1")).setValue(paymentInfo.getBillingAddress().getLine1());
            ((Textbox)window.getFellow("street2")).setValue(paymentInfo.getBillingAddress().getLine2());
            ((Textbox)window.getFellow("city")).setValue(paymentInfo.getBillingAddress().getTown());
            ((Textbox)window.getFellow("postalCode")).setValue(paymentInfo.getBillingAddress().getPostalcode());
            setSelectedCountry(paymentInfo.getBillingAddress().getCountry().getIsocode());
            ((Textbox)window.getFellow("phoneNumber")).setValue(paymentInfo.getBillingAddress().getPhone1());
            ((Textbox)window.getFellow("state")).setValue(paymentInfo.getBillingAddress().getDistrict());
            //card details
            ((Textbox)window.getFellow("nameOnCard")).setValue(paymentInfo.getCcOwner());
            ((Combobox)window.getFellow("cardType")).setValue(paymentInfo.getType().getCode());
            ((Textbox)window.getFellow("gatewayCardNumber")).setValue(paymentInfo.getNumber());
            ((Textbox)window.getFellow("gatewayCardSecurityCode")).setValue("xxx");

        }
        //set tns information
        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();
        hostedSessionTokenRequest.setAmount(BigDecimal.valueOf(amount));
        hostedSessionTokenRequest.setCancelUrl(null);
        hostedSessionTokenRequest.setOrderModel(order);
        hostedSessionTokenRequest.setPaymentCaptureType(PaymentCaptureType.PLACEORDER);
        hostedSessionTokenRequest.setPaymentMode(paymentModeService.getPaymentModeForCode("creditcard"));
        hostedSessionTokenRequest.setReturnUrl(null);
        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetPaymentService
                .getHostedSessionToken(hostedSessionTokenRequest);
        final String paymentSessionToken = targetCreateSubscriptionResult != null
                ? targetCreateSubscriptionResult.getRequestToken()
                : StringUtils.EMPTY;
        final String tnsbaseurl = configurationService.getConfiguration().getString(
                "tns.hostedPaymentFormBaseUrl");
        ((Input)window.getFellow("paymentSessionId")).setValue(paymentSessionToken);
        ((Input)window.getFellow("tnsaction")).setValue(tnsbaseurl + paymentSessionToken);


        final String baseUrl;

        baseUrl = configurationService.getConfiguration().getString("tgtcscockpit.baseurl");

        ((Input)window.getFellow("gatewayReturnURL"))
                .setValue(getGatewayReturnUrl(baseUrl));
        ((Input)window.getFellow("orderID")).setValue(order.getCode());
        ((Input)window.getFellow("paymentType")).setValue(String.valueOf(this.paymentType));

        form.setDynamicProperty("action", tnsbaseurl + paymentSessionToken);
        form.setDynamicProperty("method", "post");
        form.setDynamicProperty("target", "myiframe");

    }

    protected String getGatewayReturnUrl(final String baseUrl) {
        return baseUrl + PAYMENT_FORM_RESULT_ZUL + order.getPurchaseOption().getCode() + ZUL_FILE_EXTENSION;
    }


    private double getSuggestedAmountForPaymentOption() {
        final ApplicationContext ctx = de.hybris.platform.core.Registry.getApplicationContext();
        final CsOrderUnauthorizedTotalStrategy defaultCsOrderUnauthorizedTotalStrategy = (CsOrderUnauthorizedTotalStrategy)ctx
                .getBean("csOrderUnauthorizedTotalStrategy");

        return defaultCsOrderUnauthorizedTotalStrategy.getUnauthorizedTotal(order);
    }

    /**
     * Get a list of available Delivery countries
     * 
     * @return a CountryData list
     */
    protected List<CountryData> getDeliveryCountries() {
        final List<CountryData> result = new ArrayList<>();

        final List<CountryModel> deliveryCountries = sortCountries(deliveryService.getDeliveryCountriesForOrder(null));
        for (final CountryModel country : deliveryCountries) {
            final CountryData countryData = new CountryData();
            countryData.setIsocode(country.getIsocode());
            countryData.setName(country.getName());
            result.add(countryData);
        }

        return result;
    }

    private List<CountryModel> sortCountries(final Collection<CountryModel> countries) {
        final List<CountryModel> result = new ArrayList<CountryModel>(countries);
        Collections.sort(result, CountryComparator.INSTANCE);
        return result;
    }

    protected void setSelectedCountry(final String isoCode) {
        final Combobox countryCombobox = (Combobox)window.getFellow("country");
        for (final Object item : countryCombobox.getChildren()) {
            if (item instanceof Comboitem) {
                final String countryIsoCode = (String)((Comboitem)item).getValue();
                if (isoCode.equals(countryIsoCode)) {
                    countryCombobox.setSelectedItem((Comboitem)item);
                    break;
                }
            }
        }
    }

    /**
     * 
     * @return a Window object
     */
    public Window getWindow() {
        return window;
    }

    /**
     * @return the order
     */
    public AbstractOrderModel getOrder() {
        return order;
    }

    /**
     * @return the targetFeatureSwitchService
     */
    public TargetFeatureSwitchService getTargetFeatureSwitchService() {
        return targetFeatureSwitchService;
    }

    /**
     * @return the targetOrderService
     */
    public TargetOrderService getTargetOrderService() {
        return targetOrderService;
    }

    /**
     * @return the paymentModeService
     */
    public PaymentModeService getPaymentModeService() {
        return paymentModeService;
    }

    /**
     * @return the targetPaymentService
     */
    public TargetPaymentService getTargetPaymentService() {
        return targetPaymentService;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * @return the tgtCsCardPaymentFacade
     */
    public TgtCsCardPaymentFacade getTgtCsCardPaymentFacade() {
        return tgtCsCardPaymentFacade;
    }


    /**
     * @return the targetCheckoutFacade
     */
    public TargetCheckoutFacade getTargetCheckoutFacade() {
        return targetCheckoutFacade;
    }


    /**
     * @return the ipgPaymentTemplateType
     */
    public IpgPaymentTemplateType getIpgPaymentTemplateType() {
        return ipgPaymentTemplateType;
    }


    /**
     * @return the targetCheckoutController
     */
    public TargetCheckoutController getTargetCheckoutController() {
        return targetCheckoutController;
    }




}
