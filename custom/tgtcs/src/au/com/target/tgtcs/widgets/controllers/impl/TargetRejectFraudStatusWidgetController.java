/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import org.apache.log4j.Logger;



/**
 * @author mjanarth
 * 
 */
public class TargetRejectFraudStatusWidgetController extends TargetAbstractFraudStatusWidgetController {

    private static final Logger LOG = Logger.getLogger(TargetRejectFraudStatusWidgetController.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderModel changeOrder(final String note) {
        final OrderModel orderModel = getSelectedOrder();
        if (OrderStatus.REVIEW.equals(orderModel.getStatus())
                || OrderStatus.REVIEW_ON_HOLD.equals(orderModel.getStatus())) {
            try {
                getTargetCsFraudService().addFraudReportToOrder(orderModel, FraudStatus.FRAUD, note);
                getRejectCancelService().startRejectCancelProcess(orderModel);
            }
            catch (final Exception e) {
                LOG.info(e);
            }
        }
        else {
            LOG.info("Order could not be REJECTED as the current status is " + orderModel.getStatus().toString());
        }
        return orderModel;
    }
}
