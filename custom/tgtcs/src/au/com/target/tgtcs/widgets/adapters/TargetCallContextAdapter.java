/**
 * 
 */
package au.com.target.tgtcs.widgets.adapters;


import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cscockpit.widgets.adapters.CallContextAdapter;
import de.hybris.platform.cscockpit.widgets.events.CallContextEvent;


/**
 * 
 */
public class TargetCallContextAdapter extends CallContextAdapter {

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCockpitEvent(final CockpitEvent cockpitEvent) {
        if ((!(cockpitEvent instanceof CallContextEvent)) || (super.equals(cockpitEvent.getSource()))) {
            return;
        }
        final TypedObject currentCustomer = getWidgetController().getCurrentCustomer();
        final boolean customerChanged = getWidgetModel().setCustomer(currentCustomer);
        if (customerChanged) {
            getWidgetModel().setCustomer(null);
        }
        super.onCockpitEvent(cockpitEvent);
    }

}
