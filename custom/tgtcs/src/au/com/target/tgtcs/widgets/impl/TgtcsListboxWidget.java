package au.com.target.tgtcs.widgets.impl;

import de.hybris.platform.cockpit.widgets.impl.DefaultListboxWidget;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.models.impl.CustomerAddressesListWidgetModel;


/**
 * @author Dell
 * 
 */
public class TgtcsListboxWidget extends DefaultListboxWidget<CustomerAddressesListWidgetModel, CustomerController> {
    private int currentItemIndex = 0;

    /**
     * @param currentItemIndex
     *            the currentItemIndex to set
     */
    public void setCurrentItemIndex(final int currentItemIndex) {
        this.currentItemIndex = currentItemIndex;
    }

    /**
     * @return the currentItemIndex
     */
    public int getCurrentItemIndex() {
        return currentItemIndex;
    }

}
