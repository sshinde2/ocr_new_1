package au.com.target.tgtcs.widgets.controllers;


import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.widgets.controllers.WidgetController;

import java.util.List;

import org.zkoss.zul.impl.InputElement;

import au.com.target.tgtverifyaddr.data.AddressData;


/**
 * @author Dell
 * 
 */
public interface AddressCustomerController extends WidgetController {

    public List<AddressData> checkValidAddress(ObjectValueContainer addressContainer);

    public void updateAddressByQASFormattedAddress(ObjectValueContainer addressContainer, String addressString);

    public List<AddressData> checkValidAddress(String addressString);

    public void updateAddressByQASFormattedAddress(List<InputElement> editors, String addressString);

}
