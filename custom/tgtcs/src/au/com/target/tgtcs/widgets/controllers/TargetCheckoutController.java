/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cscockpit.widgets.controllers.CardPaymentController;
import de.hybris.platform.cscockpit.widgets.controllers.CheckoutController;


/**
 * 
 * 
 */
public interface TargetCheckoutController extends CheckoutController, CardPaymentController {

    /**
     * Set fly buys code into Check out cart if fly buys code is valid. Set empty if fly buys code is invalid.
     * 
     * @param flyBuysCode
     *            fly buys code
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    void saveFlyBuysCode(String flyBuysCode) throws ValueHandlerException;

    /**
     * Get checkout cart by master cart
     * 
     * @return CartModel
     */
    CartModel getCheckoutCartByMasterCart();


    /**
     * Get fly buys code from check out cart or customer. If fly buys code in customer profile is valid then set it into
     * checkout cart.
     * 
     * @return the fly-buys code
     */
    String preLoadFlyBuysCode() throws ValueHandlerException;

    /**
     * set the current payment mode code
     * 
     * @param currentPaymentModeCode
     */
    void setCurrentPaymentModeCode(final String currentPaymentModeCode);

}
