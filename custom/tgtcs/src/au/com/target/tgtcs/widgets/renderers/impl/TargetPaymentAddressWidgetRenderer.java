package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.cscockpit.widgets.models.impl.CartAddressWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.PaymentAddressWidgetRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCallContextController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetBasketControllersContainer;
import au.com.target.tgtutility.util.PhoneValidationUtils;
import au.com.target.tgtutility.util.TargetValidationCommon.Phone;



public class TargetPaymentAddressWidgetRenderer extends PaymentAddressWidgetRenderer {
    private static final Logger LOG = Logger.getLogger(TargetPaymentAddressWidgetRenderer.class);
    private Listbox addressSelectComponent;
    private TargetBasketControllersContainer targetBasketControllersContainer;

    @Override
    protected HtmlBasedComponent createContentInternal(final Widget widget,
            final HtmlBasedComponent rootContainer) {
        final HtmlBasedComponent paymentAddressContent = super.createContentInternal(widget, rootContainer);
        if (paymentAddressContent != null) {
            addressSelectComponent = (Listbox)paymentAddressContent.getFirstChild().getFirstChild();
        }
        return paymentAddressContent;
    }

    @Override
    protected void handleAddAddressClickEvent(final Widget widget, final Event event, final Div container) {
        super.handleAddAddressClickEvent(widget, event, container);
        for (final TargetBasketController controller : getTargetBasketControllersContainer().getBasketControllers()) {
            controller.setAddressSelectComponent(null);
        }
        final TargetBasketController basketController = (TargetBasketController)widget.getWidgetController();
        final TargetCallContextController callContextController = (TargetCallContextController)basketController
                .returnCallContextController();
        callContextController.setAddAddressInCheckoutTab(true);
        basketController.setAddressSelectComponent(addressSelectComponent);
    }



    /**
     * {@inheritDoc}
     */
    @Override
    protected void handleSelectAddressEvent(final Widget<CartAddressWidgetModel, BasketController> widget,
            final SelectEvent selectEvent) {
        final Object selectedItem = getSelectedItem(selectEvent);
        if (!(selectedItem instanceof TypedObject)) {
            return;
        }

        final TypedObject address = (TypedObject)selectedItem;
        final AddressModel addressmodel = TypeUtils.unwrapItem(address, AddressModel.class);

        if ((StringUtils.isNotBlank(addressmodel.getPhone1()) && PhoneValidationUtils
                .validatePhoneNumber(addressmodel.getPhone1()))
                || (StringUtils.isNotBlank(addressmodel.getPhone2()) && PhoneValidationUtils
                        .validatePhoneNumber(addressmodel.getPhone2()))) {
            setSelectedAddress(widget, (TypedObject)selectedItem);
            final TargetBasketController basketController = (TargetBasketController)widget
                    .getWidgetController();
            basketController.returnCallContextController().dispatchEvent(null, this, null);
            for (final TargetBasketController controller : getTargetBasketControllersContainer()
                    .getBasketControllers()) {
                controller.dispatchEvent(null, this, null);
            }
        }
        else {
            try {
                LOG.info(Labels.getLabel(TgtcsConstants.PHONE_LENGTH_ERROR_MESSAGE,
                        new Object[] { Integer.valueOf(Phone.MIN_SIZE), Integer.valueOf(Phone.MAX_SIZE) })
                        + addressmodel.getPhone1());
                Messagebox.show(
                        Labels.getLabel(TgtcsConstants.PHONE_LENGTH_ERROR_MESSAGE,
                                new Object[] { Integer.valueOf(Phone.MIN_SIZE), Integer.valueOf(Phone.MAX_SIZE) }),
                        Labels.getLabel(TgtcsConstants.INVALID_ADDRESS), 1, "z-msgbox z-msgbox-error");
                addressSelectComponent.setSelectedIndex(-1);
            }
            catch (final InterruptedException e) {
                LOG.error("Failed to display message box to user", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * @return the targetBasketControllersContainer
     */
    protected TargetBasketControllersContainer getTargetBasketControllersContainer() {
        return targetBasketControllersContainer;
    }

    /**
     * @param targetBasketControllersContainer
     *            the targetBasketControllersContainer to set
     */
    @Required
    public void setTargetBasketControllersContainer(
            final TargetBasketControllersContainer targetBasketControllersContainer) {
        this.targetBasketControllersContainer = targetBasketControllersContainer;
    }
}
