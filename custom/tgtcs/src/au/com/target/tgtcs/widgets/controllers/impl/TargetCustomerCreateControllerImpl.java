/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.model.meta.impl.ItemAttributePropertyDescriptor;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.util.TypeTools;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCustomerCreateController;
import de.hybris.platform.servicelayer.exceptions.AttributeNotSupportedException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.TargetCustomerCreateController;
import au.com.target.tgtutility.util.TargetValidationCommon;
import au.com.target.tgtutility.util.TargetValidationUtils;


/**
 * 
 */
public class TargetCustomerCreateControllerImpl extends DefaultCustomerCreateController implements
        TargetCustomerCreateController {


    protected static final String TITLE_VALIDATION_MESSAGE_KEY = "validation.message.customer.title";

    private static final Logger LOG = Logger.getLogger(TargetCustomerCreateControllerImpl.class);

    private L10NService l10nService;
    private CustomerNameStrategy customerNameStrategy;

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCustomerCreateController#createNewCustomer(de.hybris.platform.cockpit.services.values.ObjectValueContainer, java.lang.String)
     */
    @Override
    public TypedObject createNewCustomer(final ObjectValueContainer customerObjectValueContainer,
            final String customerTypeCode) throws DuplicateUidException, ValueHandlerException {

        if ((customerObjectValueContainer != null) && (customerTypeCode != null)) {

            beforeCreateNewCustomer(customerObjectValueContainer, customerTypeCode);

            final TargetCustomerModel customerModel = createNewCustomer(customerObjectValueContainer);

            if (customerModel != null) {
                try {
                    String validateMessage = validatePhoneNumber(customerModel.getPhoneNumber());
                    if (StringUtils.isNotBlank(validateMessage)) {
                        throw new ValueHandlerException(validateMessage, new Exception());
                    }
                    validateMessage = validateTitle(customerModel.getTitle());
                    if (StringUtils.isNotBlank(validateMessage)) {
                        throw new ValueHandlerException(validateMessage, new Exception());
                    }

                    getCustomerAccountService().register(customerModel, null);
                    return getCockpitTypeService().wrapItem(customerModel);
                }
                catch (final ModelSavingException e) {
                    String message = e.getMessage();
                    if ((e.getCause() instanceof InterceptorException)) {

                        final String interceptorMessage = e.getCause().getMessage();
                        message = (interceptorMessage != null) && (interceptorMessage.contains("]:"))
                                ? StringUtils.substringAfter(interceptorMessage, "]:") : interceptorMessage;
                    }

                    throw new ValueHandlerException(message, e);
                }
                catch (final IllegalArgumentException e) {
                    throw new ValueHandlerException(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    /**
     * 
     * @param phonenumber
     *            phone number value
     * @return error message if validation is fail else returns null
     */
    private String validatePhoneNumber(final String phonenumber) {

        if (!TargetValidationUtils.checkOptional(phonenumber, false)) {

            return getL10nService().getLocalizedString("type.targetcustomer.phonenumber.name")
                    + " cannot be blank";
        }
        if (!TargetValidationUtils.matchRegularExpression(phonenumber, TargetValidationCommon.Phone.PATTERN,
                TargetValidationCommon.Phone.MIN_SIZE, TargetValidationCommon.Phone.MAX_SIZE, false)) {
            return getL10nService().getLocalizedString("type.targetcustomer.phonenumber.name")
                    + " must be valid";
        }
        return null;
    }

    /**
     * 
     * @param title
     * @return error message if validation is fail else returns null
     */
    private String validateTitle(final TitleModel title) {
        if (title == null) {
            return getL10nService().getLocalizedString(TITLE_VALIDATION_MESSAGE_KEY);
        }
        return null;
    }

    @Override
    protected TargetCustomerModel createNewCustomer(final ObjectValueContainer customerObjectValueContainer) {

        final TargetCustomerModel customerModel = (TargetCustomerModel)getModelService().create(
                TargetCustomerModel.class);
        final Set<ObjectValueHolder> allValues = customerObjectValueContainer.getAllValues();
        for (final ObjectValueHolder objectValueHolder : allValues) {

            final PropertyDescriptor propertyDescriptor = objectValueHolder.getPropertyDescriptor();
            Object currentValue = objectValueHolder.getCurrentValue();
            if (((propertyDescriptor instanceof ItemAttributePropertyDescriptor)) && (currentValue != null)) {
                final ItemAttributePropertyDescriptor attributePropertyDescriptor = (ItemAttributePropertyDescriptor)propertyDescriptor;

                if (attributePropertyDescriptor.isSingleAttribute()) {
                    try {
                        currentValue = getModelService().toPersistenceLayer(
                                TypeTools.container2Item(null, currentValue));

                        if (propertyDescriptor.isLocalized()) {
                            setModelLocalizedValue(customerModel, attributePropertyDescriptor.getAttributeQualifier(),
                                    objectValueHolder.getLanguageIso(), currentValue);
                        }
                        else {
                            setModelSingleValue(customerModel, attributePropertyDescriptor.getAttributeQualifier(),
                                    currentValue);
                        }
                    }
                    catch (final AttributeNotSupportedException ex) {
                        LOG.error("Attribute: " + ex.getQualifier() + " is not supported by servicelayer! ");
                    }
                }
            }
        }

        return customerModel;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCustomerCreateController#beforeCreateNewCustomer(de.hybris.platform.cockpit.services.values.ObjectValueContainer, java.lang.String)
     */
    @Override
    protected void beforeCreateNewCustomer(final ObjectValueContainer customerObjectValueContainer,
            final String customerTypeCode) {
        final PropertyDescriptor nameDescriptor = findPropertyDescriptor(customerObjectValueContainer, "Principal.name");
        final PropertyDescriptor firstNameDescriptor = findPropertyDescriptor(customerObjectValueContainer,
                "TargetCustomer.firstname");
        final PropertyDescriptor lastNameDescriptor = findPropertyDescriptor(customerObjectValueContainer,
                "TargetCustomer.lastname");
        final ObjectValueContainer.ObjectValueHolder nameHolder = customerObjectValueContainer.getValue(nameDescriptor,
                null);
        final ObjectValueContainer.ObjectValueHolder firstNameHolder = customerObjectValueContainer.getValue(
                firstNameDescriptor, null);
        final ObjectValueContainer.ObjectValueHolder lastNameHolder = customerObjectValueContainer.getValue(
                lastNameDescriptor, null);
        nameHolder.setLocalValue(getCustomerNameStrategy().getName((String)firstNameHolder.getCurrentValue(),
                (String)lastNameHolder.getCurrentValue()));
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCustomerCreateController#getAutoFilledPropertyQualifiers()
     */
    @Override
    public Set<String> getAutoFilledPropertyQualifiers() {
        return new HashSet<>();
    }

    /**
     * @return the l10nService
     */
    protected L10NService getL10nService() {
        return l10nService;
    }

    /**
     * @param l10nService
     *            the l10nService to set
     */
    @Required
    public void setL10nService(final L10NService l10nService) {
        this.l10nService = l10nService;
    }

    /**
     * @return the customerNameStrategy
     */
    protected CustomerNameStrategy getCustomerNameStrategy() {
        return customerNameStrategy;
    }

    /**
     * @param customerNameStrategy
     *            the customerNameStrategy to set
     */
    @Required
    public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy) {
        this.customerNameStrategy = customerNameStrategy;
    }

}
