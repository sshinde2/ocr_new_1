/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;
import de.hybris.platform.cscockpit.widgets.models.impl.OrderItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.OrderDetailsWidgetRenderer;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;


/**
 * @author bhuang3
 *
 */
public class TargetOrderDetailsWidgetRenderer extends OrderDetailsWidgetRenderer {


    private OrderManagementActionsWidgetController orderManagementActionsWidgetController;

    @Override
    protected void createRefreshButton(final Widget<OrderItemWidgetModel, OrderController> widget, final Div container,
            final String buttonLabelName)
    {
        final Button button = new Button(LabelUtils.getLabel(widget, buttonLabelName, new Object[0]));
        button.setParent(container);

        button.addEventListener("onClick", new EventListener()
        {
            @Override
            public void onEvent(final Event event)
                    throws Exception
            {
                TargetOrderDetailsWidgetRenderer.this.handleRefreshTicketListEvent(widget, event);
                getOrderManagementActionsWidgetController().dispatchEvent(null, widget, null);
            }
        });
    }

    /**
     * @return the orderManagementActionsWidgetController
     */
    @Required
    protected OrderManagementActionsWidgetController getOrderManagementActionsWidgetController() {
        return orderManagementActionsWidgetController;
    }

    /**
     * @param orderManagementActionsWidgetController
     *            the orderManagementActionsWidgetController to set
     */
    public void setOrderManagementActionsWidgetController(
            final OrderManagementActionsWidgetController orderManagementActionsWidgetController) {
        this.orderManagementActionsWidgetController = orderManagementActionsWidgetController;
    }



}
