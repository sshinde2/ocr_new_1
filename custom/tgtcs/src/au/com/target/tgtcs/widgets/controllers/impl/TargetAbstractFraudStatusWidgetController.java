/**
 *
 */
package au.com.target.tgtcs.widgets.controllers.impl;


import de.hybris.platform.cockpit.widgets.controllers.impl.AbstractWidgetController;
import de.hybris.platform.cockpit.widgets.events.WidgetEvent;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcs.service.fraud.TargetCsFraudService;
import au.com.target.tgtfraud.ordercancel.RejectCancelService;


/**
 * @author mjanarth
 *
 */
public abstract class TargetAbstractFraudStatusWidgetController extends AbstractWidgetController
{

    private ModelService modelService;
    private TargetBusinessProcessService targetBusinessProcessService;
    private RejectCancelService rejectCancelService;
    private CallContextController callContextController;
    private TargetCsFraudService targetCsFraudService;


    /**
     * change order with creating FraudReportModel with fraud note.
     *
     * @param note
     *            the fraud note
     */
    public abstract OrderModel changeOrder(final String note);


    @Override
    public void dispatchEvent(final String context, final Object source, final Map<String, Object> data) {
        dispatchEvent(context, new WidgetEvent(source, context));
    }



    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the targetBusinessProcessService
     */
    protected TargetBusinessProcessService getTargetBusinessProcessService() {
        return targetBusinessProcessService;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */

    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @return the rejectCancelService
     */
    protected RejectCancelService getRejectCancelService() {
        return rejectCancelService;
    }

    /**
     * @param rejectCancelService
     *            the rejectCancelService to set
     */
    @Required
    public void setRejectCancelService(final RejectCancelService rejectCancelService) {
        this.rejectCancelService = rejectCancelService;
    }


    /**
     * @return the callContextController
     */
    public CallContextController getCallContextController() {
        return callContextController;
    }


    /**
     * @param callContextController
     *            the callContextController to set
     */
    @Required
    public void setCallContextController(final CallContextController callContextController) {
        this.callContextController = callContextController;
    }

    /**
     *
     * @return ordercode
     */
    public String getOrderCode() {
        if (null != getCallContextController().getCurrentOrder().getObject()) {
            return ((OrderModel)getCallContextController().getCurrentOrder().getObject()).getCode();
        }
        return null;
    }

    /**
     *
     * @return OrderModel
     */
    public OrderModel getSelectedOrder() {
        if (null != getCallContextController().getCurrentOrder()) {
            return (OrderModel)getCallContextController().getCurrentOrder().getObject();
        }

        return null;
    }


    /**
     * @return the targetCsFraudService
     */
    protected TargetCsFraudService getTargetCsFraudService() {
        return targetCsFraudService;
    }


    /**
     * @param targetCsFraudService the targetCsFraudService to set
     */
    @Required
    public void setTargetCsFraudService(final TargetCsFraudService targetCsFraudService) {
        this.targetCsFraudService = targetCsFraudService;
    }




}
