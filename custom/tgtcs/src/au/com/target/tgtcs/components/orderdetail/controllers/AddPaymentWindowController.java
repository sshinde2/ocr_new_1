/*
 * 
 */
package au.com.target.tgtcs.components.orderdetail.controllers;

import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.facade.TgtCsAddPaymentFormFacade;



/**
 * The Class AddPaymentWindowController.
 */
public class AddPaymentWindowController {

    private TgtCsAddPaymentFormFacade tgtCsaddPaymentFormFacade;

    /**
     * Populate refund payment form.
     * 
     * @param order
     *            the order
     * @param window
     *            the window
     */
    public void populateRefundPaymentForm(final OrderModel order, final Window window) {
        tgtCsaddPaymentFormFacade.loadFormContent(order, window);
        tgtCsaddPaymentFormFacade.populateRefundForm(order, window);
    }

    /**
     * Populate ipg refund payment form.
     * 
     * @param order
     *            the order
     * @param window
     *            the window
     */
    public void populateIpgRefundPaymentForm(final OrderModel order, final Window window) {
        tgtCsaddPaymentFormFacade.loadIpgFormContent(order, window);
    }

    /**
     * Populate cancel payment form.
     * 
     * @param order
     *            the order
     * @param window
     *            the window
     */
    public void populateCancelPaymentForm(final OrderModel order, final Window window) {
        tgtCsaddPaymentFormFacade.loadFormContent(order, window);
        tgtCsaddPaymentFormFacade.populateCancelForm(order, window);
    }

    /**
     * Populate cancel payment form.
     * 
     * @param order
     *            the order
     * @param window
     *            the window
     */
    public void populateCancelPaymentFormForIpg(final OrderModel order, final Window window) {
        tgtCsaddPaymentFormFacade.loadIpgFormContent(order, window);
    }

    /**
     * @param tgtCsaddPaymentFormFacade
     *            the tgtCsaddPaymentFormFacade to set
     */
    @Required
    public void setTgtCsaddPaymentFormFacade(final TgtCsAddPaymentFormFacade tgtCsaddPaymentFormFacade) {
        this.tgtCsaddPaymentFormFacade = tgtCsaddPaymentFormFacade;
    }

}