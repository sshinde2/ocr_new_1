/**
 * 
 */
package au.com.target.tgtcs.csagent.service.impl;

import de.hybris.platform.ticket.model.CsAgentGroupModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcs.csagent.dao.TargetCsAgentGroupDao;
import au.com.target.tgtcs.csagent.service.TargetCsAgentGroupService;


/**
 * @author mjanarth
 *
 */
public class TargetCsAgentGroupServiceImpl implements TargetCsAgentGroupService {

    private TargetCsAgentGroupDao targetCsAgentGroupDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.csagent.service.TargetCsAgentGroupService#getAllCustomerAgentGroupUid()
     */
    @Override
    public List<String> getAllCustomerAgentGroupUid() {
        final List<CsAgentGroupModel> csAgentGrps = targetCsAgentGroupDao.findAllCsAgentGroup();
        List<String> csAgentGrpIds = null;
        if (CollectionUtils.isNotEmpty(csAgentGrps)) {
            csAgentGrpIds = new ArrayList<>();
            for (final CsAgentGroupModel grpModel : csAgentGrps) {
                csAgentGrpIds.add(grpModel.getUid());
            }
        }
        return csAgentGrpIds;
    }

    /**
     * @param targetCsAgentGroupDao
     *            the targetCsAgentGroupDao to set
     */
    @Required
    public void setTargetCsAgentGroupDao(final TargetCsAgentGroupDao targetCsAgentGroupDao) {
        this.targetCsAgentGroupDao = targetCsAgentGroupDao;
    }

}
