/**
 * 
 */

package au.com.target.tgtebay.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtwebmethods.ca.TargetCaShippingService;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdatePartnerWithReadyForPickupStatusActionTest {

    @InjectMocks
    private final UpdatePartnerWithReadyForPickupStatusAction action = new UpdatePartnerWithReadyForPickupStatusAction();

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetCaShippingService targetCaShippingService;

    @Before
    public void setUp() {
        Mockito.when(orderProcess.getOrder()).thenReturn(order);
        Mockito.when(Boolean.valueOf(orderProcessParameterHelper.isPartnerUpdateRequired(orderProcess)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(targetCaShippingService.submitCncOrderStatusForReadyForPickupUpdate(order)))
                .thenReturn(Boolean.TRUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWhenOrderIsNull() throws RetryLaterException, Exception {
        Mockito.when(orderProcess.getOrder()).thenReturn(null);
        action.executeInternal(orderProcess);
    }

    @Test
    public void testExecuteActionWhenPartnerIsUpdatedSuccessfully()
            throws RetryLaterException, Exception {
        action.executeInternal(orderProcess);

        Mockito.verify(targetCaShippingService).submitCncOrderStatusForReadyForPickupUpdate(order);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteActionWhenPartnerIsUpdatedUnSuccessfully()
            throws RetryLaterException, Exception {
        Mockito.when(Boolean.valueOf(targetCaShippingService.submitCncOrderStatusForReadyForPickupUpdate(order)))
                .thenReturn(Boolean.FALSE);

        action.executeInternal(orderProcess);
    }

    @Test
    public void testExecuteActionWhenPartnerIsNotUpdated()
            throws RetryLaterException, Exception {
        Mockito.when(Boolean.valueOf(orderProcessParameterHelper.isPartnerUpdateRequired(orderProcess)))
                .thenReturn(Boolean.FALSE);
        action.executeInternal(orderProcess);

        Mockito.verifyZeroInteractions(targetCaShippingService);
    }

}
