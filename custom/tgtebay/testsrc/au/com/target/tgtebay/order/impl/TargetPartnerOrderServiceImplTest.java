/**
 * 
 */
package au.com.target.tgtebay.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtebay.dto.EbayTargetAddressDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntriesDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.dto.EbayTargetPayInfoDTO;
import au.com.target.tgtebay.exceptions.ImportOrderException;
import au.com.target.tgtebay.util.TargetPartnerOrderFieldPopulatorHelper;
import au.com.target.tgtebay.util.TargetPartnerOrderFieldValidator;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;


/**
 * @author mjanarth
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPartnerOrderServiceImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private TargetCommerceCheckoutService commerceCheckoutService;
    @Mock
    private TargetCommerceCartService commerceCartService;
    @Mock
    private PurchaseOptionConfigService purchaseConfigService;
    @Mock
    private TargetStockService targetStockService;
    @Mock
    private TargetDeliveryService deliveryMode;
    @Mock
    private TargetCustomerAccountService targetCustomerAccountService;
    @Mock
    private PaymentModeService paymentModeService;
    @Mock
    private CartModel cart;
    @Mock
    private CurrencyModel currencyModel;
    @Mock
    private PaypalPaymentInfoModel paypalpaInfoModel;
    @Mock
    private PaymentModeModel paymentModeModel;
    @Mock
    private TargetZoneDeliveryModeModel deliveryModeModel;
    @Mock
    private TargetWarehouseService targetWarehouseService;
    @Mock
    private TargetPartnerOrderFieldValidator targetPartnerOrderFieldValidator;
    @Mock
    private OrderModel orderModel;
    @Mock
    private CommerceOrderResult orderResult;
    @Mock
    private TargetPartnerOrderFieldPopulatorHelper ebayhelper;
    @Mock
    private ProductModel product;
    @Mock
    private KeyGenerator orderCodeGenerator;
    @Mock
    private TargetOrderService targetOrderService;
    @Mock
    private TargetDeliveryService targetDeliveryService;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private EbayTargetOrderDTO orderDTO;
    @Mock
    private EbayTargetAddressDTO addressDTO;
    @Mock
    private AddressModel address;
    @Mock
    private CurrencyConversionFactorsModel currencyConversion;
    @Mock
    private CurrencyConversionFactorService currencyConversionFactorService;
    @Mock
    private Map fulfillmentTypeToDeliveryToStoreMapping;
    @Mock
    private Map shippingClassToShortDeliveryTimeMapping;
    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;
    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final TargetPartnerOrderServiceImpl orderService = new TargetPartnerOrderServiceImpl();

    @Before
    public void setUp() {
        final List<TargetZoneDeliveryModeModel> deliveryModes = new ArrayList<>();
        deliveryModes.add(deliveryModeModel);

        final Map<String, String> paymentTypeToPaymentMethodMapping = new HashMap();
        paymentTypeToPaymentMethodMapping.put("PayPal", "paypal");
        paymentTypeToPaymentMethodMapping.put("TradeMe", "paynow");
        orderService.setPaymentTypeToPaymentMethodMapping(paymentTypeToPaymentMethodMapping);

        willReturn(Boolean.TRUE).given(targetPartnerOrderFieldValidator).validateEmail(anyString());
        willReturn(Boolean.TRUE).given(targetPartnerOrderFieldValidator).isOrderDataValid(orderDTO);
        willReturn(Boolean.TRUE).given(commerceCartService).calculateCart(any(CommerceCartParameter.class));

        given(cart.getCurrency()).willReturn(currencyModel);
        willReturn(Integer.valueOf(12345)).given(cart).getEBayOrderNumber();
        given(cart.getTotalPrice()).willReturn(Double.valueOf(10));
        given(ebayhelper.getCurrencyByCode(anyString())).willReturn(currencyModel);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(null);
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        given(currencyModel.getIsocode()).willReturn("AUD");
        given(orderCodeGenerator.generate()).willReturn("123456");
        given(orderDTO.getDeliveryAddress()).willReturn(addressDTO);
        given(orderDTO.getPaymentAddress()).willReturn(addressDTO);
        given(orderDTO.getSalesChannel()).willReturn("eBay");
        given(addressDTO.getCountry()).willReturn("AU");
        given(ebayhelper.populateAddressModel(addressDTO, "123456")).willReturn(address);
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        hmDMode.setIsShortDeliveryTime(Boolean.FALSE);
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(hmDMode, cncDMode));


    }

    @Test
    public void testCreateOrderforCustomerRegistration() throws InvalidCartException, ModelSavingException,
            DuplicateUidException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            ImportOrderException {

        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        final EbayTargetOrderEntriesDTO entriesDTO = mock(EbayTargetOrderEntriesDTO.class);

        given(orderDTO.getPaymentInfo()).willReturn(null);
        given(orderDTO.geteBayOrderNumber()).willReturn("123456");
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        given(orderDTO.getEbayTargetOrderEntriesDTO().getEntries()).willReturn(null);
        given(orderDTO.getBuyerEmail()).willReturn("madhu@test.com");
        given(orderDTO.getName()).willReturn("CustomerName");
        given(orderDTO.getDeliveryCost()).willReturn("10.00");
        given(orderDTO.getCreatedDate()).willReturn("2015-07-12T03:37:12 PM");
        given(paymentModeService.getPaymentModeForCode(anyString())).willReturn(paymentModeModel);
        given(targetCustomerAccountService.registerGuestForAnonymousCheckout(orderDTO.getBuyerEmail(),
                "Guest"))
                .willReturn(customer);

        given(modelService.create(CartModel.class)).willReturn(cart);
        given(commerceCheckoutService.placeOrder(any(CommerceCheckoutParameter.class))).willReturn(
                orderResult);
        given(orderModel.getCode()).willReturn("Ordercode");
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        given(orderResult.getOrder()).willReturn(orderModel);
        given(currencyConversion.getCurrencyConversionFactor()).willReturn(Double.valueOf(1.109));
        given(currencyConversionFactorService.findLatestCurrencyFactor(orderModel)).willReturn(
                currencyConversion);
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn(null);
        given(entriesDTO.getEntries()).willReturn(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        orderService.createPartnerOrders(orderDTO);

        verify(targetCustomerAccountService, times(1)).registerGuestForAnonymousCheckout(
                orderDTO.getBuyerEmail(), "Guest");
        verify(commerceCheckoutService, times(1)).placeOrder(
                any(CommerceCheckoutParameter.class));
        verify(ebayhelper, never()).populatePaymentInfo(anyString(),
                any(EbayTargetPayInfoDTO.class),
                any(CustomerModel.class));
        verify(modelService, times(1)).create(CartModel.class);
        verify(orderModel).setDate(any(Date.class));
        verify(orderModel).setCurrencyConversionFactor(any(Double.class));
        verify(modelService).save(orderModel);
    }

    @Test
    public void testCreateOrdersWithOrderEntries() throws InvalidCartException, ModelSavingException,
            DuplicateUidException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            ImportOrderException {

        final EbayTargetOrderEntriesDTO entriesDTO = mock(EbayTargetOrderEntriesDTO.class);
        orderDTO.seteBayOrderNumber("123456");
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(orderDTO.geteBayOrderNumber()).willReturn("123456");
        given(orderEntry.getBasePrice()).willReturn("10.00");
        given(orderEntry.getQuantity()).willReturn("2");
        given(orderEntry.getProductCode()).willReturn("CP2033");
        given(orderEntry.getTotalPrice()).willReturn("20.00");
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        given(orderDTO.getEbayTargetOrderEntriesDTO().getEntries())
                .willReturn(java.util.Collections.singletonList(orderEntry));
        given(orderDTO.getBuyerEmail()).willReturn("madhu@test.com");
        given(orderDTO.getName()).willReturn("CustomerName");
        given(orderDTO.getDeliveryCost()).willReturn("10.00");
        given(paymentModeService.getPaymentModeForCode(anyString())).willReturn(paymentModeModel);
        given(targetCustomerAccountService.registerGuestForAnonymousCheckout(orderDTO.getBuyerEmail(),
                "Guest"))
                .willReturn(customer);

        given(modelService.create(CartModel.class)).willReturn(cart);
        given(commerceCheckoutService.placeOrder(any(CommerceCheckoutParameter.class)))
                .willReturn(orderResult);
        given(orderModel.getCode()).willReturn("Ordercode");
        given(orderResult.getOrder()).willReturn(orderModel);
        given(orderDTO.getCreatedDate()).willReturn("2015-12-12T03:27:59 AM");

        given(orderModel.getSalesApplication()).willReturn(SalesApplication.EBAY);
        given(currencyConversion.getCurrencyConversionFactor()).willReturn(Double.valueOf(1.109));
        given(currencyConversionFactorService.findLatestCurrencyFactor(orderModel)).willReturn(
                currencyConversion);

        orderService.createPartnerOrders(orderDTO);

        verify(targetCustomerAccountService, times(1))
                .registerGuestForAnonymousCheckout(orderDTO.getBuyerEmail(), "Guest");
        verify(ebayhelper, timeout(1))
                .populateOrderEntries(anyListOf(EbayTargetOrderEntryDTO.class), any(CartModel.class));
        verify(commerceCheckoutService, times(1))
                .placeOrder(any(CommerceCheckoutParameter.class));
        verify(ebayhelper, times(1))
                .populateOrderEntries(anyListOf(EbayTargetOrderEntryDTO.class), any(CartModel.class));
        verify(modelService, times(1)).create(CartModel.class);
        verify(orderModel).setDate(any(Date.class));
        verify(orderModel, times(0)).setCurrencyConversionFactor(any(Double.class));
        verify(modelService).save(orderModel);
    }

    @Test
    public void testCreateOrderforCustomerRegistrationWithSpacesInEmailAddress()
            throws InvalidCartException, ModelSavingException,
            DuplicateUidException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            ImportOrderException {
        final String ebayOrderEmail = "     foo@bar.com     ";
        final String registeredEmail = "foo@bar.com";

        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        final EbayTargetOrderEntriesDTO entriesDTO = mock(EbayTargetOrderEntriesDTO.class);

        given(orderDTO.getPaymentInfo()).willReturn(null);
        given(orderDTO.geteBayOrderNumber()).willReturn("123456");
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        given(orderDTO.getEbayTargetOrderEntriesDTO().getEntries()).willReturn(null);
        given(orderDTO.getBuyerEmail()).willReturn(ebayOrderEmail);
        given(orderDTO.getName()).willReturn("CustomerName");
        given(orderDTO.getDeliveryCost()).willReturn("10.00");
        given(orderDTO.getCreatedDate()).willReturn("2015-07-12T03:37:12 PM");
        given(paymentModeService.getPaymentModeForCode(anyString())).willReturn(paymentModeModel);
        given(targetCustomerAccountService.registerGuestForAnonymousCheckout(registeredEmail,
                "Guest"))
                .willReturn(customer);

        given(modelService.create(CartModel.class)).willReturn(cart);
        given(commerceCheckoutService.placeOrder(any(CommerceCheckoutParameter.class))).willReturn(
                orderResult);
        given(orderModel.getCode()).willReturn("Ordercode");
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        given(orderResult.getOrder()).willReturn(orderModel);
        given(currencyConversion.getCurrencyConversionFactor()).willReturn(Double.valueOf(1.109));
        given(currencyConversionFactorService.findLatestCurrencyFactor(orderModel)).willReturn(
                currencyConversion);
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn(null);
        given(entriesDTO.getEntries()).willReturn(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        orderService.createPartnerOrders(orderDTO);

        verify(targetCustomerAccountService, times(1)).registerGuestForAnonymousCheckout(
                registeredEmail, "Guest");
        verify(commerceCheckoutService, times(1)).placeOrder(
                any(CommerceCheckoutParameter.class));
        verify(ebayhelper, never()).populatePaymentInfo(anyString(),
                any(EbayTargetPayInfoDTO.class),
                any(CustomerModel.class));
        verify(modelService, times(1)).create(CartModel.class);
        verify(orderModel).setDate(any(Date.class));
        verify(orderModel).setCurrencyConversionFactor(any(Double.class));
        verify(modelService).save(orderModel);
    }

    @Test
    public void testCreateOrdersWithPaymentInfo() throws InvalidCartException, ModelSavingException,
            DuplicateUidException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            ImportOrderException {
        verifyCreateOrderWithPaymentInfo();
    }

    @Test
    public void testReserveStock() {

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        final AbstractOrderEntryModel mockOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(4));
        given(mockOrderEntry.getProduct()).willReturn(product);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouse);
        given(orderModel.getEntries()).willReturn(java.util.Collections.singletonList(mockOrderEntry));
        orderService.reserveStockForPartnerOrder(orderModel);
        verify(targetStockService, times(1)).forceReserve(product,
                warehouse, 4,
                "We have reserved 4 of null from partner order number is null");
    }

    @Test
    public void testReserveStockWithFalconOn() throws InsufficientStockLevelException {
        given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON))).willReturn(Boolean.TRUE);
        final AbstractOrderEntryModel mockOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(4));
        given(mockOrderEntry.getProduct()).willReturn(product);
        given(orderModel.getEntries()).willReturn(java.util.Collections.singletonList(mockOrderEntry));
        orderService.reserveStockForPartnerOrder(orderModel);
        verify(targetStockService, times(1)).reserve(product, 4,
                "We have reserved 4 of null from partner order number is null");
    }

    @Test
    public void testOrderCreationWithoutDeliveryMode() throws DuplicateUidException, InvalidCartException,
            ModelSavingException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            ImportOrderException {
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(null);
        verifyCreateOrderWithPaymentInfo();
    }

    private void verifyCreateOrderWithPaymentInfo() throws InvalidCartException, ModelSavingException,
            DuplicateUidException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            ImportOrderException {
        paypalpaInfoModel = mock(PaypalPaymentInfoModel.class);
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel billingAddress = mock(AddressModel.class);

        final EbayTargetPayInfoDTO payInfoDTO = mock(EbayTargetPayInfoDTO.class);
        final EbayTargetAddressDTO deliveryAddress = mock(EbayTargetAddressDTO.class);
        final EbayTargetAddressDTO billingAddDTO = mock(EbayTargetAddressDTO.class);
        final EbayTargetOrderEntriesDTO entriesDTO = mock(EbayTargetOrderEntriesDTO.class);
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(orderDTO.geteBayOrderNumber()).willReturn("123456");
        given(orderDTO.getPaymentInfo()).willReturn(payInfoDTO);
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        given(orderDTO.getEbayTargetOrderEntriesDTO().getEntries()).willReturn(null);
        given(orderDTO.getBuyerEmail()).willReturn("madhu@test.com");
        given(orderDTO.geteBayOrderNumber()).willReturn("1234567");
        given(orderDTO.getName()).willReturn("CustomerName");
        given(orderDTO.getDeliveryCost()).willReturn("10.00");
        given(orderDTO.getPaymentAddress()).willReturn(billingAddDTO);
        given(payInfoDTO.getEmailId()).willReturn("test@test.com");
        given(payInfoDTO.getPayerId()).willReturn("test@test");
        given(payInfoDTO.getPaymentType()).willReturn("paypal");
        given(cart.getPaymentInfo()).willReturn(paypalpaInfoModel);
        given(cart.getPaymentAddress()).willReturn(billingAddress);
        given(orderDTO.getDeliveryAddress()).willReturn(deliveryAddress);
        given(deliveryAddress.getCountry()).willReturn("AU");
        given(ebayhelper.populateAddressModel(deliveryAddress, orderDTO.geteBayOrderNumber()))
                .willReturn(shippingAddress);
        given(ebayhelper.populateAddressModel(billingAddDTO, orderDTO.geteBayOrderNumber()))
                .willReturn(billingAddress);
        given(ebayhelper.populatePaymentInfo("paypal", payInfoDTO, customer))
                .willReturn(paypalpaInfoModel);
        given(paymentModeService.getPaymentModeForCode(anyString())).willReturn(paymentModeModel);
        given(paymentModeModel.getCode()).willReturn("paypal");
        given(targetCustomerAccountService.registerGuestForAnonymousCheckout(orderDTO.getBuyerEmail(),
                "Guest"))
                .willReturn(customer);
        given(customer.getUid()).willReturn("UID");
        given(customer.getDefaultShipmentAddress()).willReturn(shippingAddress);
        given(customer.getDefaultPaymentAddress()).willReturn(billingAddress);


        given(modelService.create(CartModel.class)).willReturn(cart);
        given(commerceCheckoutService.placeOrder(any(CommerceCheckoutParameter.class)))
                .willReturn(orderResult);
        given(orderModel.getCode()).willReturn("Ordercode");
        given(orderResult.getOrder()).willReturn(orderModel);
        given(orderDTO.getCreatedDate()).willReturn("2015-09-12T03:27:59 AM");
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(hmDMode, cncDMode));
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn(null);
        given(entriesDTO.getEntries()).willReturn(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);

        orderService.createPartnerOrders(orderDTO);

        verify(targetCustomerAccountService, times(1))
                .registerGuestForAnonymousCheckout(orderDTO.getBuyerEmail(), "Guest");
        verify(ebayhelper, times(1)).populatePaymentInfo("paypal", payInfoDTO, customer);
        verify(ebayhelper, times(1)).populateAddressModel(deliveryAddress,
                orderDTO.geteBayOrderNumber());
        verify(commerceCheckoutService, times(1))
                .placeOrder(any(CommerceCheckoutParameter.class));
        verify(modelService, times(1)).create(CartModel.class);
        verify(orderModel).setDate(any(Date.class));
        verify(modelService).save(orderModel);
    }

    @Test
    public void testGetSalesChannelFromDtoNull() {
        assertThat(orderService.getSalesChannelFromDto(null)).isEqualTo(SalesApplication.EBAY);
    }

    @Test
    public void testGetSalesChannelFromDtoEmpty() {
        assertThat(orderService.getSalesChannelFromDto(StringUtils.EMPTY)).isEqualTo(SalesApplication.EBAY);
    }

    @Test
    public void testGetSalesChannelFromDtoBlank() {
        assertThat(orderService.getSalesChannelFromDto(" ")).isEqualTo(SalesApplication.EBAY);
    }

    @Test
    public void testGetSalesChannelFromDtoEBay() {
        assertThat(orderService.getSalesChannelFromDto("eBay")).isEqualTo(SalesApplication.EBAY);
    }

    @Test
    public void testGetSalesChannelFromDtoTradeMe() {
        assertThat(orderService.getSalesChannelFromDto("TradeMe")).isEqualTo(SalesApplication.TRADEME);
    }

    @Test
    public void testPopulateOrderDeliveryDetailsForCNCOrder() throws Exception {
        final EbayTargetOrderEntriesDTO entriesDTO = new EbayTargetOrderEntriesDTO();
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn("Pickup");
        given(orderEntry.getCncStoreNumber()).willReturn("7196");
        entriesDTO.setEntries(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        final CartModel cartModel = new CartModel();
        cartModel.setCncStoreNumber(Integer.valueOf(7196));
        final TargetCustomerModel customerModel = mock(TargetCustomerModel.class);
        final AddressModel shipAddress = mock(AddressModel.class);
        final TitleModel titleModel = mock(TitleModel.class);
        given(customerModel.getDefaultShipmentAddress()).willReturn(shipAddress);
        given(shipAddress.getFirstname()).willReturn("FirstName");
        given(shipAddress.getLastname()).willReturn("LastName");
        given(shipAddress.getTitle()).willReturn(titleModel);
        given(shipAddress.getPhone1()).willReturn("535345");
        given(fulfillmentTypeToDeliveryToStoreMapping.get("Pickup")).willReturn(Boolean.TRUE);
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        final TargetPointOfServiceModel targetPOS = mock(TargetPointOfServiceModel.class);
        given(targetPOS.getAddress()).willReturn(address);
        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(7196)))
                .willReturn(targetPOS);
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);

        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(cncDMode, hmDMode));
        orderService.populateOrderDeliveryDetails(cartModel, orderDTO, SalesApplication.EBAY, customerModel);
        verify(commerceCheckoutService).fillCart4ClickAndCollectWithoutRecalculate(cartModel,
                Integer.valueOf(7196), shipAddress.getTitle(), shipAddress.getFirstname(), shipAddress.getLastname(),
                shipAddress.getPhone1(), cncDMode);
    }

    @Test
    public void testPopulateOrderDeliveryDetailsForHomeDelivery() throws Exception {
        final EbayTargetOrderEntriesDTO entriesDTO = new EbayTargetOrderEntriesDTO();
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn("Ship");
        given(orderEntry.getCncStoreNumber()).willReturn("7196");
        entriesDTO.setEntries(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        final CartModel cartModel = new CartModel();
        final TargetCustomerModel customerModel = mock(TargetCustomerModel.class);
        given(customerModel.getDefaultShipmentAddress()).willReturn(address);
        given(fulfillmentTypeToDeliveryToStoreMapping.get("Ship")).willReturn(Boolean.FALSE);
        given(orderDTO.getDeliveryAddress()).willReturn(null);
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        hmDMode.setIsShortDeliveryTime(Boolean.FALSE);
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(hmDMode, cncDMode));
        orderService.populateOrderDeliveryDetails(cartModel, orderDTO, SalesApplication.EBAY, customerModel);
        assertThat(cartModel.getDeliveryAddress()).isEqualTo(address);
        assertThat(cartModel.getDeliveryMode()).isEqualTo(hmDMode);
    }

    @Test
    public void testPopulateOrderDeliveryDetailsForHomeDeliveryWhenFulfilmentNull() throws Exception {
        final EbayTargetOrderEntriesDTO entriesDTO = new EbayTargetOrderEntriesDTO();
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn(null);
        entriesDTO.setEntries(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        given(orderDTO.getDeliveryAddress()).willReturn(null);
        final CartModel cartModel = new CartModel();
        final TargetCustomerModel customerModel = mock(TargetCustomerModel.class);
        given(customerModel.getDefaultShipmentAddress()).willReturn(address);
        given(fulfillmentTypeToDeliveryToStoreMapping.get("Ship")).willReturn(Boolean.FALSE);
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        hmDMode.setIsShortDeliveryTime(Boolean.FALSE);
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(hmDMode, cncDMode));
        orderService.populateOrderDeliveryDetails(cartModel, orderDTO, SalesApplication.EBAY, customerModel);
        assertThat(cartModel.getDeliveryAddress()).isEqualTo(address);
        assertThat(cartModel.getDeliveryMode()).isEqualTo(hmDMode);
    }

    @Test
    public void testPopulateOrderDeliveryDetailsForEbayExpressDelivery() throws Exception {
        final EbayTargetOrderEntriesDTO entriesDTO = new EbayTargetOrderEntriesDTO();
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn("Ship");
        given(orderEntry.getCncStoreNumber()).willReturn("7196");
        entriesDTO.setEntries(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        final CartModel cartModel = new CartModel();
        final TargetCustomerModel customerModel = mock(TargetCustomerModel.class);
        given(customerModel.getDefaultShipmentAddress()).willReturn(address);
        given(fulfillmentTypeToDeliveryToStoreMapping.get("Ship")).willReturn(Boolean.FALSE);
        given(shippingClassToShortDeliveryTimeMapping.get("EXP")).willReturn(Boolean.TRUE);
        given(addressDTO.getShippingClass()).willReturn("EXP");
        given(orderDTO.getDeliveryAddress()).willReturn(addressDTO);
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        hmDMode.setIsShortDeliveryTime(Boolean.TRUE);
        hmDMode.setCode("ebay-express-delivery");
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(hmDMode, cncDMode));
        orderService.populateOrderDeliveryDetails(cartModel, orderDTO, SalesApplication.EBAY, customerModel);
        assertThat(cartModel.getDeliveryAddress()).isEqualTo(address);
        assertThat(cartModel.getDeliveryMode()).isEqualTo(hmDMode);
    }

    @Test
    public void testPopulateOrderDeliveryDetailsForEbayExpressDeliveryWithNoActiveExpressDeliverMode()
            throws Exception {
        final EbayTargetOrderEntriesDTO entriesDTO = new EbayTargetOrderEntriesDTO();
        final EbayTargetOrderEntryDTO orderEntry = mock(EbayTargetOrderEntryDTO.class);
        given(orderEntry.getFulfillmentType()).willReturn("Ship");
        given(orderEntry.getCncStoreNumber()).willReturn("7196");
        entriesDTO.setEntries(Arrays.asList(orderEntry));
        given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(entriesDTO);
        final CartModel cartModel = new CartModel();
        final TargetCustomerModel customerModel = mock(TargetCustomerModel.class);
        given(customerModel.getDefaultShipmentAddress()).willReturn(address);
        given(fulfillmentTypeToDeliveryToStoreMapping.get("Ship")).willReturn(Boolean.FALSE);
        given(shippingClassToShortDeliveryTimeMapping.get("EXP")).willReturn(Boolean.TRUE);
        given(addressDTO.getShippingClass()).willReturn("EXP");
        given(orderDTO.getDeliveryAddress()).willReturn(addressDTO);
        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        hmDMode.setIsShortDeliveryTime(Boolean.FALSE);
        hmDMode.setCode("ebay-delivery");
        given(targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .willReturn(Arrays.asList(hmDMode, cncDMode));
        orderService.populateOrderDeliveryDetails(cartModel, orderDTO, SalesApplication.EBAY, customerModel);
        assertThat(cartModel.getDeliveryAddress()).isEqualTo(address);
        assertThat(cartModel.getDeliveryMode()).isEqualTo(hmDMode);
    }

}
