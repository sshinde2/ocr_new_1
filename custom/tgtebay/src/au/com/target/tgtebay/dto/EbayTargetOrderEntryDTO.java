/**
 * 
 */
package au.com.target.tgtebay.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetOrderEntryDTO {

    @XmlElement
    private String basePrice;
    @XmlElement
    private String quantity;
    @XmlElement
    private String totalPrice;
    @XmlElement
    private String productCode;
    @XmlElement
    private String unitcode;
    @XmlElement
    private String fulfillmentType;
    @XmlElement
    private String cncStoreNumber;

    /**
     * @return the basePrice
     */
    public String getBasePrice() {
        return basePrice;
    }

    /**
     * @param basePrice
     *            the basePrice to set
     */
    public void setBasePrice(final String basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the totalPrice
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final String totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the unitcode
     */
    public String getUnitcode() {
        return unitcode;
    }

    /**
     * @param unitcode
     *            the unitcode to set
     */
    public void setUnitcode(final String unitcode) {
        this.unitcode = unitcode;
    }

    /**
     * @return the fulfillmentType
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * @param fulfillmentType the fulfillmentType to set
     */
    public void setFulfillmentType(final String fulfillmentType) {
        this.fulfillmentType = fulfillmentType;
    }

    /**
     * @return the cncStoreNumber
     */
    public String getCncStoreNumber() {
        return cncStoreNumber;
    }

    /**
     * @param cncStoreNumber the cncStoreNumber to set
     */
    public void setCncStoreNumber(final String cncStoreNumber) {
        this.cncStoreNumber = cncStoreNumber;
    }

}
