/**
 * 
 */
package au.com.target.tgtebay.logger;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.text.MessageFormat;

import org.apache.log4j.Logger;


/**
 * @author ragarwa3
 *
 */
public class PartnerInventoryUpdateLogger {

    private static final String LOGGING_PREFIX = "PartnerInventoryUpdate: ";

    private Logger log = null;

    public PartnerInventoryUpdateLogger(final Class classname) {
        log = Logger.getLogger(classname);
    }

    public void logInventoryUpdateEntry(final String order, final String product, final int stock) {

        logInventoryUpdate("Start", order, product, stock);
    }

    public void logInventoryUpdateSuccess(final String order, final String product, final int stock) {

        logInventoryUpdate("Success", order, product, stock);
    }

    public void logInventoryUpdateFailure(final String order, final String product, final int stock) {

        logInventoryUpdate("Failure", order, product, stock);
    }

    public void logInventoryUpdateSkip(final String order, final SalesApplication salesApp) {

        log.info(LOGGING_PREFIX + MessageFormat.format("state=Skip, order={0}, salesApplication={1}", order, salesApp));
    }

    private void logInventoryUpdate(final String state, final String order, final String product, final int stock) {

        log.info(LOGGING_PREFIX + MessageFormat.format("state={0}, order={1}, product={2}, stock={3}", state, order,
                product, Integer.valueOf(stock)));
    }

    public void logRetryExceededError(final String order) {
        log.error(LOGGING_PREFIX + MessageFormat.format("state=RETRYEXCEEDED, order={0}", order));
    }

}
