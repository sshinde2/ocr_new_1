/**
 * 
 */
package au.com.target.tgtebay.stockupdate;


/**
 * @author bhuang3
 * 
 */
public interface StockUpdateToPartnerFacade {

    /**
     * call the rest web service to update the stock only for partner product
     * 
     * @param productCode
     * @param quantity
     * @return boolean true: if it is the partner sellable product and updated success. else false
     */
    boolean updateStock(final String productCode, final int quantity);

    /**
     * get the available stock level and call the updateStock to update to ca.
     * 
     * @param productCode
     * @return boolean true: if it is the partner sellable product and updated success. else false
     */
    boolean updateAvailableStock(final String productCode);

}
