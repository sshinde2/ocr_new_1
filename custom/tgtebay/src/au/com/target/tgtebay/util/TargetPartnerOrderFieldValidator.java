package au.com.target.tgtebay.util;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtebay.dto.EbayTargetAddressDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntriesDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.dto.EbayTargetPayInfoDTO;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtutility.util.TargetValidationCommon;
import au.com.target.tgtutility.util.TargetValidationUtils;


/**
 * @author mjanarth
 * 
 */
public class TargetPartnerOrderFieldValidator {

    private static final Logger LOG = Logger.getLogger(TargetPartnerOrderFieldValidator.class);

    private static final String ERR_PREIMPORT_VALIDATION = TgtutilityConstants.ErrorCode.ERR_TGTEBAY
            + ": Pre-import order validation failed for partner order with partnerOrderNumber={0}, salesApplication={1}, reason={2}";

    private Set<String> validCurrencies;

    private Set<String> validCountries;

    private Map<String, String> paymentTypeToPaymentMethodMapping;

    private TargetOrderService targetOrderService;

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    private Map<String, Boolean> fulfillmentTypeToDeliveryToStoreMapping;

    /**
     * 
     * @param phoneNumber
     * @param country
     * @return boolean
     */
    public boolean validatePhoneNumber(final String phoneNumber, final String country) {
        final int minSize;
        if (country.equals(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA)) {
            minSize = TargetValidationCommon.Phone.MIN_SIZE_AU;
        }
        else {
            minSize = TargetValidationCommon.Phone.MIN_SIZE_NZ;
        }
        return TargetValidationUtils.matchRegularExpression(phoneNumber, TargetValidationCommon.Phone.PATTERN,
                minSize, TargetValidationCommon.Phone.MAX_SIZE, true);
    }

    /**
     * 
     * @param emailAddress
     * @return boolean
     */
    public boolean validateEmail(final String emailAddress) {
        final Pattern pattern = TargetValidationCommon.Email.PATTERN;
        return pattern.matcher(emailAddress).matches();
    }

    /**
     * 
     * @param addressDTO
     * @return boolean
     */
    public boolean isEmptyAddress(final EbayTargetAddressDTO addressDTO) {
        if (addressDTO != null) {
            if (StringUtils.isBlank(addressDTO.getStreetname()) && StringUtils.isBlank(addressDTO.getStreetNumber())
                    && StringUtils.isBlank(addressDTO.getTown()) && StringUtils.isBlank(addressDTO.getDistrict())) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * 
     * @param doubleValue
     * @param fieldName
     * @return double
     */
    public Double parseDouble(final String doubleValue, final String fieldName, final String ebayOrderNumber) {
        Double value = new Double(0.0);
        if (StringUtils.isBlank(doubleValue)) {
            return value;
        }
        else {
            try {
                value = Double.valueOf(doubleValue.trim());
            }
            catch (final NumberFormatException nfe) {
                LOG.warn(
                        SplunkLogFormatter.formatMessage(MessageFormat.format(
                                "Invalid value for the field   while creating partner order field={0}-{1}, partnerOrderNumber={1}",
                                fieldName, doubleValue, ebayOrderNumber),
                                TgtutilityConstants.ErrorCode.WARN_TGTEBAY));
                return TgtebayConstants.DOUBLE_ZERO;
            }
        }
        return value;

    }

    /**
     * 
     * @param longValue
     * @param fieldName
     * @return long
     */
    public Long parseLong(final String longValue, final String fieldName, final String ebayOrderNumber) {
        Long value = Long.valueOf(0);
        if (StringUtils.isEmpty(longValue)) {
            return value;
        }
        else {
            try {
                value = Long.valueOf(longValue.trim());
            }
            catch (final NumberFormatException nfe) {
                LOG.warn(
                        SplunkLogFormatter.formatMessage(MessageFormat
                                .format("Invalid value for the field while creating partner order field={0}-{1}, partnerOrderNumber={1}"
                                        + fieldName, longValue, ebayOrderNumber),
                                TgtutilityConstants.ErrorCode.ERR_TGTEBAY));
                return TgtebayConstants.LONG_ZERO;
            }
        }
        return value;
    }

    /**
     * Checks if order data is valid. If not, order will not be imported.
     *
     * @param order
     *            the order
     * @return true, if is valid order dto
     */
    public boolean isOrderDataValid(final EbayTargetOrderDTO order) {

        //validate whether we already have a order with this partner order number and for same partner i.e. sales channel
        final String eBayOrderNumber = order.geteBayOrderNumber();
        final String salesChannel = order.getSalesChannel();

        if (!validatePartnerOrderNumber(eBayOrderNumber, salesChannel)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "Order already exists with given orderID for salesChannel=" + salesChannel));
            return false;
        }

        // 1. validate sales channel
        // Note: null/blank value is valid and treated as partner
        if (StringUtils.isNotBlank(salesChannel) && !validateSalesChannel(salesChannel)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "Invalid sales channel string received, salesChannel=" + salesChannel));
            return false;
        }

        // 2. validate currency code
        // Note: null/blank value is valid and treated as AUD
        final String currencyCode = order.getCurrencyCode();
        if (StringUtils.isNotBlank(currencyCode)) {

            if (!validCurrencies.contains(currencyCode)) {
                LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                        "Invalid currency string received, currencyCode=" + currencyCode));
                return false;
            }
        }

        // 3. validate country
        // Note: Country must be either AU or NZ
        final EbayTargetAddressDTO address = order.getDeliveryAddress();
        if (null == address) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "No shipping address received."));
            return false;
        }

        final String countryIsoCode = address.getCountry();
        if (StringUtils.isBlank(countryIsoCode)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "No country received."));
            return false;
        }
        if (!validCountries.contains(countryIsoCode)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "Invalid country received, countryIsoCode=" + countryIsoCode));
            return false;
        }

        // 4. validate whether payment info is present. If yes, payment-type and payment-method mapping is also present.
        final EbayTargetPayInfoDTO paymentInfo = order.getPaymentInfo();
        if (null == paymentInfo) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "No payment info received."));
            return false;
        }

        final String paymentType = paymentInfo.getPaymentType();
        if (StringUtils.isBlank(paymentType)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "No payment type received."));
            return false;
        }
        if (!paymentTypeToPaymentMethodMapping.keySet().contains(paymentType)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "Invalid payment type received, paymentType=" + paymentType));
            return false;
        }

        // 5. Validate order entries exist for the order
        if (order.getEbayTargetOrderEntriesDTO() == null
                || CollectionUtils.isEmpty(order.getEbayTargetOrderEntriesDTO().getEntries())) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "No order entries found"));
            return false;
        }

        // 6. Validate all entries are of same fulfillment type and for same store
        return validatePartnerOrderEntriesForFulfillmentType(order.getEbayTargetOrderEntriesDTO(), eBayOrderNumber,
                salesChannel);
    }

    /**
     * Validate sales channel.
     *
     * @param salesChannel
     *            the sales channel
     * @return true, if successful
     */
    protected boolean validateSalesChannel(final String salesChannel) {
        final List<SalesApplicationConfigModel> salesAppConfigs = salesApplicationConfigService
                .getAllPartnerSalesApplicationConfigs();

        if (CollectionUtils.isEmpty(salesAppConfigs)) {
            return false;
        }

        for (final SalesApplicationConfigModel config : salesAppConfigs) {
            final SalesApplication salesApp = config.getSalesApplication();
            if (null != salesApp && salesApp.toString().equals(salesChannel)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method to validate partner order entries contains same fulfillment type and same store number
     * 
     * @param ebayTargetOrderEntriesDTO
     * @param salesChannel
     * @return boolean
     */
    protected boolean validatePartnerOrderEntriesForFulfillmentType(
            final EbayTargetOrderEntriesDTO ebayTargetOrderEntriesDTO, final String eBayOrderNumber,
            final String salesChannel) {

        final String fulfillmentType = ebayTargetOrderEntriesDTO.getEntries().get(0).getFulfillmentType();
        final String cncStoreNumber = ebayTargetOrderEntriesDTO.getEntries().get(0).getCncStoreNumber();

        // validate that we have got the acceptable fulfillment type 
        if (fulfillmentType != null && !fulfillmentTypeToDeliveryToStoreMapping.containsKey(fulfillmentType)) {
            LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                    "Invalid fulfillment type received, fulfillmentType=" + fulfillmentType));
            return false;
        }

        for (final EbayTargetOrderEntryDTO orderEntry : ebayTargetOrderEntriesDTO.getEntries()) {

            // validate that all entries have same fulfillment type
            if (!StringUtils.equals(fulfillmentType, orderEntry.getFulfillmentType())) {
                LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                        "In-consistent fulfillment type received, fulfillmentType=" + fulfillmentType
                                + " and fulfillmentType=" + orderEntry.getFulfillmentType()));
                return false;
            }

            // validate that all entries have same cnc store number if fulfillment type is deliveryToStore
            if (BooleanUtils.isTrue(fulfillmentTypeToDeliveryToStoreMapping.get(fulfillmentType))
                    && !StringUtils.equals(cncStoreNumber, orderEntry.getCncStoreNumber())) {
                LOG.error(MessageFormat.format(ERR_PREIMPORT_VALIDATION, eBayOrderNumber, salesChannel,
                        "In-consistent cnc store number received, cncStoreNumber=" + cncStoreNumber
                                + " and cncStoreNumber=" + orderEntry.getCncStoreNumber()));
                return false;
            }
        }

        return true;
    }

    /**
     * Checks that there shouldn't be an existing order with given eBayOrderNumber and salesChannel
     * 
     * @param ebayOrderNumber
     * @param salesChannel
     */
    protected boolean validatePartnerOrderNumber(final String ebayOrderNumber, final String salesChannel) {

        if (StringUtils.isEmpty(ebayOrderNumber)) {
            return false;
        }
        final List<OrderModel> orders = targetOrderService
                .findOrdersForPartnerOrderIdAndSalesChannel(ebayOrderNumber, salesChannel);
        if (null != orders && orders.size() >= 1) {
            return false;
        }
        return true;
    }

    /**
     * @param validCurrencies
     *            the validCurrencies to set
     */
    @Required
    public void setValidCurrencies(final Set<String> validCurrencies) {
        this.validCurrencies = validCurrencies;
    }

    /**
     * @param validCountries
     *            the validCountries to set
     */
    @Required
    public void setValidCountries(final Set<String> validCountries) {
        this.validCountries = validCountries;
    }

    /**
     * @param paymentTypeToPaymentMethodMapping
     *            the paymentTypeToPaymentMethodMapping to set
     */
    @Required
    public void setPaymentTypeToPaymentMethodMapping(final Map<String, String> paymentTypeToPaymentMethodMapping) {
        this.paymentTypeToPaymentMethodMapping = paymentTypeToPaymentMethodMapping;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

    /**
     * @param fulfillmentTypeToDeliveryToStoreMapping
     *            the fulfillmentTypeToDeliveryToStoreMapping to set
     */
    @Required
    public void setFulfillmentTypeToDeliveryToStoreMapping(
            final Map<String, Boolean> fulfillmentTypeToDeliveryToStoreMapping) {
        this.fulfillmentTypeToDeliveryToStoreMapping = fulfillmentTypeToDeliveryToStoreMapping;
    }

}
