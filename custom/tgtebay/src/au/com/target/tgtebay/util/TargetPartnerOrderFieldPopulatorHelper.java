/**
 * 
 */
package au.com.target.tgtebay.util;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtebay.dto.EbayTargetAddressDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.dto.EbayTargetPayInfoDTO;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.model.PaynowPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author mjanarth
 * 
 */

public class TargetPartnerOrderFieldPopulatorHelper {
    private static final Logger LOG = Logger.getLogger(TargetPartnerOrderFieldPopulatorHelper.class);

    private static final String DEFAULT_UNIT_CODE = "pieces";
    private static final int MIN_NAME_LENGTH = 2;
    private static final int MAX_NAME_LENGTH = 32;

    private static final Pattern NAME_ALLOWED_CHARS_PATTERN = Pattern.compile("[^A-Za-z-' ]"); // Match anything that isn't A-Z, a-z, -, ' or a space

    private FlexibleSearchService flexibleSearchService;
    private ModelService modelService;
    private ProductService productService;
    private UnitService unitService;
    private TargetPartnerOrderFieldValidator targetPartnerOrderFieldValidator;
    private PaymentMethodStrategy paymentMethodStrategy;

    /**
     * This method converts the PaypalPaymentInfoDTO to PaypalPaymentInfoModel
     * 
     * @param customerModel
     * @param paymentInfoDTO
     * @return PaypalPaymentInfoModel
     */
    public PaymentInfoModel populatePaymentInfo(final String paymentMode,
            final EbayTargetPayInfoDTO paymentInfoDTO,
            final CustomerModel customerModel) {

        if (null != paymentInfoDTO && null != customerModel) {

            if (StringUtils.equals(paymentMode, TgtpaymentConstants.PAY_PAL_PAYMENT_TYPE)) {
                final PaypalPaymentInfoModel paymentInfoModel = modelService
                        .create(PaypalPaymentInfoModel.class);
                paymentInfoModel.setEmailId(paymentInfoDTO.getPaypalAccountId());
                paymentInfoModel.setPayerId(TgtebayConstants.EBAY_PAYPAL_PAYERID);
                paymentInfoModel.setToken(TgtebayConstants.EBAY_PAYPAL_TOKEN);
                paymentInfoModel.setIsPaymentSucceeded(Boolean.TRUE);
                paymentInfoModel.setUser(customerModel);
                paymentInfoModel.setBillingAddress(customerModel.getDefaultPaymentAddress());
                return paymentInfoModel;
            }
            else if (StringUtils.equals(paymentMode, TgtpaymentConstants.PAY_NOW_PAYMENT_TYPE)) {
                final PaynowPaymentInfoModel paymentInfoModel = modelService
                        .create(PaynowPaymentInfoModel.class);
                paymentInfoModel.setEmailId(paymentInfoDTO.getPaypalAccountId());
                paymentInfoModel.setPayerId(TgtebayConstants.TRADEME_PAYNOW_PAYERID);
                paymentInfoModel.setIsPaymentSucceeded(Boolean.TRUE);
                paymentInfoModel.setUser(customerModel);
                paymentInfoModel.setBillingAddress(customerModel.getDefaultPaymentAddress());
                return paymentInfoModel;
            }
        }

        return null;
    }

    /**
     * This method populates AddressModel from AddressDTO
     * 
     * @param addressDTO
     * @return addressModel
     */
    public AddressModel populateAddressModel(
            final EbayTargetAddressDTO addressDTO, final String eBayOrderNumber) {
        final AddressModel addressModel = modelService
                .create(AddressModel.class);

        populateNames(addressDTO, addressModel, eBayOrderNumber);

        addressModel.setBuilding(addressDTO.getBuilding());
        if (!(targetPartnerOrderFieldValidator.validatePhoneNumber(addressDTO.getPhone1(), addressDTO.getCountry()))) {
            LOG.warn(
                    SplunkLogFormatter.formatMessage(
                            MessageFormat.format(
                                    "Invalid phone Number while creating order for partner phoneNumber={0}, partnerOrderNumber={1}",
                                    addressDTO.getPhone1(), eBayOrderNumber),
                            TgtutilityConstants.ErrorCode.WARN_TGTEBAY));
        }
        addressModel.setPhone1(addressDTO.getPhone1());
        if (!(targetPartnerOrderFieldValidator.validatePhoneNumber(addressDTO.getPhone2(), addressDTO.getCountry()))) {
            LOG.warn(
                    SplunkLogFormatter.formatMessage(MessageFormat.format(
                            "Invalid phone Number while creating order for partner phoneNumber={0}, partnerOrderNumber={1}",
                            addressDTO.getPhone2(), eBayOrderNumber),
                            TgtutilityConstants.ErrorCode.WARN_TGTEBAY));
        }

        addressModel.setTitle(getTitle(addressDTO.getTitle()));
        addressModel.setPhone2(addressDTO.getPhone2());
        addressModel.setFax(addressDTO.getFax());
        addressModel.setTown(addressDTO.getTown());
        addressModel.setDistrict(addressDTO.getDistrict());
        if (StringUtils.isNotEmpty(addressDTO.getAddressLine1())) {
            addressModel.setStreetname(addressDTO.getAddressLine1());
        }
        if (StringUtils.isNotEmpty(addressDTO.getAddressLine2())) {
            addressModel.setStreetnumber(addressDTO.getAddressLine2());
        }
        addressModel.setPostalcode(addressDTO.getPostcode());
        addressModel.setCompany(addressDTO.getCompany());
        addressModel.setCountry(getCountryByCountryCode(addressDTO.getCountry()));
        return addressModel;
    }

    protected void populateNames(final EbayTargetAddressDTO addressDTO, final AddressModel addressModel,
            final String eBayOrderNumber) {
        final String cleanFirstName = cleanNameValue(addressDTO.getFirstName());
        final String cleanLastName = cleanNameValue(addressDTO.getLastName());

        final StringBuilder firstNameBuffer = new StringBuilder(cleanFirstName);
        final StringBuilder lastNameBuffer = new StringBuilder(cleanLastName);

        if (firstNameBuffer.length() < MIN_NAME_LENGTH) {
            firstNameBuffer.append(cleanLastName);
        }

        if (lastNameBuffer.length() < MIN_NAME_LENGTH) {
            lastNameBuffer.insert(0, cleanFirstName);
        }

        final String finalFirstName = shrinkName(firstNameBuffer.toString());
        final String finalLastName = shrinkName(lastNameBuffer.toString());

        if (finalFirstName.length() < MIN_NAME_LENGTH || finalLastName.length() < MIN_NAME_LENGTH) {
            LOG.warn(SplunkLogFormatter.formatMessage(MessageFormat.format(
                    "Not enough detail left for first or last name after cleaning for partner order partnerOrderNumber={0}",
                    eBayOrderNumber),
                    TgtutilityConstants.ErrorCode.WARN_TGTEBAY));
            addressModel.setFirstname(addressDTO.getFirstName());
            addressModel.setLastname(addressDTO.getLastName());
        }
        else {
            addressModel.setFirstname(finalFirstName);
            addressModel.setLastname(finalLastName);
        }
    }

    private String cleanNameValue(final String name) {
        // replacing all character other than A-z, a-z, -, ' or space
        String cleanFirstName = NAME_ALLOWED_CHARS_PATTERN.matcher(name).replaceAll(" ");

        // replacing " - " or " -" or "- " to "-" AND replacing " ' " or " '" or "' " to "'" 
        cleanFirstName = cleanFirstName.replaceFirst("^[-']", "").replaceAll("\\s*-\\s*", "-")
                .replaceAll("\\s*'\\s*", "'").replaceFirst("[-']$", "");

        cleanFirstName = StringUtils.trimToEmpty(cleanFirstName);
        cleanFirstName = cleanFirstName.replaceAll("\\s+", " ");

        return cleanFirstName;
    }

    private String shrinkName(String name) {
        if (name.length() > MAX_NAME_LENGTH) {
            name = name.substring(0, MAX_NAME_LENGTH);
        }

        return name;
    }

    /**
     * This method populates the list of AbstractOrderEntryModel from list of AbstractOrderEntryDTO
     * 
     * @param orderentries
     * @return List<AbstractOrderEntryModel>
     */
    public List<AbstractOrderEntryModel> populateOrderEntries(
            final List<EbayTargetOrderEntryDTO> orderentries, final CartModel cartModel) throws InvalidCartException {

        String eBayOrderNumber = null;
        if (cartModel.getEBayOrderNumber() != null) {
            eBayOrderNumber = cartModel.getEBayOrderNumber().toString();
        }
        cartModel.setPreserveItemValue(Boolean.TRUE);
        final List<AbstractOrderEntryModel> orderEntryModelList = new ArrayList<>();
        for (final EbayTargetOrderEntryDTO orderEntry : orderentries) {
            final CartEntryModel orderEntryModel = modelService
                    .create(CartEntryModel.class);

            if (orderEntry != null) {
                orderEntryModel.setBasePrice(targetPartnerOrderFieldValidator.parseDouble(orderEntry.getBasePrice(),
                        TgtebayConstants.BASE_PRICE,
                        eBayOrderNumber));
                orderEntryModel.setCalculated(Boolean.FALSE);
                orderEntryModel.setGiveAway(Boolean.FALSE);
                orderEntryModel.setQuantity(targetPartnerOrderFieldValidator.parseLong(orderEntry.getQuantity(),
                        TgtebayConstants.ORDER_ENTRY_QUANTITY,
                        eBayOrderNumber));
                orderEntryModel.setOrder(cartModel);
                orderEntryModel.setUnit(unitService.getUnitForCode(DEFAULT_UNIT_CODE));
                if (StringUtils.isNotEmpty(orderEntry.getProductCode())) {

                    final ProductModel productModel = productService.getProductForCode(orderEntry.getProductCode());
                    if (productModel != null && CollectionUtils.isEmpty(productModel.getVariants())) {
                        orderEntryModel.setProduct(productModel);
                    }
                    else {
                        throw new InvalidCartException("No Products available for product Code "
                                + orderEntry.getProductCode() + " while creating partner Order " + eBayOrderNumber);
                    }
                }
                else {
                    throw new InvalidCartException("Product code for partner Order " + eBayOrderNumber + " is null");
                }
                orderEntryModelList.add(orderEntryModel);
            }

        }
        return orderEntryModelList;
    }

    /***
     * This method populates the paymentransactions
     * 
     * @param paymentInfoModel
     * @param cartmodel
     * @param uid
     * @return transactionsList
     */
    public List<PaymentTransactionModel> populatePaymentTransaction(final EbayTargetOrderDTO order,
            final CartModel cartmodel, final PaymentInfoModel paymentInfoModel, final String uid) {
        Assert.notNull(order, "Order cannot be null");
        final EbayTargetPayInfoDTO paymentInfo = order.getPaymentInfo();
        Assert.notNull(paymentInfo, "payment information cannot be null");
        final CurrencyModel currency = cartmodel.getCurrency();
        final BigDecimal amount = new BigDecimal(paymentInfo.getPlannedAmount());
        final List<PaymentTransactionModel> transactionsList = new ArrayList<>();
        final List<PaymentTransactionEntryModel> entryList = new ArrayList<>();


        final PaymentTransactionModel paymentTransactionModel = modelService
                .create(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel paymentTransactionEntry = modelService
                .create(PaymentTransactionEntryModel.class);
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(paymentInfoModel);
        paymentTransactionModel.setCode(uid + "_" + UUID.randomUUID());
        paymentTransactionModel.setCurrency(currency);
        paymentTransactionModel.setPlannedAmount(amount);
        paymentTransactionModel.setOrder(cartmodel);
        paymentTransactionModel.setInfo(paymentInfoModel);
        paymentTransactionModel.setPaymentProvider(paymentMethod.getPaymentProvider());

        paymentTransactionEntry.setAmount(amount);
        paymentTransactionEntry.setCode(getNewEntryCode(paymentTransactionModel));
        paymentTransactionEntry.setRequestId(paymentInfo.getTransactionId());
        paymentTransactionEntry.setType(PaymentTransactionType.CAPTURE);
        paymentTransactionEntry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        paymentTransactionEntry.setTime(TargetDateUtil.getStringAsDate(order.getCreatedDate()));
        if (StringUtils.isNotEmpty(paymentInfo.getReceiptNumber())) {
            paymentTransactionEntry.setReceiptNo(paymentInfo.getReceiptNumber());
        }
        else {
            paymentTransactionEntry.setReceiptNo(TgtebayConstants.NOT_APPLICABLE);
        }
        paymentTransactionEntry.setCurrency(currency);
        entryList.add(paymentTransactionEntry);
        paymentTransactionModel.setEntries(entryList);
        transactionsList.add(paymentTransactionModel);
        return transactionsList;

    }

    /**
     * Gets the currency model based on given currency code. If blank, will return AUD.
     *
     * @param currCode
     *            the curr code
     * @return currency
     */
    public CurrencyModel getCurrencyByCode(final String currCode) {
        final CurrencyModel example = getCurrencyModelInstance();
        example.setIsocode(StringUtils.isNotBlank(currCode) ? currCode : TgtebayConstants.CURRENCY_CODE_AUD);

        return flexibleSearchService.getModelByExample(example);
    }

    protected CurrencyModel getCurrencyModelInstance() {
        return new CurrencyModel();
    }

    private CountryModel getCountryByCountryCode(final String countryCode) {
        final CountryModel country = new CountryModel();
        country.setIsocode(countryCode);
        return flexibleSearchService.getModelByExample(country);
    }

    private TitleModel getTitle(final String titleStr) {
        final TitleModel title = new TitleModel();
        title.setCode(titleStr);
        final List<TitleModel> list = flexibleSearchService
                .getModelsByExample(title);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    private String getNewEntryCode(final PaymentTransactionModel transaction) {
        final String stem = transaction.getCode();

        if (transaction.getEntries() == null) {
            return stem + "-1";
        }
        return stem + "-" + (transaction.getEntries().size() + 1);
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * @param unitService
     *            the unitService to set
     */
    @Required
    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    /**
     * @param paymentMethodStrategy
     *            the paymentMethodStrategy to set
     */
    @Required
    public void setPaymentMethodStrategy(final PaymentMethodStrategy paymentMethodStrategy) {
        this.paymentMethodStrategy = paymentMethodStrategy;
    }

    /**
     * @param targetPartnerOrderFieldValidator
     *            the targetPartnerOrderFieldValidator to set
     */
    @Required
    public void setTargetPartnerOrderFieldValidator(
            final TargetPartnerOrderFieldValidator targetPartnerOrderFieldValidator) {
        this.targetPartnerOrderFieldValidator = targetPartnerOrderFieldValidator;
    }




}
