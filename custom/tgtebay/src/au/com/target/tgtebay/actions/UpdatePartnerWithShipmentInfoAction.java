/**
 * 
 */
package au.com.target.tgtebay.actions;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtebay.logger.PartnerStatusUpdateLogger;
import au.com.target.tgtwebmethods.ca.TargetCaShippingService;


/**
 * Action to update partner with shipment information for only delivery orders from respective partner.
 * 
 * @author jjayawa1
 * 
 */
public class UpdatePartnerWithShipmentInfoAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final PartnerStatusUpdateLogger LOG = new PartnerStatusUpdateLogger(
            UpdatePartnerWithShipmentInfoAction.class);

    private TargetCaShippingService targetCaShippingService;

    private TargetSalesApplicationConfigService targetSalesApplicationConfigService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    public enum Transition {
        OK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Business process can't be null.");
        final OrderModel orderModel = process.getOrder();


        Assert.notNull(orderModel, "Order can't be null.");

        if (null != orderModel.getDeliveryMode()
                && orderModel.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {

            final TargetZoneDeliveryModeModel deliveryMode = (TargetZoneDeliveryModeModel)orderModel.getDeliveryMode();
            final SalesApplication salesApp = orderModel.getSalesApplication();
            final String hybrisOrderNumber = orderModel.getCode();
            final String partnerOrderNumber = ObjectUtils.toString(orderModel.getEBayOrderNumber());

            if (isShippingInfoApplicableForSalesAppAndDeliveryMode(salesApp, deliveryMode)) {
                LOG.logOrderStatusUpdateEntry(PartnerStatusUpdateLogger.LOG_SHIPPED, salesApp, hybrisOrderNumber,
                        partnerOrderNumber);
                if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                    final ConsignmentModel consignment = this.getOrderProcessParameterHelper().getConsignment(process);
                    submitDeliveryOrderStatusForCompleteUpdateToFluent(consignment, salesApp, hybrisOrderNumber,
                            partnerOrderNumber);
                }
                else {
                    submitDeliveryOrderStatusForCompleteUpdate(orderModel, salesApp, hybrisOrderNumber,
                            partnerOrderNumber);
                }
            }
            else {
                LOG.logOrderStatusUpdateSkip(PartnerStatusUpdateLogger.LOG_SHIPPED, salesApp, hybrisOrderNumber,
                        partnerOrderNumber);
            }
        }

        return Transition.OK.toString();

    }

    /**
     * 
     * @param consignment
     * @param salesApp
     * @param hybrisOrderNumber
     * @param partnerOrderNumber
     * @throws RetryLaterException
     */
    private void submitDeliveryOrderStatusForCompleteUpdateToFluent(final ConsignmentModel consignment,
            final SalesApplication salesApp, final String hybrisOrderNumber, final String partnerOrderNumber)
            throws RetryLaterException {
        if (targetCaShippingService.submitDeliveryConsignmentStatus(consignment)) {
            LOG.logOrderStatusUpdateSuccess(PartnerStatusUpdateLogger.LOG_SHIPPED, salesApp,
                    hybrisOrderNumber,
                    partnerOrderNumber);
        }
        else {
            LOG.logOrderStatusUpdateFailure(PartnerStatusUpdateLogger.LOG_SHIPPED, salesApp,
                    hybrisOrderNumber,
                    partnerOrderNumber);
            throw new RetryLaterException();
        }
    }

    /**
     * 
     * @param orderModel
     * @param salesApp
     * @param hybrisOrderNumber
     * @param partnerOrderNumber
     * @throws RetryLaterException
     */
    private void submitDeliveryOrderStatusForCompleteUpdate(final OrderModel orderModel,
            final SalesApplication salesApp, final String hybrisOrderNumber, final String partnerOrderNumber)
            throws RetryLaterException {
        //featureSwitch is off
        if (targetCaShippingService.submitDeliveryOrderStatusForCompleteUpdate(orderModel)) {
            LOG.logOrderStatusUpdateSuccess(PartnerStatusUpdateLogger.LOG_SHIPPED, salesApp,
                    hybrisOrderNumber,
                    partnerOrderNumber);
        }
        else {
            LOG.logOrderStatusUpdateFailure(PartnerStatusUpdateLogger.LOG_SHIPPED, salesApp,
                    hybrisOrderNumber,
                    partnerOrderNumber);
            throw new RetryLaterException();
        }

    }

    /**
     * @param salesApp
     * @param deliveryMode
     * @return true, if sales application and delivery mode is applicable for sending shipping info
     */
    private boolean isShippingInfoApplicableForSalesAppAndDeliveryMode(final SalesApplication salesApp,
            final TargetZoneDeliveryModeModel deliveryMode) {
        return targetSalesApplicationConfigService.isPartnerChannel(salesApp)
                && BooleanUtils.isNotTrue(deliveryMode.getIsDeliveryToStore());
    }

    /**
     * Log retry exceeded information
     */
    @Override
    protected void logRetryExceeded(final OrderProcessModel process) {
        final OrderModel orderModel = process.getOrder();

        LOG.logRetryExceededError(PartnerStatusUpdateLogger.LOG_SHIPPED, orderModel.getSalesApplication(),
                orderModel.getCode(), ObjectUtils.toString(orderModel.getEBayOrderNumber()));
    }

    /**
     * @param targetCaShippingService
     *            the targetCaShippingService to set
     */
    @Required
    public void setTargetCaShippingService(final TargetCaShippingService targetCaShippingService) {
        this.targetCaShippingService = targetCaShippingService;
    }

    /**
     * @param targetSalesApplicationConfigService
     *            the targetSalesApplicationConfigService to set
     */
    @Required
    public void setTargetSalesApplicationConfigService(
            final TargetSalesApplicationConfigService targetSalesApplicationConfigService) {
        this.targetSalesApplicationConfigService = targetSalesApplicationConfigService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}
