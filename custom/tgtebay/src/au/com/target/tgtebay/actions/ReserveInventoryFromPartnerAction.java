package au.com.target.tgtebay.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtebay.order.TargetPartnerOrderService;


/**
 * @author Trinadh
 * 
 *         Check Stock and reserve for the order.
 */
public class ReserveInventoryFromPartnerAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(ReserveInventoryFromPartnerAction.class);

    private static final String INFO_MESSAGE_RESERVE_STOCK = "ReserveInventory: Reserving stock for partner order orderNumber={0}, salesApplication={1}";
    private static final String INFO_MESSAGE_SKIP_RESERVE_STOCK = "ReserveInventory: Skipping stock reservation for order orderNumber={0}, salesApplication={1}";

    private TargetPartnerOrderService targetPartnerOrderService;

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Override
    public void executeAction(final OrderProcessModel orderProcess) throws RetryLaterException,
            Exception {

        Assert.notNull(orderProcess, "OrderProcess cannot be null");
        final OrderModel order = orderProcess.getOrder();
        Assert.notNull(order, "Order cannot be null");

        if (salesApplicationConfigService.isReserveStockOnAcceptOrder(order.getSalesApplication())) {
            LOG.info(MessageFormat.format(INFO_MESSAGE_RESERVE_STOCK,
                    order.getCode(), order.getSalesApplication().getCode()));
            targetPartnerOrderService.reserveStockForPartnerOrder(order);
        }
        else {
            LOG.info(MessageFormat.format(INFO_MESSAGE_SKIP_RESERVE_STOCK,
                    order.getCode(), order.getSalesApplication().getCode()));
        }

    }

    /**
     * @param targetPartnerOrderService
     *            the targetPartnerOrderService to set
     */
    @Required
    public void setTargetPartnerOrderService(final TargetPartnerOrderService targetPartnerOrderService) {
        this.targetPartnerOrderService = targetPartnerOrderService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}