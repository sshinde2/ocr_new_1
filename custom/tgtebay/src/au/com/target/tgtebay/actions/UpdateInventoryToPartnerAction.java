package au.com.target.tgtebay.actions;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.stockupdate.TargetInventoryService;
import au.com.target.tgtebay.logger.PartnerInventoryUpdateLogger;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;


/**
 * @author Trinadh
 * 
 *         Inventory Update to Partner
 */
public class UpdateInventoryToPartnerAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final PartnerInventoryUpdateLogger LOG = new PartnerInventoryUpdateLogger(
            UpdateInventoryToPartnerAction.class);

    private TargetStockService targetStockService;

    private TargetInventoryService targetCaInventoryService;

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    private FluentStockLookupService fluentStockLookupService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    public enum Transition {
        OK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    public String executeInternal(final OrderProcessModel orderProcess) throws Exception {
        Assert.notNull(orderProcess, "OrderProcess cannot be null");
        final OrderModel order = orderProcess.getOrder();
        Assert.notNull(order, "Order cannot be null");
        final String orderCode = order.getCode();

        if (isInventoryUpdateRequired(order)) {
            final List<AbstractOrderEntryModel> entries = order.getEntries();

            if (CollectionUtils.isNotEmpty(entries)) {
                final Map<String, Integer> stockMap;

                if (targetFeatureSwitchService.isFeatureEnabled("fluentStockLookup")) {
                    stockMap = getStockMapUsingFluent(entries);
                }
                else {
                    stockMap = getStockMap(entries);
                }

                logInventoryUpdate(stockMap, orderCode);
            }
        }
        else {
            LOG.logInventoryUpdateSkip(orderCode, order.getSalesApplication());
        }

        return Transition.OK.toString();

    }

    /**
     * @param stockMap
     * @param orderCode
     * @throws RetryLaterException
     */
    protected void logInventoryUpdate(final Map<String, Integer> stockMap, final String orderCode)
            throws RetryLaterException {
        for (final Map.Entry<String, Integer> stockEntry : stockMap.entrySet()) {
            final String productCode = stockEntry.getKey();
            final int stockAmount = stockEntry.getValue().intValue();
            LOG.logInventoryUpdateEntry(orderCode, productCode, stockAmount);

            if (!targetCaInventoryService.updateInventoryItemQuantity(productCode, stockAmount)) {
                LOG.logInventoryUpdateFailure(orderCode, productCode, stockAmount);

                // throwing exception to process retries
                throw new RetryLaterException();
            }
            else {
                LOG.logInventoryUpdateSuccess(orderCode, productCode, stockAmount);
            }
        }
    }

    /**
     * @param entries
     * @return map of sku to stockLevel
     * @throws FluentClientException
     */
    protected Map<String, Integer> getStockMapUsingFluent(final List<AbstractOrderEntryModel> entries)
            throws FluentClientException {
        final List<String> skuCodes = new ArrayList<>();
        for (final AbstractOrderEntryModel entry : entries) {
            if (checkAvailableForPartner(entry.getProduct())) {
                skuCodes.add(entry.getProduct().getCode());
            }
        }
        return fluentStockLookupService.lookupStock(skuCodes);
    }

    protected Map<String, Integer> getStockMap(final List<AbstractOrderEntryModel> entries) {
        final Map<String, Integer> stockMap = new HashMap<>();
        for (final AbstractOrderEntryModel entry : entries) {
            final ProductModel productModel = entry.getProduct();

            if (checkAvailableForPartner(entry.getProduct())) {
                stockMap.put(productModel.getCode(), Integer.valueOf(targetStockService
                        .getStockLevelAmountFromOnlineWarehouses(productModel)));
            }
        }
        return stockMap;
    }

    private boolean checkAvailableForPartner(final ProductModel productModel) {
        if (null != productModel) {
            if (productModel instanceof AbstractTargetVariantProductModel) {
                final AbstractTargetVariantProductModel abstractProductModel = (AbstractTargetVariantProductModel)productModel;
                if (null != abstractProductModel.getAvailableOnEbay()
                        && abstractProductModel.getAvailableOnEbay().booleanValue()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isInventoryUpdateRequired(final OrderModel order) {
        final SalesApplicationConfigModel salesApplicationConfig = salesApplicationConfigService
                .getConfigForSalesApplication(order.getSalesApplication());
        return (null == salesApplicationConfig
                || CollectionUtils.isEmpty(salesApplicationConfig.getSkipPartnerInventoryUpdateForDelModes())
                || !salesApplicationConfig
                        .getSkipPartnerInventoryUpdateForDelModes().contains(order.getDeliveryMode()));
    }

    /**
     * Log retry exceeded information
     */
    @Override
    protected void logRetryExceeded(final OrderProcessModel process) {
        LOG.logRetryExceededError(process.getOrder().getCode());
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetCaInventoryService
     *            the targetCaInventoryService to set
     */
    @Required
    public void setTargetCaInventoryService(final TargetInventoryService targetCaInventoryService) {
        this.targetCaInventoryService = targetCaInventoryService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    @Required
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}