/**
 * 
 */
package au.com.target.tgtmarketing.data.dynamic;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.TargetLookModel;


/**
 * @author mgazal
 *
 */
public class TargetLookImage implements DynamicAttributeHandler<MediaModel, TargetLookModel> {

    private String imageFormatQualifier;

    private boolean returnDefault;

    @Override
    public MediaModel get(final TargetLookModel look) {
        if (look.getImages() != null && CollectionUtils.isNotEmpty(look.getImages().getMedias())) {
            for (final MediaModel media : look.getImages().getMedias()) {
                if (media.getMediaFormat() != null
                        && imageFormatQualifier.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    return media;
                }
            }
            if (returnDefault) {
                return look.getImages().getMedias().iterator().next();
            }
        }

        return null;
    }

    @Override
    public void set(final TargetLookModel look, final MediaModel media) {
        throw new UnsupportedOperationException();
    }

    /**
     * @param imageFormatQualifier
     *            the imageFormatQualifier to set
     */
    @Required
    public void setImageFormatQualifier(final String imageFormatQualifier) {
        this.imageFormatQualifier = imageFormatQualifier;
    }

    /**
     * @param returnDefault
     *            the returnDefault to set
     */
    public void setReturnDefault(final boolean returnDefault) {
        this.returnDefault = returnDefault;
    }
}
