/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;


/**
 * @author pthoma20
 * 
 */
public class TgtMarketingQuantityBreakDealModelValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof QuantityBreakDealModel)) {
            return;
        }
        final QuantityBreakDealModel quantityBreakDeal = (QuantityBreakDealModel)model;
        if (quantityBreakDeal.getAvailableForMobile().booleanValue()) {
            throw new InterceptorException(ErrorMessages.ERROR_QTY_BREAK_DEAL_AVAILABLE_MOBILE);
        }
    }

    protected interface ErrorMessages {
        public static final String ERROR_QTY_BREAK_DEAL_AVAILABLE_MOBILE = "Error enabling Available For Mobile -"
                + " Qty Break Deal Cannot be Enabled for Mobile";
    }
}
