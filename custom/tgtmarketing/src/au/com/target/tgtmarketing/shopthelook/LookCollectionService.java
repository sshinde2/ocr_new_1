/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;


/**
 * Service to manage {@link TargetLookCollectionModel}
 * 
 * @author mgazal
 *
 */
public interface LookCollectionService {

    /**
     * Get the look collection for a given code.
     * 
     * @param code
     * @return {@link TargetLookCollectionModel}
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    TargetLookCollectionModel getLookCollectionForCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;
}
