/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookCollectionDao;


/**
 * 
 * @author mgazal
 *
 */
public class TargetLookCollectionDaoImpl extends DefaultGenericDao<TargetLookCollectionModel>
        implements LookCollectionDao {

    public TargetLookCollectionDaoImpl() {
        super(TargetLookCollectionModel._TYPECODE);
    }

    @Override
    public TargetLookCollectionModel getLookCollectionForCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        ServicesUtil.validateParameterNotNullStandardMessage("code", code);

        final Map<String, String> params = new HashMap<>();
        params.put(TargetLookCollectionModel.ID, code);
        final List<TargetLookCollectionModel> lookCollectionModels = find(params);

        TargetServicesUtil.validateIfSingleResult(lookCollectionModels, TargetLookCollectionModel.class,
                TargetLookCollectionModel.ID, code);

        return lookCollectionModels.get(0);
    }

}
