/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookProductDao;


/**
 * 
 * @author mgazal
 *
 */
public class TargetLookProductDaoImpl extends DefaultGenericDao<TargetLookProductModel> implements LookProductDao {

    public TargetLookProductDaoImpl() {
        super(TargetLookProductModel._TYPECODE);
    }

    @Override
    public TargetLookProductModel getLookProductForCodeAndLook(final String productCode, final TargetLookModel look)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        ServicesUtil.validateParameterNotNullStandardMessage("code", productCode);
        ServicesUtil.validateParameterNotNullStandardMessage("look", look);

        final Map<String, Object> params = new HashMap<>();
        params.put(TargetLookProductModel.PRODUCTCODE, productCode);
        params.put(TargetLookProductModel.LOOK, look);
        final List<TargetLookProductModel> lookProductModels = find(params);

        TargetServicesUtil.validateIfSingleResult(lookProductModels, TargetLookProductModel.class,
                TargetLookProductModel.PRODUCTCODE + "-" + TargetLookProductModel.LOOK,
                productCode + "-" + look);

        return lookProductModels.get(0);
    }

}
