/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * Service to manage {@link TargetShopTheLookModel}
 * 
 * @author mgazal
 *
 */
public interface ShopTheLookService {

    /**
     * Get shop the look for a given code.
     * 
     * @param code
     * @return {@link TargetShopTheLookModel}
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    TargetShopTheLookModel getShopTheLookForCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

    /**
     * Get list of look model by shopTheLookModel id
     * 
     * @param id
     * @return List<TargetLookModel>
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    List<TargetLookModel> getListOfLooksForShopTheLookUsingCode(final String id)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, IllegalArgumentException;

    /**
     * @param code
     * @return TargetShopTheLookModel
     */
    TargetShopTheLookModel getShopTheLookByCategoryCode(String code);
}
