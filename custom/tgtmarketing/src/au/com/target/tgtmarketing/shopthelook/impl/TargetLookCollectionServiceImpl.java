/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.shopthelook.LookCollectionService;
import au.com.target.tgtmarketing.shopthelook.dao.LookCollectionDao;


/**
 * @author mgazal
 *
 */
public class TargetLookCollectionServiceImpl extends AbstractBusinessService implements LookCollectionService {

    private LookCollectionDao lookCollectionDao;

    @Override
    public TargetLookCollectionModel getLookCollectionForCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);
        return lookCollectionDao.getLookCollectionForCode(code);
    }

    /**
     * @param lookCollectionDao
     *            the lookCollectionDao to set
     */
    @Required
    public void setLookCollectionDao(final LookCollectionDao lookCollectionDao) {
        this.lookCollectionDao = lookCollectionDao;
    }

}
