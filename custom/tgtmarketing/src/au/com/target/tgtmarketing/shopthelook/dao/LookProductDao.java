package au.com.target.tgtmarketing.shopthelook.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;


/**
 * @author mgazal
 */
public interface LookProductDao {

    /**
     * Get the look product for a given code and look.
     * 
     * @param productCode
     * @param look
     * @return {@link TargetLookProductModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetLookProductModel getLookProductForCodeAndLook(String productCode, TargetLookModel look)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}
