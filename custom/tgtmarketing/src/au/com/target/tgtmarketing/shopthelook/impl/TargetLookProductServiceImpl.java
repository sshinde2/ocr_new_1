/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.shopthelook.LookProductService;
import au.com.target.tgtmarketing.shopthelook.dao.LookProductDao;


/**
 * @author mgazal
 *
 */
public class TargetLookProductServiceImpl extends AbstractBusinessService implements LookProductService {

    private LookProductDao lookProductDao;

    @Override
    public TargetLookProductModel getLookProductForCodeAndLook(final String productCode, final TargetLookModel look)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("productCode", productCode);
        ServicesUtil.validateParameterNotNullStandardMessage("look", look);
        return lookProductDao.getLookProductForCodeAndLook(productCode, look);
    }

    /**
     * @param lookProductDao
     *            the lookProductDao to set
     */
    @Required
    public void setLookProductDao(final LookProductDao lookProductDao) {
        this.lookProductDao = lookProductDao;
    }

}
