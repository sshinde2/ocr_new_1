package au.com.target.tgtmarketing.voucher.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtmarketing.voucher.dao.TargetMobileVoucherDao;


/**
 * Default implementation of {@link TargetMobileVoucherDao}
 * 
 */
public class TargetMobileVoucherDaoImpl extends DefaultGenericDao<PromotionVoucherModel> implements
        TargetMobileVoucherDao {


    public TargetMobileVoucherDaoImpl() {
        super(PromotionVoucherModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtmarketing.voucher.dao.TargetMobileVouchersDao#getAllActiveMobileVouchers()
     */
    @Override
    public List<PromotionVoucherModel> getAllActiveMobileVouchers() {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(PromotionVoucherModel.AVAILABLEFORMOBILE, Boolean.TRUE);
        final List<PromotionVoucherModel> promotionVoucherModel = find(params);
        return promotionVoucherModel;
    }
}
