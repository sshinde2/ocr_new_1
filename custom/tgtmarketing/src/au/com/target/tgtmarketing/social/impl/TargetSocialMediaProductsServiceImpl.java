/**
 * 
 */
package au.com.target.tgtmarketing.social.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.SocialMediaProductsModel;
import au.com.target.tgtmarketing.social.TargetSocialMediaProductsService;
import au.com.target.tgtmarketing.social.dao.TargetSocialMediaProductsDao;


/**
 * @author rmcalave
 *
 */
public class TargetSocialMediaProductsServiceImpl extends AbstractBusinessService
        implements TargetSocialMediaProductsService {

    private TargetSocialMediaProductsDao targetSocialMediaProductsDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.marketing.TargetSocialMediaProductsService#getSocialMediaProductsByCreationTimeDescending(int)
     */
    @Override
    public List<SocialMediaProductsModel> getSocialMediaProductsByCreationTimeDescending(final int count) {
        Validate.isTrue(count > 0, "count must be greater than zero");

        return getTargetSocialMediaProductsDao().findSocialMediaProductsByCreationTimeDescending(count);
    }

    /**
     * @return the targetSocialMediaProductsDao
     */
    protected TargetSocialMediaProductsDao getTargetSocialMediaProductsDao() {
        return targetSocialMediaProductsDao;
    }

    /**
     * @param targetSocialMediaProductsDao
     *            the targetSocialMediaProductsDao to set
     */
    @Required
    public void setTargetSocialMediaProductsDao(final TargetSocialMediaProductsDao targetSocialMediaProductsDao) {
        this.targetSocialMediaProductsDao = targetSocialMediaProductsDao;
    }

}
