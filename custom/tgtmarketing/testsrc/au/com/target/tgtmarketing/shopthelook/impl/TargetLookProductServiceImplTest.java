/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.shopthelook.LookProductService;
import au.com.target.tgtmarketing.shopthelook.dao.LookProductDao;
import junit.framework.Assert;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookProductServiceImplTest {

    private static final String CODE = "LP001";

    private static final TargetLookModel LOOK = new TargetLookModel();

    @Mock
    private LookProductDao lookProductDao;

    @InjectMocks
    private final LookProductService lookProductService = new TargetLookProductServiceImpl();

    @Test
    public void testGetLookProduct() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(lookProductDao.getLookProductForCodeAndLook(CODE, LOOK))
                .willReturn(new TargetLookProductModel());

        final TargetLookProductModel lookProduct = lookProductService.getLookProductForCodeAndLook(CODE, LOOK);
        Assert.assertNotNull(lookProduct);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetLookProductNullCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookProductService.getLookProductForCodeAndLook(null, LOOK);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetLookProductNullLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookProductService.getLookProductForCodeAndLook(CODE, null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetLookMissing()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(lookProductDao.getLookProductForCodeAndLook(CODE, LOOK))
                .willThrow(new TargetUnknownIdentifierException(CODE + "-" + LOOK.getId()));
        lookProductService.getLookProductForCodeAndLook(CODE, LOOK);
    }

}
