/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.shopthelook.dao.LookDao;
import junit.framework.Assert;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookServiceImplTest {

    private static final String CODE = "L001";

    @Mock
    private LookDao lookDao;

    @InjectMocks
    private final LookService lookService = new TargetLookServiceImpl();

    @Test
    public void testGetLook() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        BDDMockito.given(lookDao.getLookForCode(CODE))
                .willReturn(new TargetLookModel());

        final TargetLookModel look = lookService.getLookForCode(CODE);
        Assert.assertNotNull(look);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetLookNullCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookService.getLookForCode(null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetLookMissing()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        BDDMockito.given(lookDao.getLookForCode(CODE))
                .willThrow(new TargetUnknownIdentifierException(CODE));
        lookService.getLookForCode(CODE);
    }

    @Test
    public void testGetLookForCollection() {
        final List<TargetLookModel> looks = Mockito.mock(List.class);
        BDDMockito.given(lookDao.getVisibleLooksForCollection(CODE)).willReturn(looks);
        final List<TargetLookModel> looksForCollection = lookService.getVisibleLooksForCollection(CODE);
        Assert.assertEquals(looks, looksForCollection);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetLookForCollectionNullCode() {
        lookService.getVisibleLooksForCollection(null);
    }

    @Test
    public void testGetVisibleLooksForProductCodes() {
        final List<TargetLookModel> looks = Mockito.mock(List.class);
        final ArgumentCaptor<Integer> limit = ArgumentCaptor.forClass(Integer.class);
        BDDMockito.given(lookDao.getVisibleLooksForProductCodes(Mockito.anyCollection(), limit.capture().intValue()))
                .willReturn(looks);

        List<TargetLookModel> looksForProductCodes = lookService
                .getVisibleLooksForProductCodes(Collections.EMPTY_LIST);
        Assert.assertEquals(looks, looksForProductCodes);
        Assert.assertEquals(-1, limit.getValue().intValue());

        looksForProductCodes = lookService
                .getVisibleLooksForProductCodes(Collections.EMPTY_LIST, 2);
        Assert.assertEquals(looks, looksForProductCodes);
        Assert.assertEquals(2, limit.getValue().intValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetVisibleLooksForProductCodesMissing() {
        lookService.getVisibleLooksForProductCodes(null);
    }

    @Test
    public void testGetVisibleLooksForProduct() {
        final List<TargetLookModel> looks = Mockito.mock(List.class);
        final ArgumentCaptor<Collection> productCodes = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Integer> limit = ArgumentCaptor.forClass(Integer.class);
        BDDMockito.given(lookDao.getVisibleLooksForProductCodes(productCodes.capture(), limit.capture().intValue()))
                .willReturn(looks);

        final ProductModel productModel = Mockito.mock(ProductModel.class);
        final List<VariantProductModel> variants = new ArrayList<>();
        final TargetColourVariantProductModel colourVariantProduct = Mockito
                .mock(TargetColourVariantProductModel.class);
        Mockito.when(colourVariantProduct.getCode()).thenReturn("CV01");
        variants.add(colourVariantProduct);
        final TargetSizeVariantProductModel sizeVariantProduct = Mockito.mock(TargetSizeVariantProductModel.class);
        Mockito.when(sizeVariantProduct.getCode()).thenReturn("SV01");
        variants.add(sizeVariantProduct);

        BDDMockito.given(productModel.getVariants()).willReturn(variants);
        List<TargetLookModel> looksForProduct = lookService.getVisibleLooksForProduct(productModel);
        Assert.assertEquals(1, productCodes.getValue().size());
        Assert.assertEquals(-1, limit.getValue().intValue());
        Assert.assertEquals("CV01", productCodes.getValue().iterator().next());
        Assert.assertEquals(looks, looksForProduct);

        looksForProduct = lookService.getVisibleLooksForProduct(productModel, 2);
        Assert.assertEquals(1, productCodes.getValue().size());
        Assert.assertEquals(2, limit.getValue().intValue());
        Assert.assertEquals("CV01", productCodes.getValue().iterator().next());
        Assert.assertEquals(looks, looksForProduct);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetVisibleLooksForProductMissing() {
        lookService.getVisibleLooksForProduct(null);
    }

    @Test
    public void testGetVisibleLooksForProductEmpty() {
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        final List<TargetLookModel> looksForProduct = lookService.getVisibleLooksForProduct(productModel);
        Assert.assertNotNull(looksForProduct);
        Assert.assertEquals(0, looksForProduct.size());
        Mockito.verify(lookDao, Mockito.times(0)).getVisibleLooksForProductCodes(Mockito.anyCollection(),
                Mockito.anyInt());
    }

    @Test
    public void testGetAllVisibleLooksWithInstagramUrl() {
        final List<TargetLookModel> looks = Mockito.mock(List.class);
        final ArgumentCaptor<Integer> limit = ArgumentCaptor.forClass(Integer.class);
        BDDMockito.given(lookDao.getAllVisibleLooksWithInstagramUrl(limit.capture().intValue()))
                .willReturn(looks);

        final List<TargetLookModel> looksForProductCodes = lookService.getAllVisibleLooksWithInstagramUrl();
        Assert.assertEquals(looks, looksForProductCodes);
        Assert.assertEquals(-1, limit.getValue().intValue());
    }

    @Test
    public void testGetAllVisibleLooksWithInstagramUrlWithLimit() {
        final List<TargetLookModel> looks = Mockito.mock(List.class);
        final ArgumentCaptor<Integer> limit = ArgumentCaptor.forClass(Integer.class);
        BDDMockito.given(lookDao.getAllVisibleLooksWithInstagramUrl(limit.capture().intValue()))
                .willReturn(looks);

        final List<TargetLookModel> looksForProductCodes = lookService.getAllVisibleLooksWithInstagramUrl(2);
        Assert.assertEquals(looks, looksForProductCodes);
        Assert.assertEquals(2, limit.getValue().intValue());
    }
}
