/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;
import au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao;
import junit.framework.Assert;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetShopTheLookServiceImplTest {

    private static final String CODE = "STL01";

    @Mock
    private ShopTheLookDao shopTheLookDao;

    @InjectMocks
    private final ShopTheLookService shopTheLookService = new TargetShopTheLookServiceImpl();

    @Mock
    private SearchRestrictionService searchRestrictionService;

    @Mock
    private SessionService sessionService;

    @Test
    public void testGetLookCollection() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(shopTheLookDao.getShopTheLookForCode(CODE))
                .willReturn(new TargetShopTheLookModel());


        final TargetShopTheLookModel shopTheLook = shopTheLookService.getShopTheLookForCode(CODE);
        Assert.assertNotNull(shopTheLook);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetLookCollectionNullCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        shopTheLookService.getShopTheLookForCode(null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetLookCollectionMissing()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(shopTheLookDao.getShopTheLookForCode(CODE))
                .willThrow(new TargetUnknownIdentifierException(CODE));
        shopTheLookService.getShopTheLookForCode(CODE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetListOfLooksForShopTheLookUsingCodeWithMissingArgument()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        shopTheLookService.getListOfLooksForShopTheLookUsingCode(null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetListOfLooksForShopTheLookUsingCodeWithUnknownStlid()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(shopTheLookDao.getListOfLooksForShopTheLookUsingCode(CODE))
                .willThrow(new TargetUnknownIdentifierException(CODE));
        shopTheLookService.getListOfLooksForShopTheLookUsingCode(CODE);
    }

    @Test(expected = TargetAmbiguousIdentifierException.class)
    public void testGetListOfLooksForShopTheLookUsingCodeWithAmbiguousStlid()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(shopTheLookDao.getListOfLooksForShopTheLookUsingCode(CODE))
                .willThrow(new TargetAmbiguousIdentifierException(CODE));
        shopTheLookService.getListOfLooksForShopTheLookUsingCode(CODE);
    }

    @Test
    public void testGetListOfLooksForShopTheLookUsingCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final List<TargetLookModel> lookModels = Arrays.asList(Mockito.mock(TargetLookModel.class));
        given(shopTheLookDao.getListOfLooksForShopTheLookUsingCode(CODE))
                .willReturn(lookModels);
        final List<TargetLookModel> result = shopTheLookService.getListOfLooksForShopTheLookUsingCode(CODE);
        Assert.assertEquals(lookModels, result);
    }

    @Test
    public void testGetShopTheLookByCategoryCodeWithNoModelFound() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String categoryCode = "W123454";
        final TargetShopTheLookModel shopTheLook = mock(TargetShopTheLookModel.class);
        final SessionExecutionBody sessionExecutionBody = mock(SessionExecutionBody.class);
        given(sessionExecutionBody.execute()).willReturn(shopTheLook);
        when(sessionService.executeInLocalView(sessionExecutionBody)).thenReturn(sessionService);
        when(shopTheLookDao.getShopTheLookByCategoryCode(categoryCode)).thenThrow(
                new TargetUnknownIdentifierException("TargetUnknownIdentifierException"));
        willReturn(Boolean.FALSE).given(searchRestrictionService).isSearchRestrictionsEnabled();
        final TargetShopTheLookModel result = shopTheLookService.getShopTheLookByCategoryCode(categoryCode);
        assertThat(result).isNull();
    }

    @Test
    public void testGetShopTheLookByCategoryCodeWithAmbiguousResult() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String categoryCode = "testCode";

        final TargetShopTheLookModel shopTheLook = mock(TargetShopTheLookModel.class);
        final SessionExecutionBody sessionExecutionBody = mock(SessionExecutionBody.class);
        given(sessionExecutionBody.execute()).willReturn(shopTheLook);
        when(sessionService.executeInLocalView(sessionExecutionBody)).thenReturn(sessionService);
        when(shopTheLookDao.getShopTheLookByCategoryCode(categoryCode)).thenThrow(
                new TargetAmbiguousIdentifierException("TargetAmbiguousIdentifierException"));
        willReturn(Boolean.FALSE).given(searchRestrictionService).isSearchRestrictionsEnabled();
        final TargetShopTheLookModel result = shopTheLookService.getShopTheLookByCategoryCode(categoryCode);
        assertThat(result).isNull();
    }

}
