package au.com.target.tgtmarketing.deals.dao.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.deals.dao.impl.DealsTestHelper;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * 
 */
public final class MobileDealsTestHelper {

    private static ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    private MobileDealsTestHelper() {
        // never instantiate
    }

    public static AbstractDealModel createValueBundleDealWithAvailableForMobileAttribute(final String dealId,
            final String eventId,
            final double rewardValue, final Date endDate, final Boolean availableForMobile,
            final String longTitle,
            final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingList, final String immutableKeyHash) {
        final AbstractDealModel abstractDealModel = DealsTestHelper.createValueBundleDeal(
                dealId, eventId, rewardValue, endDate);
        return enrichAvailableForMobileAttributes(abstractDealModel, availableForMobile, longTitle,
                targetMobileOfferHeadingList, immutableKeyHash);
    }

    public static AbstractDealModel enrichAvailableForMobileAttributes(final AbstractDealModel abstractDealModel,
            final Boolean availableForMobile, final String longTitle,
            final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingList, final String immutableKeyHash) {
        abstractDealModel.setAvailableForMobile(availableForMobile);
        abstractDealModel.setLongTitle(longTitle);
        abstractDealModel.setTargetMobileOfferHeadings(targetMobileOfferHeadingList);
        abstractDealModel.setImmutableKeyHash(immutableKeyHash);
        modelService.save(abstractDealModel);
        return abstractDealModel;
    }
}
