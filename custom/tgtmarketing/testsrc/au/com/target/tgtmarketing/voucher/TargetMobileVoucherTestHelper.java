package au.com.target.tgtmarketing.voucher;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import au.com.target.tgtcore.model.TargetDateRestrictionModel;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;



/**
 * 
 */
public final class TargetMobileVoucherTestHelper {

    private static ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    private TargetMobileVoucherTestHelper() {
        // never instantiate
    }

    /**
     * This method will create an active promotional voucher for the following parameters
     * 
     * @param code
     * @param enabledOnline
     * @param voucherCode
     * @param barcode
     * @param enabledInStore
     * @param availableForMobile
     * @param targetMobileOfferHeadingModelList
     * @return promotionVoucherModel
     */
    public static PromotionVoucherModel setupActivePromotionVoucherModel(final String code,
            final Boolean enabledOnline, final String voucherCode, final Long barcode, final Boolean enabledInStore,
            final Boolean availableForMobile,
            final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList) {
        final PromotionVoucherModel promotionVoucherModel = modelService
                .create(PromotionVoucherModel.class);
        promotionVoucherModel.setCode(code);
        promotionVoucherModel.setName(code + "NAME");
        promotionVoucherModel.setBarcode(barcode);
        promotionVoucherModel.setVoucherCode(voucherCode);
        promotionVoucherModel.setEnabledOnline(enabledOnline);
        promotionVoucherModel.setEnabledInstore(enabledInStore);
        promotionVoucherModel.setFreeShipping(Boolean.FALSE);
        promotionVoucherModel.setValue(Double.valueOf(2));
        promotionVoucherModel.setRedemptionQuantityLimit(Integer.valueOf(1000));
        promotionVoucherModel.setRedemptionQuantityLimitPerUser(Integer.valueOf(1000));
        promotionVoucherModel.setTargetMobileOfferHeadings(targetMobileOfferHeadingModelList);

        final TargetDateRestrictionModel targetDateRestrictionModel = modelService
                .create(TargetDateRestrictionModel.class);
        targetDateRestrictionModel.setVoucher(promotionVoucherModel);
        targetDateRestrictionModel.setStartDate(new DateTime().minus(10).toDate());
        targetDateRestrictionModel.setEndDate(new DateTime().plus(10).toDate());
        modelService.saveAll(promotionVoucherModel, targetDateRestrictionModel);
        modelService.refresh(promotionVoucherModel);
        promotionVoucherModel.setAvailableForMobile(availableForMobile);
        modelService.save(promotionVoucherModel);
        return promotionVoucherModel;
    }

    /**
     * This method takes in a list of promotionVoucherModel and removes them.
     * 
     * @param promotionVoucherModelList
     */
    public static void removePromotionVoucherModel(
            final List<PromotionVoucherModel> promotionVoucherModelList) {
        if (CollectionUtils.isEmpty(promotionVoucherModelList)) {
            return;
        }
        for (final PromotionVoucherModel promotionVoucherModel : promotionVoucherModelList) {
            modelService.remove(promotionVoucherModel);
        }
    }
}
