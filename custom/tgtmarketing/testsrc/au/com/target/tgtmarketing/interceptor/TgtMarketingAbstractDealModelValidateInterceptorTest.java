/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Arrays;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtmarketing.interceptor.TgtMarketingAbstractDealModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import junit.framework.Assert;


/**
 * @author paul
 *
 */
@UnitTest
public class TgtMarketingAbstractDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";


    private final TgtMarketingAbstractDealModelValidateInterceptor abstractDealModelValidateInterceptor = new TgtMarketingAbstractDealModelValidateInterceptor();


    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        abstractDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        abstractDealModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }


    @Test
    public void testOnValidateMobileAttributeWithoutMobileOfferHeading() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAbstractDealModel.getLongTitle()).willReturn("Long Title");
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_DEAL_AVAILABLE_MOBILE
                            + ErrorMessages.ASSOCIATED_MOBILE_OFFER_HEADING_REQUIRED);
        }
    }

    @Test
    public void testOnValidateMobileAttributeWithDuplicateMobileOfferHeading() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAbstractDealModel.getLongTitle()).willReturn("Long Title");
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel1.getCode()).willReturn("entertainment");

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel2.getCode()).willReturn("entertainment");

        BDDMockito.given(mockAbstractDealModel.getTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(targetMobileOfferHeadingModel1,
                        targetMobileOfferHeadingModel2)));
        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_DEAL_AVAILABLE_MOBILE
                            + ErrorMessages.DUPLICATE_MOBILE_OFFER_HEADING);
        }
    }

    @Test
    public void testOnValidateSetMobileAttributeSuccessful() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAbstractDealModel.getLongTitle()).willReturn("Long Title");
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(mockAbstractDealModel.getTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(targetMobileOfferHeadingModel)));
        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }

    @Test
    public void testOnValidateMobileAttributeUnsetWithoutBoldAndLongTitle() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getAvailableForMobile()).willReturn(Boolean.FALSE);
        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }

}
