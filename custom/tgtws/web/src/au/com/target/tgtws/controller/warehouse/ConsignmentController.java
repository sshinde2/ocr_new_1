/**
 * 
 */
package au.com.target.tgtws.controller.warehouse;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtwsfacades.consignment.ConsignmentIntegrationFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationConsignmentDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author pthoma20
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/consignments")
public class ConsignmentController {

    private static final Logger LOG = Logger.getLogger(ConsignmentController.class);

    private static final String CONSIGNMENT = "CONSIGNMENT";

    @Autowired
    private ConsignmentIntegrationFacade consignmentIntegrationFacade;

    /**
     * Marks all the consignments to be cancelled.
     * 
     * @param dto
     * @return List of IntegrationResponses to the ones that have failed
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/cancel", method = RequestMethod.PUT)
    @ResponseBody
    public List<IntegrationResponseDto> cancelConsignments(@RequestBody final IntegrationConsignmentDto dto) {
        JaxbLogger.logXml(LOG, dto);
        if (!isConsignmentsValid(dto)) {
            return createInvalidInputResponse(CONSIGNMENT + "-CANCEL - Invalid inputs- No consignments present");
        }
        LOG.info(MessageFormat.format(CONSIGNMENT + "-CANCEL Cancelling {0} number of consigments",
                Integer.valueOf(dto.getConsignments().getConsignments().size())));
        return consignmentIntegrationFacade.cancelConsignments(dto.getConsignments());
    }


    /**
     * Marks all the consignments to be complete.
     * 
     * @param dto
     * @return List of IntegrationResponses to the ones that have failed
     */
    @Secured("ROLE_ESBGROUPONLINE")
    @RequestMapping(value = "/complete", method = RequestMethod.PUT)
    @ResponseBody
    public List<IntegrationResponseDto> completeConsignments(@RequestBody final IntegrationConsignmentDto dto) {
        JaxbLogger.logXml(LOG, dto);
        if (!isConsignmentsValid(dto)) {
            return createInvalidInputResponse(CONSIGNMENT + "-COMPLETE - Invalid inputs- No consignments present");
        }
        LOG.info(MessageFormat.format(CONSIGNMENT + "-COMPLETE - Completing {0} number of consigments",
                Integer.valueOf(dto.getConsignments().getConsignments().size())));
        return consignmentIntegrationFacade.completeConsignments(dto.getConsignments());

    }

    /**
     * Marks all the consignments to be shipped.
     *
     * @param dto
     * @return List of IntegrationResponses to the ones that have failed
     */
    @Secured("ROLE_ESBGROUPONLINE")
    @RequestMapping(value = "/ship", method = RequestMethod.PUT)
    @ResponseBody
    public List<IntegrationResponseDto> shipConsignments(@RequestBody
    final IntegrationConsignmentDto dto) {
        JaxbLogger.logXml(LOG, dto);
        if (!isConsignmentsValid(dto)) {
            return createInvalidInputResponse(CONSIGNMENT + "-SHIP - Invalid inputs- No consignments present");
        }
        LOG.info(MessageFormat.format(CONSIGNMENT + "- SHIP - Completing {0} number of consigments",
                Integer.valueOf(dto.getConsignments().getConsignments().size())));
        return consignmentIntegrationFacade.shipConsignments(dto.getConsignments());

    }

    private boolean isConsignmentsValid(final IntegrationConsignmentDto dto) {
        if (null != dto.getConsignments()
                && CollectionUtils.isNotEmpty(dto.getConsignments().getConsignments())) {
            return true;
        }
        return false;
    }

    private List<IntegrationResponseDto> createInvalidInputResponse(final String message) {
        final List<IntegrationResponseDto> responses = new ArrayList<>();
        final IntegrationResponseDto response = new IntegrationResponseDto(null);
        response.setSuccessStatus(false);
        LOG.error(message);
        response.addMessage(message);
        responses.add(response);
        return responses;
    }
}
