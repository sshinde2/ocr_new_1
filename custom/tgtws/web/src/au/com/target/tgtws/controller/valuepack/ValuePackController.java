/**
 * 
 */
package au.com.target.tgtws.controller.valuepack;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackDto;
import au.com.target.tgtwsfacades.valuepack.ValuePackIntegrationFacade;


/**
 * @author mmaki
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/valuepackapi")
public class ValuePackController extends BaseController {

    private static final Logger LOG = Logger.getLogger(ValuePackController.class);

    @Autowired
    private ValuePackIntegrationFacade valuePackIntegrationFacade;

    /**
     * Inserts or updates the value pack
     * 
     * @param dto
     * @return IntegrationResponseDto entity with result of the update operation
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetvaluepack", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto updateValuePack(@RequestBody final IntegrationValuePackDto dto) {

        LOG.info("INFO-VALUEPACK-UPDATE Updating value pack for leadSKU: " + dto.getLeadSKU());

        final IntegrationResponseDto response = valuePackIntegrationFacade.updateValuePack(dto);

        return response;
    }

}
