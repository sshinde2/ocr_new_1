/**
 * 
 */
package au.com.target.tgtws.controller.segments;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentImportResponseDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoRequestDto;
import au.com.target.tgtwsfacades.segments.CustomerSegmentImportFacade;


/**
 * @author pthoma20
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/customer/segments")
public class CustomerSegmentImportController extends BaseController {

    private static final Logger LOG = Logger.getLogger(CustomerSegmentImportController.class);

    private static final String SEGMENT_IMPORT_INFO = "SEG-IMPORT-INFO :";

    @Autowired
    private CustomerSegmentImportFacade customerSegmentImportFacade;

    @RequestMapping(value = "/import", method = RequestMethod.PUT)
    @ResponseBody
    public CustomerSegmentImportResponseDto importCustomerSegments(
            @RequestBody final CustomerSegmentInfoRequestDto customerSegmentInfoRequestDto) {
        CustomerSegmentImportResponseDto responseDto = new CustomerSegmentImportResponseDto();
        if (null != customerSegmentInfoRequestDto
                && CollectionUtils.isNotEmpty(customerSegmentInfoRequestDto.getCustomerSegments())) {
            final int size = customerSegmentInfoRequestDto.getCustomerSegments().size();
            LOG.info(SEGMENT_IMPORT_INFO + " Started Processing UserSegment Import From SubscriptionId="
                    + customerSegmentInfoRequestDto.getCustomerSegments().get(0).getSubscriptionId()
                    + " to  SubscriptionId="
                    + customerSegmentInfoRequestDto.getCustomerSegments().get(size - 1).getSubscriptionId());
            responseDto = customerSegmentImportFacade
                    .importCustomerSegments(customerSegmentInfoRequestDto);
        }
        else {
            LOG.error("SEG-IMPORT-ERROR : Received -  customer segments " + customerSegmentInfoRequestDto);
            responseDto.setSuccess(false);
        }
        return responseDto;
    }
}
