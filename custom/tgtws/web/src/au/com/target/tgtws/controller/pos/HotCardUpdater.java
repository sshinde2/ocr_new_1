/**
 * 
 */
package au.com.target.tgtws.controller.pos;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwsfacades.pos.HotCardUpdateFacade;


/**
 * Facade on the top {@link HotCardUpdateFacade}
 * 
 * @author Olivier Lamy
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/hotcardupdater")
public class HotCardUpdater {

    private static final Logger LOG = Logger.getLogger(HotCardUpdater.class);

    @Autowired
    private HotCardUpdateFacade hotCardUpdateFacade;

    /**
     * 
     * @param hotcardNumber
     * @param toAdd
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws ModelSavingException
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/updateHotCardStatus/{hotcardNumber}/{toAdd}", method = RequestMethod.GET)
    @ResponseBody
    public Boolean updateHotCardStatus(@PathVariable("hotcardNumber") final long hotcardNumber,
            @PathVariable("toAdd") final boolean toAdd)
            throws ModelSavingException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        hotCardUpdateFacade.updateHotCardStatus(hotcardNumber, toAdd);
        LOG.info("updateHotCardStatus for card: '" + hotcardNumber + "', toAdd: '" + toAdd);
        return Boolean.TRUE;
    }


}
