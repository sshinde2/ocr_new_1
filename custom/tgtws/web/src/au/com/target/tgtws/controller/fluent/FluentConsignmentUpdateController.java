/**
 * 
 */
package au.com.target.tgtws.controller.fluent;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtws.controller.BaseController;
/**
 * @author mgazal
 *
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/consignment")
public class FluentConsignmentUpdateController extends BaseController {

    private static final Logger LOG = Logger.getLogger(FluentConsignmentUpdateController.class);

    @Autowired
    private FluentFulfilmentService fluentFulfilmentService;

    @Secured("ROLE_APIGATEWAYGROUP")
    @RequestMapping(value = "/update/{eventId}", method = RequestMethod.PUT)
    public ResponseEntity upsertFulfilment(@PathVariable final String eventId,
            @RequestBody final Fulfilment fulfilment) {
        LOG.info("FLUENT_FULFILMENT_UPDATE eventId=" + eventId + " fulfilmentId=" + fulfilment.getFulfilmentId()
                + " status=" + fulfilment.getStatus());
        try {
            fluentFulfilmentService.upsertFulfilment(fulfilment);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (final FluentFulfilmentException e) {
            return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
        }
        catch (final ConversionException | IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
