package au.com.target.tgtws.test.groovy

class TestUtil {
    static String BASE = 'http://localhost:9001/rest/v1/electronicsWS'
    static String USERNAME = 'demo'
    static String PASSWORD = '1234'

    static getConnection(path, method) {
        def url = BASE + path
        def con = url.toURL().openConnection()
        con.requestMethod = method
        return con
    }

    static messageResponseCode(returned, expected) {
        return "Response Code is: " + returned + ", expected: " + expected
    }

    static basicAuth(con) {
        String userpassword = TestUtil.USERNAME + ":" + TestUtil.PASSWORD;
        String encodedAuthorization = userpassword.bytes.encodeBase64().toString()
        con.setRequestProperty("Authorization", "Basic " + encodedAuthorization)
    }

    static basicAuth(con, username, password) {
        String userpassword = username + ":" + password;
        String encodedAuthorization = userpassword.bytes.encodeBase64().toString()
        con.setRequestProperty("Authorization", "Basic " + encodedAuthorization)
    }

    static acceptXML(con) {
        con.setRequestProperty("Accept", "application/xml");
    }

    static acceptJSON(con) {
        con.setRequestProperty("Accept", "application/json");
    }
}
