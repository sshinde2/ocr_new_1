package au.com.target.tgtws.controller.productimport;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.deals.TargetDealCategoryImportFacade;
import au.com.target.tgtwsfacades.deals.dto.TargetDealCategoryDTO;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * Unit test for {@link CategoryImportController}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryImportControllerTest {

    @InjectMocks
    private final CategoryImportController categoryImportController = new CategoryImportController();

    @Mock
    private TargetDealCategoryImportFacade dealCategoryImportFacade;

    @Test
    public void testPutTargetDealCategoryWithDiffCodes() {
        final TargetDealCategoryDTO dealCategoryDTO = Mockito.mock(TargetDealCategoryDTO.class);
        Mockito.when(dealCategoryDTO.getCode()).thenReturn("456");

        final IntegrationResponseDto responseDto = categoryImportController.putTargetDealCategory("123",
                dealCategoryDTO);

        Assert.assertFalse(responseDto.isSuccessStatus());
        Assert.assertEquals(1, responseDto.getMessages().size());
        Assert.assertEquals(
                "123 - Requested deal category code is different to the one supplied with data payload: 456",
                responseDto.getMessages().get(0));
    }

    @Test
    public void testPutTargetDealCategoryWithSameCodes() {
        final TargetDealCategoryDTO dealCategoryDTO = Mockito.mock(TargetDealCategoryDTO.class);
        Mockito.when(dealCategoryDTO.getCode()).thenReturn("456");

        final IntegrationResponseDto responseDto = new IntegrationResponseDto("456");
        responseDto.setSuccessStatus(true);
        responseDto.addMessage("Success");

        Mockito.when(dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO)).thenReturn(responseDto);

        final IntegrationResponseDto result = categoryImportController.putTargetDealCategory("456",
                dealCategoryDTO);

        Assert.assertTrue(result.isSuccessStatus());
        Assert.assertEquals(1, result.getMessages().size());
        Assert.assertEquals("Success", responseDto.getMessages().get(0));

    }
}
