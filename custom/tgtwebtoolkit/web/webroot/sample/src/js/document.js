/*
 * AMD module wrapper for the global document object 
 */
define(['window'], function(window){
	return window && window.document || {};	
});