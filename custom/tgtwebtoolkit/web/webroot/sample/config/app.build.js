({
    appDir: "../src/js",
    baseUrl: ".",
    dir: "../build/production/js",
    //Comment out the optimize line if you want
    //the code minified by UglifyJS
	
    optimize: "none",
    
    //namespace: "Sample",
	
    paths: {
    	requireLib: 'require',
    	jquery: 'lib/jquery'
    },

    modules: [
		{
		    name: "app",
		    include: ["requireLib", "app"],
		    exclude: ['jquery'],
		    //True tells the optimizer it is OK to create
		    //a new file foo.js. Normally the optimizer
		    //wants foo.js to exist in the source directory.
		    create: true
		}
	],
	
	/*
	wrap: {
		start: "(function() {",
        end: "}());"
    },
    */

    stubModules: ['text', 'hbs'],    
   

})
