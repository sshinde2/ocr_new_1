var system = require('system');

/**
 * Wait until the test condition is true or a timeout occurs. Useful for waiting
 * on a server response or for a ui change (fadeIn, etc.) to occur.
 *
 * @param testFx javascript condition that evaluates to a boolean,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param onReady what to do when testFx condition is fulfilled,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param timeOutMillis the max amount of time to wait. If not specified, 3 sec is used.
 */
function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 10001, //< Default Max Timeout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("run-jasmine.js timeout!");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 100); //< repeat check every 100ms
};


if (system.args.length !== 3) {
    console.log('Usage: run-jasmine.js URL');
    phantom.exit(1);
}

var fs = require( 'fs' );
var page = require('webpage').create();
var xmlPath =  encodeURI( fs.absolute( system.args[ 2 ] ) );

page.onInitialized = function() {
    page.evaluate(function(){
        window.__phantom_writeFile = function(filename, text) {
            window.callPhantom( {status: 'write-file', filename: filename, text: text });
        };
    });
};

// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

// Hook to inject jasmine reporters 
page.onCallback = function(data) {
    if (data && data.status ) {
        if( data.status === 'jasmine-ready') {
            page.injectJs('reporters/jasmine.terminal_reporter.js');
            page.injectJs('reporters/jasmine.tap_reporter.js');
            page.injectJs('reporters/jasmine.junit_reporter.js');
        }
        if( data.status === 'write-file') {
            fs.write( xmlPath + data.filename, data.text, "w");
        }
    }
};




//transform the path to a URL
var path = 'file:///' + encodeURI( fs.absolute( system.args[ 1 ] ) );

page.open( path , function(status){
    if (status !== "success") {
        console.log("Unable to access network");
        phantom.exit(1);
    } else {
        waitFor(function(){
            return page.evaluate(function(){
                if (document.querySelector('.jasmine-finished')) {
                    return true;
                }
                return false;
            });
        }, function(){
            var status = page.evaluate(function(){
                list = document.body.querySelectorAll('.jasmine_reporter .failed');
                //Return Exit Status
                return list.length ? 1 : 0;
            });            
            phantom.exit( status );
        },
        // 10 mins
        10 * 60 * 1000);
    }
});
