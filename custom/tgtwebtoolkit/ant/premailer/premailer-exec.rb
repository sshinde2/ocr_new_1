require 'rubygems'
require 'premailer'

puts ARGV[0]
puts ARGV[1]

files = Dir.glob(ARGV[0])
Dir.mkdir(ARGV[1]) unless File.exists?(ARGV[1])
files.each do 
	| file |
	premailer = Premailer.new( file , :warn_level => Premailer::Warnings::SAFE)

	puts "[Premailer] Converting " + File.basename(file)
	
	# Write the HTML output
	fout = File.open( File.join(ARGV[1], File.basename(file) ) , "w")
	fout.puts premailer.to_inline_css
	fout.close

	# Output any CSS warnings
	#premailer.warnings.each do |w|
	  #puts "#{w[:message]} (#{w[:level]}) may not render properly in #{w[:clients]}"
	#end				
	
end