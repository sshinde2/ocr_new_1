/**
 * 
 */
package au.com.target.tgtwebmethods.product.client.impl;

import java.io.IOException;
import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtsale.product.client.TargetPOSProductClient;
import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.exception.TargetWebMethodsClientException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


/**
 * @author rmcalave
 * 
 */
public class TargetPOSProductClientImpl extends AbstractWebMethodsClient implements TargetPOSProductClient
{
    protected static final String ERROR_COMMS_FAILURE = "Communication with WebMethods failed";
    protected static final String ERROR_EMPTY_RESPONSE = "Empty response received from WebMethods for price check";
    protected static final String ERROR_JSON_PARSE = "Unable to parse response from WebMethods for price check";

    protected static final String ERROR_TEMPLATE_PRICE_CHECK = "[PRICE CHECK] {0} [ Store number : {1} ][ EAN : {2} ]";

    private static final String ERROR_SOURCE_WEBMETHODS = "wm";

    private static final Logger LOG = Logger.getLogger(TargetPOSProductClientImpl.class);

    private String priceCheckServiceUrl;

    /*
     * (non-Javadoc)
     * 
     * @see
     * au.com.target.tgtsale.product.client.TargetPOSProductClient#getPOSProductDetails(au.com.target.tgtsale.product
     * .dto.request.ProductRequestDto)
     */
    @Override
    public ProductResponseDto getPOSProductDetails(final ProductRequestDto request) throws TargetIntegrationException
    {
        Validate.notNull(request, "request must not be null");

        final String storeNumber = request.getStoreNumber();
        final String storeState = request.getStoreState();
        final String ean = request.getEan();

        final String priceCheckServiceUrlForProduct = formatPriceCheckServiceUrl(storeNumber, storeState, ean);

        final HttpGet httpGet = new HttpGet(priceCheckServiceUrlForProduct);

        final ResponseHandler<String> handler = new ResponseHandler<String>()
        {

            /*
             * (non-Javadoc)
             * 
             * @see org.apache.http.client.ResponseHandler#handleResponse(org.apache.http.HttpResponse)
             */
            @Override
            public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException
            {
                final HttpEntity entity = response.getEntity();
                if (entity != null)
                {
                    return EntityUtils.toString(entity);
                }
                else
                {
                    return null;
                }
            }

        };

        String response = null;
        ProductResponseDto responseDto = null;
        try
        {
            response = getHttpClient().execute(httpGet, handler);
        }
        catch (final ClientProtocolException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_PRICE_CHECK, ERROR_COMMS_FAILURE, storeNumber, ean),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);

            throw new TargetWebMethodsClientException(ERROR_COMMS_FAILURE, ex);
        }
        catch (final IOException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_PRICE_CHECK, ERROR_COMMS_FAILURE, storeNumber, ean),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);

            throw new TargetWebMethodsClientException(ERROR_COMMS_FAILURE, ex);
        }

        if (StringUtils.isBlank(response))
        {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_PRICE_CHECK, ERROR_EMPTY_RESPONSE, storeNumber, ean),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            throw new TargetWebMethodsClientException(ERROR_EMPTY_RESPONSE);
        }

        try
        {
            responseDto = new Gson().fromJson(response, ProductResponseDto.class);
        }
        catch (final JsonSyntaxException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_PRICE_CHECK, ERROR_JSON_PARSE, storeNumber, ean),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);

            throw new TargetWebMethodsClientException(ERROR_JSON_PARSE, ex);
        }

        if (!responseDto.getSuccess() && ERROR_SOURCE_WEBMETHODS.equals(responseDto.getErrorSource()))
        {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_PRICE_CHECK, responseDto.getErrorMessage(), storeNumber, ean),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            if (!responseDto.isTimeout()) {
                throw new TargetWebMethodsClientException(responseDto.getErrorMessage());
            }
        }

        return responseDto;
    }

    private String formatPriceCheckServiceUrl(final String storeNumber, final String storeState, final String ean)
    {
        final StringBuilder formattedPriceCheckServiceUrl = new StringBuilder();
        formattedPriceCheckServiceUrl.append(getWebmethodsBaseUrl());
        formattedPriceCheckServiceUrl.append(MessageFormat.format(priceCheckServiceUrl, storeNumber, storeState, ean));

        return formattedPriceCheckServiceUrl.toString();
    }

    /**
     * @param priceCheckServiceUrl
     *            the priceCheckServiceUrl to set
     */
    @Required
    public void setPriceCheckServiceUrl(final String priceCheckServiceUrl)
    {
        this.priceCheckServiceUrl = priceCheckServiceUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return priceCheckServiceUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return null;
    }
}
