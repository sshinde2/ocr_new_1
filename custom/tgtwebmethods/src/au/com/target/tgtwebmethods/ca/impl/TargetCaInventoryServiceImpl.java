/**
 * 
 */
package au.com.target.tgtwebmethods.ca.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.stockupdate.TargetInventoryService;
import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtwebmethods.ca.data.request.UpdateInventoryItemQuantityRequest;
import au.com.target.tgtwebmethods.ca.data.response.UpdateInventoryItemQuantityResponse;
import au.com.target.tgtwebmethods.ca.data.types.UpdateQuantityProduct;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;


/**
 * @author bhuang3
 * 
 */
public class TargetCaInventoryServiceImpl extends AbstractWebMethodsClient implements TargetInventoryService {
    private static final Logger LOG = Logger.getLogger(TargetCaInventoryServiceImpl.class);
    private static final String ERROR_CONNECTERROR = "Unable to get response from rest web service for update the ebay item quantity";
    private static final String ERROR_MESSAGE_FROM_RESPONSE = "Error message from rest web service for update the ebay item quantity : {0}";
    private static final String ERROR_PRODUCT_CODE_UNMATCH = "Response product code: {0} is not same as request product code: {1}";
    private static final String ERROR_MESSAGE_NOT_READABLE = "The Response xml unmarshalle failure ";
    private String updateInventoryItemQuantityUrl;
    private String clientID;

    private RestTemplate caWebmethodRestTemplate;

    /**
     * send the rest call to the web method to update the inventory quantity for ebay product.
     * 
     * @param productCode
     * @param quantity
     * @return return true if get the no error response from service and response productCode match the request
     *         productCode otherwise return false
     */
    @Override
    public boolean updateInventoryItemQuantity(final String productCode, final int quantity) {
        final UpdateInventoryItemQuantityRequest updateInventoryItemQuantityRequest = createInventoryUpdateRequest(
                productCode, quantity);
        JaxbLogger.logXml(LOG, updateInventoryItemQuantityRequest);
        return processInventoryUpdateRequest(updateInventoryItemQuantityRequest, productCode);
    }


    /**
     * @param productCode
     * @param quantity
     * @return UpdateInventoryItemQuantityRequest
     */
    protected UpdateInventoryItemQuantityRequest createInventoryUpdateRequest(final String productCode,
            final int quantity) {
        final UpdateInventoryItemQuantityRequest updateInventoryItemQuantityRequest = new UpdateInventoryItemQuantityRequest();
        if (StringUtils.isNotEmpty(productCode)) {
            final List<UpdateQuantityProduct> productList = new ArrayList<>();
            final UpdateQuantityProduct productRequest = new UpdateQuantityProduct();
            productRequest.setProductCode(productCode);
            productRequest.setQuantity(Integer.valueOf(quantity));
            productList.add(productRequest);
            updateInventoryItemQuantityRequest.setClientid(clientID);
            updateInventoryItemQuantityRequest.setProductList(productList);
        }
        return updateInventoryItemQuantityRequest;
    }


    /**
     * @param updateInventoryItemQuantityRequest
     * @param productCode
     * @return processing status
     */
    protected boolean processInventoryUpdateRequest(
            final UpdateInventoryItemQuantityRequest updateInventoryItemQuantityRequest,
            final String productCode) {
        ResponseEntity<UpdateInventoryItemQuantityResponse> responseEntity = null;
        try {
            responseEntity = caWebmethodRestTemplate
                    .exchange(getWebmethodsBaseUrl() + updateInventoryItemQuantityUrl, HttpMethod.POST, new HttpEntity(
                            updateInventoryItemQuantityRequest),
                            UpdateInventoryItemQuantityResponse.class);
        }
        catch (final RestClientException rce) {
            LOG.error(ERROR_CONNECTERROR, rce);
            return false;
        }
        catch (final HttpMessageNotReadableException nre) {
            LOG.error(ERROR_MESSAGE_NOT_READABLE, nre);
            return false;
        }
        final UpdateInventoryItemQuantityResponse updateInventoryItemQuantityResponse = responseEntity.getBody();

        JaxbLogger.logXml(LOG, updateInventoryItemQuantityResponse);

        if (updateInventoryItemQuantityResponse != null) {
            final List<UpdateQuantityProduct> productListResponse = updateInventoryItemQuantityResponse
                    .getProductList();
            if (CollectionUtils.isNotEmpty(productListResponse)) {
                final UpdateQuantityProduct productResponse = productListResponse.get(0);
                if (StringUtils.equals(productResponse.getProductCode(), productCode)) {
                    if (productResponse.getResultCode() != null && productResponse.getResultCode().intValue() == 0) {
                        return true;
                    }
                    else {
                        LOG.error(MessageFormat.format(ERROR_MESSAGE_FROM_RESPONSE, productResponse.getMessage()));
                        return false;
                    }
                }
                else {
                    LOG.error(MessageFormat.format(ERROR_PRODUCT_CODE_UNMATCH, productResponse.getProductCode(),
                            productCode));
                    return false;
                }
            }
        }
        LOG.error(ERROR_MESSAGE_NOT_READABLE);
        return false;
    }

    /**
     * @param caWebmethodRestTemplate
     *            the caWebmethodRestTemplate to set
     */
    @Required
    public void setCaWebmethodRestTemplate(final RestTemplate caWebmethodRestTemplate) {
        this.caWebmethodRestTemplate = caWebmethodRestTemplate;
    }

    /**
     * @param updateInventoryItemQuantityUrl
     *            the updateInventoryItemQuantityUrl to set
     */
    @Required
    public void setUpdateInventoryItemQuantityUrl(final String updateInventoryItemQuantityUrl) {
        this.updateInventoryItemQuantityUrl = updateInventoryItemQuantityUrl;
    }

    /**
     * @return the clientID
     */
    public String getClientID() {
        return clientID;
    }

    /**
     * @param clientID
     *            the clientID to set
     */
    @Required
    public void setClientID(final String clientID) {
        this.clientID = clientID;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return updateInventoryItemQuantityUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return caWebmethodRestTemplate;
    }

}
