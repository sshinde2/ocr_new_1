/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author bhuang3
 * 
 */
@XmlRootElement(name = "lineItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderShipmentLineItem {

    @XmlElement
    private String productCode;

    @XmlElement
    private long quantity;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the quantity
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }


}
