/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtwebmethods.ca.data.types.ShipmentOrder;


/**
 * @author bhuang3
 * 
 */
@XmlRootElement(name = "submitOrderShipmentResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubmitOrderShipmentResponse {

    @XmlElement
    private Integer resultCode;

    @XmlElement
    private String resultMessage;

    @XmlElementWrapper(name = "shipmentOrders")
    @XmlElement(name = "shipmentOrder")
    private List<ShipmentOrder> shipmentList;

    /**
     * @return the shipmentList
     */
    public List<ShipmentOrder> getShipmentList() {
        return shipmentList;
    }

    /**
     * @param shipmentList
     *            the shipmentList to set
     */
    public void setShipmentList(final List<ShipmentOrder> shipmentList) {
        this.shipmentList = shipmentList;
    }

    /**
     * @return the resultCode
     */
    public Integer getResultCode() {
        return resultCode;
    }

    /**
     * @param resultCode
     *            the resultCode to set
     */
    public void setResultCode(final Integer resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * @return the resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * @param resultMessage
     *            the resultMessage to set
     */
    public void setResultMessage(final String resultMessage) {
        this.resultMessage = resultMessage;
    }


}
