/**
 * 
 */
package au.com.target.tgtwebmethods.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtwebmethods.rest.JSONResponse;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WMPollConsignmentWarehouseRestClientTest {

    /**
     * 
     */
    private static final String SECURE_BASE_URL = "secureBaseUrl";

    /**
     * 
     */
    private static final String END_POINT = "endPoint";

    @InjectMocks
    private final WMPollConsignmentWarehouseRestClient restClient = new WMPollConsignmentWarehouseRestClient();

    @Mock
    private RestTemplate restTemplate;

    private PollWarehouseRequest request;

    @Before
    public void setUp() {

        restClient.setServiceEndpoint(END_POINT);
        restClient.setWebmethodsSecureBaseUrl(SECURE_BASE_URL);

        request = new PollWarehouseRequest();
        final ArrayList<String> consignmentNumbers = new ArrayList<>();
        consignmentNumbers.add("number1");
        consignmentNumbers.add("number2");
        request.setConsignmentRequestNumbers(consignmentNumbers);

    }

    @Test
    public void testSuccess() {

        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(true);
        given(restTemplate.postForObject(SECURE_BASE_URL + END_POINT, request, JSONResponse.class))
                .willReturn(jsonResponse);
        final PollWarehouseResponse response = restClient.pollConsignments(request);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getErrorCode()).isNull();
        assertThat(response.getErrorType()).isNull();
        assertThat(response.getErrorValue()).isNull();
    }

    @Test
    public void testFailure() {

        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        given(restTemplate.postForObject(SECURE_BASE_URL + END_POINT, request, JSONResponse.class))
                .willReturn(jsonResponse);
        final PollWarehouseResponse response = restClient.pollConsignments(request);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getErrorCode()).isNotNull();
        assertThat(response.getErrorType()).isNotNull();
        assertThat(response.getErrorType()).isEqualTo(IntegrationError.MIDDLEWARE_ERROR);
        assertThat(response.getErrorValue()).isNotNull();
    }

}
