/**
 * 
 */
package au.com.target.tgtwebmethods.client.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtwebmethods.stockvisibility.data.request.StockVisibilityRequest;
import au.com.target.tgtwebmethods.stockvisibility.data.response.ItemLookUpResponseDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.ItemStockDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.PayloadDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.StockVisibilityResponse;
import au.com.target.tgtwebmethods.util.StockVisibilityUtil;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class StockVisibilityUtilTest {

    @Test
    public void testPopulateRequestNullStores() {
        final List<String> itemCodes = new ArrayList<>();
        itemCodes.add("P1000_puple_s");
        itemCodes.add("P1040_red_s");
        itemCodes.add("P1044_yellow_s");
        final StockVisibilityRequest request = StockVisibilityUtil.populateStockVisibilityRequest(null, itemCodes);
        Assert.assertNull(request);

    }

    @Test
    public void testPopulateRequestWithEmptyItems() {
        final List<String> itemCodes = new ArrayList<>();
        final List<String> stores = new ArrayList<>();
        stores.add("5617");
        stores.add("5618");
        stores.add("5619");
        final StockVisibilityRequest request = StockVisibilityUtil.populateStockVisibilityRequest(stores,
                itemCodes);
        Assert.assertNull(request);
    }

    @Test
    public void testPopulateRequest() {
        final List<String> itemCodes = new ArrayList<>();
        itemCodes.add("P1000_puple_s");
        itemCodes.add("P1040_red_s");
        final List<String> stores = new ArrayList<>();
        stores.add("5617");
        stores.add("5618");
        stores.add("5619");
        final StockVisibilityRequest request = StockVisibilityUtil.populateStockVisibilityRequest(stores,
                itemCodes);
        Assert.assertNotNull(request);
        Assert.assertEquals(2, request.getItemLookup().getItems().size());
        Assert.assertEquals(3, request.getItemLookup().getStores().size());

    }

    @Test
    public void testPopulateStockVisibilityResponseNull() {
        StockVisibilityItemLookupResponseDto response = null;
        response = StockVisibilityUtil.populateStockVisibilityResponse(null);
        Assert.assertNull(response);
    }

    @Test
    public void testPopulateStockVisibilityResponse() {
        StockVisibilityItemLookupResponseDto response = null;
        final StockVisibilityResponse webmethodsResponse = new StockVisibilityResponse();
        webmethodsResponse.setResponseCode("0");
        webmethodsResponse.setResponseMessage("Success");
        final PayloadDto payload = new PayloadDto();
        final ItemLookUpResponseDto itemLookUpResponseDto = new ItemLookUpResponseDto();
        final List<ItemStockDto> list = new ArrayList<>();
        final ItemStockDto itemStockDto = new ItemStockDto();
        itemStockDto.setCode("1235");
        itemStockDto.setSoh("95");
        itemStockDto.setStoreNumber("5317");
        list.add(itemStockDto);
        itemLookUpResponseDto.setItem(list);
        payload.setItemLookupResponse(itemLookUpResponseDto);
        webmethodsResponse.setPayload(payload);
        response = StockVisibilityUtil.populateStockVisibilityResponse(webmethodsResponse);
        Assert.assertNotNull(response);
        Assert.assertEquals("0", response.getResponseCode());
        Assert.assertEquals("Success", response.getResponseMessage());
        Assert.assertEquals(1, response.getItems().size());
        Assert.assertEquals("1235", response.getItems().get(0).getCode());
        Assert.assertEquals("95", response.getItems().get(0).getSoh());
        Assert.assertEquals("5317", response.getItems().get(0).getStoreNumber());

    }
}
