/**
 * 
 */
package au.com.target.tgtwebmethods.manifest.client.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONRequest;
import au.com.target.tgtwebmethods.rest.ManifestJSONResponse;


/**
 * Unit tests to check whether there is a response object returned for a set of scenarios.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AusPostTransmitManifestClientImplTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final AusPostTransmitManifestClientImpl auspostClient = new AusPostTransmitManifestClientImpl();

    @Before
    public void setup() {
        auspostClient.setServiceEndpoint("ENDPOINT");
    }

    @Test
    public void testNullResponse() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Mockito.any(Class.class))).willReturn(null);
        final ManifestResponseDTO response = auspostClient.transmitManifest(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                Mockito.any(Class.class));
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testFailedResponseWithoutError() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final ManifestJSONResponse expectedResponse = new ManifestJSONResponse();
        expectedResponse.setSuccess(false);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Mockito.any(Class.class))).willReturn(expectedResponse);
        final ManifestResponseDTO response = auspostClient.transmitManifest(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.REMOTE_SERVICE_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                Mockito.any(Class.class));
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testFailedResponseWithError() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final ManifestJSONResponse expectedResponse = new ManifestJSONResponse();
        expectedResponse.setSuccess(false);
        final ErrorDTO error = new ErrorDTO();
        final String code = "CODE-1";
        error.setCode(code);
        final String message = "This is an error";
        error.setValue(message);
        final List<ErrorDTO> errors = new ArrayList<>();
        errors.add(error);
        expectedResponse.setErrors(errors);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Mockito.any(Class.class))).willReturn(expectedResponse);
        final ManifestResponseDTO response = auspostClient.transmitManifest(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.MIDDLEWARE_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                Mockito.any(Class.class));
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testThrowsRestClientException() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Mockito.any(Class.class))).willThrow(new RestClientException("RestClientException"));
        final ManifestResponseDTO response = auspostClient.transmitManifest(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccess());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                Mockito.any(Class.class));
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testThrowsHttpMessageNotReadableException() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Mockito.any(Class.class))).willThrow(
                new HttpMessageNotReadableException("HttpMessageNotReadableException"));
        final ManifestResponseDTO response = auspostClient.transmitManifest(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccess());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                Mockito.any(Class.class));
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testSuccess() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final ManifestJSONResponse expectedResponse = new ManifestJSONResponse();
        expectedResponse.setSuccess(true);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Mockito.any(Class.class))).willReturn(expectedResponse);
        final ManifestResponseDTO response = auspostClient.transmitManifest(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.isSuccess());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                Mockito.any(Class.class));
        Mockito.verifyNoMoreInteractions(restTemplate);
    }
}
