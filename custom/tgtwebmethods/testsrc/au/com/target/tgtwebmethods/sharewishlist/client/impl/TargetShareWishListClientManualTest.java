/**
 * 
 */
package au.com.target.tgtwebmethods.sharewishlist.client.impl;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.fest.assertions.Assertions;
import org.junit.Test;

import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtwishlist.data.TargetCustomerWishListDto;
import au.com.target.tgtwishlist.data.TargetCustomerWishListProductsDto;
import au.com.target.tgtwishlist.data.TargetRecipientDto;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;


/**
 * @author rsamuel3
 *
 */
@ManualTest
public class TargetShareWishListClientManualTest extends ServicelayerTransactionalTest {

    private static final Logger LOG = Logger.getLogger(TargetShareWishListClientManualTest.class);

    @Resource
    private TargetShareWishListClientImpl targetShareWishListClient;

    @Test
    public void testResponseFromWebmethods() {
        final TargetWishListSendRequestDto request = buildRequest();
        final TargetIntegrationResponseDto response = targetShareWishListClient.shareWishList(request);
        LOG.info("Response : " + response);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccess()).isFalse();
    }

    protected TargetWishListSendRequestDto buildRequest() {
        final TargetWishListSendRequestDto requestWishList = new TargetWishListSendRequestDto();
        final TargetCustomerWishListDto customer = new TargetCustomerWishListDto();
        customer.setCcme(Boolean.toString(true));
        customer.setEmailAddress("john.smith@test.com");
        customer.setFirstName("John");
        customer.setLastName("Smith");
        customer.setNotShownCount("+7");
        customer.setAccessUrl("http://www.target.com.au/favouritesForJohnSmith");
        final List<TargetCustomerWishListProductsDto> products = populateProducts();
        customer.setRecipients(populateRecipient());
        customer.setProducts(products);
        requestWishList.setCustomer(customer);
        return requestWishList;
    }

    /**
     * @return List<TargetRecipientDto>
     */
    private List<TargetRecipientDto> populateRecipient() {
        final List<TargetRecipientDto> recipientDtos = new ArrayList<>();
        final TargetRecipientDto recipient = new TargetRecipientDto();
        recipient.setEmailAddress("jane.doe@test.com");
        recipient.setName("Jane Doe");
        recipient.setMessage("Hello Jane, I would like to share my favourites with you");
        recipientDtos.add(recipient);
        return recipientDtos;
    }

    /**
     * @return list of TargetCustomerWishListProducts
     */
    private List<TargetCustomerWishListProductsDto> populateProducts() {
        final List<TargetCustomerWishListProductsDto> products = new ArrayList<>();
        final TargetCustomerWishListProductsDto wishlist1 = new TargetCustomerWishListProductsDto();
        wishlist1.setBaseCode("P1000");
        wishlist1.setColour("blue");
        wishlist1.setVariantCode("P1000_blue_M");
        wishlist1.setName("T - Shirt");
        wishlist1.setImageURL("https://www.target.com.au/media/P1000_blue_M.jpg");
        wishlist1.setProductURL("https://www.target.com.au/p/P1000_blue_M");
        wishlist1.setColour("blue");
        wishlist1.setSize("4");
        wishlist1.setPrice("$39.99");
        products.add(wishlist1);

        final TargetCustomerWishListProductsDto wishlist2 = new TargetCustomerWishListProductsDto();
        wishlist2.setBaseCode("P1002");
        wishlist2.setName("Pants");
        wishlist2.setImageURL("https://www.target.com.au/media/p1002.jpg");
        wishlist2.setProductURL("https://www.target.com.au/p/P1002");
        wishlist2.setColour("no colour");
        wishlist2.setSize("10");
        wishlist2.setPrice("$10");
        products.add(wishlist2);

        return products;
    }

}
