/**
 * 
 */
package au.com.target.tgtwebmethods.stock.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentDto;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentRequestDto;
import au.com.target.tgtwebmethods.inventory.dto.response.ErrorDetailsDto;
import au.com.target.tgtwebmethods.inventory.dto.response.InventoryAdjustmentResponseDto;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetInventoryAdjustmentClientImplTest {

    private static final String WEBMETHODS_BASE_URL = "http://webmethods.notreal";
    private static final String SERVICE_ENDPOINT = "/endpoint";

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final TargetInventoryAdjustmentClientImpl targetInventoryAdjustmentClient = new TargetInventoryAdjustmentClientImpl();

    private List<InventoryAdjustmentData> inventoryAdjustmentDataList;

    @Before
    public void setup() {
        targetInventoryAdjustmentClient.setServiceEndpoint(SERVICE_ENDPOINT);
        targetInventoryAdjustmentClient.setWebmethodsSecureBaseUrl(WEBMETHODS_BASE_URL);
        targetInventoryAdjustmentClient.setSource("hybris");
        targetInventoryAdjustmentClient.setUser("hybris");
        mockInventoryAdjustmentDto();
    }


    private void mockInventoryAdjustmentDto() {
        inventoryAdjustmentDataList = new ArrayList<InventoryAdjustmentData>();
        final InventoryAdjustmentData data1 = new InventoryAdjustmentData();
        data1.setAdjustmentQty("1");
        data1.setCode("2233");
        data1.setStoreNum("3334");
        final InventoryAdjustmentData data2 = new InventoryAdjustmentData();
        data2.setAdjustmentQty("13");
        data2.setCode("4455");
        data2.setStoreNum("322334");
        inventoryAdjustmentDataList.add(data1);
        inventoryAdjustmentDataList.add(data2);

    }

    private void verifyRequestData(final InventoryAdjustmentRequestDto requestBody) {
        assertThat(requestBody.getSource()).isEqualTo("hybris");
        assertThat(requestBody.getUser()).isEqualTo("hybris");
        assertThat(requestBody.getTrackingId()).isNotNull();
        assertThat(requestBody.getTimestamp()).isNotNull();

        final List<InventoryAdjustmentDto> inventoryAdjDtoList = requestBody.getInventoryAdjustments();
        assertThat(inventoryAdjDtoList).hasSize(2);

        for (final InventoryAdjustmentDto inventoryAdjDto : inventoryAdjDtoList) {
            if (inventoryAdjDto.getCode().equals("2233")) {
                assertThat(inventoryAdjDto.getAdjustmentQty()).isEqualTo("1");
                assertThat(inventoryAdjDto.getStoreNum()).isEqualTo("3334");
            }
            if (inventoryAdjDto.getCode().equals("4455")) {
                assertThat(inventoryAdjDto.getAdjustmentQty()).isEqualTo("13");
                assertThat(inventoryAdjDto.getStoreNum()).isEqualTo("322334");
            }
        }
    }

    private ResponseEntity<InventoryAdjustmentResponseDto> getResponseFromServiceForScenario(final Boolean success) {
        final ResponseEntity<InventoryAdjustmentResponseDto> mockResponseEntity = mock(ResponseEntity.class);
        final InventoryAdjustmentResponseDto responseDto = new InventoryAdjustmentResponseDto();
        responseDto.setSuccess(success);
        given(mockResponseEntity.getBody()).willReturn(responseDto);
        return mockResponseEntity;
    }

    @Test
    public void testInventoryAdjustmentForInvalidEndPoint() throws TargetIntegrationException {
        targetInventoryAdjustmentClient.setServiceEndpoint("");
        assertThat(targetInventoryAdjustmentClient.sendInventoryAdjustment(inventoryAdjustmentDataList))
                .isFalse();
    }

    @Test
    public void testInventoryAdjustmentForNullRequest() throws TargetIntegrationException {
        assertThat(targetInventoryAdjustmentClient.sendInventoryAdjustment(null))
                .isFalse();
    }


    @Test
    public void testInventoryAdjustmentWhenExceptionThrown() throws TargetIntegrationException {
        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        final RuntimeException exception = new RuntimeException("test");
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(InventoryAdjustmentResponseDto.class))).willThrow(exception);
        assertThat(targetInventoryAdjustmentClient
                .sendInventoryAdjustment(inventoryAdjustmentDataList)).isFalse();
        Mockito.verify(restTemplate).exchange(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT, HttpMethod.POST,
                httpEntityCaptor.getValue(), InventoryAdjustmentResponseDto.class);
        final InventoryAdjustmentRequestDto requestBody = (InventoryAdjustmentRequestDto)httpEntityCaptor.getValue()
                .getBody();
        verifyRequestData(requestBody);
    }

    @Test
    public void testInventoryAdjustmentWhenResponseIsFailure() throws TargetIntegrationException {
        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        final ResponseEntity<InventoryAdjustmentResponseDto> mockResponseEntity = getResponseFromServiceForScenario(
                Boolean.FALSE);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(InventoryAdjustmentResponseDto.class))).willReturn(mockResponseEntity);
        assertThat(targetInventoryAdjustmentClient
                .sendInventoryAdjustment(inventoryAdjustmentDataList)).isFalse();
        final InventoryAdjustmentRequestDto requestBody = (InventoryAdjustmentRequestDto)httpEntityCaptor.getValue()
                .getBody();
        verifyRequestData(requestBody);
    }


    @Test
    public void testInventoryAdjustmentWhenResponseIsSuccess() throws TargetIntegrationException {
        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        final ResponseEntity<InventoryAdjustmentResponseDto> mockResponseEntity = getResponseFromServiceForScenario(
                Boolean.TRUE);
        final InventoryAdjustmentResponseDto responseDto = mock(InventoryAdjustmentResponseDto.class);
        given(responseDto.getResponseCode()).willReturn("0");
        given(mockResponseEntity.getBody()).willReturn(responseDto);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(InventoryAdjustmentResponseDto.class))).willReturn(mockResponseEntity);
        assertThat(targetInventoryAdjustmentClient
                .sendInventoryAdjustment(inventoryAdjustmentDataList)).isTrue();
        final InventoryAdjustmentRequestDto requestBody = (InventoryAdjustmentRequestDto)httpEntityCaptor.getValue()
                .getBody();
        verifyRequestData(requestBody);
    }

    @Test
    public void testInventoryAdjustmentWhenResponseIsSuccessWithErrors() throws TargetIntegrationException {
        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        final ResponseEntity<InventoryAdjustmentResponseDto> mockResponseEntity = getResponseFromServiceForScenario(
                Boolean.TRUE);

        final InventoryAdjustmentResponseDto responseDto = mock(InventoryAdjustmentResponseDto.class);
        given(responseDto.getResponseCode()).willReturn("0");
        final ErrorDetailsDto dto = mock(ErrorDetailsDto.class);
        given(dto.getErrorCode()).willReturn("errorcode");
        given(dto.getErrorMessage()).willReturn("errorMessage");
        final List<ErrorDetailsDto> errorDetailsDtoList = new ArrayList<>();
        errorDetailsDtoList.add(dto);
        given(responseDto.getErrorList()).willReturn(errorDetailsDtoList);

        given(mockResponseEntity.getBody()).willReturn(responseDto);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(InventoryAdjustmentResponseDto.class))).willReturn(mockResponseEntity);
        assertThat(targetInventoryAdjustmentClient
                .sendInventoryAdjustment(inventoryAdjustmentDataList)).isTrue();
        final InventoryAdjustmentRequestDto requestBody = (InventoryAdjustmentRequestDto)httpEntityCaptor.getValue()
                .getBody();
        verifyRequestData(requestBody);
    }

}
