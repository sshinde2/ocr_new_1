package au.com.target.tgtwebmethods.community.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtutility.http.factory.HttpClientFactory;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCommunityDonationClientImplTest {
    private static final String COMMUNITY_DONATIONS_URL = "/rest/communitydonations";
    private static final String WEBMETHODS_BASE_URL = "http://webmethods.notreal";

    @Mock
    private HttpClientFactory httpClientFactory;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final TargetCommunityDonationClientImpl targetCommunityDonationClientImpl = new TargetCommunityDonationClientImpl();

    @Before
    public void setUp() {
        targetCommunityDonationClientImpl.setWebmethodsBaseUrl(WEBMETHODS_BASE_URL);
        targetCommunityDonationClientImpl.setCommunityDonationsUrl(COMMUNITY_DONATIONS_URL);
    }

    @Test
    public void testSendDonationRequestDataWithTrueResponse() throws Exception {
        final TargetCommunityDonationRequestDto request = createRequestDto();

        final ResponseEntity<String> mockResponseEntity = mock(ResponseEntity.class);
        given(mockResponseEntity.getBody()).willReturn("true");

        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + COMMUNITY_DONATIONS_URL), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(String.class))).willReturn(mockResponseEntity);

        final boolean result = targetCommunityDonationClientImpl.sendDonationRequestData(request);

        assertThat(result).isTrue();

        final HttpEntity<String> capturedEntity = httpEntityCaptor.getValue();
        final String entityBody = capturedEntity.getBody();

        verifyResult(entityBody, request);
    }

    @Test
    public void testSendDonationRequestDataWithFalseResponse() throws Exception {
        final TargetCommunityDonationRequestDto request = createRequestDto();

        final ResponseEntity<String> mockResponseEntity = mock(ResponseEntity.class);
        given(mockResponseEntity.getBody()).willReturn("false");

        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + COMMUNITY_DONATIONS_URL), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(String.class))).willReturn(mockResponseEntity);

        final boolean result = targetCommunityDonationClientImpl.sendDonationRequestData(request);

        assertThat(result).isTrue();

        final HttpEntity<String> capturedEntity = httpEntityCaptor.getValue();
        final String entityBody = capturedEntity.getBody();

        verifyResult(entityBody, request);
    }

    @Test
    public void testSendDonationRequestDataWithRestClientException() throws Exception {
        final TargetCommunityDonationRequestDto request = createRequestDto();

        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + COMMUNITY_DONATIONS_URL), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(String.class))).willThrow(new RestClientException("Exception!"));

        final boolean result = targetCommunityDonationClientImpl.sendDonationRequestData(request);
        Assert.assertFalse(result);
    }

    private TargetCommunityDonationRequestDto createRequestDto() {
        final TargetCommunityDonationRequestDto request = new TargetCommunityDonationRequestDto();
        request.setEventName("BBQ");
        request.setOrgName("Al Harrington's Wacky Waving Inflatable Arm Flailing Tube Man emporium and warehouse");
        request.setAbnOrAcn("75004250944");
        request.setOrgFocus("Sales");
        request.setContactName("Robert McAlavey");
        request.setRole("Owner");
        request.setOrgStreet("123 Fake Street");
        request.setOrgCity("Geelong");
        request.setOrgState("VIC");
        request.setOrgPostcode("3220");
        request.setOrgCountry("Australia");
        request.setEmail("robert.mcalavey@target.com.au");
        request.setPhone("0352463359");
        request.setScope("Local");
        request.setReason("Money");
        request.setRequestNotes("I would like money");
        request.setPreferredMethodOfContact("Email");
        request.setStoreNumber("5599");
        request.setStoreState("VIC");

        return request;
    }

    private void verifyResult(final String responseJson, final TargetCommunityDonationRequestDto originalRequest) {
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        final TargetCommunityDonationRequestDto dtoFromResponseJson = gson.fromJson(responseJson,
                TargetCommunityDonationRequestDto.class);
        assertThat(dtoFromResponseJson.getEventName()).isEqualTo(originalRequest.getEventName());
        assertThat(dtoFromResponseJson.getOrgName()).isEqualTo(originalRequest.getOrgName());
        assertThat(dtoFromResponseJson.getAbnOrAcn()).isEqualTo(originalRequest.getAbnOrAcn());
        assertThat(dtoFromResponseJson.getOrgFocus()).isEqualTo(originalRequest.getOrgFocus());
        assertThat(dtoFromResponseJson.getContactName()).isEqualTo(originalRequest.getContactName());
        assertThat(dtoFromResponseJson.getRole()).isEqualTo(originalRequest.getRole());
        assertThat(dtoFromResponseJson.getOrgStreet()).isEqualTo(originalRequest.getOrgStreet());
        assertThat(dtoFromResponseJson.getOrgCity()).isEqualTo(originalRequest.getOrgCity());
        assertThat(dtoFromResponseJson.getOrgState()).isEqualTo(originalRequest.getOrgState());
        assertThat(dtoFromResponseJson.getOrgPostcode()).isEqualTo(originalRequest.getOrgPostcode());
        assertThat(dtoFromResponseJson.getOrgCountry()).isEqualTo(originalRequest.getOrgCountry());
        assertThat(dtoFromResponseJson.getEmail()).isEqualTo(originalRequest.getEmail());
        assertThat(dtoFromResponseJson.getPhone()).isEqualTo(originalRequest.getPhone());
        assertThat(dtoFromResponseJson.getScope()).isEqualTo(originalRequest.getScope());
        assertThat(dtoFromResponseJson.getReason()).isEqualTo(originalRequest.getReason());
        assertThat(dtoFromResponseJson.getRequestNotes()).isEqualTo(originalRequest.getRequestNotes());
        assertThat(dtoFromResponseJson.getPreferredMethodOfContact()).isEqualTo(
                originalRequest.getPreferredMethodOfContact());
        assertThat(dtoFromResponseJson.getStoreNumber()).isEqualTo(originalRequest.getStoreNumber());
        assertThat(dtoFromResponseJson.getStoreState()).isEqualTo(originalRequest.getStoreState());
    }
}
