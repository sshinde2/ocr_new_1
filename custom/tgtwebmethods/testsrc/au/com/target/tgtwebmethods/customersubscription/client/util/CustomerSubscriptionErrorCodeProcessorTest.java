/**
 * 
 */
package au.com.target.tgtwebmethods.customersubscription.client.util;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;


/**
 * @author htan3
 *
 */
@UnitTest
public class CustomerSubscriptionErrorCodeProcessorTest {

    private final CustomerSubscriptionErrorCodeProcessor processor = new CustomerSubscriptionErrorCodeProcessor();

    @Test
    public void testCreateSuccess() {
        assertEquals(TargetCustomerSubscriptionResponseType.SUCCESS,
                processor.getCustomerSubscriptionResponseTypeForCode("0"));
    }

    @Test
    public void testUpdateSuccess() {
        assertEquals(TargetCustomerSubscriptionResponseType.SUCCESS,
                processor.getCustomerSubscriptionResponseTypeForCode("-5"));
    }

    @Test
    public void testUnavailable() {
        assertEquals(TargetCustomerSubscriptionResponseType.UNAVAILABLE,
                processor.getCustomerSubscriptionResponseTypeForCode("-2"));
    }

    @Test
    public void testAlreadyExists() {
        assertEquals(TargetCustomerSubscriptionResponseType.ALREADY_EXISTS,
                processor.getCustomerSubscriptionResponseTypeForCode("-4"));
    }

    @Test
    public void testInvalid() {
        assertEquals(TargetCustomerSubscriptionResponseType.INVALID,
                processor.getCustomerSubscriptionResponseTypeForCode("-1"));
    }

    @Test
    public void testOthers() {
        assertEquals(TargetCustomerSubscriptionResponseType.OTHERS,
                processor.getCustomerSubscriptionResponseTypeForCode("-3"));
        assertEquals(TargetCustomerSubscriptionResponseType.OTHERS,
                processor.getCustomerSubscriptionResponseTypeForCode("1"));
        assertEquals(TargetCustomerSubscriptionResponseType.OTHERS,
                processor.getCustomerSubscriptionResponseTypeForCode(null));
    }

}
