/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author kbalasub
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateContainsPreOrderItemsFlagActionTest {

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final UpdateContainsPreOrderItemsFlagAction action = new UpdateContainsPreOrderItemsFlagAction();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Before
    public void setup() {
        action.setContainsPreOrderItems(Boolean.FALSE);
        action.setModelService(modelService);
    }

    @Test
    public void testSetContainsPreOrderItemsIsCalledByExecuteAction() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(order);
        willDoNothing().given(modelService).save(order);
        willDoNothing().given(modelService).refresh(order);
        action.executeAction(process);
        verify(order).setContainsPreOrderItems(Boolean.FALSE);
        verify(modelService).save(order);
        verify(modelService).refresh(order);
    }

    @Test
    public void testExecuteActionThrowsErrorWhenPassedNullProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Orderprocess cannot be null");
        action.executeAction(null);
    }

    @Test
    public void testExecuteActionThrowsErrorWhenPassedNullOrder() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        process.setOrder(null);
        action.executeAction(process);
    }
}
