/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * @author kbalasub
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AcceptPreOrderReleaseActionTest {

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private OrderProcessModel process;

    @InjectMocks
    @Spy
    private final AcceptPreOrderReleaseAction action = new AcceptPreOrderReleaseAction();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void executeActionTest() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(orderModel);
        final ArgumentCaptor<OrderModel> orderCaptor = ArgumentCaptor.forClass(OrderModel.class);
        doReturn(targetBusinessProcessService).when(action).getTargetBusinessProcessService();
        action.executeAction(process);
        verify(targetBusinessProcessService).startFluentPreOrderReleaseAcceptProcess(orderCaptor.capture());
        assertThat(orderCaptor.getValue()).isEqualTo(orderModel);
    }

    @Test
    public void executeActionThrowsExceptionWhenPassedANullOrderProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Pre-order release accept process cannot be null");
        action.executeAction(null);
    }

    @Test
    public void executeActionThrowsExceptionWhenPassedANullProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        final OrderProcessModel process = new OrderProcessModel();
        process.setOrder(null);
        action.executeAction(process);
    }
}
