package au.com.target.tgtbusproc.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.model.UpdateCustomerEmailProcessModel;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;


/**
 * Unit test for {@link TargetBusinessProcessServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetBusinessProcessServiceImplTest {

    @Spy
    @InjectMocks
    private final TargetBusinessProcessServiceImpl businessProcessServiceImpl = new TargetBusinessProcessServiceImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private BusinessProcessParameterModel bppm;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private OrderModel orderModel;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private BusinessProcessModel businessProcessModel;

    @Mock
    private StoreFrontCustomerProcessModel storeFrontCustomerProcessModel;

    @Mock
    private StoreFrontProcessModel storeFrontProcessModel;

    @Mock
    private ForgottenPasswordProcessModel forgottenPasswordProcessModel;

    @Mock
    private UpdateCustomerEmailProcessModel updateCustomerEmailProcessModel;


    @Before
    public void setUp() {
        given(orderModel.getCode()).willReturn("12345");
        willReturn(orderProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());
        willDoNothing().given(businessProcessServiceImpl).startProcess(orderProcessModel);
        willDoNothing().given(businessProcessServiceImpl).startProcess(businessProcessModel);
        willDoNothing().given(businessProcessServiceImpl).startProcess(storeFrontCustomerProcessModel);
        willDoNothing().given(businessProcessServiceImpl).startProcess(storeFrontProcessModel);
        willDoNothing().given(businessProcessServiceImpl).startProcess(forgottenPasswordProcessModel);
        willDoNothing().given(businessProcessServiceImpl).startProcess(updateCustomerEmailProcessModel);
        willReturn(baseSiteService).given(businessProcessServiceImpl).getBaseSiteService();
        willReturn(null).given(businessProcessServiceImpl).getProcess(anyString());
    }

    @Test
    public void testStartOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startOrderProcess(null, "test");
    }

    @Test
    public void testStartOrderProcessWithNullProcessDefinition() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("ProcessDefinitionName cannot be null");
        businessProcessServiceImpl.startOrderProcess(orderModel, null);
    }

    @Test
    public void testStartOrderProcessWithNullProcessCode() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("ProcessCode cannot be null");
        businessProcessServiceImpl.startOrderProcess(orderModel, null, "test");
    }

    @Test
    public void testStartOrderProcessWithoutProcessCode() {
        businessProcessServiceImpl.startOrderProcess(orderModel, "test");

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartOrderProcessWithProcessCode() {
        businessProcessServiceImpl.startOrderProcess(orderModel, "myProcessCode", "test");

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess("myProcessCode", "test");
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartOrderCancelProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startOrderCancelProcess(null, null, null, null, true, null, BigDecimal.ZERO);
    }

    @Test
    public void testStartOrderCancelProcessWithNullOcre() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("ocrem cannot be null");

        businessProcessServiceImpl.startOrderCancelProcess(orderModel, null, null, null, true, null, BigDecimal.ZERO);
    }

    @Test
    public void testStartOrderCancelProcess() {

        final OrderCancelRecordEntryModel ocrem = mock(OrderCancelRecordEntryModel.class);
        given(ocrem.getCode()).willReturn("98765");

        final List<PaymentTransactionEntryModel> refundPaymentEntryList = new ArrayList<>();

        final BigDecimal amount = BigDecimal.valueOf(20d).setScale(2);
        businessProcessServiceImpl.startOrderCancelProcess(orderModel, ocrem, amount,
                CancelTypeEnum.COCKPIT_CANCEL, true, refundPaymentEntryList, BigDecimal.ZERO);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(orderProcessParameterHelper).setOrderCancelRequest(orderProcessModel, ocrem);
        verify(orderProcessParameterHelper).setRefundAmount(orderProcessModel, amount);
        verify(orderProcessParameterHelper).setDoCancelExtract(orderProcessModel, true);
        verify(orderProcessParameterHelper).setRefundPaymentEntryList(orderProcessModel, refundPaymentEntryList);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartPlaceOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startPlaceOrderProcess(null, null);
    }

    @Test
    public void testStartPlaceOrderProcessWithNullPtem() {
        businessProcessServiceImpl.startPlaceOrderProcess(orderModel, null);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyZeroInteractions(orderProcessParameterHelper);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartPlaceOrderProcessWithPtem() {
        final PaymentTransactionEntryModel ptem = mock(PaymentTransactionEntryModel.class);

        businessProcessServiceImpl.startPlaceOrderProcess(orderModel, ptem);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(orderProcessParameterHelper).setPaymentTransactionEntry(orderProcessModel, ptem);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartPlacePreOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startPlacePreOrderProcess(null, null);
    }

    @Test
    public void testStartPlacePreOrderProcessWithNullPtem() {
        businessProcessServiceImpl.startPlacePreOrderProcess(orderModel, null);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyZeroInteractions(orderProcessParameterHelper);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartPlacePreOrderProcessWithPtem() {
        final PaymentTransactionEntryModel ptem = mock(PaymentTransactionEntryModel.class);

        businessProcessServiceImpl.startPlacePreOrderProcess(orderModel, ptem);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(orderProcessParameterHelper).setPaymentTransactionEntry(orderProcessModel, ptem);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartPlaceFluentOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startPlaceFluentOrderProcess(null, null);
    }

    @Test
    public void testStartPlaceFluentOrderProcessWithNullPtem() {
        businessProcessServiceImpl.startPlaceFluentOrderProcess(orderModel, null);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyZeroInteractions(orderProcessParameterHelper);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartPlaceFluentOrderProcess() {
        final PaymentTransactionEntryModel ptem = mock(PaymentTransactionEntryModel.class);

        businessProcessServiceImpl.startPlaceFluentOrderProcess(orderModel, ptem);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(orderProcessParameterHelper).setPaymentTransactionEntry(orderProcessModel, ptem);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartOrderAcceptProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startOrderAcceptProcess(null);
    }

    @Test
    public void testStartOrderAcceptProcess() {
        businessProcessServiceImpl.startOrderAcceptProcess(orderModel);

        //Verify required interactions
        verify(businessProcessServiceImpl).startOrderProcess(any(OrderModel.class),
                anyString(),
                anyString());
    }

    /**
     * Method to test pre-order accept process for no order.
     */
    @Test
    public void testStartPreOrderAcceptProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startPreOrderAcceptProcess(null);
    }

    /**
     * Method to test pre-order accept process.
     */
    @Test
    public void testStartPreOrderAcceptProcess() {
        businessProcessServiceImpl.startPreOrderAcceptProcess(orderModel);

        //Verify required interactions
        verify(businessProcessServiceImpl).startOrderProcess(any(OrderModel.class),
                anyString(),
                anyString());
    }


    @Test
    public void testStartPaymentFailedProcessNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startPaymentFailedProcess(null, "test");
    }

    @Test
    public void testStartPaymentFailedProcess() {
        given(modelService.create(BusinessProcessParameterModel.class)).willReturn(bppm);

        businessProcessServiceImpl.startPaymentFailedProcess(orderModel, "REJECTED");

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessParameterHelper)
                .setPaymentFailedReason(any(OrderProcessModel.class), eq("REJECTED"));
    }

    @Test
    public void testStartCustomerRegistrationProcessWithNullCustomer() {

        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("CustomerModel cannot be null");
        businessProcessServiceImpl.startCustomerRegistrationProcess(null, baseSiteModel);
    }

    @Test
    public void testStartCustomerRegistrationProcessWithNullSite() {

        final CustomerModel customerModel = mock(CustomerModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("BaseSiteModel cannot be null");
        businessProcessServiceImpl.startCustomerRegistrationProcess(customerModel, null);
    }

    @Test
    public void testStartCustomerRegistrationProcess() {

        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        final CustomerModel customerModel = mock(CustomerModel.class);

        given(customerModel.getUid()).willReturn("Mr.Rondel");
        willReturn(storeFrontCustomerProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());

        businessProcessServiceImpl.startCustomerRegistrationProcess(customerModel, baseSiteModel);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(storeFrontCustomerProcessModel).setCustomer(customerModel);
        verify(storeFrontCustomerProcessModel).setSite(baseSiteModel);
        verify(modelService).save(storeFrontCustomerProcessModel);
        verify(businessProcessServiceImpl).startProcess(storeFrontCustomerProcessModel);
    }

    @Test
    public void testStartForgottenPasswordProcessWithNullCustomer() {
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("CustomerModel cannot be null");
        businessProcessServiceImpl.startForgottenPasswordProcess(null, baseSiteModel, "token");
    }

    @Test
    public void testStartForgottenPasswordProcessWithNullSite() {
        final CustomerModel customerModel = mock(CustomerModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("BaseSiteModel cannot be null");
        businessProcessServiceImpl.startForgottenPasswordProcess(customerModel, null, "token");
    }

    @Test
    public void testStartForgottenPasswordProcessWithNullToken() {
        final CustomerModel customerModel = mock(CustomerModel.class);
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("token cannot be null");
        businessProcessServiceImpl.startForgottenPasswordProcess(customerModel, baseSiteModel, null);
    }

    @Test
    public void testStartForgottenPasswordProcess() {
        final CustomerModel customerModel = mock(CustomerModel.class);
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        given(customerModel.getUid()).willReturn("Mr.Rondel");
        willReturn(forgottenPasswordProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());

        businessProcessServiceImpl.startForgottenPasswordProcess(customerModel, baseSiteModel, "token");

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(
                startsWith("forgottenPasswordProcess-Mr.Rondel"),
                eq("forgottenPasswordProcess"));
        verify(forgottenPasswordProcessModel).setCustomer(customerModel);
        verify(forgottenPasswordProcessModel).setSite(baseSiteModel);
        verify(forgottenPasswordProcessModel).setToken("token");
        verify(modelService).save(forgottenPasswordProcessModel);
        verify(businessProcessServiceImpl).startProcess(forgottenPasswordProcessModel);
    }

    @Test
    public void testStartCustomerPasswordChangeProcessWithNullCustomer() {

        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Customermodel cannot be null");
        businessProcessServiceImpl.startCustomerPasswordChangeProcess(null, baseSiteModel);
    }

    @Test
    public void testStartCustomerPasswordChangeProcessWithNullSite() {

        final CustomerModel customerModel = mock(CustomerModel.class);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("BaseSitemodel cannot be null");
        businessProcessServiceImpl.startCustomerPasswordChangeProcess(customerModel, null);
    }

    @Test
    public void testStartCustomerPasswordChangeProcess() {
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        final CustomerModel customerModel = mock(CustomerModel.class);

        given(customerModel.getUid()).willReturn("Mr.Rondel");
        willReturn(storeFrontCustomerProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());

        businessProcessServiceImpl.startCustomerPasswordChangeProcess(customerModel, baseSiteModel);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(
                startsWith("customerPasswordChangeProcess-Mr.Rondel"),
                eq("customerPasswordChangeProcess"));
        verify(storeFrontCustomerProcessModel).setCustomer(customerModel);
        verify(storeFrontCustomerProcessModel).setSite(baseSiteModel);
        verify(modelService).save(storeFrontCustomerProcessModel);
        verify(businessProcessServiceImpl).startProcess(storeFrontCustomerProcessModel);
    }

    @Test
    public void testStartCustomerUpdateEmailAddrProcess() {

        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        final CustomerModel customerModel = mock(CustomerModel.class);

        given(customerModel.getUid()).willReturn("Mr.Rondel");
        willReturn(updateCustomerEmailProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());

        businessProcessServiceImpl.startCustomerUpdateEmailAddrProcess(customerModel, baseSiteModel,
                "old@target.com.au");

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(),
                anyString());
        verify(updateCustomerEmailProcessModel).setCustomer(customerModel);
        verify(updateCustomerEmailProcessModel).setSite(baseSiteModel);
        verify(updateCustomerEmailProcessModel).setOldEmailAddress("old@target.com.au");
        verify(modelService).save(updateCustomerEmailProcessModel);
        verify(businessProcessServiceImpl).startProcess(updateCustomerEmailProcessModel);
    }

    @Test
    public void testStartCustomerUpdateEmailAddrProcessNullOldEmail() {

        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        final CustomerModel customerModel = mock(CustomerModel.class);

        given(customerModel.getUid()).willReturn("Mr.Rondel");
        willReturn(updateCustomerEmailProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());

        businessProcessServiceImpl.startCustomerUpdateEmailAddrProcess(customerModel, baseSiteModel, null);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(),
                anyString());
        verify(updateCustomerEmailProcessModel).setCustomer(customerModel);
        verify(updateCustomerEmailProcessModel).setSite(baseSiteModel);
        verify(updateCustomerEmailProcessModel).setOldEmailAddress(null);
        verify(modelService).save(updateCustomerEmailProcessModel);
        verify(businessProcessServiceImpl).startProcess(updateCustomerEmailProcessModel);
    }

    /**
     * Verifies that registration of child process in the context of current process will add a special context
     * parameter.
     */
    @Test
    public void testRegisterChild() {
        final String childProcessCode = "process-code-123";
        final OrderProcessModel childProcess = mock(OrderProcessModel.class);
        when(childProcess.getCode()).thenReturn(childProcessCode);

        businessProcessServiceImpl.registerChildProcess(childProcess, orderProcessModel);

        verify(orderProcessParameterHelper).setProcessParameter(
                orderProcessModel, "last-process-code", childProcessCode);
        verify(modelService).refresh(orderProcessModel);

    }

    @Test
    public void testStartResendTaxInvoiceProcess() {
        final String newEmail = "Seabook@gmail.com";

        businessProcessServiceImpl.startResendTaxInvoiceProcess(orderModel, newEmail);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
        verify(orderProcessParameterHelper).setNewEmailAddr(orderProcessModel, newEmail);
    }

    @Test
    public void testResendConsignmentProcess() {
        willReturn(orderProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());

        when(consignment.getOrder()).thenReturn(orderModel);

        final OrderProcessModel actualOrderProcessModel = businessProcessServiceImpl
                .resendConsignmentProcess(consignment);

        Assert.assertNotNull(actualOrderProcessModel);

        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessParameterHelper).setConsignment(orderProcessModel, consignment);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testResendConsignmentProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");

        when(consignment.getOrder()).thenReturn(null);

        businessProcessServiceImpl.resendConsignmentProcess(consignment);


        verifyZeroInteractions(businessProcessServiceImpl);
        verifyZeroInteractions(orderProcessParameterHelper);
        verifyZeroInteractions(modelService);
    }


    @Test
    public void testResendConsignmentProcessWithNullConsignment() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment can not be null");

        businessProcessServiceImpl.resendConsignmentProcess(null);


        verifyZeroInteractions(businessProcessServiceImpl);
        verifyZeroInteractions(orderProcessParameterHelper);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testStartSendClickAndCollectNotificationProcess() {
        final Object cncNotificationData = new Object();

        businessProcessServiceImpl.startSendClickAndCollectNotificationProcess(orderModel, cncNotificationData, true);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
        verify(orderProcessParameterHelper).setCncNotificationData(orderProcessModel, cncNotificationData);
        verify(orderProcessParameterHelper).setPartnerUpdateRequired(orderProcessModel, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartSendClickAndCollectNotificationProcessWithNullCncNotificationData() {
        final Object cncNotificationData = null;

        businessProcessServiceImpl.startSendClickAndCollectNotificationProcess(orderModel, cncNotificationData, true);
    }

    @Test
    public void testStartcustomerSubscriptionProcess() throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException {
        final CustomerSubscriptionTestData subscriptionDetails = new CustomerSubscriptionTestData();
        subscriptionDetails.setCustomerEmail("test@email.com");
        businessProcessServiceImpl.startCustomerSubscriptionProcess(subscriptionDetails,
                TgtbusprocConstants.BusinessProcess.CUSTOMER_SUBSCRIPTION_PROCESS);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessParameterHelper).setCustomerSubscriptionData(orderProcessModel, subscriptionDetails);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);

    }

    @Test
    public void testStartcustomerSubscriptionProcessWithNullSubscriptionDetails() {
        final Object subscriptionDetails = null;

        try {
            businessProcessServiceImpl.startCustomerSubscriptionProcess(subscriptionDetails,
                    TgtbusprocConstants.BusinessProcess.CUSTOMER_SUBSCRIPTION_PROCESS);
        }
        catch (final Exception e) {
            Assert.assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void testStartcustomerSubscriptionProcessWithEmptyEmail() throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        final CustomerSubscriptionTestData subscriptionDetails = new CustomerSubscriptionTestData();
        subscriptionDetails.setCustomerEmail(StringUtils.EMPTY);

        try {
            businessProcessServiceImpl.startCustomerSubscriptionProcess(subscriptionDetails,
                    TgtbusprocConstants.BusinessProcess.CUSTOMER_PERSONALDETAILS_SUBSCRIPTION_PROCESS);
        }
        catch (final Exception e) {
            Assert.assertEquals(IllegalStateException.class, e.getClass());
            Assert.assertEquals("email cannot be empty", e.getMessage());
        }
    }

    @Test
    public void teststartOrderConsignmentProcessNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startConsignmentRejectionProcess(null, null, null, null);
        verifyZeroInteractions(orderProcessParameterHelper);
        verifyNoMoreInteractions(businessProcessServiceImpl);
    }

    @Test
    public void teststartOrderConsignmentProcessNullProcessDefinition() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("ProcessDefinitionName cannot be null");
        businessProcessServiceImpl.startConsignmentRejectionProcess(orderModel, consignment,
                ConsignmentRejectReason.PICK_FAILED, null);
        verifyZeroInteractions(orderProcessParameterHelper);
        verifyNoMoreInteractions(businessProcessServiceImpl);
    }

    @Test
    public void teststartOrderConsignmentProcessNullConsignment() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("consignment cannot be null");
        businessProcessServiceImpl.startConsignmentRejectionProcess(orderModel, null,
                ConsignmentRejectReason.PICK_FAILED, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyZeroInteractions(orderProcessParameterHelper);
        verifyNoMoreInteractions(businessProcessServiceImpl);
    }

    @Test
    public void teststartOrderConsignmentProcess() {
        given(
                businessProcessServiceImpl.createProcess(anyString(), anyString()))
                        .willReturn(orderProcessModel);
        businessProcessServiceImpl.startConsignmentRejectionProcess(orderModel, consignment,
                ConsignmentRejectReason.PICK_FAILED, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verify(orderProcessParameterHelper).setConsignment(orderProcessModel, consignment);
        verify(orderProcessParameterHelper).setConsignmentRejectReason(orderProcessModel,
                ConsignmentRejectReason.PICK_FAILED);
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyNoMoreInteractions(orderProcessParameterHelper);
        verifyNoMoreInteractions(orderProcessModel);
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testStartSendSmsToStoreForOpenOrdersProcess() {
        final SendSmsToStoreData sendSmsToStore = new SendSmsToStoreData();
        willReturn(businessProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());
        willDoNothing().given(businessProcessServiceImpl).startProcess(businessProcessModel);

        businessProcessServiceImpl.startSendSmsToStoreForOpenOrdersProcess(sendSmsToStore);

        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(businessProcessServiceImpl).startProcess(businessProcessModel);
        verify(orderProcessParameterHelper).setSendSmsToStoreData(businessProcessModel, sendSmsToStore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartSendSmsToStoreForOpenOrdersProcessWithNullSendSmsToStoreData() {
        final SendSmsToStoreData sendSmsToStore = null;
        willReturn(businessProcessModel).given(businessProcessServiceImpl)
                .createProcess(anyString(), anyString());
        willDoNothing().given(businessProcessServiceImpl).startProcess(businessProcessModel);

        businessProcessServiceImpl.startSendSmsToStoreForOpenOrdersProcess(sendSmsToStore);

        verifyZeroInteractions(businessProcessServiceImpl);
        verifyZeroInteractions(businessProcessServiceImpl);
        verifyZeroInteractions(orderProcessParameterHelper);
    }

    @Test
    public void testStartGiftCardReverseProcess() {
        final Object giftcardReverseDetails = mock(Object.class);
        final String email = "testemail";
        doReturn(storeFrontProcessModel).when(businessProcessServiceImpl).createProcess(anyString(), anyString());
        businessProcessServiceImpl.startGiftCardReverseProcess(giftcardReverseDetails, email, "receipt", null);
        verify(orderProcessParameterHelper).setGiftCardReversalData(storeFrontProcessModel, giftcardReverseDetails);
        verify(modelService, times(2)).save(storeFrontProcessModel);

    }

    public class CustomerSubscriptionTestData {
        private String customerEmail;
        private String title;
        private String firstName;

        public String getCustomerEmail() {
            return customerEmail;
        }

        public void setCustomerEmail(final String customerEmail) {
            this.customerEmail = customerEmail;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(final String title) {
            this.title = title;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(final String firstName) {
            this.firstName = firstName;
        }
    }

    @Test
    public void testStartSendClickAndCollectPickedupNotificationProcessWhenOrderIsNotNull() {

        businessProcessServiceImpl.startSendClickAndCollectPickedupNotificationProcess(orderModel, true);

        //Verify interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
        verify(orderProcessParameterHelper).setPartnerUpdateRequired(orderProcessModel, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartSendClickAndCollectPickedUpNotificationProcessWhenOrderIsNull() {
        businessProcessServiceImpl.startSendClickAndCollectPickedupNotificationProcess(null, true);
    }

    @Test
    public void testStartSendEmailWithBlackoutProcess() {
        final Object cncNotificationData = new Object();

        businessProcessServiceImpl.startSendEmailWithBlackoutProcess(orderModel, cncNotificationData);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
        verify(orderProcessParameterHelper).setCncNotificationData(orderProcessModel, cncNotificationData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartSendEmailWithBlackoutProcessWithNullCncData() {
        final Object cncNotificationData = null;

        businessProcessServiceImpl.startSendClickAndCollectNotificationProcess(orderModel, cncNotificationData, true);
    }

    @Test
    public void testStartFluentOrderConsignementPickedProcess() {
        final ConsignmentModel consignmentModel = mock(ConsignmentModel.class);

        businessProcessServiceImpl.startFluentOrderConsignmentPickedProcess(orderModel, consignmentModel,
                TgtbusprocConstants.BusinessProcess.FLUENT_CONSIGNMENT_PICKED_PROCESS);

        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(orderProcessParameterHelper).setConsignment(orderProcessModel, consignmentModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartFluentOrderConsignementShippedProcess() {
        final ConsignmentModel consignmentModel = mock(ConsignmentModel.class);
        businessProcessServiceImpl.startFluentOrderConsignmentPickedProcess(orderModel, consignmentModel,
                TgtbusprocConstants.BusinessProcess.FLUENT_CONSIGNMENT_SHIPPED_PROCESS);
        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(orderProcessParameterHelper).setConsignment(orderProcessModel, consignmentModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    @Test
    public void testStartFluentOrderShippedProcess() {
        businessProcessServiceImpl.startFluentOrderShippedProcess(orderModel,
                TgtbusprocConstants.BusinessProcess.FLUENT_ORDER_SHIPPED_PROCESS);
        //Verify required interactions
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    /**
     * This test scenario will throw illegal argument exception when we pass null value for orderModel object while
     * executing fluent pre-order business process.
     */
    @Test
    public void testStartPlaceFluentPreOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startPlaceFluentPreOrderProcess(null, null);
    }

    /**
     * This test scenario will verify required interaction while executing fluent pre-order business process and in this
     * scenario we don't pass payment transaction entry.
     */
    @Test
    public void testStartPlaceFluentPreOrderProcessWithNullPtem() {
        businessProcessServiceImpl.startPlaceFluentPreOrderProcess(orderModel, null);
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyZeroInteractions(orderProcessParameterHelper);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    /**
     * This test scenario will verify required interaction while executing fluent pre-order business process and in this
     * scenario we pass required payment transaction entry.
     */
    @Test
    public void testStartPlaceFluentPreOrderProcess() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        businessProcessServiceImpl.startPlaceFluentPreOrderProcess(orderModel, paymentTransactionEntryModel);
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(orderProcessParameterHelper).setPaymentTransactionEntry(orderProcessModel, paymentTransactionEntryModel);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    /**
     * This test scenario will throw illegal argument exception when we pass null value for orderModel object while
     * executing fluent accept pre-order business process.
     */
    @Test
    public void testStartFluentAcceptPreOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startFluentAcceptPreOrderProcess(null);
    }

    /**
     * This test scenario will verify required interaction while executing fluent pre-order accept business process.
     */
    @Test
    public void testStartFluentAcceptPreOrderProcessWithOrder() {
        businessProcessServiceImpl.startFluentAcceptPreOrderProcess(orderModel);
        verify(businessProcessServiceImpl).startOrderProcess(any(OrderModel.class), anyString(), anyString());
    }

    /**
     * This test scenario will throw illegal argument exception when we pass null value for orderModel object while
     * executing fluent pre-order release business process.
     */
    @Test
    public void testStartFluentReleasePreOrderProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startFluentReleasePreOrderProcess(null);
    }

    /**
     * This test scenario will verify required interactions while executing fluent pre-order release business process.
     */
    @Test
    public void testStartFluentReleasePreOrderProcessWithOrder() {
        businessProcessServiceImpl.startFluentReleasePreOrderProcess(orderModel);
        verify(businessProcessServiceImpl).createProcess(anyString(), anyString());
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyZeroInteractions(orderProcessParameterHelper);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }

    /**
     * This test scenario will throw illegal argument exception when we pass null value for orderModel object while
     * executing fluent pre-order release accept business process.
     */
    @Test
    public void testStartFluentPreOrderReleaseAcceptProcessWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        businessProcessServiceImpl.startFluentPreOrderReleaseAcceptProcess(null);
    }

    /**
     * This test scenario will verify required interactions while executing fluent pre-order release accept business
     * process.
     */
    @Test
    public void testStartFluentPreOrderReleaseAcceptProcessWithOrder() {
        businessProcessServiceImpl.startFluentPreOrderReleaseAcceptProcess(orderModel);
        final String separator = "-";
        final String prefix = TgtbusprocConstants.BusinessProcess.FLUENT_PREORDER_RELEASE_ACCEPT_PROCESS
                + separator + orderModel.getCode() + separator;
        verify(businessProcessServiceImpl).createProcess(startsWith(prefix),
                eq(TgtbusprocConstants.BusinessProcess.FLUENT_PREORDER_RELEASE_ACCEPT_PROCESS));
        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verifyZeroInteractions(orderProcessParameterHelper);
        verify(businessProcessServiceImpl).startProcess(orderProcessModel);
    }
}
