package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;

/**
 * Triggers a notify event to the parent process. This emulates synchronous behaviour
 * between parent-child processes flow.
 */
public abstract class SubProcessEndAction extends AbstractProceduralAction<OrderProcessModel> {

    @Override
    public void executeAction(final OrderProcessModel process) throws Exception {
        getBusinessProcessService().triggerEvent(getEvent(process));
    }

    /**
     * Returns the name of the event to trigger.
     *
     * @param process the business process model
     * @return the name of the event
     */
    public abstract String getEvent(OrderProcessModel process);


    @Override
    public OrderProcessParameterHelper getProcessParameterHelper() {
        return (OrderProcessParameterHelper)super.getProcessParameterHelper();
    }

    /**
     * Returns the business process service to be used for events bubbling.
     *
     * @return the business process service
     */
    public TargetBusinessProcessService getBusinessProcessService() {
        throw new UnsupportedOperationException("This method should not be called on a POJO");
    }

}
