package au.com.target.tgtbusproc.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;


/**
 * 
 * Action that will create and trigger
 * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#ACCEPT_PREORDER_PROCESS}
 * 
 */
public class AcceptPreOrderAction extends AbstractProceduralAction<OrderProcessModel> {

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Pre-orderprocess cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        getTargetBusinessProcessService().startOrderProcess(orderModel,
                TgtbusprocConstants.BusinessProcess.ACCEPT_PREORDER_PROCESS);
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

}
