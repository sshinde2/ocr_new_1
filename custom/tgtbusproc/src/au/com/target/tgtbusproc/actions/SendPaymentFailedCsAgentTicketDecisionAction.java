/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


public class SendPaymentFailedCsAgentTicketDecisionAction extends AbstractAction<OrderProcessModel> {

    private OrderProcessParameterHelper orderProcessParameterHelper;

    public enum Transition
    {
        REJECTED, RETRYEXCEEDED, NOK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.assertNotNull("process cannot be null", process);

        final String reason = orderProcessParameterHelper.getPaymentFailedReason(process);

        if (Transition.RETRYEXCEEDED.toString().equals(reason)) {
            return Transition.RETRYEXCEEDED.toString();
        }
        if (Transition.REJECTED.toString().equals(reason)) {
            return Transition.REJECTED.toString();
        }
        return Transition.NOK.toString();
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

}
