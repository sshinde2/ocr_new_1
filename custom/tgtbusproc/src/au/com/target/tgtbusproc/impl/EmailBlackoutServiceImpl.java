package au.com.target.tgtbusproc.impl;

import java.text.MessageFormat;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.EmailBlackoutService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;



/**
 * Default implementation of {@link EmailBlackoutService}
 */
public class EmailBlackoutServiceImpl implements EmailBlackoutService {

    private static final Logger LOG = Logger.getLogger(EmailBlackoutServiceImpl.class);

    private static final String BLACKOUT_DATE_FORMAT = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
    private static final String TIME_ZONE = "Australia/Melbourne";

    private String emailBlackoutStartTime;
    private String emailBlackoutEndTime;

    @Override
    public boolean isEmailBlackout() {

        final DateTime currentDate = getCurrentDate();
        final DateTime nonBlackoutStartTime = getBlackoutTime(emailBlackoutEndTime);
        final DateTime nonBlackoutEndTime = getBlackoutTime(emailBlackoutStartTime);

        if (nonBlackoutStartTime == null || nonBlackoutEndTime == null) {
            return false;
        }

        if (nonBlackoutStartTime.isAfter(nonBlackoutEndTime)) {
            return false;
        }

        if (currentDate.isAfter(nonBlackoutStartTime) && currentDate.isBefore(nonBlackoutEndTime)) {
            return false;
        }

        return true;
    }


    /**
     * Method to return current date
     * 
     * @return DateTime
     */
    protected DateTime getCurrentDate() {
        return new LocalDate().toDateTimeAtCurrentTime(DateTimeZone.forID(TIME_ZONE));
    }

    /**
     * Converts configured blackout time to DateTime
     * 
     * @param blackoutTime
     * @return dateTime/null
     */
    private DateTime getBlackoutTime(final String blackoutTime) {

        if (validateBlackoutTime(blackoutTime)) {
            final DateTime dateTime = new LocalDate()
                    .toDateTime(DateTimeFormat.forPattern("HH:mm").parseDateTime(blackoutTime)
                            .toMutableDateTime(DateTimeZone.forID(TIME_ZONE)));


            return dateTime;
        }

        LOG.error(SplunkLogFormatter.formatMessage(
                MessageFormat.format("Invalid Email Blackout Time, blackOutTime={0}", blackoutTime),
                TgtutilityConstants.ErrorCode.ERR_EMAIL_BLACKOUT_TIME));

        return null;
    }

    /**
     * Validates the blackout time
     * 
     * @param blackoutTime
     * @return true/false
     */
    private boolean validateBlackoutTime(final String blackoutTime) {
        if (StringUtils.isNotBlank(blackoutTime)) {
            final Pattern pattern = Pattern.compile(BLACKOUT_DATE_FORMAT);
            return pattern.matcher(blackoutTime).matches();
        }
        return false;
    }

    /**
     * @param emailBlackoutStartTime
     *            the emialBlackoutStartTime to set
     */
    @Required
    public void setEmailBlackoutStartTime(final String emailBlackoutStartTime) {
        this.emailBlackoutStartTime = StringUtils.trim(emailBlackoutStartTime);
    }

    /**
     * @param emailBlackoutEndTime
     *            the emialBlackoutEndTime to set
     */
    @Required
    public void setEmailBlackoutEndTime(final String emailBlackoutEndTime) {
        this.emailBlackoutEndTime = StringUtils.trim(emailBlackoutEndTime);
    }


}
