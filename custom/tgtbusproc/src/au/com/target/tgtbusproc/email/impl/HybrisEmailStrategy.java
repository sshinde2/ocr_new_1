/**
 * 
 */
package au.com.target.tgtbusproc.email.impl;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.email.EmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendHybrisEmailService;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;


/**
 * Generate an email using hybris.
 * 
 * Note: this is hard to integration test via yunit since it needs all the template content set up in tgtwebcore.<br/>
 * When testing via Groovy console, then for an unknown reason DefaultEmailContextFactory is unable to lookup the
 * context bean using getBeansOfType.<br/>
 * I have tested via creating and running a process in HMC under the 'Business Processes' section.
 */
public class HybrisEmailStrategy implements BusinessProcessEmailStrategy {

    private CMSEmailPageService cmsEmailPageService;
    private ProcessContextResolutionStrategy contextResolutionStrategy;
    private EmailGenerationService emailGenerationService;
    private SendHybrisEmailService sendHybrisEmailService;


    @Override
    public void sendEmail(final BusinessProcessModel process, final String frontendTemplateName)
            throws BusinessProcessEmailException {

        Assert.notNull(process, "Process cannot be null");

        final EmailMessageModel email = generateEmailFromVelocityTemplate(process, frontendTemplateName);

        try {
            sendHybrisEmailService.sendEmail(email);
        }
        catch (final EmailServiceException e) {
            throw new BusinessProcessEmailException("Could not send email " + frontendTemplateName + " for process "
                    + process.getCode(), e);
        }
    }


    /**
     * Generate a hybris EmailMessageModel for given process and velocity template.<br/>
     * This code is ported from GenerateEmailAction in AcceleratorServices ext.
     * 
     * @param process
     * @param frontendTemplateName
     * @return EmailMessageModel
     * @throws BusinessProcessEmailException
     */
    protected EmailMessageModel generateEmailFromVelocityTemplate(final BusinessProcessModel process,
            final String frontendTemplateName)
            throws BusinessProcessEmailException {

        Assert.notNull(process, "Process cannot be null");

        // Get contentCatalogVersion
        final CatalogVersionModel contentCatalogVersion = contextResolutionStrategy.getContentCatalogVersion(
                process);
        if (contentCatalogVersion == null)
        {
            throw new BusinessProcessEmailException("Error getting contentCatalogVersion for process "
                    + process.getCode());
        }

        // Retrieve email page model for template
        final EmailPageModel emailPageModel = cmsEmailPageService.getEmailPageForFrontendTemplate(frontendTemplateName,
                contentCatalogVersion);
        if (emailPageModel == null)
        {
            throw new BusinessProcessEmailException("Could not retrieve email page model for " + frontendTemplateName
                    + " and process " + process.getCode());
        }

        // Generate email message
        final EmailMessageModel email = getEmailGenerationService().generate(process, emailPageModel);
        if (email == null)
        {
            throw new BusinessProcessEmailException("Could not generate email message for " + frontendTemplateName
                    + " and process " + process.getCode());
        }

        return email;
    }


    /**
     * @return the emailGenerationService
     */
    public EmailGenerationService getEmailGenerationService() {
        return emailGenerationService;
    }

    /**
     * @param emailGenerationService
     *            the emailGenerationService to set
     */
    @Required
    public void setEmailGenerationService(final EmailGenerationService emailGenerationService) {
        this.emailGenerationService = emailGenerationService;
    }

    /**
     * @param cmsEmailPageService
     *            the cmsEmailPageService to set
     */
    @Required
    public void setCmsEmailPageService(final CMSEmailPageService cmsEmailPageService) {
        this.cmsEmailPageService = cmsEmailPageService;
    }

    /**
     * @param contextResolutionStrategy
     *            the contextResolutionStrategy to set
     */
    @Required
    public void setContextResolutionStrategy(final ProcessContextResolutionStrategy contextResolutionStrategy) {
        this.contextResolutionStrategy = contextResolutionStrategy;
    }


    /**
     * @param sendHybrisEmailService
     *            the sendHybrisEmailService to set
     */
    @Required
    public void setSendHybrisEmailService(final SendHybrisEmailService sendHybrisEmailService) {
        this.sendHybrisEmailService = sendHybrisEmailService;
    }



}
