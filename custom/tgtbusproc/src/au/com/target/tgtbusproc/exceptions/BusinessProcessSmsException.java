/**
 * 
 */
package au.com.target.tgtbusproc.exceptions;

/**
 * @author bhuang3
 * 
 */
public class BusinessProcessSmsException extends Exception {


    /**
     * @param message
     */
    public BusinessProcessSmsException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public BusinessProcessSmsException(final String message, final Throwable cause) {
        super(message, cause);
    }


}
