/**
 * 
 */
package au.com.target.tgtbusproc.event.listeners;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.event.TargetSubmitPartnerOrderEvent;



/**
 * Listener for Partner order submits.
 * 
 * @author jjayawa1
 * 
 */
public class SubmitPartnerOrderEventListener extends AbstractEventListener<TargetSubmitPartnerOrderEvent> {

    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.event.impl.AbstractEventListener#onEvent(de.hybris.platform.servicelayer.event.events.AbstractEvent)
     */
    @Override
    protected void onEvent(final TargetSubmitPartnerOrderEvent event) {
        getTargetBusinessProcessService().startOrderAcceptProcess(event.getOrder());
    }

}
