package au.com.target.tgtfacades.sitemap.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.MPropertyMap;
import com.endeca.navigation.Navigation;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetSiteMapQueryBuilder;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;


/**
 * @author pvarghe2
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSiteMapFacadeImplTest {

    private static final String PRODUCT_1 = "P1000_black";

    private static final String PRODUCT_2 = "P1000_blue";

    private static final String PRODUCT_NAME1 = "Product Name1";

    @InjectMocks
    private final TargetSiteMapFacadeImpl targetSiteMapFacadeImpl = new TargetSiteMapFacadeImpl();

    @Mock
    private ENEQueryResults queryResult;

    @Mock
    private Navigation navigation;

    @Mock
    private AggrERecList aggrERecList;

    @Mock
    private AggrERec aggrERec1;

    @Mock
    private AggrERec aggrERec2;

    @Mock
    private ERec eRec;

    @Mock
    private AssocDimLocationsList assocDimLocationsList;

    @Mock
    private AssocDimLocations assocDimLocations;

    @Mock
    private DimVal dimVal;

    @Mock
    private DimLocation dimLocation;

    @Mock
    private TargetSiteMapQueryBuilder targetSiteMapQueryBuilder;

    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;


    @Test
    public void testGetSiteMapColourVariantUrlWithNullResult() throws TargetEndecaException, ENEQueryException {
        given(targetSiteMapQueryBuilder.getQueryResults(160, 1, 250)).willReturn(queryResult);
        final List<String> result = targetSiteMapFacadeImpl.getSiteMapColourVariantUrl(160, 1, 250);
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetSiteMapColourVariantUrlWithEmptyResult() throws TargetEndecaException, ENEQueryException {
        given(targetSiteMapQueryBuilder.getQueryResults(160, 1, 250)).willReturn(queryResult);
        given(queryResult.getNavigation()).willReturn(null);
        final List<String> result = targetSiteMapFacadeImpl.getSiteMapColourVariantUrl(160, 1, 250);
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetSiteMapColourVariantUrlWithNullERecs() throws TargetEndecaException, ENEQueryException {
        given(targetSiteMapQueryBuilder.getQueryResults(160, 1, 250)).willReturn(queryResult);
        given(queryResult.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(null);
        final List<String> result = targetSiteMapFacadeImpl.getSiteMapColourVariantUrl(160, 1, 250);
        verifyZeroInteractions(targetProductDataUrlResolver);
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetSiteMapColourVariantUrlWithOneERecs() throws TargetEndecaException, ENEQueryException {
        given(targetSiteMapQueryBuilder.getQueryResults(160, 1, 250)).willReturn(queryResult);
        given(queryResult.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(aggrERecList);
        given(aggrERecList.iterator()).willReturn(createIterator(aggrERec1));
        given(aggrERec1.getRepresentative()).willReturn(eRec);
        given(eRec.getDimValues()).willReturn(assocDimLocationsList);
        given(assocDimLocationsList.iterator()).willReturn(createIterator(assocDimLocations));
        given(assocDimLocations.getDimRoot()).willReturn(dimVal);
        given(assocDimLocations.iterator()).willReturn(createIterator(dimLocation));
        given(assocDimLocations.get(0)).willReturn(dimLocation);
        given(dimVal.getDimensionName()).willReturn(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME);
        given(dimLocation.getDimValue()).willReturn(dimVal);
        given(dimVal.getName()).willReturn(PRODUCT_NAME1);
        given(aggrERec1.getRepresentative()).willReturn(eRec);
        given(targetProductDataUrlResolver.resolve(PRODUCT_NAME1, PRODUCT_1)).willReturn("/p/productName/Code");
        final MPropertyMap properties = new MPropertyMap();
        properties.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, PRODUCT_1);
        properties.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG,
                EndecaConstants.ENDECA_BOOLEAN_VALUE);
        given(eRec.getProperties()).willReturn(properties);
        final List<String> result = targetSiteMapFacadeImpl.getSiteMapColourVariantUrl(160, 1, 250);
        verify(targetProductDataUrlResolver).resolve(PRODUCT_NAME1, PRODUCT_1);
        assertThat(result).hasSize(1);
        assertThat(result.get(0)).isEqualTo("/p/productName/Code");
    }

    @Test
    public void testGetSiteMapColourVariantUrlWithMultipleERecs() throws TargetEndecaException, ENEQueryException {
        given(targetSiteMapQueryBuilder.getQueryResults(160, 1, 250)).willReturn(queryResult);
        given(queryResult.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(aggrERecList);
        given(aggrERecList.iterator()).willReturn(createIterator(aggrERec1, aggrERec2));
        given(aggrERec1.getRepresentative()).willReturn(eRec);
        given(aggrERec2.getRepresentative()).willReturn(eRec);
        given(eRec.getDimValues()).willReturn(assocDimLocationsList);
        given(assocDimLocationsList.iterator()).willReturn(createIterator(assocDimLocations));
        given(assocDimLocations.getDimRoot()).willReturn(dimVal);
        given(assocDimLocations.iterator()).willReturn(createIterator(dimLocation));
        given(assocDimLocations.get(0)).willReturn(dimLocation);
        given(dimVal.getDimensionName()).willReturn(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME);
        given(dimLocation.getDimValue()).willReturn(dimVal);
        given(dimVal.getName()).willReturn(PRODUCT_NAME1);
        given(aggrERec1.getRepresentative()).willReturn(eRec);
        given(aggrERec2.getRepresentative()).willReturn(eRec);
        final MPropertyMap properties = new MPropertyMap();
        properties.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, PRODUCT_1);
        properties.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG,
                EndecaConstants.ENDECA_BOOLEAN_VALUE);
        given(eRec.getProperties()).willReturn(properties);
        targetSiteMapFacadeImpl.getSiteMapColourVariantUrl(160, 1, 250);
        verify(targetProductDataUrlResolver, times(2)).resolve(anyString(), anyString());
    }

    @Test
    public void testGetSiteMapColourVariantUrlWithOutOfStockERecs() throws TargetEndecaException, ENEQueryException {
        final AggrERec aggrERec3 = mock(AggrERec.class);
        final ERec eRec1 = mock(ERec.class);
        given(targetSiteMapQueryBuilder.getQueryResults(160, 1, 250)).willReturn(queryResult);
        given(queryResult.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(aggrERecList);
        given(aggrERecList.iterator()).willReturn(createIterator(aggrERec1, aggrERec2, aggrERec3));
        given(aggrERec1.getRepresentative()).willReturn(eRec);
        given(aggrERec2.getRepresentative()).willReturn(eRec1);
        given(aggrERec2.getRepresentative()).willReturn(eRec);
        given(eRec.getDimValues()).willReturn(assocDimLocationsList);
        given(eRec1.getDimValues()).willReturn(assocDimLocationsList);
        given(assocDimLocationsList.iterator()).willReturn(createIterator(assocDimLocations));
        given(assocDimLocations.getDimRoot()).willReturn(dimVal);
        given(assocDimLocations.iterator()).willReturn(createIterator(dimLocation));
        given(assocDimLocations.get(0)).willReturn(dimLocation);
        given(dimVal.getDimensionName()).willReturn(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME);
        given(dimLocation.getDimValue()).willReturn(dimVal);
        given(dimVal.getName()).willReturn(PRODUCT_NAME1);
        given(aggrERec1.getRepresentative()).willReturn(eRec);
        given(aggrERec2.getRepresentative()).willReturn(eRec1);
        given(aggrERec3.getRepresentative()).willReturn(eRec);
        given(targetProductDataUrlResolver.resolve(PRODUCT_NAME1, PRODUCT_1)).willReturn("/p/productName/Code");
        final MPropertyMap properties = new MPropertyMap();
        properties.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, PRODUCT_1);
        properties.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG,
                EndecaConstants.ENDECA_BOOLEAN_VALUE);
        final MPropertyMap properties1 = new MPropertyMap();
        properties1.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, PRODUCT_1);
        properties1.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG, "0");
        given(eRec.getProperties()).willReturn(properties);
        given(eRec1.getProperties()).willReturn(properties1);
        final List<String> result = targetSiteMapFacadeImpl.getSiteMapColourVariantUrl(160, 1, 250);
        verify(targetProductDataUrlResolver).resolve(PRODUCT_NAME1, PRODUCT_1);
        assertThat(result).hasSize(1);
        assertThat(result.get(0)).isEqualTo("/p/productName/Code");
        verify(targetProductDataUrlResolver, never()).resolve(PRODUCT_NAME1, PRODUCT_2);
    }

    private Iterator createIterator(final Object... objects) {
        final List list = new ArrayList();
        for (final Object object : objects) {
            list.add(object);
        }
        return list.iterator();
    }
}
