/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetDealWithRewardResultModel;

import java.util.Collections;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;


/**
 * @author rahul
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPromotionResultPopulatorTest {

    @InjectMocks
    private final TargetPromotionResultPopulator targetPromotionResultPopulator = new TargetPromotionResultPopulator();

    @Mock
    private PromotionResultModel source;

    private final PromotionResultData target = new PromotionResultData();

    @Test
    public void testConvertTMD() {
        final TMDiscountProductPromotionModel productPromotionModel = mock(TMDiscountProductPromotionModel.class);

        given(source.getConsumedEntries()).willReturn(Collections.EMPTY_LIST);
        given(source.getCustom()).willReturn(String.valueOf(1));
        given(source.getPromotion()).willReturn(productPromotionModel);

        targetPromotionResultPopulator.populate(source, target);

        Assert.assertEquals(String.valueOf(1), target.getOrderEntryNumber().toString());
    }

    @Test
    public void testConvertNonTMD() {
        final ProductPromotionModel productPromotionModel = mock(ProductPromotionModel.class);
        final PromotionOrderEntryConsumedModel consumedModel = mock(PromotionOrderEntryConsumedModel.class);

        given(source.getConsumedEntries()).willReturn(Collections.singleton(consumedModel));
        given(source.getPromotion()).willReturn(productPromotionModel);

        targetPromotionResultPopulator.populate(source, target);

        Assert.assertNull(target.getOrderEntryNumber());
    }

    @Test
    public void testPopulateTargetPromotionDataWithCanhaveMoreRewards() {
        final TargetDealWithRewardResultModel sourceModel = mock(TargetDealWithRewardResultModel.class);
        final PromotionOrderEntryConsumedModel consumedModel = mock(PromotionOrderEntryConsumedModel.class);
        final ProductPromotionModel productPromotionModel = mock(ProductPromotionModel.class);

        given(sourceModel.getConsumedEntries()).willReturn(Collections.singleton(consumedModel));
        given(sourceModel.getMaxNumberofRewards()).willReturn(Integer.valueOf(2));
        given(sourceModel.getNumberofRewardsSofar()).willReturn(Integer.valueOf(1));
        given(sourceModel.getPromotion()).willReturn(productPromotionModel);

        targetPromotionResultPopulator.populate(sourceModel, target);
        Assert.assertTrue(target.isCanHaveMoreRewards());
    }

    @Test
    public void testPopulateTargetPromotionDataWithoutCanhaveMoreRewards() {
        final TargetDealWithRewardResultModel sourceModel = mock(TargetDealWithRewardResultModel.class);
        final PromotionOrderEntryConsumedModel consumedModel = mock(PromotionOrderEntryConsumedModel.class);
        final ProductPromotionModel productPromotionModel = mock(ProductPromotionModel.class);

        given(sourceModel.getConsumedEntries()).willReturn(Collections.singleton(consumedModel));
        given(sourceModel.getMaxNumberofRewards()).willReturn(Integer.valueOf(2));
        given(sourceModel.getNumberofRewardsSofar()).willReturn(Integer.valueOf(2));
        given(sourceModel.getPromotion()).willReturn(productPromotionModel);

        targetPromotionResultPopulator.populate(sourceModel, target);
        Assert.assertFalse(target.isCanHaveMoreRewards());
    }
}
