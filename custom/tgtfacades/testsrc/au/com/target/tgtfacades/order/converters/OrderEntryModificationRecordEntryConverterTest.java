/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.OrderModificationData;


/**
 * @author rmcalave
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderEntryModificationRecordEntryConverterTest {
    @InjectMocks
    private final OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter = new OrderEntryModificationRecordEntryConverter();

    @Mock(name = "orderEntryConverter")
    private Converter<AbstractOrderEntryModel, OrderEntryData> mockOrderEntryConverter;

    @Mock
    private PriceDataFactory mockPriceDataFactory;


    @Test
    public void testPopulate() {
        final Date creationTime = new Date();
        final BigDecimal basePrice = new BigDecimal("23");
        final PriceData basePriceData = new PriceData();
        basePriceData.setValue(basePrice);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);

        final OrderEntryModificationRecordEntryModel mockOrderEntryModificationRecordEntryModel = mock(
                OrderEntryModificationRecordEntryModel.class);
        given(mockOrderEntryModificationRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryModificationRecordEntryModel.getCreationtime()).willReturn(creationTime);

        final OrderModificationData orderModificationData = new OrderModificationData();
        // Add values that would be populated by the orderEntryConverter
        orderModificationData.setBasePrice(basePriceData);

        orderEntryModificationRecordEntryConverter.populate(mockOrderEntryModificationRecordEntryModel,
                orderModificationData);

        assertEquals(creationTime, orderModificationData.getDate());

        assertNull(orderModificationData.getReason());
        assertNull(orderModificationData.getQuantity());
        assertNull(orderModificationData.getTotalPrice());

        verify(mockOrderEntryConverter).convert(mockOrderEntryModel, orderModificationData);
    }

    @Test
    public void testPopulateWithReturnRecord() {
        final String orderCode = "12345678";
        final Date creationTime = new Date();
        final BigDecimal basePrice = new BigDecimal("23");
        final PriceData basePriceData = new PriceData();
        basePriceData.setValue(basePrice);
        final Long quantity = Long.valueOf("3");
        final BigDecimal totalPrice = new BigDecimal("69");
        final String currencyIso = "AU";

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIso);

        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getCode()).willReturn(orderCode);
        given(mockOrderModel.getCurrency()).willReturn(mockCurrency);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);
        given(mockOrderEntryModel.getOrder()).willReturn(mockOrderModel);

        final OrderEntryReturnRecordEntryModel mockOrderEntryReturnRecordEntryModel = mock(
                OrderEntryReturnRecordEntryModel.class);
        given(mockOrderEntryReturnRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryReturnRecordEntryModel.getCreationtime()).willReturn(creationTime);
        given(mockOrderEntryReturnRecordEntryModel.getReturnedQuantity()).willReturn(quantity);

        final PriceData totalPriceData = new PriceData();
        given(mockPriceDataFactory.create(PriceDataType.BUY, totalPrice, currencyIso)).willReturn(totalPriceData);

        final OrderModificationData orderModificationData = new OrderModificationData();
        // Add values that would be populated by the orderEntryConverter
        orderModificationData.setBasePrice(basePriceData);


        orderEntryModificationRecordEntryConverter
                .populate(mockOrderEntryReturnRecordEntryModel, orderModificationData);


        assertEquals(creationTime, orderModificationData.getDate());
        assertEquals(quantity, orderModificationData.getQuantity());
        assertEquals(totalPriceData, orderModificationData.getTotalPrice());

        assertNull(orderModificationData.getReason());

        verify(mockOrderEntryConverter).convert(mockOrderEntryModel, orderModificationData);
    }

    @Test
    public void testPopulateWithCancelRecordAndCancelReason() {
        final Date creationTime = new Date();
        final BigDecimal basePrice = new BigDecimal("23");
        final PriceData basePriceData = new PriceData();
        basePriceData.setValue(basePrice);
        final Integer quantity = Integer.valueOf(3);
        final CancelReason cancelReason = CancelReason.CUSTOMERREQUEST;
        final BigDecimal totalPrice = new BigDecimal("69");
        final String currencyIso = "AU";

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn("AU");

        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getCurrency()).willReturn(mockCurrency);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);
        given(mockOrderEntryModel.getOrder()).willReturn(mockOrderModel);

        final OrderEntryCancelRecordEntryModel mockOrderEntryCancelRecordEntryModel = mock(
                OrderEntryCancelRecordEntryModel.class);
        given(mockOrderEntryCancelRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryCancelRecordEntryModel.getCreationtime()).willReturn(creationTime);
        given(mockOrderEntryCancelRecordEntryModel.getCancelledQuantity()).willReturn(quantity);
        given(mockOrderEntryCancelRecordEntryModel.getCancelReason()).willReturn(cancelReason);

        final PriceData totalPriceData = new PriceData();
        given(mockPriceDataFactory.create(PriceDataType.BUY, totalPrice, currencyIso)).willReturn(totalPriceData);

        final OrderModificationData orderModificationData = new OrderModificationData();
        // Add values that would be populated by the orderEntryConverter
        orderModificationData.setBasePrice(basePriceData);

        orderEntryModificationRecordEntryConverter.populate(mockOrderEntryCancelRecordEntryModel,
                orderModificationData);

        assertEquals(creationTime, orderModificationData.getDate());
        assertEquals(cancelReason.getCode(), orderModificationData.getReason());
        assertEquals(Long.valueOf(quantity.longValue()), orderModificationData.getQuantity());
        assertEquals(totalPriceData, orderModificationData.getTotalPrice());

        verify(mockOrderEntryConverter).convert(mockOrderEntryModel, orderModificationData);
    }

    @Test
    public void testPopulateWithCancelRecordAndNoCancelReason() {
        final Date creationTime = new Date();
        final BigDecimal basePrice = new BigDecimal("23");
        final PriceData basePriceData = new PriceData();
        basePriceData.setValue(basePrice);
        final Integer quantity = Integer.valueOf(3);
        final BigDecimal totalPrice = new BigDecimal("69");
        final String currencyIso = "AU";

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn("AU");

        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getCurrency()).willReturn(mockCurrency);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);
        given(mockOrderEntryModel.getOrder()).willReturn(mockOrderModel);

        final OrderEntryCancelRecordEntryModel mockOrderEntryCancelRecordEntryModel = mock(
                OrderEntryCancelRecordEntryModel.class);
        given(mockOrderEntryCancelRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryCancelRecordEntryModel.getCreationtime()).willReturn(creationTime);
        given(mockOrderEntryCancelRecordEntryModel.getCancelledQuantity()).willReturn(quantity);

        final PriceData totalPriceData = new PriceData();
        given(mockPriceDataFactory.create(PriceDataType.BUY, totalPrice, currencyIso)).willReturn(totalPriceData);

        final OrderModificationData orderModificationData = new OrderModificationData();
        // Add values that would be populated by the orderEntryConverter
        orderModificationData.setBasePrice(basePriceData);

        orderEntryModificationRecordEntryConverter.populate(mockOrderEntryCancelRecordEntryModel,
                orderModificationData);

        assertEquals(creationTime, orderModificationData.getDate());
        assertEquals(Long.valueOf(quantity.longValue()), orderModificationData.getQuantity());
        assertEquals(totalPriceData, orderModificationData.getTotalPrice());

        assertNull(orderModificationData.getReason());

        verify(mockOrderEntryConverter).convert(mockOrderEntryModel, orderModificationData);
    }
}
