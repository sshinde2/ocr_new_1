/**
 *
 */
package au.com.target.tgtfacades.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.helper.ClickAndCollectOrderDetailsHelper;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.enums.TargetUpdateCardErrorType;
import au.com.target.tgtfacades.order.exception.TargetUpdateCardException;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;


/**
 * @author Benoit VanalderWeireldt
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderFacadeImplTest {

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private UserService userService;

    @Mock
    private PaymentModeService paymentModeService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private BaseStoreService baseStoreService;

    @Mock
    private BaseStoreModel baseStore;

    @Mock
    private CustomerAccountService customerAccountService;

    @Mock
    private TargetCustomerModel customerModel;

    @InjectMocks
    private final TargetOrderFacadeImpl targetOrderFacadeImpl = new TargetOrderFacadeImpl();

    @Mock
    private OrderModel mockOrderModel;

    @Mock
    private PaymentMethodStrategy paymentMethodStrategy;

    @Mock
    private TargetIpgPaymentMethod ipgPaymentMethod;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetCreateSubscriptionResult targetCreateSubscriptionResult;

    @Mock
    private CheckoutCustomerStrategy checkoutCustomerStrategy;

    @Mock
    private Converter<OrderModel, OrderData> converter;

    @Mock
    private ClickAndCollectOrderDetailsHelper clickAndCollectOrderDetailsHelperMock;

    @Test(expected = UnknownIdentifierException.class)
    public void testGetOrderDetailsForCode() {
        targetOrderFacadeImpl.getOrderDetailsForCode(null);
    }

    @Test
    public void testLinkOrderToCustomerIfGuestOrderEmailNotMatchesLoggedInUserEmail() {

        final UserModel guestUserModel = new UserModel();
        guestUserModel.setUid("12342332|guestUser@test.com");

        final OrderModel orderModel = new OrderModel();
        orderModel.setCode("1234");
        orderModel.setUser(guestUserModel);

        final UserModel currentUserModel = new UserModel();
        currentUserModel.setUid("registeredUser@test.com");

        given(targetOrderService.findOrderModelForOrderId("1234")).willReturn(orderModel);
        given(userService.getCurrentUser()).willReturn(currentUserModel);

        targetOrderFacadeImpl.linkOrderToCustomer("1234");
        verify(targetOrderService, never()).updateOwner(orderModel, currentUserModel);
    }

    @Test
    public void testLinkOrderToCustomerIfGuestOrderEmailMatchesLoggedInUserEmail() {

        final UserModel guestUserModel = new UserModel();
        guestUserModel.setUid("12342332|linkoder@test.com");

        final OrderModel orderModel = new OrderModel();
        orderModel.setCode("1234");
        orderModel.setUser(guestUserModel);

        final UserModel currentUserModel = new UserModel();
        currentUserModel.setUid("linkoder@test.com");

        given(targetOrderService.findOrderModelForOrderId("1234")).willReturn(orderModel);
        given(userService.getCurrentUser()).willReturn(currentUserModel);

        targetOrderFacadeImpl.linkOrderToCustomer("1234");
        verify(targetOrderService).updateOwner(orderModel, currentUserModel);

    }

    @Test
    public void testCreateIpgUrl() throws TargetUpdateCardException {
        final OrderModel orderModel = mock(OrderModel.class);
        final String orderId = "o1234";
        final String cancelUrl = "cancelUrl";
        final String returnUrl = "returnUrl";
        final String uniqueKey = "123456";
        final String demoUrl = "https://demo.ipgpayment.com";
        final Configuration configuration = mock(Configuration.class);
        final HostedSessionTokenRequest request = mock(HostedSessionTokenRequest.class);
        final String sst = "Session-Token";

        given(orderModel.getStatus()).willReturn(OrderStatus.PARKED);
        given(targetOrderService.findOrderModelForOrderId(orderId)).willReturn(orderModel);
        given(orderModel.getUniqueKeyForPayment()).willReturn(uniqueKey);
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getString(TgtFacadesConstants.IPG_BASE_URL)).willReturn(demoUrl);

        given(targetPaymentService.getHostedSessionToken(request)).willReturn(targetCreateSubscriptionResult);
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn(sst);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore)).willReturn(orderModel);

        given(targetPaymentService.createUpdateCardHostedSessionTokenRequest(orderModel,
                paymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG), cancelUrl, returnUrl,
                PaymentCaptureType.PLACEORDER, uniqueKey)).willReturn(request);
        final String ipgUrl = targetOrderFacadeImpl.createIpgUrl(orderId, cancelUrl, returnUrl);
        assertThat(ipgUrl).isNotNull();
        assertThat(ipgUrl).isEqualTo(demoUrl + "?sessionId=" + uniqueKey + "&SST=" + sst);
    }

    @Test(expected = AdapterException.class)
    public void testCreateIpgUrlWithAdapterException() throws TargetUpdateCardException {
        final String orderId = "o1234";
        final String uniqueKey = "123456";
        final OrderModel orderModel = mock(OrderModel.class);
        final HostedSessionTokenRequest request = mock(HostedSessionTokenRequest.class);
        given(orderModel.getStatus()).willReturn(OrderStatus.PARKED);
        given(orderModel.getUniqueKeyForPayment()).willReturn(uniqueKey);

        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore)).willReturn(orderModel);

        given(targetPaymentService
                .createUpdateCardHostedSessionTokenRequest(orderModel,
                        paymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG), "", "",
                        PaymentCaptureType.PLACEORDER,
                        uniqueKey)).willReturn(request);
        given(targetPaymentService.getHostedSessionToken(request)).willThrow(new AdapterException());
        targetOrderFacadeImpl.createIpgUrl(orderId, "", "");
    }

    @Test(expected = TargetUpdateCardException.class)
    public void testCreateIpgUrlWithTargetUpdateCardExceptionAndCreatedStatus() throws TargetUpdateCardException {
        final String orderId = "o1234";
        final String uniqueKey = "123456";
        final OrderModel orderModel = mock(OrderModel.class);
        final HostedSessionTokenRequest request = mock(HostedSessionTokenRequest.class);
        given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);

        given(orderModel.getUniqueKeyForPayment()).willReturn(uniqueKey);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore)).willReturn(orderModel);

        given(targetPaymentService
                .createUpdateCardHostedSessionTokenRequest(orderModel,
                        paymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG), "", "",
                        PaymentCaptureType.PLACEORDER,
                        uniqueKey)).willReturn(request);
        targetOrderFacadeImpl.createIpgUrl(orderId, "", "");
    }

    @Test
    public void testCreateIpgUrlWithTargetUpdateCardExceptionAndCancelledStatus() throws TargetUpdateCardException {
        final String orderId = "o1234";
        final String uniqueKey = "123456";
        final OrderModel orderModel = mock(OrderModel.class);
        final HostedSessionTokenRequest request = mock(HostedSessionTokenRequest.class);
        given(orderModel.getStatus()).willReturn(OrderStatus.CANCELLED);

        given(orderModel.getUniqueKeyForPayment()).willReturn(uniqueKey);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore)).willReturn(orderModel);

        given(targetOrderService.findOrderModelForOrderId(orderId)).willReturn(orderModel);
        given(targetPaymentService
                .createUpdateCardHostedSessionTokenRequest(orderModel,
                        paymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG), "", "",
                        PaymentCaptureType.PLACEORDER,
                        uniqueKey)).willReturn(request);
        try {
            targetOrderFacadeImpl.createIpgUrl(orderId, "", "");
        }
        catch (final TargetUpdateCardException e) {
            assertThat(e.getErrorType()).isEqualTo(TargetUpdateCardErrorType.INVALID_ORDER_STATE);
        }
    }

    @Test
    public void testCreateIpgUrlWithModelException() throws TargetUpdateCardException {
        final String orderId = "o1234";
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore))
                .willThrow(new ModelNotFoundException("SomeError"));
        try {
            targetOrderFacadeImpl.createIpgUrl(orderId, "", "");
        }
        catch (final TargetUpdateCardException e) {
            assertThat(e.getErrorType()).isEqualTo(TargetUpdateCardErrorType.INVALID_ORDER);
        }
    }

    @Test
    public void testCreateIpgUrlWithAmbiguousIdentifierException() throws TargetUpdateCardException {
        final String orderId = "o1234";
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore))
                .willThrow(new AmbiguousIdentifierException("SomeError"));
        try {
            targetOrderFacadeImpl.createIpgUrl(orderId, "", "");
        }
        catch (final TargetUpdateCardException e) {
            assertThat(e.getErrorType()).isEqualTo(TargetUpdateCardErrorType.INVALID_ORDER);
        }
    }

    @Test
    public void testIsUpdateCreditCardDetailsSuccessfullIpgCustomerAcccountServiceThrowsException() {
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, "OrderId", baseStore))
                .willThrow(new ModelNotFoundException("Model Not Found"));
        final boolean result = targetOrderFacadeImpl.isUpdateCreditCardDetailsSuccessful("OrderId", "ipgToken");
        assertThat(result).isFalse();
        verifyZeroInteractions(paymentMethodStrategy);
        verifyZeroInteractions(targetPaymentService);
    }

    @Test
    public void testIsUpdateCreditCardDetailsSuccessfullNonPreOrder() {

        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, "OrderId", baseStore)).willReturn(mockOrderModel);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        final boolean result = targetOrderFacadeImpl.isUpdateCreditCardDetailsSuccessful("OrderId", "ipgToken");
        assertThat(result).isFalse();
        verifyZeroInteractions(paymentMethodStrategy);
        verifyZeroInteractions(targetPaymentService);

    }

    @Test
    public void testIsUpdateCreditCardDetailsSuccessfullPreOrder() {

        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, "OrderId", baseStore)).willReturn(mockOrderModel);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.PARKED);
        final IpgCreditCardPaymentInfoModel creditCardPaymentInfo = mock(IpgCreditCardPaymentInfoModel.class);
        given(mockOrderModel.getPaymentInfo()).willReturn(creditCardPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(creditCardPaymentInfo)).willReturn(ipgPaymentMethod);
        final ArgumentCaptor<TargetQueryTransactionDetailsRequest> request = ArgumentCaptor
                .forClass(TargetQueryTransactionDetailsRequest.class);
        final TargetQueryTransactionDetailsResult queryTxnResult = mock(TargetQueryTransactionDetailsResult.class);
        given(ipgPaymentMethod.queryTransactionDetails(request.capture())).willReturn(queryTxnResult);

        final boolean result = targetOrderFacadeImpl.isUpdateCreditCardDetailsSuccessful("OrderId", "ipgToken");

        assertThat(result).isTrue();
        verify(targetPaymentService).createTransactionWithQueryResult(mockOrderModel, queryTxnResult,
                PaymentTransactionType.DEFERRED);
        verifyZeroInteractions(modelService);

    }

    @Test
    public void testIsUpdateCreditCardDetailsSuccessfullIpgResultCancel() {

        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, "OrderId", baseStore)).willReturn(mockOrderModel);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.PARKED);
        final IpgCreditCardPaymentInfoModel creditCardPaymentInfo = mock(IpgCreditCardPaymentInfoModel.class);
        given(mockOrderModel.getPaymentInfo()).willReturn(creditCardPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(creditCardPaymentInfo)).willReturn(ipgPaymentMethod);
        final ArgumentCaptor<TargetQueryTransactionDetailsRequest> request = ArgumentCaptor
                .forClass(TargetQueryTransactionDetailsRequest.class);
        final TargetQueryTransactionDetailsResult queryTxnResult = mock(TargetQueryTransactionDetailsResult.class);
        willReturn(Boolean.TRUE).given(queryTxnResult).isCancel();
        given(ipgPaymentMethod.queryTransactionDetails(request.capture())).willReturn(queryTxnResult);

        final boolean result = targetOrderFacadeImpl.isUpdateCreditCardDetailsSuccessful("OrderId", "ipgToken");

        assertThat(result).isFalse();
        verifyZeroInteractions(targetPaymentService);
        verifyZeroInteractions(modelService);

    }

    @Test
    public void testIsUpdateCreditCardDetailsSuccessfullPreOrderWhenDeferredTransactionExists() {

        given(userService.getCurrentUser()).willReturn(customerModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(customerAccountService.getOrderForCode(customerModel, "OrderId", baseStore)).willReturn(mockOrderModel);
        given(targetOrderService.findOrderModelForOrderId("OrderId")).willReturn(mockOrderModel);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.PARKED);
        final IpgCreditCardPaymentInfoModel creditCardPaymentInfo = mock(IpgCreditCardPaymentInfoModel.class);
        given(mockOrderModel.getPaymentInfo()).willReturn(creditCardPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(creditCardPaymentInfo)).willReturn(ipgPaymentMethod);
        final ArgumentCaptor<TargetQueryTransactionDetailsRequest> request = ArgumentCaptor
                .forClass(TargetQueryTransactionDetailsRequest.class);
        final TargetQueryTransactionDetailsResult queryTxnResult = mock(TargetQueryTransactionDetailsResult.class);
        given(ipgPaymentMethod.queryTransactionDetails(request.capture())).willReturn(queryTxnResult);

        createPreOrderPaymentDetails(true);
        final boolean result = targetOrderFacadeImpl.isUpdateCreditCardDetailsSuccessful("OrderId", "ipgToken");

        assertThat(result).isTrue();
        verify(targetPaymentService).createTransactionWithQueryResult(mockOrderModel, queryTxnResult,
                PaymentTransactionType.DEFERRED);
        verify(modelService).remove(mockOrderModel.getPaymentTransactions().get(0));

    }

    private void createPreOrderPaymentDetails(final boolean createDeferredEntry) {
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final List<PaymentTransactionEntryModel> transactions = new ArrayList<>();
        final CreditCardPaymentInfoModel actualPaymentInfo = mock(CreditCardPaymentInfoModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel;
        final CCPaymentInfoData ccPaymentInfoData = mock(CCPaymentInfoData.class);
        for (int i = 0; i < 5; i++) {
            paymentTransactionEntryModel = new PaymentTransactionEntryModel();
            paymentTransactionEntryModel.setAmount(new BigDecimal(i * 10));
            paymentTransactionEntryModel.setCreationtime(new Date(System.currentTimeMillis()));
            paymentTransactionEntryModel.setReceiptNo("receipt" + i);
            paymentTransactionEntryModel.setPaymentTransaction(paymentTransactionModel);
            paymentTransactionEntryModel.setTransactionStatus(i == 4 ? TransactionStatus.REJECTED.toString()
                    : TransactionStatus.ACCEPTED.toString());
            if (createDeferredEntry) {
                if (i == 0) {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CAPTURE);
                    given(ccPaymentInfoData.getCardNumber()).willReturn("CapturedCard");
                    given(ccPaymentInfoData.getExpiryYear()).willReturn("CapturedExpiryYear");
                    given(ccPaymentInfoData.getExpiryMonth()).willReturn("CapturedExpiryMonth");
                }
                else if (i == 1) {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.DEFERRED);
                    given(ccPaymentInfoData.getCardNumber()).willReturn("DeferredCard");
                    given(ccPaymentInfoData.getExpiryYear()).willReturn("DeferredExpiryYear");
                    given(ccPaymentInfoData.getExpiryMonth()).willReturn("DeferredExpiryMonth");

                }
                else {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CANCEL);
                }
            }
            else {
                if (i == 1) {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CAPTURE);
                    given(ccPaymentInfoData.getCardNumber()).willReturn("CapturedCard");
                    given(ccPaymentInfoData.getExpiryYear()).willReturn("CapturedExpiryYear");
                    given(ccPaymentInfoData.getExpiryMonth()).willReturn("CapturedExpiryMonth");
                }
                else {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CANCEL);
                }
            }
            paymentTransactionEntryModel.setIpgPaymentInfo(actualPaymentInfo);
            transactions.add(paymentTransactionEntryModel);
        }

        given(paymentTransactionModel.getEntries()).willReturn(transactions);
        willReturn(Boolean.TRUE).given(paymentTransactionModel).getIsCaptureSuccessful();
        given(mockOrderModel.getPaymentTransactions())
                .willReturn(Collections.singletonList(paymentTransactionModel));
    }

    /**
     * Method will verify custom cnc order status as 'placed'.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderPlacedCustomStatus() {
        final String orderPlaced = "placed";
        final String orderId = "00000001";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.CREATED)))
                        .willReturn(orderPlaced);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isNotEmpty().isEqualTo(orderPlaced);
    }

    /**
     * Method will verify custom cnc order status as 'completed'.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedCustomStatus() {
        final String orderCompleted = "completed";
        final String orderId = "00000002";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderCompleted);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isNotEmpty().isEqualTo(orderCompleted);
    }

    /**
     * Method will verify custom cnc order status as 'inprogress'.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderInProgressCustomStatus() {
        final String orderInProgress = "inprogress";
        final String orderId = "00000003";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.INPROGRESS)))
                        .willReturn(orderInProgress);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isNotEmpty().isEqualTo(orderInProgress);
    }

    /**
     * Method will verify custom cnc order status as 'confirmed'.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderConfirmedCustomStatus() {
        final String orderConfirmed = "confirmed";
        final String orderId = "00000004";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderConfirmed);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isNotEmpty().isEqualTo(orderConfirmed);
    }

    /**
     * Method will verify custom cnc order status as 'refunded'.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderRefundedCustomStatus() {
        final String orderRefunded = "refunded";
        final String orderId = "00000005";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.REJECTED)))
                        .willReturn(orderRefunded);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isNotEmpty().isEqualTo(orderRefunded);
    }

    /**
     * Method will verify custom cnc order status as 'redyToCollect'.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderReadyToCollectCustomStatus() {
        final String orderReadyToCollect = "readyToCollect";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderReadyToCollect);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isNotEmpty().isEqualTo(orderReadyToCollect);
    }

    /**
     * Method will verify custom cnc order status as ''<Empty>.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderEmptyStatus() {
        final String orderId = "00000007";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.CANCELLED)))
                        .willReturn(StringUtils.EMPTY);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.getCncCustomOrderStatus()).isEmpty();
    }

    /**
     * Method will verify delivery mode for the order. Returns true if the delivery mode is Cnc.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerCncDeliveryModeTrue() {
        final String orderReadyToCollect = "readyToCollect";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderReadyToCollect);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isDeliveryModeStoreDelivery()).isTrue();
    }

    /**
     * Method will verify delivery mode for the order. Return false if the delivery mode is Cnc.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerCncDeliveryModeFalse() {
        final String inProgress = "inProgress";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.FALSE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(inProgress);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isDeliveryModeStoreDelivery()).isFalse();
    }

    /**
     * Method will verify to true for completed order and consignment picked up date is available.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedWithConsignmentPickedUpDate() {
        final String orderCompleted = "completed";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderCompleted);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isCncOrderWithConsignmentPickedUpDate()).isTrue();
    }

    /**
     * Method will verify to false for completed order and consignment picked up date is not available.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedWithoutConsignmentPickedUpDate() {
        final String orderCompleted = "completed";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.FALSE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderCompleted);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isCncOrderWithConsignmentPickedUpDate()).isFalse();
    }

    /**
     * Method will verify to true for completed order and consignment ready for pick up date is available.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedWithOrderReadyForPickup() {
        final String orderCompleted = "completed";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderCompleted);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isCncOrderReadyForPickup()).isTrue();
    }

    /**
     * Method will verify to false for completed order and consignment ready for pick date is not available.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedWithoutOrderReadyForPickup() {
        final String orderCompleted = "completed";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.FALSE, orderId, OrderStatus.COMPLETED)))
                        .willReturn(orderCompleted);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isCncOrderReadyForPickup()).isFalse();
    }


    /**
     * Method will verify to true if the order refunded.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedWithCncOrderRefunded() {
        final String inProgress = "inProgress";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.TRUE, orderId, OrderStatus.INPROGRESS)))
                        .willReturn(inProgress);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isCncOrderRefunded()).isTrue();
    }

    /**
     * Method will verify to false if the order not refunded.
     */
    @Test
    public void testGetOrderDetailsForCodeUnderCurrentCustomerOrderCompletedWithoutCncOrderRefunded() {
        final String inProgress = "inProgress";
        final String orderId = "00000006";
        given(clickAndCollectOrderDetailsHelperMock
                .getCustomOrderStatus(mockCurrentUserOrderData(Boolean.FALSE, orderId, OrderStatus.INPROGRESS)))
                        .willReturn(inProgress);
        final TargetOrderData targetOrderData = targetOrderFacadeImpl
                .getOrderDetailsForCodeUnderCurrentCustomer(orderId);
        assertThat(targetOrderData.isCncOrderRefunded()).isFalse();
    }


    /**
     * Setting current user mock data.
     * 
     * @param orderId
     * @return OrderModel
     */
    private OrderModel mockCurrentUserOrderData(final Boolean statusCheck, final String orderId,
            final OrderStatus orderStatus) {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getStatus()).willReturn(orderStatus);
        willReturn(statusCheck).given(clickAndCollectOrderDetailsHelperMock).isCncDeliveryMode(orderModel);
        willReturn(statusCheck).given(clickAndCollectOrderDetailsHelperMock)
                .isOrderCompletedWithConsignmentPickedUpDate(orderModel);
        willReturn(statusCheck).given(clickAndCollectOrderDetailsHelperMock).isOrderReadyForPickup(orderModel);
        willReturn(statusCheck).given(clickAndCollectOrderDetailsHelperMock).isOrderRefunded(orderModel);
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(converter.convert(orderModel)).willReturn(new TargetOrderData());
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(customerAccountService.getOrderForCode(customerModel, orderId, baseStore)).willReturn(orderModel);
        given(targetOrderService.findOrderModelForOrderId(orderId)).willReturn(orderModel);
        return orderModel;
    }
}
