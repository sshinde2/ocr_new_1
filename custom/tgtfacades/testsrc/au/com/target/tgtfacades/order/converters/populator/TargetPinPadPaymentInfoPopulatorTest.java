/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.PinPadPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


/**
 * Unit tests for TargetPinPadPaymentInfoPopulator
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPinPadPaymentInfoPopulatorTest {
    @Mock
    private Converter<CreditCardType, CardTypeData> mockCardTypeConverter;

    @Mock
    private Converter<AddressModel, TargetAddressData> addressConverter;

    @InjectMocks
    private final TargetPinPadPaymentInfoPopulator populator = new TargetPinPadPaymentInfoPopulator();

    @Test
    public void testPopulate() {
        final PinPadPaymentInfoModel pinPadPaymentInfoModel = Mockito.mock(PinPadPaymentInfoModel.class);
        final PK pk = PK.parse("123");

        final AddressModel billingAddressModel = Mockito.mock(AddressModel.class);
        Mockito.when(billingAddressModel.getLine1()).thenReturn("12, Thompson Road");
        Mockito.when(billingAddressModel.getTown()).thenReturn("North Geelong");

        final TargetAddressData billingAddressData = Mockito.mock(TargetAddressData.class);
        Mockito.when(billingAddressData.getLine1()).thenReturn("12, Thompson Road");
        Mockito.when(billingAddressData.getTown()).thenReturn("North Geelong");

        Mockito.when(addressConverter.convert(billingAddressModel)).thenReturn(billingAddressData);

        Mockito.when(pinPadPaymentInfoModel.getPk()).thenReturn(pk);
        Mockito.when(pinPadPaymentInfoModel.getAccountType()).thenReturn("accountType");
        Mockito.when(pinPadPaymentInfoModel.getRespAscii()).thenReturn("responseAscii");
        Mockito.when(pinPadPaymentInfoModel.getAuthCode()).thenReturn("authCode");
        Mockito.when(pinPadPaymentInfoModel.getMaskedCardNumber()).thenReturn("000000000000");
        Mockito.when(pinPadPaymentInfoModel.getCardType()).thenReturn(CreditCardType.MASTER);
        Mockito.when(pinPadPaymentInfoModel.getJournRoll()).thenReturn("journalRoll");
        Mockito.when(pinPadPaymentInfoModel.getRrn()).thenReturn("rrn");
        Mockito.when(pinPadPaymentInfoModel.getBillingAddress()).thenReturn(billingAddressModel);
        populator.setAddressConverter(addressConverter);
        populator.setCardTypeConverter(mockCardTypeConverter);

        final CardTypeData cardTypeData = new CardTypeData();
        cardTypeData.setCode("master");
        given(mockCardTypeConverter.convert(CreditCardType.MASTER)).willReturn(cardTypeData);

        final TargetCCPaymentInfoData paymentInfoData = new TargetCCPaymentInfoData();
        populator.populate(pinPadPaymentInfoModel, paymentInfoData);
        Assert.assertNotNull(paymentInfoData);

        final PinPadPaymentInfoData pinPadInfoData = paymentInfoData.getPinPadPaymentInfoData();
        Assert.assertNotNull(pinPadInfoData);

        Assert.assertEquals(true, paymentInfoData.isPinPadPaymentInfo());

        Assert.assertNotNull(paymentInfoData.getBillingAddress());
        Assert.assertEquals("12, Thompson Road", paymentInfoData.getBillingAddress().getLine1());
        Assert.assertEquals("North Geelong", paymentInfoData.getBillingAddress().getTown());

        Assert.assertEquals("accountType", pinPadInfoData.getAccountType());
        Assert.assertEquals("responseAscii", pinPadInfoData.getRespAscii());
        Assert.assertEquals("authCode", pinPadInfoData.getAuthCode());
        Assert.assertEquals("000000000000", pinPadInfoData.getMaskedCardNumber());
        Assert.assertEquals(StringUtils.lowerCase(CreditCardType.MASTER.toString()), pinPadInfoData.getCardType());
        Assert.assertEquals("journalRoll", pinPadInfoData.getJournRoll());
        Assert.assertEquals("rrn", pinPadInfoData.getRrn());

        assertThat(paymentInfoData.getCardTypeData()).isEqualTo(cardTypeData);
        assertThat(paymentInfoData.getCardType()).isEqualTo("master");
    }
}