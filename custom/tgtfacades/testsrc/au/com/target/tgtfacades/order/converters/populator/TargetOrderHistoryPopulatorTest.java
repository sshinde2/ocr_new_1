/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.helper.ClickAndCollectOrderDetailsHelper;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.data.TargetOrderHistoryData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;


/**
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderHistoryPopulatorTest {

    private static final String ORDER_PLACED = "placed";
    private static final String ORDER_COMPLETED = "completed";
    private static final String ORDER_INPROGRESS = "inprogress";
    private static final String ORDER_CONFIRMED = "confirmed";
    private static final String ORDER_REFUNDED = "refunded";
    private static final String ORDER_READY_TO_COLLECT = "readyToCollect";

    @Mock
    private AbstractOrderHelper abstractOrderHelperMock;

    @Mock
    private OrderOutstandingStrategy orderOutstandingStrategyMock;

    @Mock
    private ClickAndCollectOrderDetailsHelper clickAndCollectOrderDetailsHelperMock;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelperMock;

    @Mock
    private OrderModel orderModelMock;

    @Mock
    private PriceDataFactory priceDataFactoryMock;

    @Mock
    private CurrencyModel currencyModelMock;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModelMock;

    @Mock
    private DeliveryModeModel deliveryModeModelMock;

    @InjectMocks
    @Spy
    private final TargetOrderHistoryPopulator targetOrderHistoryPopulator = new TargetOrderHistoryPopulator();

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to 'placed'.
     */
    @Test
    public void testPopulateGetCustomOrderStatusPlaced() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock)).willReturn(ORDER_PLACED);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEqualTo(ORDER_PLACED);
    }

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to 'completed'.
     */
    @Test
    public void testPopulateGetCustomOrderStatusCompleted() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock)).willReturn(ORDER_COMPLETED);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEqualTo(ORDER_COMPLETED);
    }

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to 'inprogress'.
     */
    @Test
    public void testPopulateGetCustomOrderStatusInprogress() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock)).willReturn(ORDER_INPROGRESS);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEqualTo(ORDER_INPROGRESS);
    }

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to 'confirmed'.
     */
    @Test
    public void testPopulateGetCustomOrderStatusConfirmed() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock)).willReturn(ORDER_CONFIRMED);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEqualTo(ORDER_CONFIRMED);
    }

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to 'refunded'.
     */
    @Test
    public void testPopulateGetCustomOrderStatusRefundeded() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock)).willReturn(ORDER_REFUNDED);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEqualTo(ORDER_REFUNDED);
    }

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to 'readyToCollect'.
     */
    @Test
    public void testPopulateGetCustomOrderStatusReadyToCollect() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock))
                .willReturn(ORDER_READY_TO_COLLECT);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEqualTo(ORDER_READY_TO_COLLECT);
    }

    /**
     * Method to verify custom order status for cnc order. Here custom order status set to '' <empty>.
     */
    @Test
    public void testPopulateGetCustomOrderStatusEmpty() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelperMock).isDeliveryModeStoreDelivery(deliveryModeModelMock);
        given(clickAndCollectOrderDetailsHelperMock.getCustomOrderStatus(orderModelMock))
                .willReturn(StringUtils.EMPTY);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isEmpty();
    }

    /**
     * Method to verify custom order status for non-cnc order. Here custom order status set to null.
     */
    @Test
    public void testPopulateGetCustomOrderStatusNonCncDelivery() {
        mockOrderData();
        final TargetOrderHistoryData targetOrderHistoryData = new TargetOrderHistoryData();
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelperMock)
                .isDeliveryModeStoreDelivery(deliveryModeModelMock);
        targetOrderHistoryPopulator.populate(orderModelMock, targetOrderHistoryData);
        assertThat(targetOrderHistoryData.getCustomOrderStatus()).isNull();
    }

    /**
     * Setting TargetOrderHistoryData for the placed order.
     * 
     */
    private void mockOrderData() {
        given(orderModelMock.getTotalPrice()).willReturn(Double.valueOf(100.0));
        given(orderModelMock.getCurrency()).willReturn(currencyModelMock);
        given(orderModelMock.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModelMock);
        given(purchaseOptionConfigModelMock.getAllowPaymentDues()).willReturn(Boolean.FALSE);
        given(deliveryModeModelMock.getCode()).willReturn("click-and-collect");
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
    }

}
