/**
 * 
 */
package au.com.target.tgtfacades.look.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.media.MediaModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.util.TargetPriceDataHelper;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookPopulatorTest {

    @InjectMocks
    private final TargetLookPopulator targetLookPopulator = new TargetLookPopulator();

    @Mock
    private TargetPriceDataHelper targetPriceDataHelper;

    @Mock
    private MediaModel mediaModel;

    @Mock
    private PriceData price;

    @SuppressWarnings("boxing")
    @Test
    public void testPopulate() {

        final TargetProductGroupPageModel source = Mockito.mock(TargetProductGroupPageModel.class);
        Mockito.when(source.getGroupImage()).thenReturn(mediaModel);
        Mockito.when(source.getGroupThumb()).thenReturn(mediaModel);
        Mockito.when(mediaModel.getURL()).thenReturn("testUrl1");
        Mockito.when(source.getName()).thenReturn("TestName");
        Mockito.when(source.getLabel()).thenReturn("TestLabel");

        final PriceData priceData = new PriceData();
        priceData.setFormattedValue("100");

        BDDMockito
                .given(targetPriceDataHelper.populatePriceData(Mockito.anyDouble(), Mockito.any(PriceDataType.class)))
                .willReturn(priceData);

        BDDMockito.given(source.getFromPrice()).willReturn(Double.valueOf(priceData.getFormattedValue()));
        final LookDetailsData target = new LookDetailsData();

        targetLookPopulator.populate(source, target);

        BDDMockito.verify(targetPriceDataHelper, Mockito.times(1)).populatePriceData(Mockito.anyDouble(),
                Mockito.any(PriceDataType.class));

        Assert.assertEquals(source.getName(), target.getName());
        Assert.assertEquals(source.getLabel(), target.getLabel());
        Assert.assertEquals(source.getGroupImage().getURL(), target.getGroupImgUrl());
        Assert.assertEquals(source.getGroupThumb().getURL(), target.getGroupThumbUrl());
        Assert.assertEquals(source.getFromPrice(), Double.valueOf(target.getPriceData().getFormattedValue()));
    }
}
