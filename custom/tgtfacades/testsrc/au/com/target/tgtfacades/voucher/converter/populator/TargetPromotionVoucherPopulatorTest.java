/**
 * 
 */
package au.com.target.tgtfacades.voucher.converter.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.offer.converter.TargetMobileOfferHeadingListConverter;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtfacades.voucher.data.TargetPromotionVoucherData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPromotionVoucherPopulatorTest {


    @InjectMocks
    private final TargetPromotionVoucherPopulator targetPromotionVoucherPopulator = new TargetPromotionVoucherPopulator();

    @Mock
    private TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter;


    @Test
    public void testTargetDealsPopulate() {

        final DateTime currentDate = new DateTime();
        final DateTime createdDate = currentDate.minusDays(10);
        final DateTime modifiedDate = currentDate.minusDays(6);
        final DateTime startDate = currentDate.minusDays(2);
        final DateTime endDate = currentDate.plusDays(5);

        final PromotionVoucherModel source = new PromotionVoucherModel();
        source.setModifiedtime(modifiedDate.toDate());
        source.setCreationtime(createdDate.toDate());
        source.setAvailableForMobile(Boolean.TRUE);
        source.setFeatured(Boolean.TRUE);
        source.setMobileLoyaltyVoucher(Boolean.TRUE);
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final DateRestrictionModel dateRestrictionModel = new DateRestrictionModel();
        dateRestrictionModel.setStartDate(startDate.toDate());
        dateRestrictionModel.setEndDate(endDate.toDate());
        restrictions.add(dateRestrictionModel);
        source.setRestrictions(restrictions);

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = Mockito
                .mock(TargetMobileOfferHeadingModel.class);

        final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingList = new ArrayList<TargetMobileOfferHeadingModel>(
                Arrays.asList(targetMobileOfferHeadingModel));
        source.setTargetMobileOfferHeadings(targetMobileOfferHeadingList);

        final TargetMobileOfferHeadingData targetMobileOfferHeadingData = Mockito
                .mock(TargetMobileOfferHeadingData.class);

        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = new ArrayList<TargetMobileOfferHeadingData>(
                Arrays.asList(targetMobileOfferHeadingData));

        BDDMockito.given(targetMobileOfferHeadingListConverter.convert(targetMobileOfferHeadingList)).willReturn(
                targetMobileOfferHeadingDataList);

        final TargetPromotionVoucherData target = new TargetPromotionVoucherData();

        targetPromotionVoucherPopulator.populate(source, target);
        Assertions.assertThat(target).isNotNull();
        Assertions.assertThat(source.getBarcode()).isEqualTo(target.getBarcode());
        Assertions.assertThat(source.getEnabledInstore()).isEqualTo(target.getEnabledInStore());
        Assertions.assertThat(source.getEnabledOnline()).isEqualTo(target.getEnabledOnline());
        Assertions.assertThat(source.getFeatured()).isEqualTo(target.getFeatured());
        Assertions.assertThat(source.getAvailableForMobile()).isEqualTo(target.getAvailableForMobile());
        Assertions.assertThat(source.getMobileLoyaltyVoucher()).isEqualTo(target.getMobileLoyaltyVoucher());
        Assert.assertEquals(createdDate, target.getCreatedTimeFormatted());
        Assert.assertEquals(modifiedDate, target.getModifiedTimeFormatted());
        Assert.assertEquals(startDate, target.getStartTimeFormatted());
        Assert.assertEquals(endDate, target.getEndTimeFormatted());
        Assert.assertNotNull(target.getTargetMobileOfferHeadings());

        Assert.assertSame(targetMobileOfferHeadingData, target.getTargetMobileOfferHeadings().get(0));

    }
}
