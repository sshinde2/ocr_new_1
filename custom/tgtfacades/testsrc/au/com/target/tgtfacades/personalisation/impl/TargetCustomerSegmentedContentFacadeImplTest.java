/**
 * 
 */
package au.com.target.tgtfacades.personalisation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.collections.ListUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmarketing.segments.UserSegmentRestrictionForFragmentPageEvaluator;
import au.com.target.tgtwebcore.model.cms2.TargetPersonaliseContainerComponentModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetFragmentPageModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerSegmentedContentFacadeImplTest {

    @Mock
    private CMSComponentService cmsComponentService;

    @Mock
    private UserService userService;

    @Mock
    private UserSegmentRestrictionForFragmentPageEvaluator userSegmentRestrictionForFragmentPageEvaluator;

    @InjectMocks
    private final TargetCustomerSegmentedContentFacadeImpl targetCustomerSegmentedContentFacadeImpl = new TargetCustomerSegmentedContentFacadeImpl();


    @Test
    public void getSegmentedContentForComponentNotFound() throws CMSItemNotFoundException {
        final CMSItemNotFoundException ex = Mockito.mock(CMSItemNotFoundException.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("invalid-id")))
                .willThrow(ex);
        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("invalid-id", "womens")).isNull();

    }

    @Test
    public void getSegmentedContentForInvalidComponent() throws CMSItemNotFoundException {
        final SimpleCMSComponentModel cmsComponent = Mockito.mock(SimpleCMSComponentModel.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("invalid-id"))).willReturn(cmsComponent);
        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("invalid-id", "womens")).isNull();

    }

    @Test
    public void getSegmentedContentForUserSegmentNotFound() throws CMSItemNotFoundException {
        final TargetPersonaliseContainerComponentModel targetPersonalisedComponent = Mockito
                .mock(TargetPersonaliseContainerComponentModel.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("comp-id-1")))
                .willReturn(targetPersonalisedComponent);
        BDDMockito.given((userService.getUserGroupForUID("invalidsegment")))
                .willReturn(null);

        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("comp-id-1", "invalidsegment")).isNull();

    }

    @Test
    public void getSegmentedContentForInvalidUserSegment() throws CMSItemNotFoundException {
        final TargetPersonaliseContainerComponentModel targetPersonalisedComponent = Mockito
                .mock(TargetPersonaliseContainerComponentModel.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("comp-id-1")))
                .willReturn(targetPersonalisedComponent);
        final UserGroupModel userGroup = Mockito.mock(UserGroupModel.class);
        BDDMockito.given((userService.getUserGroupForUID("invalidsegment")))
                .willReturn(userGroup);
        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("comp-id-1", "invalidsegment")).isNull();
    }

    @Test
    public void getSegmentedContentForNoContentPresentAgainstTheComponent() throws CMSItemNotFoundException {
        final TargetPersonaliseContainerComponentModel targetPersonalisedComponent = Mockito
                .mock(TargetPersonaliseContainerComponentModel.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("comp-id-1")))
                .willReturn(targetPersonalisedComponent);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given((userService.getUserGroupForUID("womens")))
                .willReturn(userSegment);
        BDDMockito.given(targetPersonalisedComponent.getPages()).willReturn(ListUtils.EMPTY_LIST);
        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("comp-id-1", "womens")).isNull();
    }

    @Test
    public void getSegmentedContentWhenNoContentRestrictionMatchedTheSegment() throws CMSItemNotFoundException {
        final TargetPersonaliseContainerComponentModel targetPersonalisedComponent = Mockito
                .mock(TargetPersonaliseContainerComponentModel.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("comp-id-1")))
                .willReturn(targetPersonalisedComponent);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given((userService.getUserGroupForUID("womens")))
                .willReturn(userSegment);

        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final Collection<TargetFragmentPageModel> pages = Arrays.asList(targetFragmentPageModel);
        BDDMockito
                .given(Boolean.valueOf(
                        userSegmentRestrictionForFragmentPageEvaluator.evaluate(targetFragmentPageModel, userSegment)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(targetPersonalisedComponent.getPages()).willReturn(pages);
        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("comp-id-1", "womens")).isNull();
    }

    @Test
    public void getSegmentedContentWhenContentRestrictionMatchesTheSegment() throws CMSItemNotFoundException {
        final TargetPersonaliseContainerComponentModel targetPersonalisedComponent = Mockito
                .mock(TargetPersonaliseContainerComponentModel.class);
        BDDMockito.given((cmsComponentService.getSimpleCMSComponent("comp-id-1")))
                .willReturn(targetPersonalisedComponent);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegment.getUid())
                .willReturn("womens");
        BDDMockito.given((userService.getUserGroupForUID("womens")))
                .willReturn(userSegment);
        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final Collection<TargetFragmentPageModel> pages = Arrays.asList(targetFragmentPageModel);
        BDDMockito.given(targetPersonalisedComponent.getPages()).willReturn(pages);
        BDDMockito
                .given(Boolean.valueOf(
                        userSegmentRestrictionForFragmentPageEvaluator.evaluate(targetFragmentPageModel, userSegment)))
                .willReturn(Boolean.TRUE);
        Assertions.assertThat(targetCustomerSegmentedContentFacadeImpl
                .getSegmentedContentFromPersonalisedComponent("comp-id-1", "womens")).isNotNull()
                .isEqualTo(targetFragmentPageModel);
    }


}
