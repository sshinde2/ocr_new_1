package au.com.target.tgtfacades.customer.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtmail.service.TargetCustomerSubscriptionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerSubscriptionFacadeImplTest {
    @Mock
    private TargetCustomerSubscriptionService mockTargetCustomerSubscriptionService;

    @Mock
    private TargetSalesApplicationService mockTargetSalesApplicationService;


    @InjectMocks
    private final TargetCustomerSubscriptionFacadeImpl targetCustomerSubscriptionFacadeImpl = new TargetCustomerSubscriptionFacadeImpl();

    @Mock
    private TargetCustomerSubscriptionResponseDto resultDto;

    @Before
    public void setUp() {
        when(
                mockTargetCustomerSubscriptionService
                        .createCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(resultDto);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testProcessCustomerDetailsWithFailure() {
        final TargetCustomerSubscriptionRequestDto data = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(mockTargetCustomerSubscriptionService.createCustomerSubscription(data)).thenReturn(response);
        when(response.getResponseType()).thenReturn(TargetCustomerSubscriptionResponseType.UNAVAILABLE);

        targetCustomerSubscriptionFacadeImpl.performCustomerSubscription(data);

        verify(mockTargetCustomerSubscriptionService).processCustomerSubscription(data);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testProcessCustomerDetailsWithSuccess() {
        final TargetCustomerSubscriptionRequestDto data = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(mockTargetCustomerSubscriptionService.createCustomerSubscription(data)).thenReturn(response);
        when(response.getResponseType()).thenReturn(TargetCustomerSubscriptionResponseType.SUCCESS);
        targetCustomerSubscriptionFacadeImpl.performCustomerSubscription(data);

        verify(mockTargetCustomerSubscriptionService, times(0)).processCustomerSubscription(data);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testProcessCustomerPersonalDetailsWithSuccess() {
        final TargetCustomerSubscriptionRequestDto data = createRequest();
        targetCustomerSubscriptionFacadeImpl.processCustomerPersonalDetails(data);
        verify(mockTargetCustomerSubscriptionService, times(1)).processCustomerPersonalDetails(data);
    }

    private TargetCustomerSubscriptionRequestDto createRequest() {
        final TargetCustomerSubscriptionRequestDto data = new TargetCustomerSubscriptionRequestDto();
        data.setCustomerEmail("foo@bar.com");
        data.setFirstName("Marty");
        data.setTitle("Mr");
        data.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        return data;
    }
}
