package au.com.target.tgtfacades.login.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;

import au.com.target.tgtcore.customer.ReusePasswordException;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CustomerRegisteredResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.LoginSuccessResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetRegisterData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCheckoutLoginResponseFacadeImplTest {

    @Mock
    private TargetAuthenticationFacade mockTargetAuthenticationFacade;

    @Mock
    private TargetCustomerFacade mockTargetCustomerFacade;

    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;

    @Mock
    private SessionService mockSessionService;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private TargetCustomerAccountService customerAccountService;

    @InjectMocks
    private final TargetCheckoutLoginResponseFacadeImpl targetCheckoutLoginResponseFacadeImpl = new TargetCheckoutLoginResponseFacadeImpl();

    @Before
    public void setup() {
        doReturn(Boolean.TRUE).when(mockTargetCheckoutFacade).isGuestCheckoutAllowed();
    }

    @Test
    public void testCreateLoginRequiredResponse() {
        final Response response = targetCheckoutLoginResponseFacadeImpl.createLoginRequiredResponse();

        assertThat(response.isSuccess()).isFalse();

        final Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_LOGIN_REQUIRED");
        assertThat(error.getMessage()).isEqualTo("please login");
    }

    @Test
    public void testCreateLoginSuccessResponse() throws Exception {
        final Response response = targetCheckoutLoginResponseFacadeImpl.createLoginSuccessResponse("/checkout");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)response.getData();
        assertThat(data.getRedirectUrl()).isEqualTo("/checkout");
    }

    @Test
    public void testCreateLoginFailedResponseWithNoSecurityException() throws Exception {
        final Response response = targetCheckoutLoginResponseFacadeImpl.createLoginFailedResponse(null);

        assertThat(response.isSuccess()).isFalse();

        final Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_LOGIN_UNKNOWN");
    }

    @Test
    public void testCreateLoginFailedResponseWithBadCredentials() throws Exception {
        final Response response = targetCheckoutLoginResponseFacadeImpl
                .createLoginFailedResponse(BadCredentialsException.class);

        assertThat(response.isSuccess()).isFalse();

        final Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_LOGIN_BAD_CREDENTIALS");
    }

    @Test
    public void testCreateLoginFailedResponseWithLockedAccount() throws Exception {
        final Response response = targetCheckoutLoginResponseFacadeImpl
                .createLoginFailedResponse(LockedException.class);

        assertThat(response.isSuccess()).isFalse();

        final Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_LOGIN_ACCOUNT_LOCKED");
    }

    @Test
    public void testCreateLoginFailedResponseWithUnhandledExceptionType() throws Exception {
        final Response response = targetCheckoutLoginResponseFacadeImpl.createLoginFailedResponse(Exception.class);

        assertThat(response.isSuccess()).isFalse();

        final Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_LOGIN_UNKNOWN");
    }

    @Test
    public void testIsCustomerRegisteredWithRegisteredNotLockedUser() {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        given(mockTargetCustomerFacade.getCustomerForUsername("therealdeal@target.com.au")).willReturn(mockCustomer);

        doReturn(Boolean.FALSE).when(mockTargetAuthenticationFacade).isAccountLocked(mockCustomer);

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .isCustomerRegistered("therealdeal@target.com.au");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData data = (CustomerRegisteredResponseData)response.getData();
        assertThat(data.isRegistered()).isTrue();
        assertThat(data.isAccountLocked()).isFalse();
    }

    @Test
    public void testIsCustomerRegisteredWithRegisteredLockedUser() {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        given(mockTargetCustomerFacade.getCustomerForUsername("therealdeal@target.com.au")).willReturn(mockCustomer);

        doReturn(Boolean.TRUE).when(mockTargetAuthenticationFacade).isAccountLocked(mockCustomer);

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .isCustomerRegistered("therealdeal@target.com.au");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData data = (CustomerRegisteredResponseData)response.getData();
        assertThat(data.isRegistered()).isTrue();
        assertThat(data.isAccountLocked()).isTrue();
    }

    @Test
    public void testIsCustomerRegisteredWithNotRegisteredUser() {
        given(mockTargetCustomerFacade.getCustomerForUsername("therealdeal@target.com.au")).willReturn(null);

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .isCustomerRegistered("therealdeal@target.com.au");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData data = (CustomerRegisteredResponseData)response.getData();
        assertThat(data.isRegistered()).isFalse();
        assertThat(data.isAccountLocked()).isFalse();
    }

    @Test
    public void testProcessGuestCheckoutRequestWithGuestCheckoutDisabled() {
        doReturn(Boolean.FALSE).when(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        final Error error = data.getError();

        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NOT_ALLOWED);
    }

    @Test
    public void testProcessGuestCheckoutRequestWithGiftCardInCart() {
        doReturn(Boolean.TRUE).when(mockTargetCheckoutFacade).isGiftCardInCart();

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        final Error error = data.getError();

        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NO_GIFT_CARDS);
    }

    @Test
    public void testProcessGuestCheckoutRequestWithExistingCustomerWithSoftLoginUser() throws DuplicateUidException {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        final CartModel checkoutCart = mock(CartModel.class);
        given(checkoutCart.getUser()).willReturn(mockCustomer);
        given(mockCustomer.getType()).willReturn(null);
        given(mockTargetCustomerFacade.getCustomerForUsername("guestuser@target.com.au")).willReturn(mockCustomer);

        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.FALSE);
        given(mockTargetCheckoutFacade.getCart()).willReturn(checkoutCart);
        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)response.getData();

        assertThat(data.getRedirectUrl()).isEqualTo("/checkout");

        verify(mockTargetCustomerFacade).createGuestUserForAnonymousCheckout("guestuser@target.com.au", "Guest");
        verify(mockSessionService).setAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
        verify(mockSessionService).setAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST, Boolean.TRUE);
    }

    @Test
    public void testProcessGuestCheckoutRequestWithNewCustomerWithAnonymousUser() throws DuplicateUidException {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        final CartModel checkoutCart = mock(CartModel.class);
        final UserModel mockCartUser = mock(UserModel.class);
        given(checkoutCart.getUser()).willReturn(mockCartUser);
        given(mockCustomer.getType()).willReturn(null);
        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.TRUE);
        given(mockTargetCheckoutFacade.getCart()).willReturn(checkoutCart);
        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)response.getData();

        assertThat(data.getRedirectUrl()).isEqualTo("/checkout");

        verify(mockTargetCustomerFacade).createGuestUserForAnonymousCheckout("guestuser@target.com.au", "Guest");
        verify(mockSessionService).setAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
    }

    @Test
    public void testProcessGuestCheckoutRequestWithNewDuplicateIdWithAnonymousUser() throws DuplicateUidException {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        final CartModel checkoutCart = mock(CartModel.class);
        final UserModel mockCartUser = mock(UserModel.class);
        given(checkoutCart.getUser()).willReturn(mockCartUser);
        given(mockCustomer.getType()).willReturn(null);
        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.TRUE);
        given(mockTargetCheckoutFacade.getCart()).willReturn(checkoutCart);
        doThrow(new DuplicateUidException()).when(mockTargetCustomerFacade)
                .createGuestUserForAnonymousCheckout("guestuser@target.com.au", "Guest");

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        final Error error = data.getError();

        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_USER_EXISTS);
    }

    @Test
    public void testProcessGuestCheckoutRequestWithAnonymousUserWithGuestUserInCart() throws DuplicateUidException {
        final CartModel checkoutCart = mock(CartModel.class);
        final TargetCustomerModel mockCartUser = mock(TargetCustomerModel.class);
        given(checkoutCart.getUser()).willReturn(mockCartUser);
        given(mockCartUser.getType()).willReturn(CustomerType.GUEST);
        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.TRUE);
        given(mockTargetCheckoutFacade.getCart()).willReturn(checkoutCart);
        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)response.getData();

        assertThat(data.getRedirectUrl()).isEqualTo("/checkout");

        verify(mockTargetCustomerFacade, Mockito.times(0)).createGuestUserForAnonymousCheckout(
                "guestuser@target.com.au", "Guest");
        verify(customerAccountService).updateUidForGuestUser("guestuser@target.com.au", mockCartUser);
    }

    @Test
    public void testProcessGuestCheckoutRequestWithAnonymousUserWithAnonymousUserInCart() throws DuplicateUidException {
        final CartModel checkoutCart = mock(CartModel.class);
        final UserModel mockCartUser = mock(UserModel.class);
        given(checkoutCart.getUser()).willReturn(mockCartUser);
        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.TRUE);
        given(mockTargetCheckoutFacade.getCart()).willReturn(checkoutCart);
        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)response.getData();

        assertThat(data.getRedirectUrl()).isEqualTo("/checkout");

        verify(mockTargetCustomerFacade).createGuestUserForAnonymousCheckout(
                "guestuser@target.com.au", "Guest");
        verify(customerAccountService, Mockito.times(0)).updateUidForGuestUser(Mockito.anyString(),
                Mockito.any(TargetCustomerModel.class));
    }

    @Test
    public void testProcessCustomerRegistrationRequest() throws DuplicateUidException {
        final TargetRegisterData registerData = new TargetRegisterData();

        final Response response = targetCheckoutLoginResponseFacadeImpl.processCustomerRegistrationRequest(
                registerData,
                "/checkout");

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)response.getData();

        assertThat(data.getRedirectUrl()).isEqualTo("/checkout");

        verify(mockTargetCustomerFacade).register(registerData);
    }

    @Test
    public void testProcessCustomerRegistrationRequestUserAlreadyExists() throws DuplicateUidException {
        final TargetRegisterData registerData = new TargetRegisterData();

        doThrow(new DuplicateUidException()).when(mockTargetCustomerFacade).register(registerData);

        final Response response = targetCheckoutLoginResponseFacadeImpl.processCustomerRegistrationRequest(
                registerData,
                "/checkout");

        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        final Error error = data.getError();

        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_USER_EXISTS);
    }

    public void testProcessCustomerRegistrationRequestWithBlacklistedPassword() {
        final TargetRegisterData registerData = new TargetRegisterData();
        registerData.setPassword("password");

        doReturn(Boolean.TRUE).when(mockTargetCustomerFacade).isPasswordBlacklisted("password");

        final Response response = targetCheckoutLoginResponseFacadeImpl.processCustomerRegistrationRequest(
                registerData,
                "/checkout");

        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        final Error error = data.getError();

        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_WEAK_PASSWORD);
    }

    @Test
    public void testProcessSetPasswordByTokenWithEmptyToken() {
        final String password = "password";
        final String token = null;
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordByToken(password, token);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo(
                TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_INVALID_TOKEN);
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_GENERAL);
    }

    @Test
    public void testProcessSetPasswordByTokenWithEmptyUser() {
        final String password = "password";
        final String token = "token";
        doThrow(new IllegalArgumentException()).when(mockTargetCustomerFacade).getUidForPasswordResetToken(token);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordByToken(password, token);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo(
                TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_GENERAL);
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_GENERAL);
    }

    @Test
    public void testProcessSetPasswordByTokenWithInvalidToken() throws TokenInvalidatedException {
        final String password = "password";
        final String token = "token";
        doReturn("test@test.com").when(mockTargetCustomerFacade).getUidForPasswordResetToken(token);
        doReturn("/redirect").when(mockTargetCustomerFacade).getRedirectPageForPasswordResetToken(token);
        doThrow(new TokenInvalidatedException()).when(mockTargetCustomerFacade).updatePassword(token, password);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordByToken(password, token);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo(
                TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_INVALID_TOKEN);
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_GENERAL);
    }

    @Test
    public void testProcessSetPasswordByTokenWithReusePasswordException() throws TokenInvalidatedException {
        final String password = "password";
        final String token = "token";
        doReturn("test@test.com").when(mockTargetCustomerFacade).getUidForPasswordResetToken(token);
        doReturn("/redirect").when(mockTargetCustomerFacade).getRedirectPageForPasswordResetToken(token);
        doThrow(new ReusePasswordException("error")).when(mockTargetCustomerFacade).updatePassword(token, password);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordByToken(password, token);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo(
                TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_REUSE_PASSWORD);
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_REUSE_PASSWORD);
    }

    @Test
    public void testProcessSetPasswordByTokenWithSuccessResponse() throws TokenInvalidatedException {
        final String password = "password";
        final String token = "token";
        final String uid = "test@test.com";
        doReturn(uid).when(mockTargetCustomerFacade).getUidForPasswordResetToken(token);
        doReturn("/redirect").when(mockTargetCustomerFacade).getRedirectPageForPasswordResetToken(token);
        doNothing().when(mockTargetCustomerFacade).updatePassword(token, password);
        doNothing().when(mockTargetCustomerFacade).resetRedirectPageForPasswordResetToken(uid);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordByToken(password, token);
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void testSetPasswordDefaultRedirection() {
        final String token = "token";
        given(mockTargetCustomerFacade.getRedirectPageForPasswordResetToken(token)).willReturn("/spc/order");
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordByToken("password", token);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo("/spc/order");
    }

    @Test
    public void testprocessSetPasswordRedirectWithDefaultRedirection() {
        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl(null);
        final Response responseInput = new Response(true);
        responseInput.setData(data);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordRedirect(responseInput);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo(
                TgtFacadesConstants.Page.MY_ACCOUNT);
    }

    @Test
    public void testSetPasswordRedirectionToCheckout() {
        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl("/spc/order");
        final Response responseInput = new Response(true);
        responseInput.setData(data);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordRedirect(responseInput);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo("/spc/order");
    }

    @Test
    public void testSetPasswordRedirectionToHomeIfNoItemsInCart() {
        final CartData cartData = mock(CartData.class);
        given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getTotalItems()).willReturn(Integer.valueOf(0));
        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl("/spc/order");
        final Response responseInput = new Response(true);
        responseInput.setData(data);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordRedirect(responseInput);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo("/");
    }

    @Test
    public void testSetPasswordRedirectionToFavourites() {
        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl("favourites");
        final Response responseInput = new Response(true);
        responseInput.setData(data);
        final Response response = targetCheckoutLoginResponseFacadeImpl.processSetPasswordRedirect(responseInput);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo("/favourites");
    }

    @Test
    public void testProcessGuestCheckoutRequestWithPreOrderInCart() {
        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).isCartHavingPreOrderItem();

        final Response response = targetCheckoutLoginResponseFacadeImpl
                .processGuestCheckoutRequest("guestuser@target.com.au", "/checkout");

        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        final Error error = data.getError();

        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NO_PREORDERS);
    }

}
