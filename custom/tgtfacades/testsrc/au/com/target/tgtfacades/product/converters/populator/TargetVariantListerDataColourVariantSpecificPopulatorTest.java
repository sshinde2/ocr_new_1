package au.com.target.tgtfacades.product.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.converters.populator.TargetProductDeliveryZonePopulator;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVariantListerDataColourVariantSpecificPopulatorTest {

    @Mock
    private TargetPreOrderService preOrderService;

    @Mock
    private TargetColourVariantProductModel source;

    @Mock
    private TargetProductDeliveryZonePopulator targetProductDeliveryZonePopulator;

    @InjectMocks
    @Spy
    private final TargetVariantListerDataColourVariantSpecificPopulator populator = new TargetVariantListerDataColourVariantSpecificPopulator();


    @Test
    public void testPopulate() throws Exception {

        given(Boolean.valueOf(source.isAssorted())).willReturn(Boolean.TRUE);
        given(source.getColourName()).willReturn("colourname");
        given(source.getEssential()).willReturn(Boolean.TRUE);
        given(source.getTargetExclusive()).willReturn(Boolean.TRUE);
        given(source.getHotProduct()).willReturn(Boolean.TRUE);
        given(source.getClearance()).willReturn(Boolean.TRUE);
        given(source.getOnlineExclusive()).willReturn(Boolean.TRUE);
        given(source.getNewLowerPriceFlag()).willReturn(Boolean.TRUE);

        given(preOrderService.getProductDisplayType(source)).willReturn(ProductDisplayType.AVAILABLE_FOR_SALE);

        final ColourModel swatchModel = mock(ColourModel.class);
        given(source.getSwatch()).willReturn(swatchModel);
        given(swatchModel.getDisplay()).willReturn(Boolean.TRUE);
        given(source.getSwatchName()).willReturn("swatchname");

        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        populator.populate(source, target);
        assertThat(target.getColourName()).isEqualTo("colourname");
        assertThat(target.getSwatchColour()).isEqualTo("swatchname");
        assertThat(target.getAssorted()).isEqualTo(Boolean.TRUE);

        assertThat(target.getPromotionStatusClass()).containsExactly(
                TargetPromotionStatusEnum.NEW_LOWER_PRICE.getPromotionClass(),
                TargetPromotionStatusEnum.ONLINE_EXCLUSIVE.getPromotionClass(),
                TargetPromotionStatusEnum.TARGET_EXCLUSIVE.getPromotionClass(),
                TargetPromotionStatusEnum.CLEARANCE.getPromotionClass(),
                TargetPromotionStatusEnum.HOT_PRODUCT.getPromotionClass(),
                TargetPromotionStatusEnum.ESSENTIALS.getPromotionClass());

    }

    /**
     * Test method to check for empty promotion statuses
     */
    @Test
    public void testPopulateEmptyPromotionStatuses() {
        given(source.getEssential()).willReturn(Boolean.FALSE);
        given(source.getTargetExclusive()).willReturn(Boolean.FALSE);
        given(source.getHotProduct()).willReturn(Boolean.FALSE);
        given(source.getClearance()).willReturn(Boolean.FALSE);
        given(source.getOnlineExclusive()).willReturn(Boolean.FALSE);
        given(source.getNewLowerPriceFlag()).willReturn(Boolean.FALSE);
        given(preOrderService.getProductDisplayType(source)).willReturn(ProductDisplayType.AVAILABLE_FOR_SALE);

        final TargetVariantProductListerData target = mock(TargetVariantProductListerData.class);
        populator.populate(source, target);

        assertThat(target.getPromotionStatusClass()).isEmpty();

    }


    /**
     * Test populate product display type,color variant as sellable Variant
     */
    @Test
    public void testPopulateVerifyProductdisplayTypeAndReleaseDate() {

        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(source.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        given(preOrderService.getProductDisplayType(source)).willReturn(ProductDisplayType.PREORDER_AVAILABLE);
        populator.populate(source, target);
        assertThat(target.getProductDisplayType()).isNotNull();
        assertThat(target.getNormalSaleStartDate()).isEqualTo(embargoDate.getTime());
        assertThat(target.getProductDisplayType()).isEqualTo(ProductDisplayType.PREORDER_AVAILABLE);
        verify(preOrderService).getProductDisplayType(source);
        verify(targetProductDeliveryZonePopulator).populate(source, target);
    }

    /**
     * Test method to verify delivery zone if the display type is COMING_SOON
     */
    @Test
    public void testPopulateVerifyDeliveryZonePopulatorWithDisplayTypeComingSoon() {
        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        given(preOrderService.getProductDisplayType(source)).willReturn(ProductDisplayType.COMING_SOON);
        populator.populate(source, target);
        assertThat(target.getProductDisplayType()).isEqualTo(ProductDisplayType.COMING_SOON);
        verify(targetProductDeliveryZonePopulator, never()).populate(source, target);
    }

    /**
     * Test method to verify delivery zone if the display type is AVAILABLE_FOR_SALE
     */
    @Test
    public void testPopulateVerifyDeliveryZonePopulatorWithDisplayTypeAvailableForSale() {
        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        given(preOrderService.getProductDisplayType(source)).willReturn(ProductDisplayType.AVAILABLE_FOR_SALE);
        populator.populate(source, target);
        assertThat(target.getProductDisplayType()).isEqualTo(ProductDisplayType.AVAILABLE_FOR_SALE);
        verify(targetProductDeliveryZonePopulator, never()).populate(source, target);
    }

    @Test
    public void testPopulateVerifyProductdisplayTypeNulleAndReleaseDate() {
        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        given(preOrderService.getProductDisplayType(source)).willReturn(null);
        given(source.getNormalSaleStartDateTime()).willReturn(null);
        populator.populate(source, target);
        assertThat(target.getProductDisplayType()).isNull();
        assertThat(target.getNormalSaleStartDate()).isEqualTo(null);
        verify(preOrderService).getProductDisplayType(source);
    }

}
