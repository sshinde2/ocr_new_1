package au.com.target.tgtfacades.product.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;
import au.com.target.tgtfacades.url.impl.TargetSizeTypeModelUrlResolver;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerBasicDataPopulatorTest {

    private static final String NAME = "LEGO R2D2";
    private static final String CODE = "81726354";
    private static final String BRAND = "LEGO";
    private static final String URL = "/p/lego-r2d2/81726354";
    private static final String DESCRIPTION = "LEGO R2D2 is awesome!";
    private static final String CARE_INSTRUCTIONS = "Keep clean|Keep away from those who would break it";
    private static final String MATERIALS = "Plastic|Paper";
    private static final String PRODUCT_FEATURES = "Fun to put together|Nice to look at";
    private static final String EXTRA_INFO = "Part of the Ultimate Collector Series";
    private static final String SIZE = "L";
    private static final String SIZE_TYPE_CODE = "babysize";
    private static final String SIZE_CHART_URL = "/size-chart/babysize";
    private static final String PRODUCT_TYPE_CODE = "bulky2";
    private static final String TARGET_COLOUR_VARIANT = "TargetProductColourVariant";
    private static final String YOUTUBE_URL = "http://www.youtube.com/embed/{0}?rel=0";
    private static final String CATEGORY_CODE = "W298278";
    private static final String CATEGORY_NAME = "Tops + Tees";
    private static final String MERCH_CODE = "355";
    private static final String MERCH_NAME = "BABY";
    private static final String CAT_LIST_CODE_1 = "W12345";
    private static final String CAT_LIST_CODE_2 = "W40585";
    private static final String CAT_LIST_NAME_1 = "Kitchenwear";
    private static final String CAT_LIST_NAME_2 = "Utensils";

    private static final Integer NUMBER_OF_REVIEWES = Integer.valueOf(473);
    private static final Double AVERAGE_RATING = Double.valueOf(4.98);

    private static final Boolean PREVIEW = Boolean.TRUE;
    private static final Boolean BULKY_BOARD_PRODUCT = Boolean.TRUE;

    @Mock
    private ModelService mockModelService;

    @Mock
    private TargetCommercePriceService targetCommercePriceService;

    @Mock
    private TargetSizeTypeModelUrlResolver mockSizeTypeModelUrlResolver;

    @Mock
    private TargetSizeTypeService mockSizeTypeService;

    @Mock
    private TargetProductDataUrlResolver mockTargetProductDataUrlResolver;

    @Mock
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @InjectMocks
    private final TargetProductListerBasicDataPopulator targetProductListerBasicDataPopulator = new TargetProductListerBasicDataPopulator();

    @Test
    public void testPopulateWithNullSource() {
        final TargetProductListerData mockTargetProductListerData = mock(TargetProductListerData.class);

        targetProductListerBasicDataPopulator.populate(null, mockTargetProductListerData);
    }

    @Test
    public void testPopulateWithNullTarget() {
        final TargetProductModel mockTargetProductModel = mock(TargetProductModel.class);

        targetProductListerBasicDataPopulator.populate(mockTargetProductModel, null);
    }

    @Test
    public void testPopulateWithIncorrectSourceType() {
        final ProductModel mockProductModel = mock(ProductModel.class);
        final TargetProductListerData mockTargetProductListerData = mock(TargetProductListerData.class);

        targetProductListerBasicDataPopulator.populate(mockProductModel, mockTargetProductListerData);

        verifyZeroInteractions(mockProductModel, mockTargetProductListerData);
    }

    @Test
    public void testPopulateWithIncorrectTargetType() {
        final TargetProductModel mockTargetProductModel = mock(TargetProductModel.class);
        final AbstractTargetProductData mockAbstractTargetProductData = mock(AbstractTargetProductData.class);

        targetProductListerBasicDataPopulator.populate(mockTargetProductModel, mockAbstractTargetProductData);

        verifyZeroInteractions(mockTargetProductModel, mockAbstractTargetProductData);
    }

    @Test
    public void testPopulate() {

        given(mockTargetProductDataUrlResolver.resolve(NAME, CODE)).willReturn(URL);

        final TargetProductModel mockTargetProductModel = setupMockProduct();

        final BrandModel mockBrand = mock(BrandModel.class);
        given(mockBrand.getCode()).willReturn(BRAND);
        given(mockTargetProductModel.getBrand()).willReturn(mockBrand);

        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);

        final Collection<VariantProductModel> colourVariants = new ArrayList<>();
        colourVariants.add(mockTargetColourVariantProduct);
        given(mockTargetProductModel.getVariants()).willReturn(colourVariants);

        final SizeTypeModel mockSizeType = mock(SizeTypeModel.class);
        given(mockSizeType.getCode()).willReturn(SIZE_TYPE_CODE);
        given(mockSizeType.getIsSizeChartDisplay()).willReturn(Boolean.TRUE);

        final TargetSizeVariantProductModel mockTargetSizeVariantProduct = mock(TargetSizeVariantProductModel.class);
        given(mockTargetSizeVariantProduct.getSize()).willReturn(SIZE);
        given(mockTargetSizeVariantProduct.getSizeClassification()).willReturn(mockSizeType);

        final Collection<VariantProductModel> sizeVariants = new ArrayList<>();
        sizeVariants.add(mockTargetSizeVariantProduct);
        given(mockTargetColourVariantProduct.getVariants()).willReturn(sizeVariants);

        given(mockSizeTypeModelUrlResolver.resolve(mockSizeType)).willReturn(SIZE_CHART_URL);

        final ProductTypeModel mockProductType = mock(ProductTypeModel.class);
        given(mockProductType.getCode()).willReturn(PRODUCT_TYPE_CODE);
        given(mockProductType.getBulky()).willReturn(Boolean.TRUE);

        given(mockTargetProductModel.getProductType()).willReturn(mockProductType);

        final VariantTypeModel variantTypeModel = mock(VariantTypeModel.class);
        given(mockTargetProductModel.getVariantType()).willReturn(variantTypeModel);
        given(variantTypeModel.getCode()).willReturn(TARGET_COLOUR_VARIANT);

        final TargetProductCategoryModel targetProductCategoryModel = mock(TargetProductCategoryModel.class);
        given(mockTargetProductModel.getOriginalCategory()).willReturn(targetProductCategoryModel);
        given(targetProductCategoryModel.getCode()).willReturn(CATEGORY_CODE);
        given(targetProductCategoryModel.getName()).willReturn(CATEGORY_NAME);
        given(targetProductListerBasicDataPopulator.getTopLevelCategory(mockTargetProductModel))
                .willReturn(targetProductCategoryModel);

        final TargetMerchDepartmentModel targetMerchDepartmentModel = mock(TargetMerchDepartmentModel.class);
        given(mockTargetProductModel.getMerchDepartment()).willReturn(targetMerchDepartmentModel);
        given(targetMerchDepartmentModel.getCode()).willReturn(MERCH_CODE);
        given(targetMerchDepartmentModel.getName()).willReturn(MERCH_NAME);

        final CategoryModel targetCategoryModel1 = mock(CategoryModel.class);
        final CategoryModel targetCategoryModel2 = mock(CategoryModel.class);
        final Collection<CategoryModel> modelList = Arrays.asList(targetCategoryModel1, targetCategoryModel2);

        final TargetCategoryData targetCategoryData1 = mock(TargetCategoryData.class);
        final TargetCategoryData targetCategoryData2 = mock(TargetCategoryData.class);
        given(targetCategoryData1.getCode()).willReturn(CAT_LIST_CODE_1);
        given(targetCategoryData2.getCode()).willReturn(CAT_LIST_CODE_2);
        given(targetCategoryData1.getName()).willReturn(CAT_LIST_NAME_1);
        given(targetCategoryData2.getName()).willReturn(CAT_LIST_NAME_2);
        given(mockTargetProductModel.getSupercategories()).willReturn(modelList);

        given(categoryConverter.convert(targetCategoryModel1)).willReturn(targetCategoryData1);
        given(categoryConverter.convert(targetCategoryModel2)).willReturn(targetCategoryData2);

        final MediaModel media = Mockito.mock(MediaModel.class);
        given(media.getURL()).willReturn(YOUTUBE_URL);
        given(mockTargetProductModel.getVideos()).willReturn(Arrays.asList(media));
        final GiftCardModel giftCardModel = Mockito.mock(GiftCardModel.class);
        given(mockTargetProductModel.getGiftCard()).willReturn(giftCardModel);

        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(Double.valueOf(10.0),
                Double.valueOf(20.0), null, null);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(mockTargetProductModel)).willReturn(
                priceRangeInformation);

        final TargetProductListerData targetProductListerData = new TargetProductListerData();
        targetProductListerBasicDataPopulator.populate(mockTargetProductModel, targetProductListerData);

        assertData(targetProductListerData);
    }


    private void assertData(final TargetProductListerData targetProductListerData) {
        assertThat(targetProductListerData.getName()).isEqualTo(NAME);
        assertThat(targetProductListerData.getCode()).isEqualTo(CODE);
        assertThat(targetProductListerData.getBrand()).isEqualTo(BRAND);
        assertThat(targetProductListerData.getUrl()).isEqualTo(URL);
        assertThat(targetProductListerData.getDescription()).isEqualTo(DESCRIPTION);
        assertThat(targetProductListerData.getNumberOfReviews()).isEqualTo(NUMBER_OF_REVIEWES);
        assertThat(targetProductListerData.getAverageRating()).isEqualTo(AVERAGE_RATING);
        assertThat(targetProductListerData.getCareInstructions()).containsExactly("Keep clean",
                "Keep away from those who would break it");
        assertThat(targetProductListerData.getMaterials()).containsExactly("Plastic", "Paper");
        assertThat(targetProductListerData.getProductFeatures()).containsExactly("Fun to put together",
                "Nice to look at");
        assertThat(targetProductListerData.getExtraInfo()).isEqualTo(EXTRA_INFO);

        assertThat(targetProductListerData.getSize()).isEqualTo(SIZE);
        assertThat(targetProductListerData.getSizeType()).isEqualTo(SIZE_TYPE_CODE);
        assertThat(targetProductListerData.getSizeChartURL()).isEqualTo(SIZE_CHART_URL);
        assertThat(targetProductListerData.getVariantType()).isEqualTo(TARGET_COLOUR_VARIANT);

        assertThat(targetProductListerData.isBigAndBulky()).isTrue();
        assertThat(targetProductListerData.isClickAndCollectExcludeTargetCountry()).isTrue();

        assertThat(targetProductListerData.isGiftCard()).isTrue();

        assertThat(targetProductListerData.getYouTubeLink()).isEqualTo(YOUTUBE_URL);

        assertThat(targetProductListerData.getProductTypeCode()).isEqualTo(PRODUCT_TYPE_CODE);

        assertThat(targetProductListerData.isPreview()).isEqualTo(PREVIEW);
        assertThat(targetProductListerData.getOriginalCategoryCode()).isEqualTo(CATEGORY_CODE);
        assertThat(targetProductListerData.getOriginalCategoryName()).isEqualTo(CATEGORY_NAME);
        assertThat(targetProductListerData.getTopLevelCategory()).isEqualTo(CATEGORY_NAME);
        assertThat(targetProductListerData.getMerchDepartmentCode()).isEqualTo(MERCH_CODE);
        assertThat(targetProductListerData.getMerchDepartmentName()).isEqualTo(MERCH_NAME);
        assertThat(targetProductListerData.isBulkyBoardProduct()).isEqualTo(BULKY_BOARD_PRODUCT);
        assertThat(targetProductListerData.getCategories()).onProperty("code").containsOnly(CAT_LIST_CODE_1,
                CAT_LIST_CODE_2);
        assertThat(targetProductListerData.getCategories()).onProperty("name").containsOnly(CAT_LIST_NAME_1,
                CAT_LIST_NAME_2);
    }

    private TargetProductModel setupMockProduct() {
        final TargetProductModel mockTargetProductModel = mock(TargetProductModel.class);
        given(mockTargetProductModel.getName()).willReturn(NAME);
        given(mockTargetProductModel.getCode()).willReturn(CODE);

        given(mockTargetProductModel.getDescription()).willReturn(DESCRIPTION);
        given(mockTargetProductModel.getNumberOfReviews()).willReturn(NUMBER_OF_REVIEWES);
        given(mockTargetProductModel.getAverageRating()).willReturn(AVERAGE_RATING);
        given(mockTargetProductModel.getCareInstructions()).willReturn(CARE_INSTRUCTIONS);
        given(mockTargetProductModel.getMaterials()).willReturn(MATERIALS);
        given(mockTargetProductModel.getProductFeatures()).willReturn(PRODUCT_FEATURES);
        given(mockTargetProductModel.getExtraInfo()).willReturn(EXTRA_INFO);
        given(mockTargetProductModel.getPreview()).willReturn(PREVIEW);
        given(mockTargetProductModel.getBulkyBoardProduct()).willReturn(BULKY_BOARD_PRODUCT);
        return mockTargetProductModel;
    }
}
