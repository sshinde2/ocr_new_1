/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.promotions.jalo.AbstractPromotion;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryConsumed;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.TargetDealWithRewardResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class TargetPromotionsPopulatorTest {

    private TargetPromotionsPopulator targetPromotionsPopulator;
    private PromotionOrderEntryConsumed consumed;
    private JaloSession session;
    private SessionContext ctx;

    @Before
    public void setUp() {

        targetPromotionsPopulator = new TargetPromotionsPopulator();
        consumed = mock(PromotionOrderEntryConsumed.class);
        session = mock(JaloSession.class);
        ctx = mock(SessionContext.class);

    }

    @Test
    public void testGetFiredPromotionsMessagesWithPromotionResult() {

        final String description = "This item is part of a deal";
        final AbstractPromotion abstractPromotion = mock(ProductPromotion.class);

        final List<PromotionResult> promotionResults = new ArrayList<>();
        final PromotionResult result1 = mock(PromotionResult.class);
        final PromotionResult result2 = mock(PromotionResult.class);

        given(result1.getDescription(ctx, null)).willReturn(description);
        given(result2.getDescription(ctx, null)).willReturn("New description");
        given(result1.getPromotion()).willReturn(abstractPromotion);
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);

        given(result1.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        promotionResults.add(result1);
        promotionResults.add(result2);
        final PromotionOrderResults promoOrderResults = new PromotionOrderResults(ctx, mock(Cart.class),
                promotionResults, 0D);


        given(Boolean.valueOf(result1.getFired(ctx))).willReturn(Boolean.TRUE);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(Boolean.valueOf(result2.getFired(ctx))).willReturn(Boolean.FALSE);

        final List<String> descList = targetPromotionsPopulator.getFiredPromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 1);
        Assert.assertEquals(descList.get(0), description);


    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetFiredPromotionsMessagesWithTargetDelRewardResult() {


        final AbstractPromotion abstractPromotion = mock(ProductPromotion.class);

        final List<PromotionResult> promotionResults = new ArrayList<>();
        final TargetDealWithRewardResult result1 = mock(TargetDealWithRewardResult.class);
        final TargetDealWithRewardResult result2 = mock(TargetDealWithRewardResult.class);
        given(result1.getPromotion()).willReturn(abstractPromotion);
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);

        given(result1.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getCouldhaveMoreRewards()).willReturn(true);
        given(result1.getCouldhaveMoreRewards()).willReturn(true);
        given(Boolean.valueOf(result1.getFired(ctx))).willReturn(Boolean.TRUE);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result2.getFired(ctx)).willReturn(Boolean.TRUE);
        promotionResults.add(result1);
        promotionResults.add(result2);
        final PromotionOrderResults promoOrderResults = new PromotionOrderResults(ctx, mock(Cart.class),
                promotionResults, 0D);

        final List<String> descList = targetPromotionsPopulator.getFiredPromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 0);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetCouldFiredPromotionsMessagesWithPromotionResult() {


        final String description = "This item is part of a deal";
        final AbstractPromotion abstractPromotion = mock(ProductPromotion.class);
        final List<PromotionResult> promotionResults = new ArrayList<>();
        final PromotionResult result1 = mock(PromotionResult.class);
        final PromotionResult result2 = mock(PromotionResult.class);
        given(result1.getDescription(ctx, null)).willReturn(description);
        given(result2.getDescription(ctx, null)).willReturn(description);
        given(abstractPromotion.getDescription()).willReturn(description);
        given(result1.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result1.getPromotion()).willReturn(abstractPromotion);
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);
        given(result1.getCouldFire()).willReturn(true);
        given(result2.getCouldFire()).willReturn(true);
        given(result2.getPromotion()).willReturn(abstractPromotion);

        promotionResults.add(result1);
        promotionResults.add(result2);
        final PromotionOrderResults promoOrderResults = new PromotionOrderResults(ctx, mock(Cart.class),
                promotionResults, 0D);

        final List<String> descList = targetPromotionsPopulator.getCouldFirePromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 2);
        Assert.assertEquals(descList.get(0), description);
        Assert.assertEquals(descList.get(1), description);


    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetCouldFiredPromotionsMessagesWithTargetDealRewardResult() {


        final AbstractPromotion abstractPromotion = mock(ProductPromotion.class);

        final List<PromotionResult> promotionResults = new ArrayList<>();
        final TargetDealWithRewardResult result1 = mock(TargetDealWithRewardResult.class);
        final TargetDealWithRewardResult result2 = mock(TargetDealWithRewardResult.class);
        given(result1.getPromotion()).willReturn(abstractPromotion);
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);

        given(result1.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getCouldhaveMoreRewards()).willReturn(true);
        given(result1.getCouldhaveMoreRewards()).willReturn(false);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result1.getCouldFire()).willReturn(true);
        given(result2.getCouldFire()).willReturn(false);
        promotionResults.add(result1);
        promotionResults.add(result2);
        final PromotionOrderResults promoOrderResults = new PromotionOrderResults(ctx, mock(Cart.class),
                promotionResults, 0D);

        final List<String> descList = targetPromotionsPopulator.getCouldFirePromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 2);


    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetCouldFiredPromotionsMessagesWithTargetDealRewardResultFired() {


        final AbstractPromotion abstractPromotion = mock(ProductPromotion.class);

        final List<PromotionResult> promotionResults = new ArrayList<>();
        final TargetDealWithRewardResult result1 = mock(TargetDealWithRewardResult.class);
        final TargetDealWithRewardResult result2 = mock(TargetDealWithRewardResult.class);
        given(result1.getPromotion()).willReturn(abstractPromotion);
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);

        given(result1.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getConsumedEntries(ctx)).willReturn(Collections.singletonList(consumed));
        given(result2.getCouldhaveMoreRewards()).willReturn(false);
        given(result1.getCouldhaveMoreRewards()).willReturn(false);
        given(result2.getPromotion()).willReturn(abstractPromotion);
        given(result1.getCouldFire()).willReturn(false);
        given(result2.getCouldFire()).willReturn(false);
        promotionResults.add(result1);
        promotionResults.add(result2);
        final PromotionOrderResults promoOrderResults = new PromotionOrderResults(ctx, mock(Cart.class),
                promotionResults, 0D);

        final List<String> descList = targetPromotionsPopulator.getCouldFirePromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 0);


    }

}
