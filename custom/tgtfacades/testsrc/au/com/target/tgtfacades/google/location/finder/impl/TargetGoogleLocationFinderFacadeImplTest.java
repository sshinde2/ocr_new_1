/**
 * 
 */
package au.com.target.tgtfacades.google.location.finder.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.eq;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.google.location.finder.data.GeocodeAddressComponent;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResultData;
import au.com.target.tgtcore.google.location.finder.service.TargetLocationFinderService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.google.location.finder.data.GoogleResponseData;
import au.com.target.tgtfacades.google.location.finder.data.Result;
import au.com.target.tgtfacades.response.data.Response;


/**
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGoogleLocationFinderFacadeImplTest {

    private static final String LOCATION_CARNEGIE_SUBURB = "Carnegie";

    private static final String STATE_VIC = "vic";

    private static final int SHORT_LEAD_TIME = 3;

    private static final String SUBURB = "Suburb";

    private static final String STATE = "State";

    private static final String HD_SHORT_LEAD_TIME = "HD Short Lead Time";

    @Mock
    private TargetLocationFinderService targetLocationFinderServiceMock;

    @Mock
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @InjectMocks
    private final TargetGoogleLocationFinderFacadeImpl targetGoogleLocationFinderFacadeImpl = new TargetGoogleLocationFinderFacadeImpl();


    /**
     * Method to verify delivery location for the given suburb. Here location details will get fetched.
     * 
     * @throws IOException
     */
    @Test
    public void testSearchUserDeliveryLocationWithResponse() throws IOException {
        final GeocodeResponse geocodingResponse = createGeocodingResponse();
        given(targetLocationFinderServiceMock.searchUserDeliveryLocationSuburb(LOCATION_CARNEGIE_SUBURB, null, null))
                .willReturn(geocodingResponse);
        willReturn(Integer.valueOf(3)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.hd.short.vic"), eq(3));
        willReturn(Integer.valueOf(1)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.ed.short.vic"), eq(3));
        willReturn(Integer.valueOf(6)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.bulky1.short.vic"), eq(3));
        willReturn(Integer.valueOf(7)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.bulky2.short.vic"), eq(3));
        willReturn(Integer.valueOf(9)).given(targetSharedConfigFacade).getInt(eq("leadtime.hd.long.vic"), eq(0));
        willReturn(Integer.valueOf(2)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.ed.long.vic"), eq(0));
        willReturn(Integer.valueOf(3)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.bulky1.long.vic"), eq(0));
        willReturn(Integer.valueOf(5)).given(targetSharedConfigFacade).getInt(
                eq("leadtime.bulky2.long.vic"), eq(0));
        willReturn(Integer.valueOf(3)).given(targetSharedConfigFacade).getInt(
                eq(TgtFacadesConstants.LeadTimes.LEADTIME_DEFAULT_CONFIG_CODE), eq(3));

        final Response response = targetGoogleLocationFinderFacadeImpl
                .searchUserDeliveryLocation(LOCATION_CARNEGIE_SUBURB, null, null);
        final GoogleResponseData googleResponseDto = (GoogleResponseData)response.getData();
        final Result result = googleResponseDto.getResults().get(0);
        assertThat(response).isNotNull();
        assertThat(result.getSuburb()).as(SUBURB).isEqualTo(LOCATION_CARNEGIE_SUBURB);
        assertThat(result.getState()).as(STATE).isEqualTo(STATE_VIC);
        assertThat(result.getHdShortLeadTime()).as(HD_SHORT_LEAD_TIME).isEqualTo(SHORT_LEAD_TIME);
        assertThat(result.getHdLongLeadTime()).isEqualTo(9);
        assertThat(result.getEdLongLeadTime()).isEqualTo(2);
        assertThat(result.getEdShortLeadTime()).isEqualTo(1);
        assertThat(result.getBulky1LongLeadTime()).isEqualTo(3);
        assertThat(result.getBulky1ShortLeadTime()).isEqualTo(6);
        assertThat(result.getBulky2ShortLeadTime()).isEqualTo(7);
        assertThat(result.getBulky2LongLeadTime()).isEqualTo(5);


    }

    /**
     * Method to verify delivery location for the given suburb. Here location details will not fetched.
     * 
     */
    @Test
    public void testSearchUserDeliveryLocationWithoutResponse() {
        final Response response = targetGoogleLocationFinderFacadeImpl
                .searchUserDeliveryLocation(LOCATION_CARNEGIE_SUBURB, null, null);
        assertThat(response.isSuccess()).isFalse();
    }

    /**
     * Method to verify delivery location for the given latlng value. Here location details will not fetched.
     * 
     */
    @Test
    public void testSearchUserCurrentAddressUsingLatLngWithoutLocation() {
        final Double latitude = Double.valueOf(-38.1272064);
        final Double longitude = Double.valueOf(144.33648639999998);
        final String location = targetGoogleLocationFinderFacadeImpl
                .searchUserCurrentAddressUsingLatLng(latitude, longitude);
        assertThat(location).isEmpty();
    }

    /**
     * Method to create mock data for the location search with Location/Postcode input parameters.
     */
    private GeocodeResponse createGeocodingResponse() {
        final GeocodeResponse geocodingResponse = new GeocodeResponse();
        final List<GeocodeResultData> results = new ArrayList<>();
        final GeocodeResultData geocodingResultData = new GeocodeResultData();
        final List<GeocodeAddressComponent> geocoderAddressComponents = new ArrayList<>();
        final GeocodeAddressComponent geocoderAddressComponent = new GeocodeAddressComponent();
        final List<String> types = new ArrayList<>();
        types.add(TgtFacadesConstants.AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1);
        types.add(TgtFacadesConstants.AddressComponentType.POLITICAL);
        geocoderAddressComponent.setTypes(types);
        geocoderAddressComponent.setShortName(STATE_VIC);
        geocodingResultData.setAddressComponents(geocoderAddressComponents);
        geocoderAddressComponents.add(geocoderAddressComponent);
        final String[] postcodeLocalities = { LOCATION_CARNEGIE_SUBURB };
        geocodingResultData.setPostcodeLocalities(postcodeLocalities);
        results.add(geocodingResultData);
        geocodingResponse.setResults(results);
        geocodingResponse.setStatus(TgtFacadesConstants.GeocoderStatus.OK);
        return geocodingResponse;
    }

}
