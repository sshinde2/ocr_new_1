/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.url.transform.TargetUrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;

import com.endeca.infront.cartridge.model.Ancestor;
import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.google.common.collect.ImmutableList;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class TargetCategoryDimensionSearchUrlResolverTest {


    private final TargetCategoryDimensionSearchUrlResolver resolver = new TargetCategoryDimensionSearchUrlResolver();

    private final UrlTokenTransformer urlTokenTransformer = new TargetUrlTokenTransformer();


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    @Test
    public void testResolveInternal() {
        final DimensionSearchValue dimensionSearchValue = new DimensionSearchValue();
        dimensionSearchValue.setNavigationState("?NTT=testNavigation");
        dimensionSearchValue.setLabel("testCategoryName");
        final List<Ancestor> ancestorsList = new ArrayList<>();
        final Ancestor ancestor1 = new Ancestor();
        ancestor1.setLabel("ancestor1");
        ancestorsList.add(ancestor1);
        final Ancestor ancestor2 = new Ancestor();
        ancestor2.setLabel("ancestor2");
        ancestorsList.add(ancestor2);
        dimensionSearchValue.setAncestors(ancestorsList);
        final Map<String, String> propertiesMap = new HashMap<String, String>();
        propertiesMap.put("code", "testCategoryCode");
        dimensionSearchValue.setProperties(propertiesMap);
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals("/c/ancestor1/ancestor2/testcategoryname/testCategoryCode?NTT=testNavigation", result);
    }

    @Test
    public void testResolveInternalWithNulDimensionSearchValue() {
        final DimensionSearchValue dimensionSearchValue = null;
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals(StringUtils.EMPTY, result);
    }

    @Test
    public void testResolveInternalWithSpaceInLabel() {
        final DimensionSearchValue dimensionSearchValue = new DimensionSearchValue();
        dimensionSearchValue.setNavigationState("?NTT=testNavigation");
        dimensionSearchValue.setLabel("test category name");
        final List<Ancestor> ancestorsList = new ArrayList<>();
        final Ancestor ancestor1 = new Ancestor();
        ancestor1.setLabel("ancestor 1");
        ancestorsList.add(ancestor1);
        final Ancestor ancestor2 = new Ancestor();
        ancestor2.setLabel("ancestor 2");
        ancestorsList.add(ancestor2);
        dimensionSearchValue.setAncestors(ancestorsList);
        final Map<String, String> propertiesMap = new HashMap<String, String>();
        propertiesMap.put("code", "testCategoryCode");
        dimensionSearchValue.setProperties(propertiesMap);
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals("/c/ancestor-1/ancestor-2/test-category-name/testCategoryCode?NTT=testNavigation", result);
    }

    @Test
    public void testResolveInternalWithEmplyCode() {
        final DimensionSearchValue dimensionSearchValue = new DimensionSearchValue();
        dimensionSearchValue.setNavigationState("?NTT=testNavigation");
        dimensionSearchValue.setLabel("test category name");
        final List<Ancestor> ancestorsList = new ArrayList<>();
        final Ancestor ancestor1 = new Ancestor();
        ancestor1.setLabel("ancestor 1");
        ancestorsList.add(ancestor1);
        final Ancestor ancestor2 = new Ancestor();
        ancestor2.setLabel("ancestor 2");
        ancestorsList.add(ancestor2);
        dimensionSearchValue.setAncestors(ancestorsList);
        final Map<String, String> propertiesMap = new HashMap<String, String>();
        propertiesMap.put("code", StringUtils.EMPTY);
        dimensionSearchValue.setProperties(propertiesMap);
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals(StringUtils.EMPTY, result);
    }
}
