/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.VariantsService;
import de.hybris.platform.variants.model.VariantAttributeDescriptorModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.TargetProductSizeData;
import au.com.target.tgtfacades.product.data.TargetVariantOptionQualifierData;


/**
 * Test cases for TargetVariantOptionDataPopulator.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVariantOptionDataPopulatorTest {

    @InjectMocks
    private final TargetVariantOptionDataPopulator targetVariantOptionDataPopulator = new TargetVariantOptionDataPopulator();

    @Mock
    private AbstractTargetVariantProductModel outofStockVariant;

    @Mock
    private AbstractTargetVariantProductModel inStockVariant;

    @Mock
    private TargetSizeVariantProductModel sizeVariant;

    @Mock
    private VariantTypeModel variantTypeModel;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private ProductModel productModel;

    @Mock
    private VariantsService variantService;

    @Mock
    private VariantAttributeDescriptorModel variantAttributeDescriptorModel;

    @Mock
    private TargetProductSizeModel targetProductSizeModel;

    @Mock
    private ColourModel colourModel;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private UrlResolver productModelUrlResolver;

    @Mock
    private CommercePriceService commercePriceService;

    @Before
    public void setupTestData() {
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);
        given(colourVariant.getVariantType()).willReturn(variantTypeModel);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
    }

    @Test
    public void populateSizeVariantsTargetProductSize() {
        final VariantOptionData variantOptionData = new VariantOptionData();

        given(variantService.getVariantAttributesForVariantType(variantTypeModel)).willReturn(
                Arrays.asList(variantAttributeDescriptorModel));

        final String attributeValue = "productSize";
        given(variantAttributeDescriptorModel.getQualifier()).willReturn(attributeValue);
        given(variantAttributeDescriptorModel.getName()).willReturn("Product Size");
        given(variantService.getVariantAttributeValue(sizeVariant, attributeValue)).willReturn(
                targetProductSizeModel);
        given(targetProductSizeModel.getSize()).willReturn("L");
        given(targetProductSizeModel.getPosition()).willReturn(Integer.valueOf(2));
        given(sizeVariant.getCode()).willReturn("code");
        targetVariantOptionDataPopulator.populate(sizeVariant, variantOptionData);

        assertThat(variantOptionData).isNotNull();
        assertThat(variantOptionData.getCode()).isEqualTo("code");
        final Collection<VariantOptionQualifierData> variantOptionQualifiers = variantOptionData
                .getVariantOptionQualifiers();
        assertThat(variantOptionQualifiers).isNotNull().isNotEmpty().hasSize(1);
        final VariantOptionQualifierData variantOptionQualifierData = variantOptionQualifiers.iterator().next();
        assertThat(variantOptionQualifierData).isInstanceOf(TargetVariantOptionQualifierData.class);
        final TargetVariantOptionQualifierData targetVariantOptionQualifierData = (TargetVariantOptionQualifierData)variantOptionQualifierData;
        final Object objectValue = targetVariantOptionQualifierData.getObjectValue();
        assertThat(objectValue).isNotNull().isInstanceOf(TargetProductSizeData.class);
        final TargetProductSizeData targetProductSizeActualData = (TargetProductSizeData)objectValue;
        assertThat(targetProductSizeActualData.getSize()).isNotNull().isNotEmpty().isEqualTo("L");
        assertThat(targetProductSizeActualData.getPosition()).isNotNull().isEqualTo(2);
        assertThat(variantOptionQualifierData.getValue()).isNullOrEmpty();

    }

    @Test
    public void populateSizeVariantsTargetProductSizeFeatureSwitchOff() {
        final VariantOptionData variantOptionData = new VariantOptionData();

        given(variantService.getVariantAttributesForVariantType(variantTypeModel)).willReturn(
                Arrays.asList(variantAttributeDescriptorModel));

        final HashMap<String, String> excludeVariants = new HashMap<String, String>();
        excludeVariants.put("productSize", "sizeOrderingUI");
        targetVariantOptionDataPopulator.setExcludeVariantQualifiers(excludeVariants);

        final String attributeValue = "productSize";
        given(variantAttributeDescriptorModel.getQualifier()).willReturn(attributeValue);
        given(variantAttributeDescriptorModel.getName()).willReturn("Product Size");
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isFeatureEnabled("sizeOrderingUI");
        targetVariantOptionDataPopulator.populate(sizeVariant, variantOptionData);

        assertThat(variantOptionData).isNotNull();
        final Collection<VariantOptionQualifierData> variantOptionQualifiers = variantOptionData
                .getVariantOptionQualifiers();
        assertThat(variantOptionQualifiers).isNullOrEmpty();

        verify(variantService).getVariantAttributesForVariantType(variantTypeModel);
        verify(targetFeatureSwitchFacade).isFeatureEnabled("sizeOrderingUI");
        verifyNoMoreInteractions(variantService, targetFeatureSwitchFacade);
    }

    @Test
    public void populateColourVariantsTargetProductSize() {
        final VariantOptionData variantOptionData = new VariantOptionData();
        given(colourVariant.getBaseProduct()).willReturn(productModel);
        given(productModel.getVariantType()).willReturn(variantTypeModel);
        given(colourModel.getDisplay()).willReturn(Boolean.TRUE);
        given(variantService.getVariantAttributesForVariantType(variantTypeModel)).willReturn(
                Arrays.asList(variantAttributeDescriptorModel));

        final String attributeValue = "productSize";
        given(variantAttributeDescriptorModel.getQualifier()).willReturn(attributeValue);
        given(variantAttributeDescriptorModel.getName()).willReturn("Product Size");
        given(variantService.getVariantAttributeValue(colourVariant, attributeValue)).willReturn(colourModel);
        given(colourModel.getName()).willReturn("colour");
        given(colourVariant.getCode()).willReturn("code");
        targetVariantOptionDataPopulator.populate(colourVariant, variantOptionData);

        assertThat(variantOptionData).isNotNull();
        assertThat(variantOptionData.getCode()).isEqualTo("code");
        final Collection<VariantOptionQualifierData> variantOptionQualifiers = variantOptionData
                .getVariantOptionQualifiers();
        assertThat(variantOptionQualifiers).isNotNull().isNotEmpty().hasSize(1);
        final VariantOptionQualifierData variantOptionQualifierData = variantOptionQualifiers.iterator().next();
        assertThat(variantOptionQualifierData).isInstanceOf(TargetVariantOptionQualifierData.class);
        final TargetVariantOptionQualifierData targetVariantOptionQualifierData = (TargetVariantOptionQualifierData)variantOptionQualifierData;
        final Object objectValue = targetVariantOptionQualifierData.getObjectValue();
        assertThat(objectValue).isNull();
        assertThat(variantOptionQualifierData.getValue()).isNotNull().isNotEmpty().isEqualTo("colour");
    }
}
