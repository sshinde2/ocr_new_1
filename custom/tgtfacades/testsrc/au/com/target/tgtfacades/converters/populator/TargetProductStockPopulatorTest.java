/**
 *
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.internal.model.attribute.DynamicAttributesProvider;
import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author Benoit Vanalderweireldt
 *
 */
@UnitTest
public class TargetProductStockPopulatorTest {

    @InjectMocks
    private final TargetProductStockPopulator<ProductModel, TargetProductData> targetProductStockPopulator = new TargetProductStockPopulator<ProductModel, TargetProductData>();

    @Mock
    private TargetWarehouseDao targetWarehouseDao;
    @Mock
    private TargetWarehouseService targetWarehouseService;
    @Mock
    private TargetStockService targetStockService;
    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;
    @Mock
    private DynamicAttributesProvider dynamicAttributesProvider;
    @Mock
    private PurchaseOptionConfigModel buyNowConfigModel;
    @Mock
    private PurchaseOptionConfigModel layByConfigModel;
    @Mock
    private PurchaseOptionConfigModel longTermLayByConfigModel;
    @Mock
    private PurchaseOptionModel buyNowModel;
    @Mock
    private PurchaseOptionModel layByModel;
    @Mock
    private PurchaseOptionModel longTermLayByModel;
    @Mock
    private WarehouseModel buyNowWarehouseModel;
    @Mock
    private WarehouseModel layByWarehouseModel;
    @Mock
    private TargetProductModel productModel;
    @Mock
    private TargetCommercePriceService targetCommercePriceService;
    @Mock
    private TargetColourVariantProductModel variantProductModelColor;
    @Mock
    private TargetSizeVariantProductModel variantProductModelSize;
    @Mock
    private AbstractTargetVariantProductModel colourVariant;
    @Mock
    private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;

    private TargetProductData targetProductData;
    private final Set<PurchaseOptionConfigModel> purchasesOptionConfigModel = new HashSet<>();
    private final List<WarehouseModel> wareHouses = new ArrayList<>();
    private final StockData stockData = new StockData();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        purchasesOptionConfigModel.add(buyNowConfigModel);
        purchasesOptionConfigModel.add(layByConfigModel);
        purchasesOptionConfigModel.add(longTermLayByConfigModel);

        wareHouses.add(buyNowWarehouseModel);
        wareHouses.add(layByWarehouseModel);

        when(buyNowModel.getCode()).thenReturn("BuyNow");
        when(layByModel.getCode()).thenReturn("LayBy");
        when(longTermLayByModel.getCode()).thenReturn("LongTermLayBy");

        when(buyNowConfigModel.getPurchaseOption()).thenReturn(buyNowModel);
        when(layByConfigModel.getPurchaseOption()).thenReturn(layByModel);
        when(longTermLayByConfigModel.getPurchaseOption()).thenReturn(longTermLayByModel);

        final AbstractItemModel abstractItemModel = null;
        given(dynamicAttributesProvider.get(Mockito.eq(abstractItemModel), anyString()))
                .willReturn(
                        Boolean.TRUE);

        given(buyNowConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(layByConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(longTermLayByConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(targetPurchaseOptionHelper.getPurchaseOptionConfigModelFromOptionCode("buynow")).willReturn(
                buyNowConfigModel);
        given(targetPurchaseOptionHelper.getPurchaseOptionConfigModelFromOptionCode("layby")).willReturn(
                layByConfigModel);
        given(targetPurchaseOptionHelper.getPurchaseOptionConfigModelFromOptionCode("longtermlayby"))
                .willReturn(longTermLayByConfigModel);
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(buyNowModel);

        given(productModel.getVariants()).willReturn(Collections.EMPTY_LIST);

        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(buyNowWarehouseModel);
        given(buyNowWarehouseModel.getPurchaseOptionConfigs()).willReturn(purchasesOptionConfigModel);
        given(layByWarehouseModel.getPurchaseOptionConfigs()).willReturn(purchasesOptionConfigModel);

        final PriceRangeInformation priceRangeInformation = Mockito.mock(PriceRangeInformation.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct((ProductModel)Mockito.any()))
                .willReturn(priceRangeInformation);

        final Set<PurchaseOptionModel> purchaseOptionSet = new HashSet<>();
        given(productModel.getPurchaseOptions()).willReturn(purchaseOptionSet);
        final PurchaseOptionModel buyNow = Mockito.mock(PurchaseOptionModel.class);
        given(buyNow.getCode()).willReturn("BuyNow");
        purchaseOptionSet.add(buyNow);
        final PurchaseOptionModel layby = Mockito.mock(PurchaseOptionModel.class);
        given(layby.getCode()).willReturn("LayBy");
        purchaseOptionSet.add(layby);
        final PurchaseOptionModel longTermLayby = Mockito.mock(PurchaseOptionModel.class);
        given(longTermLayby.getCode()).willReturn("LongTermLayBy");
        purchaseOptionSet.add(longTermLayby);

        given(colourVariant.getBaseProduct()).willReturn(productModel);

        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        when(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK)).thenReturn(stockData);
    }

    @Test
    public void populateWithAllPurchaseOptionButNoStock() {
        given(targetWarehouseDao.getWarehouses(anyString())).willReturn(wareHouses);

        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        stockData.setStockLevel(Long.valueOf(0L));
        when(
                targetStockService.getStockLevelAndStatusAmount(any(ProductModel.class)))
                        .thenReturn(stockData);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(productModel, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isFalse();
    }

    @Test
    public void populateWithAllPurchaseOptionAndStock() {
        given(targetWarehouseDao.getWarehouses(anyString())).willReturn(wareHouses);

        when(
                targetStockService.getProductStatus(any(ProductModel.class),
                        any(WarehouseModel.class)))
                                .thenReturn(StockLevelStatus.INSTOCK);
        final StockData targetStockInWarehouses = new StockData();
        when(targetStockService
                .getStockLevelAndStatusAmount(productModel)).thenReturn(targetStockInWarehouses);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(productModel, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isTrue();
    }

    @Test
    public void populateWithAllPurchaseOptionAndStockForColourVariant() {
        given(targetWarehouseDao.getWarehouses(anyString())).willReturn(wareHouses);

        final StockData targetStockInWarehouses = new StockData();
        targetStockInWarehouses.setStockLevel(Long.valueOf(10L));
        targetStockInWarehouses.setStockLevelStatus(StockLevelStatus.INSTOCK);
        when(targetStockService
                .getStockLevelAndStatusAmount(colourVariant)).thenReturn(targetStockInWarehouses);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(colourVariant, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isTrue();
    }

    @Test
    public void populateWithAllPurchaseOptionAndStockForDisplayOnlyColourVariant() {
        given(targetWarehouseDao.getWarehouses(anyString())).willReturn(wareHouses);
        given(colourVariant.getDisplayOnly()).willReturn(Boolean.TRUE);

        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(colourVariant, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isFalse();
        assertThat(targetProductData.getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void populateWithAllPurchaseOptionAndStockFromVariant() {
        final Collection<VariantProductModel> variantProductModels = new ArrayList<>();
        variantProductModels.add(variantProductModelColor);
        given(productModel.getVariants()).willReturn(variantProductModels);
        given(variantProductModelColor.getCode()).willReturn("false");

        final Collection<VariantProductModel> variantProductModelsSizes = new ArrayList<>();
        variantProductModelsSizes.add(variantProductModelSize);
        given(variantProductModelColor.getVariants()).willReturn(variantProductModelsSizes);
        given(variantProductModelSize.getVariants()).willReturn(null);
        given(variantProductModelSize.getCode()).willReturn("true");

        given(targetWarehouseDao.getWarehouses("true")).willReturn(wareHouses);
        when(
                targetStockService.getProductStatus(any(ProductModel.class)))
                        .thenReturn(StockLevelStatus.INSTOCK);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(productModel, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isTrue();
    }

    @Test
    public void populateWithAllPurchaseOptionAndStockFromDisplayOnlyVariant() {
        final Collection<VariantProductModel> variantProductModels = new ArrayList<>();
        variantProductModels.add(variantProductModelColor);
        given(productModel.getVariants()).willReturn(variantProductModels);
        given(variantProductModelColor.getCode()).willReturn("false");

        final Collection<VariantProductModel> variantProductModelsSizes = new ArrayList<>();
        variantProductModelsSizes.add(variantProductModelSize);
        given(variantProductModelColor.getVariants()).willReturn(variantProductModelsSizes);
        given(variantProductModelSize.getVariants()).willReturn(null);
        given(variantProductModelSize.getCode()).willReturn("true");
        given(variantProductModelSize.getDisplayOnly()).willReturn(Boolean.TRUE);

        given(targetWarehouseDao.getWarehouses("true")).willReturn(wareHouses);
        when(
                targetStockService.getProductStatus(any(ProductModel.class),
                        any(WarehouseModel.class)))
                                .thenReturn(StockLevelStatus.INSTOCK);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(productModel, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isFalse();
        assertThat(targetProductData.getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void populateWithAllPurchaseOptionAndStockWithVariant() {
        given(productModel.getVariants()).willReturn(null);
        given(variantProductModelColor.getCode()).willReturn("false");

        final Collection<VariantProductModel> variantProductModelsSizes = new ArrayList<>();
        variantProductModelsSizes.add(variantProductModelSize);
        given(variantProductModelColor.getVariants()).willReturn(variantProductModelsSizes);
        given(variantProductModelSize.getVariants()).willReturn(null);
        given(variantProductModelSize.getCode()).willReturn("true");

        given(targetWarehouseDao.getWarehouses("true")).willReturn(wareHouses);
        when(
                targetStockService.getProductStatus(any(ProductModel.class)))
                        .thenReturn(StockLevelStatus.INSTOCK);
        final StockData targetStockInWarehouses = new StockData();
        targetStockInWarehouses.setStockLevel(Long.valueOf(10L));
        targetStockInWarehouses.setStockLevelStatus(StockLevelStatus.INSTOCK);
        when(targetStockService
                .getStockLevelAndStatusAmount(productModel)).thenReturn(targetStockInWarehouses);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(productModel, targetProductData);

        assertThat(targetProductData.getAvailable().get("BuyNow")).isTrue();
        assertThat(targetProductData.isInStock()).isTrue();
        assertThat(targetProductData.getStock().getStockLevel()).isEqualTo(10L);
        assertThat(targetProductData.getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void populateWithAllPurchaseOptionAndStockWithVariantZeroStock() {
        given(colourVariant.getCode()).willReturn("false");
        given(colourVariant.getBaseProduct()).willReturn(productModel);

        final Collection<VariantProductModel> variantProductModelsSizes = new ArrayList<>();
        variantProductModelsSizes.add(variantProductModelSize);
        given(colourVariant.getVariants()).willReturn(variantProductModelsSizes);
        given(variantProductModelSize.getVariants()).willReturn(null);
        given(variantProductModelSize.getCode()).willReturn("true");

        given(targetWarehouseDao.getWarehouses("true")).willReturn(wareHouses);
        when(
                targetStockService.getProductStatus(any(ProductModel.class),
                        any(WarehouseModel.class)))
                                .thenReturn(StockLevelStatus.OUTOFSTOCK);
        final StockData targetStockInWarehouses = stockData;
        targetStockInWarehouses.setStockLevel(Long.valueOf(0L));
        when(targetStockService
                .getStockLevelAndStatusAmount(variantProductModelSize)).thenReturn(
                        targetStockInWarehouses);
        when(targetStockService.getProductStatus(variantProductModelSize)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        targetProductData = new TargetProductData();
        targetProductStockPopulator.populate(colourVariant, targetProductData);

        assertThat(targetProductData.getStock().getStockLevel()).isNull();
        assertThat(targetProductData.getStock().getStockLevelStatus())
                .isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    public void verifyLowStockForTargetVariantProductListerData() {
        given(targetWarehouseDao.getWarehouses(anyString())).willReturn(wareHouses);
        when(
                targetStockService.getProductStatus(any(ProductModel.class),
                        any(WarehouseModel.class)))
                                .thenReturn(StockLevelStatus.LOWSTOCK);
        final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();
        targetProductStockPopulator.populate(productModel, targetVariantProductListerData);

        assertThat(targetVariantProductListerData.isInStock()).isTrue();
        assertThat(targetProductData.getStock().getStockLevelStatus())
                .isEqualTo(StockLevelStatus.LOWSTOCK);
    }
}
