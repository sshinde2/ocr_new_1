/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;


/**
 * @author asingh78
 * 
 */
@UnitTest
public class AbstractTargetVariantProductPopulatorTest {

    private AbstractTargetVariantProductPopulator abstractTargetProductPopulator = null;

    @Before
    public void setUp() {
        abstractTargetProductPopulator = new TargetVariantProductPopulatorTest();

    }

    @Test
    public void testPopulatePromotionStatusWithDifferentCombination() {

        final Boolean onlineExclusive = Boolean.TRUE;
        final Boolean targetExclusive = Boolean.TRUE;
        final Boolean clearance = Boolean.FALSE;
        final Boolean essentials = Boolean.FALSE;
        final Boolean hotProduct = Boolean.TRUE;
        final Boolean newLowerPrice = Boolean.TRUE;

        final TargetColourVariantProductModel mockSource = mock(TargetColourVariantProductModel.class);
        given(mockSource.getOnlineExclusive()).willReturn(onlineExclusive);
        given(mockSource.getTargetExclusive()).willReturn(targetExclusive);
        given(mockSource.getClearance()).willReturn(clearance);
        given(mockSource.getEssential()).willReturn(essentials);
        given(mockSource.getHotProduct()).willReturn(hotProduct);
        given(mockSource.getNewLowerPriceFlag()).willReturn(newLowerPrice);

        final List<TargetPromotionStatusEnum> promotionStatuses = abstractTargetProductPopulator
                .getPromotionStatuses(mockSource);


        Assert.assertEquals(onlineExclusive.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE));
        Assert.assertEquals(targetExclusive.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.TARGET_EXCLUSIVE));
        Assert.assertEquals(clearance.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.CLEARANCE));
        Assert.assertEquals(essentials.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.ESSENTIALS));
        Assert.assertEquals(hotProduct.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.HOT_PRODUCT));
        Assert.assertEquals(hotProduct.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.NEW_LOWER_PRICE));

    }


    @Test
    public void testPopulatePromotionStatusWithAllTrue() {

        final Boolean onlineExclusive = Boolean.TRUE;
        final Boolean targetExclusive = Boolean.TRUE;
        final Boolean clearance = Boolean.TRUE;
        final Boolean essentials = Boolean.TRUE;
        final Boolean hotProduct = Boolean.TRUE;
        final Boolean newLowerPrice = Boolean.TRUE;

        final TargetColourVariantProductModel mockSource = mock(TargetColourVariantProductModel.class);
        given(mockSource.getOnlineExclusive()).willReturn(onlineExclusive);
        given(mockSource.getTargetExclusive()).willReturn(targetExclusive);
        given(mockSource.getClearance()).willReturn(clearance);
        given(mockSource.getEssential()).willReturn(essentials);
        given(mockSource.getHotProduct()).willReturn(hotProduct);
        given(mockSource.getNewLowerPriceFlag()).willReturn(newLowerPrice);

        final List<TargetPromotionStatusEnum> promotionStatuses = abstractTargetProductPopulator
                .getPromotionStatuses(mockSource);


        Assert.assertEquals(onlineExclusive.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE));
        Assert.assertEquals(targetExclusive.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.TARGET_EXCLUSIVE));
        Assert.assertEquals(clearance.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.CLEARANCE));
        Assert.assertEquals(essentials.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.ESSENTIALS));
        Assert.assertEquals(hotProduct.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.HOT_PRODUCT));
        Assert.assertEquals(hotProduct.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.NEW_LOWER_PRICE));
        Assert.assertEquals(promotionStatuses.get(0), TargetPromotionStatusEnum.NEW_LOWER_PRICE);

    }

    @Test
    public void testPopulatePromotionStatusWithAllFalse() {

        final Boolean onlineExclusive = Boolean.FALSE;
        final Boolean targetExclusive = Boolean.FALSE;
        final Boolean clearance = Boolean.FALSE;
        final Boolean essentials = Boolean.FALSE;
        final Boolean hotProduct = Boolean.FALSE;
        final Boolean newLowerPrice = Boolean.FALSE;

        final TargetColourVariantProductModel mockSource = mock(TargetColourVariantProductModel.class);
        given(mockSource.getOnlineExclusive()).willReturn(onlineExclusive);
        given(mockSource.getTargetExclusive()).willReturn(targetExclusive);
        given(mockSource.getClearance()).willReturn(clearance);
        given(mockSource.getEssential()).willReturn(essentials);
        given(mockSource.getHotProduct()).willReturn(hotProduct);
        given(mockSource.getNewLowerPriceFlag()).willReturn(newLowerPrice);


        final List<TargetPromotionStatusEnum> promotionStatuses = abstractTargetProductPopulator
                .getPromotionStatuses(mockSource);


        Assert.assertEquals(onlineExclusive.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE));
        Assert.assertEquals(targetExclusive.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.TARGET_EXCLUSIVE));
        Assert.assertEquals(clearance.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.CLEARANCE));
        Assert.assertEquals(essentials.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.ESSENTIALS));
        Assert.assertEquals(hotProduct.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.HOT_PRODUCT));
        Assert.assertEquals(hotProduct.booleanValue(),
                promotionStatuses.contains(TargetPromotionStatusEnum.NEW_LOWER_PRICE));

    }


    private class TargetVariantProductPopulatorTest extends AbstractTargetVariantProductPopulator {


        /* (non-Javadoc)
         * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
         */
        @Override
        public void populate(final Object source, final Object target) throws ConversionException {
            // do nothing use only for test

        }

    }
}
