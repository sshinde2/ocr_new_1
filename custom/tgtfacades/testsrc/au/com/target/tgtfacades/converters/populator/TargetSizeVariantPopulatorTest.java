/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.enums.TargetManchesterSizeEnum;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;
import au.com.target.tgtfacades.product.strategy.ShowStoreStockStrategy;


/**
 * Test suite for {@link TargetSizeVariantPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSizeVariantPopulatorTest {

    @Spy
    private final TargetSizeVariantPopulator targetSizeVariantPopulator = new TargetSizeVariantPopulator();

    @Mock
    private TargetSizeVariantProductModel mockTargetSizeVariantProductModel;

    @Mock
    private TargetProductFacade productFacade;

    @Mock
    private TargetProductModel baseProductModel;

    @Mock
    private ShowStoreStockStrategy mockShowStoreStockStrategy;

    @Before
    public void setUp() {
        willReturn(productFacade).given(targetSizeVariantPopulator).getProductFacade();
        given(baseProductModel.getCode()).willReturn("testBaseProductCode");
        given(productFacade.getBaseTargetProduct(mockTargetSizeVariantProductModel)).willReturn(baseProductModel);
    }

    /**
     * Verifies that populator sets all necessary fields to target model.
     */
    @Test
    public void testPopulate() {
        final String code = "01928374656";
        final String size = "XXL";
        final String sizeType = "babysize";
        final String displayName = "Product1";
        final SizeTypeModel sizeTypeModel = new SizeTypeModel();
        sizeTypeModel.setCode(sizeType);
        sizeTypeModel.setIsSizeChartDisplay(Boolean.TRUE);
        sizeTypeModel.setIsDefault(Boolean.TRUE);
        given(baseProductModel.getShowStoreStock()).willReturn(Boolean.TRUE);
        given(mockTargetSizeVariantProductModel.getSize()).willReturn(size);
        given(mockTargetSizeVariantProductModel.getSizeType()).willReturn(sizeType);
        given(mockTargetSizeVariantProductModel.getSizeClassification()).willReturn(sizeTypeModel);
        given(mockTargetSizeVariantProductModel.getDisplayName()).willReturn(displayName);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn(code);
        given(mockTargetSizeVariantProductModel.getDisplayOnly()).willReturn(Boolean.TRUE);

        final TargetColourVariantProductModel mockTargetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProductModel.getOnlineExclusive()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProductModel.getCode()).willReturn("testColourVariantProductCode");
        willReturn(Boolean.TRUE).given(mockTargetColourVariantProductModel).isAssorted();
        willReturn(Boolean.TRUE).given(mockTargetColourVariantProductModel).isExcludeForAfterpay();
        willReturn(Boolean.TRUE).given(mockTargetColourVariantProductModel).isExcludeForZipPayment();

        given(mockTargetSizeVariantProductModel.getBaseProduct()).willReturn(mockTargetColourVariantProductModel);

        final TargetProductData productData = new TargetProductData();

        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, productData);

        assertThat(productData.getName()).isEqualTo(displayName);
        assertThat(productData.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(productData.isSizeVariant()).isTrue();

        final List<TargetPromotionStatusEnum> promotionStatuses = productData.getPromotionStatuses();
        assertThat(promotionStatuses).isNotNull();
        assertThat(promotionStatuses.size()).isEqualTo(1);
        assertThat(promotionStatuses).contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE);
        assertThat(productData.isOnlineExclusive()).isTrue();
        assertThat(productData.getBaseProductCode()).isEqualTo("testBaseProductCode");
        assertThat(productData.getColourVariantCode()).isEqualTo("testColourVariantProductCode");
        assertThat(productData.isShowStoreStockForProduct()).isTrue();
        assertThat(productData.isDisplayOnly()).isTrue();
        assertThat(productData.isAssorted()).isTrue();
        assertThat(productData.isExcludeForAfterpay()).isTrue();
        assertThat(productData.isExcludeForZipPayment()).isTrue();
    }

    @Test
    public void testPopulateWithoutBaseProduct() {
        final String code = "01928374656";
        final String size = "XXL";
        final String sizeType = "babysize";
        final String displayName = "Product1";
        final SizeTypeModel sizeTypeModel = new SizeTypeModel();
        sizeTypeModel.setCode(sizeType);
        sizeTypeModel.setIsSizeChartDisplay(Boolean.TRUE);
        sizeTypeModel.setIsDefault(Boolean.TRUE);

        given(mockTargetSizeVariantProductModel.getSize()).willReturn(size);
        given(mockTargetSizeVariantProductModel.getSizeClassification()).willReturn(sizeTypeModel);
        given(mockTargetSizeVariantProductModel.getDisplayName()).willReturn(displayName);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn(code);

        given(productFacade.getBaseTargetProduct(mockTargetSizeVariantProductModel)).willReturn(null);

        final TargetProductData productData = new TargetProductData();

        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, productData);

        assertThat(productData.getName()).isEqualTo(displayName);
        assertThat(productData.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(productData.isSizeVariant()).isTrue();

        final List<TargetPromotionStatusEnum> promotionStatuses = productData.getPromotionStatuses();
        assertThat(promotionStatuses).isNull();

        assertThat(productData.isOnlineExclusive()).isFalse();
        assertThat(productData.getBaseProductCode()).isNull();
        assertThat(productData.getColourVariantCode()).isNull();
        assertThat(productData.isShowStoreStockForProduct()).isFalse();
    }

    @Test
    public void testPopulateWithAlternatePromotionStatus() {
        final String code = "01928374656";
        final String size = "XXL";
        final String displayName = "Product1";
        given(baseProductModel.getShowStoreStock()).willReturn(null);
        given(mockTargetSizeVariantProductModel.getSize()).willReturn(size);
        given(mockTargetSizeVariantProductModel.getDisplayName()).willReturn(displayName);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn(code);

        final TargetColourVariantProductModel mockTargetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProductModel.getTargetExclusive()).willReturn(Boolean.TRUE);

        given(mockTargetSizeVariantProductModel.getBaseProduct()).willReturn(mockTargetColourVariantProductModel);

        final TargetProductData productData = new TargetProductData();

        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, productData);

        assertThat(productData.getName()).isEqualTo(displayName);
        assertThat(productData.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(productData.isSizeVariant()).isTrue();

        final List<TargetPromotionStatusEnum> promotionStatuses = productData.getPromotionStatuses();
        assertThat(promotionStatuses).isNotNull();
        assertThat(promotionStatuses.size()).isEqualTo(1);
        assertThat(promotionStatuses).contains(TargetPromotionStatusEnum.TARGET_EXCLUSIVE);
        assertThat(productData.isShowStoreStockForProduct()).isFalse();
        assertThat(productData.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testPopulateWithNoPromotionStatus() {
        final String code = "01928374656";
        final String size = "XXL";
        final String displayName = "Product1";

        given(mockTargetSizeVariantProductModel.getSize()).willReturn(size);
        given(mockTargetSizeVariantProductModel.getDisplayName()).willReturn(displayName);
        given(mockTargetSizeVariantProductModel.getCode()).willReturn(code);

        final TargetColourVariantProductModel mockTargetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);

        given(mockTargetSizeVariantProductModel.getBaseProduct()).willReturn(mockTargetColourVariantProductModel);

        final TargetProductData productData = new TargetProductData();

        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, productData);

        assertThat(productData.getName()).isEqualTo(displayName);
        assertThat(productData.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(productData.isSizeVariant()).isTrue();

        final List<TargetPromotionStatusEnum> promotionStatuses = productData.getPromotionStatuses();
        assertThat(promotionStatuses).isNotNull();
        assertThat(promotionStatuses).isEmpty();

        assertThat(productData.isOnlineExclusive()).isFalse();
    }

    /**
     * Verifies behavior with {@code null} mockTargetSizeVariantProductModel argument.
     */
    @Test
    public void testPopulateWithNullSource() {
        final TargetProductData mockProductData = mock(TargetProductData.class);

        targetSizeVariantPopulator.populate(null, mockProductData);
        verifyZeroInteractions(mockProductData);
    }

    /**
     * Verifies behavior with {@code null} target argument.
     */
    @Test
    public void testPopulateWithNullTarget() {
        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, null);
        verifyZeroInteractions(mockTargetSizeVariantProductModel);
    }

    /**
     * Verifies that populator sets manchester size attribute when mockTargetSizeVariantProductModel model has specific
     * size type.
     */
    @Test
    public void testPopulateManchesterSizeData() {
        final String size = TargetManchesterSizeEnum.SINGLE_BED.getValue();

        given(mockTargetSizeVariantProductModel.getSize()).willReturn(size);

        final TargetProductData productData = new TargetProductData();
        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, productData);

        assertThat(productData.isHasSizeChart()).isFalse();
    }

    @Test
    public void testPopulateNewLowerPriceStartDateWhenFlagIsTrue() {
        final Date newLowerPriceStartDate = new Date();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);

        given(mockTargetSizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(colourVariantProductModel.getNewLowerPriceStartDate()).willReturn(newLowerPriceStartDate);
        given(colourVariantProductModel.getNewLowerPriceFlag()).willReturn(Boolean.TRUE);
        final TargetProductData target = new TargetProductData();

        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, target);

        assertThat(newLowerPriceStartDate).isEqualTo(target.getNewLowerPriceStartDate());
    }

    @Test
    public void testPopulateNewLowerPriceStartDateWhenFlagIsFalse() {
        final Date newLowerPriceStartDate = new Date();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);

        given(mockTargetSizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(colourVariantProductModel.getNewLowerPriceStartDate()).willReturn(newLowerPriceStartDate);
        given(colourVariantProductModel.getNewLowerPriceFlag()).willReturn(Boolean.FALSE);
        final TargetProductData target = new TargetProductData();

        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, target);

        assertThat(target.getNewLowerPriceStartDate()).isNull();
    }

    @Test
    public void testPopulateWithNotShowStockStock() {
        willReturn(Boolean.FALSE).given(mockShowStoreStockStrategy).showStoreStockForProduct(baseProductModel);

        final TargetProductData productData = new TargetProductData();
        targetSizeVariantPopulator.populate(mockTargetSizeVariantProductModel, productData);

        assertThat(productData.isShowStoreStockForProduct()).isFalse();
    }
}
