/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;


/**
 * @author htan3
 *
 */
@UnitTest
public class ResponseTest {

    @Test
    public void testToStringWithSuccuessResponse() {
        final Response response = new Response(true);
        Assertions.assertThat(response.toString()).isEqualTo("SUCCESS:true");
    }

    @Test
    public void testToStringWithFailedResponse() {
        final Response response = new Response(false);
        Assertions.assertThat(response.toString()).isEqualTo("SUCCESS:false");
    }

    @Test
    public void testToStringWithFailedResponseWithError() {
        final Response response = new Response(false);
        final BaseResponseData data = new BaseResponseData();
        final Error error = new Error("code");
        data.setError(error);
        response.setData(data);
        Assertions.assertThat(response.toString())
                .isEqualTo("SUCCESS:false ERROR=code(null)  , "
                        + "requestUri=null ,\n StackTrace:null");
    }

    @Test
    public void testToStringWithFailedResponseWithNoError() {
        final Response response = new Response(false);
        final BaseResponseData data = new BaseResponseData();
        response.setData(data);
        Assertions.assertThat(response.toString()).isEqualTo("SUCCESS:false");
    }
}
