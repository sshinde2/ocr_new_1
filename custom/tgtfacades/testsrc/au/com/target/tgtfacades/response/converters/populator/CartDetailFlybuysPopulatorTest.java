/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.FlybuysDiscountData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.FlybuysResponseData;


/**
 * Unit tests for CartDetailFlybuysPopulator.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartDetailFlybuysPopulatorTest {

    private static final String FLYBUYS_CODE = "1234567890";

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;

    @InjectMocks
    private final CartDetailFlybuysPopulator populator = new CartDetailFlybuysPopulator();

    @Mock
    private FlybuysDiscountData testFlybuysDiscountData;

    @Mock
    private FlybuysRedemptionData mockFlybuysRedemptionData;

    @Mock
    private TargetCartData mockTargetCartData;

    @Test
    public void whenCartHasNoFlybuysNumber() {
        final TargetCartData source = new TargetCartData();
        source.setFlybuysDiscountData(testFlybuysDiscountData);
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(source, target);
        assertThat(target.getFlybuysData()).isNull();
        assertThat(target.getFlybuysDiscountData()).isNull();
    }

    @Test
    public void testCartHasFlybuysNumberWithCanRedeemPoints() {
        final TargetCartData source = new TargetCartData();
        source.setFlybuysDiscountData(testFlybuysDiscountData);
        source.setFlybuysNumber(FLYBUYS_CODE);
        final CartDetailResponseData target = new CartDetailResponseData();
        final FlybuysDiscountDenialDto denialData = new FlybuysDiscountDenialDto();
        denialData.setDenied(false);
        given(flybuysDiscountFacade.getFlybuysDiscountAllowed()).willReturn(denialData);
        populator.populate(source, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(flybuysResponseData.isCanRedeemPoints()).isTrue();
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    @Test
    public void testCartHasFlybuysNumberWithCanRedeemPointsFromNullDenialDto() {
        final TargetCartData source = new TargetCartData();
        source.setFlybuysDiscountData(testFlybuysDiscountData);
        source.setFlybuysNumber(FLYBUYS_CODE);
        final CartDetailResponseData target = new CartDetailResponseData();
        given(flybuysDiscountFacade.getFlybuysDiscountAllowed()).willReturn(null);
        populator.populate(source, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(flybuysResponseData.isCanRedeemPoints()).isTrue();
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    @Test
    public void testCartHasFlybuysNumberWithNotCanRedeemPoints() {
        final TargetCartData source = new TargetCartData();
        source.setFlybuysDiscountData(testFlybuysDiscountData);
        source.setFlybuysNumber(FLYBUYS_CODE);
        final CartDetailResponseData target = new CartDetailResponseData();
        final FlybuysDiscountDenialDto denialData = new FlybuysDiscountDenialDto();
        denialData.setDenied(true);
        given(flybuysDiscountFacade.getFlybuysDiscountAllowed()).willReturn(denialData);
        populator.populate(source, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(flybuysResponseData.isCanRedeemPoints()).isFalse();
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    @Test
    public void testCartHasFlybuysNumberWithFlybuysRedeemOptions() {
        final TargetCartData source = new TargetCartData();
        source.setFlybuysDiscountData(testFlybuysDiscountData);
        source.setFlybuysNumber(FLYBUYS_CODE);
        final CartDetailResponseData target = new CartDetailResponseData();
        final FlybuysDiscountDenialDto denialData = new FlybuysDiscountDenialDto();
        denialData.setDenied(false);
        given(flybuysDiscountFacade.getFlybuysDiscountAllowed()).willReturn(denialData);
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(mockFlybuysRedemptionData);
        populator.populate(source, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(flybuysResponseData.isCanRedeemPoints()).isTrue();
        assertThat(target.getFlybuysRedemptionData()).isEqualTo(mockFlybuysRedemptionData);
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    @Test
    public void testCartHasFlybuysNumberWithoutFlybuysRedeemOptions() {
        willReturn(testFlybuysDiscountData).given(mockTargetCartData).getFlybuysDiscountData();
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        final CartDetailResponseData target = new CartDetailResponseData();
        final FlybuysDiscountDenialDto denialData = new FlybuysDiscountDenialDto();
        denialData.setDenied(true);
        given(flybuysDiscountFacade.getFlybuysDiscountAllowed()).willReturn(denialData);
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(mockFlybuysRedemptionData);
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(flybuysResponseData.isCanRedeemPoints()).isFalse();
        assertThat(target.getFlybuysRedemptionData()).isNull();
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    /**
     * Method to verify flybuys points can be redeemable or not if the cart contains only gift cards. In this case
     * customer can not redeem points.
     */
    @Test
    public void testPopulateCanRedeemPointsWithCartHaveGiftCardOnly() {
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        willReturn(mockFlybuysRedemptionData).given(flybuysDiscountFacade).getFlybuysRedemptionDataForCheckoutCart();
        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).doesCartHaveGiftCardOnly();
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.isCanRedeemPoints()).isFalse();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(target.getFlybuysRedemptionData()).isNull();
        assertThat(target.getFlybuysDiscountData()).isNull();
    }

    /**
     * Method to verify flybuys points can be redeemable or not if the cart contains gift card mixed basket. In this
     * case customer can redeem points.
     */
    @Test
    public void testPopulateCanRedeemPointsWithGiftCardMixedBasket() {
        willReturn(testFlybuysDiscountData).given(mockTargetCartData).getFlybuysDiscountData();
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        willReturn(mockFlybuysRedemptionData).given(flybuysDiscountFacade).getFlybuysRedemptionDataForCheckoutCart();
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).doesCartHaveGiftCardOnly();
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.isCanRedeemPoints()).isTrue();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(target.getFlybuysRedemptionData()).isEqualTo(mockFlybuysRedemptionData);
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    /**
     * Method will verify to false when the cart contains physical gift card(s) only. Here user can not redeem points.
     */
    @Test
    public void testPopulateDoesCartHavePhysicalGiftCardOnlyFalse() {
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        willReturn(mockFlybuysRedemptionData).given(flybuysDiscountFacade).getFlybuysRedemptionDataForCheckoutCart();
        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).doesCartHavePhysicalGiftCardOnly();
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.isCanRedeemPoints()).isFalse();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(target.getFlybuysRedemptionData()).isNull();
        assertThat(target.getFlybuysDiscountData()).isNull();
    }

    /**
     * Method will verify to true when the cart does not contains physical gift card(s) only. Here user can redeem
     * points.
     */
    @Test
    public void testPopulateDoesCartHavePhysicalGiftCardOnlyTrue() {
        willReturn(testFlybuysDiscountData).given(mockTargetCartData).getFlybuysDiscountData();
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        willReturn(mockFlybuysRedemptionData).given(flybuysDiscountFacade).getFlybuysRedemptionDataForCheckoutCart();
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).doesCartHavePhysicalGiftCardOnly();
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.isCanRedeemPoints()).isTrue();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(target.getFlybuysRedemptionData()).isEqualTo(mockFlybuysRedemptionData);
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }

    /**
     * Method will verify to false when the cart contains both physical and digital gift card(s) only. Here user can not
     * redeem points.
     */
    @Test
    public void testPopulateDoesCartHavePhysicalAndDigitalGiftCardsOnlyFalse() {
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        willReturn(mockFlybuysRedemptionData).given(flybuysDiscountFacade).getFlybuysRedemptionDataForCheckoutCart();
        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).doesCartHavePhysicalAndDigitalGiftCardsOnly();
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.isCanRedeemPoints()).isFalse();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(target.getFlybuysRedemptionData()).isNull();
        assertThat(target.getFlybuysDiscountData()).isNull();
    }

    /**
     * Method will verify to true when the cart does not contains both physical and digital gift card(s) only. Here user
     * can redeem points.
     */
    @Test
    public void testPopulateDoesCartHavePhysicalAndDigitalGiftCardsOnlyTrue() {
        willReturn(testFlybuysDiscountData).given(mockTargetCartData).getFlybuysDiscountData();
        willReturn(FLYBUYS_CODE).given(mockTargetCartData).getFlybuysNumber();
        willReturn(mockFlybuysRedemptionData).given(flybuysDiscountFacade).getFlybuysRedemptionDataForCheckoutCart();
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).doesCartHavePhysicalAndDigitalGiftCardsOnly();
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(mockTargetCartData, target);
        final FlybuysResponseData flybuysResponseData = target.getFlybuysData();
        assertThat(flybuysResponseData.isCanRedeemPoints()).isTrue();
        assertThat(flybuysResponseData.getCode()).isEqualTo(FLYBUYS_CODE);
        assertThat(target.getFlybuysRedemptionData()).isEqualTo(mockFlybuysRedemptionData);
        assertThat(target.getFlybuysDiscountData()).isEqualTo(testFlybuysDiscountData);
    }
}
