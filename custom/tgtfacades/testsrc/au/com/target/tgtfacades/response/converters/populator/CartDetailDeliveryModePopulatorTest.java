/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;

import com.google.common.collect.ImmutableList;


/**
 * @author htan3
 *
 */
@UnitTest
public class CartDetailDeliveryModePopulatorTest {

    private final CartDetailDeliveryModePopulator populator = new CartDetailDeliveryModePopulator();
    private TargetCartData source;
    private CartDetailResponseData target;
    private DeliveryModeData deliveryMode;
    private final TargetZoneDeliveryModeData deliveryModeInfo = mock(TargetZoneDeliveryModeData.class);

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
        deliveryMode = new DeliveryModeData();
        source.setDeliveryMode(deliveryMode);
        source.setDeliveryModes(ImmutableList.of(deliveryModeInfo));
        given(deliveryModeInfo.getCode()).willReturn("click-and-collect");
    }

    @Test
    public void testGetDeliveryModeInfoForCartWithoutDeliveryMode() {
        source.setDeliveryMode(null);
        populator.populate(source, target);
        assertThat(target.getDeliveryMode()).isNull();
    }

    @Test
    public void testGetDeliveryModeInfoForCartWithoutDeliveryModes() {
        source.setDeliveryModes(null);
        populator.populate(source, target);
        assertThat(target.getDeliveryMode()).isNull();
    }

    @Test
    public void testGetDeliveryModeInfoForUnmatchingDeliveryMode() {
        deliveryMode.setCode("not-available-delivery-mode");
        populator.populate(source, target);
        assertThat(target.getDeliveryMode()).isNull();
    }

    @Test
    public void testGetDeliveryModeInfo() {
        deliveryMode.setCode("click-and-collect");
        populator.populate(source, target);
        assertThat(target.getDeliveryMode()).isEqualTo(deliveryModeInfo);
        verify(deliveryModeInfo).setAvailable(true);
    }
}
