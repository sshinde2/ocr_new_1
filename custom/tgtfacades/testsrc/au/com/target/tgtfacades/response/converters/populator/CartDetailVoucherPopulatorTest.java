/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * Test cases for CartDetailVoucherPopulator.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartDetailVoucherPopulatorTest {

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @InjectMocks
    private final CartDetailVoucherPopulator cartDetailVoucherPopulator = new CartDetailVoucherPopulator();

    private TargetCartData source;
    private CartDetailResponseData target;

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
    }

    @Test
    public void noVoucherAppliedToCart() {
        BDDMockito.given(targetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        cartDetailVoucherPopulator.populate(source, target);
        Assertions.assertThat(target.getVoucher()).isNull();
    }

    @Test
    public void voucherAppliedToCart() {
        BDDMockito.given(targetCheckoutFacade.getFirstAppliedVoucher()).willReturn("abc");
        final PriceData discount = mock(PriceData.class);
        source.setTotalDiscounts(discount);
        cartDetailVoucherPopulator.populate(source, target);
        Assertions.assertThat(target.getVoucher()).isNotNull();
        Assertions.assertThat(target.getVoucher().getCode()).isEqualTo("abc");
        Assertions.assertThat(target.getVoucher().getDiscount()).isEqualTo(discount);
    }
}
