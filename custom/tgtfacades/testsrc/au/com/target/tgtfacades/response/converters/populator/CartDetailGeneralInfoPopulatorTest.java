/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * Test cases for CartDetailGeneralInfoPopulator.
 * 
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartDetailGeneralInfoPopulatorTest {

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @InjectMocks
    private final CartDetailGeneralInfoPopulator cartDetailVoucherPopulator = new CartDetailGeneralInfoPopulator();

    private TargetCartData source;
    private CartDetailResponseData target;

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
    }

    @Test
    public void testGetGeneralInfo() {
        given(targetCheckoutFacade.getThreatMatrixSessionID()).willReturn("abc");
        given(Boolean.valueOf(targetCheckoutFacade.doesCartHaveGiftCardOnly())).willReturn(Boolean.TRUE);
        source.setCode("00012000");
        cartDetailVoucherPopulator.populate(source, target);
        assertThat(target.getId()).isEqualTo("00012000");
        assertThat(target.getTmid()).isEqualTo("abc");
        assertThat(target.getContainsDigitalEntriesOnly()).isEqualTo(Boolean.TRUE);
    }

}
