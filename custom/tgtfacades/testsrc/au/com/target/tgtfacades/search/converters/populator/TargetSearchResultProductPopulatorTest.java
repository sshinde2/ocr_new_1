/**
 *
 */
package au.com.target.tgtfacades.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.converters.populator.ProductFeatureListPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.converters.populator.TargetProductPricePopulator;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;
import au.com.target.tgtfacades.util.TargetPriceHelper;


/**
 * @author rmcalave
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSearchResultProductPopulatorTest {

    @Mock
    private CommonI18NService mockCommonI18NService;

    @Mock
    private ProductFeatureListPopulator<FeatureList, ProductData> mockProductFeatureListPopulator;

    @Mock
    private UrlResolver<ProductData> mockProductDataUrlResolver;

    @Mock
    private ImageFormatMapping imageFormatMapping;

    @InjectMocks
    private final TargetProductPricePopulator targetProductPricePopulator = new TargetProductPricePopulator();

    @InjectMocks
    private final TargetPriceHelper targetPriceHelper = new TargetPriceHelper();

    @InjectMocks
    private final TargetSearchResultProductPopulator targetSearchResultProductPopulator = new TargetSearchResultProductPopulator();

    @Before
    public void setUp() {
        final CurrencyModel currencyModel = Mockito.mock(CurrencyModel.class);
        currencyModel.setDigits(Integer.valueOf(2));
        currencyModel.setSymbol("USD");
        BDDMockito.given(currencyModel.getIsocode()).willReturn("USD");

        final DefaultPriceDataFactory priceDataFactory = new DefaultPriceDataFactory();
        final I18NService i18nService = BDDMockito.mock(I18NService.class);
        final CommerceCommonI18NService commerceCommonI18NService = BDDMockito.mock(CommerceCommonI18NService.class);
        BDDMockito.given(i18nService.getCurrentLocale()).willReturn(Locale.getDefault());

        BDDMockito.given(mockCommonI18NService.getCurrentCurrency()).willReturn(currencyModel);
        BDDMockito.given(mockCommonI18NService.getCurrency("USD")).willReturn(currencyModel);
        priceDataFactory.setCommonI18NService(mockCommonI18NService);
        priceDataFactory.setI18NService(i18nService);
        priceDataFactory.setCommerceCommonI18NService(commerceCommonI18NService);
        targetPriceHelper.setPriceDataFactory(priceDataFactory);
        targetProductPricePopulator.setTargetPriceHelper(targetPriceHelper);
        targetSearchResultProductPopulator.setTargetProductPricePopulator(targetProductPricePopulator);

        final LanguageModel mockLanguageModel = Mockito.mock(LanguageModel.class);
        BDDMockito.given(mockCommonI18NService.getCurrentLanguage()).willReturn(mockLanguageModel);
        BDDMockito.given(mockCommonI18NService.getLocaleForLanguage(mockLanguageModel)).willReturn(Locale.getDefault());

        final List<String> searchResultImageFormat = new ArrayList<>();
        searchResultImageFormat.add("list");
        searchResultImageFormat.add("grid");
        targetSearchResultProductPopulator.setSearchResultImageFormat(searchResultImageFormat);

        BDDMockito.given(imageFormatMapping.getMediaFormatQualifierForImageFormat("store")).willReturn("list");
        BDDMockito.given(imageFormatMapping.getMediaFormatQualifierForImageFormat("grid")).willReturn("grid");
    }

    @Test
    public void testPopulate() {
        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        final List<TargetPromotionStatusEnum> promotionStatuses = new ArrayList<>();
        promotionStatuses.add(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE);

        final Map<String, Object> sourceValues = new HashMap<String, Object>();
        sourceValues.put("numberOfReviews", Integer.valueOf(3));
        sourceValues.put("dealDescription", "This item is part of deal");
        sourceValues.put("onlineExclusive", Collections.singletonList(Boolean.TRUE));

        BDDMockito.given(mockSource.getValues()).willReturn(sourceValues);

        final TargetProductData productData = new TargetProductData();
        final List<TargetPromotionStatusEnum> enumList = new ArrayList<>();
        enumList.add(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE);
        productData.setPromotionStatuses(enumList);
        targetSearchResultProductPopulator.populate(mockSource, productData);
        Assert.assertEquals(3, productData.getNumberOfReviews().intValue());
        Assert.assertTrue(productData.isOnlineExclusive());
        Assert.assertEquals(productData.getDealDescription(), "This item is part of deal");
    }

    @Test
    public void testPopulatePromotionsWithIncorrectTargetType() {
        final SearchResultValueData source = new SearchResultValueData();
        final ProductData mockTarget = Mockito.mock(ProductData.class);

        targetSearchResultProductPopulator.populatePromotions(source, mockTarget);

        Mockito.verifyZeroInteractions(mockTarget);
    }

    @Test
    public void testPopulatePromotionsWithOnlineExclusiveFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("onlineExclusive", Collections.singletonList(Boolean.TRUE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(1, promotionStatuses.size());
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE));
    }

    @Test
    public void testPopulatePromotionsWithTargetExclusiveFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("targetExclusive", Collections.singletonList(Boolean.TRUE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(1, promotionStatuses.size());
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.TARGET_EXCLUSIVE));
    }

    @Test
    public void testPopulatePromotionsWithClearanceFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("clearance", Collections.singletonList(Boolean.TRUE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(1, promotionStatuses.size());
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.CLEARANCE));
    }

    @Test
    public void testPopulatePromotionsWithHotProductFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("hotProduct", Collections.singletonList(Boolean.TRUE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(1, promotionStatuses.size());
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.HOT_PRODUCT));
    }

    @Test
    public void testPopulatePromotionsWithEssentialFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("essential", Collections.singletonList(Boolean.TRUE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(1, promotionStatuses.size());
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.ESSENTIALS));
    }

    @Test
    public void testPopulatePromotionsWithFalseOnlineExclusiveFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("onlineExclusive", Collections.singletonList(Boolean.FALSE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithFalseTargetExclusiveFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("targetExclusive", Collections.singletonList(Boolean.FALSE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithFalseClearanceFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("clearance", Collections.singletonList(Boolean.FALSE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithFalseHotProductFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("hotProduct", Collections.singletonList(Boolean.FALSE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithFalseEssentialFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("essential", Collections.singletonList(Boolean.FALSE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithTrueAndFalseOnlineExclusiveFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("onlineExclusive", createTrueFalseList());

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithTrueAndFalseTargetExclusiveFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("targetExclusive", createTrueFalseList());

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithTrueAndFalseClearanceFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("clearance", createTrueFalseList());

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithTrueAndFalseHotProductFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("hotProduct", createTrueFalseList());

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithTrueAndFalseEssentialFlag() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("essential", createTrueFalseList());

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(0, promotionStatuses.size());
    }

    @Test
    public void testPopulatePromotionsWithAllFlags() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        searchResultValues.put("onlineExclusive", Collections.singletonList(Boolean.TRUE));
        searchResultValues.put("targetExclusive", Collections.singletonList(Boolean.TRUE));
        searchResultValues.put("clearance", Collections.singletonList(Boolean.TRUE));
        searchResultValues.put("hotProduct", Collections.singletonList(Boolean.TRUE));
        searchResultValues.put("essential", Collections.singletonList(Boolean.TRUE));

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePromotions(mockSource, target);

        final List<TargetPromotionStatusEnum> promotionStatuses = target.getPromotionStatuses();
        Assert.assertEquals(5, promotionStatuses.size());
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE));
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.TARGET_EXCLUSIVE));
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.CLEARANCE));
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.HOT_PRODUCT));
        Assert.assertTrue(promotionStatuses.contains(TargetPromotionStatusEnum.ESSENTIALS));
    }

    @Test
    public void testAllPriceAreRange() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        final Double priceFrom = Double.valueOf(20.9);
        final Double priceTo = Double.valueOf(30.9);
        final Double priceWasFrom = Double.valueOf(25.9);
        final Double priceWasTo = Double.valueOf(40.9);
        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(priceFrom, priceTo, priceWasFrom,
                priceWasTo);

        searchResultValues.put("priceRangeInfo", priceRangeInformation);

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePrices(mockSource, target);

        Assert.assertNotNull(target.getPriceRange());
        Assert.assertNotNull(target.getWasPriceRange());
        Assert.assertNull(target.getPrice());
        Assert.assertNull(target.getWasPrice());
    }

    @Test
    public void testNoPriceAreRange() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        final Double priceFrom = Double.valueOf(20.9);
        final Double priceWasFrom = Double.valueOf(30.9);
        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(priceFrom, priceFrom,
                priceWasFrom,
                priceWasFrom);

        searchResultValues.put("priceRangeInfo", priceRangeInformation);

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePrices(mockSource, target);

        Assert.assertNull(target.getPriceRange());
        Assert.assertNull(target.getWasPriceRange());
        Assert.assertNotNull(target.getPrice());
        Assert.assertNotNull(target.getWasPrice());
    }

    @Test
    public void testOnlyWasPriceIsRange() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        final Double priceFrom = Double.valueOf(20.9);
        final Double priceWasFrom = Double.valueOf(25.9);
        final Double priceWasTo = Double.valueOf(34.9);
        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(priceFrom, priceFrom,
                priceWasFrom,
                priceWasTo);

        searchResultValues.put("priceRangeInfo", priceRangeInformation);

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePrices(mockSource, target);

        Assert.assertNull(target.getPriceRange());
        Assert.assertNotNull(target.getWasPriceRange());
        Assert.assertNotNull(target.getPrice());
        Assert.assertNull(target.getWasPrice());
    }

    @Test
    public void testOnlyPriceIsRange() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        final Double priceFrom = Double.valueOf(20.9);
        final Double priceFromTo = Double.valueOf(40.9);
        final Double priceWasFrom = Double.valueOf(25.9);
        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(priceFrom, priceFromTo,
                priceWasFrom,
                priceWasFrom);

        searchResultValues.put("priceRangeInfo", priceRangeInformation);

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePrices(mockSource, target);

        Assert.assertNotNull(target.getPriceRange());
        Assert.assertNull(target.getWasPriceRange());
        Assert.assertNull(target.getPrice());
        Assert.assertNotNull(target.getWasPrice());
    }

    /**
     * Verifies that "was price" is not populated when it's lower than current price.
     */
    @Test
    public void testWasPriceIsNotPopulatedWhenLowerThanCurrent() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();
        final Double priceFrom = Double.valueOf(20.9);
        final Double priceWasFrom = Double.valueOf(10.9);
        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(priceFrom, priceFrom,
                priceWasFrom,
                priceWasFrom);

        searchResultValues.put("priceRangeInfo", priceRangeInformation);

        final SearchResultValueData mockSource = Mockito.mock(SearchResultValueData.class);
        BDDMockito.given(mockSource.getValues()).willReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();

        targetSearchResultProductPopulator.populatePrices(mockSource, target);

        Assert.assertNull(target.getWasPriceRange());
        Assert.assertNull(target.getWasPrice());
    }

    @Test
    public void testPopulateImages() {
        final Map<String, Object> searchResultValues = new HashMap<String, Object>();

        searchResultValues.put("img-list", "list");
        searchResultValues.put("img-grid", "grid");
        final SearchResultValueData searchResultValueData = BDDMockito.mock(SearchResultValueData.class);

        BDDMockito.when(searchResultValueData.getValues()).thenReturn(searchResultValues);

        final TargetProductData target = new TargetProductData();
        Mockito.when(imageFormatMapping.getMediaFormatQualifierForImageFormat("list")).thenReturn(
                "list");
        final List<ImageData> images = targetSearchResultProductPopulator.createImageData(searchResultValueData);

        if (CollectionUtils.isNotEmpty(images)) {
            target.setImages(images);
        }

        Assert.assertNotNull(target.getImages());
    }

    private List<Boolean> createTrueFalseList() {
        final List<Boolean> trueFalseList = new ArrayList<>();

        trueFalseList.add(Boolean.TRUE);
        trueFalseList.add(Boolean.FALSE);

        return trueFalseList;
    }
}
