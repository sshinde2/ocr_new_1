/**
 * 
 */
package au.com.target.tgtfacades.search.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.model.media.MediaModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.converters.ArticleItemConverter;
import au.com.target.tgtfacades.data.ArticleItemData;
import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;


/**
 * @author asingh78
 * 
 */
@UnitTest
public class ArticleItemConverterTest {

    private static final String IMAGE_URL = "imageURL";
    private static final String HEADLINE = "headline";
    private static final String CONTENT = "content";
    private static final String PAGE_LABEL = "/page_level";
    private static final String URL_LINK = "google.com";
    private static final String YOU_TUB_LINK = "http://www.youtube.com/embed/{0}?rel=0";

    @Mock
    private CMSPageService cmsPageService;

    @InjectMocks
    private final ArticleItemConverter articleItemConverter = new ArticleItemConverter();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void convertWithAllDataTest() {
        final ArticleItemComponentModel source = Mockito.mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = Mockito.mock(MediaModel.class);
        final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
        final ArticleItemData prototype = new ArticleItemData();
        BDDMockito.given(source.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn(IMAGE_URL);
        BDDMockito.given(Boolean.valueOf(source.isExternal())).willReturn(Boolean.FALSE);
        BDDMockito.given(source.getHeadline()).willReturn(HEADLINE);
        BDDMockito.given(source.getContent()).willReturn(CONTENT);
        BDDMockito.given(source.getYouTubeLink()).willReturn(Boolean.FALSE);
        BDDMockito.given(source.getPage()).willReturn(contentPageModel);
        BDDMockito.given(contentPageModel.getLabel()).willReturn(PAGE_LABEL);
        articleItemConverter.convert(source, prototype);

        Assert.assertEquals(prototype.getContent(), CONTENT);
        Assert.assertEquals(prototype.getHeadline(), HEADLINE);
        Assert.assertEquals(prototype.getImageURL(), IMAGE_URL);
        Assert.assertEquals(prototype.getImageLink(), PAGE_LABEL);

        Assert.assertFalse(prototype.isYouTubeLink());


    }

    @Test
    public void convertWithYouTubLinkTest() {
        final ArticleItemComponentModel source = Mockito.mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = Mockito.mock(MediaModel.class);
        final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
        final ArticleItemData prototype = new ArticleItemData();
        BDDMockito.given(source.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn(IMAGE_URL);
        BDDMockito.given(Boolean.valueOf(source.isExternal())).willReturn(Boolean.FALSE);
        BDDMockito.given(source.getHeadline()).willReturn(HEADLINE);
        BDDMockito.given(source.getContent()).willReturn(CONTENT);
        BDDMockito.given(source.getYouTubeLink()).willReturn(Boolean.TRUE);

        BDDMockito.given(source.getUrlLink()).willReturn(YOU_TUB_LINK);
        BDDMockito.given(source.getPage()).willReturn(null);
        BDDMockito.given(contentPageModel.getLabel()).willReturn(PAGE_LABEL);
        articleItemConverter.convert(source, prototype);

        Assert.assertEquals(prototype.getContent(), CONTENT);
        Assert.assertEquals(prototype.getHeadline(), HEADLINE);
        Assert.assertEquals(prototype.getImageURL(), IMAGE_URL);
        Assert.assertEquals(prototype.getImageLink(), YOU_TUB_LINK);

        Assert.assertTrue(prototype.isYouTubeLink());


    }


    @Test
    public void convertWithExternalLinkTest() {
        final ArticleItemComponentModel source = Mockito.mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = Mockito.mock(MediaModel.class);
        final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
        final ArticleItemData prototype = new ArticleItemData();
        BDDMockito.given(source.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn(IMAGE_URL);
        BDDMockito.given(Boolean.valueOf(source.isExternal())).willReturn(Boolean.FALSE);
        BDDMockito.given(source.getHeadline()).willReturn(HEADLINE);
        BDDMockito.given(source.getContent()).willReturn(CONTENT);
        BDDMockito.given(source.getYouTubeLink()).willReturn(Boolean.FALSE);

        BDDMockito.given(source.getUrlLink()).willReturn(URL_LINK);
        BDDMockito.given(source.getPage()).willReturn(null);
        BDDMockito.given(contentPageModel.getLabel()).willReturn(PAGE_LABEL);
        articleItemConverter.convert(source, prototype);

        Assert.assertEquals(prototype.getContent(), CONTENT);
        Assert.assertEquals(prototype.getHeadline(), HEADLINE);
        Assert.assertEquals(prototype.getImageURL(), IMAGE_URL);
        Assert.assertEquals(prototype.getImageLink(), URL_LINK);

        Assert.assertFalse(prototype.isYouTubeLink());


    }






}
