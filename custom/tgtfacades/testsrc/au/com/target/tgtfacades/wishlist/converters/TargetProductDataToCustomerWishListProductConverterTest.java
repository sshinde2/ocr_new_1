/**
 * 
 */
package au.com.target.tgtfacades.wishlist.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.math.BigDecimal;
import java.util.Collections;

import org.apache.commons.lang.StringEscapeUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtwishlist.data.TargetCustomerWishListProductsDto;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class TargetProductDataToCustomerWishListProductConverterTest {

    private static final String PRODUCT_COLOUR = "Red";
    private static final String FQDN = "https://www.target.com.au";
    private static final String IMAGE_URL_PREFIX = "01**/";
    private static final String IMAGE_URL = "medias/static_content/product/images/grid/43/39/A764339.jpg";
    private static final String PRODUCT_SIZE = "10";
    private static final String PRODUCT_URL = "/p/P1000";
    private static final String PRODUCT_NAME = "T-Shirt";
    private static final String PRODUCT_NAME_REG = "LEGO Star Wars Captain Phasma 75118";
    private static final String BASE_CODE = "P1000";
    private static final String PRODUCT_CODE = "P1000_Red_S";

    private final TargetProductDataToCustomerWishListProductConverter converter = new TargetProductDataToCustomerWishListProductConverter();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    private TargetVariantProductListerData targetVariantProductListerData;


    @Before
    public void setUp() {
        converter.setBaseUrl(FQDN);
    }

    @Test
    public void testBaseProductNull() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        expectedException.expectMessage("Base product code cannot be null");
        converter.convert(productListerData);
    }

    @Test
    public void testGridImageMissing() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        productListerData.setBaseProductCode(BASE_CODE);
        productListerData.setPrice(buildPriceData("39.99"));
        productListerData.setUrl(PRODUCT_URL);
        targetVariantProductListerData = new TargetVariantProductListerData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        expectedException.expectMessage("No grid images available for product");
        converter.convert(productListerData);
    }

    @Test
    public void testGridPriceMissing() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        productListerData.setBaseProductCode(BASE_CODE);
        productListerData.setUrl(PRODUCT_URL);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME);
        expectedException.expectMessage("Price cannot be null");
        converter.convert(productListerData);
    }

    @Test
    public void testGridValidData() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        productListerData.setBaseProductCode(BASE_CODE);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME);
        productListerData.setPrice(buildPriceData("39.99"));
        productListerData.setUrl(PRODUCT_URL);
        productListerData.setSize(PRODUCT_SIZE);
        final TargetCustomerWishListProductsDto productsConverted = converter.convert(productListerData);
        verifyProductConversion(productsConverted, PRODUCT_NAME, "$39.99");
    }

    @Test
    public void testGridValidDataWithRegSymbol() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        productListerData.setBaseProductCode(BASE_CODE);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME_REG + " " + StringEscapeUtils.unescapeHtml("&reg;") + " "
                + StringEscapeUtils.unescapeHtml("&trade;"));
        productListerData.setPrice(buildPriceData("39.99"));
        productListerData.setUrl(PRODUCT_URL);
        productListerData.setSize(PRODUCT_SIZE);
        final TargetCustomerWishListProductsDto productsConverted = converter.convert(productListerData);
        verifyProductConversion(productsConverted, "LEGO Star Wars Captain Phasma 75118 &reg; &trade;",
                "$39.99");
    }

    @Test
    public void testGridValidDataWithNoDecimalInPrice() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        productListerData.setBaseProductCode(BASE_CODE);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME_REG);
        productListerData.setPrice(buildPriceData("39.00"));
        productListerData.setUrl(PRODUCT_URL);
        productListerData.setSize(PRODUCT_SIZE);
        final TargetCustomerWishListProductsDto productsConverted = converter.convert(productListerData);
        verifyProductConversion(productsConverted, "LEGO Star Wars Captain Phasma 75118",
                "$39");
    }

    @Test
    public void testGridValidDataWithPriceRange() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        productListerData.setBaseProductCode(BASE_CODE);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME_REG);
        final PriceRangeData priceRange = new PriceRangeData();
        priceRange.setMinPrice(buildPriceData("10"));
        priceRange.setMaxPrice(buildPriceData("39.99"));
        productListerData.setPriceRange(priceRange);
        productListerData.setUrl(PRODUCT_URL);
        productListerData.setSize(PRODUCT_SIZE);
        final TargetCustomerWishListProductsDto productsConverted = converter.convert(productListerData);
        verifyProductConversion(productsConverted, "LEGO Star Wars Captain Phasma 75118",
                "$10 - $39.99");
    }

    @Test
    public void testGridValidDataWithSelectedSizeVariant() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        final TargetSelectedVariantData selectedVariantData = new TargetSelectedVariantData();
        selectedVariantData.setColor(PRODUCT_COLOUR);
        selectedVariantData.setPrice(buildPriceData("10"));
        selectedVariantData.setSize("s");
        productListerData.setBaseProductCode(BASE_CODE);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME_REG);
        productListerData.setSelectedVariantData(selectedVariantData);
        final PriceRangeData priceRange = new PriceRangeData();
        priceRange.setMinPrice(buildPriceData("10"));
        priceRange.setMaxPrice(buildPriceData("39.99"));
        productListerData.setPriceRange(priceRange);
        selectedVariantData.setUrl(PRODUCT_URL);
        selectedVariantData.setProductCode(PRODUCT_CODE);
        final TargetCustomerWishListProductsDto productsConverted = converter.convert(productListerData);
        verifyProductConversion(productsConverted, "LEGO Star Wars Captain Phasma 75118",
                "$10");
        Assertions.assertThat(productsConverted.getColour()).isEqualTo(PRODUCT_COLOUR);
        Assertions.assertThat(productsConverted.getSize()).isEqualTo("s");
        Assertions.assertThat(productsConverted.getVariantCode()).isEqualTo(PRODUCT_CODE);

    }

    @Test
    public void testGridValidDataWithSelectedColourVariant() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        final TargetSelectedVariantData selectedVariantData = new TargetSelectedVariantData();
        selectedVariantData.setColor(PRODUCT_COLOUR);
        productListerData.setBaseProductCode(BASE_CODE);
        targetVariantProductListerData = buildVariantData();
        productListerData.setTargetVariantProductListerData(Collections.singletonList(targetVariantProductListerData));
        productListerData.setBaseName(PRODUCT_NAME_REG);
        productListerData.setSelectedVariantData(selectedVariantData);
        final PriceRangeData priceRange = new PriceRangeData();
        priceRange.setMinPrice(buildPriceData("10"));
        priceRange.setMaxPrice(buildPriceData("39.99"));
        productListerData.setPriceRange(priceRange);
        selectedVariantData.setUrl(PRODUCT_URL);
        selectedVariantData.setProductCode(PRODUCT_CODE);
        final TargetCustomerWishListProductsDto productsConverted = converter.convert(productListerData);
        verifyProductConversion(productsConverted, "LEGO Star Wars Captain Phasma 75118",
                "$10 - $39.99");
        Assertions.assertThat(productsConverted.getColour()).isEqualTo(PRODUCT_COLOUR);
        Assertions.assertThat(productsConverted.getVariantCode()).isEqualTo(PRODUCT_CODE);

    }


    /**
     * @param wishListRequest
     */
    private void verifyProductConversion(final TargetCustomerWishListProductsDto wishListRequest,
            final String productName, final String price) {
        Assertions.assertThat(wishListRequest.getBaseCode()).isNotNull().isNotEmpty().isEqualTo(BASE_CODE);

        Assertions.assertThat(wishListRequest.getImageURL()).isNotNull().isNotEmpty().isEqualTo(FQDN + "/" + IMAGE_URL);

        Assertions.assertThat(wishListRequest.getName()).isNotNull().isNotEmpty().isEqualTo(productName);

        Assertions.assertThat(wishListRequest.getPrice()).isNotNull().isNotEmpty().isEqualTo(price);

        Assertions.assertThat(wishListRequest.getProductURL()).isNotNull().isNotEmpty()
                .isEqualTo(FQDN + PRODUCT_URL);

    }

    /**
     * @param value
     * @return PriceData
     */
    private PriceData buildPriceData(final String value) {
        final PriceData price = new PriceData();
        price.setValue(new BigDecimal(value));
        price.setFormattedValue("$" + value);
        return price;
    }

    /**
     * @return ImageData
     */
    private TargetVariantProductListerData buildVariantData() {
        final TargetVariantProductListerData variant = new TargetVariantProductListerData();
        variant.setGridImageUrls(Collections
                .singletonList(IMAGE_URL_PREFIX + IMAGE_URL));
        return variant;
    }

}
