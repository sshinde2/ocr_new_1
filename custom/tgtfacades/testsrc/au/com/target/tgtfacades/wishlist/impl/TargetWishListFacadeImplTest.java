/**
 * 
 */
package au.com.target.tgtfacades.wishlist.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.wishlist.converters.TargetCustomerProductInfoConverter;
import au.com.target.tgtfacades.wishlist.converters.TargetProductDataToCustomerWishListProductConverter;
import au.com.target.tgtfacades.wishlist.converters.TargetWishListEntryModelConverter;
import au.com.target.tgtwishlist.data.TargetCustomerWishListDto;
import au.com.target.tgtwishlist.data.TargetCustomerWishListProductsDto;
import au.com.target.tgtwishlist.data.TargetRecipientDto;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.exception.InvalidWishListUserException;
import au.com.target.tgtwishlist.logger.TgtWishListLogger;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;
import au.com.target.tgtwishlist.service.TargetWishListService;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetWishListFacadeImplTest {

    @Mock
    private TargetWishListService targetWishListService;

    @Mock
    private UserService userService;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private TargetCustomerModel targetCustomerModel;

    @Mock
    private UserModel userModel;

    @Mock
    private TargetCustomerProductInfoConverter targetCustomerProductInfoConverter;

    @Mock
    private TargetWishListEntryModelConverter targetWishListEntryModelConverter;

    @Mock
    private TargetWishListModel targetWishListModel;

    @Mock
    private SalesApplication salesApplication;

    @Mock
    private TargetProductDataToCustomerWishListProductConverter targetProductListerDataConverter;

    @InjectMocks
    private final TargetWishListFacadeImpl targetWishListFacadeImpl = new TargetWishListFacadeImpl();

    @Captor
    private ArgumentCaptor<ArrayList<TargetWishListEntryData>> targetWishListEntryDataCaptor;


    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Before
    public void setup() {
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(salesApplication);
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testAddProductToListForAnonymousTargetCustomer() {

        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.TRUE);
        final CustomerProductInfo customerProductInfoMock = mock(CustomerProductInfo.class);
        targetWishListFacadeImpl.addProductToList(customerProductInfoMock, "Favourites", WishListTypeEnum.FAVOURITE);
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testAddProductToListForANonCustomer() {

        given(userService.getCurrentUser()).willReturn(userModel);
        given(Boolean.valueOf(userService.isAnonymousUser(userModel))).willReturn(Boolean.FALSE);
        final CustomerProductInfo customerProductInfoMock = mock(CustomerProductInfo.class);
        targetWishListFacadeImpl.addProductToList(customerProductInfoMock, "Favourites", WishListTypeEnum.FAVOURITE);
    }


    @Test
    public void testAddProductToListSuccessFul() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel)))
                .willReturn(Boolean.FALSE);
        final CustomerProductInfo customerProductInfoMock = mock(CustomerProductInfo.class);
        given(customerProductInfoMock.getBaseProductCode()).willReturn("P1000");
        given(customerProductInfoMock.getSelectedVariantCode()).willReturn("P1000_Black");
        given(customerProductInfoMock.getTimeStamp()).willReturn("222333444");
        final TargetWishListModel wishListModelMock = mock(TargetWishListModel.class);
        given(targetWishListService.verifyAndCreateList("Favourites", targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(wishListModelMock);
        final TargetWishListEntryData targetWishListEntryDataMock = mock(TargetWishListEntryData.class);
        given(targetCustomerProductInfoConverter.convert(any(CustomerProductInfo.class))).willReturn(
                targetWishListEntryDataMock);

        final ArgumentCaptor<TargetWishListEntryData> targetWishListEntryData = ArgumentCaptor
                .forClass(TargetWishListEntryData.class);
        final ArgumentCaptor<TargetWishListModel> targetWishListModelCaptor = ArgumentCaptor
                .forClass(TargetWishListModel.class);
        final ArgumentCaptor<SalesApplication> salesApplicationCaptor = ArgumentCaptor
                .forClass(SalesApplication.class);

        targetWishListFacadeImpl.addProductToList(customerProductInfoMock, "Favourites", WishListTypeEnum.FAVOURITE);

        verify(targetWishListService).addProductToWishList(targetWishListModelCaptor.capture(),
                targetWishListEntryData.capture(), eq(TgtWishListLogger.ADD_FAV_LIST_ACTION),
                salesApplicationCaptor.capture());

        assertThat(targetWishListEntryData.getValue()).isNotNull().isEqualTo(targetWishListEntryDataMock);
        assertThat(salesApplicationCaptor.getValue()).isEqualTo(salesApplication);
        assertThat(targetWishListModelCaptor.getValue()).isEqualTo(wishListModelMock);
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testSyncFavouritesListUserAnonymous() {
        given(userService.getCurrentUser()).willReturn(userModel);
        given(Boolean.valueOf(userService.isAnonymousUser(userModel))).willReturn(Boolean.TRUE);

        final List<CustomerProductInfo> customerProductInfo = targetWishListFacadeImpl
                .syncFavouritesList(new ArrayList<CustomerProductInfo>());
        assertThat(customerProductInfo).isNull();

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(userModel);
        verifyNoMoreInteractions(userService);
        verifyZeroInteractions(targetCustomerProductInfoConverter);
        verifyZeroInteractions(targetWishListEntryModelConverter);
        verifyZeroInteractions(targetWishListService);
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testSyncFavouritesListUserIsNotTargetCustomer() {
        given(userService.getCurrentUser()).willReturn(userModel);
        given(Boolean.valueOf(userService.isAnonymousUser(userModel))).willReturn(Boolean.FALSE);

        final List<CustomerProductInfo> customerProductInfo = targetWishListFacadeImpl
                .syncFavouritesList(new ArrayList<CustomerProductInfo>());

        assertThat(customerProductInfo).isNull();

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(userModel);
        verifyNoMoreInteractions(userService);
        verifyZeroInteractions(targetCustomerProductInfoConverter);
        verifyZeroInteractions(targetWishListEntryModelConverter);
        verifyZeroInteractions(targetWishListService);
    }

    @Test
    public void testSyncFavouritesListWithEmptyCustomerProductInfo() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);
        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);

        final List<Wishlist2EntryModel> entryModels = new ArrayList<>();
        final TargetWishListEntryModel entryModel = new TargetWishListEntryModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setCode("W134343");
        entryModel.setProduct(productModel);
        entryModel.setAddedDate(new Date());
        entryModel.setSelectedVariant("5234324");
        entryModels.add(entryModel);
        given(
                targetWishListService.syncWishListForUser(any(TargetCustomerModel.class), anyList(),
                        any(SalesApplication.class)))
                                .willReturn(targetWishListModel);
        given(targetWishListEntryModelConverter.convert(entryModel)).willReturn(info);
        given(targetWishListModel.getEntries()).willReturn(entryModels);


        final List<CustomerProductInfo> customerProductInfo = targetWishListFacadeImpl
                .syncFavouritesList(new ArrayList<CustomerProductInfo>());

        assertThat(customerProductInfo).isNotNull().isNotEmpty();

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(userService);
        verify(targetWishListService).syncWishListForUser(any(TargetCustomerModel.class),
                anyList(),
                any(SalesApplication.class));
        verify(targetWishListEntryModelConverter).convert(entryModel);
        verifyZeroInteractions(targetCustomerProductInfoConverter);
        verifyNoMoreInteractions(targetWishListEntryModelConverter);
        verifyNoMoreInteractions(targetWishListService);
    }

    @Test
    public void testSyncFavouritesListWithValidCustomerProductInfoButServiceReturnsNull() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);

        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);

        given(targetCustomerProductInfoConverter.convert(info)).willReturn(new TargetWishListEntryData());
        given(
                targetWishListService.syncWishListForUser(eq(targetCustomerModel), anyList(),
                        any(SalesApplication.class)))
                                .willReturn(
                                        null);

        final List<CustomerProductInfo> customerProductInfo = targetWishListFacadeImpl
                .syncFavouritesList(customerProductInfos);

        assertThat(customerProductInfo).isNotNull().isNotEmpty().isEqualTo(customerProductInfos);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(userService);
        verify(targetCustomerProductInfoConverter).convert(info);
        verifyNoMoreInteractions(targetCustomerProductInfoConverter);
        verifyZeroInteractions(targetWishListEntryModelConverter);
        verify(targetWishListService).syncWishListForUser(any(TargetCustomerModel.class),
                anyList(), any(SalesApplication.class));
        verifyNoMoreInteractions(targetWishListService);
    }

    @Test
    public void testSyncFavouritesListWithValidCustomerProductInfo() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);

        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);

        given(targetCustomerProductInfoConverter.convert(info)).willReturn(new TargetWishListEntryData());
        given(
                targetWishListService.syncWishListForUser(any(TargetCustomerModel.class), anyList(),
                        any(SalesApplication.class)))
                                .willReturn(targetWishListModel);


        final List<Wishlist2EntryModel> entryModels = new ArrayList<>();
        final TargetWishListEntryModel entryModel = new TargetWishListEntryModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setCode("W134343");
        entryModel.setProduct(productModel);
        entryModel.setAddedDate(new Date());
        entryModel.setSelectedVariant("5234324");
        entryModels.add(entryModel);
        given(targetWishListEntryModelConverter.convert(entryModel)).willReturn(info);
        given(targetWishListModel.getEntries()).willReturn(entryModels);

        final List<CustomerProductInfo> customerProductInfo = targetWishListFacadeImpl
                .syncFavouritesList(customerProductInfos);

        assertThat(customerProductInfo).isNotNull().isNotEmpty().isEqualTo(customerProductInfos);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(userService);
        verify(targetCustomerProductInfoConverter).convert(info);
        verifyNoMoreInteractions(targetCustomerProductInfoConverter);
        verify(targetWishListEntryModelConverter).convert(entryModel);
        verifyNoMoreInteractions(targetWishListEntryModelConverter);
        verify(targetWishListService).syncWishListForUser(any(TargetCustomerModel.class),
                anyList(), any(SalesApplication.class));
        verifyNoMoreInteractions(targetWishListService);
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testRemoveFavouritesListUserAnonymous() {
        given(userService.getCurrentUser()).willReturn(userModel);
        given(Boolean.valueOf(userService.isAnonymousUser(userModel))).willReturn(Boolean.TRUE);

        targetWishListFacadeImpl
                .removeProductsFromList(new ArrayList<CustomerProductInfo>(), StringUtils.EMPTY,
                        WishListTypeEnum.FAVOURITE);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(userModel);
        verifyNoMoreInteractions(userService);
        verifyZeroInteractions(targetCustomerProductInfoConverter);
        verifyZeroInteractions(targetWishListEntryModelConverter);
        verifyZeroInteractions(targetWishListService);
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testRemoveFavouritesListUserIsNotTargetCustomer() {
        given(userService.getCurrentUser()).willReturn(userModel);
        given(Boolean.valueOf(userService.isAnonymousUser(userModel))).willReturn(Boolean.FALSE);

        targetWishListFacadeImpl.removeProductsFromList(new ArrayList<CustomerProductInfo>(), StringUtils.EMPTY,
                WishListTypeEnum.FAVOURITE);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(userModel);
        verifyNoMoreInteractions(userService);
        verifyZeroInteractions(targetCustomerProductInfoConverter);
        verifyZeroInteractions(targetWishListEntryModelConverter);
        verifyZeroInteractions(targetWishListService);
    }


    @Test
    public void testRemoveFavouritesListWithEmptyCustomerProductInfo() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);
        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();

        targetWishListFacadeImpl.removeProductsFromList(
                customerProductInfos, StringUtils.EMPTY, WishListTypeEnum.FAVOURITE);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(userService);
        verifyZeroInteractions(targetCustomerProductInfoConverter, targetWishListEntryModelConverter,
                targetWishListService);
    }


    @Test
    public void testRemoveFavouritesListWithValidCustomerProductInfoButServiceReturnsNull() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);

        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);

        final TargetWishListEntryData entryData = new TargetWishListEntryData();
        given(targetCustomerProductInfoConverter.convert(info)).willReturn(entryData);
        given(
                targetWishListService.removeWishListEntriesForUser(eq(targetCustomerModel),
                        anyList(),
                        any(SalesApplication.class)))
                                .willReturn(null);

        targetWishListFacadeImpl
                .removeProductsFromList(customerProductInfos, StringUtils.EMPTY, WishListTypeEnum.FAVOURITE);


        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(userService);
        verify(targetCustomerProductInfoConverter).convert(info);
        verifyNoMoreInteractions(targetCustomerProductInfoConverter);
        verify(targetWishListService).removeWishListEntriesForUser(eq(targetCustomerModel),
                targetWishListEntryDataCaptor.capture(),
                any(SalesApplication.class));
        verifyNoMoreInteractions(targetWishListService);
        final ArrayList<TargetWishListEntryData> targetWishListEntries = targetWishListEntryDataCaptor.getValue();
        assertThat(targetWishListEntries).isNotNull().isNotEmpty().hasSize(1);
        final TargetWishListEntryData wishListDataEntry = targetWishListEntries.get(0);
        assertThat(wishListDataEntry).isEqualTo(entryData);
    }

    @Test
    public void testRemoveFavouritesListWithValidCustomerProductInfo() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);

        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);

        final TargetWishListEntryData entryData = buildNewTargetWishListEntryData(info);

        given(targetCustomerProductInfoConverter.convert(info)).willReturn(entryData);
        given(targetWishListService.removeWishListEntriesForUser(eq(targetCustomerModel),
                anyList(), eq(SalesApplication.WEB))).willReturn(targetWishListModel);

        targetWishListFacadeImpl
                .removeProductsFromList(customerProductInfos, StringUtils.EMPTY, WishListTypeEnum.FAVOURITE);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(userService);
        verify(targetCustomerProductInfoConverter).convert(info);
        verifyNoMoreInteractions(targetCustomerProductInfoConverter);
        verify(targetWishListService).removeWishListEntriesForUser(eq(targetCustomerModel),
                targetWishListEntryDataCaptor.capture(), any(SalesApplication.class));
        verifyNoMoreInteractions(targetWishListService);
        final List<TargetWishListEntryData> actualEntries = targetWishListEntryDataCaptor.getValue();
        assertThat(actualEntries).isNotNull().isNotEmpty().hasSize(1);
        for (final TargetWishListEntryData actualEntry : actualEntries) {
            verifyTargetWishListEntryData(actualEntry, info);
        }
    }

    @Test(expected = InvalidWishListUserException.class)
    public void testRetrieveFavouritesListWithAnonymousCustomerProductInfo() {
        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.TRUE);
        targetWishListFacadeImpl.retrieveFavouriteProducts();
    }

    @Test
    public void testRetrieveFavouritesListWithValidCustomerProductInfo() {
        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = buildCustomerProductInfo("W134343", "5234324", "234324324");
        customerProductInfos.add(info);
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);
        given(targetWishListService.retrieveWishList(StringUtils.EMPTY, targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(targetWishListModel);

        final List<Wishlist2EntryModel> entryModels = new ArrayList<>();
        final TargetWishListEntryModel entryModel = new TargetWishListEntryModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setCode("W134343");
        entryModel.setProduct(productModel);
        entryModel.setAddedDate(new Date());
        entryModel.setSelectedVariant("5234324");
        entryModels.add(entryModel);
        given(targetWishListEntryModelConverter.convert(entryModel)).willReturn(info);
        given(targetWishListModel.getEntries()).willReturn(entryModels);
        final List<CustomerProductInfo> custInfos = targetWishListFacadeImpl.retrieveFavouriteProducts();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verifyNoMoreInteractions(targetCustomerProductInfoConverter);
        verify(targetWishListEntryModelConverter).convert(entryModel);
        verifyNoMoreInteractions(targetWishListEntryModelConverter);
        verify(targetWishListService).retrieveWishList(org.apache.commons.lang.StringUtils.EMPTY,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListService);
        assertThat(custInfos.size()).isEqualTo(1);
        assertThat(custInfos.get(0).getSelectedVariantCode()).isEqualTo("5234324");
        assertThat(custInfos.get(0).getBaseProductCode()).isEqualTo("W134343");



    }

    @Test
    public void testRetrieveFavouritesListWithValidCustomerProductInfoSortingBasedOnTimestamp() {
        final ArrayList<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info1 = buildCustomerProductInfo("W134343", "5134324", "234324324");
        final CustomerProductInfo info2 = buildCustomerProductInfo("W234343", "5234324", "111111324");
        final CustomerProductInfo info3 = buildCustomerProductInfo("W334343", "5334324", "666664324");
        final CustomerProductInfo info4 = buildCustomerProductInfo("W434343", "5434324", "444444324");
        customerProductInfos.add(info1);
        customerProductInfos.add(info2);
        customerProductInfos.add(info3);
        customerProductInfos.add(info4);

        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel))).willReturn(Boolean.FALSE);
        given(targetWishListService.retrieveWishList(StringUtils.EMPTY, targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(targetWishListModel);

        final List<Wishlist2EntryModel> entryModels = new ArrayList<>();
        final TargetWishListEntryModel entryModel1 = createEntryModel("W134343", "5134324");
        final TargetWishListEntryModel entryModel2 = createEntryModel("W234343", "5234324");
        final TargetWishListEntryModel entryModel3 = createEntryModel("W334343", "5334324");
        final TargetWishListEntryModel entryModel4 = createEntryModel("W434343", "5434324");

        entryModels.add(entryModel1);
        entryModels.add(entryModel2);
        entryModels.add(entryModel3);
        entryModels.add(entryModel4);

        given(targetWishListEntryModelConverter.convert(entryModel1)).willReturn(info1);
        given(targetWishListEntryModelConverter.convert(entryModel2)).willReturn(info2);
        given(targetWishListEntryModelConverter.convert(entryModel3)).willReturn(info3);
        given(targetWishListEntryModelConverter.convert(entryModel4)).willReturn(info4);

        given(targetWishListModel.getEntries()).willReturn(entryModels);
        final List<CustomerProductInfo> custInfos = targetWishListFacadeImpl.retrieveFavouriteProducts();
        verify(userService).isAnonymousUser(targetCustomerModel);
        verify(targetWishListService).retrieveWishList(org.apache.commons.lang.StringUtils.EMPTY,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListService);
        assertThat(custInfos.size()).isEqualTo(4);
        verifyCustomerInfoResult(custInfos.get(0), info3);
        verifyCustomerInfoResult(custInfos.get(1), info4);
        verifyCustomerInfoResult(custInfos.get(2), info1);
        verifyCustomerInfoResult(custInfos.get(3), info2);
    }

    private void verifyCustomerInfoResult(final CustomerProductInfo result, final CustomerProductInfo expected) {
        assertThat(result.getBaseProductCode()).isEqualTo(expected.getBaseProductCode());
        assertThat(result.getSelectedVariantCode()).isEqualTo(expected.getSelectedVariantCode());
    }

    private TargetWishListEntryModel createEntryModel(final String code, final String selectedVariant) {
        final TargetWishListEntryModel entryModel = new TargetWishListEntryModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setCode(code);
        entryModel.setProduct(productModel);
        entryModel.setAddedDate(new Date());
        entryModel.setSelectedVariant(selectedVariant);
        return entryModel;
    }

    @Test
    public void testShareWishListForUserEmptyList() {
        final boolean success = targetWishListFacadeImpl.shareWishListForUser(Collections.EMPTY_LIST, null, null, null);
        assertThat(success).isFalse();
    }

    @Test
    public void testShareWishListForUserRecipientEmailNull() {
        expectedException.expectMessage("Recipient Email address cannot be null");
        final TargetProductListerData productListerData = new TargetProductListerData();
        targetWishListFacadeImpl.shareWishListForUser(
                Collections.singletonList(productListerData), null, null, null);
    }

    @Test
    public void testShareWishListForUserRecipientNameNull() {
        expectedException.expectMessage("Recipient Name cannot be null");
        final TargetProductListerData productListerData = new TargetProductListerData();
        targetWishListFacadeImpl.shareWishListForUser(
                Collections.singletonList(productListerData), "test@test.com", null, null);
    }

    @Test
    public void testShareWishListForUserNotCustomer() {
        given(userService.getCurrentUser()).willReturn(userModel);
        expectedException.expectMessage("User not a Target Customer");
        final TargetProductListerData productListerData = new TargetProductListerData();
        targetWishListFacadeImpl.shareWishListForUser(
                Collections.singletonList(productListerData), "test@test.com", "Jane Doe", null);
    }

    @Test
    public void testShareWishListValidData() {
        final TargetProductListerData productListerData = new TargetProductListerData();
        final TargetCustomerWishListProductsDto productsDto = new TargetCustomerWishListProductsDto();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(targetCustomerModel.getFirstname()).willReturn("John");
        given(targetCustomerModel.getLastname()).willReturn("Smith");
        given(targetCustomerModel.getContactEmail()).willReturn("john.smith@test.com");
        given(targetProductListerDataConverter.convert(productListerData)).willReturn(productsDto);
        given(Boolean.valueOf(targetWishListService.shareWishList(any(TargetWishListSendRequestDto.class))))
                .willReturn(Boolean.TRUE);
        final ArgumentCaptor<TargetWishListSendRequestDto> targetWishListSendRequestDto = ArgumentCaptor
                .forClass(TargetWishListSendRequestDto.class);
        final boolean success = targetWishListFacadeImpl.shareWishListForUser(
                Collections.singletonList(productListerData), "test@test.com", "Jane Doe", null);
        assertThat(success).isTrue();
        verify(targetWishListService).shareWishList(targetWishListSendRequestDto.capture());

        final TargetWishListSendRequestDto request = targetWishListSendRequestDto.getValue();
        assertThat(request.getEmailType()).isNotNull().isNotEmpty().isEqualTo("Favourites");
        final TargetCustomerWishListDto customerWishListDto = request.getCustomer();
        assertThat(customerWishListDto).isNotNull();
        assertThat(customerWishListDto.getFirstName()).isNotNull().isNotEmpty().isEqualTo("John");
        assertThat(customerWishListDto.getLastName()).isNotNull().isNotEmpty().isEqualTo("Smith");
        assertThat(customerWishListDto.getEmailAddress()).isNotNull().isNotEmpty().isEqualTo("john.smith@test.com");

        final List<TargetRecipientDto> recipients = customerWishListDto.getRecipients();
        assertThat(recipients).isNotNull().isNotEmpty().hasSize(1);
        final TargetRecipientDto recipient = recipients.get(0);
        assertThat(recipient.getEmailAddress()).isNotNull().isNotEmpty().isEqualTo("test@test.com");
        assertThat(recipient.getName()).isNotNull().isNotEmpty().isEqualTo("Jane Doe");
        assertThat(recipient.getMessage()).isNullOrEmpty();

        verify(targetProductListerDataConverter, times(1)).convert(productListerData);

    }

    @Test
    public void testSkipFavouritesWithMissingProduct() {
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(targetWishListService.retrieveWishList(StringUtils.EMPTY, targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(targetWishListModel);
        final List<Wishlist2EntryModel> entries = new ArrayList<>();
        final TargetWishListEntryModel entry1 = mock(TargetWishListEntryModel.class);
        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(productModel.getCode()).willReturn("W134343");
        given(entry1.getProduct()).willReturn(productModel);
        given(entry1.getAddedDate()).willReturn(new Date());
        given(entry1.getSelectedVariant()).willReturn("5234324");
        final TargetWishListEntryModel entry2 = mock(TargetWishListEntryModel.class);
        given(entry2.getAddedDate()).willReturn(new Date());
        given(entry2.getSelectedVariant()).willReturn("5234324");
        entries.add(entry1);
        entries.add(entry2);
        given(targetWishListModel.getEntries()).willReturn(entries);
        final CustomerProductInfo info1 = mock(CustomerProductInfo.class);
        given(targetWishListEntryModelConverter.convert(entry1)).willReturn(info1);

        final List<CustomerProductInfo> favouriteProducts = targetWishListFacadeImpl.retrieveFavouriteProducts();
        assertThat(favouriteProducts).isNotEmpty();
        assertThat(favouriteProducts.size()).isEqualTo(1);
        assertThat(favouriteProducts).containsExactly(info1);
    }

    @Test
    public void testReplaceFavouritesWithLatestWithSameList() {
        final List<CustomerProductInfo> customerProductInfo = new ArrayList<>();
        final List<CustomerProductInfo> updatedCustomerProductInfo = new ArrayList<>();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel)))
                .willReturn(Boolean.FALSE);
        targetWishListFacadeImpl.replaceFavouritesWithLatest(customerProductInfo,
                updatedCustomerProductInfo);
        verifyZeroInteractions(targetWishListService);
    }

    @Test
    public void testReplaceFavouritesWithLatestAsAnonymous() {
        final List<CustomerProductInfo> customerProductInfo = new ArrayList<>();
        final List<CustomerProductInfo> updatedCustomerProductInfo = new ArrayList<>();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel)))
                .willReturn(Boolean.TRUE);
        targetWishListFacadeImpl.replaceFavouritesWithLatest(customerProductInfo,
                updatedCustomerProductInfo);
        verifyZeroInteractions(targetWishListService);
    }

    @Test
    public void testReplaceFavouritesWithLatestWithDiffList() {
        final List<CustomerProductInfo> customerProductInfo = new ArrayList<>();
        customerProductInfo.add(new CustomerProductInfo());
        final List<CustomerProductInfo> updatedCustomerProductInfo = new ArrayList<>();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        given(Boolean.valueOf(userService.isAnonymousUser(targetCustomerModel)))
                .willReturn(Boolean.FALSE);
        targetWishListFacadeImpl.replaceFavouritesWithLatest(customerProductInfo,
                updatedCustomerProductInfo);
        verify(targetWishListService).replaceFavouritesWithLatest(targetCustomerModel,
                new ArrayList<TargetWishListEntryData>(), salesApplication);
    }

    /**
     * 
     * @param info
     * @return TargetWishListEntryData
     */
    private TargetWishListEntryData buildNewTargetWishListEntryData(final CustomerProductInfo info) {
        final TargetWishListEntryData entryData = new TargetWishListEntryData();
        entryData.setBaseProductCode(info.getBaseProductCode());
        entryData.setSelectedVariantCode(info.getSelectedVariantCode());
        entryData.setTimeStamp(info.getTimeStamp());
        return entryData;
    }

    /**
     * @param actualEntry
     * @param info
     */
    private void verifyTargetWishListEntryData(final TargetWishListEntryData actualEntry,
            final CustomerProductInfo info) {
        assertThat(actualEntry.getBaseProductCode()).isNotNull().isNotEmpty()
                .isEqualTo(info.getBaseProductCode());
        assertThat(actualEntry.getSelectedVariantCode()).isNotNull().isNotEmpty()
                .isEqualTo(info.getSelectedVariantCode());
        assertThat(actualEntry.getTimeStamp()).isNotNull().isNotEmpty().isEqualTo(info.getTimeStamp());
    }

    /**
     * @return CustomerProductInfo
     */
    protected CustomerProductInfo buildCustomerProductInfo(final String baseProductCode, final String variantCode,
            final String timeStamp) {
        final CustomerProductInfo info = new CustomerProductInfo();
        info.setBaseProductCode(baseProductCode);
        info.setSelectedVariantCode(variantCode);
        info.setTimeStamp(timeStamp);

        return info;
    }

}
