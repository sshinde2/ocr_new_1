/**
 * 
 */
package au.com.target.tgtfacades.storefinder.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.storefinder.data.StoreStockVisiblityHolder;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtfacades.util.TargetPointOfServiceHelper;

import com.google.common.collect.ImmutableList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductStoreStockConverterTest {

    @InjectMocks
    private final TargetProductStoreStockConverter converter = new TargetProductStoreStockConverter();
    @Mock
    private Populator<PointOfServiceDistanceData, PointOfServiceData> pointOfServiceDistanceDataPopulator;
    @Mock
    private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;
    @Mock
    private PointOfServiceModel pointOfServiceModel;
    @Mock
    private ProductModel productModel;
    @Mock
    private AddressData address;
    @Mock
    private TargetPointOfServiceHelper targetPointOfServiceHelper;
    @Mock
    private CountryData countryData;
    @Mock
    private TargetSharedConfigFacade targetSharedConfigFacade;
    private String[] stockStatusLevel;
    private TargetPointOfServiceStockData target;
    private PointOfServiceDistanceData pointOfServiceDistanceData;
    private StockVisibilityItemLookupResponseDto stockLookupResult;
    private StoreStockVisiblityHolder source;

    @Before
    public void setUp() {
        target = new TargetPointOfServiceStockData();
        source = new StoreStockVisiblityHolder();
        source.setProduct(productModel);
        target.setAddress(address);
        stockStatusLevel = new String[] { "no", "low", "high" };
        converter.setStockStatusLevel(stockStatusLevel);
        converter.setTargetClass(TargetPointOfServiceStockData.class);
        given(address.getCountry()).willReturn(countryData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertWithNoPointOfServiceDistanceData() {
        source.setPointOfServiceDistanceData(null);
        source.setStockLookupResult(null);
        converter.convert(source);
    }

    @Test
    public void testConvertWithNoStock() {
        preparePointOfServiceDistanceData();
        source.setStockLookupResult(null);
        final TargetPointOfServiceStockData storeStockData = converter.convert(source);
        assertThat(storeStockData.getStockLevel()).isNull();
    }

    @Test
    public void testConvertWithEmptyStockResult() {
        preparePointOfServiceDistanceData();
        source.setStockLookupResult(new StockVisibilityItemLookupResponseDto());
        final TargetPointOfServiceStockData storeStockData = converter.convert(source);
        assertThat(storeStockData.getStockLevel()).isNull();
    }

    @Test
    public void testConvertWithMatchedStock() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD)).willReturn("0");

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD)).willReturn("8");

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("2");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("low");
    }

    @Test
    public void testConvertWithMatchedStoresNoStock() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("0");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD)).willReturn("2");
        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD)).willReturn("8");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("no");
    }

    @Test
    public void testConvertWithMatchedStoresNoStockOnBorderValue() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();
        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("2");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD)).willReturn("2");
        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD)).willReturn("8");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("no");
    }

    @Test
    public void testConvertWithMatchedStockWithLowThresholdOnBorderValue() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD)).willReturn("2");

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD)).willReturn("8");


        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("8");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("low");
    }

    @Test
    public void testConvertWithMatchedStockWithLowThreshold() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD)).willReturn("2");

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD)).willReturn("8");

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("6");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("low");
    }


    @Test
    public void testConvertWithMatchedStockWithHighThreshold() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD)).willReturn("2");

        given(targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD)).willReturn("8");

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("9");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("high");
    }

    @Test
    public void testConvertWithMatchedStoreshighStockWithoutConfig() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("15");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");
        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("high");
    }



    @Test
    public void testConvertWithNoSoh() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");

        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("no");
    }

    @Test
    public void testConvertWithNASoh() {
        //prepare pointOfServiceDistanceData
        preparePointOfServiceDistanceData();

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("N/A");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");

        converter.populate(source, target);
        assertThat(target.getStockLevel()).isEqualTo("no");
    }

    @Test
    public void testConvertWithStockOfDifferentProductCode() {
        preparePointOfServiceDistanceData();

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode1");
        item.setSoh("1");
        item.setStoreNumber("1211");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");

        converter.populate(source, target);
        assertThat(target.getStockLevel()).isNull();
    }

    @Test
    public void testConvertWithStockFromDifferentStore() {
        preparePointOfServiceDistanceData();

        //prepare stock result with matched and unmatched result
        stockLookupResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("productCode");
        item.setSoh("0");
        item.setStoreNumber("12119");
        stockLookupResult.setItems(ImmutableList.of(item));
        source.setStockLookupResult(stockLookupResult);

        //set matching store number and product number
        target.setStoreNumber(Integer.valueOf(1211));
        given(productModel.getCode()).willReturn("productCode");

        converter.populate(source, target);
        assertThat(target.getStockLevel()).isNull();
    }

    @Test
    public void verifyDirectionUrlIsPopulated() {
        source.setLocationText("Hamyln Heights");
        target.setName("Geelong");
        given(address.getLine1()).willReturn("line1");
        given(address.getTown()).willReturn("Geelong");
        given(address.getPostalCode()).willReturn("3025");
        given(countryData.getName()).willReturn("Australia");
        preparePointOfServiceDistanceData();
        converter.populate(source, target);
        verify(targetPointOfServiceHelper).populateDirectionUrl("Hamyln Heights", target);
    }


    private void preparePointOfServiceDistanceData() {
        pointOfServiceDistanceData = new PointOfServiceDistanceData();
        pointOfServiceDistanceData.setPointOfService(pointOfServiceModel);
        pointOfServiceDistanceData.setDistanceKm(2.1d);
        source.setPointOfServiceDistanceData(pointOfServiceDistanceData);
    }
}
