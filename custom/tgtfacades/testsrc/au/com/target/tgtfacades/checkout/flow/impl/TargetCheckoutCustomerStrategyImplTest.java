package au.com.target.tgtfacades.checkout.flow.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpSession;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author asingh78
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class })
public class TargetCheckoutCustomerStrategyImplTest {
    @InjectMocks
    protected TargetCheckoutCustomerStrategyImpl defaultCheckoutCustomerStrategy = new TargetCheckoutCustomerStrategyImpl();
    @Mock
    private UserService userService;
    @Mock
    private TargetLaybyCartService targetLaybyCartService;
    @Mock
    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;
    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;
    @Mock
    private SessionService sessionService;
    @Mock
    private ModelService modelService;
    @Mock
    private HttpSession session;
    @Mock
    private TargetCustomerAccountService customerAccountService;

    @Test
    public void getCurrentUserForAnonymousCheckout() {
        final TargetCheckoutCustomerStrategyImpl checkoutCustomerStrategy = Mockito
                .spy(defaultCheckoutCustomerStrategy);
        Mockito.doReturn(Boolean.TRUE).when(checkoutCustomerStrategy).isAnonymousCheckout();
        final TargetCustomerModel customerModel = BDDMockito.mock(TargetCustomerModel.class);
        final PurchaseOptionModel purchaseOptionModel = BDDMockito.mock(PurchaseOptionModel.class);
        BDDMockito.given(sessionService
                .getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION)).willReturn("BuyNow");
        BDDMockito.given(targetPurchaseOptionHelper
                .getPurchaseOptionModel("BuyNow")).willReturn(purchaseOptionModel);

        BDDMockito.given(userService.getUserForUID(BDDMockito.anyString())).willReturn(customerModel);
        final CartModel cart = BDDMockito.mock(CartModel.class);
        BDDMockito.given(targetLaybyCartService.getSessionCart()).willReturn(cart);

        BDDMockito.given(cart.getUser()).willReturn(customerModel);
        BDDMockito.given(customerModel.getUid()).willReturn("TestUser");
        final CustomerModel customer = checkoutCustomerStrategy.getCurrentUserForCheckout();
        Assert.assertEquals(customerModel, customer);
    }

    @Test
    public void getCurrentUserForAnonymousCheckoutWithCustomerModel() {
        final TargetCheckoutCustomerStrategyImpl checkoutCustomerStrategy = Mockito
                .spy(defaultCheckoutCustomerStrategy);
        Mockito.doReturn(Boolean.TRUE).when(checkoutCustomerStrategy).isAnonymousCheckout();
        final CustomerModel customerModel = BDDMockito.mock(CustomerModel.class);
        final PurchaseOptionModel purchaseOptionModel = BDDMockito.mock(PurchaseOptionModel.class);
        BDDMockito.given(sessionService
                .getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION)).willReturn("BuyNow");
        BDDMockito.given(targetPurchaseOptionHelper
                .getPurchaseOptionModel("BuyNow")).willReturn(purchaseOptionModel);

        BDDMockito.given(userService.getUserForUID(BDDMockito.anyString())).willReturn(customerModel);
        final CartModel cart = BDDMockito.mock(CartModel.class);
        BDDMockito.given(targetLaybyCartService.getSessionCart()).willReturn(cart);

        BDDMockito.given(cart.getUser()).willReturn(customerModel);
        BDDMockito.given(customerModel.getUid()).willReturn("TestUser");
        Assert.assertNull(checkoutCustomerStrategy.getCurrentUserForCheckout());
    }

    @Test
    public void getCurrentUserForCheckoutUserAsNotCustomer() {
        final TargetCheckoutCustomerStrategyImpl checkoutCustomerStrategy = Mockito
                .spy(defaultCheckoutCustomerStrategy);
        Mockito.doReturn(Boolean.FALSE).when(checkoutCustomerStrategy).isAnonymousCheckout();
        final EmployeeModel employeeModel = BDDMockito.mock(EmployeeModel.class);
        BDDMockito.given(userService.getCurrentUser()).willReturn(employeeModel);
        final CustomerModel customer = checkoutCustomerStrategy.getCurrentUserForCheckout();
        Assert.assertNull(customer);
    }

    @Test
    public void getCurrentUserForCheckoutUserAsCustomer() {
        final TargetCheckoutCustomerStrategyImpl checkoutCustomerStrategy = Mockito
                .spy(defaultCheckoutCustomerStrategy);
        Mockito.doReturn(Boolean.FALSE).when(checkoutCustomerStrategy).isAnonymousCheckout();
        final TargetCustomerModel customerModel = BDDMockito.mock(TargetCustomerModel.class);
        BDDMockito.given(userService.getCurrentUser()).willReturn(customerModel);
        final CustomerModel customer = checkoutCustomerStrategy.getCurrentUserForCheckout();
        Assert.assertEquals(customerModel, customer);
    }

    @Test
    public void testRemoveGuestCheckoutCustomer() {
        final TargetCustomerModel guestCustomer = BDDMockito.mock(TargetCustomerModel.class);
        final TargetCustomerModel anonymousUser = BDDMockito.mock(TargetCustomerModel.class);
        final CartModel cart = new CartModel();
        cart.setUser(guestCustomer);

        BDDMockito.given(targetLaybyCartService.getSessionCart()).willReturn(cart);
        BDDMockito.given(userService.getAnonymousUser()).willReturn(anonymousUser);

        defaultCheckoutCustomerStrategy.removeGuestCheckoutCustomer(guestCustomer, anonymousUser);

        Assert.assertEquals(anonymousUser, cart.getUser());
        BDDMockito.verify(modelService).remove(guestCustomer);
    }

    @Test
    public void testIsAnonymousCheckoutWithAnonymousUser() {
        final UserModel mockUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(mockUser);

        doReturn(Boolean.TRUE).when(userService).isAnonymousUser(mockUser);

        final boolean result = defaultCheckoutCustomerStrategy.isAnonymousCheckout();

        assertThat(result).isTrue();
    }

    @Test
    public void testIsAnonymousCheckoutWithCheckoutRegisteredAsGuest() {
        final UserModel mockUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(mockUser);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockUser);

        doReturn(Boolean.TRUE).when(sessionService).getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);

        final boolean result = defaultCheckoutCustomerStrategy.isAnonymousCheckout();

        assertThat(result).isTrue();
    }

    @Test
    public void testIsAnonymousCheckoutNonAnonymousCheckout() {
        final UserModel mockUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(mockUser);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockUser);

        doReturn(Boolean.FALSE).when(sessionService).getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);

        final boolean result = defaultCheckoutCustomerStrategy.isAnonymousCheckout();

        assertThat(result).isFalse();
    }

    @Test
    public void testresetGuestCartWithCurrentUserWithCurrentUserNull() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.doNothing().when(Registry.class, "setCurrentTenantByID", "master");
        given(customerAccountService.getCurrentUserModelBySession(session)).willReturn(null);
        defaultCheckoutCustomerStrategy.resetGuestCartWithCurrentUser(session);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testresetGuestCartWithCurrentUserWithCurrentCartNull() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.doNothing().when(Registry.class, "setCurrentTenantByID", "master");
        final UserModel mockUser = mock(UserModel.class);
        given(customerAccountService.getCurrentUserModelBySession(session)).willReturn(mockUser);
        given(targetLaybyCartService.getCurrentSessionCartBySession(session)).willReturn(null);
        defaultCheckoutCustomerStrategy.resetGuestCartWithCurrentUser(session);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testresetGuestCartWithCurrentUserWithCurrentCartUserNull() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.doNothing().when(Registry.class, "setCurrentTenantByID", "master");
        final UserModel mockUser = mock(UserModel.class);
        final CartModel cart = new CartModel();
        cart.setUser(null);
        given(customerAccountService.getCurrentUserModelBySession(session)).willReturn(mockUser);
        given(targetLaybyCartService.getCurrentSessionCartBySession(session)).willReturn(cart);
        defaultCheckoutCustomerStrategy.resetGuestCartWithCurrentUser(session);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testresetGuestCartWithCurrentUserWithCartUserEqualsCurrentUser() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.doNothing().when(Registry.class, "setCurrentTenantByID", "master");
        final UserModel mockUser = mock(UserModel.class);
        final CartModel cart = new CartModel();
        cart.setUser(mockUser);
        given(customerAccountService.getCurrentUserModelBySession(session)).willReturn(mockUser);
        given(targetLaybyCartService.getCurrentSessionCartBySession(session)).willReturn(cart);
        defaultCheckoutCustomerStrategy.resetGuestCartWithCurrentUser(session);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testresetGuestCartWithCurrentUserWithCartUserAnonymous() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.doNothing().when(Registry.class, "setCurrentTenantByID", "master");
        final UserModel mockUser = mock(UserModel.class);
        final CartModel cart = new CartModel();
        final UserModel cartUser = mock(UserModel.class);
        cart.setUser(cartUser);
        given(customerAccountService.getCurrentUserModelBySession(session)).willReturn(mockUser);
        given(targetLaybyCartService.getCurrentSessionCartBySession(session)).willReturn(cart);
        defaultCheckoutCustomerStrategy.resetGuestCartWithCurrentUser(session);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testresetGuestCartWithCurrentUserWithCartUserGuest() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.doNothing().when(Registry.class, "setCurrentTenantByID", "master");
        final UserModel mockUser = mock(UserModel.class);
        final CartModel cart = new CartModel();
        final TargetCustomerModel cartUser = mock(TargetCustomerModel.class);
        doReturn(CustomerType.GUEST).when(cartUser).getType();
        cart.setUser(cartUser);
        given(customerAccountService.getCurrentUserModelBySession(session)).willReturn(mockUser);
        given(targetLaybyCartService.getCurrentSessionCartBySession(session)).willReturn(cart);
        defaultCheckoutCustomerStrategy.resetGuestCartWithCurrentUser(session);
        verify(modelService).remove(BDDMockito.any());
    }

}
