/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerDataVariantPopulatorTest {

    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @Mock
    private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;

    @Mock
    private StockData stockData;

    @Mock
    private StockLevelStatus status;

    @InjectMocks
    private final TargetProductListerDataVariantPopulator targetProductListVariantPopulator = new TargetProductListerDataVariantPopulator();

    @Test
    public void testPopulateAllInstock() {

        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        target.setName("AllInstockProduct");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        childProducts.add(createVariantRecord(target.getName(), "Red", "P1000", "1"));
        childProducts.add(createVariantRecord(target.getName(), "Green", "P1000", "1"));
        childProducts.add(createVariantRecord(target.getName(), "Blue", "P1000", "1"));
        source.setProduct(childProducts);

        given(status.getCode()).willReturn("inStock");
        given(stockData.getStockLevelStatus()).willReturn(status);
        given(stockLevelStatusConverter.convert(StockLevelStatus.INSTOCK)).willReturn(stockData);

        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo("AllInstockProduct");
        assertThat(target.getTargetVariantProductListerData()).hasSize(3);
        assertThat(target.getTargetVariantProductListerData().get(0).getGridImageUrls()).hasSize(2);
        assertThat(target.getTargetVariantProductListerData().get(1).getSwatchImageUrls()).hasSize(3);
        assertThat(target.getTargetVariantProductListerData().get(1).getHeroImageUrls()).hasSize(1);
        assertThat(target.getTargetVariantProductListerData().get(2).getListImageUrls()).hasSize(4);
        assertThat(target.getTargetVariantProductListerData().get(2).getThumbImageUrl()).hasSize(1);
        assertThat(target.isInStock()).isTrue();
        assertThat(target.getStock().getStockLevelStatus().getCode()).isEqualTo(StockLevelStatus.INSTOCK.toString());

    }

    @Test
    public void testPopulateAllOutofStock() {

        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        target.setName("AllOOSProduct");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        childProducts.add(createVariantRecord(target.getName(), "Red", "P1000", "0"));
        childProducts.add(createVariantRecord(target.getName(), "Green", "P1000", "0"));
        childProducts.add(createVariantRecord(target.getName(), "Blue", "P1000", "0"));
        source.setProduct(childProducts);

        given(status.getCode()).willReturn("outOfStock");
        given(stockData.getStockLevelStatus()).willReturn(status);
        given(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK)).willReturn(stockData);

        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo("AllOOSProduct");
        assertThat(target.isInStock()).isFalse();
        assertThat(target.getStock().getStockLevelStatus().getCode()).isEqualTo(StockLevelStatus.OUTOFSTOCK.toString());

    }

    @Test
    public void testPopulatePartialInStock() {

        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        target.setName("PartialInstockProduct");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        childProducts.add(createVariantRecord(target.getName(), "Red", "P1000", "1"));
        childProducts.add(createVariantRecord(target.getName(), "Green", "P1000", "0"));
        childProducts.add(createVariantRecord(target.getName(), "Blue", "P1000", "0"));
        source.setProduct(childProducts);

        given(status.getCode()).willReturn("inStock");
        given(stockData.getStockLevelStatus()).willReturn(status);
        given(stockLevelStatusConverter.convert(StockLevelStatus.INSTOCK)).willReturn(stockData);

        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo("PartialInstockProduct");
        assertThat(target.isInStock()).isTrue();
        assertThat(target.getStock().getStockLevelStatus().getCode()).isEqualTo(StockLevelStatus.INSTOCK.toString());

    }

    @Test
    public void testPopulateAssortedProperty() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        target.setName("Test Product");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        final EndecaProduct variantProduct1 = createVariantRecord(target.getName(), "Red", "P1000", null);
        variantProduct1.setAssorted(Boolean.FALSE);
        childProducts.add(variantProduct1);
        final EndecaProduct variantProduct2 = createVariantRecord(target.getName(), "Green", "P1000", null);
        variantProduct2.setAssorted(Boolean.TRUE);
        childProducts.add(variantProduct2);
        source.setProduct(childProducts);

        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo("Test Product");
        assertThat(target.getTargetVariantProductListerData()).isNotNull();
        assertThat(target.getTargetVariantProductListerData()).hasSize(2);
        final TargetVariantProductListerData variantData1 = target.getTargetVariantProductListerData().get(0);
        assertThat(variantData1.getAssorted()).isFalse();
        final TargetVariantProductListerData variantData2 = target.getTargetVariantProductListerData().get(1);
        assertThat(variantData2.getAssorted()).isTrue();
    }

    @Test
    public void testPopulateDisplayOnlyProperty() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        target.setName("Test Product");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        final EndecaProduct variantProduct1 = createVariantRecord(target.getName(), "Red", "P1000", null);
        variantProduct1.setDisplayOnlyFlag(Boolean.FALSE);
        childProducts.add(variantProduct1);
        final EndecaProduct variantProduct2 = createVariantRecord(target.getName(), "Green", "P1000", null);
        variantProduct2.setDisplayOnlyFlag(Boolean.TRUE);
        childProducts.add(variantProduct2);
        source.setProduct(childProducts);
        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getTargetVariantProductListerData()).isNotNull();
        assertThat(target.getTargetVariantProductListerData().size()).isEqualTo(2);
        TargetVariantProductListerData variantData1 = target.getTargetVariantProductListerData().get(0);
        assertThat(variantData1.isDisplayOnly()).isFalse();
        TargetVariantProductListerData variantData2 = target.getTargetVariantProductListerData().get(1);
        assertThat(variantData2.isDisplayOnly()).isFalse();

        childProducts.remove(variantProduct1);
        childProducts.remove(variantProduct2);
        variantProduct1.setDisplayOnlyFlag(Boolean.TRUE);
        childProducts.add(variantProduct1);
        variantProduct2.setDisplayOnlyFlag(Boolean.TRUE);
        childProducts.add(variantProduct2);
        source.setProduct(childProducts);
        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getTargetVariantProductListerData()).isNotNull();
        assertThat(target.getTargetVariantProductListerData().size()).isEqualTo(2);
        variantData1 = target.getTargetVariantProductListerData().get(0);
        assertThat(variantData1.isDisplayOnly()).isTrue();
        assertThat(variantData1.getProductDisplayType()).isEqualTo(ProductDisplayType.AVAILABLE_FOR_SALE);
        variantData2 = target.getTargetVariantProductListerData().get(1);
        assertThat(variantData2.isDisplayOnly()).isTrue();

        childProducts.remove(variantProduct1);
        childProducts.remove(variantProduct2);
        variantProduct1.setDisplayOnlyFlag(Boolean.FALSE);
        childProducts.add(variantProduct1);
        variantProduct2.setDisplayOnlyFlag(Boolean.FALSE);
        childProducts.add(variantProduct2);
        source.setProduct(childProducts);
        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getTargetVariantProductListerData()).isNotNull();
        assertThat(target.getTargetVariantProductListerData().size()).isEqualTo(2);
        variantData1 = target.getTargetVariantProductListerData().get(0);
        assertThat(variantData1.isDisplayOnly()).isFalse();
        variantData2 = target.getTargetVariantProductListerData().get(1);
        assertThat(variantData2.isDisplayOnly()).isFalse();
    }

    @Test
    public void testPopulatePreorderProduct() {

        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        final Date date = TargetDateUtil.getStringAsDate("2018-07-31 00:00:00.0");
        target.setName("PreOrderProduct");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        final EndecaProduct childProduct = createVariantRecord(target.getName(), "Red", "P1004", "0");
        childProduct.setProductDisplayType("PREORDER_AVAILABLE");
        childProduct.setNormalSaleStartDateTime(date);
        childProducts.add(childProduct);
        source.setProduct(childProducts);

        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo("PreOrderProduct");
        assertThat(target.getTargetVariantProductListerData().get(0).getProductDisplayType())
                .isEqualTo(ProductDisplayType.PREORDER_AVAILABLE);
        assertThat(target.getTargetVariantProductListerData().get(0).getNormalSaleStartDate()).isEqualTo(date);

    }

    @Test
    public void testPopulatePreorderComingSoonProduct() {

        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        final Date date = TargetDateUtil.getStringAsDate("2018-07-31 00:00:00.0");
        target.setName("PreOrderComingSoonProduct");
        final List<EndecaProduct> childProducts = new ArrayList<>();
        final EndecaProduct childProduct = createVariantRecord(target.getName(), "Red", "P1005", "0");
        childProduct.setProductDisplayType("COMING_SOON");
        childProduct.setNormalSaleStartDateTime(date);
        childProducts.add(childProduct);
        source.setProduct(childProducts);

        targetProductListVariantPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo("PreOrderComingSoonProduct");
        assertThat(target.getTargetVariantProductListerData().get(0).getProductDisplayType())
                .isEqualTo(ProductDisplayType.COMING_SOON);
        assertThat(target.getTargetVariantProductListerData().get(0).getNormalSaleStartDate()).isEqualTo(date);

    }



    private List<String> createImageUrls(final String imgType, final String pcode, final int noOfImages) {
        final List<String> imgUrls = new ArrayList<>();
        for (int i = 0; i < noOfImages; i++) {
            imgUrls.add("/" + imgType + "/" + pcode + "_" + i + ".jpg");
        }
        return imgUrls;
    }

    private EndecaProduct createVariantRecord(final String productName, final String colour, final String parentCode,
            final String instockflag) {
        final EndecaProduct endecaChildProduct = new EndecaProduct();
        endecaChildProduct.setColour(colour);
        endecaChildProduct.setSwatchColour(colour);
        endecaChildProduct.setColourVariantCode(parentCode + "_" + colour);
        endecaChildProduct.setGridImage(createImageUrls("grid", endecaChildProduct.getColourVariantCode(), 2));
        endecaChildProduct.setSwatchImage(createImageUrls("swatch", endecaChildProduct.getColourVariantCode(), 3));
        endecaChildProduct.setListImage(createImageUrls("list", endecaChildProduct.getColourVariantCode(), 4));
        endecaChildProduct.setHeroImage(createImageUrls("hero", endecaChildProduct.getColourVariantCode(), 1));
        endecaChildProduct.setThumbImage(createImageUrls("thumb", endecaChildProduct.getColourVariantCode(), 1));
        endecaChildProduct.setInStockFlag(instockflag);
        endecaChildProduct.setProductDisplayType("AVAILABLE_FOR_SALE");
        given(targetProductDataUrlResolver.resolve(productName, endecaChildProduct.getColourVariantCode()))
                .willReturn(
                        productName + "/" + endecaChildProduct.getColourVariantCode());
        return endecaChildProduct;
    }
}
