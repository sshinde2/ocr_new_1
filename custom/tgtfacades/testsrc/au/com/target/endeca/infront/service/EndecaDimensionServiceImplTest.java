/**
 * 
 */
package au.com.target.endeca.infront.service;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.MutableDimLocationList;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.soleng.urlformatter.TargetURLFormatter;


/**
 * @author smudumba
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaDimensionServiceImplTest {

    @InjectMocks
    @Spy
    private final EndecaDimensionServiceImpl dimensionServiceImpl = new EndecaDimensionServiceImpl();

    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @Mock
    private TargetURLFormatter targetURLFormatter;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Test
    public void testGetDimensionsStateThrowsTargetEndecaWrapperException() throws Exception {
        final Map<String, String> dimensionIdMap = new HashMap<String, String>();
        dimensionIdMap.put("dim1", "12345");
        given(endecaDimensionCacheService.getDimension("dim1", "12345")).willReturn(StringUtils.EMPTY);
        expectedException.expect(TargetEndecaWrapperException.class);
        expectedException.expectMessage("CATEGORY CODE NOT FOUND IN CACHE FOR CATEGORY");
        dimensionServiceImpl.getDimensionNavigationState(dimensionIdMap);

    }

    @Test
    public void testGetDimensionsStateReturnsAValue() throws Exception {
        final Map<String, String> dimensionIdMap = new HashMap<String, String>();
        dimensionIdMap.put("dim1", "12345");
        final String expectedValue = "1111111";
        doReturn(expectedValue).when(dimensionServiceImpl)
                .getEncodedNavigationState(any(MutableDimLocationList.class));
        given(endecaDimensionCacheService.getDimension("dim1", "12345")).willReturn(expectedValue);
        final String result = dimensionServiceImpl.getDimensionNavigationState(dimensionIdMap);
        assertThat(result).isEqualTo(expectedValue);
    }


}
