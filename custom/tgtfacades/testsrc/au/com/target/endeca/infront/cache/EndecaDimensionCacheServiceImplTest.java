/**
 * 
 */
package au.com.target.endeca.infront.cache;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;

import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.service.EndecaDimensionService;


/**
 * @author smudumba
 * 
 */
@UnitTest
public class EndecaDimensionCacheServiceImplTest {

    @Mock
    private EndecaDimensionService endecaDimensionService;

    /**
     * Test method for {@link au.com.target.endeca.infront.cache.EndecaDimensionCacheServiceImpl#getDimensions()}.
     */
    @Test
    public void testGetDimensions() throws Exception {
        final EndecaDimensions eds = new EndecaDimensions();
        final Map<String, Map<String, String>> dims = new HashMap<String, Map<String, String>>();
        final Map<String, String> refinement = new HashMap<String, String>();
        refinement.put("AFL", "12345");
        dims.put("brand", refinement);
        eds.setDimensions(dims);
        final String excpectedValue = "12345";

        final EndecaDimensionCacheServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionCacheServiceImpl.class);
        BDDMockito.given(mockGetDimensions.getDimensions()).willReturn(eds);
        final EndecaDimensions result = mockGetDimensions.getDimensions();
        final String value = result.getDimensions().get("brand").get("AFL");
        Assert.assertEquals(value, excpectedValue);
    }

    @Test
    public void testGetDimensionsNull() throws Exception {
        final EndecaDimensions eds = new EndecaDimensions();
        eds.setDimensions(null);
        final EndecaDimensionCacheServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionCacheServiceImpl.class);
        BDDMockito.given(mockGetDimensions.getDimensions()).willReturn(eds);
        final EndecaDimensions result = mockGetDimensions.getDimensions();
        Assert.assertEquals(result, eds);
    }

    @Test
    public void testGetDimension() throws Exception {
        final String refinement = "12345";
        final EndecaDimensionCacheServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionCacheServiceImpl.class);
        BDDMockito.given(mockGetDimensions.getDimension(Mockito.anyString(), Mockito.anyString())).willReturn(
                refinement);
        final String result = mockGetDimensions.getDimension(Mockito.anyString(), Mockito.anyString());
        Assert.assertEquals(result, refinement);
    }

    @Test
    public void testGetDimensionwithNull() throws Exception {
        final String refinement = "";
        final EndecaDimensionCacheServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionCacheServiceImpl.class);
        BDDMockito.given(mockGetDimensions.getDimension(Mockito.anyString(), Mockito.anyString())).willReturn(
                "");
        final String result = mockGetDimensions.getDimension(Mockito.anyString(), Mockito.anyString());
        Assert.assertEquals(result, refinement);
    }

    @Test
    public void testInvalidateEndecaDimensionsCache() throws Exception {
        final Boolean validValue = Boolean.TRUE;
        final EndecaDimensionCacheServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionCacheServiceImpl.class);
        BDDMockito.given(mockGetDimensions.invalidateEndecaDimensionsCache()).willReturn(Boolean.TRUE);

        final Boolean result = mockGetDimensions.invalidateEndecaDimensionsCache();
        Assert.assertEquals(result, validValue);
    }



}
