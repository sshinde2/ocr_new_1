package au.com.target.endeca.infront.querybuilder;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.UrlENEQuery;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.util.EndecaQueryConnecton;


/**
 * @author pvarghe2
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSiteMapQueryBuilderTest {

    /**
     * 
     */
    private static final int PAGE_NUMBER = 1;

    /**
     * 
     */
    private static final int MAX_PRODUCT_COUNT = 100;

    @Mock
    private EndecaQueryConnecton endecaQueryConnection;

    @Mock
    private HttpENEConnection httpENEConnection;

    @Mock
    private ENEQueryResults queryResults;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration config;

    @InjectMocks
    private final TargetSiteMapQueryBuilder targetSiteMapQueryBuilder = new TargetSiteMapQueryBuilder();

    @Before
    public void setup() {
        given(configurationService.getConfiguration()).willReturn(config);
    }

    @Test
    public void testGetQueryResults() throws TargetEndecaException, ENEQueryException {
        final ArgumentCaptor<UrlENEQuery> query = ArgumentCaptor
                .forClass(UrlENEQuery.class);
        given(endecaQueryConnection.getConection()).willReturn(httpENEConnection);
        given(httpENEConnection.query(query.capture())).willReturn(queryResults);
        given(config.getString(EndecaConstants.FieldList.SITEMPAP_FIELDLIST))
                .willReturn("productName|colorVariantCode");
        final ENEQueryResults results = targetSiteMapQueryBuilder.getQueryResults(160, PAGE_NUMBER, MAX_PRODUCT_COUNT);
        final UrlENEQuery expectedQuery = query.getValue();
        assertThat(results).isEqualTo(queryResults);
        verify(httpENEConnection).query(query.capture());
        assertThat(expectedQuery.getNavRollupKey())
                .isEqualTo(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        assertThat(expectedQuery.getNavRecordFilter())
                .isEqualTo("AND(merchDepartmentCode:160,productDisplayFlag:1)");
        assertThat(expectedQuery.getNavNumERecs())
                .isEqualTo(MAX_PRODUCT_COUNT);
        assertThat(expectedQuery.getNavAggrERecsOffset()).isEqualTo(PAGE_NUMBER * MAX_PRODUCT_COUNT);
    }

}
