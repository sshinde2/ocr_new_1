package au.com.target.endeca.infront.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.model.RecordAction;
import com.endeca.infront.navigation.model.CollectionFilter;
import com.endeca.infront.navigation.model.Filter;
import com.endeca.infront.navigation.model.PropertyFilter;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.url.impl.TargetProductRecordSearchUrlResover;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import junit.framework.Assert;


/**
 * Test class for {@link TargetEndecaUtil}
 *
 * @author ragarwa3
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetEndecaUtilTest {

    @InjectMocks
    private final TargetEndecaUtil targetEndecaUtil = new TargetEndecaUtil();

    @Mock
    private TargetProductRecordSearchUrlResover productRecordSearchUrlResover;

    @Mock
    private TargetPriceHelper targetPriceHelper;

    @Test
    public void testCreateStratifyForOrderedRecommendationsEmpty() {
        Assert.assertTrue(CollectionUtils.isEmpty(targetEndecaUtil
                .createStratifyForOrderedRecommendations(ListUtils.EMPTY_LIST)));
    }

    @Test
    public void testCreateStratifyForOrderedRecommendations() {
        final List colourVariantCodes = new ArrayList<>();
        colourVariantCodes.add("P1001_black");
        colourVariantCodes.add("P1002_black");

        final List<CollectionFilter> stratifyColl = targetEndecaUtil
                .createStratifyForOrderedRecommendations(colourVariantCodes);

        Assert.assertTrue(CollectionUtils.isNotEmpty(stratifyColl));
        Assert.assertEquals(2, stratifyColl.size());

        boolean isFirstRecord = true;
        for (final CollectionFilter filter : stratifyColl) {
            final Filter innerFilter = filter.getInnerFilter();
            Assert.assertNotNull(innerFilter);
            Assert.assertTrue(innerFilter instanceof PropertyFilter);

            final PropertyFilter propertyFilter = (PropertyFilter)innerFilter;
            Assert.assertEquals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE,
                    propertyFilter.getName());

            if (isFirstRecord) {
                Assert.assertEquals("P1001_black", propertyFilter.getValue());
            }
            else {
                Assert.assertEquals("P1002_black", propertyFilter.getValue());
            }
            isFirstRecord = false;
        }
    }

    @Test
    public void testResolveProductPageUrl() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        recordList.add(record1);
        final Record record2 = new Record();
        final RecordAction recordAction2 = new RecordAction();
        record2.setDetailsAction(recordAction2);
        recordList.add(record2);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);

        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(productRecordSearchUrlResover.resolve(record2)).thenReturn("testUrl2");

        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertEquals(2, resolvedRecordList.size());
        Assert.assertEquals("testUrl1", resolvedRecordList.get(0).getDetailsAction().getRecordState());
        Assert.assertEquals("testUrl2", resolvedRecordList.get(1).getDetailsAction().getRecordState());
    }

    @Test
    public void testResolveProductPageUrlWithEmptyRecordsList() {
        final List<Record> recordList = new ArrayList<>();
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);

        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertEquals(0, resolvedRecordList.size());
    }

    @Test
    public void testResolveProductPageUrlWithEmptyUrl() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        recordList.add(record1);
        final Record record2 = new Record();
        final RecordAction recordAction2 = new RecordAction();
        record2.setDetailsAction(recordAction2);
        recordList.add(record2);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);

        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn(StringUtils.EMPTY);
        BDDMockito.when(productRecordSearchUrlResover.resolve(record2)).thenReturn(StringUtils.EMPTY);

        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertEquals(2, resolvedRecordList.size());
        Assert.assertEquals(null, resolvedRecordList.get(0).getDetailsAction().getRecordState());
        Assert.assertEquals(null, resolvedRecordList.get(1).getDetailsAction().getRecordState());
    }

    @Test
    public void testCreateRecordFilterForColourVariants() {
        final String colourVariant1 = "test_red";
        final String colourVariant2 = "test_black";
        final List<String> variantCodes = new ArrayList<>();
        variantCodes.add(colourVariant1);
        variantCodes.add(colourVariant2);
        final String result = targetEndecaUtil.createRecordFilterForColourVariants(variantCodes);
        Assert.assertEquals("OR(colourVariantCode:test_red,colourVariantCode:test_black)", result);
    }

    @Test
    public void testCreateRecordFilterForEmptyColourVariants() {
        final String colourVariant1 = "";
        final String colourVariant2 = "";
        final List<String> variantCodes = new ArrayList<>();
        variantCodes.add(colourVariant1);
        variantCodes.add(colourVariant2);
        final String result = targetEndecaUtil.createRecordFilterForColourVariants(variantCodes);
        Assert.assertEquals("OR(colourVariantCode:,colourVariantCode:)", result);
    }

    @Test
    public void testResolveProductPricesWithMinPrice() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final PriceData fromPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(10), "$");
        final Map attributes = new HashMap();
        attributes.put("min_price", null);
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        record1.setAttributes(attributes);
        recordList.add(record1);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);

        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(targetPriceHelper.createPriceData(Double.valueOf(Mockito.anyDouble()))).thenReturn(
                fromPrice);

        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertEquals(1, resolvedRecordList.size());
        Assert.assertEquals("testUrl1", resolvedRecordList.get(0).getDetailsAction().getRecordState());
    }

    @Test
    public void testResolveProductPricesWithWasPrice() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final PriceData fromPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(10), "$");
        final Map attributes = new HashMap();
        attributes.put("max_wasPrice", null);
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        record1.setAttributes(attributes);
        recordList.add(record1);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);

        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(targetPriceHelper.createPriceData(Double.valueOf(Mockito.anyDouble()))).thenReturn(
                fromPrice);

        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertEquals(1, resolvedRecordList.size());
        Assert.assertEquals("testUrl1", resolvedRecordList.get(0).getDetailsAction().getRecordState());
    }

    private PriceData createPriceData(final PriceDataType type, final BigDecimal value, final String iso) {
        final PriceData priceData = new PriceData();
        priceData.setCurrencyIso(iso);
        priceData.setPriceType(type);
        priceData.setValue(value);
        return priceData;
    }

    @Test
    public void testEnrichEndecaRecordsWithInvalidPriceRange() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final Map recordAttrs = new HashMap();
        recordAttrs.put("min_price", "20.0");
        recordAttrs.put("min_wasPrice", "10.0");
        recordAttrs.put("max_price", "10.0");
        recordAttrs.put("max_wasPrice", "10.0");
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        record1.setAttributes(recordAttrs);
        recordList.add(record1);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);
        final PriceData mockPriceData = Mockito.mock(PriceData.class);
        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(targetPriceHelper.createPriceData(Double.valueOf(Mockito.anyDouble()))).thenReturn(
                mockPriceData);
        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertNotNull(resolvedRecordList);
        Assert.assertNull(recordAttrs.get("min_wasPrice"));
        Assert.assertNull(recordAttrs.get("max_wasPrice"));
        Assert.assertNotNull(recordAttrs.get("min_price"));
        Assert.assertNull(recordAttrs.get("max_price"));

    }

    @Test
    public void testEnrichEndecaRecordsWithValidPriceRange() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final Map recordAttrs = new HashMap();
        recordAttrs.put("min_price", "20.0");
        recordAttrs.put("max_price", "30.0");
        recordAttrs.put("min_wasPrice", "50.0");
        recordAttrs.put("max_wasPrice", "60.0");
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        record1.setAttributes(recordAttrs);
        recordList.add(record1);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);
        final PriceData mockPriceData = Mockito.mock(PriceData.class);
        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(targetPriceHelper.createPriceData(Double.valueOf(Mockito.anyDouble()))).thenReturn(
                mockPriceData);
        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertNotNull(resolvedRecordList);
        Assert.assertNotNull(recordAttrs.get("min_wasPrice"));
        Assert.assertNotNull(recordAttrs.get("max_wasPrice"));
        Assert.assertNotNull(recordAttrs.get("min_price"));
        Assert.assertNotNull(recordAttrs.get("max_price"));
        Assert.assertTrue(recordAttrs.get("min_price") instanceof PriceData);
        Assert.assertTrue(recordAttrs.get("max_price") instanceof PriceData);
        Assert.assertTrue(recordAttrs.get("min_wasPrice") instanceof PriceData);
        Assert.assertTrue(recordAttrs.get("max_wasPrice") instanceof PriceData);
    }


    @Test
    public void testUnescapeProductName() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final Map recordAttrs = new HashMap();
        recordAttrs.put("productName", "Apple&reg; Ipad");
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        record1.setAttributes(recordAttrs);
        recordList.add(record1);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);
        final PriceData mockPriceData = Mockito.mock(PriceData.class);
        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(targetPriceHelper.createPriceData(Double.valueOf(Mockito.anyDouble()))).thenReturn(
                mockPriceData);
        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                true);
        Assert.assertNotNull(resolvedRecordList);
        Assert.assertNotNull(recordAttrs.get("productName"));
        Assert.assertEquals("Apple® Ipad", recordAttrs.get("productName"));
    }

    @Test
    public void testNotToUnescapeProductName() {
        final List<Record> recordList = new ArrayList<>();
        final Record record1 = new Record();
        final Map recordAttrs = new HashMap();
        recordAttrs.put("productName", "Apple&reg; Ipad");
        final RecordAction recordAction1 = new RecordAction();
        record1.setDetailsAction(recordAction1);
        record1.setAttributes(recordAttrs);
        recordList.add(record1);
        final ResultsList inputResultList = new ResultsList(new ResultsListConfig("testType"));
        inputResultList.setRecords(recordList);
        final PriceData mockPriceData = Mockito.mock(PriceData.class);
        BDDMockito.when(productRecordSearchUrlResover.resolve(record1)).thenReturn("testUrl1");
        BDDMockito.when(targetPriceHelper.createPriceData(Double.valueOf(Mockito.anyDouble()))).thenReturn(
                mockPriceData);
        final List<Record> resolvedRecordList = targetEndecaUtil.enrichEndecaRecords(inputResultList.getRecords(),
                false);
        Assert.assertNotNull(resolvedRecordList);
        Assert.assertNotNull(recordAttrs.get("productName"));
        Assert.assertEquals("Apple&reg; Ipad", recordAttrs.get("productName"));
    }
}
