/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.MapAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.navigation.NavigationException;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.url.ActionPathProvider;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.MDimValList;
import com.endeca.navigation.MDimensionList;
import com.endeca.navigation.MPropertyMap;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;

import au.com.target.endeca.infront.constants.EndecaConstants;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetRefinementMenuHandlerTest {

    @Mock
    private MdexRequest mdexRequest;

    @Mock
    private ENEQueryResults queryResults;

    @Mock
    private Navigation navigation;

    @Mock
    private DimVal root;

    @Mock
    private DimValList refinements;

    @Mock
    private DimensionList refinementDimensions;

    @Mock
    private DimensionList descriptorDimensions;

    @Mock
    private ActionPathProvider actionPathProvider;

    @Mock
    private NavigationState navigationState;

    @Mock
    private Dimension dimension;

    @Mock
    private TargetRefinementMenuConfig cartridgeConfig;

    @InjectMocks
    private final TargetRefinementMenuHandler refinementMenuHandler = new TargetRefinementMenuHandler();

    @Before
    public void setup() throws NavigationException {
        final NavigationState reverseNavigation = mock(NavigationState.class);
        given(reverseNavigation.toString()).willReturn("reverseNavigation");
        given(navigationState.removeNavigationFilter(anyString())).willReturn(reverseNavigation);
        final NavigationState forwardNavigation = mock(NavigationState.class);
        given(forwardNavigation.toString()).willReturn("forwardNavigation");
        given(navigationState.selectNavigationFilter(anyString())).willReturn(forwardNavigation);
        given(Integer.valueOf(cartridgeConfig.getLimit())).willReturn(Integer.valueOf(200));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_DEFAULT);

        given(mdexRequest.execute()).willReturn(queryResults);
        given(queryResults.getNavigation()).willReturn(navigation);
        given(cartridgeConfig.getDimensionId()).willReturn("1");
        given(navigation.getRefinementDimensions()).willReturn(refinementDimensions);
        given(navigation.getDescriptorDimensions()).willReturn(descriptorDimensions);
        given(dimension.getRoot()).willReturn(root);
        given(Boolean.valueOf(root.isMultiSelectAnd())).willReturn(Boolean.TRUE);
        final PropertyMap propertyMap = mock(PropertyMap.class);
        given(root.getProperties()).willReturn(propertyMap);
        given(cartridgeConfig.getRefinementsShown()).willReturn("all");
        given(dimension.getRefinements()).willReturn(refinements);
        given(navigationState.putParameter(anyString(), anyString())).willReturn(navigationState);
        final DimValList completePath = mock(DimValList.class);
        given(dimension.getCompletePath()).willReturn(completePath);
    }

    @Test
    public void testRefinementsWithoutSelected() {
        final MDimValList dimValList = new MDimValList();
        dimValList.add(createDimVal(1, "colour", 1, "black", null));
        dimValList.add(createDimVal(1, "colour", 2, "blue", null));
        dimValList.add(createDimVal(1, "colour", 3, "red", null));
        given(dimension.getRefinements()).willReturn(dimValList);
        final List<Refinement> result = refinementMenuHandler.extractRefinements(dimension, cartridgeConfig,
                new MDimensionList());
        assertThat(result).isNotEmpty();
        assertThat(result.size()).isEqualTo(3);
        assertThat(result).onProperty("label").containsExactly("black", "blue", "red");
        assertThat(result).onProperty("navigationState").containsExactly("forwardNavigation", "forwardNavigation",
                "forwardNavigation");
        assertThat(result.get(0).getProperties())
                .includes(MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, "1"));
        assertThat(result.get(1).getProperties())
                .includes(MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, "2"));
        assertThat(result.get(2).getProperties())
                .includes(MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, "3"));
    }

    @Test
    public void testRefinementWithSelected() {
        final MDimValList dimValList = new MDimValList();
        dimValList.add(createDimVal(1, "colour", 1, "black", null));
        dimValList.add(createDimVal(1, "colour", 3, "red", null));
        given(Long.valueOf(dimension.getId())).willReturn(Long.valueOf(1));
        given(dimension.getRefinements()).willReturn(dimValList);
        final MDimensionList selectedList = new MDimensionList();
        final Dimension selectedDimension = mock(Dimension.class);
        given(Long.valueOf(selectedDimension.getId())).willReturn(Long.valueOf(1));
        final DimVal selectedDimVal = mock(DimVal.class);
        given(Boolean.valueOf(selectedDimVal.isMultiSelectAnd())).willReturn(Boolean.TRUE);
        given(selectedDimension.getRoot()).willReturn(selectedDimVal);
        final DimVal dimVal = createDimVal(1, "colour", 2, "blue", null);
        given(selectedDimension.getDescriptor()).willReturn(dimVal);
        selectedList.add(selectedDimension);
        final List<Refinement> result = refinementMenuHandler.extractRefinements(dimension, cartridgeConfig,
                selectedList);
        assertThat(result).isNotEmpty();
        assertThat(result.size()).isEqualTo(3);
        assertThat(result).onProperty("label").containsExactly("black", "blue", "red");
        assertThat(result).onProperty("navigationState").containsExactly("forwardNavigation", "reverseNavigation",
                "forwardNavigation");
        assertThat(result.get(0).getProperties()).excludes(
                MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_SELECTED, StringUtils.EMPTY));
        assertThat(result.get(1).getProperties()).includes(
                MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_SELECTED, StringUtils.EMPTY));
        assertThat(result.get(2).getProperties()).excludes(
                MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_SELECTED, StringUtils.EMPTY));
        assertThat(result.get(0).getProperties())
                .includes(MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, "1"));
        assertThat(result.get(1).getProperties())
                .includes(MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, "2"));
        assertThat(result.get(2).getProperties())
                .includes(MapAssert.entry(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, "3"));
    }

    @Test
    public void testProcess() throws CartridgeHandlerException {
        given(refinementDimensions.getDimension(1)).willReturn(dimension);
        refinementMenuHandler.process(cartridgeConfig);
        verify(descriptorDimensions, never()).getDimension(1);
    }

    @Test
    public void testProcessWithAllRefinementsSelected() throws CartridgeHandlerException {
        given(descriptorDimensions.getDimension(1)).willReturn(dimension);
        refinementMenuHandler.process(cartridgeConfig);
        verify(descriptorDimensions).getDimension(1);
        verify(cartridgeConfig).isSeoFollowEnabled();
    }

    @Test
    public void testProcessWithRefinements() throws CartridgeHandlerException {
        final MDimensionList selectedList = new MDimensionList();
        final Dimension selectedDimension = mock(Dimension.class);
        given(Long.valueOf(selectedDimension.getId())).willReturn(Long.valueOf(1));
        final DimVal selectedDimVal = mock(DimVal.class);
        given(Boolean.valueOf(selectedDimVal.isMultiSelectAnd())).willReturn(Boolean.TRUE);
        given(selectedDimension.getRoot()).willReturn(selectedDimVal);
        final DimVal dimVal = createDimVal(1, "colour", 2, "blue", null);
        given(selectedDimension.getDescriptor()).willReturn(dimVal);
        selectedList.add(selectedDimension);
        given(navigation.getDescriptorDimensions()).willReturn(selectedList);
        given(refinementDimensions.getDimension(1)).willReturn(dimension);
        refinementMenuHandler.process(cartridgeConfig);
        verify(cartridgeConfig, times(2)).isSeoFollowEnabled();
    }

    private DimVal createDimVal(final long dimensionId, final String dimensionName, final long id,
            final String dimensionValue, final String sortOrder) {
        final DimVal dimVal = mock(DimVal.class);
        given(Long.valueOf(dimVal.getDimensionId())).willReturn(Long.valueOf(dimensionId));
        given(dimVal.getDimensionName()).willReturn(dimensionName);
        given(dimVal.getName()).willReturn(dimensionValue);
        given(Long.valueOf(dimVal.getId())).willReturn(Long.valueOf(id));
        final MPropertyMap properties = new MPropertyMap();
        if (sortOrder != null) {
            properties.put("DGraph.Strata", sortOrder);
        }
        given(dimVal.getProperties()).willReturn(properties);
        return dimVal;
    }

}
