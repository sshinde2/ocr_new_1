/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import java.util.HashMap;
import java.util.Map;

import au.com.target.endeca.infront.constants.EndecaConstants;


/**
 * Endeca Query Helper
 * 
 * @author jjayawa1
 * 
 */
public final class EndecaQueryHelper {

    private static final Integer ASCENDING = Integer.valueOf(0);
    private static final Integer DESCENDING = Integer.valueOf(1);

    private EndecaQueryHelper() {

    }

    /**
     * 
     * Returns the correct sort parameters for a selected sort type.
     * 
     * @param sortBy
     * @return java.util.Map
     */
    public static Map<String, Integer> getEndecaSortParams(final String sortBy) {
        final Map<String, Integer> sortStateMap = new HashMap<String, Integer>();

        if (sortBy.equals("name-asc")) {
            sortStateMap.put("productName", ASCENDING);
        }
        else if (sortBy.equals("name-desc")) {
            sortStateMap.put("productName", DESCENDING);
        }
        else if (sortBy.equals("price-asc")) {
            sortStateMap.put("price", ASCENDING);
        }
        else if (sortBy.equals("price-desc")) {
            sortStateMap.put("price", DESCENDING);
        }
        else if (sortBy.equals("rating")) {
            sortStateMap.put("reviewAvgRating", DESCENDING);
        }
        else if (sortBy.equals("latest")) {
            sortStateMap.put("onlineDate", DESCENDING);
        }
        else if (sortBy.equals("brand")) {
            sortStateMap.put("brand", ASCENDING);
        }
        else if (sortBy.equals("sales")) {
            sortStateMap.put("noOfSales", DESCENDING);
        }
        else if (sortBy.equals("relevance")) {
            sortStateMap.put("relevance", DESCENDING);
        }

        return sortStateMap;
    }

    public static String getEndecaSortQueryString(final String sortBy, final Integer direction) {
        final StringBuilder sortString = new StringBuilder();
        sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_DEFAULT);
        if ("productName".equals(sortBy)) {
            if (ASCENDING.equals(direction)) {
                sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_NAME_ASC);
            }
            else {
                sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_NAME_DESC);
            }
        }
        else if ("price".equals(sortBy)) {
            if (ASCENDING.equals(direction)) {
                sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_PRICE_ASC);
            }
            else {
                sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_PRICE_DESC);
            }
        }
        else if ("reviewAvgRating".equals(sortBy)) {
            sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_RATING);
        }
        else if ("onlineDate".equals(sortBy)) {
            sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_LATEST);
        }
        else if ("brand".equals(sortBy)) {
            sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_BRAND);
        }
        else if ("noOfSales".equals(sortBy)) {
            sortString.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_SALES);
        }

        return sortString.toString();
    }
}
