/**
 * 
 */
package au.com.target.endeca.infront.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EndecaAncestor {
    @XmlElement
    private String navigationState;

    @XmlElement
    private String element;

    @XmlElement
    private String code;

    /**
     * @return the navigationState
     */
    public String getNavigationState() {
        return navigationState;
    }

    /**
     * @param navigationState
     *            the navigationState to set
     */
    public void setNavigationState(final String navigationState) {
        this.navigationState = navigationState;
    }

    /**
     * @return the element
     */
    public String getElement() {
        return element;
    }

    /**
     * @param element
     *            the element to set
     */
    public void setElement(final String element) {
        this.element = element;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

}
