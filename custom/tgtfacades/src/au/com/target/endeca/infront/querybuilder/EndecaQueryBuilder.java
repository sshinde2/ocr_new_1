/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryHelper;


/**
 * A helper class to build the query for components.
 * 
 * @author jjayawa1
 * 
 */
public class EndecaQueryBuilder {

    private static final String COMMA = ",";
    private static final String RIGHT_PARENTHESIS = ")";
    private static final String LEFT_PARENTHESIS = "(";
    private static final String DOUBLE_RIGHT_PARENTHESIS = "))";
    private static final String DEFAULT_DIMENSION = "0";
    private static final String RECORD_FILTER_PRODUCT_DISPLAY_FLAG = "productDisplayFlag";
    private static final String TRUE_VALUE = "1";
    private static final String AND_VALUE = "AND";
    private static final String OR_VALUE = "OR";
    private String brandCode = null;

    /**
     * Builds the sort query.
     * 
     * @param searchStateData
     * @return java.lang.String
     */
    public String buildSortQuery(final EndecaSearchStateData searchStateData) {
        final StringBuilder sortQuery = new StringBuilder();
        if (searchStateData.getSearchField() != null)
        // && searchStateData.getSearchField().equals(EndecaConstants.SEARCH_FIELD_PRODUCT_CODE))
        {
            sortQuery.append("Endeca.stratify(");
            for (int i = 0; i < searchStateData.getSearchTerm().size(); i++) {
                final String productCode = searchStateData.getSearchTerm().get(i);
                sortQuery.append("collection()/record[" + searchStateData.getSearchField() + "=\"");
                sortQuery.append(StringUtils.strip(productCode));
                sortQuery.append("\"]");
                if (i != (searchStateData.getSearchTerm().size() - 1)) {
                    sortQuery.append(COMMA);
                }
            }
            sortQuery.append(RIGHT_PARENTHESIS);
        }

        sortQuery.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_DEFAULT);
        if (searchStateData.getSortStateMap() != null) {
            for (final Map.Entry<String, Integer> entry : searchStateData.getSortStateMap().entrySet()) {
                sortQuery.append(EndecaQueryHelper.getEndecaSortQueryString(entry.getKey(), entry.getValue()));

            }
        }
        else if (!searchStateData.isSkipDefaultSort()) {
            sortQuery.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_LATEST);
        }
        return sortQuery.toString();
    }

    /**
     * Builds the query for ordering records in the result.
     * 
     * @param searchStateData
     * @return java.lang.String
     */
    public String buildProductRecordQuery(final EndecaSearchStateData searchStateData) {

        final StringBuilder recordQuery = new StringBuilder();
        if (searchStateData != null) {
            recordQuery.append(AND_VALUE).append(LEFT_PARENTHESIS);
            if (!searchStateData.isSkipInStockFilter()) {
                recordQuery.append(RECORD_FILTER_PRODUCT_DISPLAY_FLAG).append(':').append(TRUE_VALUE).append(',');
            }
            final boolean productAvailable = searchStateData.getSearchField() != null ? true : false;

            if (productAvailable) {
                recordQuery.append(OR_VALUE).append(LEFT_PARENTHESIS);
                for (int i = 0; i < searchStateData.getSearchTerm().size(); i++) {
                    final String productCode = searchStateData.getSearchTerm().get(i);
                    recordQuery.append(searchStateData.getSearchField()).append(':');
                    recordQuery.append(productCode);
                    if (i != (searchStateData.getSearchTerm().size() - 1)) {
                        recordQuery.append(COMMA);
                    }
                }

                appendBrandAndCategories(searchStateData, recordQuery, productAvailable);
                recordQuery.append(DOUBLE_RIGHT_PARENTHESIS);
            }
            else if (null != searchStateData.getCategoryCodes() && !searchStateData.getCategoryCodes().isEmpty()) {
                appendBrandAndCategories(searchStateData, recordQuery, productAvailable);

                recordQuery.append(RIGHT_PARENTHESIS);
            }
            else if (null != searchStateData.getBrandCode()) {
                recordQuery.append(searchStateData.getBrandCode());
                recordQuery.append(RIGHT_PARENTHESIS);
            }
            else {
                recordQuery.delete(0, recordQuery.length());
            }
        }

        return recordQuery.toString();
    }

    private void appendBrandAndCategories(final EndecaSearchStateData searchStateData, final StringBuilder recordQuery,
            final boolean productAvailable) {
        brandCode = searchStateData.getBrandCode();
        final List<String> categories = searchStateData.getCategoryCodes();
        final boolean brandCodeExists = StringUtils.isNotEmpty(brandCode);
        appendBrand(recordQuery, categories, brandCodeExists, productAvailable);

        appendCategories(recordQuery, categories, brandCodeExists, productAvailable);

        if (brandCodeExists && CollectionUtils.isNotEmpty(categories)) {
            if (categories.size() <= 1) {
                recordQuery.append(RIGHT_PARENTHESIS);
            }
            else {
                recordQuery.append(DOUBLE_RIGHT_PARENTHESIS);
            }
        }
    }

    private void appendBrand(final StringBuilder recordQuery, final List<String> categories,
            final boolean brandCodeExists, final boolean productAvailable) {

        if (brandCodeExists && CollectionUtils.isNotEmpty(categories)) {
            if (productAvailable) {
                recordQuery.append(COMMA);
            }
            recordQuery.append(AND_VALUE).append(LEFT_PARENTHESIS);
            recordQuery.append(brandCode);

            recordQuery.append(COMMA);
        }
        else if (brandCodeExists) {
            recordQuery.append(COMMA);
            recordQuery.append(brandCode);
        }
    }

    private void appendCategories(final StringBuilder recordQuery, final List<String> categories,
            final boolean brandCodeExists, final boolean productAvailable) {
        final boolean categoryExist = CollectionUtils.isNotEmpty(categories);
        if (categoryExist) {
            if (!brandCodeExists && productAvailable) {
                recordQuery.append(COMMA);
            }
            for (int i = 0; i < categories.size(); i++) {
                if (categories.size() > 1 && i == 0) {
                    recordQuery.append(OR_VALUE).append(LEFT_PARENTHESIS);
                }
                final String catCode = categories.get(i);
                recordQuery.append(catCode);

                if (i != (categories.size() - 1)) {
                    recordQuery.append(COMMA);
                }
            }
        }
        if (!brandCodeExists && categoryExist && categories.size() > 1) {
            recordQuery.append(RIGHT_PARENTHESIS);
        }
    }

    /**
     * Builds the query for concatenating dimensions.
     * 
     * @param searchStateData
     * @return java.lang.String
     */
    public String buildN(final EndecaSearchStateData searchStateData) {
        final StringBuilder recordQuery = new StringBuilder();
        if (null != searchStateData.getCategoryCodes()
                && CollectionUtils.isNotEmpty(searchStateData.getCategoryCodes())) {
            for (int i = 0; i < searchStateData.getCategoryCodes().size(); i++) {
                final String catCode = searchStateData.getCategoryCodes().get(i);
                recordQuery.append(catCode);
                if (i != (searchStateData.getCategoryCodes().size() - 1)) {
                    recordQuery.append(COMMA);
                }
            }
        }
        else {
            recordQuery.append(DEFAULT_DIMENSION);
        }

        return recordQuery.toString();
    }

    /**
     * @param searchStateData
     * @return sortQuery
     */
    public String buildYouMayAlsoLikeSortQuery(final EndecaSearchStateData searchStateData) {
        final StringBuilder sortQuery = new StringBuilder();
        if (StringUtils.isNotEmpty(searchStateData.getSizeGroup())) {
            sortQuery.append("Endeca.stratify(");
            sortQuery.append(
                    "collection()/record[" + EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_GROUP + "=\"");
            sortQuery.append(searchStateData.getSizeGroup());
            sortQuery.append("\"]");
            sortQuery.append(RIGHT_PARENTHESIS);
        }       
        sortQuery.append(EndecaConstants.EndecaSortParams.ENDECA_SORT_LATEST);
        return sortQuery.toString();
    }

    /**
     * @return recordFilterQuery
     */
    public String buildRecordFilterInStockProduct() {
        final StringBuilder recordFilterQuery = new StringBuilder();
        // Filter query: product has stock and is approved

        recordFilterQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_AND_FILTER_KEY);
        recordFilterQuery.append(LEFT_PARENTHESIS);
        recordFilterQuery.append(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG
                + EndecaConstants.EndecaFilterQuery.ENDECA_FILTER_KEY_VALUE_SEPERATOR + EndecaConstants.ENDECA_NP_1);
        recordFilterQuery.append(RIGHT_PARENTHESIS);
        return recordFilterQuery.toString();
    }

}
