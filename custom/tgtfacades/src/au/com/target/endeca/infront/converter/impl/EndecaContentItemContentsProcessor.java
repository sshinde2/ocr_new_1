/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.EndecaContentItemConverter;
import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.ContentItem;


/**
 * @author Paul
 * 
 */
public class EndecaContentItemContentsProcessor implements
        EndecaContentItemConverter<EndecaSearch> {

    private EndecaContentItemBreadCrumbProcessor endecaContentItemBreadCrumbProcessor;
    private EndecaContentItemGuidedNavProcessor endecaContentItemGuidedNavProcessor;
    private EndecaContentItemResultsListProcessor endecaContentItemResultsListProcessor;
    private EndecaContentItemRecommendationsProcessor endecaContentItemRecommendationsProcessor;

    /* (non-Javadoc)
     * @see au.com.target.endeca.infront.converter.EndecaContentItemConverter#convertContentItem(com.endeca.infront.assembler.ContentItem, java.lang.Object)
     */
    @Override
    public EndecaSearch convertContentItem(final ContentItem contentItem, final EndecaSearch endecaSearch) {
        if (null == contentItem) {
            return endecaSearch;
        }
        final List<ContentItem> contentItemList = (List<ContentItem>)contentItem
                .get(EndecaConstants.EndecaParserNodes.ENDECA_CONTENTS);
        if (CollectionUtils.isEmpty(contentItemList)) {
            return endecaSearch;
        }
        final ContentItem contentItemContents = contentItemList.get(0);
        processContentItemList(
                (List<ContentItem>)contentItemContents.get(EndecaConstants.EndecaParserNodes.ENDECA_SECONDARY_CONTENT),
                endecaSearch);

        final Object mainContent = contentItemContents.get(EndecaConstants.EndecaParserNodes.ENDECA_MAIN_CONTENT);
        if (null != mainContent &&
                mainContent instanceof List<?>) {
            processContentItemList((List<ContentItem>)mainContent, endecaSearch);
        }
        return endecaSearch;
    }

    private void processContentItemList(final List<ContentItem> contentItemList, final EndecaSearch endecaSearch) {
        if (CollectionUtils.isEmpty(contentItemList)) {
            return;
        }

        for (int i = 0; i < contentItemList.size(); i++) {
            final ContentItem contentItem = contentItemList.get(i);
            if (contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_BREADCRUMBS)) {
                endecaContentItemBreadCrumbProcessor.process(contentItem, endecaSearch);
            }
            if (contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_CONTENT_SLOT_SECONDARY)) {
                processContentItemList(
                        (List<ContentItem>)contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_CONTENTS),
                        endecaSearch);
            }
            if (contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_GUIDED_NAVIGATION)) {
                endecaContentItemGuidedNavProcessor.process(contentItem, endecaSearch);
            }
            if (contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_CONTENT_SLOT_MAIN)) {
                processContentItemList(
                        (List<ContentItem>)contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_CONTENTS),
                        endecaSearch);
            }
            if (contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_RESULTS_LIST) ||
                    contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_RESULTS_LIST_SEARCH) ||
                    contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_RESULTS_LIST_CATEGORY) ||
                    contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_RESULTS_LIST_BRAND)) {
                endecaContentItemResultsListProcessor.process(contentItem, endecaSearch);
            }
            if (contentItem.getType().equals(EndecaConstants.EndecaParserNodes.ENDECA_RECOMMENDATIONS_BOUGHTBOUGHT)) {
                endecaContentItemRecommendationsProcessor.process(contentItem, endecaSearch);
            }
        }

    }

    /**
     * @param endecaContentItemResultsListProcessor
     *            the endecaContentItemResultsListProcessor to set
     */
    @Required
    public void setEndecaContentItemResultsListProcessor(
            final EndecaContentItemResultsListProcessor endecaContentItemResultsListProcessor) {
        this.endecaContentItemResultsListProcessor = endecaContentItemResultsListProcessor;
    }

    /**
     * @param endecaContentItemBreadCrumbProcessor
     *            the endecaContentItemBreadCrumbProcessor to set
     */
    @Required
    public void setEndecaContentItemBreadCrumbProcessor(
            final EndecaContentItemBreadCrumbProcessor endecaContentItemBreadCrumbProcessor) {
        this.endecaContentItemBreadCrumbProcessor = endecaContentItemBreadCrumbProcessor;
    }

    /**
     * @param endecaContentItemGuidedNavProcessor
     *            the endecaContentItemGuidedNavProcessor to set
     */
    @Required
    public void setEndecaContentItemGuidedNavProcessor(
            final EndecaContentItemGuidedNavProcessor endecaContentItemGuidedNavProcessor) {
        this.endecaContentItemGuidedNavProcessor = endecaContentItemGuidedNavProcessor;
    }

    /**
     * @param endecaContentItemRecommendationsProcessor
     *            the endecaContentItemRecommendationsProcessor to set
     */
    @Required
    public void setEndecaContentItemRecommendationsProcessor(
            final EndecaContentItemRecommendationsProcessor endecaContentItemRecommendationsProcessor) {
        this.endecaContentItemRecommendationsProcessor = endecaContentItemRecommendationsProcessor;
    }
}
