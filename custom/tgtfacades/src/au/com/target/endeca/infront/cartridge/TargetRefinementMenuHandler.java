/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.RefinementMenuHandler;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.support.RefinementBuilder;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.request.MdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.Navigation;

import au.com.target.endeca.infront.constants.EndecaConstants;


/**
 * Customise the refinement menu handler to retrieve selected refinements along with available.
 * 
 * @author mgazal
 *
 */
public class TargetRefinementMenuHandler extends RefinementMenuHandler {

    private MdexRequest mMdexRequest;

    @Override
    protected RefinementMenuConfig wrapConfig(final ContentItem pContentItem) {
        return new TargetRefinementMenuConfig(pContentItem);
    }

    @Override
    public RefinementMenu process(final RefinementMenuConfig cartridgeConfig) throws CartridgeHandlerException {
        final ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
        final TargetRefinementMenuConfig targetRefinementMenuConfig = (TargetRefinementMenuConfig)cartridgeConfig;
        getNavigationState().inform(results);
        final Navigation navigation = results.getNavigation();
        try {
            if (navigation == null) {
                return null;
            }
            Dimension dimension = results.getNavigation().getRefinementDimensions()
                    .getDimension(Long.parseLong(cartridgeConfig.getDimensionId()));

            if (dimension == null) {
                dimension = results.getNavigation().getDescriptorDimensions()
                        .getDimension(Long.parseLong(cartridgeConfig.getDimensionId()));
            }

            if (dimension == null) {
                return null;
            }

            final TargetRefinementMenu outputModel = new TargetRefinementMenu(targetRefinementMenuConfig);
            RefinementBuilder.populateRefinementMenuProperties(outputModel, cartridgeConfig, dimension, "Nrmc",
                    "showMoreIds", getNavigationState(), getActionPathProvider());

            final List<Refinement> refinements = extractRefinements(dimension, targetRefinementMenuConfig,
                    results.getNavigation().getDescriptorDimensions());
            outputModel.setRefinements(refinements);
            outputModel.setSeoFollowEnabled(CollectionUtils.size(navigation.getDescriptorDimensions()) == 1
                    ? targetRefinementMenuConfig.isSeoFollowEnabled() : false);
            outputModel.setActualFollowup(targetRefinementMenuConfig.isSeoFollowEnabled());

            return outputModel;
        }
        catch (final NumberFormatException e) {
            throw new CartridgeHandlerException(e);
        }
    }

    /**
     * Extracts a merged list of selected and available refinements. <br/>
     * Selected refinements will have the following differences:
     * <ul>
     * <li>navaigationState to remove the refinement as opposed to selecting it.</li>
     * <li>properties map will have a "selected" property</li>
     * </ul>
     * Also sorts the refinements consistent to the sort preference in <code>cartridgeConfig</code> using
     * {@link RefinementComparator}}
     * 
     * @param dimension
     * @param cartridgeConfig
     * @param selectedList
     * @return list of {@link Refinement}s
     */
    protected List<Refinement> extractRefinements(final Dimension dimension,
            final TargetRefinementMenuConfig cartridgeConfig,
            final DimensionList selectedList) {
        final DimValList availableList = dimension.getRefinements();
        final List refinements = new ArrayList(availableList.size() + selectedList.size());

        boolean sortRequired = false;
        int j = 0;
        for (int i = 0; i < selectedList.size() && i < cartridgeConfig.getLimit(); ++i) {
            final Dimension dim = (Dimension)selectedList.get(i);
            if ((dim.getRoot().isMultiSelectAnd() || dim.getRoot().isMultiSelectOr())
                    && dim.getId() == dimension.getId()) {
                final Refinement ref = RefinementBuilder.createRefinement(getActionPathProvider(),
                        dim.getDescriptor(), getNavigationState());
                ref.getProperties().put(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_SELECTED,
                        StringUtils.EMPTY);
                ref.getProperties().put(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID,
                        String.valueOf(dim.getDescriptor().getId()));
                ref.setNavigationState(getNavigationState()
                        .removeNavigationFilter(String.valueOf(dim.getDescriptor().getId())).toString());
                refinements.add(ref);
                j++;
                sortRequired = true;
            }
        }

        for (int i = 0; (i < availableList.size()) && (j < cartridgeConfig.getLimit()); ++i, ++j) {
            final Refinement ref = RefinementBuilder.createRefinement(getActionPathProvider(),
                    availableList.getDimValue(i),
                    getNavigationState());
            ref.getProperties().put(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID,
                    String.valueOf(availableList.getDimValue(i).getId()));
            refinements.add(ref);
        }

        if (sortRequired) {
            Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        }
        return refinements;
    }

    @Override
    protected MdexRequest createMdexRequest(final FilterState pFilterState, final MdexQuery pMdexQuery)
            throws CartridgeHandlerException {
        mMdexRequest = super.createMdexRequest(pFilterState, pMdexQuery);
        return mMdexRequest;
    }

}
