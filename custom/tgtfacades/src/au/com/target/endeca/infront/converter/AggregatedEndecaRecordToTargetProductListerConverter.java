/**
 *
 */
package au.com.target.endeca.infront.converter;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.endeca.infront.converter.populator.EndecaComponentPopulator;


/**
 * Abstract populator for components
 *
 * @author jjayawa1
 *
 */
public class AggregatedEndecaRecordToTargetProductListerConverter<SOURCE, TARGET> extends
        AbstractPopulatingConverter<SOURCE, TARGET> {


    /**
     * Populator to populate a Target based on a colour variant
     *
     * @param source
     *            source object
     * @param target
     *            converted object
     * @param colourVariant
     *            colour variant to populate
     * @throws ConversionException
     *             exception thrown during conversion
     */
    public void populate(final SOURCE source, final TARGET target, final String colourVariant)
            throws ConversionException {
        if (getPopulators() != null)
        {
            for (final Populator<SOURCE, TARGET> populator : getPopulators())
            {
                if (populator instanceof EndecaComponentPopulator) {
                    ((EndecaComponentPopulator)populator).populate(source, target, colourVariant);
                }
                else {
                    populator.populate(source, target);
                }
            }
        }
    }

    /**
     * Converter to convert to a Target based on a colour variant
     *
     * @param source
     * @param colourVariantCode
     * @return TARGET
     * @throws ConversionException
     */
    public TARGET convert(final SOURCE source, final String colourVariantCode)
            throws ConversionException {
        final TARGET prototype = createFromClass();
        populate(source, prototype, colourVariantCode);
        return prototype;
    }

}
