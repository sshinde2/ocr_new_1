/**
 * 
 */
package au.com.target.endeca.infront.exception;

/**
 * Class for Endeca integration exceptions
 * 
 * @author smudumba *
 */
public class TargetEndecaException extends Exception {

    public TargetEndecaException() {
        super();
    }

    /**
     * @param message
     * @param cause
     */
    public TargetEndecaException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public TargetEndecaException(final String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public TargetEndecaException(final Throwable cause) {
        super(cause);
    }

}
