/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.Date;
import java.util.List;


/**
 * @author rsamuel3
 * 
 */
public class EndecaProduct {
    private String name;

    private String code;

    private String baseProductCode;

    private String brand;

    private String description;

    private Boolean essential;

    private Boolean hotProduct;

    private Boolean newToOnline;

    private Boolean targetExclusive;

    private String topLevelCategoryName;

    private Boolean onlineExclusive;

    private Boolean clearance;

    private String maxPrice;

    private String minPrice;

    private String maxWasPrice;

    private String minWasPrice;

    private Boolean isGiftCard;

    private String numOfReviews;

    private String reviewAvgRating;

    private List<String> promotionalStatus;

    private List<EndecaProduct> product;

    private String wasPrice;

    private String colour;

    private String swatchColour;

    private String stockLevelStatus;

    private String colourVariantCode;

    private List<String> largeImage;

    private List<String> thumbImage;

    private List<String> swatchImage;

    private List<String> gridImage;

    private List<String> heroImage;

    private List<String> listImage;

    private String showwhenoutofstock;

    private String inStockFlag;

    private String dealEndtime;

    private String dealStartTime;

    private String dealDescription;

    private List<String> applicableDeliveryPromotions;

    private String tag;

    private Boolean newLowerPriceFlag;

    private Boolean previewFlag;

    private Boolean assorted;

    private Boolean displayOnlyFlag;

    private String maxAvailOnlineQty;

    private String maxAvailStoreQty;

    private String productDisplayType;

    private Date normalSaleStartDateTime;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }


    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the product
     */
    public List<EndecaProduct> getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final List<EndecaProduct> product) {
        this.product = product;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the stockLevelStatus
     */
    public String getStockLevelStatus() {
        return stockLevelStatus;
    }


    /**
     * @return the maxPrice
     */
    public String getMaxPrice() {
        return maxPrice;
    }

    /**
     * @return the minPrice
     */
    public String getMinPrice() {
        return minPrice;
    }

    /**
     * @return the maxWasPrice
     */
    public String getMaxWasPrice() {
        return maxWasPrice;
    }

    /**
     * @return the minWasPrice
     */
    public String getMinWasPrice() {
        return minWasPrice;
    }

    /**
     * @return the wasPrice
     */
    public String getWasPrice() {
        return wasPrice;
    }

    /**
     * @return the isGiftCard
     */
    public Boolean getIsGiftCard() {
        return isGiftCard;
    }

    /**
     * @param isGiftCard
     *            the isGiftCard to set
     */
    public void setIsGiftCard(final Boolean isGiftCard) {
        this.isGiftCard = isGiftCard;
    }

    /**
     * @return the numOfReviews
     */
    public String getNumOfReviews() {
        return numOfReviews;
    }

    /**
     * @return the reviewAvgRating
     */
    public String getReviewAvgRating() {
        return reviewAvgRating;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @param stockLevelStatus
     *            the stockLevelStatus to set
     */
    public void setStockLevelStatus(final String stockLevelStatus) {
        this.stockLevelStatus = stockLevelStatus;
    }


    /**
     * @param maxPrice
     *            the maxPrice to set
     */
    public void setMaxPrice(final String maxPrice) {
        this.maxPrice = maxPrice;
    }

    /**
     * @param minPrice
     *            the minPrice to set
     */
    public void setMinPrice(final String minPrice) {
        this.minPrice = minPrice;
    }

    /**
     * @param maxWasPrice
     *            the maxWasPrice to set
     */
    public void setMaxWasPrice(final String maxWasPrice) {
        this.maxWasPrice = maxWasPrice;
    }

    /**
     * @param minWasPrice
     *            the minWasPrice to set
     */
    public void setMinWasPrice(final String minWasPrice) {
        this.minWasPrice = minWasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final String wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @param numOfReviews
     *            the numOfReviews to set
     */
    public void setNumOfReviews(final String numOfReviews) {
        this.numOfReviews = numOfReviews;
    }

    /**
     * @param reviewAvgRating
     *            the reviewAvgRating to set
     */
    public void setReviewAvgRating(final String reviewAvgRating) {
        this.reviewAvgRating = reviewAvgRating;
    }


    /**
     * @return the targetExclusive
     */
    public Boolean getTargetExclusive() {
        return targetExclusive;
    }

    /**
     * @param targetExclusive
     *            the targetExclusive to set
     */
    public void setTargetExclusive(final Boolean targetExclusive) {
        this.targetExclusive = targetExclusive;
    }


    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the swatchColour
     */
    public String getSwatchColour() {
        return swatchColour;
    }

    /**
     * @param swatchColour
     *            the swatchColour to set
     */
    public void setSwatchColour(final String swatchColour) {
        this.swatchColour = swatchColour;
    }

    /**
     * @return the largeImage
     */
    public List<String> getLargeImage() {
        return largeImage;
    }

    /**
     * @param largeImage
     *            the largeImage to set
     */
    public void setLargeImage(final List<String> largeImage) {
        this.largeImage = largeImage;
    }

    /**
     * @return the thumbImage
     */
    public List<String> getThumbImage() {
        return thumbImage;
    }

    /**
     * @param thumbImage
     *            the thumbImage to set
     */
    public void setThumbImage(final List<String> thumbImage) {
        this.thumbImage = thumbImage;
    }

    /**
     * @return the swatchImage
     */
    public List<String> getSwatchImage() {
        return swatchImage;
    }

    /**
     * @param swatchImage
     *            the swatchImage to set
     */
    public void setSwatchImage(final List<String> swatchImage) {
        this.swatchImage = swatchImage;
    }

    /**
     * @return the gridImage
     */
    public List<String> getGridImage() {
        return gridImage;
    }

    /**
     * @param gridImage
     *            the gridImage to set
     */
    public void setGridImage(final List<String> gridImage) {
        this.gridImage = gridImage;
    }

    /**
     * @return the heroImage
     */
    public List<String> getHeroImage() {
        return heroImage;
    }

    /**
     * @param heroImage
     *            the heroImage to set
     */
    public void setHeroImage(final List<String> heroImage) {
        this.heroImage = heroImage;
    }

    /**
     * @return the listImage
     */
    public List<String> getListImage() {
        return listImage;
    }

    /**
     * @param listImage
     *            the listImage to set
     */
    public void setListImage(final List<String> listImage) {
        this.listImage = listImage;
    }

    /**
     * @return the showwhenoutofstock
     */
    public String getShowwhenoutofstock() {
        return showwhenoutofstock;
    }

    /**
     * @param showwhenoutofstock
     *            the showwhenoutofstock to set
     */
    public void setShowwhenoutofstock(final String showwhenoutofstock) {
        this.showwhenoutofstock = showwhenoutofstock;
    }

    /**
     * @return the inStockFlag
     */
    public String getInStockFlag() {
        return inStockFlag;
    }

    /**
     * @param inStockFlag
     *            the inStockFlag to set
     */
    public void setInStockFlag(final String inStockFlag) {
        this.inStockFlag = inStockFlag;
    }

    /**
     * @return the colourVariantCode
     */
    public String getColourVariantCode() {
        return colourVariantCode;
    }

    /**
     * @param colourVariantCode
     *            the colourVariantCode to set
     */
    public void setColourVariantCode(final String colourVariantCode) {
        this.colourVariantCode = colourVariantCode;
    }


    /**
     * @return the promotionalStatus
     */
    public List<String> getPromotionalStatus() {
        return promotionalStatus;
    }

    /**
     * @param promotionalStatus
     *            the promotionalStatus to set
     */
    public void setPromotionalStatus(final List<String> promotionalStatus) {
        this.promotionalStatus = promotionalStatus;
    }

    /**
     * @return the essential
     */
    public Boolean getEssential() {
        return essential;
    }

    /**
     * @param essential
     *            the essential to set
     */
    public void setEssential(final Boolean essential) {
        this.essential = essential;
    }

    /**
     * @return the hotProduct
     */
    public Boolean getHotProduct() {
        return hotProduct;
    }

    /**
     * @param hotProduct
     *            the hotProduct to set
     */
    public void setHotProduct(final Boolean hotProduct) {
        this.hotProduct = hotProduct;
    }

    /**
     * @return the newToOnline
     */
    public Boolean getNewToOnline() {
        return newToOnline;
    }

    /**
     * @param newToOnline
     *            the newToOnline to set
     */
    public void setNewToOnline(final Boolean newToOnline) {
        this.newToOnline = newToOnline;
    }

    /**
     * @return the onlineExclusive
     */
    public Boolean getOnlineExclusive() {
        return onlineExclusive;
    }

    /**
     * @param onlineExclusive
     *            the onlineExclusive to set
     */
    public void setOnlineExclusive(final Boolean onlineExclusive) {
        this.onlineExclusive = onlineExclusive;
    }

    /**
     * @return the clearance
     */
    public Boolean getClearance() {
        return clearance;
    }

    /**
     * @param clearance
     *            the clearance to set
     */
    public void setClearance(final Boolean clearance) {
        this.clearance = clearance;
    }


    /**
     * @return the dealendtime
     */
    public String getDealEndtime() {
        return dealEndtime;
    }

    /**
     * 
     * @param dealEndtime
     */
    public void setDealEndtime(final String dealEndtime) {
        this.dealEndtime = dealEndtime;
    }

    /**
     * @return the dealDescription
     */
    public String getDealDescription() {
        return dealDescription;
    }

    /**
     * @param dealDescription
     *            the dealDescription to set
     */
    public void setDealDescription(final String dealDescription) {
        this.dealDescription = dealDescription;
    }

    /**
     * @return the dealStartTime
     */
    public String getDealStartTime() {
        return dealStartTime;
    }

    /**
     * @param dealStartTime
     *            the dealStartTime to set
     */
    public void setDealStartTime(final String dealStartTime) {
        this.dealStartTime = dealStartTime;
    }

    /**
     * @return the applicableDeliveryPromotions
     */
    public List<String> getApplicableDeliveryPromotions() {
        return applicableDeliveryPromotions;
    }

    /**
     * @param applicableDeliveryPromotions
     *            the applicableDeliveryPromotions to set
     */
    public void setApplicableDeliveryPromotions(final List<String> applicableDeliveryPromotions) {
        this.applicableDeliveryPromotions = applicableDeliveryPromotions;
    }

    /**
     * @return the topLevelCategoryName
     */
    public String getTopLevelCategoryName() {
        return topLevelCategoryName;
    }

    /**
     * @param topLevelCategoryName
     *            the topLevelCategoryName to set
     */
    public void setTopLevelCategoryName(final String topLevelCategoryName) {
        this.topLevelCategoryName = topLevelCategoryName;
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag
     *            the tag to set
     */
    public void setTag(final String tag) {
        this.tag = tag;
    }

    /**
     * @return the newLowerPriceFlag
     */
    public Boolean getNewLowerPriceFlag() {
        return newLowerPriceFlag;
    }

    /**
     * @param newLowerPriceFlag
     *            the newLowerPriceFlag to set
     */
    public void setNewLowerPriceFlag(final Boolean newLowerPriceFlag) {
        this.newLowerPriceFlag = newLowerPriceFlag;
    }

    /**
     * @return the previewFlag
     */
    public Boolean getPreviewFlag() {
        return previewFlag;
    }

    /**
     * @param previewFlag
     *            the previewFlag to set
     */
    public void setPreviewFlag(final Boolean previewFlag) {
        this.previewFlag = previewFlag;
    }

    /**
     * @return the assorted
     */
    public Boolean getAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final Boolean assorted) {
        this.assorted = assorted;
    }

    /**
     * @return the displayOnlyFlag
     */
    public Boolean getDisplayOnlyFlag() {
        return displayOnlyFlag;
    }

    /**
     * @param displayOnlyFlag
     *            the displayOnlyFlag to set
     */
    public void setDisplayOnlyFlag(final Boolean displayOnlyFlag) {
        this.displayOnlyFlag = displayOnlyFlag;
    }

    /**
     * @return the maxAvailOnlineQty
     */
    public String getMaxAvailOnlineQty() {
        return maxAvailOnlineQty;
    }

    /**
     * @param maxAvailOnlineQty
     *            the maxAvailOnlineQty to set
     */
    public void setMaxAvailOnlineQty(final String maxAvailOnlineQty) {
        this.maxAvailOnlineQty = maxAvailOnlineQty;
    }

    /**
     * @return the maxAvailStoreQty
     */
    public String getMaxAvailStoreQty() {
        return maxAvailStoreQty;
    }

    /**
     * @param maxAvailStoreQty
     *            the maxAvailStoreQty to set
     */
    public void setMaxAvailStoreQty(final String maxAvailStoreQty) {
        this.maxAvailStoreQty = maxAvailStoreQty;
    }

    /**
     * @return the productDisplayType
     */
    public String getProductDisplayType() {
        return productDisplayType;
    }

    /**
     * @param productDisplayType
     *            the productDisplayType to set
     */
    public void setProductDisplayType(final String productDisplayType) {
        this.productDisplayType = productDisplayType;
    }

    /**
     * @return the normalSaleStartDateTime
     */
    public Date getNormalSaleStartDateTime() {
        return normalSaleStartDateTime;
    }

    /**
     * @param normalSaleStartDateTime
     *            the normalSaleStartDateTime to set
     */
    public void setNormalSaleStartDateTime(final Date normalSaleStartDateTime) {
        this.normalSaleStartDateTime = normalSaleStartDateTime;
    }

}
