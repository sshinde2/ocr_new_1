/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.util.EndecaQueryConnecton;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.FieldList;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.UrlENEQuery;


/**
 * @author rmcalave
 *
 */
public abstract class AbstractEndecaQueryBuilder {



    protected static final Logger LOG = Logger.getLogger(AbstractEndecaQueryBuilder.class);

    private EndecaQueryConnecton endecaQueryConnection;

    private ConfigurationService configurationService;

    /**
     * Return the a field list to fetch by looking up project.properties.
     * 
     * @return FieldList
     */
    protected FieldList getFieldList(final String fieldListConfig) {
        final FieldList selectedFieldsList = new FieldList();
        final String fieldList = configurationService.getConfiguration().getString(fieldListConfig);
        if (StringUtils.isNotEmpty(fieldList)) {
            final String[] fields = StringUtils.split(fieldList, "|");
            for (final String field : fields) {
                selectedFieldsList.addField(field);
            }
        }
        return selectedFieldsList;
    }

    /**
     * Building the record filter for products based on the options from EndecaSearchStateData
     * 
     * @param keys
     *            (keys can be productcodes or category codes)
     * @return String
     */
    protected String buildRecordFilter(final List<String> keys, final List<String> recFilterOptions) {
        final StringBuilder recordsQuery = new StringBuilder();
        int productCounter = 0;

        final int totalNoOfProducts = keys.size();
        recordsQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_KEY)
                .append(EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_START);

        for (final String code : keys) {
            if (code == null) {
                continue;
            }
            populateRecordFilter(recFilterOptions, recordsQuery, code);
            if (productCounter != (totalNoOfProducts - 1)) {
                recordsQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR);
            }
            productCounter++;
        }
        recordsQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_END);
        return recordsQuery.toString();
    }

    /**
     * 
     * @return queryResults
     * @throws TargetEndecaException
     * @throws ENEQueryException
     */
    protected ENEQueryResults fetchEndecaQueryResults(final UrlENEQuery query) throws TargetEndecaException,
            ENEQueryException {
        ENEQueryResults queryResults = null;
        final HttpENEConnection connecion;
        connecion = endecaQueryConnection.getConection();
        if (query != null) {
            queryResults = connecion.query(query);
        }

        return queryResults;
    }


    /**
     * 
     * @param recFilterOptions
     * @param recordsQuery
     * @param productCode
     * @return StringBuilder
     */
    private StringBuilder populateRecordFilter(final List<String> recFilterOptions, final StringBuilder recordsQuery,
            final String productCode) {
        int optionsCounter = 0;
        //loop through all the options and add it to the record filter string
        if (CollectionUtils.isNotEmpty(recFilterOptions)) {
            for (final String option : recFilterOptions) {
                recordsQuery.append(createSpecificRecordFilterFields(
                        option, productCode));
                if (optionsCounter < recFilterOptions.size() - 1) {
                    //if there are multiple options,add the comma seperator except for the last one
                    recordsQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR);

                }
                optionsCounter++;
            }
        }
        return recordsQuery;
    }

    private String createSpecificRecordFilterFields(final String fieldName,
            final String fieldValue) {
        return fieldName + ":" + fieldValue;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }



    /**
     * @param endecaQueryConnection
     *            the endecaQueryConnection to set
     */
    @Required
    public void setEndecaQueryConnection(final EndecaQueryConnecton endecaQueryConnection) {
        this.endecaQueryConnection = endecaQueryConnection;
    }
}
