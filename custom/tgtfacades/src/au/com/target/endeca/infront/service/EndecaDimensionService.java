/**
 * 
 */
package au.com.target.endeca.infront.service;

import java.util.Map;

import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.navigation.UrlENEQueryParseException;
import com.endeca.soleng.urlformatter.UrlFormatException;

import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;


/**
 * @author smudumba
 * 
 */
public interface EndecaDimensionService {

    /*Method returns all Dimensions from Endeca
     * 
     * @return EndecaDimensions
     */
    EndecaDimensions getDimensions() throws ENEQueryException, TargetEndecaException;

    /*Method to execute Dimension Query
    * 
    * @param query
    * @param conn
    * @return ENEQueryResults
    * @throws ENEQueryException
    * @throws UrlENEQueryParseException
    */
    ENEQueryResults excecuteDimensionQuery(final String query, final ENEConnection conn)
            throws ENEQueryException,
            UrlENEQueryParseException;

    /**
     * This method calls the assembler dimension search for getting the navigation state for corresponding category
     * 
     * @param dimensionIdMap
     * @return java.lang.String
     * @throws TargetEndecaWrapperException
     */
    public String getDimensionNavigationState(final Map<String, String> dimensionIdMap)
            throws TargetEndecaWrapperException;

    /**
     * It returns navigation state
     * 
     * @param dimLocationList
     * @return navigation state
     * @throws UrlFormatException
     */
    public String getEncodedNavigationState(final MutableDimLocationList dimLocationList) throws UrlFormatException;

}
