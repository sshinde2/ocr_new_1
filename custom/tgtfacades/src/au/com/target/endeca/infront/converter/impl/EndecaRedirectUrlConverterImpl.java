/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import au.com.target.endeca.infront.converter.EndecaContentItemConverter;
import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.UrlAction;


/**
 * @author rsamuel3
 * 
 */
public class EndecaRedirectUrlConverterImpl implements EndecaContentItemConverter<EndecaSearch> {

    /* (non-Javadoc)
     * @see au.com.target.endeca.infront.converter.EndecaContentItemConverter#convertContentItem(com.endeca.infront.assembler.ContentItem, java.lang.Object)
     */
    @Override
    public EndecaSearch convertContentItem(final ContentItem item, final EndecaSearch targetObject) {
        final BasicContentItem redirect = (BasicContentItem)item.get("endeca:redirect");
        if (redirect != null) {
            final UrlAction redirecturlAction = (UrlAction)redirect.get("link");
            targetObject.setRedirectLink(redirecturlAction.getUrl());
        }
        return targetObject;
    }

}
