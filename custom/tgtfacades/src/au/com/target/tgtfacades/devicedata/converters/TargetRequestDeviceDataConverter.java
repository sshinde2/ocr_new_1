/**
 * 
 */
package au.com.target.tgtfacades.devicedata.converters;

import de.hybris.platform.acceleratorfacades.device.data.DeviceData;
import de.hybris.platform.converters.impl.AbstractConverter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceResolver;


/**
 * @author asingh78
 * 
 */
public class TargetRequestDeviceDataConverter extends AbstractConverter<HttpServletRequest, DeviceData> {

    private DeviceResolver deviceResolver;


    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final HttpServletRequest source, final DeviceData target) {

        final Device device = deviceResolver.resolveDevice(source);
        target.setDesktopBrowser(Boolean.valueOf(device.isNormal()));
        target.setMobileBrowser(Boolean.valueOf(device.isMobile()));
        target.setTabletBrowser(Boolean.valueOf(device.isTablet()));

    }

    /**
     * @param deviceResolver
     *            the deviceResolver to set
     */
    @Required
    public void setDeviceResolver(final DeviceResolver deviceResolver) {
        this.deviceResolver = deviceResolver;
    }



}
