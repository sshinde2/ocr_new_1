/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.converters.OrderEntryModificationRecordEntryConverter;
import au.com.target.tgtfacades.order.converters.OrderEntryReturnRecordEntryConverter;


/**
 * Context for order refund emails
 * 
 */
public class OrderRefundEmailContext extends OrderModificationEmailContext {

    private OrderEntryReturnRecordEntryConverter orderEntryReturnRecordEntryConverter;

    @Override
    protected OrderModificationRecordEntryModel getModificationRecord(final BusinessProcessModel businessProcessModel) {
        return getOrderProcessParameterHelper().getReturnRequest((OrderProcessModel)businessProcessModel);
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.process.email.context.OrderModificationEmailContext#getOrderEntryModificationRecordEntryConverter()
     */
    @Override
    protected OrderEntryModificationRecordEntryConverter getOrderEntryModificationRecordEntryConverter() {
        return getOrderEntryReturnRecordEntryConverter();
    }

    /**
     * @return the orderEntryReturnRecordEntryConverter
     */
    public OrderEntryReturnRecordEntryConverter getOrderEntryReturnRecordEntryConverter() {
        return orderEntryReturnRecordEntryConverter;
    }

    /**
     * @param orderEntryReturnRecordEntryConverter
     *            the orderEntryReturnRecordEntryConverter to set
     */
    @Required
    public void setOrderEntryReturnRecordEntryConverter(
            final OrderEntryReturnRecordEntryConverter orderEntryReturnRecordEntryConverter) {
        this.orderEntryReturnRecordEntryConverter = orderEntryReturnRecordEntryConverter;
    }


}
