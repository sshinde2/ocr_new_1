package au.com.target.tgtfacades.process.email.context;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

public class OrderModificationDataHelper {


    private PriceDataFactory priceDataFactory;
    private OrderProcessParameterHelper orderProcessParameterHelper;
    private static final Logger LOG = Logger.getLogger(OrderModificationDataHelper.class);



    /**
     * Get the refund amount from the process and convert
     *
     * @param process
     * @param currency
     * @return priceData
     */
    public PriceData readRefundAmount(final OrderProcessModel process, final CurrencyModel currency) {
        Assert.notNull(currency, "currency must not be null");

        final BigDecimal processAmount = orderProcessParameterHelper.getRefundAmount(process);
        if (processAmount == null) {
            LOG.warn("Refund amount is null in process: " + process.getCode());
            return null;
        }

        return createPrice(currency, processAmount);
    }

    /**
     * Get the refunded delivery cost and convert
     *
     * @param orderCancelRecordEntry
     * @param currency
     * @return priceData
     */
    public PriceData readDeliveryRefundAmount(final OrderModificationRecordEntryModel orderCancelRecordEntry,
                                            final CurrencyModel currency) {
        Assert.notNull(currency, "currency must not be null");

        final Double dDeliveryRefundAmount = orderCancelRecordEntry.getRefundedShippingAmount();
        if (dDeliveryRefundAmount == null) {
            LOG.warn("Delivery Refund amount is null in order cancel record: " + orderCancelRecordEntry.getCode());
            return null;
        }

        return createPrice(currency, BigDecimal.valueOf(dDeliveryRefundAmount.doubleValue()));
    }


    /**
     * Return the currency for this order
     *
     * @param orderCancelRecordEntry
     * @return currency
     */
    protected CurrencyModel getCurrency(final OrderModificationRecordEntryModel orderCancelRecordEntry) {

        // Convert refund amount and delivery refund amount
        if (orderCancelRecordEntry.getModificationRecord() != null) {

            final OrderModel order = orderCancelRecordEntry.getModificationRecord().getOrder();

            return order.getCurrency();
        }

        return null;
    }

    /**
     *
     * @param currency
     * @param val
     * @return priceData
     */
    private PriceData createPrice(final CurrencyModel currency, final BigDecimal val)
    {
        Assert.notNull(currency, "currency must not be null");

        return priceDataFactory.create(PriceDataType.BUY, val, currency.getIsocode());
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}
