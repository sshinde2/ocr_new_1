/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;


/**
 * 
 * @author asingh78
 * 
 */
public class OrderLayByPaymentEmailContext extends OrderNotificationEmailContext {
    private PriceData paidAmount;
    private AbstractOrderHelper abstractOrderHelper;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.process.email.context.OrderNotificationEmailContext#init(de.hybris.platform.processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
     */
    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        if (businessProcessModel instanceof OrderProcessModel)
        {
            final OrderModel orderModel = ((OrderProcessModel)businessProcessModel).getOrder();
            final PaymentTransactionEntryModel paymentTransactionEntryModel = getOrderProcessParameterHelper()
                    .getPaymentTransactionEntry(
                            (OrderProcessModel)businessProcessModel);
            if (paymentTransactionEntryModel != null && paymentTransactionEntryModel.getAmount() != null) {
                paidAmount = abstractOrderHelper.createPrice(orderModel, paymentTransactionEntryModel.getAmount());
            }
        }
    }


    /**
     * @return the paidAmmount
     */
    public PriceData getPaidAmount() {
        return paidAmount;
    }


    /**
     * @param abstractOrderHelper
     *            the abstractOrderHelper to set
     */
    public void setAbstractOrderHelper(final AbstractOrderHelper abstractOrderHelper) {
        this.abstractOrderHelper = abstractOrderHelper;
    }


}
