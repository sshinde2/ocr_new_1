/**
 * 
 */
package au.com.target.tgtfacades.process.sms.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import au.com.target.tgtfacades.process.email.context.CncNotificationEmailContext;


/**
 * @author bhuang3
 * 
 */
public class CncNotificationSmsContext extends CncNotificationEmailContext {

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
    }
}
