/**
 * 
 */
package au.com.target.tgtfacades.sort;

import de.hybris.platform.commerceservices.search.pagedata.SortData;


/**
 * For SortData details
 * 
 * @author smudumba
 * 
 */
public class TargetSortData extends SortData {

    private String url;

    private String wsUrl;

    /**
     * Constructor
     */
    public TargetSortData() {
        super();
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the wsUrl
     */
    public String getWsUrl() {
        return wsUrl;
    }

    /**
     * @param wsUrl
     *            the wsUrl to set
     */
    public void setWsUrl(final String wsUrl) {
        this.wsUrl = wsUrl;
    }



}
