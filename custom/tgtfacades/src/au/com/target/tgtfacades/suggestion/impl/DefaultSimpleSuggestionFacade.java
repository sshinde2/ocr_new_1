/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfacades.suggestion.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPrimaryImagePopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.suggestion.SimpleSuggestionService;
import au.com.target.tgtfacades.suggestion.SimpleSuggestionFacade;


/**
 * Default implementation of {@link SimpleSuggestionFacade}.
 */
public class DefaultSimpleSuggestionFacade implements SimpleSuggestionFacade {

    private UserService userService;
    private CategoryService categoryService;
    private ProductPricePopulator<ProductModel, ProductData> productPricePopulator;
    private ProductPrimaryImagePopulator<ProductModel, ProductData> productPrimaryImagePopulator;
    private Converter<ProductModel, ProductData> productConverter;
    private SimpleSuggestionService simpleSuggestionService;

    @Override
    public List<ProductData> getReferencesForPurchasedInCategory(final String categoryCode,
            final ProductReferenceTypeEnum referenceType, final boolean excludePurchased, final Integer limit) {

        final UserModel user = userService.getCurrentUser();
        final CategoryModel category = categoryService.getCategoryForCode(categoryCode);

        final List<ProductModel> products = simpleSuggestionService.getReferencesForPurchasedInCategory(category,
                user,
                referenceType, excludePurchased, limit);

        final List<ProductData> result = new LinkedList<>();

        for (final ProductModel productModel : products) {
            final ProductData productData = productConverter.convert(productModel);

            productPricePopulator.populate(productModel, productData);
            productPrimaryImagePopulator.populate(productModel, productData);

            result.add(productData);
        }
        return result;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param productPrimaryImagePopulator
     *            the productPrimaryImagePopulator to set
     */
    @Required
    public void setProductPrimaryImagePopulator(
            final ProductPrimaryImagePopulator<ProductModel, ProductData> productPrimaryImagePopulator) {
        this.productPrimaryImagePopulator = productPrimaryImagePopulator;
    }

    /**
     * @param productPricePopulator
     *            the productPricePopulator to set
     */
    @Required
    public void setProductPricePopulator(final ProductPricePopulator<ProductModel, ProductData> productPricePopulator) {
        this.productPricePopulator = productPricePopulator;
    }

    /**
     * @param productConverter
     *            the productConverter to set
     */
    @Required
    public void setProductConverter(final Converter<ProductModel, ProductData> productConverter) {
        this.productConverter = productConverter;
    }

    /**
     * @param simpleSuggestionService
     *            the simpleSuggestionService to set
     */
    @Required
    public void setSimpleSuggestionService(final SimpleSuggestionService simpleSuggestionService) {
        this.simpleSuggestionService = simpleSuggestionService;
    }
}
