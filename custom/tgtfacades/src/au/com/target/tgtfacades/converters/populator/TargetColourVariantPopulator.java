/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * @author asingh78
 * 
 */
public class TargetColourVariantPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
        AbstractTargetVariantProductPopulator<SOURCE, TARGET> {


    /*
     * (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) throws ConversionException {
        if (!(target instanceof TargetProductData) || !(source instanceof TargetColourVariantProductModel)) {
            return;
        }

        final TargetProductData targetData = (TargetProductData)target;
        final TargetColourVariantProductModel sourceModel = (TargetColourVariantProductModel)source;

        targetData.setName(sourceModel.getDisplayName()); // override name for colour variant

        targetData.setColourVariant(true);
        targetData.setColourVariantCode(sourceModel.getCode());

        if (CollectionUtils.isEmpty(sourceModel.getVariants())) {
            targetData.setSellableVariantDisplayCode(sourceModel.getCode());
            targetData.setDisplayOnly(BooleanUtils.isTrue(sourceModel.getDisplayOnly()));
        }

        targetData.setColourName(sourceModel.getColourName());
        targetData.setSwatch(sourceModel.getSwatchName());
        targetData.setAssorted(sourceModel.isAssorted());

        targetData.setPromotionStatuses(getPromotionStatuses(sourceModel));
        targetData.setOnlineExclusive(sourceModel.getOnlineExclusive().booleanValue());
        targetData.setApn(sourceModel.getEan());

        final TargetProductModel baseProductModel = getProductFacade().getBaseTargetProduct(sourceModel);
        if (baseProductModel != null) {
            targetData.setBaseProductCode(baseProductModel.getCode());
            targetData.setShowStoreStockForProduct(BooleanUtils.isTrue(baseProductModel.getShowStoreStock()));
        }

        if (BooleanUtils.isTrue(sourceModel.getNewLowerPriceFlag())) {
            targetData.setNewLowerPriceStartDate(sourceModel.getNewLowerPriceStartDate());
        }
        targetData.setExcludeForAfterpay(sourceModel.isExcludeForAfterpay());
        targetData.setExcludeForZipPayment(sourceModel.isExcludeForZipPayment());
    }

    /**
     * Lookup for TargetProductFacade
     * 
     * @return the productFacade
     */
    public TargetProductFacade getProductFacade() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getProductFacade().");
    }



}
