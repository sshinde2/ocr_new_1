/**
 * 
 */
package au.com.target.tgtfacades.converters;


import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.data.ArticleItemData;
import au.com.target.tgtutility.util.MarkupUtils;
import au.com.target.tgtutility.util.YouTubeUrlUtils;
import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;



/**
 * Class convert ArticleItemComponentModel to ArticleItemData
 * 
 * @author asingh78
 */
public class ArticleItemConverter implements Converter<ArticleItemComponentModel, ArticleItemData> {

    @Override
    public ArticleItemData convert(final ArticleItemComponentModel source, final ArticleItemData prototype)
            throws ConversionException {

        final MediaModel media = source.getMedia();
        if (media != null) {
            prototype.setImageURL(media.getURL());
        }
        prototype.setExternal(source.isExternal());
        prototype.setHeadline(MarkupUtils.stripHtmlTagsFromText(source.getHeadline()));
        prototype.setContent(MarkupUtils.stripHtmlTagsFromText(source.getContent()));
        prototype.setYouTubeLink(BooleanUtils.toBoolean(source.getYouTubeLink()));


        if (null != source.getPage()) {
            prototype.setImageLink(source.getPage().getLabel());
            prototype.setContentPage(true);
        }
        else {
            if (source.getYouTubeLink().booleanValue()) {
                prototype.setImageLink(YouTubeUrlUtils.buildYoutubeEmbeddedUrl(source.getUrlLink()));
                if (StringUtils.isBlank(prototype.getImageLink())) {
                    prototype.setYouTubeLink(false);
                }
            }
            else {
                prototype.setImageLink(source.getUrlLink());
            }
            prototype.setContentPage(false);
        }
        return prototype;
    }



    @Override
    public ArticleItemData convert(final ArticleItemComponentModel source) throws ConversionException {
        return convert(source, new ArticleItemData());
    }

}
