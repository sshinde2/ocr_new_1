/**
 * 
 */
package au.com.target.tgtfacades.user;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;

import java.util.List;



/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface TargetUserFacade extends UserFacade {
    /**
     * Check if the address id is the default one
     * 
     * @param addressId
     * @return true if the address is the default one, return if the user id is null or if the address doesn't exist
     */
    boolean isAddressDefault(String addressId);


    /**
     * Check if current user is Anonymous
     * 
     * @return true if user is an Anonymous
     */
    boolean isCurrentUserAnonymous();

    /**
     * Check if current user has saved valid IPG credit cards (expired date after current date)
     * 
     * @return true if user has
     */
    boolean hasSavedValidIpgCreditCards();

    /**
     * get the saved ipg credit card payment infos within valid time.
     * 
     * @return List<CCPaymentInfoData>
     */
    List<CCPaymentInfoData> getSavedCreditCardPaymentInfosInValidTime();

    /**
     * get all user segments separated by a configured separator
     * 
     * @return String representation of all segments
     */
    List<String> getAllUserSegmentsForCurrentUser();
}
