/**
 * 
 */
package au.com.target.tgtfacades.user.data;



/**
 * ContactDetails data
 * 
 * @author jjayawa1
 *
 */
public class ContactDetailsData {

    private String firstName;
    private String lastName;
    private String formattedTitle;
    private String title;
    private String mobileNumber;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the formattedTitle
     */
    public String getFormattedTitle() {
        return formattedTitle;
    }

    /**
     * @param formattedTitle
     *            the formattedTitle to set
     */
    public void setFormattedTitle(final String formattedTitle) {
        this.formattedTitle = formattedTitle;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber
     *            the mobileNumber to set
     */
    public void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
