package au.com.target.tgtfacades.data;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;


public class ArticleItemData extends AbstractBannerData implements Serializable {
    private String headline;
    private String content;
    private boolean youTubeLink;

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(final String headline) {
        this.headline = headline;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public boolean isHeadlineAvailable() {
        return StringUtils.isNotEmpty(headline);
    }

    public boolean isContentAvailable() {
        return StringUtils.isNotEmpty(content);
    }

    /**
     * @return the youTubeLink
     */
    public boolean isYouTubeLink() {
        return youTubeLink;
    }

    /**
     * @param youTubeLink
     *            the youTubeLink to set
     */
    public void setYouTubeLink(final boolean youTubeLink) {
        this.youTubeLink = youTubeLink;
    }

}
