/**
 * 
 */
package au.com.target.tgtfacades.flybuys.data;

/**
 * @author vivek
 * 
 */
public class FlybuysRedeemConfigData {

    private Double minRedeemable;
    private Double maxRedeemable;
    private Double minCartValue;
    private Integer minPointsBalance;
    private Boolean active;

    /**
     * @return the minRedeemable
     */
    public Double getMinRedeemable() {
        return minRedeemable;
    }

    /**
     * @param minRedeemable
     *            the minRedeemable to set
     */
    public void setMinRedeemable(final Double minRedeemable) {
        this.minRedeemable = minRedeemable;
    }

    /**
     * @return the maxRedeemable
     */
    public Double getMaxRedeemable() {
        return maxRedeemable;
    }

    /**
     * @param maxRedeemable
     *            the maxRedeemable to set
     */
    public void setMaxRedeemable(final Double maxRedeemable) {
        this.maxRedeemable = maxRedeemable;
    }

    /**
     * @return the minCartValue
     */
    public Double getMinCartValue() {
        return minCartValue;
    }

    /**
     * @param minCartValue
     *            the minCartValue to set
     */
    public void setMinCartValue(final Double minCartValue) {
        this.minCartValue = minCartValue;
    }

    /**
     * @return the minPointsBalance
     */
    public Integer getMinPointsBalance() {
        return minPointsBalance;
    }

    /**
     * @param minPointsBalance
     *            the minPointsBalance to set
     */
    public void setMinPointsBalance(final Integer minPointsBalance) {
        this.minPointsBalance = minPointsBalance;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(final Boolean active) {
        this.active = active;
    }

}
