/**
 * 
 */
package au.com.target.tgtfacades.flybuys;

import java.util.Date;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemTierData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;



/**
 * @author rmcalave
 * 
 */
public interface FlybuysDiscountFacade {

    /**
     * Retrieve the flybuys Redemption data from session that is applicable for the checkout cart
     * 
     * @return The flybuys redemption data
     */
    FlybuysRedemptionData getFlybuysRedemptionDataForCheckoutCart();

    /**
     * Retrieve the flybuys redeem options, store in session and return error/success info
     * 
     * @param flybuysCardNumber
     *            flybuys card number
     * @param dateOfBirth
     *            date of birth
     * @param postcode
     *            postcode
     * @return status info
     */
    FlybuysRequestStatusData getFlybuysRedemptionData(String flybuysCardNumber, Date dateOfBirth, String postcode);


    /**
     * Gets the flybuys redeem config data.
     * 
     * @return the flybuys redeem config data
     */
    FlybuysRedeemConfigData getFlybuysRedeemConfigData();

    /**
     * Gets the flybuys discount allowed.
     * 
     * @return the flybuys discount allowed
     */
    FlybuysDiscountDenialDto getFlybuysDiscountAllowed();

    /**
     * Remove any existing flybuys discount from the checkout cart, then apply the discount associated with the provided
     * redeem code, if one exists in the session.
     * 
     * @param redeemCode
     *            the redeem code
     * @return true if the discount was successfully applied, false otherwise
     */
    boolean applyFlybuysDiscountToCheckoutCart(final String redeemCode);

    /**
     * Remove any existing flybuys discount from the checkout cart
     */
    boolean removeFlybuysDiscountFromCheckoutCart();

    /**
     * Retrieves details of the flybuys redeem tier that is currently applied to the cart.
     * 
     * @return the flybuys redeem tier if one is applied, null otherwise
     */
    FlybuysRedeemTierData getAppliedFlybuysRedeemTierForCheckoutCart();

    /**
     * Reassess the validity of any flybuys Discount applied to the checkout cart if there is one.
     * 
     * Should return false in all cases where the discount is not modified, including if there isn't one to begin with.
     * 
     * @return true if the discount was removed or modified, false otherwise
     */
    boolean reassessFlybuysDiscountOnCheckoutCart();

    /**
     * check if the flybuys discount is existed in the cart model.
     * 
     * @return true, if successful
     */
    boolean checkFlybuysDiscountUsed();
}