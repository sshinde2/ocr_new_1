/**
 * 
 */
package au.com.target.tgtfacades.order.data.placeorder;

/**
 * @author hsing179
 *
 */
public class TargetPlaceOrderZipPaymentInfoData implements TargetPlaceOrderPaymentInfoData {

    private String checkoutId;
    private String result;

    /**
     *
     * @return
     */
    public String getCheckoutId() {
        return checkoutId;
    }

    /**
     *
     * @param checkoutId
     */
    public void setCheckoutId(final String checkoutId) {
        this.checkoutId = checkoutId;
    }

    /**
     *
     * @return
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     */
    public void setResult(final String result) {
        this.result = result;
    }

}
