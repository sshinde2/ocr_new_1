/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.PaypalInfoData;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;


/**
 * @author asingh78
 * 
 */
public class TargetPayPalPaymentInfoConverter extends
        AbstractPopulatingConverter<PaypalPaymentInfoModel, TargetCCPaymentInfoData> {

    private Converter<AddressModel, AddressData> addressConverter;


    @Override
    public void populate(final PaypalPaymentInfoModel source, final TargetCCPaymentInfoData target)
    {
        target.setId(source.getPk().toString());
        target.setCardNumber(source.getCode());
        target.setPaypalPaymentInfo(true);
        final PaypalInfoData paypalInfoData = new PaypalInfoData();
        if (null != source.getEmailId()) {

            paypalInfoData.setPaypalEmailId(source.getEmailId());

        }
        target.setPaypalInfoData(paypalInfoData);

        if (source.getBillingAddress() != null)
        {
            target.setBillingAddress(addressConverter.convert(source.getBillingAddress()));
        }

        super.populate(source, target);
    }

    /**
     * @param addressConverter
     *            the addressConverter to set
     */
    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }



}
