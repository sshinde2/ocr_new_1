/**
 * 
 */
package au.com.target.tgtfacades.order.data.placeorder;

/**
 * @author rmcalave
 *
 */
public class TargetPlaceOrderIPGPaymentInfoData implements TargetPlaceOrderPaymentInfoData {

    private String ipgToken;

    private String sessionId;

    /**
     * @return the ipgToken
     */
    public String getIpgToken() {
        return ipgToken;
    }

    /**
     * @param ipgToken
     *            the ipgToken to set
     */
    public void setIpgToken(final String ipgToken) {
        this.ipgToken = ipgToken;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

}
