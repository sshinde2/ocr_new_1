/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;


/**
 * Populate IPG Payment Info
 * 
 * @author htan3
 *
 */
public class TargetIpgPaymentInfoPopulator implements
        Populator<IpgPaymentInfoModel, TargetCCPaymentInfoData> {

    private Converter<AddressModel, TargetAddressData> addressConverter;

    @Override
    public void populate(final IpgPaymentInfoModel source, final TargetCCPaymentInfoData target) {

        target.setId(source.getPk().toString());
        target.setIpgPaymentInfo(true);
        final IpgPaymentInfoData ipgData = new IpgPaymentInfoData();
        ipgData.setToken(source.getToken());
        ipgData.setIpgPaymentTemplateType(source.getIpgPaymentTemplateType());
        target.setIpgPaymentInfoData(ipgData);
        if (source.getBillingAddress() != null) {
            target.setBillingAddress(addressConverter.convert(source.getBillingAddress()));
        }
    }

    /**
     * @param addressConverter
     *            the addressConverter to set
     */
    @Required
    public void setAddressConverter(final Converter<AddressModel, TargetAddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

}
