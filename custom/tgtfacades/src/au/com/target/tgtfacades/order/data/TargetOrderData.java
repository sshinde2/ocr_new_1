/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtfacades.affiliate.data.AffiliateOrderData;


/**
 * @author Benoit VanalderWeireldt
 * 
 */
public class TargetOrderData extends OrderData implements TargetOrderDataInterface {
    private String email;

    private Integer cncStoreNumber;

    private String tmdNumber;

    private String flybuysNumber;

    private String maskedFlybuysNumber;

    private boolean isLayBy;

    private List<LayByDueDateData> dueDates;

    private String fullPaymentDueDate;

    private PriceData layByFee;

    private PriceData initialLayByPaymentAmount;

    private String purchaseOptionCode;

    private PriceData layByAmountPaid;

    private PriceData outstandingAmount;

    private PriceData amountDueToday;

    private String barcodePngUrl;

    private String barcodeSvgUrl;

    private String barcodeNumber;

    private String receiptNumber;

    private TransactionStatus transactionStatus;

    private List<OrderModificationData> cancelledProducts;

    private List<OrderModificationData> returnAndRefundProducts;

    private List<ConsignmentData> consignment;

    private List<LaybyTransactionData> laybyTransactions;

    private FlybuysDiscountData flybuysDiscountData;

    private Integer eBayOrderNumber;

    private boolean isEbayOrder;

    private boolean isTradeMeOrder;

    private boolean multipleConsignments;

    private List<String> appliedVoucherCodes;

    private AffiliateOrderData affiliateOrderData;

    private boolean hasDigitalProducts;

    private boolean hasPhysicalGiftCards;

    private List<CCPaymentInfoData> paymentsInfo;

	private boolean containsPreOrderItems;

    private PriceData preOrderDepositAmount;

    private PriceData preOrderOutstandingAmount;

    private List<CCPaymentInfoData> outstandingAmountPaymentsInfo;

    private boolean deliveryModeStoreDelivery;

    private String cncCustomOrderStatus;

    private boolean cncOrderWithConsignmentPickedUpDate;

    private boolean cncOrderReadyForPickup;

    private boolean cncOrderRefunded;

    public boolean isFullCancel() {
        return OrderStatus.CANCELLED.equals(this.getStatus());
    }

    /**
     * @return the cncStoreNumber
     */
    @Override
    public Integer getCncStoreNumber() {
        return cncStoreNumber;
    }

    /**
     * @param cncStoreNumber
     *            the cncStoreNumber to set
     */
    public void setCncStoreNumber(final Integer cncStoreNumber) {
        this.cncStoreNumber = cncStoreNumber;
    }

    /**
     * @return the tmdNumber
     */
    @Override
    public String getTmdNumber() {
        return tmdNumber;
    }

    /**
     * @param tmdNumber
     *            the tmdNumber to set
     */
    public void setTmdNumber(final String tmdNumber) {
        this.tmdNumber = tmdNumber;
    }

    /**
     * @return the isLayBy
     */
    @Override
    public boolean isLayBy() {
        return isLayBy;
    }

    /**
     * @param layBy
     *            the isLayBy to set
     */
    public void setLayBy(final boolean layBy) {
        this.isLayBy = layBy;
    }

    /**
     * @return the dueDates
     */
    @Override
    public List<LayByDueDateData> getDueDates() {
        return dueDates;
    }

    /**
     * @param dueDates
     *            the dueDates to set
     */
    public void setDueDates(final List<LayByDueDateData> dueDates) {
        this.dueDates = dueDates;
    }

    /**
     * @return the fullPaymentDueDate
     */
    @Override
    public String getFullPaymentDueDate() {
        return fullPaymentDueDate;
    }

    /**
     * @param fullPaymentDueDate
     *            the fullPaymentDueDate to set
     */
    public void setFullPaymentDueDate(final String fullPaymentDueDate) {
        this.fullPaymentDueDate = fullPaymentDueDate;
    }

    /**
     * @return the layByFee
     */
    @Override
    public PriceData getLayByFee() {
        return layByFee;
    }

    /**
     * @param layByFee
     *            the layByFee to set
     */
    public void setLayByFee(final PriceData layByFee) {
        this.layByFee = layByFee;
    }

    /**
     * @return the initialLayByPaymentAmount
     */
    @Override
    public PriceData getInitialLayByPaymentAmount() {
        return initialLayByPaymentAmount;
    }

    /**
     * @param initialLayByPaymentAmount
     *            the initialLayByPaymentAmount to set
     */
    public void setInitialLayByPaymentAmount(final PriceData initialLayByPaymentAmount) {
        this.initialLayByPaymentAmount = initialLayByPaymentAmount;
    }

    /**
     * @return the purchaseOptionCode
     */
    @Override
    public String getPurchaseOptionCode() {
        return purchaseOptionCode;
    }

    /**
     * @param purchaseOptionCode
     *            the purchaseOptionCode to set
     */
    public void setPurchaseOptionCode(final String purchaseOptionCode) {
        this.purchaseOptionCode = purchaseOptionCode;
    }

    /**
     * @return the barcodeNumber
     */
    public String getBarcodeNumber() {
        return barcodeNumber;
    }

    /**
     * @param barcodeNumber
     *            the barcodeNumber to set
     */
    public void setBarcodeNumber(final String barcodeNumber) {
        this.barcodeNumber = barcodeNumber;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the transactionStatus
     */
    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * @param transactionStatus
     *            the transactionStatus to set
     */
    public void setTransactionStatus(final TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    /**
     * @return the outstandingAmount
     */
    public PriceData getOutstandingAmount() {
        return outstandingAmount;
    }

    /**
     * @param outstandingAmount
     *            the outstandingAmount to set
     */
    public void setOutstandingAmount(final PriceData outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    /**
     * @return the layByAmountPaid
     */
    public PriceData getLayByAmountPaid() {
        return layByAmountPaid;
    }

    /**
     * @param layByAmountPaid
     *            the layByAmountPaid to set
     */
    public void setLayByAmountPaid(final PriceData layByAmountPaid) {
        this.layByAmountPaid = layByAmountPaid;
    }

    /**
     * @return the cancelledProducts
     */
    public List<OrderModificationData> getCancelledProducts() {
        return cancelledProducts;
    }

    /**
     * @param cancelledProducts
     *            the cancelledProducts to set
     */
    public void setCancelledProducts(final List<OrderModificationData> cancelledProducts) {
        this.cancelledProducts = cancelledProducts;
    }

    /**
     * @return the returnAndRefundProducts
     */
    public List<OrderModificationData> getReturnAndRefundProducts() {
        return returnAndRefundProducts;
    }

    /**
     * @param returnAndRefundProducts
     *            the returnAndRefundProducts to set
     */
    public void setReturnAndRefundProducts(final List<OrderModificationData> returnAndRefundProducts) {
        this.returnAndRefundProducts = returnAndRefundProducts;
    }

    /**
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the consignment
     */
    public List<ConsignmentData> getConsignment() {
        return consignment;
    }

    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final List<ConsignmentData> consignment) {
        this.consignment = consignment;
    }


    /**
     * @return the amountDueToday
     */
    public PriceData getAmountDueToday() {
        return amountDueToday;
    }

    /**
     * @param amountDueToday
     *            the amountDueToday to set
     */
    public void setAmountDueToday(final PriceData amountDueToday) {
        this.amountDueToday = amountDueToday;
    }

    /**
     * @return the laybyTransactions
     */
    public List<LaybyTransactionData> getLaybyTransactions() {
        return laybyTransactions;
    }

    /**
     * @param laybyTransactions
     *            the laybyTransactions to set
     */
    public void setLaybyTransactions(final List<LaybyTransactionData> laybyTransactions) {
        this.laybyTransactions = laybyTransactions;
    }

    /**
     * @return the flybuysNumber
     */
    public String getFlybuysNumber() {
        return flybuysNumber;
    }

    /**
     * @param flybuysNumber
     *            the flybuysNumber to set
     */
    public void setFlybuysNumber(final String flybuysNumber) {
        this.flybuysNumber = flybuysNumber;
    }

    /**
     * @return the maskedFlybuysNumber
     */
    public String getMaskedFlybuysNumber() {
        return maskedFlybuysNumber;
    }

    /**
     * @param maskedFlybuysNumber
     *            the maskedFlybuysNumber to set
     */
    public void setMaskedFlybuysNumber(final String maskedFlybuysNumber) {
        this.maskedFlybuysNumber = maskedFlybuysNumber;
    }

    /**
     * @return the barcodePngUrl
     */
    public String getBarcodePngUrl() {
        return barcodePngUrl;
    }

    /**
     * @param barcodePngUrl
     *            the barcodePngUrl to set
     */
    public void setBarcodePngUrl(final String barcodePngUrl) {
        this.barcodePngUrl = barcodePngUrl;
    }

    /**
     * @return the barcodeSvgUrl
     */
    public String getBarcodeSvgUrl() {
        return barcodeSvgUrl;
    }

    /**
     * @param barcodeSvgUrl
     *            the barcodeSvgUrl to set
     */
    public void setBarcodeSvgUrl(final String barcodeSvgUrl) {
        this.barcodeSvgUrl = barcodeSvgUrl;
    }

    /**
     * @return the flybuysDiscountData
     */
    @Override
    public FlybuysDiscountData getFlybuysDiscountData() {

        return flybuysDiscountData;
    }

    /**
     * @param flybuysDiscountData
     *            the flybuysDiscountData to set
     */
    @Override
    public void setFlybuysDiscountData(final FlybuysDiscountData flybuysDiscountData) {
        this.flybuysDiscountData = flybuysDiscountData;
    }

    /**
     * @return the isEbayOrder
     */
    public boolean isEbayOrder() {
        return isEbayOrder;
    }

    /**
     * @param ebayOrder
     *            the isEbayOrder to set
     */
    public void setEbayOrder(final boolean ebayOrder) {
        this.isEbayOrder = ebayOrder;
    }

    /**
     * @return the appliedVoucherCode
     */
    public List<String> getAppliedVoucherCodes() {
        return appliedVoucherCodes;
    }

    /**
     * @param appliedVoucherCodes
     *            the appliedVoucherCodes to set
     */
    public void setAppliedVoucherCodes(final List<String> appliedVoucherCodes) {
        this.appliedVoucherCodes = appliedVoucherCodes;
    }

    /**
     * @return the affiliateOrderData
     */
    public AffiliateOrderData getAffiliateOrderData() {
        return affiliateOrderData;
    }

    /**
     * @param affiliateOrderData
     *            the affiliateOrderData to set
     */
    public void setAffiliateOrderData(final AffiliateOrderData affiliateOrderData) {
        this.affiliateOrderData = affiliateOrderData;
    }

    /**
     * @return the hasDigitalProducts
     */
    @Override
    public boolean getHasDigitalProducts() {
        return hasDigitalProducts;
    }

    /**
     * @param hasDigitalProducts
     *            the hasDigitalProducts to set
     */
    public void setHasDigitalProducts(final boolean hasDigitalProducts) {
        this.hasDigitalProducts = hasDigitalProducts;
    }

    /**
     * @return the paymentsInfo
     */
    public List<CCPaymentInfoData> getPaymentsInfo() {
        return paymentsInfo;
    }

    /**
     * Ass a payment info to list of payments
     * 
     * @param paymentInfo
     *            the payment info to be added
     */
    public void addPaymentInfo(final CCPaymentInfoData paymentInfo) {
        if (paymentsInfo == null) {
            paymentsInfo = new ArrayList<>();
        }
        if (paymentInfo != null) {
            paymentsInfo.add(paymentInfo);
        }
    }

    /**
     * @return the isTradeMeOrder
     */
    public boolean isTradeMeOrder() {
        return isTradeMeOrder;
    }

    /**
     * @param tradeMeOrder
     *            the isTradeMeOrder to set
     */
    public void setTradeMeOrder(final boolean tradeMeOrder) {
        this.isTradeMeOrder = tradeMeOrder;
    }

    /**
     * @return the eBayOrderNumber
     */
    public Integer geteBayOrderNumber() {
        return eBayOrderNumber;
    }

    /**
     * @param eBayOrderNumber
     *            the eBayOrderNumber to set
     */
    public void seteBayOrderNumber(final Integer eBayOrderNumber) {
        this.eBayOrderNumber = eBayOrderNumber;
    }

    /**
     * @return the hasPhysicalGiftCards
     */
    public boolean isHasPhysicalGiftCards() {
        return hasPhysicalGiftCards;
    }

    /**
     * @param hasPhysicalGiftCards
     *            the hasPhysicalGiftCards to set
     */
    public void setHasPhysicalGiftCards(final boolean hasPhysicalGiftCards) {
        this.hasPhysicalGiftCards = hasPhysicalGiftCards;
    }

	/**
	 * @return the containsPreOrderItems
	 */
	public boolean isContainsPreOrderItems() {
		return containsPreOrderItems;
	}

	/**
	 * @param containsPreOrderItems
	 *            the containsPreOrderItems to set
	 */
    public void setContainsPreOrderItems(boolean containsPreOrderItems) {
		this.containsPreOrderItems = containsPreOrderItems;
	}


    /**
     * @return the preOrderDepositAmount
     */
    public PriceData getPreOrderDepositAmount() {
        return preOrderDepositAmount;
    }

    /**
     * @param preOrderDepositAmount
     *            the preOrderDepositAmount to set
     */
    public void setPreOrderDepositAmount(final PriceData preOrderDepositAmount) {
        this.preOrderDepositAmount = preOrderDepositAmount;
    }

    /**
     * @return the preOrderOutstandingAmount
     */
    public PriceData getPreOrderOutstandingAmount() {
        return preOrderOutstandingAmount;
    }

    /**
     * @param preOrderOutstandingAmount
     *            the preOrderOutstandingAmount to set
     */
    public void setPreOrderOutstandingAmount(final PriceData preOrderOutstandingAmount) {
        this.preOrderOutstandingAmount = preOrderOutstandingAmount;
    }

    /**
     * @return the remainingBalancePayment
     */
    public List<CCPaymentInfoData> getOutstandingAmountPaymentsInfo() {
        return outstandingAmountPaymentsInfo;
    }

    public void addOutstandingAmountPaymentInfo(final CCPaymentInfoData outstandingAmountPaymentInfo) {
        if (outstandingAmountPaymentsInfo == null) {
            outstandingAmountPaymentsInfo = new ArrayList<>();
        }
        if (outstandingAmountPaymentInfo != null) {
            outstandingAmountPaymentsInfo.add(outstandingAmountPaymentInfo);
        }
    }

    /**
     * @return the multipleConsignments
     */
    public boolean isMultipleConsignments() {
        return multipleConsignments;
    }

    /**
     * @param multipleConsignments
     *            the multipleConsignments to set
     */
    public void setMultipleConsignments(final boolean multipleConsignments) {
        this.multipleConsignments = multipleConsignments;
    }

    /**
     * @return the deliveryModeStoreDelivery
     */
    public boolean isDeliveryModeStoreDelivery() {
        return deliveryModeStoreDelivery;
    }

    /**
     * @param deliveryModeStoreDelivery
     *            the deliveryModeStoreDelivery to set
     */
    public void setDeliveryModeStoreDelivery(final boolean deliveryModeStoreDelivery) {
        this.deliveryModeStoreDelivery = deliveryModeStoreDelivery;
    }

    /**
     * @return the cncCustomOrderStatus
     */
    public String getCncCustomOrderStatus() {
        return cncCustomOrderStatus;
    }

    /**
     * @param cncCustomOrderStatus
     *            the cncCustomOrderStatus to set
     */
    public void setCncCustomOrderStatus(final String cncCustomOrderStatus) {
        this.cncCustomOrderStatus = cncCustomOrderStatus;
    }

    /**
     * @return the cncOrderWithConsignmentPickedUpDate
     */
    public boolean isCncOrderWithConsignmentPickedUpDate() {
        return cncOrderWithConsignmentPickedUpDate;
    }

    /**
     * @return the cncOrderReadyForPickup
     */
    public boolean isCncOrderReadyForPickup() {
        return cncOrderReadyForPickup;
    }

    /**
     * @param cncOrderWithConsignmentPickedUpDate
     *            the cncOrderWithConsignmentPickedUpDate to set
     */
    public void setCncOrderWithConsignmentPickedUpDate(final boolean cncOrderWithConsignmentPickedUpDate) {
        this.cncOrderWithConsignmentPickedUpDate = cncOrderWithConsignmentPickedUpDate;
    }

    /**
     * @param cncOrderReadyForPickup
     *            the cncOrderReadyForPickup to set
     */
    public void setCncOrderReadyForPickup(final boolean cncOrderReadyForPickup) {
        this.cncOrderReadyForPickup = cncOrderReadyForPickup;
    }

    /**
     * @return the cncOrderRefunded
     */
    public boolean isCncOrderRefunded() {
        return cncOrderRefunded;
    }

    /**
     * @param cncOrderRefunded
     *            the cncOrderRefunded to set
     */
    public void setCncOrderRefunded(final boolean cncOrderRefunded) {
        this.cncOrderRefunded = cncOrderRefunded;
    }

}