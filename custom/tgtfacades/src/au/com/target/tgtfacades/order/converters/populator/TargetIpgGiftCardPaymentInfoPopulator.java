/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;


/**
 * Populate IPG gift card information
 */
public class TargetIpgGiftCardPaymentInfoPopulator implements
        Populator<IpgGiftCardPaymentInfoModel, TargetCCPaymentInfoData> {

    private Converter<CreditCardType, CardTypeData> cardTypeConverter;

    @Override
    public void populate(final IpgGiftCardPaymentInfoModel source, final TargetCCPaymentInfoData target)
            throws ConversionException {

        target.setId(source.getPk().toString());
        target.setCardNumber(source.getMaskedNumber());

        if (source.getCardType() != null) {
            final CardTypeData cardTypeData = cardTypeConverter.convert(source.getCardType());
            target.setCardType(cardTypeData.getCode());
            target.setCardTypeData(cardTypeData);
        }
    }

    /**
     * @param cardTypeConverter
     *            the cardTypeConverter to set
     */
    @Required
    public void setCardTypeConverter(final Converter<CreditCardType, CardTypeData> cardTypeConverter) {
        this.cardTypeConverter = cardTypeConverter;
    }
}