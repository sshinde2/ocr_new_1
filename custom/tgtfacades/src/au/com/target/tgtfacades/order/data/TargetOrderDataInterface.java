/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.product.data.PriceData;

import java.util.List;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface TargetOrderDataInterface {

    /**
     * 
     * @return Integer
     */
    Integer getCncStoreNumber();

    /**
     * 
     * @return String
     */
    String getTmdNumber();

    /**
     * 
     * @return boolean
     */
    boolean isLayBy();

    /**
     * 
     * @return List<LayByDueDateData>
     */
    List<LayByDueDateData> getDueDates();

    /**
     * 
     * @return String
     */
    String getFullPaymentDueDate();

    /**
     * 
     * @return PriceData
     */
    PriceData getLayByFee();

    /**
     * 
     * @return PriceData
     */
    PriceData getInitialLayByPaymentAmount();

    /**
     * 
     * @return String
     */
    String getPurchaseOptionCode();

    /**
     * 
     * @return FlybuysDiscountData
     */
    FlybuysDiscountData getFlybuysDiscountData();

    /**
     * @param flybuysDiscountData
     * 
     */
    void setFlybuysDiscountData(FlybuysDiscountData flybuysDiscountData);

    /**
     * 
     * @return boolean
     */
    boolean getHasDigitalProducts();

}
