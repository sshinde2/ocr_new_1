/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.product.data.PriceData;



/**
 * @author rmcalave
 * 
 */
public class DeliveryModeData {

    private String deliveryModeCode;

    private PriceData deliveryFee;

    private String message;

    private String disclaimer;

    private boolean deliverToStores;

    private boolean available;

    private int displayOrder;

    /**
     * @return the deliveryModeCode
     */
    public String getDeliveryModeCode() {
        return deliveryModeCode;
    }

    /**
     * @param deliveryModeCode
     *            the deliveryModeCode to set
     */
    public void setDeliveryModeCode(final String deliveryModeCode) {
        this.deliveryModeCode = deliveryModeCode;
    }

    /**
     * @return the deliveryFee
     */
    public PriceData getDeliveryFee() {
        return deliveryFee;
    }

    /**
     * @param deliveryFee
     *            the deliveryFee to set
     */
    public void setDeliveryFee(final PriceData deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the disclaimer
     */
    public String getDisclaimer() {
        return disclaimer;
    }

    /**
     * @param disclaimer
     *            the disclaimer to set
     */
    public void setDisclaimer(final String disclaimer) {
        this.disclaimer = disclaimer;
    }

    /**
     * @return the deliverToStores
     */
    public boolean isDeliverToStores() {
        return deliverToStores;
    }

    /**
     * @param deliverToStores
     *            the deliverToStores to set
     */
    public void setDeliverToStores(final boolean deliverToStores) {
        this.deliverToStores = deliverToStores;
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final boolean available) {
        this.available = available;
    }

    /**
     * @return the displayOrder
     */
    public int getDisplayOrder() {
        return displayOrder;
    }

    /**
     * @param displayOrder
     *            the displayOrder to set
     */
    public void setDisplayOrder(final int displayOrder) {
        this.displayOrder = displayOrder;
    }
}
