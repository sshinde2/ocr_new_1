/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.formatter.InvoiceFormatterHelper;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.converters.TargetFlybuysDiscountConverter;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author rmcalave
 *
 */
public class TargetCartPopulator extends CartPopulator<TargetCartData> {

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    private TargetLaybyPaymentDueService targetLaybyPaymentDueService;

    private AbstractOrderHelper abstractOrderHelper;

    private TargetCheckoutCustomerStrategy targetCheckoutCustomerStrategy;

    private Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> targetPinPadPaymentInfoConverter;

    private Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> targetIpgPaymentInfoConverter;

    private Converter<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> targetZoneDeliveryModeConverter;

    private TargetDeliveryService targetDeliveryService;

    private TargetFlybuysDiscountConverter targetFlybuysDiscountConverter;

    private TargetSalesApplicationService targetSalesApplicationService;

    private InvoiceFormatterHelper invoiceFormatterHelper;

    private TargetPriceHelper targetPriceHelper;

    @Override
    public void populate(final CartModel source, final TargetCartData target) {

        final DeliveryModeModel deliveryModeModel = source.getDeliveryMode();
        if (deliveryModeModel instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetDeliveryModeModel = (TargetZoneDeliveryModeModel)deliveryModeModel;
            final TargetZoneDeliveryModeData deliveryMode = targetZoneDeliveryModeConverter
                    .convert(targetDeliveryModeModel);
            target.setDeliveryMode(deliveryMode);
            target.setAllowBillingSameAsDelivery(
                    BooleanUtils.isNotTrue(targetDeliveryModeModel.getIsDeliveryToStore()));
            final boolean hasDigitalProductInCart = hasAnyDigitalProductInCart(source);
            //For Basket has no digital gift product
            if (!hasDigitalProductInCart
                    && TgtFacadesConstants.DIGITAL_GIFT_CARD_DELIVERY.equals(deliveryMode.getCode())) {
                target.setDeliveryMode(null);
                target.setDeliveryCost(null);
            }
        }
        target.setCncStoreNumber(source.getCncStoreNumber());

        populateAllDeliveryModes(source, target);

        final PurchaseOptionModel purchaseOption = source.getPurchaseOption();
        if (purchaseOption != null) {
            target.setPurchaseOptionCode(purchaseOption.getCode());
        }

        // For layby purchase options we need to check if the target subtotal is sufficient to proceed
        // (without the effect of TMD)
        if (purchaseOption != null) {
            target.setInsufficientAmount(isInsufficientAmount(purchaseOption, target.getSubTotal().getValue()));
        }

        final PurchaseOptionConfigModel purchaseOptionConfigModel = source.getPurchaseOptionConfig();

        setLayByInfo(source, target);
        if (null != purchaseOptionConfigModel) {
            target.setAllowGuestCheckout(purchaseOptionConfigModel.getAllowGuestCheckout().booleanValue());
        }
        target.setTmdNumber(source.getTmdCardNumber());
        target.setFlybuysNumber(source.getFlyBuysCode());
        target.setMaskedFlybuysNumber(invoiceFormatterHelper.getMaskedFlybuysNumber(source.getFlyBuysCode()));

        setUserInfo(source, target);
        targetFlybuysDiscountConverter.convertFlybuysDiscount(source, target);

        updateCartEntriesWithPromotionInformation(target);

        if (BooleanUtils.isTrue(source.getContainsPreOrderItems())) {
            setPreOrderInfo(source, target);
            target.setExcludeForAfterpay(true);
            target.setExcludeForZipPayment(true);
        }

        target.setBillingAddress(getBillingAddress(source));
    }

    /**
     * @param source
     * @param target
     */
    private void setPreOrderInfo(final CartModel source, final TargetCartData target) {
        final Double totalPrice = source.getTotalPrice();
        final Double preOrderDeposit = source.getPreOrderDepositAmount();
        final Double preOrderOutstandingAmount = Double
                .valueOf(totalPrice.doubleValue() - preOrderDeposit.doubleValue());

        target.setPreOrderDepositAmount(
                targetPriceHelper.createPriceData(preOrderDeposit));

        target.setPreOrderOutstandingAmount(targetPriceHelper.createPriceData(preOrderOutstandingAmount));
    }

    /**
     * @param source
     * @return digitalProduct
     */
    private boolean hasAnyDigitalProductInCart(final CartModel source) {
        if (null != source && CollectionUtils.isNotEmpty(source.getEntries())) {
            for (final AbstractOrderEntryModel entry : source.getEntries()) {
                if (ProductUtil.isProductTypeDigital(entry.getProduct())) {
                    return true;
                }
            }
        }
        return false;
    }

    protected AddressData getBillingAddress(final CartModel cartModel) {
        if (cartModel.getPaymentAddress() != null) {
            return getAddressConverter().convert(cartModel.getPaymentAddress());
        }
        return null;
    }

    protected void updateCartEntriesWithPromotionInformation(final CartData cartData) {
        final List<PromotionResultData> productPromotions = cartData.getAppliedProductPromotions();
        if (!Collections.isEmpty(productPromotions)) {
            for (final OrderEntryData entry : cartData.getEntries()) {
                if (entry instanceof TargetOrderEntryData) {
                    final boolean dealsApplied = doesEntryHavePromotion(productPromotions,
                            entry.getEntryNumber().intValue());
                    ((TargetOrderEntryData)entry).setDealsApplied(dealsApplied);
                }
            }
        }
    }

    protected boolean doesEntryHavePromotion(final List<PromotionResultData> productPromotions, final int entryNumber) {
        if (productPromotions != null && !productPromotions.isEmpty()) {
            final Integer entryNumberToFind = Integer.valueOf(entryNumber);

            for (final PromotionResultData productPromotion : productPromotions) {
                if (StringUtils.isNotBlank(productPromotion.getDescription())) {
                    final List<PromotionOrderEntryConsumedData> consumedEntries = productPromotion.getConsumedEntries();
                    if (consumedEntries != null && !consumedEntries.isEmpty()) {
                        for (final PromotionOrderEntryConsumedData consumedEntry : consumedEntries) {
                            if (entryNumberToFind.equals(consumedEntry.getOrderEntryNumber())) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Populates all delivery modes.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     */
    protected void populateAllDeliveryModes(final CartModel source, final TargetCartData target) {

        final List<AbstractOrderEntryModel> cartEntries = source.getEntries();

        //digital delivery Mode...
        final List<TargetZoneDeliveryModeModel> digitalDeliveryModes = getDigitalDeliveryModes(cartEntries);


        if (CollectionUtils.isNotEmpty(digitalDeliveryModes)) {
            final List<TargetZoneDeliveryModeData> digitalDeliveryData = new ArrayList<>();
            for (final TargetZoneDeliveryModeModel digitalDeliveryMode : digitalDeliveryModes) {
                final TargetZoneDeliveryModeData targetZoneDeliveryModeData = targetZoneDeliveryModeConverter
                        .convert(digitalDeliveryMode);
                final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = targetDeliveryService
                        .getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(digitalDeliveryMode,
                                source);
                if (CollectionUtils.isNotEmpty(valueModelList)) {
                    final AbstractTargetZoneDeliveryModeValueModel deliveryValueModel = valueModelList.get(0);
                    targetZoneDeliveryModeData
                            .setLongDescription(deliveryValueModel.getMessage());
                    targetZoneDeliveryModeData.setShortDescription(deliveryValueModel
                            .getShortMessage());
                    targetZoneDeliveryModeData.setDisclaimer(deliveryValueModel.getDisclaimer());
                    targetZoneDeliveryModeData.setAvailable(true);
                    digitalDeliveryData.add(targetZoneDeliveryModeData);
                }
                target.setDigitalDeliveryModes(digitalDeliveryData);
            }
        }

        //Iterate over All the available non digital delivery modes in the system.
        target.setDeliveryModes(abstractOrderHelper.getPhysicalDeliveryModes(source));

        final boolean hasAnyProductWithPhysicalDelMode = orderHasProductsWithAPhysicalDelMode(source);
        final boolean hasValidDeliveryModes = CollectionUtils.isNotEmpty(target.getDeliveryModes())
                || (!hasAnyProductWithPhysicalDelMode && CollectionUtils.isNotEmpty(target.getDigitalDeliveryModes()));

        target.setHasValidDeliveryModes(hasValidDeliveryModes);
    }




    /**
     * @param cartModel
     * @return true if the cart has any product that is physical
     */
    private boolean orderHasProductsWithAPhysicalDelMode(final CartModel cartModel) {
        for (final AbstractOrderEntryModel entry : cartModel.getEntries()) {
            if (entry.getProduct() instanceof AbstractTargetVariantProductModel) {
                final AbstractTargetVariantProductModel product = (AbstractTargetVariantProductModel)entry
                        .getProduct();
                if (targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(product)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * @param cartEntries
     * @return Digital target Zone delivery mode
     */
    private List<TargetZoneDeliveryModeModel> getDigitalDeliveryModes(final List<AbstractOrderEntryModel> cartEntries) {
        final List<TargetZoneDeliveryModeModel> digitalDeliveryModes = new ArrayList<>();
        TargetZoneDeliveryModeModel digitalDeliveryMode = null;
        for (final AbstractOrderEntryModel cartEntry : cartEntries) {
            digitalDeliveryMode = targetDeliveryModeHelper.getDigitalDeliveryMode(cartEntry.getProduct());
            if (digitalDeliveryMode != null) {
                //to prevent adding duplicates in the list
                if (null == CollectionUtils.find(digitalDeliveryModes,
                        new DeliveryModePredicate(digitalDeliveryMode))) {
                    digitalDeliveryModes.add(digitalDeliveryMode);
                }
            }
        }

        return digitalDeliveryModes;
    }

    /**
     * @param target
     * @param source
     *
     */
    private void setLayByInfo(final CartModel source, final TargetCartData target) {
        if (!abstractOrderHelper.isLayBy(source)) {
            target.setIsLayBy(false);
            return;
        }
        target.setIsLayBy(true);
        if (source.getAmountCurrentPayment() != null) {
            target.setInitialLayByPaymentAmount(createPrice(source, source.getAmountCurrentPayment()));
        }

        final PurchaseOptionConfigModel layby = targetPurchaseOptionHelper.getPurchaseOptionConfigModelFromOptionCode(
                source.getPurchaseOption().getCode());

        if (layby != null) {
            target.setLayByFee(createPrice(source, layby.getServiceFee()));
            target.setLayByMin(createPrice(source, layby.getMinimumPurchaseAmount()));
        }

        target.setMinimumPaymentAmount(abstractOrderHelper.createMinimumPaymentAmountData(source,
                targetLaybyPaymentDueService.getInitialPaymentDue(source)));
        target.setMaximumPaymentAmount(abstractOrderHelper.createMaximumPaymentAmountData(source));
        target.setDueDates(abstractOrderHelper.createAmountDueDates(targetLaybyPaymentDueService
                .getAllPaymentDuesForAbstractOrder(source)));
        target.setFullPaymentDueDate(AbstractOrderHelper.DUE_DATE_FORMATTER.format(targetLaybyPaymentDueService
                .getFinalPaymentDue(source).getDueDate()));

    }


    private boolean isInsufficientAmount(final PurchaseOptionModel purchaseOption, final BigDecimal adjustedSubtotal) {

        final PurchaseOptionConfigModel purchaseOptionConfig = targetPurchaseOptionHelper
                .getPurchaseOptionConfigModelFromOptionCode(purchaseOption.getCode().toLowerCase());

        final boolean isInsufficient = (purchaseOptionConfig != null
                && purchaseOptionConfig.getMinimumPurchaseAmount() != null
                && adjustedSubtotal != null
                && purchaseOptionConfig.getMinimumPurchaseAmount().doubleValue() > adjustedSubtotal.doubleValue());
        return isInsufficient;
    }


    /**
     *
     * @param source
     * @param target
     */
    public void setUserInfo(final CartModel source, final TargetCartData target) {
        final boolean isGuestUser = targetCheckoutCustomerStrategy.isAnonymousCheckout();
        target.setAllowSaveAddress(!isGuestUser);
        target.setAllowSavePayment(!isGuestUser);
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @param abstractOrderHelper
     *            the abstractOrderHelper to set
     */
    @Required
    public void setAbstractOrderHelper(final AbstractOrderHelper abstractOrderHelper) {
        this.abstractOrderHelper = abstractOrderHelper;
    }

    /**
     * @param targetCheckoutCustomerStrategy
     *            the checkoutCustomerStrategy to set
     */
    @Required
    public void setTargetCheckoutCustomerStrategy(final TargetCheckoutCustomerStrategy targetCheckoutCustomerStrategy) {
        this.targetCheckoutCustomerStrategy = targetCheckoutCustomerStrategy;
    }


    /**
     * @param targetLaybyPaymentDueService
     *            the targetLaybyPaymentDueService to set
     */
    @Required
    public void setTargetLaybyPaymentDueService(final TargetLaybyPaymentDueService targetLaybyPaymentDueService) {
        this.targetLaybyPaymentDueService = targetLaybyPaymentDueService;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.converters.AbstractOrderConverter#addPaymentInformation(de.hybris.platform.core.model.order.AbstractOrderModel, de.hybris.platform.commercefacades.order.data.AbstractOrderData)
     */
    @Override
    protected void addPaymentInformation(final AbstractOrderModel source, final AbstractOrderData prototype) {
        final PaymentInfoModel paymentInfo = source.getPaymentInfo();
        if (paymentInfo instanceof CreditCardPaymentInfoModel) {
            prototype.setPaymentInfo(getCreditCardPaymentInfoConverter().convert(
                    (CreditCardPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof PinPadPaymentInfoModel) {
            prototype.setPaymentInfo(getTargetPinPadPaymentInfoConverter().convert(
                    (PinPadPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof IpgPaymentInfoModel) {
            prototype.setPaymentInfo(getTargetIpgPaymentInfoConverter().convert(
                    (IpgPaymentInfoModel)paymentInfo));
        }
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.converters.AbstractOrderConverter#addPromotions(de.hybris.platform.core.model.order.AbstractOrderModel, de.hybris.platform.promotions.result.PromotionOrderResults, de.hybris.platform.commercefacades.order.data.AbstractOrderData)
     */
    @Override
    protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype) {
        super.addPromotions(source, prototype);

        AbstractOrderHelper.addTargetProductPromotions(
                getPromotionsService().getPromotionResults(source),
                getModelService(), getPromotionResultConverter(), prototype);
        AbstractOrderHelper.addTargetOrderPromotions(
                getPromotionsService().getPromotionResults(source),
                getModelService(), getPromotionResultConverter(), prototype);
    }

    /**
     * @return the targetPinPadPaymentInfoConverter
     */
    protected Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> getTargetPinPadPaymentInfoConverter() {
        return targetPinPadPaymentInfoConverter;
    }

    /**
     * @return the targetIpgPaymentInfoConverter
     */
    protected Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> getTargetIpgPaymentInfoConverter() {
        return targetIpgPaymentInfoConverter;
    }

    /**
     * @param targetPinPadPaymentInfoConverter
     *            the pinPadPaymentInfoConverter to set
     */
    @Required
    public void setTargetPinPadPaymentInfoConverter(
            final Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> targetPinPadPaymentInfoConverter) {
        this.targetPinPadPaymentInfoConverter = targetPinPadPaymentInfoConverter;
    }

    /**
     * @param targetIpgPaymentInfoConverter
     *            the ipgPaymentInfoConverter to set
     */
    @Required
    public void setTargetIpgPaymentInfoConverter(
            final Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> targetIpgPaymentInfoConverter) {
        this.targetIpgPaymentInfoConverter = targetIpgPaymentInfoConverter;
    }


    /**
     * @param targetZoneDeliveryModeConverter
     *            the targetZoneDeliveryModeConverter to set
     */
    @Required
    public void setTargetZoneDeliveryModeConverter(
            final Converter<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> targetZoneDeliveryModeConverter) {
        this.targetZoneDeliveryModeConverter = targetZoneDeliveryModeConverter;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param targetFlybuysDiscountConverter
     *            the targetFlybuysDiscountConverter to set
     */
    @Required
    public void setTargetFlybuysDiscountConverter(final TargetFlybuysDiscountConverter targetFlybuysDiscountConverter) {
        this.targetFlybuysDiscountConverter = targetFlybuysDiscountConverter;
    }

    /**
     * @return the targetSalesApplicationService
     */
    public TargetSalesApplicationService getTargetSalesApplicationService() {
        return this.targetSalesApplicationService;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

    /**
     * @param invoiceFormatterHelper
     *            the invoiceFormatterHelper to set
     */
    @Required
    public void setInvoiceFormatterHelper(final InvoiceFormatterHelper invoiceFormatterHelper) {
        this.invoiceFormatterHelper = invoiceFormatterHelper;
    }

    /**
     * deems two delivery modes as equal if they have the same codes
     * 
     * @author rsamuel3
     *
     */
    private class DeliveryModePredicate implements Predicate {

        private final DeliveryModeModel delMode1;

        protected DeliveryModePredicate(final DeliveryModeModel delMode) {
            this.delMode1 = delMode;
        }

        /* (non-Javadoc)
         * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
         */
        @Override
        public boolean evaluate(final Object obj) {
            if (obj instanceof DeliveryModeModel) {
                final DeliveryModeModel delMode = (DeliveryModeModel)obj;
                return StringUtils.equals(delMode1.getCode(), delMode.getCode());
            }
            return false;
        }
        /* (non-Javadoc)
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */

    }

    /**
     * @param targetPriceHelper
     *            the targetPriceHelper to set
     */
    @Required
    public void setTargetPriceHelper(final TargetPriceHelper targetPriceHelper) {
        this.targetPriceHelper = targetPriceHelper;
    }

}
