/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetDealWithRewardResultModel;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;


/**
 * The Class TargetPromotionResultPopulator.
 *
 * @author rahul
 */
public class TargetPromotionResultPopulator implements
        Populator<PromotionResultModel, PromotionResultData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final PromotionResultModel source, final PromotionResultData target) {

        if (null != source.getCustom() && source.getPromotion() instanceof TMDiscountProductPromotionModel) {
            target.setOrderEntryNumber(Integer.valueOf(source.getCustom()));
            final PromotionOrderEntryConsumedData consumedData = new PromotionOrderEntryConsumedData();
            consumedData.setOrderEntryNumber(Integer.valueOf(source.getCustom()));
            final List<PromotionOrderEntryConsumedData> consumedDatas =
                    new ArrayList<>();
            consumedDatas.add(consumedData);
            target.setConsumedEntries(consumedDatas);
        }

        if (source instanceof TargetDealWithRewardResultModel) {
            final TargetDealWithRewardResultModel result = (TargetDealWithRewardResultModel)source;
            target.setCanHaveMoreRewards(result.getNumberofRewardsSofar().compareTo(result.getMaxNumberofRewards()) < 0);
        }
    }

}