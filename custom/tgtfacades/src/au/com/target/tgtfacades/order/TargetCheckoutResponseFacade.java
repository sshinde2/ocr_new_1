/**
 * 
 */
package au.com.target.tgtfacades.order;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfacades.voucher.data.TargetFlybuysLoginData;


/**
 * This facade interface is created to return checkout related data/service/result as Response. Hence it can be
 * converted to Json object easily by storefront controllers.
 * 
 * TargetCheckoutResponseFacade is heavily dependent on TargetCheckoutFacade and TargetCartFacade.
 * 
 * 
 * 
 * @author htan3
 *
 */
public interface TargetCheckoutResponseFacade {

    /**
     * Get applicable delivery modes for the session cart. A successful response will still be returned with no data if
     * cart/delivery modes unavailable.
     * 
     * @return response
     */
    Response getApplicableDeliveryModes();

    /**
     * Set the delivery mode to cart if a delivery mode is applicable to that cart.
     * 
     * @param deliveryMode
     * @return Response
     */
    Response setDeliveryMode(final String deliveryMode);



    /**
     * Set the pickup store to cart if is applicable to that cart.
     * 
     * @param storeNumber
     * @return Response
     */
    Response setCncStore(final Integer storeNumber);

    /**
     * Set the pickup details for cnc to cart if is applicable to that cart.
     * 
     * @param cncDeliveryDetails
     * 
     * @return {@link Response}
     */
    Response setCncPickupDetails(ClickAndCollectDeliveryDetailData cncDeliveryDetails);

    /**
     * search nearby cnc stores
     * 
     * @param locationText
     * @param pageableData
     * @return Response
     */
    Response searchCncStores(final String locationText, final PageableData pageableData);

    /**
     * Get cart detail
     * 
     * @return cart detail
     */
    Response getCartDetail();

    /**
     * Get cart summary
     * 
     * @return cart summary
     */
    Response getCartSummary();

    /**
     * get addresses with validDeliveryModelIds for current cart.
     */
    Response getDeliveryAddresses();

    /**
     * apply team member discount card to the current cart. return true if tmd card applies successfully.
     * 
     * @param tmdNumber
     * @return Response
     */
    Response applyTmd(final String tmdNumber);

    /**
     * Set voucher to cart.
     * 
     * @param voucherCode
     * @return Response
     */
    Response applyVoucher(final String voucherCode);

    /**
     * Remove a voucher from cart if one already exists.
     * 
     * @return Response
     */
    Response removeVoucher();

    /**
     * Set flybuysNumber to cart.
     * 
     * @param flybuysNumber
     * @return Response
     */
    Response applyFlybuys(final String flybuysNumber);

    /**
     * select the address for given delivery mode code
     * 
     * @param addressId
     * @return successful if address is set to the current cart successfully
     */
    Response selectDeliveryAddress(final String addressId);

    /**
     * create new delivery address for the current userOrderDe
     * 
     * @param newAddressData
     * @return Response with saved address
     */
    Response createAddress(TargetAddressData newAddressData);

    /**
     * Get applicable payment methods.
     * 
     * @return Response with paymentMethods
     */
    Response getApplicablePaymentMethods();

    /**
     * Get suggested address using the given keyword via QAS
     * 
     * @param keyword
     * @param maxSize
     *            defines the maximum number of suggestions
     * @return Response with addressSuggestions
     */
    Response searchAddress(String keyword, int maxSize);

    /**
     * Set ipg payment mode for session cart and fetch token from ipg
     * 
     * @param paymentMode
     * @param returnUrl
     * @return Response with token and redirect url
     */
    Response setIpgPaymentMode(String paymentMode, String returnUrl);

    /**
     * Set paypal payment mode for session cart and fetch return url
     * 
     * @param payPalReturnUrl
     * @param payPalCancelUrl
     * @return Response with token and redirect url
     */
    Response setPaypalPaymentMode(final String payPalReturnUrl, final String payPalCancelUrl);

    /**
     * Set afterpay payment mode for session cart and fetch return url
     * 
     * @param afterpayReturnUrl
     * @param afterpayCancelUrl
     * @return Response with token and redirect url
     */
    Response setAfterpayPaymentMode(final String afterpayReturnUrl, final String afterpayCancelUrl);

    /**
     * Set zippay payment mode for session cart and fetch redirect url
     * 
     * @param zippayRedirectUrl
     * @return Response with redirect URL
     */
    Response setZippayPaymentMode(final String zippayRedirectUrl);

    /**
     * reverse existing gift card payments if there is, and remove existing payment info from cart
     */
    void removeExistingPayment();

    /**
     * Get countries which the billing address can be
     * 
     * @return Response with list of countries
     */
    Response getBillingCountries();

    /**
     * create new billing address for the current user
     * 
     * @param newAddressData
     * @return Response with saved address
     */
    Response createBillingAddress(TargetAddressData newAddressData);

    /**
     * Remove flybuysNumber from cart.
     * 
     * @return Response
     */
    Response removeFlybuys();

    /**
     * @param addressId
     */
    Response selectBillingAddress(String addressId);

    /**
     * check whether there is successful gift card payment on cart
     */
    Response getIpgPaymentStatus();

    /**
     * login the flybuys
     * 
     * @param flybuysLoginData
     * @return Response
     */
    Response loginFlybuys(TargetFlybuysLoginData flybuysLoginData);

    /**
     * Apply flybuys redemption
     * 
     * @param redeemCode
     * @return Response
     */
    Response redeemFlybuys(String redeemCode);

    /**
     * Show flybuys redemption option
     * 
     * @return Response
     */
    Response showFlybuysRedemptionOption();

    /**
     * Gets order detail data
     * 
     * @param orderCode
     * @param sohUpdates
     * @return Response
     */
    Response getOrderDetail(String orderCode, AdjustedCartEntriesData sohUpdates);

    /**
     * Registers a customer after placing an order.
     * 
     * @param orderId
     * @param password
     * 
     * @return Response
     */
    Response registerCustomer(String orderId, String password);

    /**
     * Gets the response for checkout problem.
     * 
     * @return Response
     */
    Response checkoutProblem();

    /**
     * verify whether the customer email is registered for Australia Post Delivery Club
     * 
     * @return - DeliveryClubEmailVerificationResponseData
     */
    Response verifyShipsterStatus();

}
