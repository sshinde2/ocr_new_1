/**
 * 
 */
package au.com.target.tgtfacades.salesapplication.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;


/**
 * Implementation of SalesApplicationFacade
 * 
 * @author jjayawa1
 * 
 */
public class SalesApplicationFacadeImpl implements SalesApplicationFacade {



    private TargetSalesApplicationService targetSalesApplicationService;

    private SessionService sessionService;

    @Override
    public void initializeRequest(final HttpServletRequest request) {
        final String mode = request.getHeader(TgtFacadesConstants.CustomHeaders.X_TARGET_HEADER);

        if (TgtFacadesConstants.CustomHeaders.KIOSK_MODE.equals(mode)) {
            targetSalesApplicationService.setCurrentSalesApplication(SalesApplication.KIOSK);
        }
        else if (sessionService.getAttribute(TgtFacadesConstants.ASSISTED_CHECKOUT_USER_SESSION_KEY) != null) {
            targetSalesApplicationService.setCurrentSalesApplication(SalesApplication.STOREASSISTED);
        }
        else if (TgtFacadesConstants.CustomHeaders.MOBILE_APP.equals(mode)) {
            targetSalesApplicationService.setCurrentSalesApplication(SalesApplication.MOBILEAPP);
        }
        else {
            targetSalesApplicationService.setCurrentSalesApplication(SalesApplication.WEB);
        }
    }

    @Override
    public SalesApplication getCurrentSalesApplication() {
        return targetSalesApplicationService.getCurrentSalesApplication();
    }

    @Override
    public boolean isKioskApplication() {
        return getCurrentSalesApplication() == SalesApplication.KIOSK;
    }

    @Override
    public boolean isSalesChannelMobileApp() {
        return SalesApplication.MOBILEAPP == getCurrentSalesApplication();
    }


    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

    /**
     * @return the targetSalesApplicationService
     */
    protected TargetSalesApplicationService getTargetSalesApplicationService() {
        return targetSalesApplicationService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

}
