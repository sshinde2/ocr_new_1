/**
 * 
 */
package au.com.target.tgtfacades.wishlist;

import java.util.List;

import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;


/**
 * @author paul
 *
 */
public interface TargetWishListFacade {

    /**
     * This method fetches the list and adds the product to the list. Note - if the list type is FAVOURITE-> the name of
     * the list will be taken as Favourites and the existing name will be ignored.
     * 
     * 
     */
    void addProductToList(final CustomerProductInfo customerProductInfo, final String listName,
            final WishListTypeEnum wishListTypeEum);

    /**
     * This method fetches the list and removes the product from the list. Note - if the list type is FAVOURITE-> the
     * name of the list will be taken as Favourites and the existing name will be ignored.
     * 
     * 
     */
    void removeProductsFromList(final List<CustomerProductInfo> customerProductInfo, final String listName,
            final WishListTypeEnum wishListTypeEum);


    /**
     * This method syncs the list of favourites saved at the client end to the server
     * 
     * @param customerProductInfo
     * @return updated favourites list
     */
    List<CustomerProductInfo> syncFavouritesList(final List<CustomerProductInfo> customerProductInfo);

    /**
     * This method retrieves the list of favourites saved at the server
     * 
     * 
     * @return favourites list in server
     */
    List<CustomerProductInfo> retrieveFavouriteProducts();


    /**
     * Converts the targetproductlisterdata and send the list of products to exacttarget to be shared in the email
     * 
     * @param targetProductListerData
     * @param receipientEmailAddress
     * @param name
     * @param messageText
     * @return boolean
     */
    boolean shareWishListForUser(List<TargetProductListerData> targetProductListerData,
            String receipientEmailAddress, String name, String messageText);

    /**
     * This method replaces all the wish list entries with the provided customer data list.
     * 
     * @param customerProductInfo
     * @param updatedCustomerProductInfo
     */
    void replaceFavouritesWithLatest(List<CustomerProductInfo> customerProductInfo,
            List<CustomerProductInfo> updatedCustomerProductInfo);
}
