/**
 * 
 */
package au.com.target.tgtfacades.storelocator.data;



public class TargetPointOfServiceStockData extends TargetPointOfServiceData {

    private String stockLevel;

    /**
     * @return the stockLevel
     */
    public String getStockLevel() {
        return stockLevel;
    }


    /**
     * @param stockLevel
     *            the stockLevel to set
     */
    public void setStockLevel(final String stockLevel) {
        this.stockLevel = stockLevel;
    }


}
