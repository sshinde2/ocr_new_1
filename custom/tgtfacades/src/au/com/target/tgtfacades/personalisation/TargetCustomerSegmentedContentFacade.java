/**
 * 
 */
package au.com.target.tgtfacades.personalisation;

import au.com.target.tgtwebcore.model.cms2.pages.TargetFragmentPageModel;


/**
 * @author pthoma20
 *
 */
public interface TargetCustomerSegmentedContentFacade {

    public TargetFragmentPageModel getSegmentedContentFromPersonalisedComponent(final String componentId,
            String userSegmentId);
}
