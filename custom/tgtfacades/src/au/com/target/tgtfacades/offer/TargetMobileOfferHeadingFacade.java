/**
 * 
 */
package au.com.target.tgtfacades.offer;

import java.util.List;

import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;


/**
 * @author paul
 *
 */
public interface TargetMobileOfferHeadingFacade {

    /**
     * This method fetches all the offer headings for mobile.
     * 
     * @return targetDealsData
     */
    public List<TargetMobileOfferHeadingData> getTargetMobileOfferHeadings();

}
