/**
 *
 */
package au.com.target.tgtfacades.offer.converter.populator;

import de.hybris.platform.converters.Populator;

import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * The Class TargetDealsPopulator.
 *
 * @author pthoma20
 */
public class TargetMobileOfferHeadingPopulator implements
        Populator<TargetMobileOfferHeadingModel, TargetMobileOfferHeadingData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetMobileOfferHeadingModel source, final TargetMobileOfferHeadingData target) {
        target.setCode(source.getCode());
        target.setColour(source.getColour());
        target.setName(source.getName());
    }
}