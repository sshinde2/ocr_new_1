/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantTypeModel;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;


/**
 * The Class TargetVariantProductListerBasicDataPopulator.
 *
 * @author pthoma20
 */
public class TargetVariantProductListerBasicDataPopulator implements
        Populator<AbstractTargetVariantProductModel, TargetVariantProductListerData> {

    private TargetProductDataUrlResolver targetProductDataUrlResolver;


    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final AbstractTargetVariantProductModel source, final TargetVariantProductListerData target)
            throws ConversionException {
        target.setCode(source.getCode());
        target.setName(source.getDisplayName());
        target.setUrl(targetProductDataUrlResolver.resolve(source.getName(), source.getCode()));
        final VariantTypeModel variantTypeModel = source.getVariantType();
        if (variantTypeModel != null) {
            target.setVariantType(variantTypeModel.getCode());
        }
        target.setItemType(source.getItemtype());
        target.setPreview(BooleanUtils.isTrue(source.getPreview()));

        if (StringUtils.isNotBlank(source.getEan())) {
            target.setApn(source.getEan());
        }
    }


    /**
     * @param targetProductDataUrlResolver
     *            the targetProductDataUrlResolver to set
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }



}