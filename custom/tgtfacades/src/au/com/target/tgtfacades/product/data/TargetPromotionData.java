/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import de.hybris.platform.commercefacades.product.data.PromotionData;


/**
 * @author rmcalave
 * 
 */
public class TargetPromotionData extends PromotionData {
    private boolean tmdPromotion;

    /**
     * @return the isTmdPromotion
     */
    public boolean isTmdPromotion() {
        return tmdPromotion;
    }

    /**
     * @param tmdPromotion
     *            the isTmdPromotion to set
     */
    public void setTmdPromotion(final boolean tmdPromotion) {
        this.tmdPromotion = tmdPromotion;
    }
}
