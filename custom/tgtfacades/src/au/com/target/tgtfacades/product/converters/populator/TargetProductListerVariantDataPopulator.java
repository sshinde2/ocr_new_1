/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * The Class TargetProductListerVariantDataPopulator.
 *
 * @author pthoma20
 */
public class TargetProductListerVariantDataPopulator implements
        Populator<TargetProductModel, TargetProductListerData> {


    private Converter<VariantProductModel, TargetVariantProductListerData> variantPdtListerConverter;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetProductModel source, final TargetProductListerData target)
            throws ConversionException {

        final Collection<VariantProductModel> variants = source.getVariants();
        if (CollectionUtils.isEmpty(variants)) {
            return;
        }
        final List<TargetVariantProductListerData> tgtVariantPdtListerData = new ArrayList<>();
        for (final VariantProductModel variantPdtModel : variants) {
            final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();
            tgtVariantPdtListerData
                    .add(variantPdtListerConverter.convert(variantPdtModel, targetVariantProductListerData));
        }
        target.setTargetVariantProductListerData(tgtVariantPdtListerData);
    }

    /**
     * @param variantPdtListerConverter
     *            the variantPdtListerConverter to set
     */
    @Required
    public void setVariantPdtListerConverter(
            final Converter<VariantProductModel, TargetVariantProductListerData> variantPdtListerConverter) {
        this.variantPdtListerConverter = variantPdtListerConverter;
    }


}