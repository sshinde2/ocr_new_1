/**
 * 
 */
package au.com.target.tgtfacades.google.location.finder;

import au.com.target.tgtfacades.response.data.Response;

/**
 * @author bbaral1
 *
 */
public interface TargetGoogleLocationFinderFacade {

    /**
     * Find user's delivery location based on the location/postCode parameter.
     * 
     * @param locationOrPostCode
     * @param latitude
     * @param longitude
     * @return GoogleResponse
     */
    Response searchUserDeliveryLocation(final String locationOrPostCode, final Double latitude, final Double longitude);

    /**
     * Find user's current location details based on latitude and longitude.
     * 
     * @param latitude
     * @param longitude
     * @return String
     */
    String searchUserCurrentAddressUsingLatLng(final Double latitude, final Double longitude);
}
