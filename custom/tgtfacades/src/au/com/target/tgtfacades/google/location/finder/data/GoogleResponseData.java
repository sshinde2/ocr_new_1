/**
 * 
 */
package au.com.target.tgtfacades.google.location.finder.data;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.response.data.BaseResponseData;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class GoogleResponseData extends BaseResponseData {

    private String searchTerm;

    private List<Result> results;

    /**
     * @return the searchTerm
     */
    public String getSearchTerm() {
        return searchTerm;
    }

    /**
     * @param searchTerm
     *            the searchTerm to set
     */
    public void setSearchTerm(final String searchTerm) {
        this.searchTerm = searchTerm;
    }

    /**
     * @return the results
     */
    public List<Result> getResults() {
        return results;
    }

    /**
     * @param results
     *            the results to set
     */
    public void setResults(final List<Result> results) {
        this.results = results;
    }
}