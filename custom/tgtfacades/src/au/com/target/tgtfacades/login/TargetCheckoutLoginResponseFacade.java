/**
 * 
 */
package au.com.target.tgtfacades.login;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetRegisterData;


/**
 * @author rmcalave
 *
 */
public interface TargetCheckoutLoginResponseFacade {

    /**
     * Create a Response for login required.
     * 
     * @return a Response
     */
    Response createLoginRequiredResponse();

    /**
     * Create a Response for login success.
     * 
     * @param redirectUrl
     *            The URL that the client should redirect to
     * @return a Response
     */
    Response createLoginSuccessResponse(String redirectUrl);

    /**
     * Create a Response for login failed.
     * 
     * @param exceptionClass
     *            The Exception returned by Spring Security during the authentication process
     * @return a Response
     */
    Response createLoginFailedResponse(Class exceptionClass);

    /**
     * Lookup whether a customer exists with the given user name.
     * 
     * @param username
     *            The username to look up
     * @return a Response
     */
    Response isCustomerRegistered(String username);

    /**
     * Process the request to begin checkout as guest
     * 
     * @param username
     *            The email address to begin guest checkout as
     * @param redirectUrl
     *            The URL that the client should redirect to on success
     * @return a Response
     */
    Response processGuestCheckoutRequest(String username, String redirectUrl);

    /**
     * Process the request to register an account.
     * 
     * @param registerData
     *            The registration data
     * @param redirectUrl
     *            The URL that the client should redirect to on success
     * @return a Response
     */
    Response processCustomerRegistrationRequest(TargetRegisterData registerData, String redirectUrl);

    /**
     * Process set new password by token
     * 
     * @param password
     * @param token
     * @return Response
     */
    Response processSetPasswordByToken(String password, String token);

    /**
     * Process redirect after login
     * 
     * @param response
     * @return Response
     */
    Response processSetPasswordRedirect(Response response);
}