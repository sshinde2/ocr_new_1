/**
 * 
 */
package au.com.target.tgtfacades.login;

import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author rmcalave
 * 
 */
public interface TargetAuthenticationFacade {

    /**
     * Determines whether the customer associated with the provided username is locked.
     * 
     * Will return false if the account is not locked, but also if there is no customer for the provided username.
     * 
     * @param username
     *            The user name of the customer to check
     * @return true if the account is locked, false in all other cases
     * @see #isAccountLocked(TargetCustomerModel)
     */
    boolean isAccountLocked(String username);

    /**
     * Determines whether the provided customer is locked.
     * 
     * Will return false if the account is not locked, but also if there is no customer for the provided username.
     * 
     * @param customer
     *            The customer to check
     * @return true if the account is locked, false in all other cases
     */
    boolean isAccountLocked(TargetCustomerModel customer);

    /**
     * Increments the failed login attempt counter on the customer associated with the provided username.
     * 
     * Will do nothing if there is no customer for the provided username.
     * 
     * @param username
     *            the user name of the customer to increment the counter on
     */
    void incrementFailedLoginAttemptCounter(String username);

    /**
     * Resets the failed login attempt counter on the customer associated with the provided username.
     * 
     * Will do nothing if there is no customer for the provided username.
     * 
     * @param username
     *            the user name of the customer to reset the counter on
     */
    void resetFailedLoginAttemptCounter(String username);

}