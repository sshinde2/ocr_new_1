/**
 * 
 */
package au.com.target.tgtfacades.login.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;

import au.com.target.tgtcore.customer.ReusePasswordException;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CustomerRegisteredResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.LoginSuccessResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetRegisterData;



/**
 * @author rmcalave
 *
 */
public class TargetCheckoutLoginResponseFacadeImpl implements TargetCheckoutLoginResponseFacade {

    private TargetAuthenticationFacade targetAuthenticationFacade;

    private TargetCustomerFacade targetCustomerFacade;

    private TargetCheckoutFacade targetCheckoutFacade;

    private SessionService sessionService;

    private TargetUserFacade targetUserFacade;

    private TargetCustomerAccountService customerAccountService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade#createLoginRequiredResponse()
     */
    @Override
    public Response createLoginRequiredResponse() {
        final Error error = new Error(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REQUIRED);
        error.setMessage("please login");

        final BaseResponseData data = new BaseResponseData();
        data.setError(error);

        final Response response = new Response(false);
        response.setData(data);

        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade#createLoginSuccessResponse(java.lang.String)
     */
    @Override
    public Response createLoginSuccessResponse(final String redirectUrl) {
        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl(redirectUrl);

        final Response response = new Response(true);
        response.setData(data);

        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade#createLoginFailedResponse(java.lang.Class)
     */
    @Override
    public Response createLoginFailedResponse(final Class exceptionClass) {
        final Response response = new Response(false);
        final BaseResponseData data = new BaseResponseData();

        data.setError(getLoginErrorIfLastLoginAttemptFailed(exceptionClass));

        response.setData(data);

        return response;
    }

    private Error getLoginErrorIfLastLoginAttemptFailed(final Class exceptionClass) {
        Error error = null;
        if (exceptionClass != null) {
            if (exceptionClass.equals(BadCredentialsException.class)) {
                error = new Error(TgtFacadesConstants.WebServiceError.ERR_LOGIN_BAD_CREDENTIALS);
            }
            else if (exceptionClass.equals(LockedException.class)) {
                error = new Error(TgtFacadesConstants.WebServiceError.ERR_LOGIN_ACCOUNT_LOCKED);
            }
        }

        if (error == null) {
            error = new Error(TgtFacadesConstants.WebServiceError.ERR_LOGIN_UNKNOWN);
        }

        return error;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade#isCustomerRegistered(java.lang.String)
     */
    @Override
    public Response isCustomerRegistered(final String username) {
        final TargetCustomerModel customer = targetCustomerFacade.getCustomerForUsername(username);

        final CustomerRegisteredResponseData data = new CustomerRegisteredResponseData();

        if (customer == null) {
            data.setRegistered(false);
        }
        else {
            data.setRegistered(true);
            data.setAccountLocked(targetAuthenticationFacade.isAccountLocked(customer));
        }

        final Response response = new Response(true);
        response.setData(data);

        return response;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade#processGuestCheckoutRequest(java.lang.String, java.lang.String)
     */
    @Override
    public Response processGuestCheckoutRequest(final String username, final String redirectUrl) {
        if (!targetCheckoutFacade.isGuestCheckoutAllowed()) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NOT_ALLOWED,
                    "Guest checkout is not currently allowed.");
        }

        if (targetCheckoutFacade.isGiftCardInCart()) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NO_GIFT_CARDS,
                    "Guest checkout cannot be used to purchase gift cards.");
        }

        if (targetCheckoutFacade.isCartHavingPreOrderItem()) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NO_PREORDERS,
                    "An account is required in order to purchase a pre-order item.");
        }
        try {
            final CartModel checkoutCart = targetCheckoutFacade.getCart();

            if (targetUserFacade.isCurrentUserAnonymous() && checkoutCart != null
                    && checkoutCart.getUser() instanceof TargetCustomerModel
                    && CustomerType.GUEST.equals(((TargetCustomerModel)checkoutCart.getUser()).getType())) {
                //update the new email address for guest checkout switching to another guest checkout
                customerAccountService.updateUidForGuestUser(username, (TargetCustomerModel)checkoutCart.getUser());
            }
            else {

                targetCustomerFacade.createGuestUserForAnonymousCheckout(username,
                        "Guest");
            }
        }
        catch (final DuplicateUidException ex) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_USER_EXISTS,
                    "A user with this ID already exists.");
        }

        sessionService.setAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);

        final TargetCustomerModel customer = targetCustomerFacade.getCustomerForUsername(username);
        if (customer != null) {
            sessionService.setAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST, Boolean.TRUE);
        }

        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl(redirectUrl);

        final Response response = new Response(true);
        response.setData(data);

        return response;
    }

    @Override
    public Response processCustomerRegistrationRequest(final TargetRegisterData registerData,
            final String redirectUrl) {
        if (targetCustomerFacade.isPasswordBlacklisted(registerData.getPassword())) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_WEAK_PASSWORD,
                    "Supplied password is too weak");
        }

        try {
            targetCustomerFacade.register(registerData);
        }
        catch (final DuplicateUidException ex) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_USER_EXISTS,
                    "A user with this ID already exists.");
        }

        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl(redirectUrl);

        final Response response = new Response(true);
        response.setData(data);

        return response;
    }


    @Override
    public Response processSetPasswordByToken(final String password, final String token) {
        if (StringUtils.isEmpty(token)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_INVALID_TOKEN,
                    TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_GENERAL);
        }

        try {
            return processSetPassword(password, token);
        }
        catch (final TokenInvalidatedException e) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_INVALID_TOKEN,
                    TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_GENERAL);
        }
        catch (final ReusePasswordException ex) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_REUSE_PASSWORD,
                    TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_REUSE_PASSWORD);
        }
        catch (final IllegalArgumentException ex) {
            // This occurs when the token is too short, has expired, the associated user is not found
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_PASSWORDRESET_GENERAL,
                    TgtFacadesConstants.WebServiceMessage.ERR_MESS_PASSWORDRESET_GENERAL);
        }
    }

    private Response processSetPassword(final String password, final String token) throws TokenInvalidatedException {
        final String uid = targetCustomerFacade.getUidForPasswordResetToken(token);
        final String redirectPage = targetCustomerFacade.getRedirectPageForPasswordResetToken(token);
        targetCustomerFacade.updatePassword(token, password);
        if (StringUtils.isNotEmpty(uid)) {
            targetCustomerFacade.resetRedirectPageForPasswordResetToken(uid);
        }
        final LoginSuccessResponseData data = new LoginSuccessResponseData();
        data.setRedirectUrl(redirectPage);
        final Response response = new Response(true);
        response.setData(data);
        return response;
    }

    @Override
    public Response processSetPasswordRedirect(final Response response) {
        final LoginSuccessResponseData data;
        if (response.getData() instanceof LoginSuccessResponseData) {
            data = (LoginSuccessResponseData)response.getData();
        }
        else {
            data = new LoginSuccessResponseData();
        }
        String redirectPage = data.getRedirectUrl();
        if (StringUtils.isEmpty(redirectPage)) {
            redirectPage = TgtFacadesConstants.Page.MY_ACCOUNT;
        }
        else if (TgtFacadesConstants.Page.SINGLE_PAGE_CHECKOUT_AND_LOGIN.equals(redirectPage)) {
            final CartData cart = targetCheckoutFacade.getCheckoutCart();
            if (cart != null && Integer.valueOf(0).equals(cart.getTotalItems())) {
                redirectPage = "/"; // home page
            }
        }
        if (!redirectPage.startsWith("/")) {
            redirectPage = "/" + redirectPage;
        }
        data.setRedirectUrl(redirectPage);
        response.setData(data);
        return response;
    }



    private Response createErrorResponse(final String errorCode, final String errorMessage) {
        final Response response = new Response(false);

        if (errorCode != null) {
            final Error error = new Error(errorCode);
            error.setMessage(errorMessage);

            final BaseResponseData data = new BaseResponseData();
            data.setError(error);

            response.setData(data);
        }

        return response;
    }

    /**
     * @param targetAuthenticationFacade
     *            the targetAuthenticationFacade to set
     */
    @Required
    public void setTargetAuthenticationFacade(final TargetAuthenticationFacade targetAuthenticationFacade) {
        this.targetAuthenticationFacade = targetAuthenticationFacade;
    }

    /**
     * @param targetCustomerFacade
     *            the targetCustomerFacade to set
     */
    @Required
    public void setTargetCustomerFacade(final TargetCustomerFacade targetCustomerFacade) {
        this.targetCustomerFacade = targetCustomerFacade;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param targetUserFacade
     *            the targetUserFacade to set
     */
    @Required
    public void setTargetUserFacade(final TargetUserFacade targetUserFacade) {
        this.targetUserFacade = targetUserFacade;
    }

    /**
     * @param customerAccountService
     *            the customerAccountService to set
     */
    @Required
    public void setCustomerAccountService(final TargetCustomerAccountService customerAccountService) {
        this.customerAccountService = customerAccountService;
    }

}
