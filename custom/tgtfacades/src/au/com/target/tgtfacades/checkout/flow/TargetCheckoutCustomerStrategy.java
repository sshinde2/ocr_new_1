/**
 * 
 */
package au.com.target.tgtfacades.checkout.flow;

import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.user.UserModel;

import javax.servlet.http.HttpSession;

import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author asingh78
 * 
 */
public interface TargetCheckoutCustomerStrategy extends CheckoutCustomerStrategy {

    /**
     * Removes the guest customer created during anonymous checkout. Removes all the associated checkout carts and also
     * clears the TMD from the master cart if present
     */
    void removeGuestCheckoutCustomer(final TargetCustomerModel guestCustomer, UserModel newCartCustomer);

    /**
     * Reset the cart with registered user
     */
    void resetGuestCartWithCurrentUser(HttpSession session);

}
