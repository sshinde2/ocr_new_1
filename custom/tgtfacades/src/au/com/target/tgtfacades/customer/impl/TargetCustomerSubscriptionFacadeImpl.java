/**
 * 
 */
package au.com.target.tgtfacades.customer.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.service.TargetCustomerSubscriptionService;


/**
 * @author rmcalave
 * 
 */
public class TargetCustomerSubscriptionFacadeImpl implements TargetCustomerSubscriptionFacade {

    private TargetCustomerSubscriptionService targetCustomerSubscriptionService;

    private TargetSalesApplicationService targetSalesApplicationService;


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.impl.TargetCustomerSubscriptionFacade#updateCustomerDetails(au.com.target.tgtfacades.customer.data.TargetCustomerSubscriptionData)
     */
    @Override
    public TargetCustomerSubscriptionResponseDto performCustomerSubscription(
            final TargetCustomerSubscriptionRequestDto subscriptionDto) {

        final SalesApplication currSalesApp = targetSalesApplicationService.getCurrentSalesApplication();
        if (SalesApplication.KIOSK.equals(currSalesApp)) {
            subscriptionDto.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.KIOSK);
        }
        final TargetCustomerSubscriptionResponseDto responseDto = targetCustomerSubscriptionService
                .createCustomerSubscription(subscriptionDto);

        if (responseDto == null || TargetCustomerSubscriptionResponseType.UNAVAILABLE == responseDto.getResponseType()) {
            targetCustomerSubscriptionService.processCustomerSubscription(subscriptionDto);
        }
        return responseDto;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade#processCustomerPersonalDetails(au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto)
     */
    @Override
    public boolean processCustomerPersonalDetails(final TargetCustomerSubscriptionRequestDto customerSubscriptionDto) {

        final SalesApplication currSalesApp = targetSalesApplicationService.getCurrentSalesApplication();
        if (SalesApplication.KIOSK.equals(currSalesApp)) {
            customerSubscriptionDto.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.KIOSK);
        }
        final boolean success = targetCustomerSubscriptionService
                .processCustomerPersonalDetails(customerSubscriptionDto);
        return success;
    }

    /**
     * @param targetCustomerSubscriptionService
     *            the targetCustomerSubscriptionService to set
     */
    @Required
    public void setTargetCustomerSubscriptionService(
            final TargetCustomerSubscriptionService targetCustomerSubscriptionService) {
        this.targetCustomerSubscriptionService = targetCustomerSubscriptionService;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }



}
