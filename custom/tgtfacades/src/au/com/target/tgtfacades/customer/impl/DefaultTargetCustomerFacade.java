/**
 * 
 */
package au.com.target.tgtfacades.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserConstants;
import de.hybris.platform.util.WebSessionFunctions;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.customergroups.TargetCustomerGroupFacade;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerData;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtutility.util.StringTools;


/**
 * @author asingh78
 * 
 */
public class DefaultTargetCustomerFacade<T extends CustomerData> extends DefaultCustomerFacade implements
        TargetCustomerFacade<T> {

    protected static final String BLACKLISTED_PASSWORD_KEY = "blacklisted.passwords";

    private static final Logger LOG = Logger.getLogger(DefaultTargetCustomerFacade.class);

    private static final String PASSWORD_REGEX_FRONT = "(.)*";
    private static final String PASSWORD_REGEX_BACK = "(.)*";
    private static Pattern blacklistedPasswordRegex;

    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;
    private TargetCartFacade targetCartFacade;
    private SalesApplicationFacade salesApplicationFacade;
    private Map<String, String> userGroupForSalesApplication;
    private TargetCustomerGroupFacade targetCustomerGroupFacade;
    private ConfigurationService configurationService;

    private TargetOrderService targetOrderService;

    private TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.TargetCustomerFacade#createGuestUserForAnonymousCheckout(java.lang.String, java.lang.String)
     */
    @Override
    public void createGuestUserForAnonymousCheckout(final String email, final String name)
            throws DuplicateUidException {
        validateParameterNotNullStandardMessage("email", email);

        final TargetCustomerModel guestCustomer = ((TargetCustomerAccountService)getCustomerAccountService())
                .registerGuestForAnonymousCheckout(email, name);
        updateCartsWithGuestForAnonymousCheckout(getCustomerConverter().convert(guestCustomer));

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.TargetCustomerFacade#updateCartWithGuestForAnonymousCheckout(de.hybris.platform.commercefacades.user.data.CustomerData)
     */
    @Override
    public void updateCartsWithGuestForAnonymousCheckout(final CustomerData guestCustomerData) {
        updateCartsWithGuestForAnonymousCheckout(guestCustomerData.getUid());
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.TargetCustomerFacade#updateCartsWithGuestForAnonymousCheckout(java.lang.String)
     */
    @Override
    public void updateCartsWithGuestForAnonymousCheckout(final String uid) {
        if (getCartService().hasSessionCart()) {
            final CartModel sessionCart = getCartService().getSessionCart();
            targetLaybyCommerceCheckoutService.changeCurrentCustomerOnAllCheckoutCarts(sessionCart, getUserService()
                    .getUserForUID(uid));
        }
    }

    @Override
    public void convertGuestToRegisteredUser(final TargetRegisterData registerData) throws DuplicateUidException {
        validateParameterNotNullStandardMessage("registerData", registerData);
        Assert.hasText(registerData.getUid(), "The field [UID] cannot be empty");
        Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
        Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
        Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

        final TargetCustomerModel newCustomer = (TargetCustomerModel)getUserService().getUserForUID(
                registerData.getUid());
        setNames(newCustomer, registerData.getFirstName(), registerData.getLastName());
        if (StringUtils.isNotBlank(registerData.getTitleCode())) {
            final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
            newCustomer.setTitle(title);
        }
        if (StringUtils.isNotBlank(registerData.getMobileNumber())) {
            newCustomer.setMobileNumber(registerData.getMobileNumber());
            newCustomer.setSmsAlert(Boolean.TRUE);
        }
        newCustomer.setType(null);
        newCustomer.setSubscribeToCompetitions(Boolean.TRUE);
        setUidForRegister(registerData, newCustomer);
        newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
        newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
        newCustomer.setSubscribeToEmails(Boolean.TRUE);
        getCustomerAccountService().register(newCustomer, registerData.getPassword());
    }


    @Override
    public void register(final TargetRegisterData registerData) throws DuplicateUidException {
        validateParameterNotNullStandardMessage("registerData", registerData);
        Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
        Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
        Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

        final TargetCustomerModel newCustomer = getModelService().create(TargetCustomerModel.class);
        setNames(newCustomer, registerData.getFirstName(), registerData.getLastName());
        if (StringUtils.isNotBlank(registerData.getTitleCode())) {
            final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
            newCustomer.setTitle(title);
        }
        if (StringUtils.isNotBlank(registerData.getMobileNumber())) {
            newCustomer.setMobileNumber(registerData.getMobileNumber());
            newCustomer.setSmsAlert(Boolean.TRUE);
        }
        newCustomer.setSubscribeToCompetitions(Boolean.TRUE);
        setUidForRegister(registerData, newCustomer);
        newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
        newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
        newCustomer.setSubscribeToEmails(Boolean.TRUE);
        getCustomerAccountService().register(newCustomer, registerData.getPassword());
    }



    @Override
    public void loginSuccess() {
        super.loginSuccess();

        if (!getCartService().hasSessionCart() || getCartService().getSessionCart().getEntries().isEmpty()) {
            try {
                final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
                if (null != WebSessionFunctions.getCurrentHttpSession() && isCartEntryModified) {
                    WebSessionFunctions.getCurrentHttpSession().setAttribute(
                            TgtFacadesConstants.LOGIN_CART_MODIFICATIONS_SESSION_ATTRIBUTE,
                            Boolean.valueOf(isCartEntryModified));
                }
            }
            catch (final CommerceCartRestorationException e) {
                LOG.error("Error while restoring saved cart", e);
            }
        }

        // Also update user in all the purchase option carts
        if (getCartService().hasSessionCart()) {

            final CartModel masterCart = getCartService().getSessionCart();
            final UserModel customer = getUserService().getCurrentUser();
            final CommerceCartService cartService = getCommerceCartService();

            if (cartService instanceof TargetCommerceCartService) {
                masterCart.setUser(customer);
                postLoginCleanup(masterCart, (TargetCustomerModel)customer);
            }

            targetLaybyCommerceCheckoutService.changeCurrentCustomerOnAllCheckoutCarts(masterCart, customer);

        }
        ((TargetCustomerAccountService)getCustomerAccountService()).createTrackingGUID();
        associateUserGroupForUserBasedOnSalesApplication();

        getSessionService().removeAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        getSessionService().removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
    }

    /**
     * Some cleanup post login.
     * 
     * @param masterCart
     * @param customer
     */
    private void postLoginCleanup(final CartModel masterCart, final TargetCustomerModel customer) {
        final AddressModel oldDeliveryAddress = masterCart.getDeliveryAddress();
        if (oldDeliveryAddress != null
                && TargetCustomerModel._TYPECODE.equals(oldDeliveryAddress.getOwner().getItemtype())) {
            if (CollectionUtils.isEmpty(customer.getAddresses())) {
                final AddressModel newDeliveryAddress = getModelService().clone(oldDeliveryAddress);
                newDeliveryAddress.setOwner(customer);
                getModelService().save(newDeliveryAddress);
                masterCart.setDeliveryAddress(newDeliveryAddress);
            }
            final TargetCustomerModel previousUser = (TargetCustomerModel)oldDeliveryAddress.getOwner();
            if (CustomerType.GUEST.equals(previousUser.getType())) {
                removeGuestUser(masterCart, customer, previousUser);
            }
        }
    }

    /**
     * Remove the guest user.
     * 
     * @param masterCart
     * @param customer
     * @param previousUser
     */
    private void removeGuestUser(final CartModel masterCart, final TargetCustomerModel customer,
            final TargetCustomerModel previousUser) {
        getModelService().remove(previousUser);
        if (masterCart.getDeliveryMode() != null) {
            final OrderModel previousOrder = targetOrderService.findLatestOrderForUser(customer);
            final TargetZoneDeliveryModeModel preferredDeliveryMode = targetPrepopulateCheckoutCartFacade
                    .getPreferredDeliveryMode(customer, previousOrder);
            if (preferredDeliveryMode != null && !preferredDeliveryMode.getCode()
                    .equalsIgnoreCase(masterCart.getDeliveryMode().getCode())) {
                masterCart.setDeliveryMode(null);
                masterCart.setDeliveryAddress(null);
            }
        }
    }

    @Override
    public void updateProfile(final CustomerData customerData) throws DuplicateUidException {
        validateDataBeforeUpdate(customerData);

        final TargetCustomerModel customer = (TargetCustomerModel)getCurrentSessionCustomer();
        setNames(customer, customerData.getFirstName(), customerData.getLastName());

        final String titleCode = customerData.getTitleCode();
        if (StringUtils.isNotBlank(titleCode)) {
            customer.setTitle(getUserService().getTitleForCode(customerData.getTitleCode()));
        }
        if (customerData instanceof TargetCustomerData) {
            customer.setAllowTracking(Boolean.valueOf(((TargetCustomerData)customerData).isAllowTracking()));
        }

        getModelService().save(customer);
    }

    private void setNames(final TargetCustomerModel customer, final String firstName, final String lastName) {
        final String trimmedFirstName = StringTools.trimAllWhiteSpaces(firstName);
        final String trimmedLastName = StringTools.trimAllWhiteSpaces(lastName);
        customer.setName(getCustomerNameStrategy().getName(trimmedFirstName, trimmedLastName));
        customer.setFirstname(trimmedFirstName);
        customer.setLastname(trimmedLastName);
    }

    @Override
    protected void validateDataBeforeUpdate(final CustomerData customerData) {
        validateParameterNotNullStandardMessage("customerData", customerData);
        Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
        Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");
        Assert.hasText(customerData.getUid(), "The field [Uid] cannot be empty");
    }

    @Override
    public void invalidateUserSession() {

        JaloSession.getCurrentSession().close();
    }

    @Override
    public TargetCustomerInfoData getCustomerInfo() {
        final TargetCustomerInfoData targetCustomerInfo = new TargetCustomerInfoData();
        final UserModel sessionUser = getUserService().getCurrentUser();
        targetCustomerInfo.setUid(sessionUser.getUid());
        if (!(sessionUser instanceof TargetCustomerModel)) {
            return targetCustomerInfo;
        }
        final TargetCustomerModel customer = (TargetCustomerModel)sessionUser;
        final CartModel sessionCart = getCartService().getSessionCart();
        final UserModel cartUser = (null != sessionCart) ? sessionCart.getUser() : null;
        if (null != cartUser && customer.getUid().equals(cartUser.getUid())) {
            targetCustomerInfo.setFirstName(customer.getFirstname());
            targetCustomerInfo.setLastName(customer.getLastname());
            targetCustomerInfo.setEmail(customer.getContactEmail());
            targetCustomerInfo.setCartGuid(sessionCart.getGuid());
            targetCustomerInfo.setMobileNumber(customer.getMobileNumber());

            if (null != customer.getTitle()) {
                targetCustomerInfo.setTitle(customer.getTitle().getCode());
                targetCustomerInfo.setFormattedTitle(customer.getTitle().getName());
            }
        }

        return targetCustomerInfo;
    }

    @Override
    public void associateUserGroupForUserBasedOnSalesApplication() {
        final UserModel currentUser = getUserService().getCurrentUser();
        if (getUserService().isAnonymousUser(currentUser)) {
            return;
        }
        final SalesApplication salesApplication = salesApplicationFacade.getCurrentSalesApplication();
        final String userGroup = (userGroupForSalesApplication != null && salesApplication != null
                && userGroupForSalesApplication.containsKey(salesApplication.getCode()))
                        ? userGroupForSalesApplication.get(salesApplication.getCode()) : null;
        if (StringUtils.isNotBlank(userGroup)
                && !targetCustomerGroupFacade.isUserMemberOfTheGroup(userGroup)) {
            targetCustomerGroupFacade.addUserToCustomerGroup(userGroup, currentUser);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.TargetCustomerFacade#forgottenPassword(java.lang.String, java.lang.String)
     */
    @Override
    public void forgottenPassword(final String uid, final String redirectPage) {
        Assert.hasText(uid, "The field [uid] cannot be empty");
        final TargetCustomerModel customerModel = getUserService().getUserForUID(uid.toLowerCase(),
                TargetCustomerModel.class);

        if (!StringUtils.isEmpty(redirectPage)) {
            customerModel.setRedirectPage(redirectPage);
        }
        getCustomerAccountService().forgottenPassword(customerModel);
        LOG.info("PASSWORD-RESET: requested for customerPk=" + customerModel.getPk());
    }

    @Override
    public String getUidForPasswordResetToken(final String token) {
        final TargetCustomerModel targetCustomer = ((TargetCustomerAccountService)getCustomerAccountService())
                .getUserForResetPasswordToken(token);
        return targetCustomer.getUid();
    }

    @Override
    public void resetRedirectPageForPasswordResetToken(final String uid) {
        Assert.hasText(uid, "The field [uid] cannot be empty");
        final TargetCustomerModel targetCustomer = getUserService().getUserForUID(uid.toLowerCase(),
                TargetCustomerModel.class);
        if (targetCustomer != null) {
            targetCustomer.setRedirectPage(StringUtils.EMPTY);
            getModelService().save(targetCustomer);
        }
    }

    @Override
    public String getRedirectPageForPasswordResetToken(final String token) {
        final TargetCustomerModel targetCustomer = ((TargetCustomerAccountService)getCustomerAccountService())
                .getUserForResetPasswordToken(token);
        if (targetCustomer != null) {
            return targetCustomer.getRedirectPage();
        }
        return null;
    }

    @Override
    public TargetCustomerModel getCustomerForUsername(final String username) {
        Validate.isTrue(StringUtils.isNotBlank(username), "username must not be blank");

        try {
            return getUserService().getUserForUID(username, TargetCustomerModel.class);
        }
        catch (final UnknownIdentifierException ex) {
            return null;
        }
        catch (final ClassMismatchException ex) {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.customer.TargetCustomerFacade#isPasswordBlacklisted(java.lang.String)
     */
    @Override
    public boolean isPasswordBlacklisted(final String password) {
        return getBlacklistedPasswordFragments().matcher(password).matches();
    }

    @Override
    public String getCartUserEmail() {
        if (!getCartService().hasSessionCart()) {
            return null;
        }
        final CartModel sessionCart = getCartService().getSessionCart();
        final UserModel cartUser = (null != sessionCart) ? sessionCart.getUser() : null;
        if (cartUser instanceof TargetCustomerModel
                && !UserConstants.ANONYMOUS_CUSTOMER_UID.equals(cartUser.getUid())) {
            return CustomerType.GUEST.equals(((TargetCustomerModel)cartUser).getType())
                    ? StringUtils.substringAfter(cartUser.getUid(), "|") : cartUser.getUid();
        }
        return null;
    }

    private Pattern getBlacklistedPasswordFragments() {
        if (blacklistedPasswordRegex != null) {
            return blacklistedPasswordRegex;
        }

        final StringBuilder passwordsRegex = new StringBuilder("^");
        passwordsRegex.append(PASSWORD_REGEX_FRONT);
        passwordsRegex.append("(");
        passwordsRegex.append(configurationService.getConfiguration()
                .getString(BLACKLISTED_PASSWORD_KEY));
        passwordsRegex.append(")");
        passwordsRegex.append(PASSWORD_REGEX_BACK);
        passwordsRegex.append("$");
        blacklistedPasswordRegex = Pattern.compile(passwordsRegex.toString(), Pattern.CASE_INSENSITIVE);

        return blacklistedPasswordRegex;
    }

    /**
     * @param targetLaybyCommerceCheckoutService
     *            the targetLaybyCommerceCheckoutService to set
     */
    @Required
    public void setTargetLaybyCommerceCheckoutService(
            final TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService) {
        this.targetLaybyCommerceCheckoutService = targetLaybyCommerceCheckoutService;
    }

    /**
     * @param targetCartFacade
     *            the targetCartFacade to set
     */
    @Required
    public void setTargetCartFacade(final TargetCartFacade targetCartFacade) {
        this.targetCartFacade = targetCartFacade;
    }

    /**
     * @param userGroupForSalesApplication
     *            the userGroupForSalesApplication to set
     */
    @Required
    public void setUserGroupForSalesApplication(final Map<String, String> userGroupForSalesApplication) {
        this.userGroupForSalesApplication = userGroupForSalesApplication;
    }

    /**
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    /**
     * @param targetCustomerGroupFacade
     *            the targetCustomerGroupFacade to set
     */
    @Required
    public void setTargetCustomerGroupFacade(final TargetCustomerGroupFacade targetCustomerGroupFacade) {
        this.targetCustomerGroupFacade = targetCustomerGroupFacade;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param targetPrepopulateCheckoutCartFacade
     *            the targetPrepopulateCheckoutCartFacade to set
     */
    @Required
    public void setTargetPrepopulateCheckoutCartFacade(
            final TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade) {
        this.targetPrepopulateCheckoutCartFacade = targetPrepopulateCheckoutCartFacade;
    }

}
