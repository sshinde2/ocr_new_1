/**
 * 
 */
package au.com.target.tgtfacades.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;


/**
 * @author asingh78
 * 
 */
public interface TargetCustomerFacade<T extends CustomerData> extends CustomerFacade {

    /**
     * Guest customer which is created for anonymous checkout will be the cart user. The session user will remain
     * anonymous.
     * 
     * @param guestCustomerData
     * @see TargetCustomerFacade#updateCartsWithGuestForAnonymousCheckout(String)
     */
    void updateCartsWithGuestForAnonymousCheckout(CustomerData guestCustomerData);

    /**
     * The user attached to the provided uid will be the cart user. The session user will remain anonymous.
     * 
     * @param uid
     *            The uid to set as the guest user
     */
    void updateCartsWithGuestForAnonymousCheckout(final String uid);

    /**
     * @param registerData
     * @throws DuplicateUidException
     */
    void register(TargetRegisterData registerData) throws DuplicateUidException;

    /**
     * convert guest user to register user
     * 
     * @param registerData
     * @throws DuplicateUidException
     */
    void convertGuestToRegisteredUser(final TargetRegisterData registerData) throws DuplicateUidException;

    /**
     * Invalidate the user session so they get logged out
     */
    void invalidateUserSession();

    /**
     * Gets Basic Customer Information of the current user along with the current cart guid
     */
    TargetCustomerInfoData getCustomerInfo();

    /**
     * Updates the user group for the current non-anonymous user.
     */
    void associateUserGroupForUserBasedOnSalesApplication();

    /**
     * This method will return the uid for a password request token requested.
     * 
     * @param token
     * @return uid
     */
    String getUidForPasswordResetToken(String token);

    /**
     * Returns the saved redirect page when a user resets a password.
     * 
     * @param token
     * @return String
     */
    String getRedirectPageForPasswordResetToken(final String token);

    /**
     * Reset the redirect page for given user token
     * 
     * @param uid
     */
    void resetRedirectPageForPasswordResetToken(final String uid);

    /**
     * Saves the information for redirecting when a user forgets password and wants to change to customer.
     * 
     * @param uid
     * @param redirectPage
     */
    void forgottenPassword(final String uid, final String redirectPage);

    /**
     * Get the customer for the provided username
     * 
     * @param username
     *            The user name to look up
     * @return The customer
     */
    TargetCustomerModel getCustomerForUsername(String username);

    /**
     * Checks whether the provided password is in the list of blacklisted passwords.
     * 
     * @param password
     *            the password to check
     * @return true if blacklisted, false otherwise
     */
    boolean isPasswordBlacklisted(String password);

    /**
     * return the email address from the user on the cart
     * 
     * @return string
     */
    String getCartUserEmail();
}
