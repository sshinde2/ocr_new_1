/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import java.math.BigDecimal;


/**
 * @author mgazal
 *
 */
public class EcommEntry {

    private String baseCode;

    private String baseName;

    private String code;

    private String name;

    private Long quantity;

    private BigDecimal price;

    private String category;

    private String topLevelCategory;

    private String brand;

    private boolean assorted;

    private String departmentCode;

    /**
     * @return the baseCode
     */
    public String getBaseCode() {
        return baseCode;
    }

    /**
     * @param baseCode
     *            the baseCode to set
     */
    public void setBaseCode(final String baseCode) {
        this.baseCode = baseCode;
    }

    /**
     * @return the baseName
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName
     *            the baseName to set
     */
    public void setBaseName(final String baseName) {
        this.baseName = baseName;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the quantity
     */
    public Long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final Long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category
     *            the category to set
     */
    public void setCategory(final String category) {
        this.category = category;
    }

    /**
     * @return the topLevelCategory
     */
    public String getTopLevelCategory() {
        return topLevelCategory;
    }

    /**
     * @param topLevelCategory
     *            the topLevelCategory to set
     */
    public void setTopLevelCategory(final String topLevelCategory) {
        this.topLevelCategory = topLevelCategory;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the assorted
     */
    public boolean isAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final boolean assorted) {
        this.assorted = assorted;
    }

    /**
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /**
     * @param departmentCode
     *            the departmentCode to set
     */
    public void setDepartmentCode(final String departmentCode) {
        this.departmentCode = departmentCode;
    }
}
