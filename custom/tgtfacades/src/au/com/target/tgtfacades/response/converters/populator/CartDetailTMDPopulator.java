/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * @author htan3
 *
 */
public class CartDetailTMDPopulator implements Populator<TargetCartData, CartDetailResponseData> {
    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        target.setTmdNumber(source.getTmdNumber());
    }
}
