package au.com.target.tgtfacades.url.transform;

/**
 * Processes whole URL after it has been composed by
 * {@link de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver}.
 */
public interface UrlTransformer {


    /**
     * Applies transformations to the given {@code url}.
     *
     * @param url the url to transform
     * @return transformed url
     */
    String transform(String url);
}
