package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtfacades.product.data.BrandData;


/**
 * URL resolver for BrandData instances. The pattern could be of the form specified in the TargetBrandModelUrlResolver
 */
public class TargetBrandDataUrlResolver extends AbstractUrlResolver<BrandData>
{
    private static final String CACHE_KEY = TargetBrandDataUrlResolver.class.getName();

    private BrandService brandService;
    private UrlResolver<BrandModel> brandModelUrlResolver;

    protected BrandService getBrandService()
    {
        return brandService;
    }

    @Required
    public void setBrandService(final BrandService brandService)
    {
        this.brandService = brandService;
    }

    protected UrlResolver<BrandModel> getBrandModelUrlResolver()
    {
        return brandModelUrlResolver;
    }

    @Required
    public void setBrandModelUrlResolver(final UrlResolver<BrandModel> brandModelUrlResolver)
    {
        this.brandModelUrlResolver = brandModelUrlResolver;
    }

    @Override
    protected String getKey(final BrandData source)
    {
        return CACHE_KEY + "." + source.getCode();
    }

    @Override
    protected String resolveInternal(final BrandData source)
    {
        final BrandModel brandModel = getBrandService().getBrandForCode(source.getCode());
        return getBrandModelUrlResolver().resolve(brandModel);
    }
}
