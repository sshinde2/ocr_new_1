/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;

import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.Record;


/**
 * @author bhuang3
 *
 */
public class TargetProductRecordSearchUrlResover extends AbstractUrlResolver<Record> {

    private static final String BASE_PRODUCT_PAGE_URL = "/p";
    private static final String SLASH = "/";
    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final Record record) {
        final StringBuilder url = new StringBuilder();
        if (record != null) {
            final Map<String, Attribute> attributeMap = record.getAttributes();
            if (MapUtils.isNotEmpty(attributeMap)) {
                final Attribute colourVariantCodeAttribute = attributeMap
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
                if (colourVariantCodeAttribute != null && StringUtils.isNotEmpty(colourVariantCodeAttribute.toString())) {
                    url.append(BASE_PRODUCT_PAGE_URL);
                    final String colourVariantCode = colourVariantCodeAttribute.toString();
                    final Attribute productNameAttribute = attributeMap
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME);
                    if (productNameAttribute != null && StringUtils.isNotEmpty(productNameAttribute.toString())) {
                        url.append(SLASH).append(urlSafe(productNameAttribute.toString()));
                    }
                    url.append(SLASH).append(colourVariantCode);
                }
            }
        }
        return url.toString();
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }

    /**
     * @return the urlTokenTransformers
     */
    protected List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }
}
