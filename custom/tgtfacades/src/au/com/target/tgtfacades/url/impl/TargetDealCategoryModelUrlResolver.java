/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;


/**
 * @author bhuang3
 * 
 */
public class TargetDealCategoryModelUrlResolver extends AbstractUrlResolver<TargetDealCategoryModel> {


    private static final String PATTERN = "/d/%s/%s";
    private static final String PATTERN_WITHOUT_PAGETITLE = "/d/%s";

    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    private TargetDealService targetDealService;

    @Override
    protected String resolveInternal(final TargetDealCategoryModel model) {
        String url = null;
        final AbstractSimpleDealModel dealModel = targetDealService.getDealByCategory(model);

        if (dealModel != null) {
            final String categoryTitle = model.getCode();
            if (StringUtils.isNotEmpty(dealModel.getFacetTitle())) {
                url = String.format(PATTERN, urlSafe(dealModel.getFacetTitle()), categoryTitle);
            }
            else {
                url = String.format(PATTERN_WITHOUT_PAGETITLE, categoryTitle);
            }
        }
        return url;
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    /**
     * @param urlTokenTransformers
     *            the urlTokenTransformers to set
     */
    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }


    /**
     * @return the urlTokenTransformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    /**
     * @param targetDealService
     *            the targetDealService to set
     */
    @Required
    public void setTargetDealService(final TargetDealService targetDealService) {
        this.targetDealService = targetDealService;
    }




}
