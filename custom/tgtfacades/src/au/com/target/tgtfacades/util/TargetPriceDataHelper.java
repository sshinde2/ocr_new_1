/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author ayushman
 *
 */
public class TargetPriceDataHelper {

    private PriceDataFactory priceDataFactory;

    private CommonI18NService commonI18NService;

    /**
     * Create Price Data for the given price and type
     * 
     * @param price
     * @param priceType
     * @return PriceData
     */
    public PriceData populatePriceData(final Double price, final PriceDataType priceType) {
        if (null != price && null != priceType) {
            return priceDataFactory.create(priceType,
                    BigDecimal.valueOf(price.doubleValue()),
                    commonI18NService.getCurrentCurrency().getIsocode());
        }
        return null;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }


}
