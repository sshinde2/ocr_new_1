/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * @author pthoma20
 *
 */
public class TargetPriceInformation {

    private PriceRangeData priceRangeData;

    private PriceRangeData wasPriceRangeData;

    private PriceData priceData;

    private PriceData wasPriceData;

    private boolean missingPrice;

    private boolean showWasPrice;


    /**
     * @return the priceRangeData
     */
    public PriceRangeData getPriceRangeData() {
        return priceRangeData;
    }

    /**
     * @param priceRangeData
     *            the priceRangeData to set
     */
    public void setPriceRangeData(final PriceRangeData priceRangeData) {
        this.priceRangeData = priceRangeData;
    }

    /**
     * @return the wasPriceRangeData
     */
    public PriceRangeData getWasPriceRangeData() {
        return wasPriceRangeData;
    }

    /**
     * @param wasPriceRangeData
     *            the wasPriceRangeData to set
     */
    public void setWasPriceRangeData(final PriceRangeData wasPriceRangeData) {
        this.wasPriceRangeData = wasPriceRangeData;
    }

    /**
     * @return the priceData
     */
    public PriceData getPriceData() {
        return priceData;
    }

    /**
     * @param priceData
     *            the priceData to set
     */
    public void setPriceData(final PriceData priceData) {
        this.priceData = priceData;
    }

    /**
     * @return the wasPriceData
     */
    public PriceData getWasPriceData() {
        return wasPriceData;
    }

    /**
     * @param wasPriceData
     *            the wasPriceData to set
     */
    public void setWasPriceData(final PriceData wasPriceData) {
        this.wasPriceData = wasPriceData;
    }

    /**
     * @return the missingPrice
     */
    public boolean isMissingPrice() {
        return missingPrice;
    }

    /**
     * @param missingPrice
     *            the missingPrice to set
     */
    public void setMissingPrice(final boolean missingPrice) {
        this.missingPrice = missingPrice;
    }

    /**
     * @return the showWasPrice
     */
    public boolean isShowWasPrice() {
        return showWasPrice;
    }

    /**
     * @param showWasPrice
     *            the showWasPrice to set
     */
    public void setShowWasPrice(final boolean showWasPrice) {
        this.showWasPrice = showWasPrice;
    }
}
