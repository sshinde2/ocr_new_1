/**
 * 
 */
package au.com.target.tgtfacades.assistedcheckout.data;

import de.hybris.platform.commercefacades.user.data.PrincipalData;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author rmcalave
 * 
 */
public class StoreEmployeeData extends PrincipalData {

    private TargetPointOfServiceData store;

    /**
     * @return the store
     */
    public TargetPointOfServiceData getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final TargetPointOfServiceData store) {
        this.store = store;
    }
}
