/**
 * 
 */
package au.com.target.tgtfacades.payment.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtpayment.model.AfterpayConfigModel;


/**
 * @author mgazal
 *
 */
public class AfterpayConfigPopulator implements Populator<AfterpayConfigModel, AfterpayConfigData> {

    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(final AfterpayConfigModel source, final AfterpayConfigData target) throws ConversionException {
        target.setPaymentType(source.getPaymentType());
        target.setDescription(source.getDescription());
        target.setMinimumAmount(priceDataFactory.create(PriceDataType.BUY, source.getMinimumAmount(),
                TgtCoreConstants.AUSTRALIAN_DOLLARS));
        target.setMaximumAmount(priceDataFactory.create(PriceDataType.BUY, source.getMaximumAmount(),
                TgtCoreConstants.AUSTRALIAN_DOLLARS));
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}
