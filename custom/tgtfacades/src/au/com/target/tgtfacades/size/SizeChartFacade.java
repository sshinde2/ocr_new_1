/**
 * 
 */
package au.com.target.tgtfacades.size;

/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface SizeChartFacade {
    /**
     * 
     * @param sizeType
     * @return true if the string match any existing size chart
     */
    boolean isSizeChart(String sizeType);
}
