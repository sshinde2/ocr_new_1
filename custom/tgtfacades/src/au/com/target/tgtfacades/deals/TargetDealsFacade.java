/**
 * 
 */
package au.com.target.tgtfacades.deals;

import au.com.target.tgtfacades.deals.data.TargetDealsData;


/**
 * @author paul
 *
 */
public interface TargetDealsFacade {

    /**
     * This method fetches all the deals which are applicable for mobile along with the offer headings
     * 
     * @return targetDealsData
     */
    public TargetDealsData getAllMobileActiveDealsWithOfferHeadings();

}
