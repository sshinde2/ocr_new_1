package au.com.target.tgtshareddto.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import au.com.target.tgtshareddto.constants.TgtshareddtoConstants;


public class TgtshareddtoManager extends GeneratedTgtshareddtoManager {
    public static final TgtshareddtoManager getInstance()
    {
        final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
        return (TgtshareddtoManager)em.getExtension(TgtshareddtoConstants.EXTENSIONNAME);
    }

}
