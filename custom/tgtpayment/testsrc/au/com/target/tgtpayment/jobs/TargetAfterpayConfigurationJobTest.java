/**
 * 
 */
package au.com.target.tgtpayment.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.service.AfterpayConfigService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAfterpayConfigurationJobTest {

    @InjectMocks
    private final TargetAfterpayConfigurationJob job = new TargetAfterpayConfigurationJob();

    @Mock
    private AfterpayConfigService afterpayConfigService;

    @Mock
    private CronJobModel cronJob;

    @Test
    public void testPerform() {
        final PerformResult result = job.perform(cronJob);
        verify(afterpayConfigService).createOrUpdateAfterpayConfig();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithException() {
        doThrow(new RuntimeException()).when(afterpayConfigService).createOrUpdateAfterpayConfig();
        final PerformResult result = job.perform(cronJob);
        verify(afterpayConfigService).createOrUpdateAfterpayConfig();
        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }

}
