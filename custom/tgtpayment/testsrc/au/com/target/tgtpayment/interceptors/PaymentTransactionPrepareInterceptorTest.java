/**
 * 
 */
package au.com.target.tgtpayment.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentTransactionPrepareInterceptorTest {

    @Mock
    private PaymentTransactionModel pt;
    @Mock
    private CurrencyModel currency;
    @Mock
    private InterceptorContext ctx;
    @InjectMocks
    private final PaymentTransactionPrepareInterceptor interceptor = new PaymentTransactionPrepareInterceptor();

    @Before
    public void setup() {
        BDDMockito.given(pt.getCurrency()).willReturn(currency);
        BDDMockito.given(currency.getDigits()).willReturn(Integer.valueOf(2));
    }

    @Test
    public void testRoundPTEAmount2DecimalPlaces() throws InterceptorException {
        BDDMockito.given(pt.getPlannedAmount()).willReturn(BigDecimal.valueOf(19.600));
        interceptor.onPrepare(pt, ctx);
        Mockito.verify(pt).setPlannedAmount(BigDecimal.valueOf(19.60).setScale(2));
    }

    @Test
    public void testRoundUpPTEAmount() throws InterceptorException {
        BDDMockito.given(pt.getPlannedAmount()).willReturn(BigDecimal.valueOf(19.696));
        interceptor.onPrepare(pt, ctx);
        Mockito.verify(pt).setPlannedAmount(BigDecimal.valueOf(19.70).setScale(2));
    }

    @Test
    public void testRoundDownPTEAmount() throws InterceptorException {
        BDDMockito.given(pt.getPlannedAmount()).willReturn(BigDecimal.valueOf(19.612));
        interceptor.onPrepare(pt, ctx);
        Mockito.verify(pt).setPlannedAmount(BigDecimal.valueOf(19.61).setScale(2));
    }
}
