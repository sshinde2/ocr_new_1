package au.com.target.tgtpayment.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.sql.Date;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;



/**
 * Unit test for {@link PaymentsInProgressServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentsInProgressServiceImplTest {

    @InjectMocks
    private final PaymentsInProgressServiceImpl paymentsInProgressServiceImpl = new PaymentsInProgressServiceImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private CartModel cartModel;

    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;

    @Mock
    private PaymentsInProgressModel paymentsInProgressModel;

    @Before
    public void setUp() {
        BDDMockito.given(modelService.create(PaymentsInProgressModel.class)).willReturn(paymentsInProgressModel);
    }

    @Test
    public void testCreateInProgressPaymentForNullCart() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("abstractOrderModel cannot be null");
        paymentsInProgressServiceImpl.createInProgressPayment(null, paymentTransactionEntryModel,
                PaymentCaptureType.PLACEORDER);
    }

    @Test
    public void testCreateInProgressPaymentForValidCart() {
        final PaymentsInProgressModel result = paymentsInProgressServiceImpl.createInProgressPayment(cartModel,
                paymentTransactionEntryModel, PaymentCaptureType.PLACEORDER);

        Assert.assertNotNull(result);
        Assert.assertEquals(paymentsInProgressModel, result);

        Mockito.verify(modelService).create(PaymentsInProgressModel.class);
        Mockito.verify(result).setAbstractOrder(cartModel);
        Mockito.verify(result).setPaymentInitiatedTime(Mockito.any(Date.class));
        Mockito.verify(result).setPaymentCaptureType(PaymentCaptureType.PLACEORDER);
        Mockito.verify(result).setPaymentTransactionEntry(paymentTransactionEntryModel);
        Mockito.verify(modelService).save(result);
    }

    @Test
    public void testHasInProgressPaymentForNullCart() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("abstractOrderModel cannot be null");
        paymentsInProgressServiceImpl.hasInProgressPayment(null);
    }

    @Test
    public void testHasInProgressPaymentForNonExistingCart() {
        BDDMockito.given(flexibleSearchService.getModelsByExample(Mockito.any(PaymentsInProgressModel.class)))
                .willThrow(new ModelNotFoundException("not found"));

        final boolean result = paymentsInProgressServiceImpl.hasInProgressPayment(cartModel);

        Assert.assertFalse(result);
    }

    @Test
    public void testHasInProgressPaymentForExistingCart() {
        BDDMockito.given(flexibleSearchService.getModelsByExample(Mockito.any(PaymentsInProgressModel.class)))
                .willReturn(Collections.singletonList(paymentsInProgressModel));

        final boolean result = paymentsInProgressServiceImpl.hasInProgressPayment(cartModel);

        Assert.assertTrue(result);
    }

    @Test
    public void testRemoveProgressPaymentForNullCart() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("abstractOrderModel cannot be null");
        paymentsInProgressServiceImpl.removeInProgressPayment(null);
    }

    @Test
    public void testRemoveProgressPaymentForNonExistingCart() {
        BDDMockito.given(flexibleSearchService.getModelsByExample(Mockito.any(PaymentsInProgressModel.class)))
                .willThrow(new ModelNotFoundException("not found"));

        final boolean result = paymentsInProgressServiceImpl.removeInProgressPayment(cartModel);

        Assert.assertFalse(result);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testRemoveProgressPaymentForExistingCart() {
        BDDMockito.given(flexibleSearchService.getModelsByExample(Mockito.any(PaymentsInProgressModel.class)))
                .willReturn(Collections.singletonList(paymentsInProgressModel));

        final boolean result = paymentsInProgressServiceImpl.removeInProgressPayment(cartModel);

        Assert.assertTrue(result);
        Mockito.verify(modelService).remove(paymentsInProgressModel);
    }

}