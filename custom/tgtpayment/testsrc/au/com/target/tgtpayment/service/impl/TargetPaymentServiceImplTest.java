/**
 * 
 */
package au.com.target.tgtpayment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.payment.strategy.TransactionCodeGenerator;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest.TrnType;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetAbstractPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.dto.GiftCardReversalData;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.exceptions.InvalidTransactionRequestException;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;

import com.google.common.collect.ImmutableList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPaymentServiceImplTest {

    private static final String REQUEST_TOKEN = "1234567890_REQUEST_TOKEN";
    private static final String REQUEST_ID = "1234567890_REQUEST_ID";
    private static final String AFTERPAY_REQUEST_ID = "12345_REQUEST_ID";
    private static final String SUBSCRIPTION_ID = "test";
    private static final String PAYPAL_PAYMENT_PROVIDER = "paypal";
    private static final String IPG_PAYMENT_PROVIDER = "ipg";
    private static final String PAYER_ID = "payerId";
    private static final String AUSTRALIAN_DOLLAR = "AUD";
    private static final String CREDIT_CARD_NUMBER = "4589698523654587";
    private final String ipgToken = "KLKDHLFDWE4EREDLIJF";
    private final String afterpayToken = "KLKDHLFDWE4EREDLIJF";
    private final String ipgSessionId = "23132564465";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Spy
    @InjectMocks
    private final TargetPaymentServiceImpl targetPaymentServiceImpl = new TargetPaymentServiceImpl();

    @Mock
    private PaymentMethodStrategy paymentMethodStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private TransactionCodeGenerator transactionCodeGenerator;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private TargetPaymentMethod paymentMethod;

    @Mock
    private PaymentTransactionModel paymentTransactionModel;

    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private PaymentInfoModel paymentInfoModel;

    @Mock
    private TargetCaptureResult captureResult;

    @Mock
    private TargetPaymentAbstractOrderConverterImpl targetPaymentAbstractOrderConverter;

    @Mock
    private EnumerationService enumerationService;

    @Mock
    private IpgPaymentInfoModel ipgPaymentInfo;

    @Mock
    private IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel;

    @Mock
    private IpgGiftCardPaymentInfoModel ipgGiftCardPaymentInfoModel;

    @Mock
    private AbstractOrderModel abstractOrderModel;

    @Mock
    private PaymentTransactionEntryModel captureEntry;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private CartModel cartModel;

    @Mock
    private PaymentsInProgressService paymentsInProgressService;

    @Mock
    private TargetBusinessProcessService businessProcess;

    @Mock
    private AddressModel address;

    @Mock
    private TargetIpgPaymentMethod ipgPaymentMethod;

    @Mock
    private TargetQueryTransactionDetailsResult queryResult;

    @Mock
    private AfterpayPaymentInfoModel afterpayPaymentInfoModel;

    @Mock
    private ZippayPaymentInfoModel zippayPaymentInfoModel;

    private final ArrayList<PaymentTransactionModel> trans = new ArrayList<>();
    private final List<PaymentTransactionEntryModel> entries = new ArrayList<>();

    @Before
    public void setUp() {

        given(transactionCodeGenerator.generateCode(Mockito.anyString())).willReturn("12345678");

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(modelService.create(PaymentTransactionModel.class)).willReturn(paymentTransactionModel);
        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(
                paymentTransactionEntryModel);
        given(modelService.create(IpgCreditCardPaymentInfoModel.class)).willReturn(
                ipgCreditCardPaymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(Mockito.any(PaymentInfoModel.class))).willReturn(
                paymentMethod);
        given(paymentMethodStrategy.getPaymentMethod(Mockito.any(PaymentModeModel.class))).willReturn(
                paymentMethod);
        given(
                paymentMethodStrategy.getPaymentMethod(Mockito.any(PaymentInfoModel.class), Mockito.anyString()))
                .willReturn(paymentMethod);

        given(orderModel.getCurrency()).willReturn(currencyModel);
        given(abstractOrderModel.getUser()).willReturn(customerModel);
        given(currencyModel.getIsocode()).willReturn(AUSTRALIAN_DOLLAR);
        given(orderModel.getUser()).willReturn(customerModel);
        given(orderModel.getCode()).willReturn("order-code");
        given(customerModel.getUid()).willReturn("unittestCustomerUID");
        given(captureEntry.getAmount()).willReturn(BigDecimal.ONE);
        given(captureEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getCode()).willReturn("test_t1");
        given(paymentTransactionModel.getOrder()).willReturn(orderModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.TEN);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        given(ipgPaymentInfo.getBillingAddress()).willReturn(address);
        given(cartModel.getDeliveryAddress()).willReturn(address);
        given(cartModel.getUser()).willReturn(customerModel);
        doReturn(businessProcess).when(targetPaymentServiceImpl).getTargetBusinessProcessService();
        final TitleModel title = mock(TitleModel.class);
        given(address.getTitle()).willReturn(title);
        given(ipgPaymentMethod.queryTransactionDetails(any(TargetQueryTransactionDetailsRequest.class))).willReturn(
                queryResult);
        given(cartModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cartModel.getUser()).willReturn(customerModel);
        given(paymentMethodStrategy.getPaymentMethod("ipg")).willReturn(
                ipgPaymentMethod);

        given(ipgPaymentInfo.getSessionId()).willReturn(ipgSessionId);
        given(ipgPaymentInfo.getToken()).willReturn(ipgToken);

        given(afterpayPaymentInfoModel.getAfterpayToken()).willReturn(afterpayToken);
        trans.clear();
        entries.clear();
    }

    @Test
    public void testHostedSessionToken() {
        final HostedSessionTokenRequest hstr = createHostedSessionTokenRequest();

        final BigDecimal amount = new BigDecimal("10.9");
        final AddressModel addressModel = Mockito.mock(AddressModel.class);
        given(addressModel.getLine1()).willReturn("line1");
        given(addressModel.getLine2()).willReturn("line2");
        final CountryModel countryModel = Mockito.mock(CountryModel.class);
        given(addressModel.getCountry()).willReturn(countryModel);

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
        orderEntries.add(entry);
        orderModel.setEntries(orderEntries);

        given(targetPaymentAbstractOrderConverter.convertAbstractOrderModelToOrder(orderModel, amount, null))
                .willReturn(new Order());

        final TargetCreateSubscriptionResult subscriptionResult = new TargetCreateSubscriptionResult();
        subscriptionResult.setRequestToken(SUBSCRIPTION_ID);
        given(paymentMethod.createSubscription(Mockito.any(TargetCreateSubscriptionRequest.class)))
                .willReturn(subscriptionResult);

        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetPaymentServiceImpl
                .getHostedSessionToken(hstr);
        assertThat(targetCreateSubscriptionResult.getRequestToken()).isEqualTo(SUBSCRIPTION_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCaptureWithNullOrder() {
        targetPaymentServiceImpl.capture(null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCaptureWithNullPaymentInfo() {
        targetPaymentServiceImpl.capture(orderModel, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCaptureWithNullAmount() {
        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, null, currencyModel, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCaptureWithNullCurrency() {
        final BigDecimal amount = BigDecimal.valueOf(10);
        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, amount, null, null);
    }

    @Test
    public void testSuccessfullCapture() {
        final BigDecimal amount = BigDecimal.valueOf(10);

        given(paymentMethod.getPaymentProvider()).willReturn("TNS");

        final TargetCaptureResult result = mock(TargetCaptureResult.class);
        given(result.getRequestId()).willReturn(REQUEST_ID);
        given(result.getRequestToken()).willReturn(REQUEST_TOKEN);
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getTransactionStatusDetails()).willReturn(TransactionStatusDetails.SUCCESFULL);
        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willReturn(
                result);

        final PaymentTransactionEntryModel entry = targetPaymentServiceImpl.capture(orderModel, paymentInfoModel,
                amount, currencyModel, null);

        assertThat(entry).isNotNull();

        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(paymentInfoModel);
        verify(paymentTransactionModel).setOrder(orderModel);
        verify(paymentTransactionModel).setCurrency(currencyModel);
        verify(paymentTransactionModel).setPlannedAmount(amount);
        verify(paymentTransactionModel).setPaymentProvider("TNS");

        verify(paymentTransactionEntryModel).setAmount(amount);
        verify(paymentTransactionEntryModel).setCurrency(currencyModel);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);

        verify(modelService).saveAll(paymentTransactionModel, paymentTransactionEntryModel);


        verify(paymentMethod).capture(Mockito.any(TargetCaptureRequest.class));

        // Verify the transaction entry is updated with the capture result
        verify(paymentTransactionEntryModel).setRequestId(REQUEST_ID);
        verify(paymentTransactionEntryModel).setRequestToken(REQUEST_TOKEN);
        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(paymentTransactionEntryModel).setTransactionStatusDetails(
                TransactionStatusDetails.SUCCESFULL.toString());
        verify(modelService).saveAll(Mockito.anyCollection());
    }

    @Test
    public void testCaptureWithAdapterException() {
        final BigDecimal amount = BigDecimal.valueOf(10);

        given(paymentMethod.getPaymentProvider()).willReturn("TNS");

        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willThrow(
                new AdapterException());

        final PaymentTransactionEntryModel entry = targetPaymentServiceImpl.capture(orderModel, paymentInfoModel,
                amount, currencyModel, null);

        assertThat(entry).isNotNull();

        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(paymentInfoModel);
        verify(paymentTransactionModel).setOrder(orderModel);
        verify(paymentTransactionModel).setCurrency(currencyModel);
        verify(paymentTransactionModel).setPlannedAmount(amount);
        verify(paymentTransactionModel).setPaymentProvider("TNS");

        verify(paymentTransactionEntryModel).setAmount(amount);
        verify(paymentTransactionEntryModel).setCurrency(currencyModel);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);

        verify(modelService).saveAll(paymentTransactionModel, paymentTransactionEntryModel);

        verify(paymentMethod).capture(Mockito.any(TargetCaptureRequest.class));

        // The request token will not be filled in
        verify(paymentTransactionEntryModel, Mockito.never()).setRequestToken(Mockito.anyString());
    }

    @Test
    public void testCaptureWithPaymentException() {
        final BigDecimal amount = BigDecimal.valueOf(10);

        given(paymentMethod.getPaymentProvider()).willReturn("PayPal");

        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willThrow(
                new PaymentException("Bad funding method"));

        try {
            targetPaymentServiceImpl.capture(orderModel, paymentInfoModel,
                    amount, currencyModel, null);
            Assert.fail("PaymentException expected");
        }
        //CHECKSTYLE:OFF
        catch (final PaymentException pe) {
            // Catching the exception to verify the required interactions
        }
        //CHECKSTYLE:ON

        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(paymentInfoModel);
        verify(paymentTransactionModel).setOrder(orderModel);
        verify(paymentTransactionModel).setCurrency(currencyModel);
        verify(paymentTransactionModel).setPlannedAmount(amount);
        verify(paymentTransactionModel).setPaymentProvider("PayPal");

        verify(paymentTransactionEntryModel).setAmount(amount);
        verify(paymentTransactionEntryModel).setCurrency(currencyModel);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);
        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.REVIEW.toString());
        verify(paymentTransactionEntryModel)
                .setTransactionStatusDetails(TransactionStatusDetails.REVIEW_NEEDED.toString());

        verify(modelService).saveAll(paymentTransactionModel, paymentTransactionEntryModel);

        verify(paymentMethod).capture(Mockito.any(TargetCaptureRequest.class));

        // The request token will not be filled in
        verify(paymentTransactionEntryModel, Mockito.never()).setRequestToken(Mockito.anyString());

        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.REJECTED.toString());
        verify(paymentTransactionEntryModel)
                .setTransactionStatusDetails(Mockito.contains("Bad funding method"));
    }

    @Test
    public void testSetAfterpayOrderIdAfterCapture() {
        final BigDecimal amount = BigDecimal.valueOf(10);

        given(paymentMethod.getPaymentProvider()).willReturn("Afterpay");

        final TargetCaptureResult result = Mockito.mock(TargetCaptureResult.class);
        given(result.getRequestId()).willReturn(AFTERPAY_REQUEST_ID);
        given(result.getRequestToken()).willReturn(REQUEST_TOKEN);
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getTransactionStatusDetails()).willReturn(TransactionStatusDetails.SUCCESFULL);
        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willReturn(
                result);

        final AfterpayPaymentInfoModel paymentInfoForOrderModel = mock(AfterpayPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(paymentInfoForOrderModel);
        final PaymentTransactionEntryModel entry = targetPaymentServiceImpl.capture(orderModel,
                afterpayPaymentInfoModel,
                amount, currencyModel, null);

        assertThat(entry).isNotNull();
        verify(paymentInfoForOrderModel).setAfterpayOrderId(AFTERPAY_REQUEST_ID);
        verify(modelService).save(paymentInfoForOrderModel);
        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(afterpayPaymentInfoModel);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);
        verify(paymentMethod).capture(Mockito.any(TargetCaptureRequest.class));

        // Verify the transaction entry is updated with the capture result
        verify(paymentTransactionEntryModel).setRequestId(AFTERPAY_REQUEST_ID);
        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(paymentTransactionEntryModel).setTransactionStatusDetails(
                TransactionStatusDetails.SUCCESFULL.toString());
        verify(modelService).saveAll(Mockito.anyCollection());
    }

    @Test
    public void testSetAfterpayOrderIdWithRequestIdNull() {
        final BigDecimal amount = BigDecimal.valueOf(10);

        given(paymentMethod.getPaymentProvider()).willReturn("Afterpay");

        final TargetCaptureResult result = Mockito.mock(TargetCaptureResult.class);
        given(result.getRequestId()).willReturn(null);
        given(result.getRequestToken()).willReturn(REQUEST_TOKEN);
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getTransactionStatusDetails()).willReturn(TransactionStatusDetails.SUCCESFULL);
        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willReturn(
                result);
        final PaymentTransactionEntryModel entry = targetPaymentServiceImpl.capture(orderModel,
                afterpayPaymentInfoModel,
                amount, currencyModel, null);

        assertThat(entry).isNotNull();

        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(afterpayPaymentInfoModel);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);
        verify(paymentMethod).capture(Mockito.any(TargetCaptureRequest.class));

        // Verify the transaction entry is updated with the capture result
        verify(paymentTransactionEntryModel).setRequestId(null);
        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(paymentTransactionEntryModel).setTransactionStatusDetails(
                TransactionStatusDetails.SUCCESFULL.toString());
        verify(modelService).saveAll(Mockito.anyCollection());
    }

    @Test
    public void testRetryCapture() {
        final PaypalPaymentInfoModel paypalInfo = mock(PaypalPaymentInfoModel.class);
        final CreditCardPaymentInfoModel ccInfo = mock(CreditCardPaymentInfoModel.class);

        final TargetCaptureResult result = mock(TargetCaptureResult.class);
        given(result.getRequestId()).willReturn(REQUEST_ID);
        given(result.getRequestToken()).willReturn(REQUEST_TOKEN);
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getTransactionStatusDetails()).willReturn(TransactionStatusDetails.SUCCESFULL);
        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willReturn(
                result);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getInfo()).willReturn(paymentInfoModel);
        given(paymentTransactionEntryModel.getPaymentTransaction().getOrder()).willReturn(orderModel);
        given(orderModel.getCode()).willReturn("order1");
        given(paymentTransactionModel.getCode()).willReturn("12345678");
        given(paymentTransactionEntryModel.getCurrency()).willReturn(currencyModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(10.0));
        given(paymentTransactionEntryModel.getCode()).willReturn("ptem-code");
        given(paypalInfo.getPayerId()).willReturn("paypal");
        given(ccInfo.getSubscriptionId()).willReturn("creditcard");
        final ArgumentCaptor<TargetCaptureRequest> argumentCaptor = ArgumentCaptor.forClass(TargetCaptureRequest.class);

        targetPaymentServiceImpl.retryCapture(paymentTransactionEntryModel);

        verify(paymentMethod).capture(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getOrderId()).isEqualTo("12345678");
        assertThat(argumentCaptor.getValue().getTotalAmount()).isEqualTo(BigDecimal.valueOf(10.0));
    }

    @Test
    public void testStandaloneRefundCapture() {
        targetPaymentServiceImpl.refundStandaloneCapture(orderModel, paymentInfoModel, "1234");
        verify(targetPaymentServiceImpl).capture(orderModel, paymentInfoModel,
                TargetPaymentServiceImpl.REFUND_STANDALONE_CAPTURE_AMOUNT, currencyModel, null,
                "1234", null);
    }

    @Test
    public void testStandaloneRefund() {

        final BigDecimal amount = new BigDecimal(10.0);

        // Already have a capture
        final PaymentTransactionModel captureTransaction = mock(PaymentTransactionModel.class);

        given(captureEntry.getPaymentTransaction()).willReturn(captureTransaction);
        given(captureTransaction.getInfo()).willReturn(paymentInfoModel);
        given(captureEntry.getCurrency()).willReturn(currencyModel);

        // Refund transaction
        final PaymentTransactionEntryModel refundEntry = mock(PaymentTransactionEntryModel.class);
        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(refundEntry);

        // Refund result
        final TargetExcessiveRefundResult result = mock(TargetExcessiveRefundResult.class);
        given(paymentMethodStrategy.getPaymentMethod(Mockito.eq(paymentInfoModel), Mockito.anyString()))
                .willReturn(paymentMethod);
        given(paymentMethod.excessiveRefund(Mockito.any(TargetExcessiveRefundRequest.class))).willReturn(result);

        doNothing().when(targetPaymentServiceImpl).updateTransactionEntryWithCaptureResult(refundEntry, result);

        final PaymentTransactionEntryModel resultEntry = targetPaymentServiceImpl.refundStandaloneRefund(orderModel,
                captureEntry, amount,
                "1234");
        assertThat(resultEntry).isEqualTo(refundEntry);
        verify(targetPaymentServiceImpl).updateTransactionEntryWithCaptureResult(refundEntry, result);
    }

    @Test
    public void testRefundFollowOn() {
        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        transaction.setRequestToken(REQUEST_TOKEN);

        final PaypalPaymentInfoModel realPaymentInfoModel = new PaypalPaymentInfoModel();
        realPaymentInfoModel.setPayerId(PAYER_ID);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        entries.add(entry);
        transaction.setEntries(entries);

        final BigDecimal amount = new BigDecimal("10.9");

        given(entry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(entry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(entry.getPaymentTransaction()).willReturn(transaction);
        given(entry.getAmount()).willReturn(amount);

        transaction.setPaymentProvider(PAYPAL_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);

        final TargetFollowOnRefundResult result = new TargetFollowOnRefundResult();
        result.setRequestId(REQUEST_ID);
        result.setRequestToken(REQUEST_TOKEN);

        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willReturn(
                result);

        final PaymentTransactionEntryModel entryModel = new PaymentTransactionEntryModel();
        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(entryModel);

        targetPaymentServiceImpl.refundFollowOn(transaction, amount);

        assertThat(entryModel.getType()).isEqualTo(PaymentTransactionType.REFUND_FOLLOW_ON);
        assertThat(entryModel.getRequestId()).isEqualTo(REQUEST_ID);
        assertThat(entryModel.getRequestToken()).isEqualTo(REQUEST_TOKEN);

    }

    @Test
    public void testRefundFollowOnForAfterpay() {
        final PaymentTransactionEntryModel refundEntry = mock(PaymentTransactionEntryModel.class);
        given(refundEntry.getAmount()).willReturn(BigDecimal.TEN);
        given(refundEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        final AfterpayPaymentInfoModel afterpayPaymentInfo = mock(AfterpayPaymentInfoModel.class);
        given(paymentTransactionModel.getInfo()).willReturn(afterpayPaymentInfo);
        final TargetFollowOnRefundResult result = mock(TargetFollowOnRefundResult.class);
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getTotalAmount()).willReturn(BigDecimal.TEN);
        given(result.getRequestId()).willReturn(REQUEST_ID);
        given(result.getReconciliationId()).willReturn("1234234");

        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willReturn(
                result);

        targetPaymentServiceImpl.executeFollowOnRefund(paymentTransactionEntryModel,
                paymentTransactionEntryModel.getPaymentTransaction(), refundEntry);
        verify(refundEntry).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(refundEntry).setAmount(BigDecimal.TEN);
        verify(refundEntry).setReceiptNo("1234234");
        verify(afterpayPaymentInfo).setAfterpayRefundId("1234234");
        verify(afterpayPaymentInfo).setAfterpayRefundRequestId(REQUEST_ID);
        verify(modelService).saveAll(Arrays.asList(refundEntry, afterpayPaymentInfo));
    }

    @Test
    public void testRefundFollowOnForAfterpayRejected() {
        final PaymentTransactionEntryModel refundEntry = mock(PaymentTransactionEntryModel.class);
        given(refundEntry.getAmount()).willReturn(BigDecimal.TEN);
        given(refundEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        final AfterpayPaymentInfoModel afterpayPaymentInfo = mock(AfterpayPaymentInfoModel.class);
        given(paymentTransactionModel.getInfo()).willReturn(afterpayPaymentInfo);
        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class)))
                .willThrow(new AdapterException());

        targetPaymentServiceImpl.executeFollowOnRefund(paymentTransactionEntryModel,
                paymentTransactionEntryModel.getPaymentTransaction(), refundEntry);
        verify(refundEntry).setTransactionStatusDetails(Mockito.anyString());
        verify(modelService).save(refundEntry);
        verify(targetPaymentServiceImpl, never()).updateTransactionEntryWithCaptureResult(
                Mockito.any(PaymentTransactionEntryModel.class), Mockito.any(TargetAbstractPaymentResult.class));
    }


    @Test
    public void testRefundFollowOnForZip() {
        final PaymentTransactionEntryModel refundEntry = mock(PaymentTransactionEntryModel.class);
        given(refundEntry.getAmount()).willReturn(BigDecimal.TEN);
        given(refundEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        final ZippayPaymentInfoModel zipPaymentInfo = mock(ZippayPaymentInfoModel.class);
        given(paymentTransactionModel.getInfo()).willReturn(zipPaymentInfo);
        final TargetFollowOnRefundResult result = mock(TargetFollowOnRefundResult.class);
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getReconciliationId()).willReturn("rf_H5J24ZnqpKMByItvF2Jh85");

        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willReturn(
                result);

        targetPaymentServiceImpl.executeFollowOnRefund(paymentTransactionEntryModel,
                paymentTransactionEntryModel.getPaymentTransaction(), refundEntry);
        verify(refundEntry).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(refundEntry).setReceiptNo("rf_H5J24ZnqpKMByItvF2Jh85");
        verify(zipPaymentInfo).setRefundId("rf_H5J24ZnqpKMByItvF2Jh85");
        verify(modelService).saveAll(Arrays.asList(refundEntry, zipPaymentInfo));
    }

    @Test
    public void testRefundFollowOnForZipRejected() {
        final PaymentTransactionEntryModel refundEntry = mock(PaymentTransactionEntryModel.class);
        given(refundEntry.getAmount()).willReturn(BigDecimal.TEN);
        given(refundEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        final ZippayPaymentInfoModel zipPaymentInfo = mock(ZippayPaymentInfoModel.class);
        given(paymentTransactionModel.getInfo()).willReturn(zipPaymentInfo);
        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class)))
                .willThrow(new AdapterException());

        targetPaymentServiceImpl.executeFollowOnRefund(paymentTransactionEntryModel,
                paymentTransactionEntryModel.getPaymentTransaction(), refundEntry);
        verify(refundEntry).setTransactionStatusDetails(Mockito.anyString());
        verify(modelService).save(refundEntry);
        verify(targetPaymentServiceImpl, never()).updateTransactionEntryWithCaptureResult(
                Mockito.any(PaymentTransactionEntryModel.class), Mockito.any(TargetAbstractPaymentResult.class));
    }

    private PaymentInfoModel createPaymentInfoModel(final long num, final boolean isGiftcard) {

        final PaymentInfoModel paymentInfor;
        if (isGiftcard) {
            paymentInfor = mock(IpgGiftCardPaymentInfoModel.class);
        }
        else {
            paymentInfor = mock(PaymentInfoModel.class);
        }

        given(paymentInfor.getPk()).willReturn(PK.createFixedCounterPK(1, num));
        return paymentInfor;
    }

    private PaymentTransactionEntryModel setUpPaymentTransactionEntry(final PaymentTransactionModel transaction,
            final BigDecimal amount,
            final PaymentTransactionType transactionType, final TransactionStatus transactionStatus,
            final PaymentInfoModel paymentInfo) {
        final PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);
        given(paymentTransactionEntry.getPaymentTransaction()).willReturn(transaction);
        given(paymentTransactionEntry.getIpgPaymentInfo()).willReturn(paymentInfo);
        given(paymentTransactionEntry.getType()).willReturn(transactionType);
        if (transactionStatus != null) {
            given(paymentTransactionEntry.getTransactionStatus()).willReturn(transactionStatus.toString());
        }
        given(paymentTransactionEntry.getAmount()).willReturn(amount);

        return paymentTransactionEntry;
    }

    @Test
    public void testRefundFollowOnWithMutipleCards() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount5 = BigDecimal.valueOf(14.21d).setScale(2);
        final BigDecimal amount6 = BigDecimal.valueOf(3.48d).setScale(2);
        final BigDecimal amount7 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount8 = BigDecimal.valueOf(5.21d).setScale(2);
        final BigDecimal amount9 = BigDecimal.valueOf(1.01d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);
        final PaymentInfoModel paymentInfor5 = createPaymentInfoModel(5, true);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, paymentInfor1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, paymentInfor2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionCapture5 = setUpPaymentTransactionEntry(
                paymentTransaction, amount5,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(paymentTransaction,
                amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(paymentTransaction,
                amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(paymentTransaction,
                amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(paymentTransaction,
                amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);


        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture5, paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3,
                        paymentTransactionCapture4, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(ipgPaymentInfoModel);

        final BigDecimal totalAmount = BigDecimal.valueOf(28.00d).setScale(2);

        final TargetFollowOnRefundResult refundResult = new TargetFollowOnRefundResult();
        refundResult.setRequestId(REQUEST_ID);
        refundResult.setRequestToken(REQUEST_TOKEN);

        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willReturn(
                refundResult);

        final PaymentTransactionEntryModel entryModel = new PaymentTransactionEntryModel();
        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(entryModel);

        final List<PaymentTransactionEntryModel> resultList = targetPaymentServiceImpl.refundFollowOn(
                paymentTransaction, totalAmount);
        assertThat(resultList.size()).isEqualTo(1);
        assertThat(resultList.get(0).getType()).isEqualTo(PaymentTransactionType.REFUND_FOLLOW_ON);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRefundFollowOnWithEmptyAmount() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        targetPaymentServiceImpl.refundFollowOn(paymentTransaction, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRefundFollowOnWithEmptyTransaction() {
        final PaymentTransactionModel transaction = null;
        targetPaymentServiceImpl.refundFollowOn(transaction, BigDecimal.TEN);
    }

    private void prepareForRefundFollowOn(final PaymentTransactionEntryModel entry, final BigDecimal capturedAmount,
            final PaymentTransactionEntryModel returnEntry) {
        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        final IpgPaymentInfoModel realPaymentInfoModel = new IpgPaymentInfoModel();

        entries.add(entry);
        transaction.setRequestToken(REQUEST_TOKEN);
        transaction.setEntries(entries);
        transaction.setPaymentProvider(IPG_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);
        transaction.setOrder(orderModel);


        given(entry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(entry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(entry.getPaymentTransaction()).willReturn(transaction);
        given(entry.getAmount()).willReturn(capturedAmount);
        given(entry.getPaymentTransaction()).willReturn(transaction);

        given(paymentMethodStrategy.getPaymentMethod(realPaymentInfoModel, transaction.getPaymentProvider()))
                .willReturn(paymentMethod);

        final TargetFollowOnRefundResult result = new TargetFollowOnRefundResult();
        result.setRequestId(REQUEST_ID);
        result.setRequestToken(REQUEST_TOKEN);

        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willReturn(
                result);

        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(returnEntry);
    }

    @Test
    public void testRefundFollowOnBaseOnPaymentTransactionEntryModel() {
        final PaymentTransactionEntryModel entry = Mockito.mock(PaymentTransactionEntryModel.class);
        final BigDecimal amount = new BigDecimal("10.9");
        final PaymentTransactionEntryModel returnEntryModel = new PaymentTransactionEntryModel();
        prepareForRefundFollowOn(entry, amount, returnEntryModel);
        targetPaymentServiceImpl.refundFollowOn(entry, amount);

        assertThat(returnEntryModel.getType()).isEqualTo(PaymentTransactionType.REFUND_FOLLOW_ON);
        assertThat(returnEntryModel.getRequestId()).isEqualTo(REQUEST_ID);
        assertThat(returnEntryModel.getRequestToken()).isEqualTo(REQUEST_TOKEN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRefundFollowOnBaseOnPaymentTransactionEntryModelWithEmptyAmount() {
        final PaymentTransactionEntryModel entry = Mockito.mock(PaymentTransactionEntryModel.class);
        final BigDecimal amount = null;
        final PaymentTransactionEntryModel returnEntryModel = new PaymentTransactionEntryModel();
        prepareForRefundFollowOn(entry, amount, returnEntryModel);
        targetPaymentServiceImpl.refundFollowOn(entry, amount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRefundFollowOnBaseOnPaymentTransactionEntryModelWithEmptyPaymentTransactionEntryModel() {
        PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final BigDecimal amount = new BigDecimal("10.9");
        final PaymentTransactionEntryModel returnEntryModel = new PaymentTransactionEntryModel();
        prepareForRefundFollowOn(entry, amount, returnEntryModel);
        entry = null;
        targetPaymentServiceImpl.refundFollowOn(entry, amount);
    }

    @Test(expected = PaymentException.class)
    public void testRefundFollowOnBaseOnPaymentTransactionEntryModelWithAmountMoreThanCaptured() {
        final PaymentTransactionEntryModel entry = Mockito.mock(PaymentTransactionEntryModel.class);
        final BigDecimal capturedAmount = new BigDecimal("10.9");
        final PaymentTransactionEntryModel returnEntryModel = new PaymentTransactionEntryModel();
        prepareForRefundFollowOn(entry, capturedAmount, returnEntryModel);
        final BigDecimal refundAmount = new BigDecimal("10.99");
        targetPaymentServiceImpl.refundFollowOn(entry, refundAmount);
    }

    @Test
    public void testRefundFollowOnBaseOnPaymentTransactionEntryModelWithErrorWhileRefund() {
        final BigDecimal capturedAmount = new BigDecimal("10.9");
        final PaymentTransactionEntryModel returnEntryModel = new PaymentTransactionEntryModel();
        prepareForRefundFollowOn(paymentTransactionEntryModel, capturedAmount, returnEntryModel);
        final AdapterException ae = new AdapterException("test message");
        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willThrow(ae);
        final BigDecimal refundAmount = new BigDecimal("10.8");
        final PaymentTransactionEntryModel resultEntry = targetPaymentServiceImpl.refundFollowOn(
                paymentTransactionEntryModel, refundAmount);
        assertThat(resultEntry.getTransactionStatusDetails())
                .isEqualTo("Follow on refund failed for order order-code : " + ae.getMessage());
    }

    @Test
    public void testRefundFollowOnBaseOnPaymentTransactionEntryModelWithErrorWhileRefundNoOrder() {
        final BigDecimal capturedAmount = new BigDecimal("10.9");
        final PaymentTransactionEntryModel returnEntryModel = new PaymentTransactionEntryModel();
        prepareForRefundFollowOn(paymentTransactionEntryModel, capturedAmount, returnEntryModel);
        paymentTransactionEntryModel.getPaymentTransaction().setOrder(null);
        final AdapterException ae = new AdapterException("test message");
        given(paymentMethod.followOnRefund(Mockito.any(TargetFollowOnRefundRequest.class))).willThrow(ae);
        final BigDecimal refundAmount = new BigDecimal("10.8");
        final PaymentTransactionEntryModel resultEntry = targetPaymentServiceImpl.refundFollowOn(
                paymentTransactionEntryModel, refundAmount);
        assertThat(resultEntry.getTransactionStatusDetails())
                .isEqualTo("Follow on refund failed for order null : " + ae.getMessage());
    }

    @Test
    public void testGetPaymentTransactionEntry() {
        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        final PaypalPaymentInfoModel realPaymentInfoModel = new PaypalPaymentInfoModel();
        transaction.setPaymentProvider(PAYPAL_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();
        entry.setCode("abc");
        entry.setPaymentTransaction(transaction);

        given(paymentMethodStrategy.getPaymentMethod(realPaymentInfoModel, transaction.getPaymentProvider()))
                .willReturn(paymentMethod);

        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();
        result.setRequestId(REQUEST_ID);
        result.setRequestToken(REQUEST_TOKEN);

        given(paymentMethod.retrieveTransaction(Mockito.any(TargetRetrieveTransactionRequest.class)))
                .willReturn(
                        result);

        targetPaymentServiceImpl.retrieveTransactionEntry(entry);

        assertThat(entry.getRequestId()).isEqualTo(REQUEST_ID);
        assertThat(entry.getRequestToken()).isEqualTo(REQUEST_TOKEN);
    }

    @Test
    public void testGetPaymentTransactionEntryWithNoResult() {
        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        final PaypalPaymentInfoModel realPaymentInfoModel = new PaypalPaymentInfoModel();
        transaction.setPaymentProvider(PAYPAL_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();
        entry.setCode("abc");
        entry.setPaymentTransaction(transaction);

        given(paymentMethodStrategy.getPaymentMethod(realPaymentInfoModel, transaction.getPaymentProvider()))
                .willReturn(paymentMethod);
        given(paymentMethod.retrieveTransaction(Mockito.any(TargetRetrieveTransactionRequest.class)))
                .willReturn(null);

        targetPaymentServiceImpl.retrieveTransactionEntry(entry);
        verifyZeroInteractions(modelService);
    }

    @Test(expected = InvalidTransactionRequestException.class)
    public void testGetPaymentTransactionEntryWithAdapterException() {
        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        final PaypalPaymentInfoModel realPaymentInfoModel = new PaypalPaymentInfoModel();
        transaction.setPaymentProvider(PAYPAL_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();
        entry.setCode("abc");
        entry.setPaymentTransaction(transaction);

        given(paymentMethodStrategy.getPaymentMethod(realPaymentInfoModel, transaction.getPaymentProvider()))
                .willReturn(paymentMethod);
        final AdapterException adapterException = new AdapterException("test");
        given(paymentMethod.retrieveTransaction(Mockito.any(TargetRetrieveTransactionRequest.class)))
                .willThrow(adapterException);

        targetPaymentServiceImpl.retrieveTransactionEntry(entry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTokenizeWithNullSessionToken() {
        targetPaymentServiceImpl.tokenize(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTokenizeWithNullPaymentMode() {
        targetPaymentServiceImpl.tokenize("sessiontoken", null);
    }

    @Test
    public void testTokenizeWithAdapterException() {
        final String sessionToken = "sessionToken";
        final PaymentModeModel paymentMode = mock(PaymentModeModel.class);

        given(paymentMethod.tokenize(Mockito.any(TargetTokenizeRequest.class)))
                .willThrow(new AdapterException());

        final String result = targetPaymentServiceImpl.tokenize(sessionToken, paymentMode);

        assertThat(result).isNull();
    }

    @Test
    public void testTokenize() {
        final String sessionToken = "sessionToken";
        final PaymentModeModel paymentMode = Mockito.mock(PaymentModeModel.class);

        final TargetTokenizeResult tokenizeResult = Mockito.mock(TargetTokenizeResult.class);
        given(tokenizeResult.getSavedToken()).willReturn("12345678");

        given(paymentMethod.tokenize(Mockito.any(TargetTokenizeRequest.class)))
                .willReturn(tokenizeResult);

        final String result = targetPaymentServiceImpl.tokenize(sessionToken, paymentMode);

        assertThat(result).isNotNull();

        final ArgumentCaptor<TargetTokenizeRequest> tokenizeRequest = ArgumentCaptor
                .forClass(TargetTokenizeRequest.class);
        verify(paymentMethod).tokenize(tokenizeRequest.capture());

        assertThat(tokenizeRequest.getValue().getSessionToken()).isEqualTo(sessionToken);
        assertThat(result).isEqualTo("12345678");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreatePaymentTransactionEntry() {

        final BigDecimal amount = new BigDecimal("10.5");
        final PaymentInfoModel paymentInfo = Mockito.mock(PaymentInfoModel.class);
        final CurrencyModel currency = Mockito.mock(CurrencyModel.class);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(modelService.create(PaymentTransactionModel.class)).willReturn(paymentTransactionModel);
        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(paymentTransactionEntryModel);
        given(orderModel.getTotalPrice()).willReturn(amount.doubleValue());
        given(orderModel.getPaymentInfo()).willReturn(paymentInfo);
        given(orderModel.getCurrency()).willReturn(currency);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfo)).willReturn(ipgPaymentMethod);
        given(ipgPaymentMethod.getPaymentProvider()).willReturn("IPG");

        final PaymentTransactionEntryModel entry = targetPaymentServiceImpl.createPaymentTransactionEntry(orderModel);

        assertThat(entry).isNotNull();

        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(paymentInfo);
        verify(paymentTransactionModel).setOrder(orderModel);
        verify(paymentTransactionModel).setPlannedAmount(amount);
        verify(paymentTransactionModel).setPaymentProvider("IPG");

        verify(paymentTransactionEntryModel).setAmount(amount);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);

        verify(modelService, times(1)).save(paymentTransactionModel);
    }

    @Test
    public void testCreateCaptureRequestForPayPal() {
        final String orderId = "123456";
        final BigDecimal amount = new BigDecimal("10");
        final CurrencyModel currency = Mockito.mock(CurrencyModel.class);
        final String paypalToken = "KLKDHLFDWE4EREDLIJF";
        final String paypalPayerId = "4584LKDHFLD";

        given(currency.getIsocode()).willReturn(AUSTRALIAN_DOLLAR);

        final Order order = Mockito.mock(Order.class);
        final PaypalPaymentInfoModel paypalPaymentInfo = Mockito.mock(PaypalPaymentInfoModel.class);

        given(paypalPaymentInfo.getToken()).willReturn(paypalToken);
        given(paypalPaymentInfo.getPayerId()).willReturn(paypalPayerId);

        final TargetCaptureRequest captureRequest = targetPaymentServiceImpl.createCaptureRequest(orderId, amount,
                currency, order, paypalPaymentInfo, abstractOrderModel,
                paymentTransactionEntryModel);

        assertThat(captureRequest).isNotNull();
        assertThat(captureRequest.getOrderId()).isEqualTo(orderId);
        assertThat(captureRequest.getOrder()).isEqualTo(order);
        assertThat(captureRequest.getTotalAmount()).isEqualTo(amount);
        assertThat(captureRequest.getToken()).isEqualTo(paypalToken);
        assertThat(captureRequest.getPayerId()).isEqualTo(paypalPayerId);
        assertThat(captureRequest.getSessionId()).isNull();

    }

    @Test
    public void testCreateCaptureRequestForTns() {
        final String orderId = "123456";
        final BigDecimal amount = new BigDecimal("10");
        final CurrencyModel currency = mock(CurrencyModel.class);
        final String tnsToken = "KLKDHLFDWE4EREDLIJF";

        given(currency.getIsocode()).willReturn(AUSTRALIAN_DOLLAR);

        final Order order = mock(Order.class);
        final CreditCardPaymentInfoModel creditCardPaymentInfo = mock(CreditCardPaymentInfoModel.class);

        given(creditCardPaymentInfo.getSubscriptionId()).willReturn(tnsToken);

        final TargetCaptureRequest captureRequest = targetPaymentServiceImpl.createCaptureRequest(orderId, amount,
                currency, order, creditCardPaymentInfo, abstractOrderModel,
                paymentTransactionEntryModel);

        assertThat(captureRequest).isNotNull();
        assertThat(captureRequest.getOrderId()).isEqualTo(orderId);
        assertThat(captureRequest.getOrder()).isEqualTo(order);
        assertThat(captureRequest.getTotalAmount()).isEqualTo(amount);
        assertThat(captureRequest.getToken()).isEqualTo(tnsToken);
        assertThat(captureRequest.getPayerId()).isNull();
        assertThat(captureRequest.getSessionId()).isNull();
    }

    @Test
    public void testCreateCaptureRequestForIpg() {
        final String orderId = "123456";
        final BigDecimal amount = new BigDecimal("10");
        final CurrencyModel currency = mock(CurrencyModel.class);

        given(currency.getIsocode()).willReturn(AUSTRALIAN_DOLLAR);

        final Order order = mock(Order.class);

        given(abstractOrderModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(ipgPaymentInfo.getSessionId()).willReturn(ipgSessionId);
        given(ipgPaymentInfo.getToken()).willReturn(ipgToken);

        final TargetCardResult ipgCardResult = mock(TargetCardResult.class);
        doReturn(ipgCardResult).when(targetPaymentServiceImpl).getIpgCardResult(paymentTransactionEntryModel);

        final TargetCaptureRequest captureRequest = targetPaymentServiceImpl.createCaptureRequest(orderId, amount,
                currency, order, ipgPaymentInfo, abstractOrderModel,
                paymentTransactionEntryModel);

        assertThat(captureRequest).isNotNull();
        assertThat(captureRequest.getOrderId()).isEqualTo(orderId);
        assertThat(captureRequest.getOrder()).isEqualTo(order);
        assertThat(captureRequest.getTotalAmount()).isEqualTo(amount);
        assertThat(captureRequest.getToken()).isEqualTo(ipgToken);
        assertThat(captureRequest.getPayerId()).isNull();
        assertThat(captureRequest.getSessionId()).isEqualTo(ipgSessionId);
        assertThat(captureRequest.getCardResult()).isNotNull();
    }

    @Test
    public void testGetIpgCardResultForCreditCard() {
        final String ccNumber = "465498713465";
        final String ccType = "VISA";
        final String ccToken = "1231654";
        final String ccValidToMonth = "03";
        final String ccValidToYear = "16";
        final String ccExpiry = "03/16";
        final String ccBin = "34";
        final String ccPromoId = "123";
        final String ccTokenExpiry = "01/01/2016";


        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgCreditCardPaymentInfoModel);
        given(ipgCreditCardPaymentInfoModel.getNumber()).willReturn(ccNumber);
        given(ipgCreditCardPaymentInfoModel.getType()).willReturn(CreditCardType.valueOf(ccType));
        given(ipgCreditCardPaymentInfoModel.getToken()).willReturn(ccToken);
        given(ipgCreditCardPaymentInfoModel.getTokenExpiry()).willReturn(ccTokenExpiry);
        given(ipgCreditCardPaymentInfoModel.getValidToMonth()).willReturn(ccValidToMonth);
        given(ipgCreditCardPaymentInfoModel.getValidToYear()).willReturn(ccValidToYear);
        given(ipgCreditCardPaymentInfoModel.getBin()).willReturn(ccBin);
        given(ipgCreditCardPaymentInfoModel.getPromoId()).willReturn(ccPromoId);

        final TargetCardResult ipgCardResult = targetPaymentServiceImpl.getIpgCardResult(paymentTransactionEntryModel);

        assertThat(ipgCardResult).isNotNull();
        assertThat(ipgCardResult.getCardNumber()).isEqualTo(ccNumber);
        assertThat(ipgCardResult.getCardType().toString().toUpperCase()).isEqualTo(ccType);
        assertThat(ipgCardResult.getToken()).isEqualTo(ccToken);
        assertThat(ipgCardResult.getTokenExpiry()).isEqualTo(ccTokenExpiry);
        assertThat(ipgCardResult.getCardExpiry()).isEqualTo(ccExpiry);
        assertThat(ipgCardResult.getBin()).isEqualTo(ccBin);
        assertThat(ipgCardResult.getPromoId()).isEqualTo(ccPromoId);

    }

    @Test
    public void testGetIpgCardResultForGiftCard() {
        final String giftCardNumber = "627345*******0002";
        final String bin = "627345";

        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgGiftCardPaymentInfoModel);
        given(ipgGiftCardPaymentInfoModel.getMaskedNumber()).willReturn(giftCardNumber);
        given(ipgGiftCardPaymentInfoModel.getCardType()).willReturn(CreditCardType.GIFTCARD);
        given(ipgGiftCardPaymentInfoModel.getBin()).willReturn(bin);

        final TargetCardResult ipgCardResult = targetPaymentServiceImpl.getIpgCardResult(paymentTransactionEntryModel);

        assertThat(ipgCardResult).isNotNull();
        assertThat(ipgCardResult.getCardNumber()).isEqualTo(giftCardNumber);
        assertThat(ipgCardResult.getCardType()).isEqualTo(CreditCardType.GIFTCARD.getCode());
        assertThat(ipgCardResult.getBin()).isEqualTo(bin);

    }

    @Test
    public void testCreateIpgCreditCardPaymentInfoModel() {
        final IpgCreditCardPaymentInfoModel ipgCreditCard = new IpgCreditCardPaymentInfoModel();

        given(modelService.create(IpgCreditCardPaymentInfoModel.class)).willReturn(
                ipgCreditCard);

        final String cardNumber = "53454***3432";
        final String cardType = "VISA";
        final String cardExpiry = "03/17";
        final String token = "123456";
        final String uId = "123133";
        final String bin = "34";
        final String tokenExpiry = "01/01/2016";

        given(customerModel.getUid()).willReturn(uId);

        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardNumber(cardNumber);
        ipgCardResult.setCardType(cardType);
        ipgCardResult.setCardExpiry(cardExpiry);
        ipgCardResult.setToken(token);
        ipgCardResult.setBin(bin);
        ipgCardResult.setTokenExpiry(tokenExpiry);
        ipgCardResult.setDefaultCard(true);

        given(enumerationService.getEnumerationValue(CreditCardType.class.getSimpleName(), cardType))
                .willReturn(CreditCardType.VISA);

        final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModelReturned = targetPaymentServiceImpl
                .createIpgCreditCardPaymentInfoModel(orderModel, ipgCardResult);

        assertThat(ipgCreditCardPaymentInfoModelReturned).isNotNull();
        assertThat(ipgCreditCardPaymentInfoModelReturned.getNumber()).isEqualTo(cardNumber);
        assertThat(ipgCreditCardPaymentInfoModelReturned.getType()).isEqualTo(CreditCardType.VISA);
        assertThat(ipgCreditCardPaymentInfoModelReturned.getValidToMonth()).isEqualTo("03");
        assertThat(ipgCreditCardPaymentInfoModelReturned.getValidToYear()).isEqualTo("17");
        assertThat(ipgCreditCardPaymentInfoModelReturned.getToken()).isEqualTo(token);
        assertThat(ipgCreditCardPaymentInfoModelReturned.getTokenExpiry()).isEqualTo(tokenExpiry);
        assertThat(ipgCreditCardPaymentInfoModelReturned.getBin()).isEqualTo(bin);
        assertThat(ipgCreditCardPaymentInfoModelReturned.getCcOwner()).isEqualTo("");
        assertThat(ipgCreditCardPaymentInfoModelReturned.getDefaultCard().booleanValue()).isTrue();

    }

    @Test
    public void testCreateIpgGiftCardPaymentInfoModel() {
        final IpgGiftCardPaymentInfoModel ipgGiftCard = new IpgGiftCardPaymentInfoModel();

        given(modelService.create(IpgGiftCardPaymentInfoModel.class)).willReturn(
                ipgGiftCard);

        final String cardNumber = "627345*******0002";
        final String cardType = "giftcard";
        final String uId = "123133";
        final String bin = "627345";
        given(customerModel.getUid()).willReturn(uId);

        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardNumber(cardNumber);
        ipgCardResult.setCardType(cardType);
        ipgCardResult.setBin(bin);

        given(enumerationService.getEnumerationValue(CreditCardType.class.getSimpleName(), cardType))
                .willReturn(CreditCardType.GIFTCARD);

        final IpgGiftCardPaymentInfoModel ipgGiftCardPaymentInfoModelReturned = targetPaymentServiceImpl
                .createIpgGiftCardPaymentInfoModel(orderModel, ipgCardResult);

        assertThat(ipgGiftCardPaymentInfoModelReturned).isNotNull();
        assertThat(ipgGiftCardPaymentInfoModelReturned.getMaskedNumber()).isEqualTo(cardNumber);
        assertThat(ipgGiftCardPaymentInfoModelReturned.getCardType()).isEqualTo(CreditCardType.GIFTCARD);
        assertThat(ipgGiftCardPaymentInfoModelReturned.getBin()).isEqualTo(bin);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreatePaymentTransactionWithQueryResult() {

        final TargetQueryTransactionDetailsResult queryTransactionResult = new TargetQueryTransactionDetailsResult();
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        queryTransactionResult.addCardResult(cardResult);
        queryTransactionResult.addCardResult(cardResult);
        final List<TargetCardResult> savedCards = Collections.singletonList(cardResult);
        given(cardResult.isDefaultCard()).willReturn(true);
        queryTransactionResult.setSavedCards(savedCards);
        queryTransactionResult.setSuccess(true);

        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);
        given(cardResult.getCardType()).willReturn(CreditCardType.VISA.getCode());

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);

        final PaymentTransactionModel curTransaction = targetPaymentServiceImpl.createTransactionWithQueryResult(
                abstractOrderModel,
                queryTransactionResult);

        assertThat(curTransaction).isEqualTo(paymentTransactionModel);
        verify(paymentTransactionModel).setOrder(abstractOrderModel);
        verify(targetPaymentServiceImpl, times(2)).saveCardResult(abstractOrderModel, cardResult,
                paymentTransactionEntryModel);
        verify(modelService, times(2)).save(paymentTransactionModel);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePaymentTransactionWithQueryResultWithNull() {
        targetPaymentServiceImpl.createTransactionWithQueryResult(abstractOrderModel, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePaymentTransactionWithQueryResultWithFailedResult() {
        final TargetQueryTransactionDetailsResult queryTransactionResult = new TargetQueryTransactionDetailsResult();
        queryTransactionResult.setSuccess(false);
        targetPaymentServiceImpl.createTransactionWithQueryResult(abstractOrderModel, queryTransactionResult);
    }

    @Test
    public void testCreateIpgPaymentInfoModelForCreditCard() {
        final TargetCardResult ipgCardResult = mock(TargetCardResult.class);
        given(ipgCardResult.getCardType()).willReturn(CreditCardType.VISA.getCode());
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel, ipgCardResult);

        targetPaymentServiceImpl.createIpgPaymentInfoModel(abstractOrderModel, ipgCardResult);
        verify(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(abstractOrderModel, ipgCardResult);
        verify(targetPaymentServiceImpl, never()).createIpgGiftCardPaymentInfoModel(abstractOrderModel, ipgCardResult);
    }

    @Test
    public void testCreateIpgPaymentInfoModelForGiftCard() {
        final TargetCardResult ipgCardResult = mock(TargetCardResult.class);
        given(ipgCardResult.getCardType()).willReturn(CreditCardType.GIFTCARD.getCode());
        doReturn(ipgGiftCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgGiftCardPaymentInfoModel(
                abstractOrderModel, ipgCardResult);

        targetPaymentServiceImpl.createIpgPaymentInfoModel(abstractOrderModel, ipgCardResult);
        verify(targetPaymentServiceImpl).createIpgGiftCardPaymentInfoModel(abstractOrderModel, ipgCardResult);
        verify(targetPaymentServiceImpl, never()).createIpgCreditCardPaymentInfoModel(abstractOrderModel,
                ipgCardResult);
    }

    @Test
    public void testSaveCardResultsForCreditCardPayment() {
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);
        given(cardResult.getCardType()).willReturn(CreditCardType.VISA.getCode());

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        cardResults.add(cardResult);
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);

        targetPaymentServiceImpl.saveCardResult(abstractOrderModel, cardResults.get(0), paymentTransactionEntryModel);
        verify(paymentTransactionEntryModel).setAmount(amount);
        verify(paymentTransactionEntryModel).setIpgPaymentInfo(ipgCreditCardPaymentInfoModel);
        verify(ipgCreditCardPaymentInfoModel).setOwner(paymentTransactionEntryModel);
        verify(ipgCreditCardPaymentInfoModel).setDuplicate(Boolean.TRUE);
        verify(modelService).saveAll(paymentTransactionEntryModel, ipgCreditCardPaymentInfoModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSaveCardResultsForGiftCardPayment() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);

        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);
        given(cardResult.getCardType()).willReturn(CreditCardType.GIFTCARD.getCode());
        given(ipgTransactionResult.isImmediatePayment()).willReturn(true);
        given(ipgTransactionResult.isPaymentSuccessful()).willReturn(true);

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        cardResults.add(cardResult);

        given(entry.getIpgPaymentInfo()).willReturn(ipgPaymentInfo);

        doReturn(ipgGiftCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgGiftCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);

        targetPaymentServiceImpl.saveCardResult(abstractOrderModel, cardResults.get(0), entry);

        verify(targetPaymentServiceImpl, times(1)).updateTransactionEntryWithImmediatePaymentResult(entry,
                ipgTransactionResult);
        verify(entry, Mockito.atLeastOnce()).setAmount(amount);
        verify(entry).setIpgPaymentInfo(ipgGiftCardPaymentInfoModel);
        verify(ipgGiftCardPaymentInfoModel).setOwner(entry);
        verify(ipgGiftCardPaymentInfoModel).setDuplicate(Boolean.TRUE);
        verify(modelService).saveAll(entry, ipgGiftCardPaymentInfoModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateTransactionEntryWithImmediatePaymentResultForSuccessfulPayment() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);

        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        given(ipgTransactionResult.isImmediatePayment()).willReturn(true);
        given(ipgTransactionResult.isPaymentSuccessful()).willReturn(true);

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);

        final Date now = new Date();
        given(ipgTransactionResult.getTransactionDate()).willReturn(now);

        final String receiptNumber = "123465";
        given(ipgTransactionResult.getReceiptNumber()).willReturn(receiptNumber);

        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(ipgPaymentInfoModel);

        targetPaymentServiceImpl.updateTransactionEntryWithImmediatePaymentResult(entry, ipgTransactionResult);

        verify(entry).setAmount(amount);
        verify(entry).setTime(now);
        verify(entry).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(entry).setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());
        verify(entry.getIpgPaymentInfo()).setIsPaymentSucceeded(true);
        verify(entry).setReceiptNo(receiptNumber);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateTransactionEntryWithImmediatePaymentResultForFailedPayment() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);

        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        given(ipgTransactionResult.isImmediatePayment()).willReturn(true);
        given(ipgTransactionResult.isPaymentSuccessful()).willReturn(false);

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);

        final Date now = new Date();
        given(ipgTransactionResult.getTransactionDate()).willReturn(now);

        final String receiptNumber = "123465";
        given(ipgTransactionResult.getReceiptNumber()).willReturn(receiptNumber);

        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(ipgPaymentInfoModel);

        final String responseCode = "2";
        final String responseText = "Failed transaction";
        given(ipgTransactionResult.getResponseText()).willReturn(responseText);
        given(ipgTransactionResult.getResponseCode()).willReturn(responseCode);

        targetPaymentServiceImpl.updateTransactionEntryWithImmediatePaymentResult(entry, ipgTransactionResult);

        verify(entry).setAmount(amount);
        verify(entry).setTime(now);
        verify(entry).setTransactionStatus(TransactionStatus.REJECTED.toString());
        verify(entry).setTransactionStatusDetails(responseText);
        verify(entry).setDeclinedTransactionStatusInfo(responseCode + " - " + responseText);
        verify(entry.getIpgPaymentInfo()).setIsPaymentSucceeded(false);
        verify(entry).setReceiptNo(receiptNumber);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateTransactionEntryWithImmediatePaymentResultForSuccessfulPaymentNoReceiptNumber() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);

        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        given(ipgTransactionResult.isImmediatePayment()).willReturn(true);
        given(ipgTransactionResult.isPaymentSuccessful()).willReturn(true);

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);

        final Date now = new Date();
        given(ipgTransactionResult.getTransactionDate()).willReturn(now);

        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(ipgPaymentInfoModel);

        targetPaymentServiceImpl.updateTransactionEntryWithImmediatePaymentResult(entry, ipgTransactionResult);

        verify(entry).setAmount(amount);
        verify(entry).setTime(now);
        verify(entry).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(entry).setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());
        verify(entry.getIpgPaymentInfo()).setIsPaymentSucceeded(true);
        verify(entry).setReceiptNo(TgtpaymentConstants.NOT_AVAILABLE);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSaveSavedCards() {
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        given(cardResult.isDefaultCard()).willReturn(Boolean.TRUE);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);

        final List<TargetCardResult> savedCards = Collections.singletonList(cardResult);
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);

        targetPaymentServiceImpl.updateSavedCards(abstractOrderModel, savedCards);
        verify(ipgCreditCardPaymentInfoModel).setDuplicate(Boolean.FALSE);
        verify(ipgCreditCardPaymentInfoModel).setSaved(true);
        verify(ipgCreditCardPaymentInfoModel).setIsPaymentSucceeded(Boolean.TRUE);
        verify(customerModel).setDefaultPaymentInfo(ipgCreditCardPaymentInfoModel);
        verify(modelService).save(customerModel);
    }

    @Test
    public void testupdateTransactionEntryWithCaptureResultWithDeclineCode() {
        final TargetAbstractPaymentResult paymentResult = mock(TargetFollowOnRefundResult.class);
        given(paymentResult.getDeclineCode()).willReturn("59");
        given(paymentResult.getDeclineMessage()).willReturn("Decline Message");
        targetPaymentServiceImpl.updateTransactionEntryWithCaptureResult(paymentTransactionEntryModel, paymentResult);
        verify(paymentTransactionEntryModel).setDeclinedTransactionStatusInfo("59 - Decline Message");

    }

    @Test
    public void testupdateTransactionEntryWithCaptureResultWithNullMessage() {
        final TargetAbstractPaymentResult paymentResult = mock(TargetFollowOnRefundResult.class);
        given(paymentResult.getDeclineCode()).willReturn("17");
        given(paymentResult.getDeclineMessage()).willReturn(null);
        targetPaymentServiceImpl.updateTransactionEntryWithCaptureResult(paymentTransactionEntryModel, paymentResult);
        verify(paymentTransactionEntryModel).setDeclinedTransactionStatusInfo("17 - N/A");

    }

    @Test
    public void testUpdateTransactionEntryWithCaptureResultForGiftCard() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final IpgGiftCardPaymentInfoModel giftCardPaymentInfoModel = mock(IpgGiftCardPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(giftCardPaymentInfoModel);
        given(entry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        final TargetAbstractPaymentResult paymentResult = mock(TargetFollowOnRefundResult.class);
        targetPaymentServiceImpl.updateTransactionEntryWithCaptureResult(entry, paymentResult);
        verify(entry, never()).setReceiptNo(Mockito.anyString());
    }

    @Test
    public void testUpdateTransactionEntryWithAcceptedRefundFollowonResultForGiftCard() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final IpgGiftCardPaymentInfoModel giftCardPaymentInfoModel = mock(IpgGiftCardPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(giftCardPaymentInfoModel);
        given(entry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(entry.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        final TargetAbstractPaymentResult paymentResult = mock(TargetFollowOnRefundResult.class);
        given(paymentResult.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        targetPaymentServiceImpl.updateTransactionEntryWithCaptureResult(entry, paymentResult);
        verify(entry).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(entry, never()).setDeclinedTransactionStatusInfo(anyString());
    }

    @Test
    public void testUpdateTransactionEntryWithDeclinedRefundFollowonResultForGiftCard() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final IpgGiftCardPaymentInfoModel giftCardPaymentInfoModel = mock(IpgGiftCardPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(giftCardPaymentInfoModel);
        given(entry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(entry.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        final TargetAbstractPaymentResult paymentResult = mock(TargetFollowOnRefundResult.class);
        given(paymentResult.getTransactionStatus()).willReturn(TransactionStatus.REJECTED);
        given(paymentResult.getDeclineCode()).willReturn("68");
        given(paymentResult.getDeclineMessage()).willReturn("NotFound");
        targetPaymentServiceImpl.updateTransactionEntryWithCaptureResult(entry, paymentResult);
        verify(entry).setTransactionStatus(TransactionStatus.REJECTED.toString());
        verify(entry).setDeclinedTransactionStatusInfo("68 - NotFound");
    }

    @Test
    public void testUpdateSavedCardsWithNull() {
        targetPaymentServiceImpl.updateSavedCards(orderModel, null);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateSavedCardsWithEmptyList() {
        targetPaymentServiceImpl.updateSavedCards(orderModel, new ArrayList<TargetCardResult>());
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateSavedCardsWithPrimaryCard() {
        final ArrayList<TargetCardResult> savedCards = new ArrayList<>();
        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setDefaultCard(true);
        savedCards.add(ipgCardResult);
        targetPaymentServiceImpl.updateSavedCards(orderModel, savedCards);
        verify(customerModel).setDefaultPaymentInfo(any(PaymentInfoModel.class));
        verify(modelService).saveAll(any(Collection.class));
        verify(ipgCreditCardPaymentInfoModel).setSaved(true);
        verify(ipgCreditCardPaymentInfoModel).setDuplicate(Boolean.FALSE);
        verify(ipgCreditCardPaymentInfoModel).setDefaultCard(Boolean.TRUE);
    }

    @Test
    public void testUpdateSavedCardsWithNoPrimaryCard() {
        final ArrayList<TargetCardResult> savedCards = new ArrayList<>();
        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setDefaultCard(false);
        savedCards.add(ipgCardResult);
        targetPaymentServiceImpl.updateSavedCards(orderModel, savedCards);
        verify(customerModel, never()).setDefaultPaymentInfo(any(PaymentInfoModel.class));
        verify(modelService).saveAll(any(Collection.class));
        verify(ipgCreditCardPaymentInfoModel).setSaved(true);
        verify(ipgCreditCardPaymentInfoModel).setDuplicate(Boolean.FALSE);
        verify(ipgCreditCardPaymentInfoModel).setDefaultCard(Boolean.FALSE);
    }

    public void testGetIPGCreditCardInfoForSuccessPaymentWithNulltrans() {
        given(orderModel.getPaymentTransactions()).willReturn(null);
        final List<PaymentInfoModel> successfulPayments = targetPaymentServiceImpl
                .getSuccessfulPayments(orderModel);
        assertThat(CollectionUtils.isEmpty(successfulPayments)).isTrue();
    }

    @Test
    public void testGetIPGCreditCardInfoForSuccessPaymentWithNullEntry() {
        given(orderModel.getPaymentTransactions()).willReturn(Collections.singletonList(paymentTransactionModel));
        given(paymentTransactionModel.getEntries()).willReturn(null);
        final List<PaymentInfoModel> successfulPayments = targetPaymentServiceImpl
                .getSuccessfulPayments(orderModel);
        assertThat(CollectionUtils.isEmpty(successfulPayments)).isTrue();
    }

    @Test
    public void testGetIPGCreditCardInfoForSuccessPaymentWithNonIPGEntries() {
        given(orderModel.getPaymentTransactions()).willReturn(Collections.singletonList(paymentTransactionModel));
        given(paymentTransactionModel.getEntries()).willReturn(Collections.singletonList(paymentTransactionEntryModel));
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);
        final List<PaymentInfoModel> successfulPayments = targetPaymentServiceImpl.getSuccessfulPayments(orderModel);
        assertThat(CollectionUtils.isEmpty(successfulPayments)).isTrue();
    }

    /**
     * Test method with IPGpayment method,but declined payment
     */
    @Test
    public void testGetIPGCreditCardInfoForSuccessPaymentWithIPGEntries() {
        given(orderModel.getPaymentTransactions()).willReturn(Collections.singletonList(paymentTransactionModel));
        given(paymentTransactionModel.getEntries()).willReturn(
                Collections.singletonList(paymentTransactionEntryModel));
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(
                ipgCreditCardPaymentInfoModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.REJECTED.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(
                PaymentTransactionType.CAPTURE);
        final List<PaymentInfoModel> successfulPayments = targetPaymentServiceImpl
                .getSuccessfulPayments(orderModel);
        assertThat(CollectionUtils.isEmpty(successfulPayments)).isTrue();

    }

    /**
     * Test method with IPGpayment method,but Success payment
     */
    @Test
    public void testGetIPGCreditCardInfoForSuccessPaymentIPGEntries() {
        given(orderModel.getPaymentTransactions()).willReturn(Collections.singletonList(paymentTransactionModel));
        given(paymentTransactionModel.getEntries()).willReturn(
                Collections.singletonList(paymentTransactionEntryModel));
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(
                ipgCreditCardPaymentInfoModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(
                PaymentTransactionType.CAPTURE);
        given(ipgCreditCardPaymentInfoModel.getNumber()).willReturn(
                CREDIT_CARD_NUMBER);

        final List<PaymentInfoModel> successfulPayments = targetPaymentServiceImpl
                .getSuccessfulPayments(orderModel);
        assertThat(CollectionUtils.isEmpty(successfulPayments)).isFalse();
        final IpgCreditCardPaymentInfoModel creditCard = (IpgCreditCardPaymentInfoModel)successfulPayments.get(0);
        assertThat(creditCard).isNotNull();
        assertThat(creditCard.getNumber()).isEqualTo(CREDIT_CARD_NUMBER);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapturePaymentTransactionWithNoOrder() {
        targetPaymentServiceImpl.capture(null, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapturePaymentTransactionWithNoPaymentInfo() {
        targetPaymentServiceImpl.capture(orderModel, null, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapturePaymentTransactionWithNoAmount() {
        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, null, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapturePaymentTransactionWithNoCurrency() {
        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, null,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapturePaymentTransactionWithNoEntry() {
        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapturePaymentTransactionWithEmptyEntry() {
        given(paymentTransactionModel.getEntries()).willReturn(new ArrayList<PaymentTransactionEntryModel>());
        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test
    public void testCapturePaymentTransactionWithSingleEntry() {
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();
        entry.setAmount(BigDecimal.ONE);
        entries.add(entry);
        entry.setPaymentTransaction(paymentTransactionModel);
        given(paymentTransactionModel.getEntries()).willReturn(entries);

        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);

        verify(targetPaymentServiceImpl).capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, entry);
    }

    @Test
    public void multipleCaptureWillContinueIfAccepted() {
        final PaymentTransactionEntryModel nextEntry = mock(PaymentTransactionEntryModel.class);
        entries.add(captureEntry);
        entries.add(nextEntry);
        given(nextEntry.getAmount()).willReturn(BigDecimal.ONE);
        given(nextEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        given(captureEntry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());

        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);

        verify(targetPaymentServiceImpl).capture(orderModel, paymentInfoModel, BigDecimal.ONE, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, captureEntry);
        verify(targetPaymentServiceImpl).capture(orderModel, paymentInfoModel, BigDecimal.ONE, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, nextEntry);
    }

    @Test
    public void multipleCaptureWillTerminateIfReview() {
        final PaymentTransactionEntryModel nextEntry = mock(PaymentTransactionEntryModel.class);
        entries.add(captureEntry);
        entries.add(nextEntry);
        given(nextEntry.getAmount()).willReturn(BigDecimal.ONE);
        given(nextEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        given(captureEntry.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.toString());

        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);

        verify(targetPaymentServiceImpl).capture(orderModel, paymentInfoModel, BigDecimal.ONE, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, captureEntry);
        verify(targetPaymentServiceImpl, times(0)).capture(orderModel, paymentInfoModel, BigDecimal.ONE, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, nextEntry);
    }

    @Test
    public void capturePaymentTransactionWithMultipleEntriesShouldTerminateIfOneFails() {
        final PaymentTransactionEntryModel nextEntry = mock(PaymentTransactionEntryModel.class);
        entries.add(captureEntry);
        entries.add(nextEntry);
        given(nextEntry.getAmount()).willReturn(BigDecimal.ONE);
        given(nextEntry.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        given(captureEntry.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.toString());

        targetPaymentServiceImpl.capture(orderModel, paymentInfoModel, BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);

        verify(targetPaymentServiceImpl).capture(orderModel, paymentInfoModel, BigDecimal.ONE, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, captureEntry);
        verify(targetPaymentServiceImpl, times(0)).capture(orderModel, paymentInfoModel, BigDecimal.ONE, currencyModel,
                PaymentCaptureType.PLACEORDER,
                null, nextEntry);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWithOneGiftCard() {
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(
                ipgPaymentMethod);
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        final TargetRetrieveTransactionResult retrieveResult = mock(TargetRetrieveTransactionResult.class);
        given(ipgPaymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class))).willReturn(
                retrieveResult);
        given(retrieveResult.getPayments()).willReturn(ImmutableList.of(new PaymentTransactionEntryData()));

        final List<TargetCardResult> giftCards = createGiftCardList(BigDecimal.TEN, 1);

        final TargetPaymentVoidResult voidResult = createVoidResult(true);

        given(ipgPaymentMethod.voidPayment(any(TargetPaymentVoidRequest.class))).willReturn(
                voidResult);

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(cartModel, giftCards);

        assertThat(isReversalSuccessful).isTrue();
        verify(ipgPaymentMethod, times(1)).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWithOneGiftCardAndFailed() {
        givenCartHasIpgPayment();
        final BigDecimal amount = mock(BigDecimal.class);
        final List<TargetCardResult> giftCards = createGiftCardList(amount, 1);
        given(amount.setScale(2, BigDecimal.ROUND_HALF_DOWN)).willReturn(amount);
        final TargetPaymentVoidResult createVoidResult = createVoidResult(false);
        given(ipgPaymentMethod.voidPayment(any(TargetPaymentVoidRequest.class))).willReturn(
                createVoidResult);

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(cartModel, giftCards);

        assertThat(isReversalSuccessful).isFalse();
        verify(ipgPaymentMethod, times(1)).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
        verify(businessProcess).startGiftCardReverseProcess(any(GiftCardReversalData.class), anyString(), anyString(),
                any(PaymentTransactionEntryModel.class));
        verify(cartModel).getModifiedtime();
        verify(cartModel, never()).getDate();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWithOneGiftCardAndWithException() {
        givenCartHasIpgPayment();
        final List<TargetCardResult> giftCards = createGiftCardList(BigDecimal.TEN, 1);
        given(ipgPaymentMethod.voidPayment(Mockito.any(TargetPaymentVoidRequest.class))).willThrow(
                new AdapterException("test"));

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(cartModel, giftCards);

        assertThat(isReversalSuccessful).isFalse();
        verify(ipgPaymentMethod, times(1)).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
        verify(businessProcess).startGiftCardReverseProcess(any(GiftCardReversalData.class), anyString(), anyString(),
                any(PaymentTransactionEntryModel.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWhenNoGiftCards() {
        givenCartHasIpgPayment();

        final List<TargetCardResult> giftCards = new ArrayList<>();
        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(
                cartModel, giftCards);

        assertThat(isReversalSuccessful).isTrue();
        verify(ipgPaymentMethod, never()).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
    }

    @Test
    public void testReverseGiftCardsPaymentWithException() {
        final AdapterException adapterException = new AdapterException("test");
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        given(ipgPaymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class)))
                .willThrow(adapterException);
        final boolean isReversalSuccessful = targetPaymentServiceImpl.reverseGiftCardPayment(cartModel);
        assertThat(isReversalSuccessful).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWhenGiftCardPaymentNotSuccessful() {
        given(paymentMethodStrategy.getPaymentMethod(Mockito.any(PaymentModeModel.class))).willReturn(
                paymentMethod);
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);

        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        final TargetCardPaymentResult transactionResult = new TargetCardPaymentResult();
        transactionResult.setResponseCode("1");
        giftCard.setTargetCardPaymentResult(transactionResult);
        giftCards.add(giftCard);

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(
                cartModel, giftCards);

        assertThat(isReversalSuccessful).isTrue();
        verify(paymentMethod, never()).voidPayment(any(TargetPaymentVoidRequest.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWithNoApprovedReceipt() {
        given(paymentMethodStrategy.getPaymentMethod(Mockito.any(PaymentModeModel.class))).willReturn(
                paymentMethod);
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(paymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class))).willReturn(null);
        final List<TargetCardResult> giftCards = createGiftCardList(BigDecimal.TEN, 2);
        final TargetPaymentVoidResult voidResult = createVoidResult(true);
        given(paymentMethod.voidPayment(Mockito.any(TargetPaymentVoidRequest.class))).willReturn(
                voidResult);

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(cartModel, giftCards);

        assertThat(isReversalSuccessful).isTrue();
        verify(paymentMethod, never()).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWithMultipleGiftCards() {
        givenCartHasIpgPayment();
        final List<TargetCardResult> giftCards = createGiftCardList(BigDecimal.TEN, 2);
        final TargetPaymentVoidResult voidResult = createVoidResult(true);
        given(ipgPaymentMethod.voidPayment(Mockito.any(TargetPaymentVoidRequest.class))).willReturn(
                voidResult);

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(cartModel, giftCards);

        assertThat(isReversalSuccessful).isTrue();
        verify(ipgPaymentMethod, times(2)).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardsPaymentWithMultipleReversedGiftCards() {
        givenCartHasIpgPayment();
        given(ipgPaymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class))).willReturn(null);
        final List<TargetCardResult> giftCards = createGiftCardList(BigDecimal.TEN, 2);
        final TargetPaymentVoidResult voidResult = createVoidResult(true);
        given(ipgPaymentMethod.voidPayment(Mockito.any(TargetPaymentVoidRequest.class))).willReturn(
                voidResult);

        final boolean isReversalSuccessful = targetPaymentServiceImpl.reversePayment(cartModel, giftCards);

        assertThat(isReversalSuccessful).isTrue();
        verify(ipgPaymentMethod, times(2)).retrieveTransaction(BDDMockito.any(TargetRetrieveTransactionRequest.class));
        verify(ipgPaymentMethod, never()).voidPayment(BDDMockito.any(TargetPaymentVoidRequest.class));
    }

    @Test
    public void testCreateRefundFollowonRequestForGiftcardEntry() {
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgGiftCardPaymentInfoModel);
        final TargetFollowOnRefundRequest request = targetPaymentServiceImpl.createRefundFollowOnRequest(
                paymentTransactionEntryModel, paymentTransactionModel,
                BigDecimal.TEN);
        assertThat(request.isGiftcard()).isTrue();
    }

    @Test
    public void testCreateRefundFollowonRequestForIpgCreditCardEntry() {
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgCreditCardPaymentInfoModel);
        final TargetFollowOnRefundRequest request = targetPaymentServiceImpl.createRefundFollowOnRequest(
                paymentTransactionEntryModel, paymentTransactionModel,
                BigDecimal.TEN);
        assertThat(request.isGiftcard()).isFalse();
    }

    @Test
    public void testCreateRefundFollowonRequestForNonIpgEntry() {
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);
        final TargetFollowOnRefundRequest request = targetPaymentServiceImpl.createRefundFollowOnRequest(
                paymentTransactionEntryModel, paymentTransactionModel,
                BigDecimal.TEN);
        assertThat(request.isGiftcard()).isFalse();
    }

    @Test
    public void testCreateRefundFollowonRequestWithCurrency() {
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);
        given(paymentTransactionEntryModel.getCurrency()).willReturn(currencyModel);
        final TargetFollowOnRefundRequest request = targetPaymentServiceImpl.createRefundFollowOnRequest(
                paymentTransactionEntryModel, paymentTransactionModel,
                BigDecimal.TEN);
        verify(currencyModel).getIsocode();
        assertThat(request.isGiftcard()).isFalse();
    }

    @Test
    public void testSkipRefundLastCaptureIfNoTransactionFound() {
        given(abstractOrderModel.getPaymentTransactions()).willReturn(null);
        targetPaymentServiceImpl.refundLastCaptureTransaction(abstractOrderModel);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(paymentMethod);
    }

    @Test
    public void testRefundLastAcceptedCapture() {
        trans.add(paymentTransactionModel);
        given(abstractOrderModel.getPaymentTransactions()).willReturn(trans);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        entries.add(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.TEN);
        targetPaymentServiceImpl.refundLastCaptureTransaction(abstractOrderModel);
        verify(paymentMethod).followOnRefund(any(TargetFollowOnRefundRequest.class));
        verify(abstractOrderModel, never()).getCode();
    }

    @Test
    public void testRefundLastReviewCapture() {
        trans.add(paymentTransactionModel);
        given(abstractOrderModel.getPaymentTransactions()).willReturn(trans);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        entries.add(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.toString());
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.TEN);
        targetPaymentServiceImpl.refundLastCaptureTransaction(abstractOrderModel);
        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.ERROR.toString());
        verify(paymentTransactionEntryModel).setTransactionStatusDetails(
                TransactionStatusDetails.PROCESSOR_DECLINE.toString());
        verify(paymentTransactionEntryModel).setDeclinedTransactionStatusInfo(
                "Transaction cancelled due to validation/payment error");
    }

    @Test
    public void testFailedToRefundLastAcceptedCapture() {
        trans.add(paymentTransactionModel);
        given(abstractOrderModel.getPaymentTransactions()).willReturn(trans);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        entries.add(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        final BigDecimal amount = mock(BigDecimal.class);
        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);
        given(paymentMethod.followOnRefund(any(TargetFollowOnRefundRequest.class))).willThrow(
                new RuntimeException("refund failed"));
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgGiftCardPaymentInfoModel);
        given(paymentTransactionModel.getOrder()).willReturn(abstractOrderModel);
        given(abstractOrderModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(abstractOrderModel.getDeliveryAddress()).willReturn(address);
        given(amount.setScale(2, BigDecimal.ROUND_HALF_DOWN)).willReturn(BigDecimal.ONE);
        targetPaymentServiceImpl.refundLastCaptureTransaction(abstractOrderModel);
        verify(paymentMethod).followOnRefund(any(TargetFollowOnRefundRequest.class));
        verify(abstractOrderModel, times(2)).getCode();
    }

    @Test
    public void testSkipRefundLastNonAcceptedCapture() {
        trans.add(paymentTransactionModel);
        given(abstractOrderModel.getPaymentTransactions()).willReturn(trans);
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        entries.add(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ERROR.toString());
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.TEN);
        targetPaymentServiceImpl.refundLastCaptureTransaction(abstractOrderModel);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(paymentMethod);
    }

    @Test
    public void testSkipRefundLastCaptureIfLastTransactionIsNotCaptureOnly() {
        trans.add(paymentTransactionModel);
        given(abstractOrderModel.getPaymentTransactions()).willReturn(trans);
        entries.add(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        targetPaymentServiceImpl.refundLastCaptureTransaction(abstractOrderModel);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(paymentMethod);
    }

    @Test
    public void testFindRefundedAmountAfterRefundFollowOn() {
        final PaymentTransactionModel paymentTransaction = Mockito.mock(PaymentTransactionModel.class);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(1, true);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(11.11d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(41.00d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(99.99d).setScale(2);

        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor1);
        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor2);
        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor3);
        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REJECTED, paymentInfor4);
        final List<PaymentTransactionEntryModel> refundedEntries = new ArrayList<PaymentTransactionEntryModel>(
                Arrays.asList(paymentTransactionRefund1, paymentTransactionRefund2, paymentTransactionRefund3,
                        paymentTransactionRefund4));
        final BigDecimal resultAmount = targetPaymentServiceImpl.findRefundedAmountAfterRefundFollowOn(refundedEntries);
        assertThat(amount1.add(amount2).add(amount3)).isEqualTo(resultAmount);
    }

    @Test
    public void testRetrieveFailedGiftCardDetails() {
        final TargetRetrieveTransactionResult result = mock(TargetRetrieveTransactionResult.class);
        given(paymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class))).willReturn(result);
        targetPaymentServiceImpl.retrieveFailedGiftcardPayment(orderModel);
        verify(paymentMethod).retrieveTransaction(any(TargetRetrieveTransactionRequest.class));
    }

    @Test
    public void testRefundIpgManualRefund() {
        final BigDecimal amount = BigDecimal.valueOf(12.34d).setScale(2);
        final String receiptNo = "testReceiptNo";
        final PaymentTransactionModel paymentTransaction = Mockito.mock(PaymentTransactionModel.class);
        given(paymentTransaction.getCode()).willReturn("testCode");
        given(paymentTransaction.getCurrency()).willReturn(currencyModel);
        final PaymentTransactionEntryModel refundTransactionEntry = new PaymentTransactionEntryModel();
        given(modelService.create(PaymentTransactionEntryModel.class)).willReturn(refundTransactionEntry);
        final PaymentTransactionEntryModel transactionEntry = targetPaymentServiceImpl.refundIpgManualRefund(
                paymentTransaction, amount, receiptNo);
        assertThat(transactionEntry.getReceiptNo()).isEqualTo(receiptNo);
        assertThat(transactionEntry.getAmount()).isEqualTo(amount);
    }

    @Test
    public void testReverseGiftWithReversalData() {
        final GiftCardReversalData reversalData = mock(GiftCardReversalData.class);
        given(reversalData.getProvider()).willReturn("ipg");
        given(paymentMethodStrategy.getPaymentMethod("ipg")).willReturn(paymentMethod);
        final TargetRetrieveTransactionResult retrieveResult = mock(TargetRetrieveTransactionResult.class);
        given(paymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class)))
                .willReturn(retrieveResult);
        given(retrieveResult.getPayments()).willReturn(ImmutableList.of(new PaymentTransactionEntryData()));

        targetPaymentServiceImpl.reversePayment(reversalData);
        verify(paymentMethodStrategy).getPaymentMethod("ipg");
        verify(paymentMethod).retrieveTransaction(any(TargetRetrieveTransactionRequest.class));
        verify(paymentMethod).voidPayment(any(TargetPaymentVoidRequest.class));
    }

    public void testQueryTransactionDetails() {
        final String sessionId = "132456789";
        final String token = "13156465465";
        final String cardNumber = "423145614561";
        final String cardType = "VISA";
        final BigDecimal amount = new BigDecimal("19.99");

        final TargetCaptureRequest captureRequest = new TargetCaptureRequest();
        captureRequest.setSessionId(sessionId);
        captureRequest.setToken(token);

        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        result.setSessionId(sessionId);
        result.setSessionToken(token);

        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardNumber(cardNumber);
        ipgCardResult.setCardType(cardType);

        final TargetCardPaymentResult ipgTransactionResult = new TargetCardPaymentResult();
        ipgTransactionResult.setAmount(amount);
        ipgCardResult.setTargetCardPaymentResult(ipgTransactionResult);
        result.addCardResult(ipgCardResult);
        given(
                ipgPaymentMethod.queryTransactionDetails(mock(TargetQueryTransactionDetailsRequest.class)))
                .willReturn(result);
        given(ipgPaymentMethod.queryTransactionDetails(any(TargetQueryTransactionDetailsRequest.class)))
                .willReturn(result);

        final TargetQueryTransactionDetailsResult resultReturned = targetPaymentServiceImpl.queryTransactionDetails(
                ipgPaymentMethod, ipgPaymentInfo);

        assertThat(resultReturned).isNotNull();
        assertThat(resultReturned.getSessionId()).isEqualTo(sessionId);
        assertThat(resultReturned.getSessionToken()).isEqualTo(token);
        assertThat(resultReturned.getCardResults()).isNotNull();
        assertThat(resultReturned.getCardResults().size()).isEqualTo(1);
        assertThat((resultReturned.getCardResults().get(0)).getCardNumber()).isEqualTo(cardNumber);
        assertThat((resultReturned.getCardResults().get(0)).getCardType()).isEqualTo(cardType);
        assertThat((resultReturned.getCardResults().get(0)).getTargetCardPaymentResult()).isNotNull();
        assertThat((resultReturned.getCardResults().get(0)).getTargetCardPaymentResult().getAmount()).isEqualTo(amount);

    }

    @Test
    public void testGetGiftCardPaymentsWhenQueryTransactionNotSuccessfulOrPending() {
        final CartModel mockCartModel = Mockito.mock(CartModel.class);

        given(mockCartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(mockCartModel.getUser()).willReturn(customerModel);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);

        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        result.setSuccess(false);
        result.setPending(false);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgPaymentInfo);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(mockCartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isEmpty();
    }

    @Test
    public void testGetGiftCardPaymentsWhenSuccessfulTransactionHasOnlyCreditCards() {
        final CartModel mockCartModel = Mockito.mock(CartModel.class);

        given(mockCartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(mockCartModel.getUser()).willReturn(customerModel);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);

        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardType("VISA");
        result.addCardResult(ipgCardResult);
        result.setSuccess(true);
        result.setPending(false);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgPaymentInfo);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(mockCartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isEmpty();
    }

    @Test
    public void testGetGiftCardPaymentsWhenPendingTransactionHasOnlyCreditCards() {
        final CartModel mockCartModel = Mockito.mock(CartModel.class);

        given(mockCartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(mockCartModel.getUser()).willReturn(customerModel);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);

        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardType("VISA");
        result.addCardResult(ipgCardResult);
        result.setSuccess(false);
        result.setPending(true);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgPaymentInfo);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(mockCartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isEmpty();
    }

    @Test
    public void testGetGiftCardPaymentsWhenSuccessfulTransactionHasOneGiftCard() {
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);
        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardType("giftcard");
        result.addCardResult(ipgCardResult);
        result.setSuccess(true);
        result.setPending(false);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgToken, ipgSessionId);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(cartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isNotEmpty();
        assertThat(giftCards.size()).isEqualTo(1);
    }

    @Test
    public void testGetGiftCardPaymentsWhenPendingTransactionHasOneGiftCard() {
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);
        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult ipgCardResult = new TargetCardResult();
        ipgCardResult.setCardType("giftcard");
        result.addCardResult(ipgCardResult);
        result.setSuccess(false);
        result.setPending(true);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgToken, ipgSessionId);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(cartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isNotEmpty();
        assertThat(giftCards.size()).isEqualTo(1);
    }

    @Test
    public void testGetGiftCardPaymentsWhenSuccessfulTransactionHasMultipleGiftCards() {
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);

        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult ipgCardResult1 = new TargetCardResult();
        ipgCardResult1.setCardType("giftcard");
        result.addCardResult(ipgCardResult1);

        final TargetCardResult ipgCardResult2 = new TargetCardResult();
        ipgCardResult2.setCardType("giftcard");
        result.addCardResult(ipgCardResult2);
        result.setSuccess(true);
        result.setPending(false);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgToken, ipgSessionId);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(cartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isNotEmpty();
        assertThat(giftCards.size()).isEqualTo(2);
    }

    @Test
    public void testGetGiftCardPaymentsWhenPendingTransactionHasMultipleGiftCards() {
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfo)).willReturn(ipgPaymentMethod);

        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult ipgCardResult1 = new TargetCardResult();
        ipgCardResult1.setCardType("giftcard");
        result.addCardResult(ipgCardResult1);

        final TargetCardResult ipgCardResult2 = new TargetCardResult();
        ipgCardResult2.setCardType("giftcard");
        result.addCardResult(ipgCardResult2);
        result.setSuccess(false);
        result.setPending(true);

        Mockito.doReturn(result).when(targetPaymentServiceImpl)
                .queryTransactionDetails(ipgPaymentMethod, ipgToken, ipgSessionId);

        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(cartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isNotEmpty();
        assertThat(giftCards.size()).isEqualTo(2);
    }

    @Test
    public void testGetGiftCardPaymentsWithNoCart() {
        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(null);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isEmpty();
    }

    @Test
    public void testGetGiftCardPaymentsWithNullIpgCart() {
        given(cartModel.getPaymentInfo()).willReturn(paymentInfoModel);
        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(cartModel);

        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isEmpty();
    }

    @Test
    public void testGetGiftCardPaymentsWithCartWithNoPaymentInfo() {
        given(cartModel.getPaymentInfo()).willReturn(null);
        final List<TargetCardResult> giftCards = targetPaymentServiceImpl.getGiftCardPayments(cartModel);
        verifyZeroInteractions(paymentMethodStrategy);
        assertThat(giftCards).isNotNull();
        assertThat(giftCards).isEmpty();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardSuccessful() {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        giftCards.add(giftCard);

        Mockito.doReturn(giftCards).when(targetPaymentServiceImpl).getGiftCardPayments(cartModel);
        Mockito.doReturn(true).when(targetPaymentServiceImpl)
                .reversePayment(Mockito.any(CartModel.class), Mockito.anyList());


        final boolean isGiftCardReversalSucceeded = targetPaymentServiceImpl.reverseGiftCardPayment(cartModel);

        assertThat(isGiftCardReversalSucceeded).isTrue();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReverseGiftCardFail() {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        giftCards.add(giftCard);

        Mockito.doReturn(giftCards).when(targetPaymentServiceImpl).getGiftCardPayments(cartModel);
        Mockito.doReturn(false).when(targetPaymentServiceImpl)
                .reversePayment(Mockito.any(CartModel.class), Mockito.anyList());


        final boolean isGiftCardReversalSucceeded = targetPaymentServiceImpl.reverseGiftCardPayment(cartModel);

        assertThat(isGiftCardReversalSucceeded).isFalse();
    }

    @Test
    public void testReverseGiftCardWhenGiftCardsListIsEmpty() {
        final List<TargetCardResult> giftCards = new ArrayList<>();

        Mockito.doReturn(giftCards).when(targetPaymentServiceImpl)
                .getGiftCardPayments(cartModel);

        targetPaymentServiceImpl.reverseGiftCardPayment(cartModel);

        verify(targetPaymentServiceImpl, never()).reversePayment(Mockito.any(CartModel.class),
                Mockito.anyList());
    }

    @Test
    public void testReverseGiftCardWhenGiftCardsListIsNull() {
        Mockito.doReturn(null).when(targetPaymentServiceImpl).getGiftCardPayments(cartModel);

        targetPaymentServiceImpl.reverseGiftCardPayment(cartModel);

        verify(targetPaymentServiceImpl, never()).reversePayment(Mockito.any(CartModel.class),
                Mockito.anyList());
    }

    @Test
    public void testReverseGiftCardSuccessfulWithNoCart() {
        final TargetRetrieveTransactionResult retrieveResult = mock(TargetRetrieveTransactionResult.class);
        given(ipgPaymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class))).willReturn(
                retrieveResult);
        final PaymentTransactionEntryData paymentTransactionEntryData = new PaymentTransactionEntryData();
        given(retrieveResult.getPayments()).willReturn(ImmutableList.of(paymentTransactionEntryData));
        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        giftCards.add(giftCard);
        final TargetCardPaymentResult targetCardPaymentResult = new TargetCardPaymentResult();
        giftCard.setTargetCardPaymentResult(targetCardPaymentResult);
        targetCardPaymentResult.setResponseCode(TargetCardPaymentResult.RESPONSE_CODE_SUCCESS);

        targetPaymentServiceImpl.reversePayment(null, giftCards);

        verify(ipgPaymentMethod).voidPayment(any(TargetPaymentVoidRequest.class));
    }

    @Test
    public void testCreateRetrieveTransacitonRequestForIpg() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final Date date = mock(Date.class);
        given(paymentTransaction.getOrder()).willReturn(abstractOrderModel);
        given(abstractOrderModel.getCode()).willReturn("001");
        given(abstractOrderModel.getDate()).willReturn(date);
        given(entry.getAmount()).willReturn(BigDecimal.TEN);
        given(paymentTransaction.getInfo()).willReturn(ipgPaymentInfo);
        final TargetRetrieveTransactionRequest request = targetPaymentServiceImpl.createRetrieveTransacitonRequest(
                entry, paymentTransaction);

        assertThat(request.getOrderId()).isEqualTo("001");
        assertThat(request.getOrderDate()).isEqualTo(date);
        assertThat(request.getEntryAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(request.isQueryGiftCardOnly()).isFalse();
    }

    @Test
    public void testCreateRetrieveTransacitonRequestForIpgGiftCard() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final Date date = mock(Date.class);
        given(paymentTransaction.getOrder()).willReturn(abstractOrderModel);
        given(abstractOrderModel.getCode()).willReturn("001");
        given(abstractOrderModel.getDate()).willReturn(date);
        given(entry.getAmount()).willReturn(BigDecimal.TEN);
        given(paymentTransaction.getInfo()).willReturn(ipgPaymentInfo);
        given(entry.getIpgPaymentInfo()).willReturn(ipgGiftCardPaymentInfoModel);
        final TargetRetrieveTransactionRequest request = targetPaymentServiceImpl.createRetrieveTransacitonRequest(
                entry, paymentTransaction);

        assertThat(request.getOrderId()).isEqualTo("001");
        assertThat(request.getOrderDate()).isEqualTo(date);
        assertThat(request.getEntryAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(request.isQueryGiftCardOnly()).isTrue();
        assertThat(request.getTrnType()).isEqualTo(TrnType.APPROVED);
    }

    @Test
    public void testCreateRetrieveTransacitonRequestForAfterpay() {
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final Date date = mock(Date.class);
        given(paymentTransaction.getOrder()).willReturn(abstractOrderModel);
        given(abstractOrderModel.getCode()).willReturn("001");
        given(abstractOrderModel.getDate()).willReturn(date);
        given(paymentTransaction.getInfo()).willReturn(afterpayPaymentInfoModel);
        final TargetRetrieveTransactionRequest request = targetPaymentServiceImpl.createRetrieveTransacitonRequest(
                entry, paymentTransaction);

        assertThat(request.getSessionToken()).isEqualTo(afterpayToken);

    }

    @Test
    public void testCreateReverseProcessGetNameFromShippingAddressWithTitle() {
        final TargetCardResult card = Mockito.mock(TargetCardResult.class);
        final TargetCardPaymentResult targetCardPaymentResult = Mockito.mock(TargetCardPaymentResult.class);
        given(targetCardPaymentResult.getAmount()).willReturn(BigDecimal.TEN);
        given(targetCardPaymentResult.getReceiptNumber()).willReturn("test number");
        given(card.getTargetCardPaymentResult()).willReturn(targetCardPaymentResult);
        given(card.getCardNumber()).willReturn("4242424242424242");
        final AddressModel shipAddress = Mockito.mock(AddressModel.class);
        final TitleModel shippingTitle = Mockito.mock(TitleModel.class);
        given(shippingTitle.getName()).willReturn("Mr");
        given(shipAddress.getTitle()).willReturn(shippingTitle);
        given(shipAddress.getPhone1()).willReturn("123456");
        given(shipAddress.getFirstname()).willReturn("firstName");
        given(shipAddress.getLastname()).willReturn("lastName");
        given(cartModel.getDeliveryAddress()).willReturn(shipAddress);
        final AddressModel billingAddress = Mockito.mock(AddressModel.class);
        final TitleModel title = Mockito.mock(TitleModel.class);
        given(title.getName()).willReturn("Mr");
        given(billingAddress.getTitle()).willReturn(title);
        given(billingAddress.getFirstname()).willReturn("firstName");
        given(billingAddress.getLastname()).willReturn("lastName");
        given(shipAddress.getPhone1()).willReturn("123456");
        final PaymentInfoModel paymentInfoModel1 = Mockito.mock(PaymentInfoModel.class);
        given(paymentInfoModel1.getBillingAddress()).willReturn(null);
        given(cartModel.getPaymentInfo()).willReturn(paymentInfoModel1);
        given(cartModel.getCode()).willReturn("test cart id");
        targetPaymentServiceImpl.createReverseProcess(cartModel, card);
        verify(businessProcess).startGiftCardReverseProcess(Mockito.any(GiftCardReversalData.class),
                Mockito.anyString(), Mockito.anyString(), Mockito.any(PaymentTransactionEntryModel.class));
        verify(shipAddress).getPhone1();
        verify(card).getCardNumber();
        verify(targetCardPaymentResult).getAmount();
        verify(targetCardPaymentResult).getReceiptNumber();
        verify(cartModel).getCode();
        verify(shippingTitle).getName();
        verify(shipAddress).getFirstname();
        verify(shipAddress).getLastname();
    }

    @Test
    public void testCreateReverseProcessGetNameFromShippingAddressWithoutTitle() {
        final TargetCardResult card = Mockito.mock(TargetCardResult.class);
        final TargetCardPaymentResult targetCardPaymentResult = Mockito.mock(TargetCardPaymentResult.class);
        given(targetCardPaymentResult.getAmount()).willReturn(BigDecimal.TEN);
        given(targetCardPaymentResult.getReceiptNumber()).willReturn("test number");
        given(card.getTargetCardPaymentResult()).willReturn(targetCardPaymentResult);
        given(card.getCardNumber()).willReturn("4242424242424242");
        final AddressModel shipAddress = Mockito.mock(AddressModel.class);
        given(shipAddress.getPhone1()).willReturn("123456");
        given(shipAddress.getFirstname()).willReturn("firstName");
        given(shipAddress.getLastname()).willReturn("lastName");
        given(cartModel.getDeliveryAddress()).willReturn(shipAddress);
        final AddressModel billingAddress = Mockito.mock(AddressModel.class);
        final TitleModel title = Mockito.mock(TitleModel.class);
        given(title.getName()).willReturn("Mr");
        given(billingAddress.getTitle()).willReturn(title);
        given(billingAddress.getFirstname()).willReturn("firstName");
        given(billingAddress.getLastname()).willReturn("lastName");
        given(shipAddress.getPhone1()).willReturn("123456");
        final PaymentInfoModel paymentInfoModel1 = Mockito.mock(PaymentInfoModel.class);
        given(paymentInfoModel1.getBillingAddress()).willReturn(null);
        given(cartModel.getPaymentInfo()).willReturn(paymentInfoModel1);
        given(cartModel.getCode()).willReturn("test cart id");
        targetPaymentServiceImpl.createReverseProcess(cartModel, card);
        verify(businessProcess).startGiftCardReverseProcess(Mockito.any(GiftCardReversalData.class),
                Mockito.anyString(), Mockito.anyString(), Mockito.any(PaymentTransactionEntryModel.class));
        verify(shipAddress).getPhone1();
        verify(card).getCardNumber();
        verify(targetCardPaymentResult).getAmount();
        verify(targetCardPaymentResult).getReceiptNumber();
        verify(cartModel).getCode();
        verify(shipAddress).getFirstname();
        verify(shipAddress).getLastname();
    }

    @Test
    public void testCaptureAcceptedPaymentTransactionLogging() {

        final PaymentTransactionEntryModel payTransEntry = new PaymentTransactionEntryModel();
        payTransEntry.setAmount(BigDecimal.ONE);
        entries.add(payTransEntry);
        payTransEntry.setPaymentTransaction(paymentTransactionModel);

        capturePaymentTransactionLoggingUtil(TransactionStatus.ACCEPTED, afterpayPaymentInfoModel);

        final PaymentInfoModel paymentInfoMod = checkPaymentType(1, false, true, false);

        final PaymentTransactionEntryModel paytranModel = targetPaymentServiceImpl.capture(orderModel, paymentInfoMod,
                BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, "123456", payTransEntry);

        assertThat(paytranModel.getTransactionStatus().equals(TransactionStatus.ACCEPTED));

    }


    @Test
    public void testCaptureRejectedPaymentTransactionLogging() {

        final PaymentTransactionEntryModel payTransEntry = new PaymentTransactionEntryModel();
        payTransEntry.setAmount(BigDecimal.ONE);
        entries.add(payTransEntry);
        payTransEntry.setPaymentTransaction(paymentTransactionModel);

        capturePaymentTransactionLoggingUtil(TransactionStatus.REJECTED, ipgPaymentInfo);

        final PaymentTransactionEntryModel paytranModel = targetPaymentServiceImpl.capture(orderModel,
                paymentInfoModel,
                BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, "123456", payTransEntry);
        assertThat(paytranModel.getTransactionStatus().equals(TransactionStatus.REJECTED));

    }

    @Test
    public void testCaptureReviewPaymentTransactionLogging() {

        final PaymentTransactionEntryModel payTransEntry = new PaymentTransactionEntryModel();
        payTransEntry.setAmount(BigDecimal.ONE);
        entries.add(payTransEntry);
        payTransEntry.setPaymentTransaction(paymentTransactionModel);
        getCardResultForCreditCard();
        payTransEntry.setIpgPaymentInfo(ipgCreditCardPaymentInfoModel);

        capturePaymentTransactionLoggingUtil(TransactionStatus.REVIEW, ipgPaymentInfo);

        final PaymentInfoModel paymentInfoMod = checkPaymentType(1, true, false, false);

        final PaymentTransactionEntryModel paytranModel = targetPaymentServiceImpl.capture(orderModel, paymentInfoMod,
                BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, "123456", payTransEntry);
        assertThat(paytranModel.getTransactionStatus().equals(TransactionStatus.REVIEW));

    }

    @Test
    public void testCaptureErrorPaymentTransactionLogging() {

        final PaymentTransactionEntryModel payTransEntry = new PaymentTransactionEntryModel();
        payTransEntry.setAmount(BigDecimal.ONE);
        entries.add(payTransEntry);
        payTransEntry.setPaymentTransaction(paymentTransactionModel);

        capturePaymentTransactionLoggingUtil(TransactionStatus.ERROR, ipgPaymentInfo);

        final PaymentInfoModel paymentInfoMod = checkPaymentType(1, false, false, true);

        final PaymentTransactionEntryModel paytranModel = targetPaymentServiceImpl.capture(orderModel, paymentInfoMod,
                BigDecimal.TEN, currencyModel,
                PaymentCaptureType.PLACEORDER, "123456", payTransEntry);
        assertThat(paytranModel.getTransactionStatus().equals(TransactionStatus.ERROR));

    }

    @Test
    public void testCreatePaymentTransactionWithQueryResultPreOrder() {

        final TargetQueryTransactionDetailsResult queryTransactionResult = new TargetQueryTransactionDetailsResult();
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        queryTransactionResult.addCardResult(cardResult);
        queryTransactionResult.addCardResult(cardResult);
        final List<TargetCardResult> savedCards = Collections.singletonList(cardResult);
        willReturn(Boolean.TRUE).given(cardResult).isDefaultCard();
        queryTransactionResult.setSavedCards(savedCards);
        queryTransactionResult.setSuccess(true);

        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);
        given(cardResult.getCardType()).willReturn(CreditCardType.VISA.getCode());

        final BigDecimal amount = new BigDecimal("10");
        given(ipgTransactionResult.getAmount()).willReturn(amount);
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);
        final Date normalSaleStartDate = new Date();
        willReturn(normalSaleStartDate).given(abstractOrderModel).getNormalSaleStartDateTime();
        given(abstractOrderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.67d));

        final PaymentTransactionModel curTransaction = targetPaymentServiceImpl.createTransactionWithQueryResult(
                abstractOrderModel,
                queryTransactionResult);

        assertThat(curTransaction).isEqualTo(paymentTransactionModel);
        verify(paymentTransactionModel).setOrder(abstractOrderModel);
        verify(targetPaymentServiceImpl, times(2)).saveCardResult(abstractOrderModel, cardResult,
                paymentTransactionEntryModel);
        verify(modelService, times(2)).save(paymentTransactionModel);
        verify(curTransaction).setPlannedAmount(new BigDecimal("10.67"));
        verify(abstractOrderModel, never()).getTotalPrice();

    }

    @Test
    public void testCreatePaymentTransactionWithQueryDeferredTransactionType() {

        final TargetQueryTransactionDetailsResult queryTransactionResult = new TargetQueryTransactionDetailsResult();
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        queryTransactionResult.addCardResult(cardResult);
        final List<TargetCardResult> savedCards = Collections.singletonList(cardResult);
        willReturn(Boolean.TRUE).given(cardResult).isDefaultCard();
        queryTransactionResult.setSavedCards(savedCards);
        queryTransactionResult.setSuccess(true);

        given(ipgTransactionResult.getAmount()).willReturn(BigDecimal.valueOf(390.0));
        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);
        given(cardResult.getCardType()).willReturn(CreditCardType.VISA.getCode());

        given(abstractOrderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.0));
        given(abstractOrderModel.getTotalPrice()).willReturn(Double.valueOf(400.0));
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);
        given(paymentTransactionModel.getOrder()).willReturn(abstractOrderModel);
        given(paymentTransactionModel.getPlannedAmount()).willReturn(BigDecimal.valueOf(390.0));

        final Date normalSaleStartDate = new Date();
        willReturn(normalSaleStartDate).given(abstractOrderModel).getNormalSaleStartDateTime();

        final PaymentTransactionModel curTransaction = targetPaymentServiceImpl.createTransactionWithQueryResult(
                abstractOrderModel,
                queryTransactionResult, PaymentTransactionType.DEFERRED);

        assertThat(curTransaction).isEqualTo(paymentTransactionModel);
        verify(curTransaction).setPlannedAmount(BigDecimal.valueOf(390.0));
        verify(paymentTransactionEntryModel).setAmount(BigDecimal.valueOf(390.0));
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.DEFERRED);
        verify(paymentTransactionModel).setOrder(abstractOrderModel);
        verify(targetPaymentServiceImpl).saveCardResult(abstractOrderModel, cardResult,
                paymentTransactionEntryModel);
        verify(modelService).save(paymentTransactionModel);

    }

    /**
     * Utility stub method. Capture payment logging.
     * 
     * @param transactionStatus
     * @param paymentInnfoModel
     */
    private void capturePaymentTransactionLoggingUtil(final TransactionStatus transactionStatus,
            final PaymentInfoModel paymentInnfoModel) {
        given(paymentTransactionModel.getEntries()).willReturn(entries);
        given(abstractOrderModel.getPaymentInfo()).willReturn(paymentInnfoModel);
        given(ipgPaymentInfo.getSessionId()).willReturn(ipgSessionId);
        given(ipgPaymentInfo.getToken()).willReturn(ipgToken);
        given(paymentMethod.capture(any(TargetCaptureRequest.class))).willReturn(captureResult);
        given(orderModel.getPaymentInfo()).willReturn(paymentInnfoModel);

        final TargetCaptureResult targetCaptureResult = mock(TargetCaptureResult.class);
        given(targetCaptureResult.getRequestId()).willReturn(REQUEST_ID);
        given(targetCaptureResult.getRequestToken()).willReturn(REQUEST_TOKEN);
        given(targetCaptureResult.getTransactionStatus()).willReturn(transactionStatus);

        given(paymentMethod.capture(any(TargetCaptureRequest.class))).willReturn(targetCaptureResult);
    }

    /**
     * Method to check payment type.
     * 
     * @param num
     * @param isGiftCardOrCreditCard
     * @param isAfterPay
     * @param isPayPal
     * @return paymentInfoMethod
     */
    private PaymentInfoModel checkPaymentType(final long num, final boolean isGiftCardOrCreditCard,
            final boolean isAfterPay, final boolean isPayPal) {
        PaymentInfoModel paymentInfoMod = null;

        if (isGiftCardOrCreditCard) {
            paymentInfoMod = mock(IpgPaymentInfoModel.class);
        }
        else if (isAfterPay) {
            paymentInfoMod = mock(AfterpayPaymentInfoModel.class);
        }
        else if (isPayPal) {
            paymentInfoMod = mock(PaypalPaymentInfoModel.class);
        }
        else {
            paymentInfoMod = mock(PaymentInfoModel.class);
        }

        given(paymentInfoMod.getPk()).willReturn(PK.createFixedCounterPK(1, num));

        return paymentInfoMod;
    }

    /**
     * Method to fetch credit card details.
     */
    private void getCardResultForCreditCard() {

        final String creditCardNumber = "4242424242424242";
        final String creditCardToken = "1212121";
        final String creditCardValidTillMonth = "03";
        final String creditCardValidTillYear = "20";
        final String creditCardBin = "34";
        final String creditCardPromoId = "123";
        final String creditCardTokenExpiry = "01/01/2020";

        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgCreditCardPaymentInfoModel);
        given(ipgCreditCardPaymentInfoModel.getNumber()).willReturn(creditCardNumber);
        given(ipgCreditCardPaymentInfoModel.getType()).willReturn(CreditCardType.VISA);
        given(ipgCreditCardPaymentInfoModel.getToken()).willReturn(creditCardToken);
        given(ipgCreditCardPaymentInfoModel.getValidToMonth()).willReturn(creditCardValidTillMonth);
        given(ipgCreditCardPaymentInfoModel.getValidFromYear()).willReturn(creditCardValidTillYear);
        given(ipgCreditCardPaymentInfoModel.getTokenExpiry()).willReturn(creditCardTokenExpiry);
        given(ipgCreditCardPaymentInfoModel.getBin()).willReturn(creditCardBin);
        given(ipgCreditCardPaymentInfoModel.getPromoId()).willReturn(creditCardPromoId);

    }

    private void givenCartHasIpgPayment() {
        given(paymentMethodStrategy.getPaymentMethod(Mockito.any(PaymentModeModel.class))).willReturn(
                paymentMethod);
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        final TargetRetrieveTransactionResult retrieveResult = mock(TargetRetrieveTransactionResult.class);
        given(ipgPaymentMethod.retrieveTransaction(any(TargetRetrieveTransactionRequest.class))).willReturn(
                retrieveResult);
        given(retrieveResult.getPayments()).willReturn(ImmutableList.of(new PaymentTransactionEntryData()));
    }

    @SuppressWarnings("boxing")
    private TargetPaymentVoidResult createVoidResult(final boolean successful) {
        final TargetPaymentVoidResult voidResult = Mockito.mock(TargetPaymentVoidResult.class);
        final TargetCardPaymentResult result = Mockito.mock(TargetCardPaymentResult.class);
        given(voidResult.getVoidResult()).willReturn(result);
        given(result.isPaymentSuccessful()).willReturn(successful);
        return voidResult;
    }

    private List<TargetCardResult> createGiftCardList(final BigDecimal amount, final int size) {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            final TargetCardResult giftCard = new TargetCardResult();
            final TargetCardPaymentResult transactionResult = new TargetCardPaymentResult();
            transactionResult.setResponseCode("0");
            transactionResult.setReceiptNumber("123456789");
            transactionResult.setAmount(amount);
            giftCard.setTargetCardPaymentResult(transactionResult);
            giftCards.add(giftCard);
        }
        return giftCards;
    }

    @Test
    public void testCreateHostedSessionTokenRequestForIpg() {
        final BigDecimal amount = new BigDecimal("10.99");
        final String cancelUrl = "Cancel url";
        final PaymentCaptureType paymentCaptureType = PaymentCaptureType.PLACEORDER;
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        final String returnUrl = "Return url";
        final String sessionId = "123456789";

        final CartModel cart = mock(CartModel.class);
        given(cart.getUser()).willReturn(customerModel);

        final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = mock(IpgCreditCardPaymentInfoModel.class);
        final String token = "AHDLFH2131564";
        final String tokenExpiry = "01/01/2016";
        final CreditCardType cardType = CreditCardType.VISA;
        final String cardNumber = "45213*******3256";
        final String validToMonth = "03";
        final String validToYear = "17";
        final String bin = "34";
        final Boolean isDefaultCard = Boolean.TRUE;
        final String promoId = "123";

        given(ipgCreditCardPaymentInfoModel.getToken()).willReturn(token);
        given(ipgCreditCardPaymentInfoModel.getTokenExpiry()).willReturn(tokenExpiry);
        given(ipgCreditCardPaymentInfoModel.getType()).willReturn(cardType);
        given(ipgCreditCardPaymentInfoModel.getNumber()).willReturn(cardNumber);
        given(ipgCreditCardPaymentInfoModel.getValidToMonth()).willReturn(validToMonth);
        given(ipgCreditCardPaymentInfoModel.getValidToYear()).willReturn(validToYear);
        given(ipgCreditCardPaymentInfoModel.getBin()).willReturn(bin);
        given(ipgCreditCardPaymentInfoModel.getDefaultCard()).willReturn(isDefaultCard);
        given(ipgCreditCardPaymentInfoModel.getPromoId()).willReturn(promoId);
        willReturn(Double.valueOf(10.99)).given(cart).getTotalPrice();

        final List<CreditCardPaymentInfoModel> savedCards = new ArrayList<>();
        savedCards.add(ipgCreditCardPaymentInfoModel);

        final HostedSessionTokenRequest hostedSessionTokenRequest = targetPaymentServiceImpl
                .createHostedSessionTokenRequest(cart, paymentModeModel, cancelUrl, returnUrl, paymentCaptureType,
                        sessionId,
                        savedCards);

        assertThat(hostedSessionTokenRequest).isNotNull();
        assertThat(hostedSessionTokenRequest.getAmount()).isEqualTo(amount);
        assertThat(hostedSessionTokenRequest.getCancelUrl()).isEqualTo(cancelUrl);
        assertThat(hostedSessionTokenRequest.getOrderModel()).isEqualTo(cart);
        assertThat(hostedSessionTokenRequest.getPaymentCaptureType()).isEqualTo(paymentCaptureType);
        assertThat(hostedSessionTokenRequest.getPaymentMode()).isEqualTo(paymentModeModel);
        assertThat(hostedSessionTokenRequest.getReturnUrl()).isEqualTo(returnUrl);
        assertThat(hostedSessionTokenRequest.getSessionId()).isEqualTo(sessionId);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards()).isNotNull();
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().size()).isEqualTo(1);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getToken()).isEqualTo(token);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getTokenExpiry()).isEqualTo(tokenExpiry);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getCardType()).isEqualTo(cardType.toString());
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getMaskedCard()).isEqualTo(cardNumber);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getCardExpiry())
                .isEqualTo(validToMonth + "/" + validToYear);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getBin()).isEqualTo(bin);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getDefaultCard()).isEqualTo(isDefaultCard);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getPromoId()).isEqualTo(promoId);
    }

    @Test
    public void testAddSavedCardInformation() {
        final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = mock(IpgCreditCardPaymentInfoModel.class);

        final String token = "AHDLFH2131564";
        final String tokenExpiry = "01/01/2016";
        final CreditCardType cardType = CreditCardType.VISA;
        final String cardNumber = "45213*******3256";
        final String validToMonth = "03";
        final String validToYear = "17";
        final String bin = "34";
        final Boolean isDefaultCard = Boolean.TRUE;
        final String promoId = "123";
        final CartModel cart = mock(CartModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);

        given(ipgCreditCardPaymentInfoModel.getToken()).willReturn(token);
        given(ipgCreditCardPaymentInfoModel.getTokenExpiry()).willReturn(tokenExpiry);
        given(ipgCreditCardPaymentInfoModel.getType()).willReturn(cardType);
        given(ipgCreditCardPaymentInfoModel.getNumber()).willReturn(cardNumber);
        given(ipgCreditCardPaymentInfoModel.getValidToMonth()).willReturn(validToMonth);
        given(ipgCreditCardPaymentInfoModel.getValidToYear()).willReturn(validToYear);
        given(ipgCreditCardPaymentInfoModel.getBin()).willReturn(bin);
        given(ipgCreditCardPaymentInfoModel.getDefaultCard()).willReturn(isDefaultCard);
        given(ipgCreditCardPaymentInfoModel.getPromoId()).willReturn(promoId);

        final List<CreditCardPaymentInfoModel> savedCards = new ArrayList<>();
        savedCards.add(ipgCreditCardPaymentInfoModel);

        final HostedSessionTokenRequest hostedSessionTokenRequest = targetPaymentServiceImpl
                .createHostedSessionTokenRequest(cart, paymentModeModel, null, null, PaymentCaptureType.PLACEORDER,
                        "1234",
                        savedCards);

        assertThat(hostedSessionTokenRequest).isNotNull();
        assertThat(hostedSessionTokenRequest.getSavedCreditCards()).isNotNull();
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().size()).isEqualTo(1);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getToken()).isEqualTo(token);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getTokenExpiry()).isEqualTo(tokenExpiry);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getCardType()).isEqualTo(cardType.toString());
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getMaskedCard()).isEqualTo(cardNumber);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getCardExpiry())
                .isEqualTo(validToMonth + "/" + validToYear);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getBin()).isEqualTo(bin);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getDefaultCard()).isEqualTo(isDefaultCard);
        assertThat(hostedSessionTokenRequest.getSavedCreditCards().get(0).getPromoId()).isEqualTo(promoId);
    }




    @Test
    public void testIPGCreateHostedSessionNoSavedCardsSendToIPGForPreOrder() {
        final String cancelUrl = "Cancel url";
        final PaymentCaptureType paymentCaptureType = PaymentCaptureType.PLACEORDER;
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        final String returnUrl = "Return url";
        final String sessionId = "123456789";

        final CartModel cart = mock(CartModel.class);
        given(cart.getUser()).willReturn(customerModel);
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        willReturn(Double.valueOf(10.99)).given(cart).getTotalPrice();

        final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = mock(IpgCreditCardPaymentInfoModel.class);

        final List<CreditCardPaymentInfoModel> savedCards = new ArrayList<>();
        savedCards.add(ipgCreditCardPaymentInfoModel);

        final HostedSessionTokenRequest hostedSessionTokenRequest = targetPaymentServiceImpl
                .createHostedSessionTokenRequest(cart, paymentModeModel, cancelUrl, returnUrl, paymentCaptureType,
                        sessionId,
                        savedCards);

        assertThat(hostedSessionTokenRequest.getSavedCreditCards()).isNull();
    }

    @Test
    public void testAddNullSavedCardInformation() {
        final CartModel cart = mock(CartModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        final PaymentCaptureType pct = PaymentCaptureType.PLACEORDER;
        final String sessionId = "123456";

        willReturn(Double.valueOf(10.99)).given(cart).getTotalPrice();
        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();
        targetPaymentServiceImpl
                .createHostedSessionTokenRequest(cart, paymentModeModel, null, null, pct,
                        sessionId,
                        null);

        assertThat(hostedSessionTokenRequest).isNotNull();
        assertThat(hostedSessionTokenRequest.getSavedCreditCards()).isNull();
    }

    @Test
    public void testAddEmptySavedCardInformation() {
        final CartModel cart = mock(CartModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        final PaymentCaptureType pct = PaymentCaptureType.PLACEORDER;
        final String sessionId = "123456";
        final List<CreditCardPaymentInfoModel> savedCards = new ArrayList<>();

        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();
        willReturn(Double.valueOf(10.99)).given(cart).getTotalPrice();
        targetPaymentServiceImpl
                .createHostedSessionTokenRequest(cart, paymentModeModel, null, null, pct,
                        sessionId,
                        savedCards);

        assertThat(hostedSessionTokenRequest).isNotNull();
        assertThat(hostedSessionTokenRequest.getSavedCreditCards()).isNull();
    }

    private HostedSessionTokenRequest createHostedSessionTokenRequest() {
        final PaymentModeModel creditCard = Mockito.mock(PaymentModeModel.class);
        final HostedSessionTokenRequest hstr = new HostedSessionTokenRequest();
        hstr.setAmount(BigDecimal.valueOf(10.9));
        hstr.setCancelUrl("test");
        hstr.setReturnUrl("test");
        hstr.setPaymentCaptureType(PaymentCaptureType.PLACEORDER);
        hstr.setPaymentMode(creditCard);
        hstr.setOrderModel(orderModel);
        hstr.setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        return hstr;
    }

    @Test
    public void testCreateUpdateCardHostedSessionTokenRequestForIpg() {
        final BigDecimal amount = new BigDecimal("90.99");
        final Double preOrderDeposit = new Double("10.00");
        final Double orderAmount = new Double("100.99");
        final String cancelUrl = "Cancel url";
        final PaymentCaptureType paymentCaptureType = PaymentCaptureType.PLACEORDER;
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        final String returnUrl = "Return url";
        final String sessionId = "123456789";

        final OrderModel order = mock(OrderModel.class);
        willReturn(orderAmount).given(order).getTotalPrice();
        willReturn(preOrderDeposit).given(order).getPreOrderDepositAmount();

        final HostedSessionTokenRequest hostedSessionTokenRequest = targetPaymentServiceImpl
                .createUpdateCardHostedSessionTokenRequest(order, paymentModeModel, cancelUrl, returnUrl,
                        paymentCaptureType, sessionId);

        assertThat(hostedSessionTokenRequest).isNotNull();
        assertThat(hostedSessionTokenRequest.getAmount()).isEqualTo(amount);
        assertThat(hostedSessionTokenRequest.getCancelUrl()).isEqualTo(cancelUrl);
        assertThat(hostedSessionTokenRequest.getOrderModel()).isEqualTo(order);
        assertThat(hostedSessionTokenRequest.getPaymentCaptureType()).isEqualTo(paymentCaptureType);
        assertThat(hostedSessionTokenRequest.getPaymentMode()).isEqualTo(paymentModeModel);
        assertThat(hostedSessionTokenRequest.getReturnUrl()).isEqualTo(returnUrl);
        assertThat(hostedSessionTokenRequest.getSessionId()).isEqualTo(sessionId);
        assertThat(hostedSessionTokenRequest.getIpgPaymentTemplateType()).isEqualTo(IpgPaymentTemplateType.UPDATECARD);
    }

    @Test
    public void testPing() {
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);
        final TargetPaymentPingResult result = new TargetPaymentPingResult();
        result.setSuccess(true);

        given(paymentMethod.ping(any(TargetPaymentPingRequest.class))).willReturn(result);
        final boolean output = targetPaymentServiceImpl.ping(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE);
        assertThat(output).isTrue();
    }

    @Test
    public void testPingWhenResultNull() {
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);

        given(paymentMethod.ping(any(TargetPaymentPingRequest.class))).willReturn(null);
        final boolean output = targetPaymentServiceImpl.ping(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE);
        assertThat(output).isFalse();
    }

    @Test
    public void testSaveCardResultsForPreOrderBalancePayment() {
        final TargetCardResult cardResult = mock(TargetCardResult.class);
        final TargetCardPaymentResult ipgTransactionResult = mock(TargetCardPaymentResult.class);
        given(cardResult.getTargetCardPaymentResult()).willReturn(ipgTransactionResult);
        given(cardResult.getCardType()).willReturn(CreditCardType.VISA.getCode());
        given(abstractOrderModel.getTotalPrice()).willReturn(Double.valueOf(100d));
        given(abstractOrderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10d));
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.DEFERRED);
        given(abstractOrderModel.getNormalSaleStartDateTime()).willReturn(new Date());

        given(ipgTransactionResult.getAmount()).willReturn(BigDecimal.valueOf(100.0));

        final List<TargetCardResult> cardResults = new ArrayList<>();
        cardResults.add(cardResult);
        doReturn(ipgCreditCardPaymentInfoModel).when(targetPaymentServiceImpl).createIpgCreditCardPaymentInfoModel(
                abstractOrderModel,
                cardResult);

        targetPaymentServiceImpl.saveCardResult(abstractOrderModel, cardResults.get(0), paymentTransactionEntryModel);
        verify(paymentTransactionEntryModel).setAmount(new BigDecimal("90.00"));
        verify(paymentTransactionEntryModel).setIpgPaymentInfo(ipgCreditCardPaymentInfoModel);
        verify(ipgCreditCardPaymentInfoModel).setOwner(paymentTransactionEntryModel);
        verify(ipgCreditCardPaymentInfoModel).setDuplicate(Boolean.TRUE);
        verify(modelService).saveAll(paymentTransactionEntryModel, ipgCreditCardPaymentInfoModel);
    }

    @Test
    public void testVerifyIfPaymentCapturedAndUpdateEntryWhenPaymentNotCaptured() {

        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        transaction.setOrder(abstractOrderModel);
        final IpgPaymentInfoModel realPaymentInfoModel = new IpgPaymentInfoModel();
        realPaymentInfoModel.setSessionId("SessionId");
        realPaymentInfoModel.setToken("token");
        transaction.setPaymentProvider(IPG_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);
        given(paymentTransactionEntryModel.getCode()).willReturn("abc");
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(transaction);

        given(paymentMethodStrategy.getPaymentMethod(realPaymentInfoModel, transaction.getPaymentProvider()))
                .willReturn(paymentMethod);

        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();
        result.setRequestId(REQUEST_ID);
        result.setRequestToken(REQUEST_TOKEN);

        final ArgumentCaptor<TargetRetrieveTransactionRequest> argumentCaptor = ArgumentCaptor
                .forClass(TargetRetrieveTransactionRequest.class);

        given(paymentMethod.retrieveTransaction(argumentCaptor.capture())).willReturn(result);

        final boolean isCaptured = targetPaymentServiceImpl
                .verifyIfPaymentCapturedAndUpdateEntry(paymentTransactionEntryModel);
        assertThat(isCaptured).isFalse();
        assertThat(argumentCaptor.getValue().getTransactionId()).isEqualTo("abc");
        assertThat(argumentCaptor.getValue().getSessionId()).isEqualTo(realPaymentInfoModel.getSessionId());
        assertThat(argumentCaptor.getValue().getSessionToken()).isEqualTo(realPaymentInfoModel.getToken());

        verifyZeroInteractions(modelService);
    }


    @Test
    public void testVerifyIfPaymentCapturedAndUpdateEntryWhenPaymentAlreadyCaptured() {

        final PaymentTransactionModel transaction = new PaymentTransactionModel();
        transaction.setOrder(abstractOrderModel);
        final IpgPaymentInfoModel realPaymentInfoModel = new IpgPaymentInfoModel();
        realPaymentInfoModel.setSessionId("SessionId");
        realPaymentInfoModel.setToken("token");
        transaction.setPaymentProvider(IPG_PAYMENT_PROVIDER);
        transaction.setInfo(realPaymentInfoModel);
        given(paymentTransactionEntryModel.getCode()).willReturn("abc");
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(transaction);
        final IpgCreditCardPaymentInfoModel ccDetails = new IpgCreditCardPaymentInfoModel();
        ccDetails.setToken("token123");
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ccDetails);

        given(paymentMethodStrategy.getPaymentMethod(realPaymentInfoModel, transaction.getPaymentProvider()))
                .willReturn(paymentMethod);

        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();
        result.setMatchingCount(1);
        result.setRequestId(REQUEST_ID);
        result.setRequestToken(REQUEST_TOKEN);

        final ArgumentCaptor<TargetRetrieveTransactionRequest> argumentCaptor = ArgumentCaptor
                .forClass(TargetRetrieveTransactionRequest.class);

        given(paymentMethod.retrieveTransaction(argumentCaptor.capture())).willReturn(result);

        final boolean isCaptured = targetPaymentServiceImpl
                .verifyIfPaymentCapturedAndUpdateEntry(paymentTransactionEntryModel);
        assertThat(isCaptured).isTrue();
        assertThat(argumentCaptor.getValue().getTransactionId()).isEqualTo("abc");
        assertThat(argumentCaptor.getValue().getSessionId()).isEqualTo(realPaymentInfoModel.getSessionId());
        assertThat(argumentCaptor.getValue().getSessionToken()).isEqualTo(realPaymentInfoModel.getToken());

        verify(paymentTransactionEntryModel).setRequestToken("token123");
        verify(modelService).save(paymentTransactionEntryModel);
        verify(modelService).refresh(paymentTransactionEntryModel);
    }


    @Test
    public void testSetZipPaymentDetailsAfterCapture() {
        final BigDecimal amount = BigDecimal.valueOf(10);

        given(paymentMethod.getPaymentProvider()).willReturn("Zippay");

        final TargetCaptureResult result = Mockito.mock(TargetCaptureResult.class);
        given(result.getRequestId()).willReturn("1234556");
        given(result.getReconciliationId()).willReturn("111234555");
        given(result.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED);
        given(result.getTransactionStatusDetails()).willReturn(TransactionStatusDetails.SUCCESFULL);
        given(paymentMethod.capture(Mockito.any(TargetCaptureRequest.class))).willReturn(
                result);

        given(orderModel.getPaymentInfo()).willReturn(zippayPaymentInfoModel);
        given(orderModel.getPaymentAddress()).willReturn(address);
        final PaymentTransactionEntryModel entry = targetPaymentServiceImpl.capture(orderModel,
                zippayPaymentInfoModel,
                amount, currencyModel, null);

        assertThat(entry).isNotNull();
        verify(zippayPaymentInfoModel).setReceiptNo("111234555");
        verify(zippayPaymentInfoModel).setChargeId("1234556");
        verify(zippayPaymentInfoModel).setBillingAddress(address);

        // Verify the transaction and transaction entry are saved with the initial values
        verify(paymentTransactionModel).setInfo(zippayPaymentInfoModel);
        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);
        verify(paymentMethod).capture(Mockito.any(TargetCaptureRequest.class));

        // Verify the transaction entry is updated with the capture result
        verify(paymentTransactionEntryModel).setRequestId("1234556");
        verify(paymentTransactionEntryModel).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        verify(paymentTransactionEntryModel).setTransactionStatusDetails(
                TransactionStatusDetails.SUCCESFULL.toString());
        verify(modelService).saveAll(Mockito.anyCollection());
        verify(modelService).save(zippayPaymentInfoModel);
        verify(modelService).save(paymentTransactionEntryModel);
    }
}