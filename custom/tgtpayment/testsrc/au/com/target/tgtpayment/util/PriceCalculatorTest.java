package au.com.target.tgtpayment.util;

import static au.com.target.tgtpayment.util.PriceCalculator.add;
import static au.com.target.tgtpayment.util.PriceCalculator.subtract;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigDecimal;

import org.junit.Test;


/**
 * Test suite for {@link PriceCalculator}.
 */
@UnitTest
public class PriceCalculatorTest {

    /**
     * Verifies that result of addition is rounded to the scale of {@code 2} using
     * {@link java.math.RoundingMode#HALF_UP} rounding mode.
     */
    @Test
    public void testAdditionRounding() {
        assertEquals(BigDecimal.valueOf(7.44d), add(Double.valueOf(2.993d), Double.valueOf(4.442d)));
    }

    /**
     * Verifies that result of subtraction is rounded to the scale of {@code 2} using
     * {@link java.math.RoundingMode#HALF_UP} rounding mode.
     */
    @Test
    public void testSubtractionRounding() {
        assertEquals(BigDecimal.valueOf(11.00d).setScale(2), subtract(Double.valueOf(5.002d), Double.valueOf(15.997d)));
    }

    /**
     * Verifies that arguments are rounded before compared {@link PriceCalculator#equals(Double, Double)}.
     */
    @Test
    public void testEqualsOperation() {
        assertTrue(PriceCalculator.equals(Double.valueOf(3.223d), Double.valueOf(3.22d)));
    }

}
