/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtpayment.constants;

/**
 * Global class for all Tgtpayment constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtpaymentConstants extends GeneratedTgtpaymentConstants {
    public static final String EXTENSIONNAME = "tgtpayment";
    public static final String MOCKID = "MockID";
    public static final String MOCKTOKEN = "MockToken";
    public static final String PAY_PAL_PAYMENT_TYPE = "paypal";
    public static final String PAY_NOW_PAYMENT_TYPE = "paynow";
    public static final String IPG_PAYMENT_TYPE = "ipg";
    public static final String AFTERPAY_PAYMENT_TYPE = "afterpay";
    public static final String ZIPPAY_PAYMENT_TYPE = "zippay";
    public static final String ZIP_CAPTURE_CHECKOUT_ID = "checkout_id";
    public static final String NOT_AVAILABLE = "N/A";
    public static final String CREDIT_GIFT_CARD = "CreditCard / GiftCard";
    public static final String CAPTURE_PAYMENT_ACCEPTED = "Payment captured successfully for the order= ";
    public static final String CAPTURE_PAYMENT_REJECTED = "Capture rejected for the order= ";
    public static final String CAPTURE_PAYMENT_REVIEW = "Capture payment is under review for the order= ";
    public static final String CAPTURE_PAYMENT_ERROR = "Error in capturing payment information for the order= ";
    public static final String BASE_SECURE_URL_KEY = "tgtstorefront.host.fqdn";
    public static final String ZIP_CAPTURED_SUCCESS = "captured";

    private TgtpaymentConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}
