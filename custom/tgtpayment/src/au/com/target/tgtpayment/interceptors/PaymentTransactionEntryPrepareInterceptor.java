/**
 * 
 */
package au.com.target.tgtpayment.interceptors;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.math.BigDecimal;


public class PaymentTransactionEntryPrepareInterceptor implements PrepareInterceptor {

    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof PaymentTransactionEntryModel) {
            final PaymentTransactionEntryModel pte = (PaymentTransactionEntryModel)model;
            if (pte.getAmount() != null) {
                final CurrencyModel currency = pte.getCurrency();
                if (currency != null) {
                    pte.setAmount(pte.getAmount().setScale(currency.getDigits().intValue(),
                            BigDecimal.ROUND_HALF_UP));
                }
            }
        }
    }
}
