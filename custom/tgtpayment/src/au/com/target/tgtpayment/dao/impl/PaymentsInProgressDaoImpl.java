/**
 * 
 */
package au.com.target.tgtpayment.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtpayment.dao.PaymentsInProgressDao;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;


public class PaymentsInProgressDaoImpl extends AbstractItemDao implements PaymentsInProgressDao {

    // get orders before a specified time
    private static final String QUERY_GET_CRASHED_ORDERS = "SELECT  {"
            + PaymentsInProgressModel.PK
            + "} FROM {"
            + PaymentsInProgressModel._TYPECODE
            + " AS P }"
            + " WHERE {P.paymentInitiatedTime} < ?paymentInitiatedTime";


    @Override
    public List<PaymentsInProgressModel> fetchPaymentsInProgressAfterServerCrash(final Date date) {

        final Map<String, Object> queryParamMap = new HashMap<String, Object>();
        queryParamMap.put("paymentInitiatedTime", date);

        final SearchResult<PaymentsInProgressModel> searchResult = search(QUERY_GET_CRASHED_ORDERS, queryParamMap);

        final List<PaymentsInProgressModel> result = searchResult.getResult();

        return result == null ? Collections.EMPTY_LIST : result;
    }


}
