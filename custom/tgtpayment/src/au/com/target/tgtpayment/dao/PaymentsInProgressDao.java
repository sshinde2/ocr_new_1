/**
 * 
 */
package au.com.target.tgtpayment.dao;

import java.util.Date;
import java.util.List;

import au.com.target.tgtpayment.model.PaymentsInProgressModel;


public interface PaymentsInProgressDao {

    /**
     * Find all the PaymentsInProgress in the Temporary Table
     * 
     * @return PaymentsInProgressModel
     */
    List<PaymentsInProgressModel> fetchPaymentsInProgressAfterServerCrash(final Date date);

}
