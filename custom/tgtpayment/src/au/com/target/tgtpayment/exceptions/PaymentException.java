/**
 * 
 */
package au.com.target.tgtpayment.exceptions;

/**
 * Exception for if something goes wrong in the payment service
 * 
 */
public class PaymentException extends RuntimeException {

    public PaymentException(final String message) {
        super(message);
    }

    public PaymentException(final String message, final Throwable t) {
        super(message, t);
    }

}
