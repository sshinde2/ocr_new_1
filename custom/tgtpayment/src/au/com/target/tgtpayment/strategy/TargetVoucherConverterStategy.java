/**
 * 
 */
package au.com.target.tgtpayment.strategy;

import de.hybris.platform.voucher.model.VoucherModel;


/**
 * @author dcwillia
 * 
 */
public interface TargetVoucherConverterStategy {
    /**
     * Return description for the voucher
     * 
     * @param voucher
     * @param voucherCode
     * @return item description
     */
    String getVoucherDescription(final VoucherModel voucher, final String voucherCode);

    /**
     * Return name for the voucher
     * 
     * @param voucher
     * @param voucherCode
     * @return line name
     */
    String getVoucherName(final VoucherModel voucher, final String voucherCode);
}
