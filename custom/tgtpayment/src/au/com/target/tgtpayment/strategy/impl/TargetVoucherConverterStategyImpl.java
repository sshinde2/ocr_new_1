/**
 * 
 */
package au.com.target.tgtpayment.strategy.impl;

import de.hybris.platform.voucher.model.VoucherModel;

import au.com.target.tgtpayment.strategy.TargetVoucherConverterStategy;


/**
 * @author dcwillia
 * 
 */
public class TargetVoucherConverterStategyImpl implements TargetVoucherConverterStategy {
    static final String VOUCHER_LINE_NAME = "Voucher";

    @Override
    public String getVoucherDescription(final VoucherModel voucher, final String voucherCode) {
        return voucherCode;
    }

    @Override
    public String getVoucherName(final VoucherModel voucher, final String voucherCode) {
        return VOUCHER_LINE_NAME;
    }

}
