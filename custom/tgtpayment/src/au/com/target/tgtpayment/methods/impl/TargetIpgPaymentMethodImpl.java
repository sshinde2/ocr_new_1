package au.com.target.tgtpayment.methods.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import org.apache.log4j.Logger;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetPaymentVoidCommand;
import au.com.target.tgtpayment.commands.TargetQueryTransactionDetailsCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;


/**
 * Payment method implementation for IPG
 *
 */
public class TargetIpgPaymentMethodImpl implements TargetIpgPaymentMethod {
    private static final Logger LOG = Logger.getLogger(TargetIpgPaymentMethodImpl.class);

    private CommandFactory commandFactory;

    @Override
    public TargetCaptureResult capture(final TargetCaptureRequest captureRequest) {
        try {
            final TargetCaptureCommand command = commandFactory.createCommand(TargetCaptureCommand.class);

            return command.perform(captureRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetTokenizeResult tokenize(final TargetTokenizeRequest tokenizeRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetCreateSubscriptionResult createSubscription(
            final TargetCreateSubscriptionRequest createSubscriptionRequest) {
        try {
            final TargetCreateSubscriptionCommand command = commandFactory
                    .createCommand(TargetCreateSubscriptionCommand.class);

            return command.perform(createSubscriptionRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetStandaloneRefundResult standaloneRefund(
            final AbstractTargetStandaloneRefundRequest standaloneRefundRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetFollowOnRefundResult followOnRefund(final TargetFollowOnRefundRequest followOnRefundRequest) {
        try {
            final TargetFollowOnRefundCommand command = commandFactory
                    .createCommand(TargetFollowOnRefundCommand.class);

            return command.perform(followOnRefundRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetRetrieveTransactionResult retrieveTransaction(
            final TargetRetrieveTransactionRequest retrieveTransactionRequest) {
        try {
            final TargetRetrieveTransactionCommand command = commandFactory
                    .createCommand(TargetRetrieveTransactionCommand.class);

            return command.perform(retrieveTransactionRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetExcessiveRefundResult excessiveRefund(
            final TargetExcessiveRefundRequest targetExcessiveRefundRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(
            final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest) {

        try {
            final TargetQueryTransactionDetailsCommand command = commandFactory
                    .createCommand(TargetQueryTransactionDetailsCommand.class);

            return command.perform(targetQueryTransactionDetailsRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public void setCommandFactory(final CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public String getPaymentProvider() {
        if (commandFactory == null) {
            return null;
        }
        return commandFactory.getPaymentProvider();
    }

    @Override
    public TargetPaymentVoidResult voidPayment(final TargetPaymentVoidRequest voidRequest) {
        try {
            final TargetPaymentVoidCommand command = commandFactory.createCommand(TargetPaymentVoidCommand.class);

            return command.perform(voidRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetGetPaymentConfigurationResult getConfiguration(final TargetGetPaymentConfigurationRequest request) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetPaymentPingResult ping(final TargetPaymentPingRequest request) {
        throw new UnsupportedOperationException("We don't support ping feature for Ipg payment.");
    }

    @Override
    public TargetGetSubscriptionResult getSubscription(final SubscriptionDataRequest request) {
        throw new UnsupportedOperationException("We don't support get subscription for Ipg payment.");
    }
}