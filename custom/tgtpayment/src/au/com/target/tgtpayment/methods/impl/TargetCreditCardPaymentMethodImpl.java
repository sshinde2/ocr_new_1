package au.com.target.tgtpayment.methods.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import org.apache.log4j.Logger;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetExcessiveRefundCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.TargetTokenizeCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.methods.CreditCardTargetPaymentMethod;


public class TargetCreditCardPaymentMethodImpl implements CreditCardTargetPaymentMethod {
    private static final Logger LOG = Logger.getLogger(TargetCreditCardPaymentMethodImpl.class);

    private CommandFactory commandFactory;

    @Override
    public TargetCaptureResult capture(final TargetCaptureRequest captureRequest) {
        try {
            final TargetCaptureCommand command = commandFactory
                    .createCommand(
                            TargetCaptureCommand.class);

            return command.perform(captureRequest);
        }
        catch (final CommandNotSupportedException e) {
            LOG.error(e);
            throw new AdapterException(e.getMessage(), e);
        }
    }

    @Override
    public TargetCreateSubscriptionResult createSubscription(
            final TargetCreateSubscriptionRequest createSubscriptionRequest) {
        try {
            final TargetCreateSubscriptionCommand command = commandFactory
                    .createCommand(
                            TargetCreateSubscriptionCommand.class);

            return command.perform(createSubscriptionRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetTokenizeResult tokenize(final TargetTokenizeRequest tokenizeRequest) {
        try {
            final TargetTokenizeCommand command = commandFactory
                    .createCommand(TargetTokenizeCommand.class);

            return command.perform(tokenizeRequest);
        }

        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetFollowOnRefundResult followOnRefund(final TargetFollowOnRefundRequest followOnRefundRequest) {
        try {
            final TargetFollowOnRefundCommand command = commandFactory
                    .createCommand(
                            TargetFollowOnRefundCommand.class);

            return command.perform(followOnRefundRequest);
        }
        catch (final CommandNotSupportedException e) {
            LOG.error(e);
            throw new AdapterException(e.getMessage(), e);
        }
    }

    @Override
    public TargetRetrieveTransactionResult retrieveTransaction(
            final TargetRetrieveTransactionRequest retrieveTransactionRequest) {
        try {
            final TargetRetrieveTransactionCommand command = commandFactory
                    .createCommand(
                            TargetRetrieveTransactionCommand.class);

            return command.perform(retrieveTransactionRequest);
        }
        catch (final CommandNotSupportedException e) {
            LOG.error(e);
            throw new AdapterException(e.getMessage(), e);
        }
    }

    @Override
    public TargetStandaloneRefundResult standaloneRefund(
            final AbstractTargetStandaloneRefundRequest standaloneRefundRequest) {
        try {
            final TargetStandaloneRefundCommand command = commandFactory
                    .createCommand(
                            TargetStandaloneRefundCommand.class);

            return command.perform(standaloneRefundRequest);
        }
        catch (final CommandNotSupportedException e) {
            LOG.error(e);
            throw new AdapterException(e.getMessage(), e);
        }
    }

    @Override
    public TargetExcessiveRefundResult excessiveRefund(
            final TargetExcessiveRefundRequest targetExcessiveRefundRequest) {
        try {
            final TargetExcessiveRefundCommand command = commandFactory
                    .createCommand(TargetExcessiveRefundCommand.class);

            return command.perform(targetExcessiveRefundRequest);
        }

        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(
            final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest) {
        throw new UnsupportedOperationException("We don't support querying transaction details for credit card (tns).");
    }

    @Override
    public String getPaymentProvider() {
        if (commandFactory == null) {
            return null;
        }
        return commandFactory.getPaymentProvider();
    }

    @Override
    public void setCommandFactory(final CommandFactory cmdFactory) {
        this.commandFactory = cmdFactory;
    }

    @Override
    public TargetPaymentVoidResult voidPayment(final TargetPaymentVoidRequest voidRequest) {
        throw new UnsupportedOperationException("TNS does not support payment void");
    }

    @Override
    public TargetGetPaymentConfigurationResult getConfiguration(final TargetGetPaymentConfigurationRequest request) {
        throw new UnsupportedOperationException("We don't support get configuration for credit card (tns).");
    }

    @Override
    public TargetPaymentPingResult ping(final TargetPaymentPingRequest request) {
        throw new UnsupportedOperationException("We don't support ping feature for credit card (tns).");
    }

    @Override
    public TargetGetSubscriptionResult getSubscription(final SubscriptionDataRequest request) {
        throw new UnsupportedOperationException("We don't support get subscription for credit card (tns).");
    }

}