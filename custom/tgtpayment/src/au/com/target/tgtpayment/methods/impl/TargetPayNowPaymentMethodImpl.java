package au.com.target.tgtpayment.methods.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.commands.result.SubscriptionDataResult;

import org.apache.log4j.Logger;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.methods.TargetPayNowPaymentMethod;


/**
 * @author ragarwa3
 */
public class TargetPayNowPaymentMethodImpl implements TargetPayNowPaymentMethod {

    private static final Logger LOG = Logger.getLogger(TargetPayNowPaymentMethodImpl.class);
    private CommandFactory commandFactory;

    @Override
    public TargetCaptureResult capture(final TargetCaptureRequest captureRequest) {

        try {
            final GetSubscriptionDataCommand subCommand = commandFactory
                    .createCommand(GetSubscriptionDataCommand.class);

            final SubscriptionDataRequest request = new SubscriptionDataRequest(captureRequest.getTransactionId(),
                    captureRequest.getToken(), getPaymentProvider());
            final SubscriptionDataResult subResult = subCommand.perform(request);
            final TargetCaptureCommand command = commandFactory
                    .createCommand(TargetCaptureCommand.class);

            final TargetCaptureResult result = command.perform(captureRequest);

            result.setBillingInfo(subResult.getBillingInfo());

            return result;
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetCreateSubscriptionResult createSubscription(
            final TargetCreateSubscriptionRequest createSubscriptionRequest) {

        try {
            final TargetCreateSubscriptionCommand command = commandFactory
                    .createCommand(TargetCreateSubscriptionCommand.class);

            return command.perform(createSubscriptionRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetFollowOnRefundResult followOnRefund(final TargetFollowOnRefundRequest followOnRefundRequest) {

        try {
            final TargetFollowOnRefundCommand command = commandFactory
                    .createCommand(TargetFollowOnRefundCommand.class);

            return command.perform(followOnRefundRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetRetrieveTransactionResult retrieveTransaction(
            final TargetRetrieveTransactionRequest retrieveTransactionRequest) {

        try {
            final TargetRetrieveTransactionCommand command = commandFactory
                    .createCommand(TargetRetrieveTransactionCommand.class);

            return command.perform(retrieveTransactionRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetStandaloneRefundResult standaloneRefund(
            final AbstractTargetStandaloneRefundRequest standaloneRefundRequest) {

        try {
            final TargetStandaloneRefundCommand command = commandFactory
                    .createCommand(TargetStandaloneRefundCommand.class);

            return command.perform(standaloneRefundRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }

    @Override
    public TargetTokenizeResult tokenize(final TargetTokenizeRequest tokenizeRequest) {
        throw new UnsupportedOperationException("We don't support tokenize for Paynow.");
    }

    @Override
    public TargetPaymentVoidResult voidPayment(final TargetPaymentVoidRequest voidRequest) {
        throw new UnsupportedOperationException("We don't support payment void for Paynow.");
    }

    @Override
    public TargetExcessiveRefundResult excessiveRefund(
            final TargetExcessiveRefundRequest targetExcessiveRefundRequest) {
        throw new UnsupportedOperationException("We don't support excessive refund for Paynow.");
    }

    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(
            final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest) {
        throw new UnsupportedOperationException("We don't support querying transaction details for Paynow.");
    }

    @Override
    public String getPaymentProvider() {
        if (commandFactory == null) {
            return null;
        }
        return commandFactory.getPaymentProvider();
    }

    @Override
    public void setCommandFactory(final CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public TargetGetPaymentConfigurationResult getConfiguration(final TargetGetPaymentConfigurationRequest request) {
        throw new UnsupportedOperationException("We don't support get configuration for Paynow.");
    }

    @Override
    public TargetPaymentPingResult ping(final TargetPaymentPingRequest request) {
        throw new UnsupportedOperationException("We don't support ping feature for Paynow.");
    }

    @Override
    public TargetGetSubscriptionResult getSubscription(final SubscriptionDataRequest request) {
        throw new UnsupportedOperationException("We don't support get subscription for Paynow.");
    }

}