/**
 * 
 */
package au.com.target.tgtpayment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpayment.service.AfterpayConfigService;


/**
 * @author bhuang3
 *
 */
public class TargetAfterpayConfigurationJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetAfterpayConfigurationJob.class);

    private AfterpayConfigService afterpayConfigService;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("Afterpay configuration cronjob: Starting cronjob to get the afterpay configurtion");
        try {
            afterpayConfigService.createOrUpdateAfterpayConfig();
        }
        catch (final Exception e) {
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }
        LOG.info("Afterpay configuration cronjob: Finished successfully");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param afterpayConfigService
     *            the afterpayConfigService to set
     */
    @Required
    public void setAfterpayConfigService(final AfterpayConfigService afterpayConfigService) {
        this.afterpayConfigService = afterpayConfigService;
    }

}
