/**
 * 
 */
package au.com.target.tgtpayment.resumer.impl;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.resumer.PaymentProcessResumerFactory;


/**
 * @author Vivek
 * 
 */
public class TargetPaymentProcessResumerFactory implements PaymentProcessResumerFactory {

    /* (non-Javadoc)
     * @see au.com.target.tgtpayment.resumer.PaymentProcessResumerFactory#getPaymentProcessResumer(au.com.target.tgtpayment.enums.PaymentCaptureType)
     */
    @Override
    public PaymentProcessResumer getPaymentProcessResumer(final PaymentCaptureType paymentCaptureType) {
        return null;
    }

}
