package au.com.target.tgtpayment.service.impl;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.payment.strategy.TransactionCodeGenerator;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest.TrnType;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetAbstractPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.dto.CreditCard;
import au.com.target.tgtpayment.dto.GiftCardReversalData;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.exceptions.InvalidTransactionRequestException;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalHerePaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentAbstractOrderConverter;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtpayment.util.PriceCalculator;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.constants.TgtutilityConstants.InfoCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * 
 * Implementation of {@link TargetPaymentService}
 * 
 */
public class TargetPaymentServiceImpl extends AbstractBusinessService implements TargetPaymentService {

    protected static final BigDecimal REFUND_STANDALONE_CAPTURE_AMOUNT = BigDecimal.ONE;
    private static final String GIFTCARD_PAYMENT_RESPONSE_CODE = "0";
    private static final String GIFTCARD_INFO_MESSAGE = "Gift card payment taken, customerName=[%s], status=[%s], receiptNumber=[%s], cartId=[%s]";

    private static final Logger LOG = Logger.getLogger(TargetPaymentServiceImpl.class);

    private PaymentMethodStrategy paymentMethodStrategy;

    private CommonI18NService commonI18NService;

    private FlexibleSearchService flexibleSearchService;

    private TransactionCodeGenerator transactionCodeGenerator;

    private TargetPaymentAbstractOrderConverter hostedSessionOrderConverter;

    private PaymentsInProgressService paymentsInProgressService;

    private TargetPaymentAbstractOrderConverter captureOrderConverter;

    private EnumerationService enumerationService;

    @Override
    public TargetCreateSubscriptionResult getHostedSessionToken(
            final HostedSessionTokenRequest hostedSessionTokenRequest) {
        Assert.notNull(hostedSessionTokenRequest.getOrderModel());

        final Order order = hostedSessionOrderConverter.convertAbstractOrderModelToOrder(
                hostedSessionTokenRequest.getOrderModel(), hostedSessionTokenRequest.getAmount(),
                hostedSessionTokenRequest.getPaymentCaptureType());
        if (null != hostedSessionTokenRequest.getAmount() && null != order) {
            final BigDecimal orderTotalCents = hostedSessionTokenRequest.getAmount()
                    .multiply(new BigDecimal("100"));
            order.setOrderTotalCents(Integer.valueOf(orderTotalCents.intValue()));
        }
        return getSessionToken(hostedSessionTokenRequest, order);
    }

    private TargetCreateSubscriptionResult getSessionToken(final HostedSessionTokenRequest hostedSessionTokenRequest,
            final Order order) {
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(hostedSessionTokenRequest
                .getPaymentMode());

        final TargetCreateSubscriptionRequest subscriptionRequest = new TargetCreateSubscriptionRequest();

        orderDeliveryAddressLog(hostedSessionTokenRequest.getOrderModel());
        subscriptionRequest.setOrder(order);
        subscriptionRequest.setCancelUrl(hostedSessionTokenRequest.getCancelUrl());
        subscriptionRequest.setReturnUrl(hostedSessionTokenRequest.getReturnUrl());
        subscriptionRequest.setSessionId(hostedSessionTokenRequest.getSessionId());
        subscriptionRequest.setSavedCreditCards(hostedSessionTokenRequest.getSavedCreditCards());
        subscriptionRequest.setIpgPaymentTemplateType(hostedSessionTokenRequest.getIpgPaymentTemplateType());

        return paymentMethod.createSubscription(subscriptionRequest);
    }

    private void orderDeliveryAddressLog(final AbstractOrderModel abstractOrderModel) {
        if (abstractOrderModel != null) {
            final AddressModel address = abstractOrderModel.getDeliveryAddress();
            if (address != null) {
                LOG.info(MessageFormat.format("=====Delivery Address for Order {0}:{1} {2}, {3} {4},{5}=====",
                        abstractOrderModel.getCode(), address.getStreetname(), address.getTown(),
                        address.getDistrict(),
                        address.getPostalcode(),
                        address.getCountry().getName()));
            }
        }
    }

    @Override
    public String tokenize(final String sessionToken, final PaymentModeModel paymentMode) {
        Assert.notNull(sessionToken, "Session token cannot be null");
        Assert.notNull(paymentMode, "Payment mode cannot be null");

        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(paymentMode);

        final TargetTokenizeRequest tokenizeRequest = new TargetTokenizeRequest();
        tokenizeRequest.setSessionToken(sessionToken);

        try {
            final TargetTokenizeResult tokenizeResult = paymentMethod.tokenize(tokenizeRequest);
            return tokenizeResult.getSavedToken();
        }
        catch (final AdapterException ae) {
            LOG.warn(SplunkLogFormatter.formatOrderMessage("AdapterException during tokenize",
                    TgtutilityConstants.ErrorCode.WARN_PAYMENT, null), ae);
            return null;
        }
    }

    @Override
    public PaymentTransactionEntryModel capture(final AbstractOrderModel orderModel,
            final PaymentInfoModel paymentInfo,
            final BigDecimal amount,
            final CurrencyModel currency,
            final PaymentCaptureType paymentCaptureType) {

        return capture(orderModel, paymentInfo, amount, currency, paymentCaptureType, null, null);
    }

    @Override
    public void capture(final AbstractOrderModel orderModel,
            final PaymentInfoModel paymentInfo,
            final BigDecimal amount,
            final CurrencyModel currency, final PaymentCaptureType paymentCaptureType,
            final PaymentTransactionModel transaction) {
        Assert.notNull(orderModel, "Order/Cart cannot be null!");
        Assert.notNull(paymentInfo, "PaymentInfo cannot be null!");
        Assert.notNull(amount, "Amount cannot be null!");
        Assert.notNull(currency, "Currency cannot be null!");
        Assert.notEmpty(transaction.getEntries(), "Transaction Entry cannot be empty!");
        final boolean singlePayment = transaction.getEntries().size() > 1 ? false : true;
        BigDecimal captureAmount;
        for (final PaymentTransactionEntryModel transactionEntry : transaction.getEntries()) {
            captureAmount = singlePayment ? amount : transactionEntry.getAmount();
            capture(orderModel, paymentInfo, captureAmount, currency, paymentCaptureType, null, transactionEntry);
            //terminate the capture if one transaction entry is not successful
            if (!TransactionStatus.ACCEPTED.toString().equals(transactionEntry.getTransactionStatus())) {
                break;
            }
        }
    }

    @Override
    public PaymentTransactionEntryModel createPaymentTransactionEntry(final AbstractOrderModel orderModel) {
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(orderModel.getPaymentInfo());
        final BigDecimal amount = BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue());

        final PaymentTransactionModel paymentTransactionModel = getModelService().create(PaymentTransactionModel.class);
        paymentTransactionModel.setCode(transactionCodeGenerator.generateCode(null));
        paymentTransactionModel.setInfo(orderModel.getPaymentInfo());
        paymentTransactionModel.setOrder(orderModel);
        paymentTransactionModel.setPlannedAmount(amount);
        paymentTransactionModel.setCurrency(orderModel.getCurrency());
        paymentTransactionModel.setPaymentProvider(paymentMethod.getPaymentProvider());

        final PaymentTransactionEntryModel entry = createInitialisedPaymentTransactionEntry(paymentTransactionModel,
                PaymentTransactionType.CAPTURE, getNewEntryCode(paymentTransactionModel), amount,
                paymentTransactionModel.getCurrency());

        getModelService().saveAll(paymentTransactionModel, entry);
        getModelService().refresh(orderModel);

        return entry;
    }

    @Override
    public boolean reversePayment(final CartModel cartModel, final List<TargetCardResult> cards) {
        boolean isReversalSuccessful = true;
        if (CollectionUtils.isNotEmpty(cards)) {
            final TargetPaymentMethod paymentMethod = paymentMethodStrategy
                    .getPaymentMethod(TgtpaymentConstants.IPG_PAYMENT_TYPE);
            for (final TargetCardResult card : cards) {
                if (card.getTargetCardPaymentResult() != null
                        && card.getTargetCardPaymentResult().isPaymentSuccessful()) {
                    Date modifiedtime = new Date();
                    String code = null;
                    if (cartModel != null) {
                        modifiedtime = cartModel.getModifiedtime();
                        code = cartModel.getCode();
                    }
                    final boolean reversed = reverseGiftCardPayment(paymentMethod, card.getTargetCardPaymentResult()
                            .getReceiptNumber(), modifiedtime,
                            code);
                    if (!reversed && cartModel != null) {
                        createReverseProcess(cartModel, card);
                    }
                    isReversalSuccessful &= reversed;
                }
            }
        }
        return isReversalSuccessful;
    }

    @Override
    public boolean reversePayment(final GiftCardReversalData reversalData) {
        Assert.notNull(reversalData, "reversalData cannot be null");
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy
                .getPaymentMethod(reversalData.getProvider());
        return reverseGiftCardPayment(paymentMethod, reversalData.getReceiptNumber(),
                reversalData.getPaymentDate(), reversalData.getCartId());
    }

    @Override
    public PaymentTransactionModel createTransactionWithQueryResult(final AbstractOrderModel orderModel,
            final TargetQueryTransactionDetailsResult result) {
        return createTransactionWithQueryResult(orderModel, result, PaymentTransactionType.CAPTURE);
    }

    @Override
    public PaymentTransactionModel createTransactionWithQueryResult(final AbstractOrderModel orderModel,
            final TargetQueryTransactionDetailsResult result, final PaymentTransactionType transactionType) {
        Assert.notNull(result, "ipg query result should not be null");
        Assert.isTrue(result.isSuccess(), "ipg query result should be successful");
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(orderModel.getPaymentInfo());

        BigDecimal amount;

        if (orderModel.getNormalSaleStartDateTime() != null) {
            amount = BigDecimal.valueOf(orderModel.getPreOrderDepositAmount().doubleValue());
            if (PaymentTransactionType.DEFERRED == transactionType) {
                // updating cc details for preOrder, set amount to outstanding amount
                final double outstandingAmount = orderModel.getTotalPrice().doubleValue()
                        - orderModel.getPreOrderDepositAmount().doubleValue();
                amount = BigDecimal.valueOf(outstandingAmount);
            }
        }
        else {
            amount = BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue());
        }

        final PaymentTransactionModel paymentTransactionModel = getModelService().create(PaymentTransactionModel.class);
        paymentTransactionModel.setCode(transactionCodeGenerator.generateCode(null));
        paymentTransactionModel.setInfo(orderModel.getPaymentInfo());
        paymentTransactionModel.setOrder(orderModel);
        paymentTransactionModel.setPlannedAmount(amount);
        paymentTransactionModel.setCurrency(orderModel.getCurrency());
        paymentTransactionModel.setPaymentProvider(paymentMethod.getPaymentProvider());
        PaymentTransactionEntryModel entry;
        for (final TargetCardResult cardResult : result.getCardResults()) {
            entry = createInitialisedPaymentTransactionEntry(paymentTransactionModel,
                    transactionType, getNewEntryCode(paymentTransactionModel), cardResult
                            .getTargetCardPaymentResult().getAmount(),
                    paymentTransactionModel.getCurrency());
            saveCardResult(orderModel, cardResult, entry);
        }
        getModelService().refresh(orderModel);
        getModelService().refresh(paymentTransactionModel);
        return paymentTransactionModel;
    }

    /**
     * Save card details
     * 
     * @param orderModel
     *            - order model
     * @param cardResult
     *            - card result
     * @param entry
     *            - payment transaction entry
     */
    protected void saveCardResult(final AbstractOrderModel orderModel, final TargetCardResult cardResult,
            final PaymentTransactionEntryModel entry) {
        final PaymentInfoModel ipgPaymentInfoModel = createIpgPaymentInfoModel(orderModel, cardResult);
        final TargetCardPaymentResult cardPaymentResult = cardResult.getTargetCardPaymentResult();

        if (PaymentTransactionType.DEFERRED != entry.getType()
                && orderModel.getNormalSaleStartDateTime() != null) {
            entry.setAmount(BigDecimal.valueOf(orderModel.getPreOrderDepositAmount().doubleValue()));
        }
        else {
            if (orderModel.getNormalSaleStartDateTime() != null) {
                // capture outstanding amount
                entry.setAmount(
                        PriceCalculator.subtract(orderModel.getPreOrderDepositAmount(), orderModel.getTotalPrice()));
            }
            else {
                entry.setAmount(cardResult.getTargetCardPaymentResult().getAmount());
            }
        }

        entry.setIpgPaymentInfo(ipgPaymentInfoModel);
        ipgPaymentInfoModel.setOwner(entry);
        ipgPaymentInfoModel.setDuplicate(Boolean.TRUE);

        // If immediate payment is successful, update transaction entry accordingly
        if (cardPaymentResult.isImmediatePayment()) {
            updateTransactionEntryWithImmediatePaymentResult(entry, cardPaymentResult);
        }
        getModelService().saveAll(entry, ipgPaymentInfoModel);
    }

    /**
     * Update payment transaction entry model with immediate payment information
     * 
     * @param entry
     *            - payment transaction entry
     * @param ipgTransactionResult
     *            - transaction result
     */
    protected void updateTransactionEntryWithImmediatePaymentResult(final PaymentTransactionEntryModel entry,
            final TargetCardPaymentResult ipgTransactionResult) {

        if (ipgTransactionResult.getAmount() != null) {
            entry.setAmount(ipgTransactionResult.getAmount());
        }

        entry.setTime(ipgTransactionResult.getTransactionDate());

        if (StringUtils.isNotBlank(ipgTransactionResult.getReceiptNumber())) {
            entry.setReceiptNo(ipgTransactionResult.getReceiptNumber());
        }
        else {
            entry.setReceiptNo(TgtpaymentConstants.NOT_AVAILABLE);
        }

        if (ipgTransactionResult.isPaymentSuccessful()) {
            entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
            entry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());
            entry.getIpgPaymentInfo().setIsPaymentSucceeded(Boolean.TRUE);
        }
        else {
            entry.setTransactionStatus(TransactionStatus.REJECTED.toString());
            entry.setTransactionStatusDetails(ipgTransactionResult.getResponseText());
            entry.setDeclinedTransactionStatusInfo(ipgTransactionResult.getResponseCode() + " - "
                    + ipgTransactionResult.getResponseText());
            entry.getIpgPaymentInfo().setIsPaymentSucceeded(Boolean.FALSE);
        }
    }

    /**
     * Creates a payment info model to save IPG card information (credit card/ gift card)
     * 
     * @param orderModel
     *            - order model
     * @param ipgCardResult
     *            - IPG card result
     * 
     * @return payment info model
     */
    protected PaymentInfoModel createIpgPaymentInfoModel(final AbstractOrderModel orderModel,
            final TargetCardResult ipgCardResult) {
        Assert.notNull(ipgCardResult, "Ipg card results cannot be null");
        Assert.notNull(ipgCardResult.getCardType(), "Ipg card type cannot be null");

        final PaymentInfoModel ipgPaymentInfoModel;

        if (ipgCardResult.getCardType().equalsIgnoreCase(CreditCardType.GIFTCARD.getCode())) {
            ipgPaymentInfoModel = createIpgGiftCardPaymentInfoModel(orderModel, ipgCardResult);
        }
        else {
            ipgPaymentInfoModel = createIpgCreditCardPaymentInfoModel(orderModel, ipgCardResult);
        }
        return ipgPaymentInfoModel;
    }

    /**
     * Save customer saved cards
     * 
     * @param orderModel
     *            - order model
     * @param savedCards
     *            - saved cards
     */
    @Override
    public void updateSavedCards(final AbstractOrderModel orderModel, final List<TargetCardResult> savedCards) {
        if (CollectionUtils.isNotEmpty(savedCards)) {
            CustomerModel customerModel = null;
            final List<IpgCreditCardPaymentInfoModel> cloneCardList = new ArrayList<>();

            for (final TargetCardResult savedCard : savedCards) {
                final IpgCreditCardPaymentInfoModel savedCardModel = createIpgCreditCardPaymentInfoModel(
                        orderModel,
                        savedCard);
                savedCardModel.setDuplicate(Boolean.FALSE);
                savedCardModel.setSaved(true);
                savedCardModel.setIsPaymentSucceeded(Boolean.TRUE);
                if (savedCard.isDefaultCard()) {
                    customerModel = (CustomerModel)orderModel.getUser();
                    customerModel.setDefaultPaymentInfo(savedCardModel);
                }
                cloneCardList.add(savedCardModel);
            }
            getModelService().saveAll(cloneCardList);
            if (null != customerModel) {
                getModelService().save(customerModel);
            }
        }
    }

    /**
     * Create an IPG gift card payment info model to be saved against transaction payment entry
     * 
     * @param orderModel
     *            - order model
     * @param ipgCardResult
     *            - IPG card result
     * 
     * @return IPG gift card payment info model
     */
    protected IpgGiftCardPaymentInfoModel createIpgGiftCardPaymentInfoModel(final AbstractOrderModel orderModel,
            final TargetCardResult ipgCardResult) {

        final IpgGiftCardPaymentInfoModel ipgGiftCardPaymentInfoModel = getModelService().create(
                IpgGiftCardPaymentInfoModel.class);

        final CustomerModel customerModel = (CustomerModel)orderModel.getUser();

        ipgGiftCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
        ipgGiftCardPaymentInfoModel.setUser(orderModel.getUser());
        ipgGiftCardPaymentInfoModel.setMaskedNumber(ipgCardResult.getCardNumber());

        if (StringUtils.isNotEmpty(ipgCardResult.getCardType())) {
            final CreditCardType cardType = enumerationService.getEnumerationValue(
                    CreditCardType.class.getSimpleName(), ipgCardResult.getCardType().toLowerCase());
            ipgGiftCardPaymentInfoModel.setCardType(cardType);
        }
        ipgGiftCardPaymentInfoModel.setBin(ipgCardResult.getBin());

        return ipgGiftCardPaymentInfoModel;
    }

    /**
     * Create an IPG credit card payment info model to be saved against transaction payment entry
     * 
     * @param orderModel
     *            - order model
     * @param ipgCardResult
     *            - IPG card result
     * 
     * @return IPG credit card payment info model
     */
    protected IpgCreditCardPaymentInfoModel createIpgCreditCardPaymentInfoModel(final AbstractOrderModel orderModel,
            final TargetCardResult ipgCardResult) {

        final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = getModelService().create(
                IpgCreditCardPaymentInfoModel.class);

        final CustomerModel customerModel = (CustomerModel)orderModel.getUser();

        ipgCreditCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
        ipgCreditCardPaymentInfoModel.setUser(orderModel.getUser());
        ipgCreditCardPaymentInfoModel.setNumber(ipgCardResult.getCardNumber());

        if (StringUtils.isNotEmpty(ipgCardResult.getCardType())) {
            final CreditCardType cardType = enumerationService.getEnumerationValue(
                    CreditCardType.class.getSimpleName(), ipgCardResult.getCardType());
            ipgCreditCardPaymentInfoModel.setType(cardType);
        }

        if (StringUtils.isNotEmpty(ipgCardResult.getCardExpiry())) {
            ipgCreditCardPaymentInfoModel.setValidToMonth(PaymentTransactionHelper.extractCardExpiryMonth(ipgCardResult
                    .getCardExpiry()));
            ipgCreditCardPaymentInfoModel.setValidToYear(PaymentTransactionHelper.extractCardExpiryYear(ipgCardResult
                    .getCardExpiry()));
        }

        // IPG does not accept name on credit card.
        ipgCreditCardPaymentInfoModel.setCcOwner("");
        ipgCreditCardPaymentInfoModel.setToken(ipgCardResult.getToken());
        ipgCreditCardPaymentInfoModel.setTokenExpiry(ipgCardResult.getTokenExpiry());
        ipgCreditCardPaymentInfoModel.setBin(ipgCardResult.getBin());
        ipgCreditCardPaymentInfoModel.setDefaultCard(Boolean.valueOf(ipgCardResult.isDefaultCard()));
        ipgCreditCardPaymentInfoModel.setCardOnFile(ipgCardResult.isCardOnFile());
        ipgCreditCardPaymentInfoModel.setFirstCredentialStorage(ipgCardResult.isFirstCredentialStorage());


        return ipgCreditCardPaymentInfoModel;
    }

    /**
     * Capture payment
     * 
     * @param orderModel
     * @param paymentInfo
     * @param amount
     * @param currency
     * @param paymentCaptureType
     * @param orderId
     * @param paymentTransactionEntryModel
     * @return transaction entry
     */
    protected PaymentTransactionEntryModel capture(final AbstractOrderModel orderModel,
            final PaymentInfoModel paymentInfo,
            final BigDecimal amount,
            final CurrencyModel currency,
            final PaymentCaptureType paymentCaptureType,
            final String orderId,
            final PaymentTransactionEntryModel paymentTransactionEntryModel) {

        Assert.notNull(orderModel, "Order/Cart cannot be null!");
        Assert.notNull(paymentInfo, "PaymentInfo cannot be null!");
        Assert.notNull(amount, "Amount cannot be null!");
        Assert.notNull(currency, "Currency cannot be null!");

        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(paymentInfo);

        final PaymentTransactionEntryModel entry;
        if (paymentTransactionEntryModel == null) {
            // Create the PaymentTransaction and PaymentTransactionEntry models
            final PaymentTransactionModel paymentTransaction = getModelService().create(PaymentTransactionModel.class);
            paymentTransaction.setCode(transactionCodeGenerator.generateCode(null));
            paymentTransaction.setInfo(paymentInfo);
            paymentTransaction.setOrder(orderModel);
            paymentTransaction.setPlannedAmount(amount);
            paymentTransaction.setCurrency(currency);
            paymentTransaction.setPaymentProvider(paymentMethod.getPaymentProvider());

            // Create the result PTEM in advance so it remains even after failure
            entry = createInitialisedPaymentTransactionEntry(paymentTransaction,
                    PaymentTransactionType.CAPTURE, getNewEntryCode(paymentTransaction), amount, currency);

            getModelService().saveAll(paymentTransaction, entry);
            getModelService().refresh(orderModel);
        }
        else {
            entry = paymentTransactionEntryModel;
        }

        // Save the payment in case there is a problem in the capture we could resume later
        // Currently, refundStandalone sends paymentCaptureType=null so we don't create a PaymentsInProgress entry in that case
        if (paymentCaptureType != null) {
            paymentsInProgressService.createInProgressPayment(orderModel, entry, paymentCaptureType);
        }

        // Proceed with the capture
        LOG.info(MessageFormat.format(
                "ORDER-PAYMENTCAPTURE : Capturing the payment on order :{0} for an amount of {1}",
                orderModel.getCode(), amount.toString()));
        final TargetCaptureResult result = doCapture(orderModel, paymentInfo, entry, amount, currency,
                paymentMethod, paymentCaptureType,
                orderId == null ? entry.getPaymentTransaction().getCode() : orderId);
        if (result != null) {
            captureTransactionLog(orderModel, paymentInfo, result);

            updateTransactionEntryWithCaptureResult(entry, result);
        }

        return entry;
    }

    /**
     * Method to capture payment log based on the transaction done while placing an order.
     * 
     * @param orderModel
     * @param result
     */
    private void captureTransactionLog(final AbstractOrderModel orderModel, final PaymentInfoModel paymentInfoModel,
            final TargetCaptureResult result) {

        final String typeOfPayment = PaymentTransactionHelper.capturPaymentType(paymentInfoModel);

        final TransactionStatus transactionStatus = result.getTransactionStatus();

        if (transactionStatus != null && TransactionStatus.ACCEPTED.equals(transactionStatus)) {
            logMessage(TgtpaymentConstants.CAPTURE_PAYMENT_ACCEPTED, orderModel, typeOfPayment, transactionStatus);
        }

        else if (transactionStatus != null && TransactionStatus.REJECTED.equals(transactionStatus)) {
            logMessage(TgtpaymentConstants.CAPTURE_PAYMENT_REJECTED, orderModel, typeOfPayment, transactionStatus);

        }

        else if (transactionStatus != null && TransactionStatus.REVIEW.equals(transactionStatus)) {
            logMessage(TgtpaymentConstants.CAPTURE_PAYMENT_REVIEW, orderModel, typeOfPayment, transactionStatus);
        }
        else {
            LOG.info(SplunkLogFormatter.formatMessage(TgtpaymentConstants.CAPTURE_PAYMENT_ERROR
                    + orderModel.getCode() + ", status= " + TransactionStatus.ERROR + " and type=" + typeOfPayment,
                    ErrorCode.INFO_ORDERPAYMENTCAPTURE));
        }
    }

    /**
     * @param orderModel
     * @param typeOfPayment
     */
    private void logMessage(final String message, final AbstractOrderModel orderModel, final String typeOfPayment,
            final TransactionStatus transactionStatus) {
        LOG.info(SplunkLogFormatter.formatInfoMessage(message
                + orderModel.getCode() + ", status= " + transactionStatus + " and type="
                + typeOfPayment, String.valueOf(InfoCode.INFO_ORDERPAYMENTCAPTURE)));
    }



    @SuppressWarnings("boxing")
    private TargetCaptureResult doCapture(final AbstractOrderModel orderModel,
            final PaymentInfoModel paymentInfo,
            final PaymentTransactionEntryModel paymentTransactionEntry,
            final BigDecimal amount,
            final CurrencyModel currency,
            final TargetPaymentMethod paymentMethod,
            final PaymentCaptureType paymentCaptureType,
            final String orderId) {

        Assert.notNull(orderModel, "orderModel cannot be null!");
        Assert.notNull(paymentInfo, "paymentInfo cannot be null!");
        Assert.notNull(paymentTransactionEntry, "paymentTransactionEntry cannot be null!");
        Assert.notNull(amount, "amount cannot be null!");
        Assert.notNull(currency, "currency cannot be null!");
        Assert.notNull(paymentMethod, "paymentMethod cannot be null!");

        final Order order = captureOrderConverter.convertAbstractOrderModelToOrder(orderModel, amount,
                paymentCaptureType);

        final TargetCaptureRequest captureRequest = createCaptureRequest(orderId, amount, currency, order, paymentInfo,
                orderModel, paymentTransactionEntry);

        TargetCaptureResult result = null;

        final String typeOfPayment = PaymentTransactionHelper.capturPaymentType(paymentInfo);

        try {
            LOG.info(SplunkLogFormatter.formatInfoMessage(
                    "Trying to capture payment, order=" + orderModel.getCode() + " type=" + typeOfPayment,
                    String.valueOf(InfoCode.INFO_ORDERPAYMENTCAPTURE)));

            result = paymentMethod.capture(captureRequest);
        }
        catch (final PaymentException pe) {
            paymentTransactionEntry.setTransactionStatus(TransactionStatus.REJECTED.toString());
            setEntryErrorDetails(paymentTransactionEntry, "Capture failed for order " + orderModel.getCode()
                    + " : " + pe.getMessage());
            throw pe;
        }
        catch (final AdapterException ae) {
            setEntryErrorDetails(paymentTransactionEntry, "Capture failed for order " + orderModel.getCode()
                    + " : " + ae.getMessage());
            return null;
        }

        if (result != null) {
            // If its paypal we want to pull the billing address and set it in the order
            if (paymentInfo instanceof PaypalPaymentInfoModel && result.getBillingInfo() != null) {
                setPaypalBillingAddress(orderModel, (PaypalPaymentInfoModel)paymentInfo, result);

            }
            // For IPG we need to update IPG credit card payment info with capture status. This is required only for deferred payment model (credit card)  
            else if (paymentInfo instanceof IpgPaymentInfoModel && result.getTransactionStatus() != null) {
                paymentTransactionEntry.getIpgPaymentInfo().setIsPaymentSucceeded(
                        result.getTransactionStatus().equals(TransactionStatus.ACCEPTED));
                getModelService().save(paymentTransactionEntry.getIpgPaymentInfo());

            }
            else if (paymentInfo instanceof AfterpayPaymentInfoModel && result.getRequestId() != null) {
                setAfterPayOrderId(orderModel, (AfterpayPaymentInfoModel)paymentInfo, result);
            }
            else if (paymentInfo instanceof ZippayPaymentInfoModel && result.getReconciliationId() != null) {
                setZipPaymentDetails(orderModel, paymentInfo, paymentTransactionEntry, result);

            }
        }

        return result;
    }

    private void setZipPaymentDetails(final AbstractOrderModel orderModel, final PaymentInfoModel paymentInfo,
            final PaymentTransactionEntryModel paymentTransactionEntry, final TargetCaptureResult result) {

        final ZippayPaymentInfoModel zipPaymentInfo = (ZippayPaymentInfoModel)paymentInfo;
        paymentTransactionEntry.setReceiptNo(result.getReconciliationId());
        zipPaymentInfo.setChargeId(result.getRequestId());
        zipPaymentInfo.setReceiptNo(result.getReconciliationId());

        if (orderModel.getPaymentAddress() != null) {
            final AddressModel billingAddress = orderModel.getPaymentAddress();
            billingAddress.setOwner(zipPaymentInfo);
            zipPaymentInfo.setBillingAddress(billingAddress);
        }
        try {
            getModelService().save(zipPaymentInfo);
            getModelService().save(paymentTransactionEntry);
        }
        catch (final ModelSavingException e) {
            LOG.warn(SplunkLogFormatter.formatOrderMessage("Saving of zip PaymentInfo failed",
                    TgtutilityConstants.ErrorCode.WARN_PAYMENT, orderModel.getCode()), e);
        }
    }

    /**
     * @param orderModel
     * @param paymentInfo
     * @param result
     */
    private void setPaypalBillingAddress(final AbstractOrderModel orderModel, final PaypalPaymentInfoModel paymentInfo,
            final TargetCaptureResult result) {
        final BillingInfo billingInfo = result.getBillingInfo();
        final AddressModel billingAddress = buildBillingAddress(billingInfo);
        billingAddress.setOwner(paymentInfo);
        paymentInfo.setBillingAddress(billingAddress);
        paymentInfo.setEmailId(result.getBillingInfo().getEmail());
        try {
            getModelService().save(paymentInfo);
        }
        catch (final ModelSavingException e) {
            LOG.warn(SplunkLogFormatter.formatOrderMessage("Saving of paypal PaymentInfo failed",
                    TgtutilityConstants.ErrorCode.WARN_PAYMENT, orderModel.getCode()), e);
        }
    }

    /**
     * @param orderModel
     * @param paymentInfo
     *            - coming from PaymentTransactionEntryModel.getPaymentTransaction.getInfo
     * @param result
     */
    private void setAfterPayOrderId(final AbstractOrderModel orderModel, final AfterpayPaymentInfoModel paymentInfo,
            final TargetCaptureResult result) {

        paymentInfo.setAfterpayOrderId(result.getRequestId());
        // populate in order model as well, because in AccertifyUtil, payment info object used is from orderModel.getpaymentInfo() and need this info there.
        final AfterpayPaymentInfoModel afterpayPaymentInfoWithOrderModel = (AfterpayPaymentInfoModel)orderModel
                .getPaymentInfo();
        afterpayPaymentInfoWithOrderModel.setAfterpayOrderId(result.getRequestId());
        try {
            getModelService().save(paymentInfo);
            getModelService().save(afterpayPaymentInfoWithOrderModel);
        }
        catch (final ModelSavingException e) {
            LOG.warn(SplunkLogFormatter.formatOrderMessage("Saving of afterPay PaymentInfo failed",
                    TgtutilityConstants.ErrorCode.WARN_PAYMENT, orderModel.getCode()), e);
        }
    }

    /**
     * Creates a payment capture request
     * 
     * @param orderId
     * @param amount
     * @param currency
     * @param order
     * @param paymentInfo
     * @param orderModel
     * @param paymentTransactionEntry
     * @return Capture request
     */
    protected TargetCaptureRequest createCaptureRequest(final String orderId, final BigDecimal amount,
            final CurrencyModel currency, final Order order, final PaymentInfoModel paymentInfo,
            final AbstractOrderModel orderModel, final PaymentTransactionEntryModel paymentTransactionEntry) {

        final TargetCaptureRequest captureRequest = new TargetCaptureRequest();

        captureRequest.setTransactionId(paymentTransactionEntry.getCode());
        captureRequest.setOrderId(orderId);
        captureRequest.setTotalAmount(amount);
        captureRequest.setCurrency(Currency.getInstance(currency.getIsocode()));
        captureRequest.setOrder(order);

        if (paymentInfo instanceof PaypalPaymentInfoModel) {
            captureRequest.setToken(((PaypalPaymentInfoModel)paymentInfo).getToken());
            captureRequest.setPayerId(((PaypalPaymentInfoModel)paymentInfo).getPayerId());
        }
        else if (paymentInfo instanceof CreditCardPaymentInfoModel) {
            captureRequest.setToken(((CreditCardPaymentInfoModel)paymentInfo).getSubscriptionId());
        }
        else if (paymentInfo instanceof IpgPaymentInfoModel) {
            final IpgPaymentInfoModel ipgPaymentInfoModel = (IpgPaymentInfoModel)orderModel.getPaymentInfo();
            captureRequest.setSessionId(ipgPaymentInfoModel.getSessionId());
            captureRequest.setToken(ipgPaymentInfoModel.getToken());

            final TargetCardResult ipgCardResult = getIpgCardResult(paymentTransactionEntry);

            final TargetCardPaymentResult ipgTransactionResult = new TargetCardPaymentResult();
            ipgTransactionResult.setAmount(paymentTransactionEntry.getAmount());
            ipgCardResult.setTargetCardPaymentResult(ipgTransactionResult);

            captureRequest.setCardResult(ipgCardResult);
        }
        else if (paymentInfo instanceof AfterpayPaymentInfoModel) {
            captureRequest.setToken(((AfterpayPaymentInfoModel)paymentInfo).getAfterpayToken());
        }
        else if (paymentInfo instanceof ZippayPaymentInfoModel) {
            captureRequest.setAuthorityValue(((ZippayPaymentInfoModel)paymentInfo).getCheckoutId());
            captureRequest.setAuthorityType(TgtpaymentConstants.ZIP_CAPTURE_CHECKOUT_ID);
            captureRequest.setCapture(true);
        }
        return captureRequest;
    }

    /**
     * Creates a {@link TargetCardResult} from {@link PaymentTransactionEntryModel}
     * 
     * @param paymentTransactionEntry
     * @return IPG card results
     */
    protected TargetCardResult getIpgCardResult(final PaymentTransactionEntryModel paymentTransactionEntry) {
        TargetCardResult ipgCardResult = null;

        // IPG payment card information is saved as PaymentInfoModel associated with PaymentTransactionEntryModel
        final PaymentInfoModel cardInfo = paymentTransactionEntry.getIpgPaymentInfo();

        if (cardInfo instanceof IpgCreditCardPaymentInfoModel) {
            final IpgCreditCardPaymentInfoModel creditCard = (IpgCreditCardPaymentInfoModel)cardInfo;

            ipgCardResult = new TargetCardResult();
            ipgCardResult.setCardNumber(creditCard.getNumber());
            ipgCardResult.setCardType(creditCard.getType().getCode());

            final String cardExpiry = new StringBuilder("")
                    .append(creditCard.getValidToMonth())
                    .append("/")
                    .append(creditCard.getValidToYear()).toString();

            ipgCardResult.setCardExpiry(cardExpiry);
            ipgCardResult.setBin(creditCard.getBin());
            ipgCardResult.setPromoId(creditCard.getPromoId());
            ipgCardResult.setToken(creditCard.getToken());
            ipgCardResult.setTokenExpiry(creditCard.getTokenExpiry());
            ipgCardResult.setCardOnFile(creditCard.isCardOnFile());
            ipgCardResult.setFirstCredentialStorage(creditCard.isFirstCredentialStorage());

        }
        else if (cardInfo instanceof IpgGiftCardPaymentInfoModel) {
            final IpgGiftCardPaymentInfoModel giftCard = (IpgGiftCardPaymentInfoModel)cardInfo;

            ipgCardResult = new TargetCardResult();
            ipgCardResult.setCardNumber(giftCard.getMaskedNumber());
            ipgCardResult.setCardType(giftCard.getCardType().getCode());
            ipgCardResult.setBin(giftCard.getBin());
        }

        return ipgCardResult;
    }

    private void setEntryErrorDetails(final PaymentTransactionEntryModel entry, final String message) {

        LOG.warn(message);

        // We have to truncate the message otherwise SQLServer throws a truncate exception
        String newMessage = message;
        if (message.length() > 200) {
            newMessage = message.substring(0, 200);
        }

        entry.setTransactionStatusDetails(newMessage);
        try {
            getModelService().save(entry);
        }
        catch (final Exception e) {
            LOG.warn("Exception saving error message to payment transaction entry ", e);
        }
    }

    private AddressModel buildBillingAddress(final BillingInfo billingInfo) {
        final AddressModel billingAddress = createBillingAddress();
        populateBillingAddress(billingAddress, billingInfo);
        return billingAddress;
    }

    protected AddressModel createBillingAddress() {
        return getModelService().create(AddressModel.class);
    }

    protected void populateBillingAddress(final AddressModel billingAddress, final BillingInfo billingInfo) {
        final CountryModel countryModel = commonI18NService.getCountry(billingInfo.getCountry());
        //This is absurd and should be refactored out, if we do international shipping
        if (countryModel != null) {
            for (final RegionModel regionModel : countryModel.getRegions()) {
                if (regionModel.getName().equalsIgnoreCase(billingInfo.getState())) {
                    billingAddress.setRegion(regionModel);
                    break;
                }
            }
            billingAddress.setCountry(countryModel);
        }
        billingAddress.setFirstname(billingInfo.getFirstName());
        billingAddress.setLastname(billingInfo.getLastName());
        billingAddress.setEmail(billingInfo.getEmail());
        billingAddress.setLine1(billingInfo.getStreet1());
        billingAddress.setLine2(billingInfo.getStreet2());
        billingAddress.setTown(billingInfo.getCity());
        billingAddress.setPostalcode(billingInfo.getPostalCode());
        billingAddress.setPhone1(billingInfo.getPhoneNumber());
        billingAddress.setBillingAddress(Boolean.TRUE);
    }

    /**
     * @param entry
     * @param result
     */
    protected void updateTransactionEntryWithCaptureResult(final PaymentTransactionEntryModel entry,
            final TargetAbstractPaymentResult result) {

        // For immediate payment (gift card), payment transaction entry has already been updated
        // as successful/failure. Hence, it is not required to update again.
        if (entry.getIpgPaymentInfo() instanceof IpgGiftCardPaymentInfoModel
                && PaymentTransactionType.CAPTURE.equals(entry.getType())) {
            return;
        }

        final List<Object> toSave = new LinkedList<>();
        toSave.add(entry);

        if (result.getTotalAmount() != null) {
            entry.setAmount(result.getTotalAmount());
        }

        if (result.getRequestTime() != null) {
            entry.setTime(result.getRequestTime());
        }

        if (result.getCurrency() != null) {
            entry.setCurrency(commonI18NService.getCurrency(result.getCurrency().getCurrencyCode()));
        }

        entry.setRequestId(result.getRequestId());
        entry.setRequestToken(result.getRequestToken());

        if (StringUtils.isNotBlank(result.getReconciliationId())) {
            entry.setReceiptNo(result.getReconciliationId());
        }
        else {
            entry.setReceiptNo(TgtpaymentConstants.NOT_AVAILABLE);
        }

        if (result.getTransactionStatus() != null) {
            entry.setTransactionStatus(result.getTransactionStatus().toString());
        }

        if (result.getTransactionStatusDetails() != null) {
            final TransactionStatusDetails transactionStatusDetails = result.getTransactionStatusDetails();
            //Override the transaction status to 'Review' for Timeout
            if (TransactionStatusDetails.TIMEOUT.equals(transactionStatusDetails)) {
                entry.setTransactionStatus(TransactionStatus.REVIEW.toString());
            }
            entry.setTransactionStatusDetails(result.getTransactionStatusDetails().toString());
        }
        else if (TransactionStatus.ACCEPTED.equals(result.getTransactionStatus())) {
            // If transaction was accepted and no details supplied, then clear existing REVIEW_NEEDED description
            entry.setTransactionStatusDetails(null);
        }
        //Sets the decline code and message in transaction entry for IPG payment
        if (StringUtils.isNotEmpty(result.getDeclineCode())) {
            final String message = result.getDeclineMessage();
            if (StringUtils.isNotEmpty(message)) {
                entry.setDeclinedTransactionStatusInfo(result.getDeclineCode() + " - " + message);
            }
            else {
                entry.setDeclinedTransactionStatusInfo(result.getDeclineCode() + " - " + "N/A");
            }
        }
        if (result instanceof TargetRetrieveTransactionResult) {
            final PaymentInfoModel info = entry.getPaymentTransaction().getInfo();
            if (info instanceof PaypalHerePaymentInfoModel) {
                final PaypalHerePaymentInfoModel payPalHereInfo = (PaypalHerePaymentInfoModel)info;
                final TargetRetrieveTransactionResult retrieveResult = (TargetRetrieveTransactionResult)result;
                payPalHereInfo.setPayerId(retrieveResult.getPayerId());
                toSave.add(payPalHereInfo);

                //Dodgy pay pal app it is possible to change the amount to be different to what we requested
                final Double capturedAmount = retrieveResult.getCapturedAmount();
                if (capturedAmount != null && capturedAmount.doubleValue() > 0.0d) {
                    final BigDecimal captured = BigDecimal.valueOf(capturedAmount.doubleValue()).setScale(2); // in Australia we use 2 decimal places
                    entry.setAmount(captured);
                }
            }
        }
        if (result instanceof TargetFollowOnRefundResult) {
            if (entry.getPaymentTransaction().getInfo() instanceof AfterpayPaymentInfoModel) {
                final AfterpayPaymentInfoModel afterpayPaymentInfo = (AfterpayPaymentInfoModel)entry
                        .getPaymentTransaction()
                        .getInfo();
                afterpayPaymentInfo.setAfterpayRefundId(result.getReconciliationId());
                afterpayPaymentInfo.setAfterpayRefundRequestId(result.getRequestId());
                toSave.add(afterpayPaymentInfo);
            }
            else if (entry.getPaymentTransaction().getInfo() instanceof ZippayPaymentInfoModel) {
                final ZippayPaymentInfoModel zipPaymentInfo = (ZippayPaymentInfoModel)entry
                        .getPaymentTransaction()
                        .getInfo();
                zipPaymentInfo.setRefundId(result.getReconciliationId());
                toSave.add(zipPaymentInfo);
            }
        }
        try {
            getModelService().saveAll(toSave);
        }
        catch (final ModelSavingException e) {
            String orderCode = null;
            try {
                orderCode = entry.getPaymentTransaction().getOrder().getCode();
            }
            catch (final Exception ex) {
                orderCode = null;
            }
            LOG.warn(SplunkLogFormatter.formatOrderMessage("Saving of updated PaymentTransactionEntryModel failed",
                    TgtutilityConstants.ErrorCode.WARN_PAYMENT, orderCode), e);
        }
    }

    @Override
    public void retryCapture(final PaymentTransactionEntryModel paymentTransactionEntry) {
        Assert.notNull(paymentTransactionEntry);

        final AbstractOrderModel order = paymentTransactionEntry.getPaymentTransaction().getOrder();
        final PaymentTransactionModel paymentTransaction = paymentTransactionEntry.getPaymentTransaction();
        final PaymentInfoModel paymentInfo = paymentTransaction.getInfo();
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(paymentInfo,
                paymentTransaction.getPaymentProvider());

        final BigDecimal amount = paymentTransactionEntry.getAmount();

        LOG.info(MessageFormat.format(
                "ORDER-PAYMENTRECAPTURE : Re-capturing the payment on order :{0} for an amount of {1}",
                order.getCode(), amount.toString()));

        final TargetCaptureResult result = doCapture(order, paymentInfo, paymentTransactionEntry,
                amount, paymentTransactionEntry.getCurrency(), paymentMethod, null,
                paymentTransactionEntry.getPaymentTransaction().getCode());

        if (result != null) {
            updateTransactionEntryWithCaptureResult(paymentTransactionEntry, result);

            // In the case of a retry place order for paypal, 
            // the order has a clone of the paymentinfo that has not been filled with paypal details yet,
            // so we need to do that too. See TargetCommerceCheckoutServiceImpl.placeOrder
            if (paymentInfo instanceof PaypalPaymentInfoModel) {
                if (order.getPaymentInfo().getBillingAddress() == null) {
                    order.getPaymentInfo().setBillingAddress(getModelService().clone(paymentInfo.getBillingAddress()));
                }

                if (order.getPaymentInfo() instanceof PaypalPaymentInfoModel) {
                    final PaypalPaymentInfoModel orderPaymentInfo = (PaypalPaymentInfoModel)order.getPaymentInfo();

                    if (orderPaymentInfo.getEmailId() == null) {
                        orderPaymentInfo.setEmailId(((PaypalPaymentInfoModel)paymentInfo).getEmailId());
                    }
                }

                if (order.getPaymentAddress() == null) {
                    order.setPaymentAddress(order.getPaymentInfo().getBillingAddress());
                }

                try {
                    getModelService().saveAll(order.getPaymentInfo(), order);
                }
                catch (final ModelSavingException e) {
                    LOG.warn(SplunkLogFormatter.formatOrderMessage("Saving of paypal PaymentInfo failed",
                            TgtutilityConstants.ErrorCode.WARN_PAYMENT, order.getCode()), e);
                }
            }

        }
    }

    @Override
    public List<PaymentTransactionEntryModel> refundFollowOn(final PaymentTransactionModel transaction,
            final BigDecimal amount) {
        final List<PaymentTransactionEntryModel> refundedEntries = new ArrayList<>();
        Assert.notNull(transaction, "transaction must not be null");
        Assert.notNull(amount, "amount must not be null");

        final List<PaymentTransactionEntryData> captureList = PaymentTransactionHelper
                .createPaymentTransactionEntryData(transaction, amount);
        if (CollectionUtils.isEmpty(captureList)) {
            throw new PaymentException("Could not perform follow-on refund for this order.");
        }

        for (final PaymentTransactionEntryData capture : captureList) {
            final PaymentTransactionEntryModel entryModel = capture.getPaymentTransactionEntryModel();
            final BigDecimal refundAmount = capture.getAmount();
            if (entryModel == null || refundAmount == null) {
                continue;
            }
            PaymentTransactionEntryModel refundEntry = null;
            try {
                refundEntry = refundFollowOn(entryModel, refundAmount);
                getModelService().refresh(transaction);
            }
            catch (final Exception e) {
                LOG.error(SplunkLogFormatter.formatOrderMessage(MessageFormat.format(
                        "Failed to refund transaction entry ={0} in transaction={1} in order={2}", entryModel.getPk(),
                        transaction.getPk(), transaction.getOrder() != null ? transaction.getOrder().getPk()
                                : StringUtils.EMPTY),
                        ErrorCode.ERR_IPGPAYMENT, transaction.getOrder() != null ? transaction.getOrder().getCode()
                                : StringUtils.EMPTY),
                        e);
            }
            if (refundEntry != null) {
                refundedEntries.add(refundEntry);
            }
        }

        return refundedEntries;

    }

    /**
     * This is to refund the last payment transaction if its all entries are capture entries.
     */
    @Override
    public void refundLastCaptureTransaction(final AbstractOrderModel abstractOrderModel) {
        Assert.notNull(abstractOrderModel, "cart/order must not be null");
        final PaymentTransactionModel captureTransaction = PaymentTransactionHelper
                .findLastTransaction(abstractOrderModel);
        if (captureTransaction == null || !PaymentTransactionHelper.isCaptureTransaction(captureTransaction)) {
            return;
        }
        for (final PaymentTransactionEntryModel captureEntry : captureTransaction.getEntries()) {
            if (TransactionStatus.ACCEPTED.toString().equals(captureEntry.getTransactionStatus())) {
                PaymentTransactionEntryModel refundEntry = null;
                try {
                    refundEntry = refundFollowOn(captureEntry, captureEntry.getAmount());
                }
                catch (final Exception e) {
                    LOG.error(SplunkLogFormatter.formatOrderMessage(MessageFormat.format(
                            "Failed to refund transaction entry receiptNumber:{0}", captureEntry.getReceiptNo()),
                            ErrorCode.ERR_IPGPAYMENT, abstractOrderModel.getCode()),
                            e);
                }
                finally {
                    if (refundEntry == null
                            || (!TransactionStatus.ACCEPTED.toString().equals(refundEntry.getTransactionStatus()))) {
                        createReverseProcess(captureEntry);
                    }
                }
            }
            else if (TransactionStatus.REVIEW.toString().equals(captureEntry.getTransactionStatus())) {
                markTransactionEntryAsError(captureEntry);
            }
        }
    }

    @Override
    public PaymentTransactionEntryModel refundFollowOn(final PaymentTransactionEntryModel paymentTransactionEntryModel,
            final BigDecimal amount) {
        Assert.notNull(amount, "amount must not be null");
        Assert.notNull(paymentTransactionEntryModel, "paymentTransactionEntryModel must not be null");

        final PaymentTransactionModel transaction = paymentTransactionEntryModel.getPaymentTransaction();
        Assert.notNull(transaction, "transaction must not be null");

        //Throw an exception, if we are trying to refund more than what we captured.
        if (amount.compareTo(paymentTransactionEntryModel.getAmount()) > 0) {
            throw new PaymentException(MessageFormat.format("Could not refund {0}, while the captured amount is {1}",
                    amount, paymentTransactionEntryModel.getAmount()));
        }

        // Create the result PTEM in advance so it remains even after failure
        final PaymentTransactionEntryModel entry = createInitialisedPaymentTransactionEntryForRefundFollowOn(
                transaction,
                PaymentTransactionType.REFUND_FOLLOW_ON, getNewEntryCode(transaction), amount,
                paymentTransactionEntryModel.getCurrency(), paymentTransactionEntryModel);

        getModelService().save(entry);
        executeFollowOnRefund(paymentTransactionEntryModel, transaction, entry);
        getModelService().refresh(entry);
        return entry;
    }

    /**
     * perform refund follow on for given payment transaction entry
     * 
     * @param paymentTransactionEntryModel
     *            is the original capture entry
     * @param transaction
     *            is the related payment transaction record
     * @param refundEntry
     *            is the newly created entry with info of the current refund
     */
    protected void executeFollowOnRefund(
            final PaymentTransactionEntryModel paymentTransactionEntryModel, final PaymentTransactionModel transaction,
            final PaymentTransactionEntryModel refundEntry) {
        final TargetFollowOnRefundRequest followOnRefundRequest = createRefundFollowOnRequest(
                paymentTransactionEntryModel, transaction, refundEntry.getAmount());

        // Call payment provider
        TargetFollowOnRefundResult result = null;
        try {
            result = paymentMethodStrategy.getPaymentMethod(transaction.getInfo(),
                    transaction.getPaymentProvider()).followOnRefund(followOnRefundRequest);
        }
        catch (final AdapterException ae) {
            // Set details with error message
            String orderCode = null;
            if (transaction.getOrder() != null) {
                orderCode = transaction.getOrder().getCode();
            }
            setEntryErrorDetails(refundEntry,
                    "Follow on refund failed for order " + orderCode + " : " + ae.getMessage());
        }

        // Populate entry from result
        if (result != null) {
            updateTransactionEntryWithCaptureResult(refundEntry, result);
        }
    }

    /**
     * Create refund request for given transaction entry
     * 
     * @param paymentTransactionEntryModel
     *            is the original capture entry
     * @param transaction
     *            is the payment transaction record
     * @param amount
     *            is the amount to refund
     * @return followOnRefundRequest
     */
    protected TargetFollowOnRefundRequest createRefundFollowOnRequest(
            final PaymentTransactionEntryModel paymentTransactionEntryModel, final PaymentTransactionModel transaction,
            final BigDecimal amount) {
        final TargetFollowOnRefundRequest followOnRefundRequest = new TargetFollowOnRefundRequest();
        final String entryCode = getNewEntryCode(transaction);

        if (paymentTransactionEntryModel.getCurrency() != null) {
            followOnRefundRequest.setCurrency(Currency.getInstance(paymentTransactionEntryModel.getCurrency()
                    .getIsocode()));
        }
        followOnRefundRequest.setTotalAmount(amount);
        followOnRefundRequest.setOrderId(transaction.getCode());
        followOnRefundRequest.setTransactionId(entryCode);
        followOnRefundRequest.setToken(paymentTransactionEntryModel.getRequestToken());
        // The transactionId should be capture.requestId, which was set in the initial capture
        // For PayPal it is generated by the PSP, for TNS we pass it through
        followOnRefundRequest.setCaptureTransactionId(paymentTransactionEntryModel.getRequestId());
        //Set if it is partial or full refund
        followOnRefundRequest.setPartial(amount.compareTo(paymentTransactionEntryModel.getAmount()) < 0);
        followOnRefundRequest.setReceiptNumber(paymentTransactionEntryModel.getReceiptNo());
        final PaymentInfoModel ipgPaymentInfo = paymentTransactionEntryModel.getIpgPaymentInfo();
        followOnRefundRequest.setGiftcard(ipgPaymentInfo != null
                && ipgPaymentInfo instanceof IpgGiftCardPaymentInfoModel);
        return followOnRefundRequest;
    }

    private PaymentTransactionEntryModel createInitialisedPaymentTransactionEntryForRefundFollowOn(
            final PaymentTransactionModel transaction,
            final PaymentTransactionType type, final String code, final BigDecimal amount,
            final CurrencyModel currency, final PaymentTransactionEntryModel captureEntry) {
        final PaymentTransactionEntryModel entry = createInitialisedPaymentTransactionEntry(transaction, type, code,
                amount, currency);
        final PaymentInfoModel info = transaction.getInfo();
        if (info instanceof IpgPaymentInfoModel && PaymentTransactionType.REFUND_FOLLOW_ON.equals(type)) {
            entry.setIpgPaymentInfo(captureEntry.getIpgPaymentInfo());
        }
        return entry;
    }

    private PaymentTransactionEntryModel createInitialisedPaymentTransactionEntry(
            final PaymentTransactionModel transaction,
            final PaymentTransactionType type, final String code, final BigDecimal amount,
            final CurrencyModel currency) {

        final PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);
        entry.setPaymentTransaction(transaction);
        entry.setType(type);
        entry.setCode(code);
        if (transaction.getOrder() != null && transaction.getOrder().getNormalSaleStartDateTime() != null) {
            entry.setAmount(transaction.getPlannedAmount());
        }
        else {
            entry.setAmount(amount);
        }

        entry.setCurrency(currency);
        entry.setTime(new Date());

        // Initially we'll set the status to REVIEW - it will remain as this if there is a problem 
        entry.setTransactionStatus(TransactionStatus.REVIEW.toString());
        entry.setTransactionStatusDetails(TransactionStatusDetails.REVIEW_NEEDED.toString());
        //The credit card information will get populated only for IPG
        return entry;
    }

    @Override
    public void retrieveTransactionEntry(final PaymentTransactionEntryModel entry)
            throws InvalidTransactionRequestException {
        final PaymentTransactionModel paymentTransaction = entry.getPaymentTransaction();
        final TargetRetrieveTransactionRequest request = createRetrieveTransacitonRequest(entry, paymentTransaction);
        try {
            final TargetRetrieveTransactionResult result = paymentMethodStrategy.getPaymentMethod(
                    paymentTransaction.getInfo(),
                    paymentTransaction.getPaymentProvider()).retrieveTransaction(request);
            if (result != null) {
                updateTransactionEntryWithCaptureResult(entry, result);
                getModelService().refresh(entry);
            }
        }
        catch (final AdapterException aep) {
            LOG.warn(MessageFormat.format("Transaction {0} not found: ", paymentTransaction.getCode()), aep);
            throw new InvalidTransactionRequestException(aep);
        }
    }

    /**
     * @param entry
     * @param paymentTransaction
     * @return TargetRetrieveTransactionRequest
     */
    protected TargetRetrieveTransactionRequest createRetrieveTransacitonRequest(
            final PaymentTransactionEntryModel entry,
            final PaymentTransactionModel paymentTransaction) {
        final TargetRetrieveTransactionRequest request = new TargetRetrieveTransactionRequest();
        request.setTransactionId(entry.getCode());
        request.setOrderId(paymentTransaction.getCode());
        final AbstractOrderModel order = paymentTransaction.getOrder();
        if (null != order) {
            final PaymentInfoModel paymentInfo = paymentTransaction.getInfo();
            request.setOrderId(order.getCode());
            request.setOrderDate(order.getDate());
            if (paymentInfo != null) {
                if (paymentInfo instanceof IpgPaymentInfoModel) {
                    setSessionIdTokenAmountAndTrnTypeForIpg(entry, request, (IpgPaymentInfoModel)paymentInfo);
                }
                else if (paymentInfo instanceof AfterpayPaymentInfoModel) {
                    setSessionTokenForAfterpay(request, (AfterpayPaymentInfoModel)paymentInfo);

                }

            }
        }
        return request;
    }

    /**
     * @param request
     * @param paymentInfo
     */
    private void setSessionTokenForAfterpay(final TargetRetrieveTransactionRequest request,
            final AfterpayPaymentInfoModel paymentInfo) {
        request.setSessionToken(paymentInfo.getAfterpayToken());
    }

    /**
     * @param entry
     * @param request
     * @param paymentInfo
     */
    private void setSessionIdTokenAmountAndTrnTypeForIpg(final PaymentTransactionEntryModel entry,
            final TargetRetrieveTransactionRequest request, final IpgPaymentInfoModel paymentInfo) {
        request.setSessionId(paymentInfo.getSessionId());
        request.setSessionToken(paymentInfo.getToken());
        request.setEntryAmount(entry.getAmount());
        if (entry.getIpgPaymentInfo() != null) {
            request.setQueryGiftCardOnly(entry.getIpgPaymentInfo() instanceof IpgGiftCardPaymentInfoModel);
        }
        //for ipg, only try to retrieve successful transaction
        request.setTrnType(TrnType.APPROVED);
    }

    @Override
    public PaymentTransactionEntryModel refundStandaloneCapture(final AbstractOrderModel orderModel,
            final PaymentInfoModel paymentInfo, final String captureOrderId) {

        Assert.notNull(orderModel, "orderModel must not be null");
        Assert.notNull(paymentInfo, "paymentInfo must not be null");
        Assert.notNull(captureOrderId, "captureOrderId must not be null");

        return capture(orderModel, paymentInfo, REFUND_STANDALONE_CAPTURE_AMOUNT, orderModel.getCurrency(), null,
                captureOrderId, null);
    }

    @Override
    public List<PaymentInfoModel> getSuccessfulPayments(final OrderModel order) {
        final List<PaymentInfoModel> successfulPayments = new ArrayList<>();
        final List<PaymentTransactionModel> paymentTransactions = order.getPaymentTransactions();

        if (paymentTransactions != null) {
            for (final PaymentTransactionModel paymentTransaction : paymentTransactions) {
                final List<PaymentTransactionEntryModel> transactionEntries = paymentTransaction.getEntries();

                if (transactionEntries != null) {
                    for (final PaymentTransactionEntryModel transactionEntry : transactionEntries) {

                        if (PaymentTransactionType.CAPTURE.equals(transactionEntry.getType())
                                && TransactionStatus.ACCEPTED.toString()
                                        .equals(transactionEntry.getTransactionStatus())) {

                            final PaymentInfoModel paymentInfo = transactionEntry.getIpgPaymentInfo();
                            if (paymentInfo instanceof IpgCreditCardPaymentInfoModel) {
                                successfulPayments.add(paymentInfo);
                            }
                        }
                    }
                }
            }
        }
        return successfulPayments;
    }

    @Override
    public PaymentTransactionEntryModel refundStandaloneRefund(final AbstractOrderModel orderModel,
            final PaymentTransactionEntryModel captureEntry,
            final BigDecimal amount, final String captureOrderId) {

        Assert.notNull(orderModel, "orderModel must not be null");
        Assert.notNull(captureEntry, "captureEntry must not be null");
        Assert.notNull(amount, "amount must not be null");
        Assert.notNull(captureOrderId, "captureOrderId must not be null");

        // Retrieve the payment transaction and info
        final PaymentTransactionModel paymentTransaction = captureEntry.getPaymentTransaction();
        Assert.notNull(paymentTransaction, "paymentTransaction from entry must not be null");
        final PaymentInfoModel paymentInfo = paymentTransaction.getInfo();
        Assert.notNull(paymentInfo, "paymentInfo from entry must not be null");

        // Create the result PTEM in advance so it remains even after failure
        final String entryCode = getNewEntryCode(captureEntry.getPaymentTransaction());
        final PaymentTransactionEntryModel entry = createInitialisedPaymentTransactionEntry(
                captureEntry.getPaymentTransaction(),
                PaymentTransactionType.REFUND_STANDALONE, entryCode, amount.add(REFUND_STANDALONE_CAPTURE_AMOUNT),
                captureEntry.getCurrency());

        getModelService().save(entry);

        final TargetExcessiveRefundRequest request = new TargetExcessiveRefundRequest();
        request.setTotalAmount(amount.add(REFUND_STANDALONE_CAPTURE_AMOUNT));
        request.setCurrency(Currency.getInstance(captureEntry.getCurrency().getIsocode()));
        request.setTransactionId(entryCode);
        request.setOriginalTransactionId(captureOrderId);

        // Call payment provider
        TargetExcessiveRefundResult result = null;
        try {
            result = paymentMethodStrategy.getPaymentMethod(
                    paymentInfo, captureEntry.getPaymentTransaction().getPaymentProvider()).excessiveRefund(request);
        }
        catch (final AdapterException ae) {
            setEntryErrorDetails(entry, "Standalone refund failed for order " + orderModel.getCode()
                    + " : " + ae.getMessage());
        }

        if (result != null) {
            updateTransactionEntryWithCaptureResult(entry, result);
        }

        return entry;
    }

    @Override
    public PaymentTransactionEntryModel refundIpgManualRefund(final PaymentTransactionModel captureTransaction,
            final BigDecimal amount, final String receiptNo) {
        Assert.notNull(captureTransaction, "capture Transaction must not be null");
        Assert.notNull(amount, "amount must not be null");
        final String entryCode = getNewEntryCode(captureTransaction);
        final PaymentTransactionEntryModel entry = createInitialisedPaymentTransactionEntry(
                captureTransaction,
                PaymentTransactionType.REFUND_STANDALONE, entryCode, amount, captureTransaction.getCurrency());
        entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        entry.setTransactionStatusDetails(TransactionStatus.ACCEPTED.toString());
        entry.setReceiptNo(receiptNo);
        getModelService().save(entry);
        getModelService().refresh(entry);
        getModelService().refresh(captureTransaction);
        return entry;
    }

    /**
     * @param entry
     */
    private void markTransactionEntryAsError(final PaymentTransactionEntryModel entry) {
        entry.setTransactionStatus(TransactionStatus.ERROR.toString());
        entry.setTransactionStatusDetails(TransactionStatusDetails.PROCESSOR_DECLINE.toString());
        entry.setDeclinedTransactionStatusInfo("Transaction cancelled due to validation/payment error");
        getModelService().save(entry);
    }

    private String getNewEntryCode(final PaymentTransactionModel transaction) {
        getModelService().save(transaction);
        getModelService().refresh(transaction);
        final String stem = transaction.getCode();

        if (transaction.getEntries() == null) {
            return stem + "-1";
        }
        return stem + "-" + (transaction.getEntries().size() + 1);
    }

    @Override
    public BigDecimal findRefundedAmountAfterRefundFollowOn(final List<PaymentTransactionEntryModel> refundedEntries) {
        BigDecimal refundedAmount = new BigDecimal(0);
        for (final PaymentTransactionEntryModel refundedEntry : refundedEntries) {
            if (TransactionStatus.ACCEPTED
                    .equals(TransactionStatus.valueOf(refundedEntry.getTransactionStatus()))
                    && PaymentTransactionType.REFUND_FOLLOW_ON.equals(refundedEntry.getType())
                    && refundedEntry.getAmount() != null && refundedEntry.getAmount().compareTo(BigDecimal.ZERO) >= 0) {
                refundedAmount = refundedAmount.add(refundedEntry.getAmount());
            }
        }
        return refundedAmount;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtpayment.TargetPaymentService#queryFailedGiftCardPayments()
     */
    @Override
    public List<PaymentTransactionEntryData> retrieveFailedGiftcardPayment(
            final AbstractOrderModel abstractOrderModel) {
        final TargetRetrieveTransactionRequest request = new TargetRetrieveTransactionRequest();
        request.setOrderId(abstractOrderModel.getCode());
        request.setOrderDate(abstractOrderModel.getDate());
        request.setQueryGiftCardOnly(true);
        request.setTrnType(TrnType.FAILED);
        final TargetRetrieveTransactionResult result = paymentMethodStrategy.getPaymentMethod(
                abstractOrderModel.getPaymentInfo()).retrieveTransaction(
                request);
        return result == null ? null : result.getPayments();
    }

    /**
     * @param cartModel
     * @param card
     */
    protected void createReverseProcess(final CartModel cartModel, final TargetCardResult card) {
        Assert.notNull(cartModel, "cartModel should not be null");
        final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
        Assert.notNull(paymentInfo, "paymentInfo should not be null");
        final AddressModel shipAddress = cartModel.getDeliveryAddress();
        final GiftCardReversalData reversalData = new GiftCardReversalData();
        reversalData.setAmount(card.getTargetCardPaymentResult().getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)
                .toString());
        reversalData.setCustomerEmail(cartModel.getUser().getUid());
        reversalData.setMaskedCardNumber(card.getCardNumber());
        reversalData.setProvider(TgtpaymentConstants.IPG_PAYMENT_TYPE);
        reversalData.setReceiptNumber(card.getTargetCardPaymentResult().getReceiptNumber());
        if (shipAddress != null) {
            reversalData.setMobile(shipAddress.getPhone1());
            if (shipAddress.getTitle() != null) {
                reversalData.setTitle(shipAddress.getTitle().getName());
            }
            reversalData.setFirstName(shipAddress.getFirstname());
            reversalData.setLastName(shipAddress.getLastname());
        }
        reversalData.setCartId(cartModel.getCode());
        reversalData.setPaymentDate(new Date());
        getTargetBusinessProcessService().startGiftCardReverseProcess(reversalData, reversalData.getCustomerEmail(),
                reversalData.getReceiptNumber(),
                null);
    }



    /**
     * @param captureEntry
     */
    private void createReverseProcess(final PaymentTransactionEntryModel captureEntry) {
        Assert.notNull(captureEntry, "cartModel should not be null");
        if (captureEntry.getIpgPaymentInfo() != null
                && captureEntry.getIpgPaymentInfo() instanceof IpgGiftCardPaymentInfoModel) {
            final IpgGiftCardPaymentInfoModel giftCardInfo = (IpgGiftCardPaymentInfoModel)captureEntry
                    .getIpgPaymentInfo();
            final AbstractOrderModel order = captureEntry.getPaymentTransaction().getOrder();
            final PaymentInfoModel paymentInfo = order.getPaymentInfo();
            Assert.notNull(paymentInfo, "paymentInfo should not be null");
            final AddressModel shipAddress = order.getDeliveryAddress();
            final GiftCardReversalData reversalData = new GiftCardReversalData();
            reversalData.setAmount(captureEntry.getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
            reversalData.setCustomerEmail(order.getUser().getUid());
            reversalData.setMaskedCardNumber(giftCardInfo.getMaskedNumber());
            reversalData.setProvider(TgtpaymentConstants.IPG_PAYMENT_TYPE);
            reversalData.setReceiptNumber(captureEntry.getReceiptNo());
            if (shipAddress != null) {
                reversalData.setMobile(shipAddress.getPhone1());
                if (shipAddress.getTitle() != null) {
                    reversalData.setTitle(shipAddress.getTitle().getName());
                }
                reversalData.setFirstName(shipAddress.getFirstname());
                reversalData.setLastName(shipAddress.getLastname());
            }
            reversalData.setCartId(order.getCode());
            reversalData.setPaymentDate(new Date());
            getTargetBusinessProcessService()
                    .startGiftCardReverseProcess(reversalData, reversalData.getCustomerEmail(),
                            reversalData.getReceiptNumber(), captureEntry);
        }
    }

    /**
     * 
     * @param paymentMethod
     * @param receiptNumber
     * @param date
     * @param customerRef
     * @return isReversalSuccessful
     */
    private boolean reverseGiftCardPayment(final TargetPaymentMethod paymentMethod,
            final String receiptNumber, final Date date, final String customerRef) {
        if (hasApprovedTransaction(paymentMethod, receiptNumber, date, customerRef)) {
            final TargetPaymentVoidRequest voidRequest = new TargetPaymentVoidRequest();
            voidRequest.setReceiptNumber(receiptNumber);
            try {
                final TargetPaymentVoidResult result = paymentMethod.voidPayment(voidRequest);
                return result != null && result.getVoidResult() != null && result.getVoidResult().isPaymentSuccessful();
            }
            catch (final Exception e) {
                LOG.error(SplunkLogFormatter.formatMessage(MessageFormat.format(
                        "Failed to reverse giftcard receiptNumber:{0}", receiptNumber),
                        TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), e);
                return false;
            }
        }
        return true;
    }

    /**
     * @param paymentMethod
     * @param receiptNumber
     * @param date
     * @param customerRef
     * @return true if has approved transaction else false
     */
    private boolean hasApprovedTransaction(final TargetPaymentMethod paymentMethod, final String receiptNumber,
            final Date date, final String customerRef) {
        final TargetRetrieveTransactionRequest retrieveTransactionRequest = new TargetRetrieveTransactionRequest();
        retrieveTransactionRequest.setOrderDate(date);
        retrieveTransactionRequest.setOrderId(customerRef);
        retrieveTransactionRequest.setReceiptNumber(receiptNumber);
        retrieveTransactionRequest.setQueryGiftCardOnly(true);
        retrieveTransactionRequest.setTrnType(TrnType.APPROVED);
        final TargetRetrieveTransactionResult queryResult = paymentMethod
                .retrieveTransaction(retrieveTransactionRequest);
        return queryResult != null && !CollectionUtils.isEmpty(queryResult.getPayments());
    }

    @Override
    public List<TargetCardResult> getGiftCardPayments(final CartModel cartModel) {
        List<TargetCardResult> giftCards = new ArrayList<>();
        if (cartModel != null) {
            final PaymentInfoModel paymentInfoModel = cartModel.getPaymentInfo();
            if (paymentInfoModel != null) {
                final TargetPaymentMethod paymentMethod = getPaymentMethodStrategy().getPaymentMethod(paymentInfoModel);
                if (paymentMethod != null && paymentMethod instanceof TargetIpgPaymentMethod) {
                    final IpgPaymentInfoModel ipgPaymentInfoModel = (IpgPaymentInfoModel)paymentInfoModel;
                    giftCards = getGiftCardPayments(ipgPaymentInfoModel.getToken(),
                            ipgPaymentInfoModel.getSessionId());
                    if (giftCards != null) {
                        for (final TargetCardResult ipgCardResult : giftCards) {
                            String deliveryName = "N/A";
                            String receiptNumber = "N/A";
                            String status = "";
                            if (cartModel.getDeliveryAddress() != null) {
                                deliveryName = new StringBuilder()
                                        .append(cartModel.getDeliveryAddress().getFirstname())
                                        .append(" ")
                                        .append(cartModel.getDeliveryAddress().getLastname()).toString();
                            }
                            if (ipgCardResult.getTargetCardPaymentResult() != null) {
                                receiptNumber = ipgCardResult.getTargetCardPaymentResult().getReceiptNumber();
                                if (StringUtils.isNotEmpty(ipgCardResult.getTargetCardPaymentResult()
                                        .getResponseCode())) {
                                    status = ipgCardResult.getTargetCardPaymentResult().getResponseCode().equals(
                                            GIFTCARD_PAYMENT_RESPONSE_CODE) ? "SUCEESS" : "FAIL";
                                }
                            }
                            // It is not required to store gift card information. Just log them to appear in Splunk
                            LOG.info(SplunkLogFormatter.formatInfoMessage(
                                    String.format(GIFTCARD_INFO_MESSAGE, deliveryName, status, receiptNumber,
                                            cartModel.getCode()),
                                    InfoCode.INFO_GIFTCARD_PAYMENT));
                        }
                    }
                }
            }
        }

        return giftCards;
    }

    private List<TargetCardResult> getGiftCardPayments(final String ipgToken, final String ipgSessionId) {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetPaymentMethod paymentMethod = getPaymentMethodStrategy().getPaymentMethod(
                TgtpaymentConstants.IPG_PAYMENT_TYPE);
        if (paymentMethod != null && paymentMethod instanceof TargetIpgPaymentMethod) {
            // Query transaction details
            final TargetQueryTransactionDetailsResult result = queryTransactionDetails(paymentMethod, ipgToken,
                    ipgSessionId);
            // Query transaction can happen after payment completion (success) or before payment completion (pending)
            if (result != null && (result.isSuccess() || result.isPending())) {
                final List<TargetCardResult> cardResults = result.getCardResults();
                if (CollectionUtils.isNotEmpty(cardResults)) {
                    for (final TargetCardResult ipgCardResult : cardResults) {
                        if (CreditCardType.GIFTCARD.getCode().equalsIgnoreCase(ipgCardResult.getCardType())) {
                            giftCards.add(ipgCardResult);
                        }
                    }
                }
            }
        }
        return giftCards;
    }

    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(final TargetPaymentMethod paymentMethod,
            final String ipgToken, final String ipgSessionId) {
        Assert.notNull(paymentMethod, "Payment method should not be null");
        final TargetQueryTransactionDetailsRequest request = new TargetQueryTransactionDetailsRequest();
        request.setSessionToken(ipgToken);
        request.setSessionId(ipgSessionId);
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = paymentMethod
                .queryTransactionDetails(request);
        return queryTransactionDetailsResult;
    }

    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(final TargetPaymentMethod paymentMethod,
            final PaymentInfoModel paymentInfoModel) {
        final TargetQueryTransactionDetailsRequest request = new TargetQueryTransactionDetailsRequest();
        final IpgPaymentInfoModel ipgPaymentInfoModel = (IpgPaymentInfoModel)paymentInfoModel;
        request.setSessionToken(ipgPaymentInfoModel.getToken());
        request.setSessionId(ipgPaymentInfoModel.getSessionId());
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = paymentMethod
                .queryTransactionDetails(request);
        return queryTransactionDetailsResult;
    }


    @Override
    public boolean reverseGiftCardPayment(final CartModel cartModel) {
        boolean giftCardReversalSuccessful = true;
        if (cartModel != null) {
            try {
                final List<TargetCardResult> giftCards = getGiftCardPayments(cartModel);
                if (CollectionUtils.isNotEmpty(giftCards)) {
                    giftCardReversalSuccessful = reversePayment(cartModel, giftCards);
                }
            }
            catch (final Exception e) {
                LOG.error("error reversing giftcard for cart=" + cartModel.getCode(), e);
                giftCardReversalSuccessful = false;
            }
        }
        return giftCardReversalSuccessful;
    }

    /**
     * @return false if failed to reverse any gift card payment, otherwise true (including no gift card found)
     */
    @Override
    public boolean reverseGiftCardPayment(final String ipgToken, final String ipgSessionId) {
        boolean result = true;

        final List<TargetCardResult> giftCards = getGiftCardPayments(ipgToken, ipgSessionId);

        if (CollectionUtils.isNotEmpty(giftCards)) {
            result = reversePayment(null, giftCards);
        }
        return result;
    }

    /**
     * @return the paymentMethodStrategy
     */
    protected PaymentMethodStrategy getPaymentMethodStrategy() {
        return paymentMethodStrategy;
    }

    /**
     * @param paymentMethodStrategy
     *            the paymentMethodStrategy to set
     */
    public void setPaymentMethodStrategy(final PaymentMethodStrategy paymentMethodStrategy) {
        this.paymentMethodStrategy = paymentMethodStrategy;
    }

    /**
     * @return the commonI18NService
     */
    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @return the flexibleSearchService
     */
    protected FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }


    /**
     * @return the transactionCodeGenerator
     */
    protected TransactionCodeGenerator getTransactionCodeGenerator() {
        return transactionCodeGenerator;
    }


    /**
     * @param transactionCodeGenerator
     *            the transactionCodeGenerator to set
     */
    @Required
    public void setTransactionCodeGenerator(final TransactionCodeGenerator transactionCodeGenerator) {
        this.transactionCodeGenerator = transactionCodeGenerator;
    }

    /**
     * @param paymentsInProgressService
     *            the paymentsInProgressService to set
     */
    @Required
    public void setPaymentsInProgressService(final PaymentsInProgressService paymentsInProgressService) {
        this.paymentsInProgressService = paymentsInProgressService;
    }

    /**
     * @param hostedSessionOrderConverter
     *            the hostedSessionOrderConverter to set
     */
    @Required
    public void setHostedSessionOrderConverter(final TargetPaymentAbstractOrderConverter hostedSessionOrderConverter) {
        this.hostedSessionOrderConverter = hostedSessionOrderConverter;
    }

    /**
     * @param captureOrderConverter
     *            the captureOrderConverter to set
     */
    @Required
    public void setCaptureOrderConverter(final TargetPaymentAbstractOrderConverter captureOrderConverter) {
        this.captureOrderConverter = captureOrderConverter;
    }

    /**
     * @param enumerationService
     *            the enumerationService to set
     */
    @Required
    public void setEnumerationService(final EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    /**
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    @Override
    public HostedSessionTokenRequest createHostedSessionTokenRequest(final AbstractOrderModel cart,
            final PaymentModeModel paymentMode,
            final String cancelUrl, final String returnUrl, final PaymentCaptureType pct,
            final String sessionId, final List<CreditCardPaymentInfoModel> savedCreditCards) {

        final BigDecimal amount = BigDecimal.valueOf(cart.getTotalPrice().doubleValue());
        final HostedSessionTokenRequest hostedSessionTokenRequest = getHostedSessionTokenRequest(cart, paymentMode,
                cancelUrl, returnUrl, pct, sessionId, amount);
        if (cart.getPaymentInfo() instanceof IpgPaymentInfoModel) {
            final IpgPaymentInfoModel ipgPaymentInfoModel = (IpgPaymentInfoModel)cart.getPaymentInfo();
            hostedSessionTokenRequest.setIpgPaymentTemplateType(ipgPaymentInfoModel.getIpgPaymentTemplateType());
        }
        if (BooleanUtils.isFalse(cart.getContainsPreOrderItems())) {
            // skip sending saved cards to IPG for preOrders
            addSavedCardInformation(savedCreditCards, hostedSessionTokenRequest);
        }

        return hostedSessionTokenRequest;
    }

    @Override
    public HostedSessionTokenRequest createUpdateCardHostedSessionTokenRequest(final AbstractOrderModel order,
            final PaymentModeModel paymentMode,
            final String cancelUrl, final String returnUrl, final PaymentCaptureType pct,
            final String sessionId) {
        final double remainingOrderAmount = order.getTotalPrice().doubleValue()
                - order.getPreOrderDepositAmount().doubleValue();
        final BigDecimal amount = BigDecimal
                .valueOf(remainingOrderAmount);
        final HostedSessionTokenRequest hostedSessionTokenRequest = getHostedSessionTokenRequest(order, paymentMode,
                cancelUrl, returnUrl, pct, sessionId, amount);
        hostedSessionTokenRequest.setIpgPaymentTemplateType(IpgPaymentTemplateType.UPDATECARD);
        return hostedSessionTokenRequest;

    }

    @Override
    public boolean verifyIfPaymentCapturedAndUpdateEntry(final PaymentTransactionEntryModel entry) {

        boolean isPaymentCaptured = false;
        final PaymentTransactionModel paymentTransaction = entry.getPaymentTransaction();
        final TargetRetrieveTransactionRequest request = createRetrieveTransacitonRequest(entry, paymentTransaction);
        final TargetRetrieveTransactionResult result = paymentMethodStrategy.getPaymentMethod(
                paymentTransaction.getInfo(),
                paymentTransaction.getPaymentProvider()).retrieveTransaction(request);

        if (result != null && result.getMatchingCount() > 0) {
            updateTransactionEntryWithCaptureResult(entry, result);
            // retrieveTransaction does not return the token, hence populate back
            if (paymentTransaction.getInfo() instanceof IpgPaymentInfoModel) {
                final IpgCreditCardPaymentInfoModel ccPaymentInfo = (IpgCreditCardPaymentInfoModel)entry
                        .getIpgPaymentInfo();
                entry.setRequestToken(ccPaymentInfo.getToken());
            }

            getModelService().save(entry);
            getModelService().refresh(entry);

            isPaymentCaptured = true;
        }

        return isPaymentCaptured;
    }

    private HostedSessionTokenRequest getHostedSessionTokenRequest(final AbstractOrderModel cart,
            final PaymentModeModel paymentMode, final String cancelUrl, final String returnUrl,
            final PaymentCaptureType pct, final String sessionId, final BigDecimal amount) {
        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();

        hostedSessionTokenRequest.setAmount(amount);
        hostedSessionTokenRequest.setCancelUrl(cancelUrl);
        hostedSessionTokenRequest.setOrderModel(cart);
        hostedSessionTokenRequest.setPaymentCaptureType(pct);
        hostedSessionTokenRequest.setPaymentMode(paymentMode);
        hostedSessionTokenRequest.setReturnUrl(returnUrl);
        hostedSessionTokenRequest.setSessionId(sessionId);
        return hostedSessionTokenRequest;
    }

    /**
     * Add customer saved card information to hosted session token request
     * 
     * @param savedCreditCards
     * @param hostedSessionTokenRequest
     */
    private void addSavedCardInformation(final List<CreditCardPaymentInfoModel> savedCreditCards,
            final HostedSessionTokenRequest hostedSessionTokenRequest) {

        if (CollectionUtils.isNotEmpty(savedCreditCards)) {
            for (final CreditCardPaymentInfoModel creditCardPaymentInfoModel : savedCreditCards) {

                if (creditCardPaymentInfoModel instanceof IpgCreditCardPaymentInfoModel) {
                    final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = (IpgCreditCardPaymentInfoModel) creditCardPaymentInfoModel;

                    final CreditCard creditCard = new CreditCard();
                    creditCard.setToken(ipgCreditCardPaymentInfoModel.getToken());
                    creditCard.setTokenExpiry(ipgCreditCardPaymentInfoModel.getTokenExpiry());
                    creditCard.setCardType(ipgCreditCardPaymentInfoModel.getType().toString());
                    creditCard.setMaskedCard(ipgCreditCardPaymentInfoModel.getNumber());
                    creditCard.setCardExpiry(ipgCreditCardPaymentInfoModel.getValidToMonth() + "/"
                            + ipgCreditCardPaymentInfoModel.getValidToYear());
                    creditCard.setBin(ipgCreditCardPaymentInfoModel.getBin());
                    creditCard.setDefaultCard(ipgCreditCardPaymentInfoModel.getDefaultCard());
                    creditCard.setPromoId(ipgCreditCardPaymentInfoModel.getPromoId());
                    creditCard.setCardOnFile(ipgCreditCardPaymentInfoModel.isCardOnFile());
                    creditCard.setFirstCredentialStorage(ipgCreditCardPaymentInfoModel.isFirstCredentialStorage());
                    hostedSessionTokenRequest.addSavedCreditCard(creditCard);
                }
            }
        }
    }

    /**
     * check Zip Pay availability.
     * 
     * @return - boolean
     */
    @Override
    public boolean ping(final String paymentProvider) {
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy
                .getPaymentMethod(paymentProvider);
        Assert.notNull(paymentMethod, "paymentMethod cannot be null!");

        final TargetPaymentPingResult result = paymentMethod
                .ping(new TargetPaymentPingRequest());
        if (result != null) {
            return result.isSuccess();
        }
        else {
            return false;
        }
    }

}