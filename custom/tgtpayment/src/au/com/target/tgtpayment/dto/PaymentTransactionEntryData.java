/**
 * 
 */
package au.com.target.tgtpayment.dto;

import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;


/**
 * @author bhuang3
 *
 */
public class PaymentTransactionEntryData {

    private PaymentTransactionEntryModel paymentTransactionEntryModel;

    private BigDecimal amount;

    private String receiptNumber;

    public PaymentTransactionEntryData() {
    }


    /**
     * @param paymentTransactionEntryModel
     * @param amount
     */
    public PaymentTransactionEntryData(final PaymentTransactionEntryModel paymentTransactionEntryModel,
            final BigDecimal amount) {
        this.paymentTransactionEntryModel = paymentTransactionEntryModel;
        this.amount = amount;
    }

    /**
     * @return the paymentTransactionEntryModel
     */
    public PaymentTransactionEntryModel getPaymentTransactionEntryModel() {
        return paymentTransactionEntryModel;
    }

    /**
     * @param paymentTransactionEntryModel
     *            the paymentTransactionEntryModel to set
     */
    public void setPaymentTransactionEntryModel(final PaymentTransactionEntryModel paymentTransactionEntryModel) {
        this.paymentTransactionEntryModel = paymentTransactionEntryModel;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

}
