package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetTokenizeCommand;


//CHECKSTYLE:ON

/**
 * Request for {@link TargetTokenizeCommand}
 * 
 */
public class TargetTokenizeRequest extends AbstractRequest {

    private String sessionToken;

    /**
     * @return the sessionToken
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * @param sessionToken
     *            the sessionToken to set
     */
    public void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }

}
