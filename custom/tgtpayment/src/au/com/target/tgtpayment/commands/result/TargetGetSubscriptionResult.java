/**
 * 
 */
package au.com.target.tgtpayment.commands.result;

import de.hybris.platform.payment.commands.result.SubscriptionDataResult;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtpayment.dto.LineItem;


/**
 * @author gsing236
 *
 */
public class TargetGetSubscriptionResult extends SubscriptionDataResult {

    private List<LineItem> lineItems;
    private BigDecimal amount;
    private String afterpayToken;
    private String orderState;
    private String orderReference;

    /**
     * @return the lineItems
     */
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems
     *            the lineItems to set
     */
    public void setLineItems(final List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the afterpayToken
     */
    public String getAfterpayToken() {
        return afterpayToken;
    }

    /**
     * @param afterpayToken
     *            the afterpayToken to set
     */
    public void setAfterpayToken(final String afterpayToken) {
        this.afterpayToken = afterpayToken;
    }

    /**
     * @return the orderState
     */
    public String getOrderState() {
        return orderState;
    }

    /**
     * @param orderState
     *            the orderState to set
     */
    public void setOrderState(final String orderState) {
        this.orderState = orderState;
    }

    /**
     * @return the orderReference
     */
    public String getOrderReference() {
        return orderReference;
    }

    /**
     * @param orderReference
     *            the orderReference to set
     */
    public void setOrderReference(final String orderReference) {
        this.orderReference = orderReference;
    }

}
