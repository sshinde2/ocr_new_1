package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetQueryTransactionDetailsCommand;


//CHECKSTYLE:ON

/**
 * 
 * Request for {@link TargetQueryTransactionDetailsCommand}
 * 
 */
public class TargetQueryTransactionDetailsRequest extends AbstractRequest {
    /**
     * Order Id
     */
    private String orderId;

    /**
     * Session Token associated with payment gateway
     */
    private String sessionToken;

    /**
     * Session id to which the transaction is associated with
     */
    private String sessionId;

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the sessionToken
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * @param sessionToken
     *            the sessionToken to set
     */
    public void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }
}