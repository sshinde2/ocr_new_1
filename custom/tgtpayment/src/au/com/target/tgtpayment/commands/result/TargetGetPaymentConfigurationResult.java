/**
 * 
 */
package au.com.target.tgtpayment.commands.result;

import java.math.BigDecimal;


/**
 * @author bhuang3
 *
 */
public class TargetGetPaymentConfigurationResult {

    private String description;

    private String type;

    private BigDecimal maximumAmount;

    private BigDecimal minimumAmount;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the maximumAmount
     */
    public BigDecimal getMaximumAmount() {
        return maximumAmount;
    }

    /**
     * @param maximumAmount
     *            the maximumAmount to set
     */
    public void setMaximumAmount(final BigDecimal maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    /**
     * @return the minimumAmount
     */
    public BigDecimal getMinimumAmount() {
        return minimumAmount;
    }

    /**
     * @param minimumAmount
     *            the minimumAmount to set
     */
    public void setMinimumAmount(final BigDecimal minimumAmount) {
        this.minimumAmount = minimumAmount;
    }


}
