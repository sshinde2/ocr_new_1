/**
 * 
 */
package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;


/**
 * Command for make payment void with payment gateway e.g. Gift card reversal
 *
 */
public interface TargetPaymentVoidCommand extends Command<TargetPaymentVoidRequest, TargetPaymentVoidResult> {
    //
}
