/**
 * 
 */
package au.com.target.tgtpayment.commands.result;

/**
 * @author gsing236
 *
 */
public class TargetPaymentPingResult {

    private boolean isSuccess;

    /**
     * @return the isSuccess
     */
    public boolean isSuccess() {
        return isSuccess;
    }

    /**
     * @param pSuccess
     *            the isSuccess to set
     */
    public void setSuccess(final boolean pSuccess) {
        this.isSuccess = pSuccess;
    }
}
