package au.com.target.tgtfraud.strategy.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.EnumSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test suite for {@link OrderStatusReviewableCheck}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderStatusReviewableCheckTest {

    private static final EnumSet<OrderStatus> VALID_STATUSES =
            EnumSet.of(OrderStatus.REVIEW, OrderStatus.REVIEW_ON_HOLD);

    @InjectMocks
    private OrderStatusReviewableCheck check = new OrderStatusReviewableCheck();

    @Mock
    private OrderModel order;


    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        check.setAptStatuses(VALID_STATUSES);
    }

    /**
     * Verifies that check will return {@code false} if order does not have status.
     */
    @Test
    public void testCheckOrderWithEmptyStatus() {
        assertFalse(check.perform(order));
    }

    /**
     * Verifies that check passes if an order has a valid status.
     */
    @Test
    public void testCheckOrderWithEligibleStatus() {
        for (OrderStatus status : VALID_STATUSES) {
            when(order.getStatus()).thenReturn(status);
            assertTrue(check.perform(order));
        }
    }

    /**
     * Verifies that check fails if an order has an invalid status.
     */
    @Test
    public void testCheckOrderWithIneligibleStatus() {
        for (OrderStatus status : EnumSet.complementOf(VALID_STATUSES)) {
            when(order.getStatus()).thenReturn(status);
            assertFalse(check.perform(order));
        }
    }
}
