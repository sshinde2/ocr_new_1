package au.com.target.tgtfraud.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfraud.model.FraudOrderDetailsModel;
import au.com.target.tgtfraud.order.ClientDetails;


/**
 * Unit test for {@link TargetFraudCommerceCheckoutServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFraudCommerceCheckoutServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    private final TargetFraudCommerceCheckoutServiceImpl fraudCommerceCheckoutServiceImpl = new TargetFraudCommerceCheckoutServiceImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private FraudOrderDetailsModel fraudOrderDetailsModel;

    @Before
    public void setUp() {
        BDDMockito.given(modelService.create(FraudOrderDetailsModel.class)).willReturn(fraudOrderDetailsModel);
    }

    @Test
    public void testPopulateClientDetailsForFraudDetectionWithNullCart() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("cartModel cannot be null");

        final ClientDetails clientDetails = Mockito.mock(ClientDetails.class);
        fraudCommerceCheckoutServiceImpl.populateClientDetailsForFraudDetection(null, clientDetails);
    }

    @Test
    public void testPopulateClientDetailsForFraudDetectionWithNullClientDetails() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("clientDetails cannot be null");

        final CartModel cartModel = Mockito.mock(CartModel.class);
        fraudCommerceCheckoutServiceImpl.populateClientDetailsForFraudDetection(cartModel, null);
    }

    @Test
    public void testPopulateClientDetailsForFraudDetection() {

        final CartModel cartModel = Mockito.mock(CartModel.class);
        final ClientDetails clientDetails = Mockito.mock(ClientDetails.class);
        BDDMockito.given(clientDetails.getBrowserCookies()).willReturn("cookies");
        BDDMockito.given(clientDetails.getIpAddress()).willReturn("127.0.0.1");

        fraudCommerceCheckoutServiceImpl.populateClientDetailsForFraudDetection(cartModel, clientDetails);

        Mockito.verify(fraudOrderDetailsModel).setBrowserCookies("cookies");
        Mockito.verify(fraudOrderDetailsModel).setCustomerIP("127.0.0.1");
        Mockito.verify(modelService).saveAll(fraudOrderDetailsModel, cartModel);
    }

}
