/**
 * 
 */
package au.com.target.tgtfraud.provider.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.voucher.VoucherService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtfraud.model.FraudOrderDetailsModel;
import au.com.target.tgtfraud.provider.data.BillingAddress;
import au.com.target.tgtfraud.provider.data.ChannelTypeEnum;
import au.com.target.tgtfraud.provider.data.CreditCardDetail;
import au.com.target.tgtfraud.provider.data.FailedPaymentInfo;
import au.com.target.tgtfraud.provider.data.Member;
import au.com.target.tgtfraud.provider.data.Order;
import au.com.target.tgtfraud.provider.data.OrderDetail;
import au.com.target.tgtfraud.provider.data.OrderTypeEnum;
import au.com.target.tgtfraud.provider.data.PayPalDetail;
import au.com.target.tgtfraud.provider.data.PaymentInfo;
import au.com.target.tgtfraud.provider.data.PaymentTypeEnum;
import au.com.target.tgtfraud.provider.data.ShippingInfo;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AccertifyUtilTest {

    @InjectMocks
    private final AccertifyUtil accertifyUtil = new AccertifyUtil();

    @Mock
    private AddressModel addressModel;
    @Mock
    private CreditCardPaymentInfoModel ccPaymentInfoModel;
    @Mock
    private PinPadPaymentInfoModel pinPadPaymentInfoModel;
    @Mock
    private PaypalPaymentInfoModel paypalPaymentInfoModel;
    @Mock
    private PaymentTransactionModel ccModel;
    @Mock
    private PaymentTransactionModel paypalModel;
    @Mock
    private PaymentTransactionModel pinPadModel;
    @Mock
    private PaymentTransactionModel ipgTxnModel;
    @Mock
    private PaymentTransactionEntryModel ccTxnEntry;
    @Mock
    private PaymentTransactionEntryModel refundEntry;
    @Mock
    private PaymentTransactionEntryModel paypalTxnEntry;
    @Mock
    private PaymentTransactionEntryModel pinPadTxnEntry;
    @Mock
    private PaymentTransactionEntryModel ipgTxnEntry;
    @Mock
    private PaymentTransactionEntryModel ipgFailedCCTxnEntry;
    @Mock
    private PaymentTransactionEntryModel ipgGCTxnEntry;
    @Mock
    private OrderEntryModel orderEntryModel;
    @Mock
    private TargetColourVariantProductModel productModel;
    @Mock
    private TargetMerchDepartmentModel merchDepartmentModel;
    @Mock
    private TargetDiscountService targetDiscountService;
    @Mock
    private VoucherService voucherService;
    @Mock
    private IpgCreditCardPaymentInfoModel ipgCreditCard;
    @Mock
    private IpgGiftCardPaymentInfoModel ipgGiftCard;
    @Mock
    private IpgPaymentInfoModel ipgPaymentInfoModel;
    @Mock
    private OrderModel orderModel;
    @Mock
    private OrderModificationRecordEntryModel orderModRecordEntryModel;
    @Mock
    private TargetFeatureSwitchService featureSwitchService;
    @Mock
    private TargetPaymentService paymentService;
    @Mock
    private TargetZoneDeliveryModeModel targetDeliveryModel;
    @Mock
    private StandardPaymentModeModel paymentMode;
    @Mock
    private PaymentTransactionEntryModel afterpayTxnEntry;
    @Mock
    private PaymentTransactionModel afterpayTxnModel;
    @Mock
    private AfterpayPaymentInfoModel afterpayPaymentInfoModel;
    @Mock
    private ZippayPaymentInfoModel zippayPaymentInfoModel;

    @Before
    @SuppressWarnings({ "deprecation" })
    public void setUp() {
        when(addressModel.getFirstname()).thenReturn("Jack");
        when(addressModel.getLastname()).thenReturn("Rabbit");
        when(addressModel.getLine1()).thenReturn("Delivery St");
        when(addressModel.getLine2()).thenReturn("");
        when(addressModel.getTown()).thenReturn("Geelong");
        when(addressModel.getDistrict()).thenReturn("VIC");
        when(addressModel.getCountry()).thenReturn(new CountryModel("AU"));
        when(addressModel.getCellphone()).thenReturn("048484848");

        when(ccPaymentInfoModel.getNumber()).thenReturn("5XXXXXXXXXXXXX123");
        when(ccPaymentInfoModel.getBillingAddress()).thenReturn(addressModel);
        when(ccPaymentInfoModel.getType()).thenReturn(CreditCardType.MASTER);
        when(ccPaymentInfoModel.getValidToMonth()).thenReturn("07");
        when(ccPaymentInfoModel.getValidToYear()).thenReturn("2017");
        when(ccPaymentInfoModel.getSubscriptionId()).thenReturn("abcd-def-ghi");

        when(pinPadPaymentInfoModel.getBillingAddress()).thenReturn(addressModel);
        when(pinPadPaymentInfoModel.getMaskedCardNumber()).thenReturn("5XXXXXXXXXXXXXX999");
        when(pinPadPaymentInfoModel.getBillingAddress()).thenReturn(addressModel);
        when(pinPadPaymentInfoModel.getCardType()).thenReturn(CreditCardType.VISA);

        when(paypalPaymentInfoModel.getPayerId()).thenReturn("Jack@rabbit.com");
        when(paypalPaymentInfoModel.getBillingAddress()).thenReturn(addressModel);

        when(ccModel.getInfo()).thenReturn(ccPaymentInfoModel);
        when(ccModel.getEntries()).thenReturn(new LinkedList() {
            {
                add(refundEntry);
                add(ccTxnEntry);
            }
        });

        when(paypalModel.getInfo()).thenReturn(paypalPaymentInfoModel);
        when(paypalModel.getEntries()).thenReturn(new LinkedList() {
            {
                add(paypalTxnEntry);
            }
        });

        when(pinPadModel.getInfo()).thenReturn(pinPadPaymentInfoModel);
        when(pinPadModel.getEntries()).thenReturn(new LinkedList() {
            {
                add(pinPadTxnEntry);
            }
        });

        when(ipgCreditCard.getNumber()).thenReturn("4242******4242");
        when(ipgCreditCard.getType()).thenReturn(CreditCardType.VISA);
        when(ipgCreditCard.getValidToMonth()).thenReturn("09");
        when(ipgCreditCard.getValidToYear()).thenReturn("2017");
        when(ipgCreditCard.getToken()).thenReturn("98695478556566");
        when(ipgGiftCard.getMaskedNumber()).thenReturn("658421******4242");
        when(ipgGiftCard.getBin()).thenReturn("658421");

        when(ipgTxnEntry.getIpgPaymentInfo()).thenReturn(ipgCreditCard);
        when(ipgGCTxnEntry.getIpgPaymentInfo()).thenReturn(ipgGiftCard);
        when(ipgTxnModel.getInfo()).thenReturn(ipgPaymentInfoModel);
        when(ipgTxnModel.getIsCaptureSuccessful()).thenReturn(Boolean.TRUE);
        when(ipgPaymentInfoModel.getBillingAddress()).thenReturn(addressModel);

        when(ipgTxnModel.getEntries()).thenReturn(new LinkedList() {
            {
                add(ipgFailedCCTxnEntry);
                add(ipgGCTxnEntry);
                add(ipgTxnEntry);
            }
        });

        setTransactionEntryValues(ccTxnEntry, "cc-1", PaymentTransactionType.CAPTURE, ccModel, new BigDecimal("20.5"),
                "ACCEPTED");
        setTransactionEntryValues(refundEntry, "refund-1", PaymentTransactionType.REFUND_FOLLOW_ON, ccModel,
                new BigDecimal("20.5"), "ACCEPTED");
        setTransactionEntryValues(pinPadTxnEntry, "pinpad-1", PaymentTransactionType.CAPTURE, pinPadModel,
                new BigDecimal("20.002"), "ACCEPTED");
        setTransactionEntryValues(paypalTxnEntry, "paypal-1", PaymentTransactionType.CAPTURE, paypalModel,
                new BigDecimal("19.999"), "ACCEPTED");
        setTransactionEntryValues(ipgTxnEntry, "ipg-1", PaymentTransactionType.CAPTURE, ipgTxnModel, new BigDecimal(
                "50.05"), "ACCEPTED");
        setTransactionEntryValues(ipgFailedCCTxnEntry, "ipg-0", PaymentTransactionType.CAPTURE, ipgTxnModel,
                new BigDecimal(
                        "50.05"),
                "REJECTED");
        setTransactionEntryValues(ipgGCTxnEntry, "ipg-2", PaymentTransactionType.CAPTURE, ipgTxnModel, new BigDecimal(
                "19"), "ACCEPTED");

        final BrandModel brandModel = mock(BrandModel.class);
        final TargetProductCategoryModel categoryModel = mock(TargetProductCategoryModel.class);

        when(orderEntryModel.getBasePrice()).thenReturn(Double.valueOf(10d));
        when(orderEntryModel.getQuantity()).thenReturn(Long.valueOf(2l));
        when(orderEntryModel.getProduct()).thenReturn(productModel);
        when(productModel.getBrand()).thenReturn(brandModel);
        when(orderEntryModel.getProduct().getName()).thenReturn("Fabulous product");
        when(orderEntryModel.getProduct().getCode()).thenReturn("12345");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ccModel));
        when(orderModel.getDeliveryMode()).thenReturn(targetDeliveryModel);
        when(productModel.getMerchDepartment()).thenReturn(merchDepartmentModel);
        when(productModel.getBrand()).thenReturn(brandModel);
        when(productModel.getPrimarySuperCategory()).thenReturn(categoryModel);
        when(categoryModel.getName()).thenReturn("Kids");
        when(merchDepartmentModel.getCode()).thenReturn("111");
        when(brandModel.getName()).thenReturn("Target");
        willReturn(Boolean.FALSE).given(featureSwitchService).isFeatureEnabled("acceritfy.newformat");

        setTransactionEntryValues(afterpayTxnEntry, "afterpay", PaymentTransactionType.CAPTURE, afterpayTxnModel,
                new BigDecimal(
                        "19"),
                "ACCEPTED");
        given(afterpayTxnModel.getInfo()).willReturn(afterpayPaymentInfoModel);
        given(afterpayTxnModel.getEntries()).willReturn(new LinkedList() {
            {
                add(afterpayTxnEntry);
            }
        });
        given(afterpayPaymentInfoModel.getAfterpayOrderId()).willReturn("123456");

    }

    /**
     * @param type
     * @param status
     *            YTODO
     * 
     */
    private void setTransactionEntryValues(final PaymentTransactionEntryModel txEntry, final String code,
            final PaymentTransactionType type, final PaymentTransactionModel txModel, final BigDecimal amount,
            final String status) {
        when(txEntry.getRequestId()).thenReturn(code);
        when(txEntry.getAmount()).thenReturn(amount);
        when(txEntry.getType()).thenReturn(type);
        when(txEntry.getTransactionStatus()).thenReturn(status);
        when(txEntry.getPaymentTransaction()).thenReturn(txModel);
        when(txEntry.getReceiptNo()).thenReturn(code);
        when(txEntry.getCode()).thenReturn(code);
        when(txEntry.getTime()).thenReturn(new Date());
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getBasicOrderInformation(de.hybris.platform.core.model.order.AbstractOrderModel, au.com.target.tgtfraud.provider.data.OrderTypeEnum)}
     * .
     */
    @SuppressWarnings("boxing")
    @Test
    public void testGetBasicOrderInformation() {
        final AccertifyUtil mockAccertifyUtil = Mockito.spy(accertifyUtil);

        final Date date = new Date();
        when(orderModel.getCode()).thenReturn("12345");
        when(orderModel.getCreationtime()).thenReturn(date);
        when(orderModel.getTotalPrice()).thenReturn(Double.valueOf("30.00"));
        when(orderModel.getDeliveryCost()).thenReturn(Double.valueOf("9.00"));
        when(orderModel.getTotalTax()).thenReturn(Double.valueOf("2.80"));
        when(orderModel.getSalesApplication()).thenReturn(SalesApplication.WEB);
        when(orderModel.getThreatMatrixSessionID()).thenReturn("5a7d1d63ab903d74cf8ec0f3a9f666ac");
        when(targetDiscountService.getTotalDiscount(orderModel)).thenReturn(Double.valueOf("1.50"));

        final FraudOrderDetailsModel fraudOrderDetails = Mockito.mock(FraudOrderDetailsModel.class);
        when(fraudOrderDetails.getBrowserCookies()).thenReturn("i'm a cookie :P");
        when(fraudOrderDetails.getCustomerIP()).thenReturn("127.0.0.1");
        when(orderModel.getFraudOrderDetails()).thenReturn(fraudOrderDetails);

        // Given null voucher code
        Mockito.doReturn(null).when(mockAccertifyUtil).getVoucherCode(orderModel);
        Order order = mockAccertifyUtil.getBasicOrderInformation(orderModel, OrderTypeEnum.SALE);
        checkOrderBasicDetails(order, date);
        assertTrue("Order promotion code", StringUtils.isEmpty(order.getPromotionCode()));

        // Given populated voucher code
        Mockito.doReturn("ABC").when(mockAccertifyUtil).getVoucherCode(orderModel);
        order = mockAccertifyUtil.getBasicOrderInformation(orderModel, OrderTypeEnum.SALE);
        checkOrderBasicDetails(order, date);
        assertEquals("Order promotion code", "ABC", order.getPromotionCode());

    }

    private void checkOrderBasicDetails(final Order order, final Date date) {
        assertNotNull(order);
        assertEquals("Order code", "12345", order.getOrderNumber());
        assertEquals("Order creation time", date, order.getOrderDateTime());
        assertEquals("Order Total", BigDecimal.valueOf(30.00d), order.getTotalAmount());
        assertEquals("Order delivery cost", BigDecimal.valueOf(9.00d), order.getTotalShippingCharges());
        assertEquals("Order total tax", BigDecimal.valueOf(2.80d), order.getTotalSalesTax());
        assertEquals("Order sales app", ChannelTypeEnum.WEB, order.getSalesChannel());
        assertEquals("Order total discounts", BigDecimal.valueOf(1.50d), order.getPromotionAmount());
        assertEquals("Order type", OrderTypeEnum.SALE, order.getOrderType());
        assertEquals("Order cookie", "i'm a cookie :P", order.getBrowserCookie());
        assertEquals("Order IP Address", "127.0.0.1", order.getIpAddress());
        assertEquals("Order Threat Metrix SessionID", "5a7d1d63ab903d74cf8ec0f3a9f666ac",
                order.getThreatMetrixSessionID());
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getChannelTypeFromSalesApplication(de.hybris.platform.commerceservices.enums.SalesApplication)}
     * .
     */
    @Test
    public void testGetChannelTypeFromSalesApplication() {
        assertEquals("Sales application ", ChannelTypeEnum.WEB,
                accertifyUtil.getChannelTypeFromSalesApplication(SalesApplication.WEB));
        assertEquals("Sales application ", ChannelTypeEnum.WEB,
                accertifyUtil.getChannelTypeFromSalesApplication(SalesApplication.WEBMOBILE));
        assertEquals("Sales application ", ChannelTypeEnum.PHONE,
                accertifyUtil.getChannelTypeFromSalesApplication(SalesApplication.CALLCENTER));
        assertEquals("Sales application ", ChannelTypeEnum.B2B,
                accertifyUtil.getChannelTypeFromSalesApplication(SalesApplication.B2B));
        assertEquals("Sales application ", ChannelTypeEnum.AUTOMATION,
                accertifyUtil.getChannelTypeFromSalesApplication(null));
    }

    @Test
    public void testGetPaymentInfoWithNoAddress() {
        final PaymentInfo paymentInfo = accertifyUtil.getPaymentInfo(ccModel.getEntries(),
                ccPaymentInfoModel).get(0);

        assertEquals("Billing lastname", null, paymentInfo.getBillingLastName());
        assertEquals("Billing country", null, paymentInfo.getBillingCountry());
    }

    @Test
    public void testGetPaymentInfoNewFormat() {
        final PaymentInfo paymentInfo = accertifyUtil.getPaymentInfo(ImmutableList.of(ccTxnEntry),
                ccPaymentInfoModel).get(0);

        assertNotNull(paymentInfo);
        assertEquals("Payment type", PaymentTypeEnum.CREDITCARD, paymentInfo.getPaymentType());
        assertEquals("Card type", "master", paymentInfo.getCardType());
        assertEquals("Card expiry date", "2017/07", paymentInfo.getCardExpireDate());
        assertEquals("Billing lastname", null, paymentInfo.getBillingLastName());
        assertEquals("Billing country", null, paymentInfo.getBillingCountry());
        assertEquals("Receipt no.", "cc-1", paymentInfo.getCardRRN());
        assertEquals("Request token.", "abcd-def-ghi", paymentInfo.getCardTokenID());
        assertEquals("Txn no.", "cc-1", paymentInfo.getCardTransactionID());
    }

    @Test
    public void testGetPaymentInfoAfterpay() {
        final PaymentInfo paymentInfo = accertifyUtil.getPaymentInfo(ImmutableList.of(afterpayTxnEntry),
                afterpayPaymentInfoModel).get(0);

        assertThat(paymentInfo).isNotNull();
        assertThat(paymentInfo.getPaymentType()).isEqualTo(PaymentTypeEnum.AFTERPAY);
        assertThat(paymentInfo.getAfterPayOrderId()).isEqualTo("123456");
        assertThat(paymentInfo.getAfterPayAmount()).isEqualTo(new BigDecimal("19"));
    }

    @Test
    public void testGetFailedPaymentInfoAfterpay() {
        given(afterpayTxnEntry.getTransactionStatus()).willReturn("REJECTED");
        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(
                afterpayTxnModel));
        given(paymentMode.getCode()).willReturn("Afterpay");
        given(orderModel.getPaymentMode()).willReturn(paymentMode);
        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        assertThat(failedPaymentInfo).isNotNull();
        assertThat(failedPaymentInfo.getTotalFailedAPAttempts()).isEqualTo(1);
    }

    @Test
    public void testGetPaymentInfoWithPinPadNewFormat() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(pinPadModel));
        final PaymentInfo paymentInfo = accertifyUtil.getPaymentInfo(pinPadModel.getEntries(),
                pinPadPaymentInfoModel).get(0);

        assertNotNull(paymentInfo);
        assertEquals("Payment type", PaymentTypeEnum.PINPAD, paymentInfo.getPaymentType());
        assertEquals("Billing lastname", null, paymentInfo.getBillingLastName());
        assertEquals("Billing country", null, paymentInfo.getBillingCountry());
        assertEquals("Txn no.", "pinpad-1", paymentInfo.getCardTransactionID());
    }

    @Test
    public void testGetPaymentInfoWithIPGCreditCard() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ipgTxnModel));
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfoModel);
        final PaymentInfo paymentInfo = accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.SALE, null)
                .getOrders().get(0).getPaymentInformation().get(1);
        assertNotNull(paymentInfo);
        assertEquals("Payment type", PaymentTypeEnum.CREDITCARD, paymentInfo.getPaymentType());
        assertEquals("4242******4242", paymentInfo.getCardNumber());
        assertEquals("ipg-1", paymentInfo.getCardRRN());
        assertEquals("visa", paymentInfo.getCardType());
        assertEquals("ipg-1", paymentInfo.getCardTransactionID());
        assertEquals("98695478556566", paymentInfo.getCardTokenID());

    }

    @Test
    public void testGetFailedPaymentInfo() {
        when(ccTxnEntry.getTransactionStatus()).thenReturn("REVIEW");
        when(paypalTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(ipgTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(pinPadTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(
                ccModel,
                paypalModel, pinPadModel, ipgTxnModel));
        when(orderModel.getPaymentMode()).thenReturn(paymentMode);
        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        verify(paymentService).retrieveFailedGiftcardPayment(orderModel);
        assertNotNull(failedPaymentInfo);
        assertEquals("Failed CC attempts", 3, failedPaymentInfo.getTotalFailedCCAttempts());
        assertEquals("Failed Paypal attempts", 1, failedPaymentInfo.getTotalFailedPPAttempts());
    }

    @Test
    public void testGetFailedPaymentInfoWithNullPaymentInfoCC() {
        when(ccTxnEntry.getTransactionStatus()).thenReturn("REVIEW");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(
                ccModel));
        when(ccModel.getInfo()).thenReturn(null);
        when(orderModel.getPaymentMode()).thenReturn(paymentMode);
        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        Mockito.verify(paymentService, Mockito.times(1)).retrieveFailedGiftcardPayment(orderModel);
        assertEquals(failedPaymentInfo.getFailCCdetails().size(), 0);
    }

    @Test
    public void testGetFailedPaymentInfoWithNullPaymentInfoPaypalAndIPG() {
        when(paypalTxnEntry.getTransactionStatus()).thenReturn("REVIEW");
        when(ipgTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(
                paypalModel, ipgTxnModel));
        when(ipgTxnModel.getInfo()).thenReturn(null);
        when(orderModel.getPaymentMode()).thenReturn(paymentMode);
        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        Mockito.verify(paymentService, Mockito.times(1)).retrieveFailedGiftcardPayment(orderModel);
        assertEquals(failedPaymentInfo.getFailCCdetails().size(), 0);
        assertEquals("Failed Paypal attempts", 1, failedPaymentInfo.getTotalFailedPPAttempts());
    }


    @Test
    public void testGetFailedPaymentInfoWithPayPalSkipGC() {
        when(paypalTxnEntry.getTransactionStatus()).thenReturn("REVIEW");
        when(ipgTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(
                paypalModel, ipgTxnModel));
        when(ipgTxnModel.getInfo()).thenReturn(null);
        when(orderModel.getPaymentMode()).thenReturn(paymentMode);
        when(paymentMode.getCode()).thenReturn(PaymentTypeEnum.PAYPAL.toString());
        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        Mockito.verify(paymentService, Mockito.times(0)).retrieveFailedGiftcardPayment(orderModel);
        assertEquals(failedPaymentInfo.getFailCCdetails().size(), 0);
        assertEquals("Failed Paypal attempts", 1, failedPaymentInfo.getTotalFailedPPAttempts());
    }

    @Test
    public void testGetFailedPaymentInfoWithFailedGC() {
        final PaymentTransactionEntryData failedGCData = mock(PaymentTransactionEntryData.class);
        when(ccTxnEntry.getTransactionStatus()).thenReturn("REVIEW");
        when(paypalTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(ipgTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(pinPadTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(
                ccModel,
                paypalModel, pinPadModel, ipgTxnModel));
        when(orderModel.getPaymentMode()).thenReturn(paymentMode);
        when(paymentService.retrieveFailedGiftcardPayment(orderModel)).thenReturn(ImmutableList.of(failedGCData));
        when(failedGCData.getAmount()).thenReturn(new BigDecimal("5.061").setScale(2, BigDecimal.ROUND_HALF_DOWN));

        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        verify(paymentService).retrieveFailedGiftcardPayment(orderModel);
        assertNotNull(failedPaymentInfo);
        assertEquals("Failed CC attempts", 3, failedPaymentInfo.getTotalFailedCCAttempts());
        assertEquals("Failed Paypal attempts", 1, failedPaymentInfo.getTotalFailedPPAttempts());
        assertEquals("Failed gc attempts", 1, failedPaymentInfo.getTotalFailedGCAttempts());
        assertEquals("5.06", failedPaymentInfo.getFailGCdetails().get(0).getFailedAmount());
    }

    @Test
    public void testGetFailedPaymentInfoWithException() {
        final PaymentTransactionEntryData failedGCData = mock(PaymentTransactionEntryData.class);
        when(ccTxnEntry.getTransactionStatus()).thenReturn("REVIEW");
        when(paypalTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(ipgTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(pinPadTxnEntry.getTransactionStatus()).thenReturn("REJECTED");
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(
                ccModel,
                paypalModel, pinPadModel, ipgTxnModel));
        when(orderModel.getPaymentMode()).thenReturn(paymentMode);
        when(paymentService.retrieveFailedGiftcardPayment(orderModel)).thenThrow(new RuntimeException("test"));
        when(failedGCData.getAmount()).thenReturn(new BigDecimal("5.061").setScale(2, BigDecimal.ROUND_HALF_DOWN));

        final FailedPaymentInfo failedPaymentInfo = accertifyUtil.getFailedPaymentInfo(orderModel);
        verify(paymentService).retrieveFailedGiftcardPayment(orderModel);
        assertNotNull(failedPaymentInfo);
        assertEquals("Failed CC attempts", 3, failedPaymentInfo.getTotalFailedCCAttempts());
        assertEquals("Failed Paypal attempts", 1, failedPaymentInfo.getTotalFailedPPAttempts());
        assertEquals("Failed gc attempts", 0, failedPaymentInfo.getTotalFailedGCAttempts());
        assertNull(failedPaymentInfo.getFailGCdetails());
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#populateCreditCardOrPinPadDetails(de.hybris.platform.core.model.order.payment.PaymentInfoModel, AddressModel)}
     * .
     */
    @SuppressWarnings("javadoc")
    @Test
    public void testPopulateCreditCardPinPadDetailsWithCC() {
        doReturn(Boolean.TRUE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(ccPaymentInfoModel,
                addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXX123", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "master", cardDetail.getFailedcardType());
        assertEquals("Card Expiry date", "07/2017", cardDetail.getFailedcardExpireDate());
        verifyAddressDetails(addressModel, cardDetail);
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#populateCreditCardOrPinPadDetails(de.hybris.platform.core.model.order.payment.PaymentInfoModel, AddressModel)}
     * .
     */
    @SuppressWarnings("javadoc")
    @Test
    public void testPopulateCreditCardPinPadDetailsWithPinPad() {
        doReturn(Boolean.TRUE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(pinPadPaymentInfoModel,
                addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXXX999", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        verifyAddressDetails(addressModel, cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithNoCardDetail() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(null,
                addressModel, orderModel);
        assertNull(cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithNoBillingAddress() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(pinPadPaymentInfoModel,
                null, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXXX999", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        verifyAddressDetails(new AddressModel(), cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithPinPadExcludeBillingAddress() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(pinPadPaymentInfoModel,
                addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXXX999", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        verifyAddressDetails(new AddressModel(), cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithPinPadForCncOrder() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        when(targetDeliveryModel.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(pinPadPaymentInfoModel,
                addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXXX999", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        verifyAddressDetails(addressModel, cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithEGiftcardOrder() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        when(targetDeliveryModel.getIsDigital()).thenReturn(Boolean.TRUE);
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(pinPadPaymentInfoModel,
                addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXXX999", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        verifyAddressDetails(addressModel, cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithNormalOrder() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryMode);
        final CreditCardDetail cardDetail = accertifyUtil.populateCreditCardOrPinPadDetails(pinPadPaymentInfoModel,
                addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "5XXXXXXXXXXXXXX999", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        verifyAddressDetails(new AddressModel(), cardDetail);
    }

    @Test
    public void testPopulateCreditCardPinPadDetailsWithIPGCC() {
        doReturn(Boolean.TRUE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        final CreditCardDetail cardDetail = accertifyUtil
                .populateCreditCardOrPinPadDetails(ipgCreditCard, addressModel, orderModel);
        assertNotNull(cardDetail);
        assertEquals("Card number", "4242******4242", cardDetail.getFailedcardNumber());
        assertEquals("Card Type", "visa", cardDetail.getFailedcardType());
        assertEquals("Card Expiry date", "09/2017", cardDetail.getFailedcardExpireDate());
        verifyAddressDetails(addressModel, cardDetail);
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getPayPalDetailFromPaymentInfo(au.com.target.tgtpayment.model.PaypalPaymentInfoModel, de.hybris.platform.payment.model.PaymentTransactionEntryModel)}
     * .
     */
    @Test
    public void testGetPayPalDetailFromPaymentInfo() {
        final PayPalDetail paypalDetail = accertifyUtil.getPayPalDetailFromPaymentInfo(paypalPaymentInfoModel,
                paypalTxnEntry);

        assertNotNull(paypalDetail);
        assertEquals("Payer ID", "Jack@rabbit.com", paypalDetail.getFailedpayPalEmail());
        assertEquals("Request ID", "paypal-1", paypalDetail.getFailedpayPalRequestID());
        assertEquals("Amount", "20.00", paypalDetail.getFailedpayPalAmount());
        assertEquals("Status", "ACCEPTED", paypalDetail.getFailedpayPalStatus());
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getMemberInfo(au.com.target.tgtcore.model.TargetCustomerModel, java.lang.String, java.lang.String)}
     * .
     */
    @Test
    public void testGetMemberInfo() {
        final Date date = new Date();
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        when(customer.getCustomerID()).thenReturn("12345");
        when(customer.getCreationtime()).thenReturn(date);
        when(customer.getDisplayName()).thenReturn("Jack Sparrow");
        when(customer.getFirstname()).thenReturn("Jack");
        when(customer.getLastname()).thenReturn("Sparrow");
        when(customer.getContactEmail()).thenReturn("jsparrow@target.com.au");
        when(customer.getMobileNumber()).thenReturn("048484848");
        when(customer.getType()).thenReturn(CustomerType.GUEST);

        final Member member = accertifyUtil.getMemberInfo(customer, "21212121", "31313131");

        assertNotNull(member);
        assertEquals("ID", "12345", member.getMemberID());
        assertEquals("Full name", "Jack Sparrow", member.getMemberFullName());
        assertEquals("First name", "Jack", member.getMemberFirstName());
        assertEquals("Last name", "Sparrow", member.getMemberLastName());
        assertEquals("Creation date", date, member.getMembershipDate());
        assertEquals("Email", "jsparrow@target.com.au", member.getMemberEmail());
        assertEquals("Mobile", "048484848", member.getMemberPhone());
        assertEquals("Type", "Guest", member.getInternetMemberRegisteredStatus());
        assertEquals("TMD", "21212121", member.getTargetStaffDiscountCard());
        assertEquals("Flybuys", "31313131", member.getLoyaltyNumber());
    }

    /**
     * Test method for {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getOrderDetails(java.util.List)}.
     */
    @Test
    public void testGetOrderDetails() {
        final List orderEntries = new LinkedList();
        orderEntries.add(orderEntryModel);

        final OrderDetail detail = accertifyUtil.getOrderDetails(orderEntries);

        assertNotNull(detail);
        assertEquals("Number of entries", 1, detail.getOrderItem().size());
        assertEquals("Item code", "12345", detail.getOrderItem().get(0).getItemNumber());
        assertEquals("Item desc", "Fabulous product", detail.getOrderItem().get(0).getItemDescription());
        assertEquals("Unit price", BigDecimal.valueOf(10.00d), detail.getOrderItem().get(0).getUnitPrice());
        assertEquals("Quantity", 2l, detail.getOrderItem().get(0).getQuantity());
        assertEquals("Brand", "Target", detail.getOrderItem().get(0).getBrandname());
        assertEquals("Category", "Kids", detail.getOrderItem().get(0).getCategory());
        assertEquals("Department number", 111, detail.getOrderItem().get(0).getDepartmentNumber());

    }

    @Test
    public void testGetOrderDetailsWithNonNumericDepartment() {
        when(merchDepartmentModel.getCode()).thenReturn("kids");

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntryModel);
        final OrderDetail detail = accertifyUtil.getOrderDetails(orderEntries);

        assertNotNull(detail);
        assertEquals("Department number", 0, detail.getOrderItem().get(0).getDepartmentNumber());
    }

    @Test
    public void testGetOrderDetailsReadFromDeptTypeAttr() {
        when(merchDepartmentModel.getCode()).thenReturn("111");
        when(productModel.getMerchDepartment()).thenReturn(merchDepartmentModel);

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntryModel);
        final OrderDetail detail = accertifyUtil.getOrderDetails(orderEntries);

        assertNotNull(detail);
        assertEquals("Department number", 111, detail.getOrderItem().get(0).getDepartmentNumber());

        Mockito.verify(productModel, Mockito.atLeastOnce()).getMerchDepartment();
        Mockito.verify(productModel, Mockito.never()).getDepartment();
    }

    @Test
    public void testGetOrderDetailsReadFromDeptPrimitiveAttr() {
        when(productModel.getMerchDepartment()).thenReturn(null);
        when(productModel.getDepartment()).thenReturn(Integer.valueOf(111));

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntryModel);
        final OrderDetail detail = accertifyUtil.getOrderDetails(orderEntries);

        assertNotNull(detail);
        assertEquals("Department number", 111, detail.getOrderItem().get(0).getDepartmentNumber());

        Mockito.verify(productModel, Mockito.atLeastOnce()).getMerchDepartment();
        Mockito.verify(productModel, Mockito.atLeastOnce()).getDepartment();
    }

    @Test
    public void testGetOrderDetailsNullDept() {
        when(productModel.getMerchDepartment()).thenReturn(null);
        when(productModel.getDepartment()).thenReturn(null);

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntryModel);
        final OrderDetail detail = accertifyUtil.getOrderDetails(orderEntries);

        assertNotNull(detail);
        assertEquals("Department number", 0, detail.getOrderItem().get(0).getDepartmentNumber());

        Mockito.verify(productModel, Mockito.atLeastOnce()).getMerchDepartment();
        Mockito.verify(productModel, Mockito.atLeastOnce()).getDepartment();
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getOrderDetailsFromCancelReturnRecord(java.util.Collection)}
     * .
     */
    @Test
    public void testGetOrderDetailsFromCancelReturnRecord() {
        final OrderEntryCancelRecordEntryModel oecrem = mock(OrderEntryCancelRecordEntryModel.class);
        when(oecrem.getOrderEntry()).thenReturn(orderEntryModel);
        when(oecrem.getCancelledQuantity()).thenReturn(Integer.valueOf(2));

        //Have to do this, since ImmutableList.of() doesn't work here :(
        final List entries = new LinkedList() {
            {
                add(oecrem);
            }
        };
        final OrderDetail detail = accertifyUtil.getOrderDetailsFromCancelReturnRecord(entries);

        assertNotNull(detail);
        assertEquals("Number of entries", 1, detail.getOrderItem().size());
        assertEquals("Item code", "12345", detail.getOrderItem().get(0).getItemNumber());
        assertEquals("Item desc", "Fabulous product", detail.getOrderItem().get(0).getItemDescription());
        assertEquals("Unit price", BigDecimal.valueOf(10.00d), detail.getOrderItem().get(0).getUnitPrice());
        assertEquals("Quantity", 2l, detail.getOrderItem().get(0).getQuantity());
        assertEquals("Brand", "Target", detail.getOrderItem().get(0).getBrandname());
        assertEquals("Category", "Kids", detail.getOrderItem().get(0).getCategory());
        assertEquals("Department number", 111, detail.getOrderItem().get(0).getDepartmentNumber());
        assertEquals("Quantity", 2, detail.getOrderItem().get(0).getQuantity());
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfraud.provider.util.AccertifyUtil#getShippingInfo(de.hybris.platform.core.model.user.AddressModel, de.hybris.platform.core.model.order.delivery.DeliveryModeModel)}
     * .
     */
    @Test
    public void testGetShippingInfo() {
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        when(deliveryMode.getName()).thenReturn("AUS POST");

        final ShippingInfo shippingInfo = accertifyUtil.getShippingInfo(addressModel, deliveryMode);

        assertNotNull(shippingInfo);
        assertEquals("Full name ", "Jack Rabbit", shippingInfo.getShippingFullName());
        assertEquals("Line 1 ", "Delivery St", shippingInfo.getShippingAddressLine1());
        assertEquals("Line 2 ", "", shippingInfo.getShippingAddressLine2());
        assertEquals("City ", "Geelong", shippingInfo.getShippingCity());
        assertEquals("State ", "VIC", shippingInfo.getShippingRegion());
        assertEquals("Country ", "AU", shippingInfo.getShippingCountry());
        assertEquals("Cell phone ", "048484848", shippingInfo.getShippingPhone());
        assertEquals("Delivery mode ", "AUS POST", shippingInfo.getShippingMethod());
    }

    @Test
    public void testGetItemTax() {
        // 2 at $10 * 10% should be $2 tax
        final TaxValue tv1 = new TaxValue("aus-gst-1000", 10, false, 2, "AUD");
        when(orderEntryModel.getTaxValues()).thenReturn(Collections.singletonList(tv1));

        final BigDecimal totalTax = accertifyUtil.getItemTax(orderEntryModel);

        assertNotNull(totalTax);
        assertEquals(Double.valueOf(2), Double.valueOf(totalTax.doubleValue()));
    }

    @Test
    public void testGetItemTaxNullTaxValues() {
        final BigDecimal totalTax = accertifyUtil.getItemTax(orderEntryModel);

        assertNotNull(totalTax);
        assertEquals(Double.valueOf(0), Double.valueOf(totalTax.doubleValue()));
    }

    @Test
    public void testGetVoucherCodeNoVoucherDiscounts() {
        // Given a null return list - expect no code
        when(voucherService.getAppliedVoucherCodes(orderModel)).thenReturn(null);
        String voucherCode = accertifyUtil.getVoucherCode(orderModel);
        assertNull(voucherCode);

        // Given an empty return list - expect no code
        when(voucherService.getAppliedVoucherCodes(orderModel)).thenReturn(Collections.EMPTY_LIST);
        voucherCode = accertifyUtil.getVoucherCode(orderModel);
        assertNull(voucherCode);

    }

    @Test
    public void testGetVoucherCodeGivenOneVoucherNullCode() {
        // Given one voucher
        when(voucherService.getAppliedVoucherCodes(orderModel)).thenReturn(Collections.singletonList((String)null));
        final String voucherCode = accertifyUtil.getVoucherCode(orderModel);
        assertNull(voucherCode);
    }

    @Test
    public void testGetVoucherCodeGivenOneVoucher() {
        // Given one voucher
        when(voucherService.getAppliedVoucherCodes(orderModel)).thenReturn(Collections.singletonList("1234A"));
        final String voucherCode = accertifyUtil.getVoucherCode(orderModel);
        assertEquals("1234A", voucherCode);
    }

    @Test
    public void testGetVoucherCodeGivenTwoVouchers() {
        // Given two voucher
        final Collection<String> coll = new ArrayList<>();
        coll.add("1234A");
        coll.add("1234B");
        when(voucherService.getAppliedVoucherCodes(orderModel)).thenReturn(coll);
        final String voucherCode = accertifyUtil.getVoucherCode(orderModel);
        assertEquals("1234A", voucherCode);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildTransactionForRefundWithoutRefundShipFee() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ccModel));
        when(orderModel.getPaymentInfo()).thenReturn(ccPaymentInfoModel);
        when(orderModRecordEntryModel.getRefundedShippingAmount()).thenReturn(null);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, orderModRecordEntryModel,
                OrderTypeEnum.REFUND, ImmutableList.of(refundEntry));
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.REFUND, order.getOrderType());
        assertEquals(1, order.getPaymentInformation().size());
        assertEquals("refund-1", order.getPaymentInformation().get(0).getCardRRN());
        assertEquals(BigDecimal.valueOf(0d), order.getTotalShippingCharges());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildTransactionForRefund() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ccModel));
        when(orderModel.getPaymentInfo()).thenReturn(ccPaymentInfoModel);
        when(orderModRecordEntryModel.getRefundedShippingAmount()).thenReturn(5d);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, orderModRecordEntryModel,
                OrderTypeEnum.REFUND, ImmutableList.of(refundEntry));
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.REFUND, order.getOrderType());
        assertEquals(1, order.getPaymentInformation().size());
        assertEquals("refund-1", order.getPaymentInformation().get(0).getCardRRN());
        assertEquals(BigDecimal.valueOf(5d), order.getTotalShippingCharges());
        assertNull(order.getBillingAddress());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildTransactionForRefundPaypal() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(paypalModel));
        when(orderModel.getPaymentInfo()).thenReturn(paypalPaymentInfoModel);
        when(orderModRecordEntryModel.getRefundedShippingAmount()).thenReturn(5d);
        when(orderModRecordEntryModel.getRefundedAmount()).thenReturn(20d);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, orderModRecordEntryModel,
                OrderTypeEnum.REFUND, ImmutableList.of(paypalTxnEntry));
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.REFUND, order.getOrderType());
        assertEquals(1, order.getPaymentInformation().size());
        assertEquals("paypal-1", order.getPaymentInformation().get(0).getPayPalRRN());
        assertEquals(BigDecimal.valueOf(5d), order.getTotalShippingCharges());
        assertEquals(new BigDecimal("20.00"), order.getTotalAmount());
        assertNull(order.getBillingAddress());
    }

    @Test
    public void testBuildTransactionForRefundWithNewFormat() {
        doReturn(Boolean.TRUE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ccModel));
        when(orderModel.getPaymentInfo()).thenReturn(ccPaymentInfoModel);
        when(orderModRecordEntryModel.getRefundedShippingAmount()).thenReturn(Double.valueOf(5d));
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, orderModRecordEntryModel,
                OrderTypeEnum.REFUND, ImmutableList.of(refundEntry));
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.REFUND, order.getOrderType());
        assertEquals(1, order.getPaymentInformation().size());
        assertEquals("refund-1", order.getPaymentInformation().get(0).getCardRRN());
        assertEquals(BigDecimal.valueOf(5d), order.getTotalShippingCharges());
        assertNotNull(order.getBillingAddress());
    }

    @Test
    public void testBuildTransactionForCCSale() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ccModel));
        when(orderModel.getPaymentInfo()).thenReturn(ccPaymentInfoModel);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, null,
                OrderTypeEnum.SALE, null);
        verify(orderModel).getEntries();
        assertEquals(1, transaction.getOrders().size());
        assertEquals(OrderTypeEnum.SALE, transaction.getOrders().get(0).getOrderType());
        assertEquals(1, transaction.getOrders().get(0).getPaymentInformation().size());
        assertEquals("cc-1", transaction.getOrders().get(0).getPaymentInformation().get(0).getCardRRN());
    }

    @Test
    public void testBuildTransactionForKioskSale() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ccModel));
        when(orderModel.getPaymentInfo()).thenReturn(ccPaymentInfoModel);
        when(orderModel.getSalesApplication()).thenReturn(SalesApplication.KIOSK);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, null,
                OrderTypeEnum.SALE, null);
        verify(orderModel).getEntries();
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.SALE, order.getOrderType());
        assertEquals(1, order.getPaymentInformation().size());
        assertEquals("cc-1", order.getPaymentInformation().get(0).getCardRRN());
        assertEquals(ChannelTypeEnum.KIOSK, order.getSalesChannel());
    }

    @Test
    public void testBuildTransactionForPaypalSale() {
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(paypalModel));
        when(orderModel.getPaymentInfo()).thenReturn(paypalPaymentInfoModel);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, null,
                OrderTypeEnum.SALE, null);
        verify(orderModel).getEntries();
        assertEquals(1, transaction.getOrders().size());
        assertEquals(OrderTypeEnum.SALE, transaction.getOrders().get(0).getOrderType());
        assertEquals(1, transaction.getOrders().get(0).getPaymentInformation().size());
        assertEquals("paypal-1", transaction.getOrders().get(0).getPaymentInformation().get(0).getPayPalRRN());
    }

    @Test
    public void testBuildTransactionForIpgMixedSale() {
        doReturn(Boolean.TRUE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ipgTxnModel));
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfoModel);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, null,
                OrderTypeEnum.SALE, null);
        verify(orderModel).getEntries();
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.SALE, order.getOrderType());
        final List<PaymentInfo> paymentInformation = order.getPaymentInformation();
        assertEquals(2, paymentInformation.size());
        assertEquals("ipg-2", paymentInformation.get(0).getCardRRN());
        assertEquals("ipg-1", paymentInformation.get(1).getCardRRN());
        final BillingAddress billingAddress = order.getBillingAddress();
        assertNotNull(billingAddress);
        assertEquals("billingAddress address1", "Delivery St", billingAddress.getBillingAddressLine1());
        assertEquals("billingAddress address2", "", billingAddress.getBillingAddressLine2());
        assertEquals("billingAddress country", "AU", billingAddress.getBillingCountry());
    }

    @Test
    public void testBuildTransactionForIpgMixedSaleWithNoBillingAddress() {
        doReturn(Boolean.TRUE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ipgTxnModel));
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfoModel);
        when(ipgPaymentInfoModel.getBillingAddress()).thenReturn(null);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, null,
                OrderTypeEnum.SALE, null);
        verify(orderModel).getEntries();
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.SALE, order.getOrderType());
        final List<PaymentInfo> paymentInformation = order.getPaymentInformation();
        assertEquals(2, paymentInformation.size());
        assertEquals("ipg-2", paymentInformation.get(0).getCardRRN());
        assertEquals("ipg-1", paymentInformation.get(1).getCardRRN());
        final BillingAddress billingAddress = order.getBillingAddress();
        assertNull(billingAddress);
    }

    @Test
    public void testBuildTransactionForIpgMixedSaleExcludeBillingAddress() {
        doReturn(Boolean.FALSE).when(featureSwitchService).includeBillingAddressInAccertifyFeed();
        when(orderModel.getPaymentTransactions()).thenReturn(ImmutableList.of(ipgTxnModel));
        when(orderModel.getPaymentInfo()).thenReturn(ipgPaymentInfoModel);
        final Transaction transaction = accertifyUtil.buildTransaction(orderModel, null,
                OrderTypeEnum.SALE, null);
        verify(orderModel).getEntries();
        assertEquals(1, transaction.getOrders().size());
        final Order order = transaction.getOrders().get(0);
        assertEquals(OrderTypeEnum.SALE, order.getOrderType());
        final List<PaymentInfo> paymentInformation = order.getPaymentInformation();
        assertEquals(2, paymentInformation.size());
        assertEquals("ipg-2", paymentInformation.get(0).getCardRRN());
        assertEquals("ipg-1", paymentInformation.get(1).getCardRRN());
        final BillingAddress billingAddress = order.getBillingAddress();
        assertNull(billingAddress);
    }

    @Test
    public void testPopulateRecptEmailAddressEmpty() {
        final List<GiftRecipientModel> emptyLIst = new ArrayList<>();
        when(orderEntryModel.getGiftRecipients()).thenReturn(emptyLIst);
        final List<String> emailAddresses = accertifyUtil.populateRecipientEmailAddress(orderEntryModel);
        assertNull(emailAddresses);
    }

    @Test
    public void testPopulateRecptEmailAddressNull() {
        when(orderEntryModel.getGiftRecipients()).thenReturn(null);
        final List<String> emailAddresses = accertifyUtil.populateRecipientEmailAddress(orderEntryModel);
        assertNull(emailAddresses);
    }

    @Test
    public void testPopulateRecptEmailAddress() {
        final List<GiftRecipientModel> giftRecipientModelList = new ArrayList<>();
        final GiftRecipientModel mockGiftRecipient1 = Mockito.mock(GiftRecipientModel.class);
        final GiftRecipientModel mockGiftRecipient2 = Mockito.mock(GiftRecipientModel.class);
        when(mockGiftRecipient1.getEmail()).thenReturn("test@test.com");
        when(mockGiftRecipient2.getEmail()).thenReturn("test1@test.com");
        giftRecipientModelList.add(mockGiftRecipient1);
        giftRecipientModelList.add(mockGiftRecipient2);
        when(orderEntryModel.getGiftRecipients()).thenReturn(giftRecipientModelList);
        final List<String> emailAddresses = accertifyUtil.populateRecipientEmailAddress(orderEntryModel);
        assertNotNull(emailAddresses);
        assertEquals(emailAddresses.size(), giftRecipientModelList.size());
        assertEquals(emailAddresses.get(0), mockGiftRecipient1.getEmail());
        assertEquals(emailAddresses.get(1), mockGiftRecipient2.getEmail());

    }

    private void verifyAddressDetails(final AddressModel address, final CreditCardDetail creditCardDetail) {
        assertEquals(address.getLine2(), creditCardDetail.getFailedbillingAddressLine2());
        assertEquals(address.getLine1(), creditCardDetail.getFailedbillingAddressLine1());
        assertEquals(address.getFirstname(), creditCardDetail.getFailedbillingFirstName());
        assertEquals(address.getLastname(), creditCardDetail.getFailedbillingLastName());
        assertEquals(address.getTown(), creditCardDetail.getFailedbillingCity());
        assertEquals(address.getDistrict(), creditCardDetail.getFailedbillingRegion());
        assertEquals(address.getPostalcode(), creditCardDetail.getFailedbillingPostalCode());
    }

    @Test
    public void testBuildTransactionZipPaymentSales(){

        PaymentTransactionModel zipTxnModel = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel paymentEntry = mock(PaymentTransactionEntryModel.class);
        setTransactionEntryValues(paymentEntry, "zip-939514bc9f10", PaymentTransactionType.CAPTURE, zipTxnModel,
                new BigDecimal(15.00),"ACCEPTED");
        given(orderModel.getPaymentInfo()).willReturn(zippayPaymentInfoModel);
        given(zippayPaymentInfoModel.getReceiptNo()).willReturn("REC-NO#12345");
        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(zipTxnModel));
        given(zipTxnModel.getEntries()).willReturn(new LinkedList() {
            {
                add(paymentEntry);
            }
        });
        Transaction trans = accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.SALE, null);
        assertThat(trans.getOrders()).isNotEmpty();
        List<PaymentInfo> paymenInfo = trans.getOrders().get(0).getPaymentInformation();
        FailedPaymentInfo failedPaymenInfo = trans.getOrders().get(0).getFailedPaymentInformation();
        assertThat(paymenInfo).isNotEmpty();
        assertThat(paymenInfo.get(0).getPaymentType()).isEqualTo(PaymentTypeEnum.ZIPPAY);
        assertThat(paymenInfo.get(0).getZipOrderID()).isEqualTo("REC-NO#12345");
        assertThat(paymenInfo.get(0).getZipAmount()).isEqualTo(new BigDecimal(15).setScale(2, BigDecimal.ROUND_HALF_DOWN));
        assertThat(paymenInfo.get(0).getZipRefundId()).isNull();
        assertThat(paymenInfo.get(0).getZipStatus()).isEqualTo("ACCEPTED");
        assertThat(failedPaymenInfo.getTotalFailedZIPAttempts()).isEqualTo(0);
    }

    @Test
    public void testBuildTransactionZipPaymentRefund(){

        final PaymentTransactionModel zipTxnModel = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel paymentEntry = mock(PaymentTransactionEntryModel.class);
        setTransactionEntryValues(paymentEntry, "zip-939514bc9f10", PaymentTransactionType.CAPTURE, zipTxnModel,
                new BigDecimal(15),"ACCEPTED");
        given(orderModel.getPaymentInfo()).willReturn(zippayPaymentInfoModel);
        given(zippayPaymentInfoModel.getReceiptNo()).willReturn("REC-NO#12345");
        given(paymentEntry.getPaymentTransaction()).willReturn(zipTxnModel);
        given(zipTxnModel.getInfo()).willReturn(zippayPaymentInfoModel);
        given(zippayPaymentInfoModel.getRefundId()).willReturn("rf_HoegMxTI846ufjq68WN5m1");
        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(zipTxnModel));
        given(zipTxnModel.getEntries()).willReturn(new LinkedList() {
            {
                add(paymentEntry);
            }
        });
        final Transaction trans = accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.REFUND, null);
        assertThat(trans.getOrders()).isNotEmpty();
        final List<PaymentInfo> paymenInfo = trans.getOrders().get(0).getPaymentInformation();
        final FailedPaymentInfo failedPaymenInfo = trans.getOrders().get(0).getFailedPaymentInformation();
        assertThat(paymenInfo).isNotEmpty();
        assertThat(paymenInfo.get(0).getPaymentType()).isEqualTo(PaymentTypeEnum.ZIPPAY);
        assertThat(paymenInfo.get(0).getZipOrderID()).isEqualTo("REC-NO#12345");
        assertThat(paymenInfo.get(0).getZipAmount()).isEqualTo(new BigDecimal(15).setScale(2, BigDecimal.ROUND_HALF_DOWN));
        assertThat(paymenInfo.get(0).getZipRefundId()).isEqualTo("rf_HoegMxTI846ufjq68WN5m1");
        assertThat(paymenInfo.get(0).getZipStatus()).isEqualTo("ACCEPTED");
        assertThat(failedPaymenInfo.getTotalFailedZIPAttempts()).isEqualTo(0);
    }


    @Test
    public void testBuildTransactionZipPaymentFailedSales(){

        PaymentTransactionModel zipTxnModel = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel paymentEntry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel paymentEntryFailed = mock(PaymentTransactionEntryModel.class);

        setTransactionEntryValues(paymentEntry, "zip-939514bc9f10", PaymentTransactionType.CAPTURE, zipTxnModel,
                new BigDecimal(                        15),"ACCEPTED");
        setTransactionEntryValues(paymentEntryFailed, "zip-939514bc9f1213131", PaymentTransactionType.CAPTURE, zipTxnModel,
                new BigDecimal(                        15),"ERROR");


        given(orderModel.getPaymentInfo()).willReturn(zippayPaymentInfoModel);
        given(zippayPaymentInfoModel.getReceiptNo()).willReturn("REC-NO#12345");
        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(zipTxnModel));
        given(zipTxnModel.getInfo()).willReturn(zippayPaymentInfoModel);

        LinkedList paymentEntries = new LinkedList();
        paymentEntries.add(paymentEntryFailed);
        paymentEntries.add(paymentEntry);

        given(zipTxnModel.getEntries()).willReturn(paymentEntries);

        Transaction trans = accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.SALE, null);
        assertThat(trans.getOrders()).isNotEmpty();
        List<PaymentInfo> paymenInfo = trans.getOrders().get(0).getPaymentInformation();
        FailedPaymentInfo failedPaymenInfo = trans.getOrders().get(0).getFailedPaymentInformation();
        assertThat(paymenInfo).isNotEmpty();
        assertThat(paymenInfo.get(0).getPaymentType()).isEqualTo(PaymentTypeEnum.ZIPPAY);
        assertThat(paymenInfo.get(0).getZipOrderID()).isEqualTo("REC-NO#12345");
        assertThat(paymenInfo.get(0).getZipAmount()).isEqualTo(new BigDecimal(15).setScale(2, BigDecimal.ROUND_HALF_DOWN));
        assertThat(paymenInfo.get(0).getZipRefundId()).isNull();
        assertThat(paymenInfo.get(0).getZipStatus()).isEqualTo("ACCEPTED");
        assertThat(failedPaymenInfo.getTotalFailedZIPAttempts()).isEqualTo(1);
        assertThat(failedPaymenInfo.getFailZIPDetails()).hasSize(1);
        assertThat(failedPaymenInfo.getFailZIPDetails().get(0).getFailedAmount()).isEqualTo("15.00");
    }
}
