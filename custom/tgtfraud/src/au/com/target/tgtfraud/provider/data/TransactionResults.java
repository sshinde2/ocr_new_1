/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.util.List;


/**
 * @author vmuthura
 * 
 */
public class TransactionResults
{
    private List<Result> results;

    /**
     * @return the results
     */
    public List<Result> getResults()
    {
        return results;
    }

    /**
     * @param results
     *            the results to set
     */
    public void setResults(final List<Result> results)
    {
        this.results = results;
    }

}
