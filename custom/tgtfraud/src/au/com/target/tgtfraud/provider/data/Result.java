/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement(name = "transaction-results")
public class Result
{
    private String transactionId;
    private String crossReference;
    private String rulesTripped;
    private int totlaScore;
    private RecommendationTypeEnum recommendation;
    private String remarks;

    private String message;

    /**
     * @return the transactionId
     */
    @XmlElement(name = "transaction-id")
    public String getTransactionId()
    {
        return transactionId;
    }

    /**
     * @param transactionId
     *            the transactionId to set
     */
    public void setTransactionId(final String transactionId)
    {
        this.transactionId = transactionId;
    }

    /**
     * @return the crossReference
     */
    @XmlElement(name = "cross-reference")
    public String getCrossReference()
    {
        return crossReference;
    }

    /**
     * @param crossReference
     *            the crossReference to set
     */
    public void setCrossReference(final String crossReference)
    {
        this.crossReference = crossReference;
    }

    /**
     * @return the rulesTripped
     */
    @XmlElement(name = "rules-tripped")
    public String getRulesTripped()
    {
        return rulesTripped;
    }

    /**
     * @param rulesTripped
     *            the rulesTripped to set
     */
    public void setRulesTripped(final String rulesTripped)
    {
        this.rulesTripped = rulesTripped;
    }

    /**
     * @return the totlaScore
     */
    @XmlElement(name = "total-score")
    public int getTotlaScore()
    {
        return totlaScore;
    }

    /**
     * @param totlaScore
     *            the totlaScore to set
     */
    public void setTotlaScore(final int totlaScore)
    {
        this.totlaScore = totlaScore;
    }

    /**
     * @return the recommendation
     */
    @XmlElement(name = "recommendation-code")
    public RecommendationTypeEnum getRecommendation()
    {
        return recommendation;
    }

    /**
     * @param recommendation
     *            the recommendation to set
     */
    public void setRecommendation(final RecommendationTypeEnum recommendation)
    {
        this.recommendation = recommendation;
    }

    /**
     * @return the remarks
     */
    @XmlElement
    public String getRemarks()
    {
        return remarks;
    }

    /**
     * @param remarks
     *            the remarks to set
     */
    public void setRemarks(final String remarks)
    {
        this.remarks = remarks;
    }

    /**
     * @return the message
     */
    @XmlElement(name = "ERROR")
    public String getMessage()
    {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }

}
