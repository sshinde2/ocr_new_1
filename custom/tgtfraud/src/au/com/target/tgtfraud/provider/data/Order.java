/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtfraud.provider.util.OrderDateTimeAdapter;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "orderNumber",
        "orderType",
        "orderDateTime",
        "totalAmount",
        "totalShippingCharges",
        "totalSalesTax",
        "salesRep",
        "salesChannel",
        "promotionCode",
        "promotionAmount",
        "ipAddress",
        "browserCookie",
        "paymentInformation",
        "billingAddress",
        "failedPaymentInformation",
        "memberInformation",
        "orderDetails",
        "shippingInformation",
        "threatMetrixSessionID"
})
public class Order
{
    private String orderNumber;
    private OrderTypeEnum orderType;
    private Date orderDateTime;
    private BigDecimal totalAmount;
    private BigDecimal totalSalesTax;
    private BigDecimal totalShippingCharges;
    private String salesRep;
    private ChannelTypeEnum salesChannel;
    private String promotionCode;
    private BigDecimal promotionAmount;
    private String ipAddress;
    private String browserCookie;
    private List<PaymentInfo> paymentInformation;
    private BillingAddress billingAddress;
    private FailedPaymentInfo failedPaymentInformation;
    private Member memberInformation;
    private OrderDetail orderDetails;
    private ShippingInfo shippingInformation;
    private String threatMetrixSessionID;

    /**
     * @return the orderNumber
     */
    public String getOrderNumber()
    {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the orderType
     */
    public OrderTypeEnum getOrderType()
    {
        return orderType;
    }

    /**
     * @param orderType
     *            the orderType to set
     */
    public void setOrderType(final OrderTypeEnum orderType)
    {
        this.orderType = orderType;
    }

    /**
     * @return the orderDateTime
     */
    @XmlJavaTypeAdapter(OrderDateTimeAdapter.class)
    public Date getOrderDateTime()
    {
        return orderDateTime;
    }

    /**
     * @param orderDateTime
     *            the orderDateTime to set
     */
    public void setOrderDateTime(final Date orderDateTime)
    {
        this.orderDateTime = orderDateTime;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the totalAmount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the totalSalesTax
     */
    public BigDecimal getTotalSalesTax()
    {
        return totalSalesTax;
    }

    /**
     * @param totalSalesTax
     *            the totalSalesTax to set
     */
    public void setTotalSalesTax(final BigDecimal totalSalesTax)
    {
        this.totalSalesTax = totalSalesTax;
    }

    /**
     * @return the totalShippingCharges
     */
    public BigDecimal getTotalShippingCharges()
    {
        return totalShippingCharges;
    }

    /**
     * @param totalShippingCharges
     *            the totalShippingCharges to set
     */
    public void setTotalShippingCharges(final BigDecimal totalShippingCharges)
    {
        this.totalShippingCharges = totalShippingCharges;
    }

    /**
     * @return the salesRep
     */
    public String getSalesRep()
    {
        return salesRep;
    }

    /**
     * @param salesRep
     *            the salesRep to set
     */
    public void setSalesRep(final String salesRep)
    {
        this.salesRep = salesRep;
    }

    /**
     * @return the salesChannel
     */
    public ChannelTypeEnum getSalesChannel()
    {
        return salesChannel;
    }

    /**
     * @param salesChannel
     *            the salesChannel to set
     */
    public void setSalesChannel(final ChannelTypeEnum salesChannel)
    {
        this.salesChannel = salesChannel;
    }

    /**
     * @return the promotionCode
     */
    public String getPromotionCode()
    {
        return promotionCode;
    }

    /**
     * @param promotionCode
     *            the promotionCode to set
     */
    public void setPromotionCode(final String promotionCode)
    {
        this.promotionCode = promotionCode;
    }

    /**
     * @return the promotionAmount
     */
    public BigDecimal getPromotionAmount()
    {
        return promotionAmount;
    }

    /**
     * @param promotionAmount
     *            the promotionAmount to set
     */
    public void setPromotionAmount(final BigDecimal promotionAmount)
    {
        this.promotionAmount = promotionAmount;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress()
    {
        return ipAddress;
    }

    /**
     * @param ipAddress
     *            the ipAddress to set
     */
    public void setIpAddress(final String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the browserCookie
     */
    public String getBrowserCookie()
    {
        return browserCookie;
    }

    /**
     * @param browserCookie
     *            the browserCookie to set
     */
    public void setBrowserCookie(final String browserCookie)
    {
        this.browserCookie = browserCookie;
    }

    /**
     * @return the paymentInformation
     */
    public List<PaymentInfo> getPaymentInformation()
    {
        return paymentInformation;
    }

    /**
     * @param paymentInformation
     *            the paymentInformation to set
     */
    public void setPaymentInformation(final List<PaymentInfo> paymentInformation)
    {
        this.paymentInformation = paymentInformation;
    }

    /**
     * @return the failedPaymentInformation
     */
    public FailedPaymentInfo getFailedPaymentInformation()
    {
        return failedPaymentInformation;
    }

    /**
     * @param failedPaymentInformation
     *            the failedPaymentInformation to set
     */
    public void setFailedPaymentInformation(final FailedPaymentInfo failedPaymentInformation)
    {
        this.failedPaymentInformation = failedPaymentInformation;
    }

    /**
     * @return the memberInformation
     */
    public Member getMemberInformation()
    {
        return memberInformation;
    }

    /**
     * @param memberInformation
     *            the memberInformation to set
     */
    public void setMemberInformation(final Member memberInformation)
    {
        this.memberInformation = memberInformation;
    }

    /**
     * @return the orderDetails
     */
    public OrderDetail getOrderDetails()
    {
        return orderDetails;
    }

    /**
     * @param orderDetails
     *            the orderDetails to set
     */
    public void setOrderDetails(final OrderDetail orderDetails)
    {
        this.orderDetails = orderDetails;
    }

    /**
     * @return the shippingInformation
     */
    public ShippingInfo getShippingInformation()
    {
        return shippingInformation;
    }

    /**
     * @param shippingInformation
     *            the shippingInformation to set
     */
    public void setShippingInformation(final ShippingInfo shippingInformation)
    {
        this.shippingInformation = shippingInformation;
    }

    /**
     * @return the threatMetrixSessionID
     */
    public String getThreatMetrixSessionID() {
        return threatMetrixSessionID;
    }

    /**
     * @param threatMetrixSessionID
     *            the threatMetrixSessionID to set
     */
    public void setThreatMetrixSessionID(final String threatMetrixSessionID) {
        this.threatMetrixSessionID = threatMetrixSessionID;
    }

    /**
     * @return the billingAddress
     */
    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

}
