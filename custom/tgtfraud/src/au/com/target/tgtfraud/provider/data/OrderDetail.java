/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
public class OrderDetail
{
    private List<OrderItem> orderItem;

    /**
     * @return the orderItem
     */
    public List<OrderItem> getOrderItem()
    {
        return orderItem;
    }

    /**
     * @param orderItem
     *            the orderItem to set
     */
    public void setOrderItem(final List<OrderItem> orderItem)
    {
        this.orderItem = orderItem;
    }

}
