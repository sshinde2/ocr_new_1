/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlType(propOrder = {
        "failedReceiptNumber",
        "failedcardType",
        "failedAmount"
})
public class FailedGiftCardDetail
{
    private String failedReceiptNumber;
    private String failedcardType;
    private String failedAmount;

    /**
     * @return the failedReceiptNumber
     */
    public String getFailedReceiptNumber() {
        return failedReceiptNumber;
    }

    /**
     * @param failedReceiptNumber
     *            the failedReceiptNumber to set
     */
    public void setFailedReceiptNumber(final String failedReceiptNumber) {
        this.failedReceiptNumber = failedReceiptNumber;
    }

    /**
     * @return the failedcardType
     */
    public String getFailedcardType() {
        return failedcardType;
    }

    /**
     * @param failedcardType
     *            the failedcardType to set
     */
    public void setFailedcardType(final String failedcardType) {
        this.failedcardType = failedcardType;
    }

    /**
     * @return the amount
     */
    public String getFailedAmount() {
        return failedAmount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setFailedAmount(final String amount) {
        this.failedAmount = amount;
    }

}
