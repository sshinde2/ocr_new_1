/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "paymentType",
        "cardNumber",
        "cardHolderName",
        "cardType",
        "cardAuthorizedAmount",
        "cardExpireDate",
        "cardTransactionID",
        "cardTokenID",
        "cardRRN",

        "payPalRequestID",
        "payPalEmail",
        "payPalAmount",
        "payPalStatus",
        "payPalTransactionId",
        "payPalRRN",

        "afterPayOrderId",
        "afterPayAmount",
        "afterPayStatus",
        "afterPayRefundId",

        "zipOrderID",
        "zipAmount",
        "zipStatus",
        "zipRefundId",

        "billingFullName",
        "billingFirstName",
        "billingLastName",
        "billingAddressLine1",
        "billingAddressLine2",
        "billingCity",
        "billingRegion",
        "billingPostalCode",
        "billingCountry",
        "billingPhone"
})
public class PaymentInfo {
    private PaymentTypeEnum paymentType;
    private String payPalRequestID;
    private String payPalEmail;
    private BigDecimal payPalAmount;
    private String payPalStatus;
    private String payPalTransactionId;
    private String payPalRRN;
    private String afterPayOrderId;
    private String afterPayRefundId;
    private BigDecimal afterPayAmount;
    private String afterPayStatus;

    private String cardNumber;
    private String cardHolderName;
    private String cardType;
    private BigDecimal cardAuthorizedAmount;
    private String cardExpireDate;
    private String cardTransactionID;
    private String cardTokenID;
    private String cardRRN;

    private String billingFullName;
    private String billingFirstName;
    private String billingLastName;
    private String billingAddressLine1;
    private String billingAddressLine2;
    private String billingCity;
    private String billingRegion;
    private String billingPostalCode;
    private String billingCountry;
    private String billingPhone;

    private String zipOrderID;
    private String zipStatus;
    private BigDecimal zipAmount;
    private String zipRefundId;

    /**
     * @return the paymentType
     */
    public PaymentTypeEnum getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType
     *            the paymentType to set
     */
    public void setPaymentType(final PaymentTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the payPalRequestID
     */
    public String getPayPalRequestID() {
        return payPalRequestID;
    }

    /**
     * @param payPalRequestID
     *            the payPalRequestID to set
     */
    public void setPayPalRequestID(final String payPalRequestID) {
        this.payPalRequestID = payPalRequestID;
    }

    /**
     * @return the payPalEmail
     */
    public String getPayPalEmail() {
        return payPalEmail;
    }

    /**
     * @param payPalEmail
     *            the payPalEmail to set
     */
    public void setPayPalEmail(final String payPalEmail) {
        this.payPalEmail = payPalEmail;
    }

    /**
     * @return the payPalAmount
     */
    public BigDecimal getPayPalAmount() {
        return payPalAmount;
    }

    /**
     * @param payPalAmount
     *            the payPalAmount to set
     */
    public void setPayPalAmount(final BigDecimal payPalAmount) {
        this.payPalAmount = payPalAmount;
    }

    /**
     * @return the payPalStatus
     */
    public String getPayPalStatus() {
        return payPalStatus;
    }

    /**
     * @param payPalStatus
     *            the payPalStatus to set
     */
    public void setPayPalStatus(final String payPalStatus) {
        this.payPalStatus = payPalStatus;
    }

    /**
     * @return the payPalTransactionId
     */
    public String getPayPalTransactionId() {
        return payPalTransactionId;
    }

    /**
     * @param payPalTransactionId
     *            the payPalTransactionId to set
     */
    public void setPayPalTransactionId(final String payPalTransactionId) {
        this.payPalTransactionId = payPalTransactionId;
    }

    /**
     * @return the payPalRRN
     */
    public String getPayPalRRN() {
        return payPalRRN;
    }

    /**
     * @param payPalRRN
     *            the payPalRRN to set
     */
    public void setPayPalRRN(final String payPalRRN) {
        this.payPalRRN = payPalRRN;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardHolderName
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * @param cardHolderName
     *            the cardHolderName to set
     */
    public void setCardHolderName(final String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardAuthorizedAmount
     */
    public BigDecimal getCardAuthorizedAmount() {
        return cardAuthorizedAmount;
    }

    /**
     * @param cardAuthorizedAmount
     *            the cardAuthorizedAmount to set
     */
    public void setCardAuthorizedAmount(final BigDecimal cardAuthorizedAmount) {
        if (cardAuthorizedAmount != null) {
            this.cardAuthorizedAmount = cardAuthorizedAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        }
    }

    /**
     * @return the cardExpireDate
     */
    public String getCardExpireDate() {
        return cardExpireDate;
    }

    /**
     * @param cardExpireDate
     *            the cardExpireDate to set
     */
    public void setCardExpireDate(final String cardExpireDate) {
        this.cardExpireDate = cardExpireDate;
    }

    /**
     * @return the cardTransactionID
     */
    public String getCardTransactionID() {
        return cardTransactionID;
    }

    /**
     * @param cardTransactionID
     *            the cardTransactionID to set
     */
    public void setCardTransactionID(final String cardTransactionID) {
        this.cardTransactionID = cardTransactionID;
    }

    /**
     * @return the cardTokenID
     */
    public String getCardTokenID() {
        return cardTokenID;
    }

    /**
     * @param cardTokenID
     *            the cardTokenID to set
     */
    public void setCardTokenID(final String cardTokenID) {
        this.cardTokenID = cardTokenID;
    }

    /**
     * @return the cardRRN
     */
    public String getCardRRN() {
        return cardRRN;
    }

    /**
     * @param cardRRN
     *            the cardRRN to set
     */
    public void setCardRRN(final String cardRRN) {
        this.cardRRN = cardRRN;
    }

    /**
     * @return the billingFullName
     */
    public String getBillingFullName() {
        return billingFullName;
    }

    /**
     * @param billingFullName
     *            the billingFullName to set
     */
    public void setBillingFullName(final String billingFullName) {
        this.billingFullName = billingFullName;
    }

    /**
     * @return the billingFirstName
     */
    public String getBillingFirstName() {
        return billingFirstName;
    }

    /**
     * @param billingFirstName
     *            the billingFirstName to set
     */
    public void setBillingFirstName(final String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    /**
     * @return the billingLastName
     */
    public String getBillingLastName() {
        return billingLastName;
    }

    /**
     * @param billingLastName
     *            the billingLastName to set
     */
    public void setBillingLastName(final String billingLastName) {
        this.billingLastName = billingLastName;
    }

    /**
     * @return the billingAddressLine1
     */
    public String getBillingAddressLine1() {
        return billingAddressLine1;
    }

    /**
     * @param billingAddressLine1
     *            the billingAddressLine1 to set
     */
    public void setBillingAddressLine1(final String billingAddressLine1) {
        this.billingAddressLine1 = billingAddressLine1;
    }

    /**
     * @return the billingAddressLine2
     */
    public String getBillingAddressLine2() {
        return billingAddressLine2;
    }

    /**
     * @param billingAddressLine2
     *            the billingAddressLine2 to set
     */
    public void setBillingAddressLine2(final String billingAddressLine2) {
        this.billingAddressLine2 = billingAddressLine2;
    }

    /**
     * @return the billingCity
     */
    public String getBillingCity() {
        return billingCity;
    }

    /**
     * @param billingCity
     *            the billingCity to set
     */
    public void setBillingCity(final String billingCity) {
        this.billingCity = billingCity;
    }

    /**
     * @return the billingRegion
     */
    public String getBillingRegion() {
        return billingRegion;
    }

    /**
     * @param billingRegion
     *            the billingRegion to set
     */
    public void setBillingRegion(final String billingRegion) {
        this.billingRegion = billingRegion;
    }

    /**
     * @return the billingPostalCode
     */
    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    /**
     * @param billingPostalCode
     *            the billingPostalCode to set
     */
    public void setBillingPostalCode(final String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    /**
     * @return the billingCountry
     */
    public String getBillingCountry() {
        return billingCountry;
    }

    /**
     * @param billingCountry
     *            the billingCountry to set
     */
    public void setBillingCountry(final String billingCountry) {
        this.billingCountry = billingCountry;
    }

    /**
     * @return the billingPhone
     */
    public String getBillingPhone() {
        return billingPhone;
    }

    /**
     * @param billingPhone
     *            the billingPhone to set
     */
    public void setBillingPhone(final String billingPhone) {
        this.billingPhone = billingPhone;
    }

    /**
     * @return the afterPayOrderId
     */
    public String getAfterPayOrderId() {
        return afterPayOrderId;
    }

    /**
     * @param afterPayOrderId
     *            the afterPayOrderId to set
     */
    public void setAfterPayOrderId(final String afterPayOrderId) {
        this.afterPayOrderId = afterPayOrderId;
    }

    /**
     * @return the afterPayAmount
     */
    public BigDecimal getAfterPayAmount() {
        return afterPayAmount;
    }

    /**
     * @param afterPayAmount
     *            the afterPayAmount to set
     */
    public void setAfterPayAmount(final BigDecimal afterPayAmount) {
        this.afterPayAmount = afterPayAmount;
    }

    /**
     * @return the afterPayStatus
     */
    public String getAfterPayStatus() {
        return afterPayStatus;
    }

    /**
     * @param afterPayStatus
     *            the afterPayStatus to set
     */
    public void setAfterPayStatus(final String afterPayStatus) {
        this.afterPayStatus = afterPayStatus;
    }

    /**
     * @return the afterPayRefundId
     */
    public String getAfterPayRefundId() {
        return afterPayRefundId;
    }

    /**
     * @param afterPayRefundId
     *            the afterPayRefundId to set
     */
    public void setAfterPayRefundId(final String afterPayRefundId) {
        this.afterPayRefundId = afterPayRefundId;
    }

    /**
     *
     * @return
     */
    public String getZipOrderID() {
        return zipOrderID;
    }

    /**
     *
     * @param zipOrderID
     */
    public void setZipOrderID(final String zipOrderID) {
        this.zipOrderID = zipOrderID;
    }

    /**
     *
     * @return
     */
    public String getZipStatus() {
        return zipStatus;
    }

    /**
     *
     * @param zipStatus
     */
    public void setZipStatus(final String zipStatus) {
        this.zipStatus = zipStatus;
    }

    /**
     *
     * @return
     */
    public BigDecimal getZipAmount() {
        return zipAmount;
    }

    /**
     *
     * @param zipAmount
     */
    public void setZipAmount(final BigDecimal zipAmount) {
        this.zipAmount = zipAmount;
    }

    /**
     *
     * @return
     */
    public String getZipRefundId() {
        return zipRefundId;
    }

    /**
     *
     * @param zipRefundId
     */
    public void setZipRefundId(final String zipRefundId) {
        this.zipRefundId = zipRefundId;
    }
}
