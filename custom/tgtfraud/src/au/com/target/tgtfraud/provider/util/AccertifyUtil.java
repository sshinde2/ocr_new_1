/**
 * 
 */
package au.com.target.tgtfraud.provider.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtfraud.provider.data.AfterpayDetail;
import au.com.target.tgtfraud.provider.data.BillingAddress;
import au.com.target.tgtfraud.provider.data.ChannelTypeEnum;
import au.com.target.tgtfraud.provider.data.CreditCardDetail;
import au.com.target.tgtfraud.provider.data.FailedGiftCardDetail;
import au.com.target.tgtfraud.provider.data.FailedPaymentInfo;
import au.com.target.tgtfraud.provider.data.Member;
import au.com.target.tgtfraud.provider.data.Order;
import au.com.target.tgtfraud.provider.data.OrderDetail;
import au.com.target.tgtfraud.provider.data.OrderItem;
import au.com.target.tgtfraud.provider.data.OrderTypeEnum;
import au.com.target.tgtfraud.provider.data.PayPalDetail;
import au.com.target.tgtfraud.provider.data.PaymentInfo;
import au.com.target.tgtfraud.provider.data.PaymentTypeEnum;
import au.com.target.tgtfraud.provider.data.ShippingInfo;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgtfraud.provider.data.ZipPaymentDetail;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.voucher.VoucherService;


/**
 * @author vmuthura
 * 
 */
public class AccertifyUtil {
    private static final Logger LOG = Logger.getLogger(AccertifyUtil.class);
    private static final String PAYMENTINFO_NOTFOUND = "AccertifyMissingPaymentInfo: Payment Info missing for the Order={0}";
    public static final String PATTERN_AMT = "#.00";
    private TargetDiscountService targetDiscountService = null;
    private VoucherService voucherService = null;
    private TargetFeatureSwitchService featureSwitchService = null;
    private TargetPaymentService targetPaymentService = null;



    /**
     * @param orderModel
     * @param orderModRecordEntryModel
     * @param orderType
     * @return transaction
     */
    public Transaction buildTransaction(final OrderModel orderModel,
            final OrderModificationRecordEntryModel orderModRecordEntryModel, final OrderTypeEnum orderType,
            final List<PaymentTransactionEntryModel> refundEntries) {
        Assert.notNull(orderModel, "OrderModel is null");
        final Transaction transaction = new Transaction();
        final List<Order> orders = new ArrayList<>();
        final Order order = getBasicOrderInformation(orderModel, orderType);
        //Billing Address
        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        final AddressModel billingAddress = paymentInfo.getBillingAddress();
        if (includeBillingAddress(orderModel, billingAddress)) {
            order.setBillingAddress(builderBillingAddress(billingAddress));
        }
        //Failed Payment
        order.setFailedPaymentInformation(getFailedPaymentInfo(orderModel));
        //Member details
        order.setMemberInformation(getMemberInfo((TargetCustomerModel)orderModel.getUser(),
                orderModel.getTmdCardNumber(), orderModel.getFlyBuysCode()));
        //Shipping information
        order.setShippingInformation(getShippingInfo(orderModel.getDeliveryAddress(),
                orderModel.getDeliveryMode()));

        if (orderModRecordEntryModel == null) {
            //PaymentInformation
            order.setPaymentInformation(getPaymentInfo(getCapturePaymentEntries(orderModel), paymentInfo));
            //Order details
            order.setOrderDetails(getOrderDetails(orderModel.getEntries()));
        }
        else {
            //Order details
            order.setOrderDetails(getOrderDetailsFromCancelReturnRecord(orderModRecordEntryModel
                    .getOrderEntriesModificationEntries()));
            order.setPaymentInformation(getPaymentInfo(refundEntries, paymentInfo));
            final BigDecimal refundedAmount = BigDecimal.valueOf(orderModRecordEntryModel.getRefundedAmount()
                    .doubleValue());
            order.setTotalAmount(refundedAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN));
            // The shipping charges should be the amount of shipping refunded
            final Double refundedShipAmount = orderModRecordEntryModel.getRefundedShippingAmount();
            if (refundedShipAmount != null) {
                order.setTotalShippingCharges(BigDecimal.valueOf(refundedShipAmount.doubleValue()));
            }
        }

        orders.add(order);
        transaction.setOrders(orders);
        return transaction;
    }

    public Order getBasicOrderInformation(final AbstractOrderModel orderModel, final OrderTypeEnum orderType) {
        Assert.notNull(orderModel, "OrderModel is null");
        final Order order = new Order();
        order.setOrderNumber(orderModel.getCode());
        order.setOrderType(orderType);
        order.setOrderDateTime(orderModel.getCreationtime());
        order.setTotalAmount(BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue()));
        order.setTotalShippingCharges(BigDecimal.valueOf(orderModel.getDeliveryCost().doubleValue()));
        order.setTotalSalesTax(BigDecimal.valueOf(orderModel.getTotalTax().doubleValue()));
        order.setSalesRep("");
        order.setSalesChannel(getChannelTypeFromSalesApplication(((OrderModel)orderModel).getSalesApplication()));
        order.setThreatMetrixSessionID(orderModel.getThreatMatrixSessionID());

        final String voucherCode = getVoucherCode((OrderModel)orderModel);
        order.setPromotionCode(voucherCode == null ? "" : voucherCode);

        final double totalDiscount = targetDiscountService.getTotalDiscount(orderModel);
        order.setPromotionAmount(totalDiscount > 0 ? BigDecimal.valueOf(totalDiscount) : null);

        if (orderModel.getFraudOrderDetails() != null) {
            order.setIpAddress(orderModel.getFraudOrderDetails().getCustomerIP());
            order.setBrowserCookie(orderModel.getFraudOrderDetails().getBrowserCookies());
        }
        return order;
    }

    public ChannelTypeEnum getChannelTypeFromSalesApplication(final SalesApplication salesApplication) {
        if (SalesApplication.WEB.equals(salesApplication) || SalesApplication.WEBMOBILE.equals(salesApplication)) {
            return ChannelTypeEnum.WEB;
        }

        if (SalesApplication.CALLCENTER.equals(salesApplication)) {
            return ChannelTypeEnum.PHONE;
        }

        if (SalesApplication.KIOSK.equals(salesApplication)) {
            return ChannelTypeEnum.KIOSK;
        }

        if (SalesApplication.B2B.equals(salesApplication)) {
            return ChannelTypeEnum.B2B;
        }

        return ChannelTypeEnum.AUTOMATION;
    }

    protected List<PaymentInfo> getPaymentInfo(final List<PaymentTransactionEntryModel> entries,
            final PaymentInfoModel paymentInfoModel) {
        final List<PaymentInfo> paymentInfoList = new LinkedList<>();
        PaymentInfo paymentInfo;
        for (final PaymentTransactionEntryModel paymentTxnEntryModel : entries) {
            if (TransactionStatus.ACCEPTED.toString().equals(paymentTxnEntryModel.getTransactionStatus())) {
                paymentInfo = new PaymentInfo();
                populatePaymentDetails(paymentInfo, paymentInfoModel, paymentTxnEntryModel);
                paymentInfoList.add(paymentInfo);
            }
        }
        return paymentInfoList;
    }

    /**
     * 
     * @param orderModel
     * @return FailedPaymentInfo
     */
    public FailedPaymentInfo getFailedPaymentInfo(final OrderModel orderModel) {
        final FailedPaymentInfo failedPaymentInfo = new FailedPaymentInfo();
        final List<CreditCardDetail> failedCreditCardDetails = new LinkedList<>();
        final List<PayPalDetail> failedPayPalDetails = new LinkedList<>();
        final List<AfterpayDetail> failedAfterpayDetails = new ArrayList<>();
        final List<ZipPaymentDetail> failZIPDetail = new ArrayList<>();
        AddressModel address;
        for (final PaymentTransactionModel paymentTransaction : orderModel.getPaymentTransactions()) {
            for (final PaymentTransactionEntryModel ptem : paymentTransaction.getEntries()) {
                if (!TransactionStatus.ACCEPTED.toString().equals(ptem.getTransactionStatus())) {
                    final PaymentInfoModel paymentInfoModel = paymentTransaction.getInfo();
                    //Some reason,few failed transactions are missing paymentinfo,added null check and logging
                    //to create splunk alert which can be investigated further
                    if (null == paymentInfoModel) {
                        LOG.error(MessageFormat.format(PAYMENTINFO_NOTFOUND, orderModel.getCode()));
                        continue;
                    }
                    address = paymentInfoModel.getBillingAddress();
                    if (paymentInfoModel instanceof CreditCardPaymentInfoModel ||
                            paymentInfoModel instanceof PinPadPaymentInfoModel) {
                        failedCreditCardDetails
                                .add(populateCreditCardOrPinPadDetails(paymentInfoModel, address, orderModel));
                    }
                    else if (paymentInfoModel instanceof IpgPaymentInfoModel) {
                        if (ptem.getIpgPaymentInfo() instanceof IpgCreditCardPaymentInfoModel) {
                            final IpgCreditCardPaymentInfoModel ipgCreditCard = (IpgCreditCardPaymentInfoModel)ptem
                                    .getIpgPaymentInfo();
                            failedCreditCardDetails
                                    .add(populateCreditCardOrPinPadDetails(ipgCreditCard, address, orderModel));
                        }
                    }
                    else if (paymentInfoModel instanceof PaypalPaymentInfoModel) {
                        failedPayPalDetails
                                .add(getPayPalDetailFromPaymentInfo((PaypalPaymentInfoModel)paymentInfoModel, ptem));
                    }
                    else if (paymentInfoModel instanceof AfterpayPaymentInfoModel) {
                        failedAfterpayDetails.add(
                                getAfterpayDetailFromPaymentInfo((AfterpayPaymentInfoModel)paymentInfoModel, ptem));
                    }
                    else if (paymentInfoModel instanceof ZippayPaymentInfoModel) {
                        failZIPDetail.add(
                                getFailedPaymentInfo(ptem));
                    }
                }
            }
        }


        if (null != orderModel.getPaymentMode()
                && !PaymentTypeEnum.PAYPAL.toString().equalsIgnoreCase(orderModel.getPaymentMode().getCode())
                && !PaymentTypeEnum.AFTERPAY.toString().equalsIgnoreCase(orderModel.getPaymentMode().getCode())
                && !PaymentTypeEnum.ZIPPAY.toString().equalsIgnoreCase(orderModel.getPaymentMode().getCode())) {
            populateFailedGCDetail(orderModel, failedPaymentInfo);
        } //skip paypal

        failedPaymentInfo.setFailCCdetails(failedCreditCardDetails);
        failedPaymentInfo.setTotalFailedCCAttempts(failedCreditCardDetails.size());
        failedPaymentInfo.setFailPPdetails(failedPayPalDetails);
        failedPaymentInfo.setTotalFailedPPAttempts(failedPayPalDetails.size());
        failedPaymentInfo.setFailAPDetails(failedAfterpayDetails);
        failedPaymentInfo.setTotalFailedAPAttempts(failedAfterpayDetails.size());
        failedPaymentInfo.setTotalFailedZIPAttempts(failZIPDetail.size());
        failedPaymentInfo.setFailZIPDetails(failZIPDetail);

        return failedPaymentInfo;
    }

    private ZipPaymentDetail getFailedPaymentInfo(final PaymentTransactionEntryModel ptem) {
        final ZipPaymentDetail zippaymentDetail = new ZipPaymentDetail();
        zippaymentDetail.setFailedRequestId(ptem.getCode());
        zippaymentDetail.setFailedStatus(ptem.getTransactionStatus());
        if(ptem.getAmount() != null) {
            zippaymentDetail.setFailedAmount(getFormattedAmount(ptem.getAmount()));
        }
        return zippaymentDetail;
    }

    public String getFormattedAmount(final BigDecimal amount) {
        ServicesUtil.validateParameterNotNull(amount, "Amount can not be null");
        final DecimalFormat df = new DecimalFormat(PATTERN_AMT);
        return df.format(amount);
    }

    protected boolean includeBillingAddress(final OrderModel orderModel, final AddressModel billingAddress) {
        boolean includeBillingAddress = false;
        if (billingAddress != null) {
            if (featureSwitchService.includeBillingAddressInAccertifyFeed()) {
                includeBillingAddress = true;
            }
            else {
                final DeliveryModeModel deliveryModeModel = orderModel.getDeliveryMode();
                if (deliveryModeModel instanceof TargetZoneDeliveryModeModel) {
                    final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryModeModel;
                    if (BooleanUtils.isTrue(targetDeliveryMode.getIsDigital())
                            || BooleanUtils.isTrue(targetDeliveryMode.getIsDeliveryToStore())) {
                        includeBillingAddress = true;
                    }
                }
            }
        }
        return includeBillingAddress;
    }

    private List<PaymentTransactionEntryModel> getCapturePaymentEntries(final OrderModel orderModel) {
        final List<PaymentTransactionEntryModel> entryList = new LinkedList<>();
        final PaymentTransactionModel paymentTxnModel = PaymentTransactionHelper.findCaptureTransaction(orderModel);
        if (paymentTxnModel != null) {
            for (final PaymentTransactionEntryModel paymentTxnEntryModel : paymentTxnModel.getEntries()) {
                if (PaymentTransactionType.CAPTURE.equals(paymentTxnEntryModel.getType())
                        && TransactionStatus.ACCEPTED.toString().equals(paymentTxnEntryModel.getTransactionStatus())) {
                    entryList.add(paymentTxnEntryModel);
                }
            }
        }
        return entryList;
    }

    /**
     * @param orderModel
     * @param failedPaymentInfo
     */
    private void populateFailedGCDetail(final OrderModel orderModel, final FailedPaymentInfo failedPaymentInfo) {
        try {
            final List<FailedGiftCardDetail> failedGCDetails = new LinkedList<>();
            final List<PaymentTransactionEntryData> failedGiftCardPaymentList = targetPaymentService
                    .retrieveFailedGiftcardPayment(orderModel);
            if (CollectionUtils.isNotEmpty(failedGiftCardPaymentList)) {
                for (final PaymentTransactionEntryData failedGiftCardPayment : failedGiftCardPaymentList) {
                    final FailedGiftCardDetail failedDetail = new FailedGiftCardDetail();
                    failedDetail.setFailedAmount(failedGiftCardPayment.getAmount().toString());
                    failedDetail.setFailedReceiptNumber(failedGiftCardPayment.getReceiptNumber());
                    failedDetail.setFailedcardType("giftcard");
                    failedGCDetails.add(failedDetail);
                }
            }
            failedPaymentInfo.setFailGCdetails(failedGCDetails);
            failedPaymentInfo.setTotalFailedGCAttempts(failedGCDetails.size());
        }
        catch (final Exception ex) {
            LOG.warn(MessageFormat.format("failed to fetch failed gc detail for order:{0}", orderModel.getCode()), ex);
        }
    }

    /**
     * 
     * @param creditCardDetail
     * @param pinPadPaymentInfoModel
     */
    private void populatePinPadSpecificDetailFromPaymentInfo(final CreditCardDetail creditCardDetail,
            final PinPadPaymentInfoModel pinPadPaymentInfoModel) {
        creditCardDetail.setFailedcardNumber(pinPadPaymentInfoModel.getMaskedCardNumber());
        if (null != pinPadPaymentInfoModel.getCardType()) {
            creditCardDetail.setFailedcardType(pinPadPaymentInfoModel.getCardType().getCode());
        }
    }


    /**
     * @param paymentInfoModel
     */
    private void populateCreditCardSpecificDetailFromPaymentInfo(final CreditCardDetail creditCardDetail,
            final CreditCardPaymentInfoModel paymentInfoModel) {
        creditCardDetail.setFailedcardNumber(paymentInfoModel.getNumber());
        creditCardDetail.setFailedcardType(paymentInfoModel.getType().getCode());
        creditCardDetail.setFailedcardExpireDate(paymentInfoModel.getValidToMonth() + "/"
                + paymentInfoModel.getValidToYear());
    }

    private void populateCreditCardDetails(final PaymentInfo paymentInfo, final CreditCardPaymentInfoModel creditCard,
            final PaymentTransactionEntryModel paymentTxnEntryModel) {
        paymentInfo.setPaymentType(PaymentTypeEnum.CREDITCARD);
        paymentInfo.setCardNumber(creditCard.getNumber());
        paymentInfo.setCardType(creditCard.getType().getCode());
        paymentInfo.setCardExpireDate(creditCard.getValidToYear() + "/"
                + creditCard.getValidToMonth());
        paymentInfo.setCardAuthorizedAmount(paymentTxnEntryModel.getAmount());
        paymentInfo.setCardTransactionID(paymentTxnEntryModel.getCode());
        paymentInfo.setCardRRN(paymentTxnEntryModel.getReceiptNo());
    }

    /**
     * 
     * @param address
     * @return billingAddress
     */
    private BillingAddress builderBillingAddress(final AddressModel address) {
        final BillingAddress billingAddress = new BillingAddress();
        billingAddress.setBillingFullName(getFullName(address.getFirstname(), address.getLastname()));
        billingAddress.setBillingFirstName(address.getFirstname());
        billingAddress.setBillingLastName(address.getLastname());
        billingAddress.setBillingAddressLine1(address.getLine1());
        billingAddress.setBillingAddressLine2(address.getLine2());
        billingAddress.setBillingCity(address.getTown());
        billingAddress.setBillingRegion(address.getDistrict());
        //There is a chance country will be null for paypal, if the billing country is NOT Australia
        billingAddress.setBillingCountry(address.getCountry() != null ? address.getCountry().getIsocode()
                : null);
        billingAddress.setBillingPostalCode(address.getPostalcode());
        billingAddress.setBillingPhone(address.getPhone1());
        return billingAddress;
    }

    /**
     * @param paymentInfo
     * @param paymentInfoModel
     * @param paymentTxnEntryModel
     */
    private void populatePaymentDetails(final PaymentInfo paymentInfo, final PaymentInfoModel paymentInfoModel,
            final PaymentTransactionEntryModel paymentTxnEntryModel) {
        if (paymentInfoModel instanceof CreditCardPaymentInfoModel) {
            paymentInfo.setPaymentType(PaymentTypeEnum.CREDITCARD);
            final CreditCardPaymentInfoModel ccPaymentInfoModel = (CreditCardPaymentInfoModel)paymentInfoModel;
            paymentInfo.setCardHolderName(ccPaymentInfoModel.getCcOwner());
            populateCreditCardDetails(paymentInfo, ccPaymentInfoModel, paymentTxnEntryModel);
            paymentInfo.setCardTokenID(ccPaymentInfoModel.getSubscriptionId());
        }
        else if (paymentInfoModel instanceof PaypalPaymentInfoModel) {
            paymentInfo.setPaymentType(PaymentTypeEnum.PAYPAL);
            paymentInfo.setPayPalEmail(((PaypalPaymentInfoModel)paymentInfoModel).getEmailId());
            paymentInfo.setPayPalAmount(paymentTxnEntryModel.getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN));
            paymentInfo.setPayPalStatus(paymentTxnEntryModel.getTransactionStatus());
            paymentInfo.setPayPalTransactionId(paymentTxnEntryModel.getCode());
            paymentInfo.setPayPalRRN(paymentTxnEntryModel.getReceiptNo());
            paymentInfo.setPayPalRequestID(paymentTxnEntryModel.getRequestToken());
        }
        else if (paymentInfoModel instanceof PinPadPaymentInfoModel) {
            paymentInfo.setPaymentType(PaymentTypeEnum.PINPAD);
            final PinPadPaymentInfoModel pinPadPaymentInfoModel = (PinPadPaymentInfoModel)paymentInfoModel;
            paymentInfo.setCardNumber(pinPadPaymentInfoModel.getMaskedCardNumber());
            paymentInfo.setCardAuthorizedAmount(paymentTxnEntryModel.getAmount());
            paymentInfo.setCardTransactionID(paymentTxnEntryModel.getCode());
            paymentInfo.setCardRRN(paymentTxnEntryModel.getReceiptNo());
        }
        else if (paymentInfoModel instanceof IpgPaymentInfoModel) {
            final PaymentInfoModel ipgInfo = paymentTxnEntryModel.getIpgPaymentInfo();
            if (ipgInfo instanceof IpgCreditCardPaymentInfoModel) {
                final IpgCreditCardPaymentInfoModel ipgCreditCard = (IpgCreditCardPaymentInfoModel)ipgInfo;
                populateCreditCardDetails(paymentInfo, ipgCreditCard, paymentTxnEntryModel);
                paymentInfo.setCardTokenID(ipgCreditCard.getToken());
            }
            else if (ipgInfo instanceof IpgGiftCardPaymentInfoModel) {
                populateGiftCardDetails(paymentInfo, (IpgGiftCardPaymentInfoModel)ipgInfo,
                        paymentTxnEntryModel);
            }
        }
        else if (paymentInfoModel instanceof AfterpayPaymentInfoModel) {
            final AfterpayPaymentInfoModel afterpayInfoModel = (AfterpayPaymentInfoModel)paymentInfoModel;
            paymentInfo.setPaymentType(PaymentTypeEnum.AFTERPAY);
            paymentInfo.setAfterPayStatus(paymentTxnEntryModel.getTransactionStatus());

            paymentInfo.setAfterPayOrderId(afterpayInfoModel.getAfterpayOrderId());
            paymentInfo.setAfterPayRefundId(afterpayInfoModel.getAfterpayRefundId());
            paymentInfo.setAfterPayAmount(paymentTxnEntryModel.getAmount());

        } else if (paymentInfoModel instanceof ZippayPaymentInfoModel) {
            final ZippayPaymentInfoModel paymentinfoModel = (ZippayPaymentInfoModel)paymentInfoModel;
            paymentInfo.setPaymentType(PaymentTypeEnum.ZIPPAY);

            if(hasPaymentInfo(paymentTxnEntryModel)) {
                final ZippayPaymentInfoModel pinfoModelTransaction = (ZippayPaymentInfoModel) paymentTxnEntryModel.getPaymentTransaction().getInfo();
                if(StringUtils.isNotEmpty(pinfoModelTransaction.getRefundId())) {
                    paymentInfo.setZipRefundId(pinfoModelTransaction.getRefundId());
                }
            }
            paymentInfo.setZipOrderID(paymentinfoModel.getReceiptNo());
            paymentInfo.setZipStatus(paymentTxnEntryModel.getTransactionStatus());
            paymentInfo.setZipAmount(paymentTxnEntryModel.getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN));

        }
    }

    private boolean hasPaymentInfo(final PaymentTransactionEntryModel paymentTxnEntryModel) {
        return paymentTxnEntryModel != null && paymentTxnEntryModel.getPaymentTransaction()!=null && paymentTxnEntryModel.getPaymentTransaction().getInfo() != null;
    }


    /**
     * 
     * @param paymentInfoModel
     * @param orderModel
     * @return @link CreditCardDetail}
     */
    public CreditCardDetail populateCreditCardOrPinPadDetails(final PaymentInfoModel paymentInfoModel,
            final AddressModel billingAddress, final OrderModel orderModel) {
        if (paymentInfoModel == null) {
            return null;
        }
        final CreditCardDetail creditCardDetail = new CreditCardDetail();
        if (includeBillingAddress(orderModel, billingAddress)) {
            creditCardDetail.setFailedbillingFullName(billingAddress.getFirstname() + " "
                    + billingAddress.getLastname());
            creditCardDetail.setFailedbillingFirstName(billingAddress.getFirstname());
            creditCardDetail.setFailedbillingLastName(billingAddress.getLastname());
            creditCardDetail.setFailedbillingAddressLine1(billingAddress.getLine1());
            creditCardDetail.setFailedbillingAddressLine2(billingAddress.getLine2());
            creditCardDetail.setFailedbillingCity(billingAddress.getTown());
            creditCardDetail.setFailedbillingRegion(billingAddress.getDistrict());
            creditCardDetail.setFailedbillingPostalCode(billingAddress.getPostalcode());
            creditCardDetail.setFailedbillingCountry(billingAddress.getCountry().getIsocode());
            creditCardDetail.setFailedbillingPhone(StringUtils.isNotBlank(billingAddress.getPhone1()) ? billingAddress
                    .getPhone1()
                    : billingAddress.getCellphone());
        }
        if (paymentInfoModel instanceof CreditCardPaymentInfoModel) {
            populateCreditCardSpecificDetailFromPaymentInfo(creditCardDetail,
                    (CreditCardPaymentInfoModel)paymentInfoModel);
        }
        else if (paymentInfoModel instanceof PinPadPaymentInfoModel) {
            populatePinPadSpecificDetailFromPaymentInfo(creditCardDetail, (PinPadPaymentInfoModel)paymentInfoModel);
        }
        return creditCardDetail;

    }

    private void populateGiftCardDetails(final PaymentInfo paymentInfo, final IpgGiftCardPaymentInfoModel giftCard,
            final PaymentTransactionEntryModel paymentTxnEntryModel) {
        paymentInfo.setPaymentType(PaymentTypeEnum.GIFTCARD);
        paymentInfo.setCardNumber(giftCard.getMaskedNumber());
        paymentInfo.setCardType("giftcard");
        paymentInfo.setCardAuthorizedAmount(paymentTxnEntryModel.getAmount());
        paymentInfo.setCardTransactionID(paymentTxnEntryModel.getCode());
        paymentInfo.setCardRRN(paymentTxnEntryModel.getReceiptNo());
    }

    /**
     * @param paymentInfoModel
     * @return {@link PayPalDetail}
     */
    public PayPalDetail getPayPalDetailFromPaymentInfo(final PaypalPaymentInfoModel paymentInfoModel,
            final PaymentTransactionEntryModel paymentTxnEntryModel) {
        final PayPalDetail payPalDetail = new PayPalDetail();
        payPalDetail.setFailedpayPalEmail(paymentInfoModel.getPayerId());
        payPalDetail.setFailedpayPalRequestID(paymentTxnEntryModel.getRequestId());
        payPalDetail.setFailedpayPalStatus(paymentTxnEntryModel.getTransactionStatus());

        final DecimalFormat df = new DecimalFormat("#.00");
        payPalDetail.setFailedpayPalAmount(df.format(paymentTxnEntryModel.getAmount()));

        return payPalDetail;
    }


    /**
     * @param paymentInfoModel
     * @return {@link AfterpayDetail}
     */
    public AfterpayDetail getAfterpayDetailFromPaymentInfo(final AfterpayPaymentInfoModel paymentInfoModel,
            final PaymentTransactionEntryModel paymentTxnEntryModel) {
        final AfterpayDetail afterpayDetail = new AfterpayDetail();
        afterpayDetail.setFailedAfterpayRequestId(paymentInfoModel.getAfterpayOrderId());
        afterpayDetail.setFailedAfterpayStatus(paymentTxnEntryModel.getTransactionStatus());

        final DecimalFormat df = new DecimalFormat("#.00");
        afterpayDetail.setFailedAfterpayAmount(df.format(paymentTxnEntryModel.getAmount()));

        return afterpayDetail;
    }


    public Member getMemberInfo(final TargetCustomerModel customer, final String tmd, final String flybuys) {
        if (customer == null) {
            return null;
        }

        final Member member = new Member();
        member.setMemberID(customer.getCustomerID());
        member.setMembershipDate(customer.getCreationtime());
        member.setMemberFullName(customer.getDisplayName());
        member.setMemberFirstName(customer.getFirstname());
        member.setMemberLastName(customer.getLastname());
        member.setMemberEmail(customer.getContactEmail());
        member.setMemberPhone(customer.getPhoneNumber() != null ? customer.getPhoneNumber() : customer
                .getMobileNumber());
        member.setTargetStaffDiscountCard(tmd);
        member.setLoyaltyNumber(flybuys);
        member.setInternetMemberRegisteredStatus(CustomerType.GUEST.equals(customer.getType())
                ? "Guest"
                : "Registered");

        return member;
    }

    public OrderDetail getOrderDetails(final List<AbstractOrderEntryModel> orderEntries) {
        if (CollectionUtils.isEmpty(orderEntries)) {
            return null;
        }

        final OrderDetail orderDetail = new OrderDetail();
        final List<OrderItem> items = new ArrayList<>();
        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            items.add(getOrderItemFromOrderEntry(orderEntry, orderEntry.getQuantity() != null ? orderEntry
                    .getQuantity().longValue() : 0l));
        }

        orderDetail.setOrderItem(items);

        return orderDetail;
    }

    public OrderDetail getOrderDetailsFromCancelReturnRecord(
            final Collection<OrderEntryModificationRecordEntryModel> oemre) {
        if (CollectionUtils.isEmpty(oemre)) {
            return null;
        }

        final OrderDetail orderDetail = new OrderDetail();
        final List<OrderItem> items = new ArrayList<>();
        for (final OrderEntryModificationRecordEntryModel entry : oemre) {
            long quantity = 0l;
            if (entry instanceof OrderEntryCancelRecordEntryModel) {
                quantity = ((OrderEntryCancelRecordEntryModel)entry).getCancelledQuantity() != null
                        ? ((OrderEntryCancelRecordEntryModel)entry).getCancelledQuantity().longValue()
                        : 0l;
            }
            else if (entry instanceof OrderEntryReturnRecordEntryModel) {
                quantity = ((OrderEntryReturnRecordEntryModel)entry).getReturnedQuantity() != null
                        ? ((OrderEntryReturnRecordEntryModel)entry).getReturnedQuantity().longValue()
                        : 0l;
            }
            items.add(getOrderItemFromOrderEntry(entry.getOrderEntry(), quantity));
        }

        orderDetail.setOrderItem(items);

        return orderDetail;
    }

    private OrderItem getOrderItemFromOrderEntry(final AbstractOrderEntryModel orderEntryModel,
            final long quantity) {
        final AbstractTargetVariantProductModel product = (AbstractTargetVariantProductModel)orderEntryModel
                .getProduct();

        final OrderItem item = new OrderItem();
        item.setItemNumber(product != null ? product.getCode() : "");
        item.setItemDescription(product != null ? product.getName() : "");
        item.setUnitPrice(BigDecimal.valueOf(orderEntryModel.getBasePrice().doubleValue()));
        item.setBrandname(product != null && product.getBrand() != null ? product.getBrand().getName() : "");
        item.setQuantity(quantity);
        item.setCategory(product != null && product.getPrimarySuperCategory() != null ? product
                .getPrimarySuperCategory().getName()
                : null);
        item.setTax(getItemTax(orderEntryModel));
        item.setDepartmentNumber(getMerchDepartmentNumber(product));
        final List<String> giftRecipientEmailAdress = populateRecipientEmailAddress(orderEntryModel);
        if (CollectionUtils.isNotEmpty(giftRecipientEmailAdress)) {
            item.setRecipientemailAddress(giftRecipientEmailAdress);
        }
        return item;
    }

    private int getMerchDepartmentNumber(final AbstractTargetVariantProductModel product) {
        if (product == null) {
            return 0;
        }
        if (product.getMerchDepartment() == null) {
            return product.getDepartment() == null ? 0 : product.getDepartment().intValue();
        }
        return NumberUtils.toInt(product.getMerchDepartment().getCode());
    }


    public BigDecimal getItemTax(final AbstractOrderEntryModel orderEntry) {
        Assert.notNull(orderEntry, "orderEntry cannot be null");
        // Assume the TaxValues have been applied
        BigDecimal total = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN);
        for (final TaxValue tax : orderEntry.getTaxValues()) {
            total = total.add(BigDecimal.valueOf(tax.getAppliedValue()));
        }

        return total;
    }

    public ShippingInfo getShippingInfo(final AddressModel deliveryAddress, final DeliveryModeModel deliveryMode) {
        if (deliveryAddress == null) {
            return null;
        }

        final ShippingInfo shippingInfo = new ShippingInfo();
        shippingInfo.setShippingFullName(getFullName(deliveryAddress.getFirstname(), deliveryAddress.getLastname()));
        shippingInfo.setShippingFirstName(deliveryAddress.getFirstname());
        shippingInfo.setShippingLastName(deliveryAddress.getLastname());
        shippingInfo.setShippingAddressLine1(deliveryAddress.getLine1());
        shippingInfo.setShippingAddressLine2(deliveryAddress.getLine2());
        shippingInfo.setShippingCity(deliveryAddress.getTown());
        shippingInfo.setShippingRegion(deliveryAddress.getDistrict());
        shippingInfo.setShippingPostalCode(deliveryAddress.getPostalcode());
        shippingInfo.setShippingCountry(deliveryAddress.getCountry().getIsocode());
        shippingInfo.setShippingPhone(deliveryAddress.getPhone1() != null ? deliveryAddress.getPhone1()
                : deliveryAddress.getCellphone());
        shippingInfo.setShippingMethod(deliveryMode != null ? deliveryMode.getName() : "");

        return shippingInfo;
    }

    public String getFullName(final String firstName, final String lastName) {
        if (StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName)) {
            return null;
        }

        final StringBuilder fullName = new StringBuilder(StringUtils.isNotBlank(firstName) ? firstName : "");
        if (StringUtils.isNotBlank(lastName)) {
            if (StringUtils.isNotBlank(firstName)) {
                fullName.append(" ");
            }
            fullName.append(lastName);
        }

        return fullName.toString();
    }


    /**
     * Return the voucher code for the given order if there is one. If more than one then just returns the first since
     * we assume only one voucher per order.
     * 
     * @param orderModel
     * @return voucher code or null if none
     */
    protected String getVoucherCode(final OrderModel orderModel) {

        final Collection<String> voucherCodes = voucherService.getAppliedVoucherCodes(orderModel);
        if (CollectionUtils.isEmpty(voucherCodes)) {
            return null;
        }

        return voucherCodes.iterator().next();
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    /**
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    /**
     * @param featureSwitchService
     *            the featureSwitchService to set
     */
    @Required
    public void setFeatureSwitchService(final TargetFeatureSwitchService featureSwitchService) {
        this.featureSwitchService = featureSwitchService;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * Populates gift card recipient email
     * 
     * @param orderEntry
     * @return List
     */
    protected List<String> populateRecipientEmailAddress(final AbstractOrderEntryModel orderEntry) {
        final List<GiftRecipientModel> giftRecipientList = orderEntry.getGiftRecipients();
        if (CollectionUtils.isNotEmpty(giftRecipientList)) {
            final List<String> recipientEmailAddresses = new ArrayList<>();
            for (final GiftRecipientModel gitRecipient : giftRecipientList) {
                recipientEmailAddresses.add(gitRecipient.getEmail());
            }
            return recipientEmailAddresses;
        }
        return null;
    }
}
