/**
 * 
 */
package au.com.target.tgtfraud.provider;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.impl.FraudSymptom;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import au.com.target.tgtfraud.TargetFraudServiceProvider;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;
import au.com.target.tgtfraud.provider.data.OrderTypeEnum;
import au.com.target.tgtfraud.provider.data.Result;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgtfraud.provider.util.AccertifyUtil;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtutility.logging.JaxbLogger;


/**
 * Implementation of fraud check service using Accertify.<br/>
 * 
 * The FraudCheckServiceUnavailableException should be thrown if Accertify is not available.
 * 
 * @author steveb, vmuthura
 * 
 */
public class AccertifyFraudServiceProvider implements TargetFraudServiceProvider {
    private static final Logger LOG = Logger.getLogger(AccertifyFraudServiceProvider.class);
    private RestOperations restOperations;
    private String providerName;
    private String url;
    private AccertifyUtil accertifyUtil;

    /**
     * @param providerName
     *            the providerName to set
     */
    @Required
    public void setProviderName(final String providerName) {
        this.providerName = providerName;
    }

    /**
     * @param url
     *            the url to set
     */
    @Required
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @param restOperations
     *            the restOperations to set
     */
    @Required
    public void setRestOperations(final RestOperations restOperations) {
        this.restOperations = restOperations;
    }

    @Override
    public String getProviderName() {
        return providerName;
    }

    @Override
    public FraudServiceResponse recognizeOrderFraudSymptoms(final AbstractOrderModel abstractordermodel) {
        Assert.notNull(abstractordermodel, "OrderModel is null");

        final PurchaseOptionConfigModel purchaseOptionConfig = ((OrderModel)abstractordermodel)
                .getPurchaseOptionConfig();

        OrderTypeEnum orderType = purchaseOptionConfig != null
                && purchaseOptionConfig.getAllowPaymentDues().booleanValue()
                ? OrderTypeEnum.LAYBY_SALE
                : OrderTypeEnum.SALE;
        if (BooleanUtils.isTrue(abstractordermodel.getContainsPreOrderItems())) {
            orderType = OrderTypeEnum.PREORDER;
        }
        final Transaction transaction = accertifyUtil
                .buildTransaction((OrderModel)abstractordermodel, null, orderType,
                        null);

        return performFraudCheck(transaction);
    }

    @Override
    public FraudServiceResponse recognizeUserActivitySymptoms(final UserModel usermodel) {
        throw new UnsupportedOperationException("This api is not supported by accertify");
    }

    @Override
    public FraudServiceResponse submitPayment(final OrderModel orderModel)
            throws FraudCheckServiceUnavailableException {
        Assert.notNull(orderModel, "orderModel is null");
        final Transaction transaction = accertifyUtil
                .buildTransaction(orderModel, null, OrderTypeEnum.LAYBY_PAYMENT, null);
        return performFraudCheck(transaction);
    }

    @Override
    public FraudServiceResponse submitRefund(final OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundEntries)
            throws FraudCheckServiceUnavailableException {
        Assert.notNull(orderModificationRecordEntryModel, "OrderModificationRecordEntryModel is null");
        if (CollectionUtils.isEmpty(refundEntries)) {
            //this can happen for manual refund stand alone.
            return null;
        }

        final OrderModel originalOrder = orderModificationRecordEntryModel.getModificationRecord().getOrder();

        final Transaction transaction = accertifyUtil
                .buildTransaction(originalOrder, orderModificationRecordEntryModel, OrderTypeEnum.REFUND,
                        refundEntries);

        JaxbLogger.logXml(LOG, transaction);
        return performFraudCheck(transaction);
    }

    protected FraudServiceResponse performFraudCheck(final Transaction transaction)
            throws FraudCheckServiceUnavailableException {
        JaxbLogger.logXml(LOG, transaction);

        try {
            final Result response = restOperations.postForObject(url, transaction, Result.class);
            // Debug logging the xml response
            JaxbLogger.logXml(LOG, response);

            final TargetFraudServiceResponse fraudServiceResponse = new TargetFraudServiceResponse(getProviderName());
            if (!StringUtils.isBlank(response.getRulesTripped())) {
                final String[] rulesTripped = response.getRulesTripped().split(";");
                for (final String rule : rulesTripped) {
                    final String[] components = rule.split(":");
                    if (components.length == 3) {
                        final double score = Double.parseDouble(components[2]);
                        fraudServiceResponse.addSymptom(new FraudSymptom(components[1], score, components[0]));
                    }
                }
            }
            fraudServiceResponse.setRecommendation(response.getRecommendation());
            return fraudServiceResponse;
        }
        catch (final RestClientException e) {
            LOG.error("ERR-ACCERTIFY-SERVICEEXCEPTION ", e);
            throw new FraudCheckServiceUnavailableException("Failed to perform accertify check: ", e);
        }
    }

    /**
     * @param accertifyUtil
     *            the accertifyUtil to set
     */
    @Required
    public void setAccertifyUtil(final AccertifyUtil accertifyUtil) {
        this.accertifyUtil = accertifyUtil;
    }
}
