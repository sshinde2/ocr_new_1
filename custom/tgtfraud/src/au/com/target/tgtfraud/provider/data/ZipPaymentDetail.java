/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author hsing179
 *
 */
@XmlRootElement
@XmlType(propOrder = {
        "failedRequestId",
        "failedAmount",
        "failedStatus",
})
public class ZipPaymentDetail {

    private String failedRequestId;
    private String failedAmount;
    private String failedStatus;

    /**
     *
     * @return
     */
    public String getFailedRequestId() {
        return failedRequestId;
    }

    /**
     *
     * @param failedRequestId
     */
    public void setFailedRequestId(final String failedRequestId) {
        this.failedRequestId = failedRequestId;
    }

    /**
     *
     * @return
     */
    public String getFailedAmount() {
        return failedAmount;
    }

    /**
     *
     * @param failedAmount
     */
    public void setFailedAmount(final String failedAmount) {
        this.failedAmount = failedAmount;
    }

    /**
     *
     * @return
     */
    public String getFailedStatus() {
        return failedStatus;
    }

    /**
     *
     * @param failedStatus
     */
    public void setFailedStatus(final String failedStatus) {
        this.failedStatus = failedStatus;
    }
}
