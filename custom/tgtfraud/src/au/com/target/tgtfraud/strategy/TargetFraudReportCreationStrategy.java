/**
 * 
 */
package au.com.target.tgtfraud.strategy;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;


/**
 * @author mjanarth
 * 
 */
public interface TargetFraudReportCreationStrategy {

    /**
     * Creates the fraud report for the order
     * 
     * @param response
     * @param orderModel
     * @param status
     * @return FraudReportModel
     */
    FraudReportModel createFraudReport(FraudServiceResponse response, OrderModel orderModel, FraudStatus status);

}
