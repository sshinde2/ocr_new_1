/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import org.springframework.util.Assert;

import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;


/**
 * Send to fraud check service eg Accertify for a return
 * 
 */
public class SendReturnRefundForFraudCheckAction extends AbstractSendDetailsForFraudCheckAction {

    @Override
    protected FraudServiceResponse sendDetails(final OrderProcessModel process)
            throws FraudCheckServiceUnavailableException {

        final OrderReturnRecordEntryModel returnRequestModel = getOrderProcessParameterHelper()
                .getReturnRequest(process);
        Assert.notNull(returnRequestModel, "returnRequestModel may not be null");

        try {
            return targetFraudService.submitRefund(getProviderName(), returnRequestModel,
                    getOrderProcessParameterHelper().getRefundPaymentEntryList(process));
        }
        catch (final FraudCheckServiceUnavailableException e) {
            throw e;
        }
        //CHECKSTYLE:OFF really don't want fraud check to terminate business process
        catch (final Throwable t) {
            throw new FraudCheckServiceUnavailableException("Unexpected error processing fraud verification", t);
        }
        //CHECKSTYLE:ON
    }

}
