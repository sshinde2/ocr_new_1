/**
 * 
 */
package au.com.target.tgtfraud;

/**
 * Service for auto accepting orders
 * 
 */
public interface AutoAcceptService {

    /**
     * trigger accept business process for all orders in review status for more then 72 hours
     */
    void triggerAcceptProcessForAutoAcceptReviewOrders();


}
