/**
 * 
 */
package au.com.target.tgtfraud.exception;

/**
 * Exception thrown when the fraud check service is not available
 * 
 */
public class FraudCheckServiceUnavailableException extends RuntimeException {

    /**
     * @param message
     */
    public FraudCheckServiceUnavailableException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public FraudCheckServiceUnavailableException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
