/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;
import au.com.target.tgtcore.jobs.util.CronJobUtilityService;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.util.TargetProductMockHelper;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetBatchUpdateCronJobModel;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateEbayCNCDeliveryModeCronJobTest {

    @Mock
    private TargetUpdateProductDao targetUpdateProductDao;

    @Mock
    private ModelService modelService;


    @InjectMocks
    private final UpdateEbayCNCDeliveryModeCronJob updateEbayCNCDeliveryModeCronJob = new UpdateEbayCNCDeliveryModeCronJob();

    private final TargetZoneDeliveryModeModel cncDeliverMode = new TargetZoneDeliveryModeModel();
    private final TargetZoneDeliveryModeModel ebaycncDeliverMode = new TargetZoneDeliveryModeModel();

    @Mock
    private CronJobUtilityService cronJobUtilityService;

    @Mock
    private CatalogVersionService catalogVersionService;

    private final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

    private TargetProductMockHelper targetProductMockHelper;

    private final List<TargetProductModel> targetProductModelList = new ArrayList<>();

    @Mock
    private TargetDeliveryService targetDeliveryService;


    @Before
    public void setUp() throws Exception {
        targetProductMockHelper = new TargetProductMockHelper(catalogVersionModel);
        TargetProductModel tgtProductModel = null;
        for (int i = 0; i < 50; i++) {
            tgtProductModel = targetProductMockHelper.createTargetProduct(Integer.valueOf(457),
                    "BP" + i, 0, 2, null,
                    Boolean.TRUE,
                    Boolean.TRUE);
            final Set<DeliveryModeModel> deliverModes = new HashSet<>();
            deliverModes.add(cncDeliverMode);
            tgtProductModel.setDeliveryModes(deliverModes);
            targetProductModelList.add(tgtProductModel);
        }
        when(targetDeliveryService
                .getDeliveryModeForCode(Mockito.anyString())).thenReturn(ebaycncDeliverMode);

    }

    @Test
    public final void testIdealScenario() {
        final TargetBatchUpdateCronJobModel cronJobModel = Mockito.mock(TargetBatchUpdateCronJobModel.class);
        when(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).thenReturn(catalogVersionModel);
        when(Integer.valueOf(cronJobModel.getBatchSize())).thenReturn(Integer.valueOf(50));
        when(
                targetUpdateProductDao.getCncAndEbayAvailableProducts("click-and-collect", "ebay-click-and-collect",
                        50, catalogVersionModel)).thenReturn(
                targetProductModelList, targetProductModelList, null);
        when(cronJobModel.getRequestAbort()).thenReturn(Boolean.FALSE);

        final TargetBatchUpdateCronJobModel cronJob = Mockito.mock(TargetBatchUpdateCronJobModel.class);
        when(cronJobUtilityService.getRunningSyncJob()).thenReturn(cronJob);
        final JobModel jobModel = Mockito.mock(JobModel.class);
        when(cronJob.getJob()).thenReturn(jobModel);
        when(jobModel.getCode()).thenReturn("Sync");

        final PerformResult performResult = updateEbayCNCDeliveryModeCronJob.perform(cronJobModel);
        Assert.assertEquals(Integer.valueOf(2),
                Integer.valueOf(targetProductModelList.get(0).getDeliveryModes().size()));
        Assert.assertTrue(targetProductModelList.get(0).getDeliveryModes().contains(cncDeliverMode));
        Assert.assertTrue(targetProductModelList.get(0).getDeliveryModes().contains(ebaycncDeliverMode));
        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());
        Assert.assertEquals(performResult.getStatus().getCode(), CronJobStatus.FINISHED.getCode());
    }

    @Test
    public final void testAbortScenario() throws CronJobAbortException {
        final TargetBatchUpdateCronJobModel cronJobModel = Mockito.mock(TargetBatchUpdateCronJobModel.class);
        when(Integer.valueOf(cronJobModel.getBatchSize())).thenReturn(Integer.valueOf(50));
        when(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).thenReturn(catalogVersionModel);
        when(
                targetUpdateProductDao.getCncAndEbayAvailableProducts("click-and-collect", "ebay-click-and-collect",
                        50, catalogVersionModel)).thenReturn(
                targetProductModelList, targetProductModelList, null);

        Mockito.doThrow(new CronJobAbortException("Aborted")).when(cronJobUtilityService)
                .checkIfCurrentJobIsAborted(cronJobModel);

        final PerformResult performResult = updateEbayCNCDeliveryModeCronJob.perform(cronJobModel);

        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.UNKNOWN.getCode());
        Assert.assertEquals(performResult.getStatus().getCode(), CronJobStatus.ABORTED.getCode());
    }
}
