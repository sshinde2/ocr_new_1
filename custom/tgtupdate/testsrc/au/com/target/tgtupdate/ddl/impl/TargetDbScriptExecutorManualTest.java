/**
 * 
 */
package au.com.target.tgtupdate.ddl.impl;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Test;

import au.com.target.tgtupdate.ddl.TargetDbScriptExecutor;



/**
 * @author htan3
 *
 */
@ManualTest
public class TargetDbScriptExecutorManualTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetDbScriptExecutor targetDbScriptExecutor;

    @Test
    public void testCreateIndex() {
        targetDbScriptExecutor.executeDdl("/tgtupdate/db/custom-index.sql");
    }

}
