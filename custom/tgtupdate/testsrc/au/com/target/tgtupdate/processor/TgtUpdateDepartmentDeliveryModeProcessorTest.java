/**
 * 
 */
package au.com.target.tgtupdate.processor;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductDeliveryModeService;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.util.TargetProductMockHelper;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtUpdateDepartmentDeliveryModeProcessorTest {


    @Mock
    private CategoryService categoryService;
    @Mock
    private ModelService modelService;

    @Mock
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Mock
    private TargetProductDepartmentService targetProductDepartmentService;

    @Mock
    private TargetProductDeliveryModeService targetProductDeliveryModeService;

    @Captor
    private ArgumentCaptor<List<TargetProductModel>> argCaptor;

    @Captor
    private ArgumentCaptor<TargetMerchDepartmentModel> targetMerchDepartmentModelCaptor;


    @Captor
    private ArgumentCaptor<TargetProductModel> tgtProductModelCaptor;

    @InjectMocks
    private final TgtUpdateDepartmentDeliveryModeProcessor tgtUpdateDepartmentDeliveryModeProcessor = new TgtUpdateDepartmentDeliveryModeProcessor();

    private final List<TargetProductModel> targetProductModelList = new ArrayList<>();

    private final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

    private TargetProductMockHelper targetProductMockHelper;


    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        targetProductMockHelper = new TargetProductMockHelper(catalogVersionModel);
        when(zoneDeliveryModeService.getDeliveryModeForCode("express-delivery")).thenReturn(
                targetProductMockHelper.createDeliveryModeModel("express-delivery"));
    }

    /**
     * Test method for
     * {@link au.com.target.tgtupdate.processor.TgtUpdateDepartmentDeliveryModeProcessor#processDepartmentDeliveryModeUpdate(java.util.List)}
     * .
     */
    @SuppressWarnings("boxing")
    @Test
    public final void testUpdateDepartmentToTargetProduct() {


        final TargetProductModel tgtProductModel = targetProductMockHelper.createTargetProduct(Integer.valueOf(457),
                "DM1", 0, 2, null,
                Boolean.TRUE,
                Boolean.TRUE);

        final TargetMerchDepartmentModel categoryModel = targetProductMockHelper.createDepartmentCategoryModel(
                "457", Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                categoryModel);
        when(targetProductDepartmentService.processMerchDeparment(tgtProductModel, categoryModel))
                .thenReturn(true);
        when(targetProductDeliveryModeService.processExpressDeliveryModeUpdation(tgtProductModel, categoryModel))
                .thenReturn(true);

        when(targetProductDepartmentService.getDepartmentCategoryModel(catalogVersionModel, Integer.valueOf(457)))
                .thenReturn(categoryModel);

        targetProductModelList.add(tgtProductModel);

        tgtUpdateDepartmentDeliveryModeProcessor.processDepartmentDeliveryModeUpdate(this.targetProductModelList);

        Mockito.verify(targetProductDepartmentService).processMerchDeparment(
                tgtProductModel, categoryModel);

        Mockito.verify(targetProductDeliveryModeService).processExpressDeliveryModeUpdation(
                tgtProductModel, categoryModel);



        Mockito.verify(targetProductDepartmentService).processMerchDeparment(tgtProductModelCaptor.capture(),
                targetMerchDepartmentModelCaptor.capture());

        final TargetMerchDepartmentModel departmentToBeSaved = targetMerchDepartmentModelCaptor.getValue();
        Assert.assertEquals("457", departmentToBeSaved.getCode());

        Mockito.verify(modelService).saveAll(argCaptor.capture());

        final List<TargetProductModel> targetProductModelListResult = argCaptor.getValue();

        Assert.assertEquals(targetProductModelListResult.size(), 1);



    }

    /**
     * Test method for
     * {@link au.com.target.tgtupdate.processor.TgtUpdateDepartmentDeliveryModeProcessor#processDepartmentDeliveryModeUpdate(java.util.List)}
     * .
     */
    @SuppressWarnings("boxing")
    @Test
    public final void testDefaultDepartmentAssignment() {

        //variant department 2000 does not exist.
        final TargetProductModel tgtProductModel = targetProductMockHelper.createTargetProduct(Integer.valueOf(2000),
                "DM1", 0, 2, null,
                Boolean.TRUE,
                Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "2000")).thenReturn(
                null);

        final TargetMerchDepartmentModel categoryModel = targetProductMockHelper.createDepartmentCategoryModel(
                "999", Boolean.FALSE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "999")).thenReturn(
                categoryModel);
        when(targetProductDepartmentService.processMerchDeparment(tgtProductModel, categoryModel))
                .thenReturn(true);

        when(targetProductDeliveryModeService.processExpressDeliveryModeUpdation(tgtProductModel, categoryModel))
                .thenReturn(true);

        when(targetProductDepartmentService.getDepartmentCategoryModel(catalogVersionModel, Integer.valueOf(999)))
                .thenReturn(categoryModel);

        tgtUpdateDepartmentDeliveryModeProcessor.setDefaultDepartment(Integer.valueOf(999));

        targetProductModelList.add(tgtProductModel);

        tgtUpdateDepartmentDeliveryModeProcessor.processDepartmentDeliveryModeUpdate(this.targetProductModelList);

        Mockito.verify(targetProductDepartmentService).processMerchDeparment(
                tgtProductModel, categoryModel);

        Mockito.verify(targetProductDeliveryModeService).processExpressDeliveryModeUpdation(
                tgtProductModel, categoryModel);


        Mockito.verify(targetProductDepartmentService).processMerchDeparment(tgtProductModelCaptor.capture(),
                targetMerchDepartmentModelCaptor.capture());


        final TargetMerchDepartmentModel departmentToBeSaved = targetMerchDepartmentModelCaptor.getValue();
        Assert.assertEquals("999", departmentToBeSaved.getCode());

        Mockito.verify(modelService).saveAll(argCaptor.capture());

        final List<TargetProductModel> targetProductModelListResult = argCaptor.getValue();



        Assert.assertEquals(targetProductModelListResult.size(), 1);




    }



}
