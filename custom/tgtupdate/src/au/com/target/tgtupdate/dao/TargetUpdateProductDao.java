/**
 * 
 */
package au.com.target.tgtupdate.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;
import java.util.Set;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author ragarwa3
 *
 */
public interface TargetUpdateProductDao {

    /**
     * Get batch of products with empty Online From date
     * 
     * @return List<TargetColourVariantProductModel>
     */
    List<TargetColourVariantProductModel> getColourVariantsWithNullOnlineFromDate();

    /**
     * Retrieve products available on ebay and available for web cnc and exclude which are already available for ebayCNC
     * 
     * @param includedDeliveryMode
     * @param excludedDeliveryMode
     * @param batchSize
     * @return List<TargetProductModel>
     */
    List<TargetProductModel> getCncAndEbayAvailableProducts(final String includedDeliveryMode,
            final String excludedDeliveryMode, final int batchSize, final CatalogVersionModel catalogVersion);

    /**
     * Find size variants Which does not have an association with the product size.
     * 
     * @param codes
     * @param catalogVersion
     * @return Map of TargetSizeVariantModel and code
     */
    List<TargetSizeVariantProductModel> findSizeVariantsWhichDoesNotHaveProductSizeAssociation(Set<String> codes,
            final CatalogVersionModel catalogVersion);
}
