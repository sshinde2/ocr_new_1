/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SendToWarehouseRequest DTO
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SendToWarehouseRequest {
    private ConsignmentDTO consignment;

    /**
     * @return the consignment
     */
    public ConsignmentDTO getConsignment() {
        return consignment;
    }

    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final ConsignmentDTO consignment) {
        this.consignment = consignment;
    }

}
