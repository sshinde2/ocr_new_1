/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto;

/**
 * Base response DTO.
 * 
 * @author jjayawa1
 *
 */
public class BaseResponseDTO {
    private boolean success;
    private IntegrationError errorType;
    private String errorCode;
    private String errorValue;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the errorType
     */
    public IntegrationError getErrorType() {
        return errorType;
    }

    /**
     * @param errorType
     *            the errorType to set
     */
    public void setErrorType(final IntegrationError errorType) {
        this.errorType = errorType;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorValue
     */
    public String getErrorValue() {
        return errorValue;
    }

    /**
     * @param errorValue
     *            the errorValue to set
     */
    public void setErrorValue(final String errorValue) {
        this.errorValue = errorValue;
    }
}
