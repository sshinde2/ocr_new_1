/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;


/**
 * ConsignmentEntryModel to DTO converter.
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseConsignmentEntryConverter implements
        TargetConverter<ConsignmentEntryModel, ConsignmentEntryDTO> {

    private SendToWarehouseProductConverter sendToWarehouseProductConverter;
    private SendToWarehouseRecipientConverter sendToWarehouseRecipientConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public ConsignmentEntryDTO convert(final ConsignmentEntryModel source) {
        Assert.notNull(source, "ConsignmentEntryModel cannot be null");

        final ConsignmentEntryDTO consignmentEntry = new ConsignmentEntryDTO();
        final AbstractOrderEntryModel orderEntry = source.getOrderEntry();
        if (orderEntry != null) {
            final ProductModel product = orderEntry.getProduct();
            if (product != null) {
                if (product instanceof AbstractTargetVariantProductModel) {
                    consignmentEntry.setProduct(sendToWarehouseProductConverter
                            .convert((AbstractTargetVariantProductModel)product));
                }
            }
            final List<GiftRecipientModel> giftRecipients = orderEntry.getGiftRecipients();
            final List<RecipientDTO> recipients = new ArrayList<>();

            // Gift recipient model only populated for digital gift cards.
            if (CollectionUtils.isNotEmpty(giftRecipients) && ProductUtil.isProductTypeDigital(product)) {
                for (final GiftRecipientModel recipient : giftRecipients) {
                    final RecipientDTO recipientDTO = sendToWarehouseRecipientConverter.convert(recipient);
                    recipients.add(recipientDTO);
                }
            }


            populateRecipientDetailsIfPhysicalGiftcard(orderEntry, product, recipients);

            if (CollectionUtils.isNotEmpty(recipients)) {
                consignmentEntry.setRecipients(recipients);
            }
            consignmentEntry.setQuantity(String.valueOf(orderEntry.getQuantity()));
        }
        return consignmentEntry;
    }

    /**
     * creates recipient details for physical giftcard from delivery address.
     * 
     * @param orderEntry
     * @param product
     * @param recipients
     */
    private void populateRecipientDetailsIfPhysicalGiftcard(final AbstractOrderEntryModel orderEntry,
            final ProductModel product, final List<RecipientDTO> recipients) {

        if (ProductUtil.isProductTypePhysicalGiftcard(product)) {
            final RecipientDTO recipientDTO = new RecipientDTO();
            final AddressModel deliveryAddress = orderEntry.getDeliveryAddress();
            recipientDTO.setFirstName(deliveryAddress.getFirstname());
            recipientDTO.setLastName(deliveryAddress.getLastname());
            recipientDTO.setEmailAddress(null);
            recipientDTO.setStreetAddress1(deliveryAddress.getLine1());
            recipientDTO.setStreetAddress2(deliveryAddress.getLine2());
            recipientDTO.setCity(deliveryAddress.getTown());
            recipientDTO.setState(deliveryAddress.getDistrict());
            recipientDTO.setPostCode(deliveryAddress.getPostalcode());
            recipientDTO.setCountry(deliveryAddress.getCountry().getIsocode());

            recipients.add(recipientDTO);
        }
    }

    /**
     * @param sendToWarehouseProductConverter
     *            the sendToWarehouseProductConverter to set
     */
    @Required
    public void setSendToWarehouseProductConverter(
            final SendToWarehouseProductConverter sendToWarehouseProductConverter) {
        this.sendToWarehouseProductConverter = sendToWarehouseProductConverter;
    }

    /**
     * @param sendToWarehouseRecipientConverter
     *            the sendToWarehouseRecipientConverter to set
     */
    @Required
    public void setSendToWarehouseRecipientConverter(
            final SendToWarehouseRecipientConverter sendToWarehouseRecipientConverter) {
        this.sendToWarehouseRecipientConverter = sendToWarehouseRecipientConverter;
    }

}
