/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto;

/**
 * Label layout options
 * 
 * @author sbryan6
 *
 */
public enum LabelLayout {

    // The enum values will match the strings passed into the API as layout param

    a41pp("A4-1PP"),
    a42pp("A4-2PP"),
    a61pp("THERMAL LABEL-A6-1PP");

    private final String code;

    LabelLayout(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static LabelLayout getDefaultLayout() {
        return LabelLayout.a41pp;
    }

}
