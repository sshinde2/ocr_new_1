/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelCancelableEntriesStrategy;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * @author Rahul
 *
 */
public class TargetOrderCancelCancelableEntriesStrategy extends DefaultOrderCancelCancelableEntriesStrategy {

    private TargetConsignmentService targetConsignmentService;

    @Override
    public Map<AbstractOrderEntryModel, Long> getAllCancelableEntries(final OrderModel order,
            final PrincipalModel requestor) {
        // get the active consignments
        final List<TargetConsignmentModel> consignments = targetConsignmentService
                .getActiveConsignmentsForOrder(order);

        if (CollectionUtils.isEmpty(consignments)) {
            return MapUtils.EMPTY_MAP;
        }

        // all entries are cancel-able from active consignments so add all of them
        final Map cancelableEntries = new HashMap<AbstractOrderEntryModel, Long>();

        for (final TargetConsignmentModel consignment : consignments) {
            for (final ConsignmentEntryModel entryModel : consignment.getConsignmentEntries()) {
                //considering one order entry is assigned to only one active consignment, if quantities can also be splitted than logic needs to be updated in future
                cancelableEntries.put(entryModel.getOrderEntry(), entryModel.getQuantity());
            }
        }

        return cancelableEntries;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

}
