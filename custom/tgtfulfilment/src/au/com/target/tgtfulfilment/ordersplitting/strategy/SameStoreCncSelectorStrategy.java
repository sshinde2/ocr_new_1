/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * Interface for SameStoreCncSelector
 * 
 * @author sbryan6
 *
 */
public interface SameStoreCncSelectorStrategy {


    /**
     * Whether the order group is valid for same store cnc
     * 
     * @param oeg
     * @param cncStoreNumber
     * @return tpos if we can sent to store
     */
    TargetPointOfServiceModel isValidForSameStoreCnc(OrderEntryGroup oeg, Integer cncStoreNumber);
}
