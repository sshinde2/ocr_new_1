/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Interface for a SpecificStoreFulfilmentDenialStrategy
 * 
 * @author sbryan6
 *
 */
public interface SpecificStoreFulfilmentDenialStrategy {

    /**
     * Return true if instore fulfilment is denied by this strategy
     * 
     * @param oeg
     * @param tpos
     * @return DenialResponse
     */
    DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos);

}
