/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author smishra1
 *
 */
public class SpecificStoreProductWeightDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(SpecificStoreProductWeightDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        Assert.notEmpty(oeg, "OrderEntryGroup must not be empty");
        Assert.notNull(tpos, "POS object must not be null");

        final String orderCode = getOegParameterHelper().getOrderCode(oeg);

        if (LOG.isDebugEnabled()) {
            LOG.debug(" START order=" + orderCode + " store=" + tpos.getStoreNumber());
        }


        Double productWeight = null;
        final Double maxAllowedWeight = getStoreFulfilmentCapabilitiesService().getMaxAllowedProductWeightForDelivery(
                tpos);


        if (null == maxAllowedWeight) {
            LOG.warn("No StoreCapablities found : " + orderCode);
            return DenialResponse.createDenied("SystemError");
        }
        if (maxAllowedWeight.equals(Double.valueOf(0))) {
            return DenialResponse.createNotDenied();
        }
        if (CollectionUtils.isEmpty(oeg)) {
            LOG.warn("No OrderEntries present in the Order : " + orderCode);
            return DenialResponse.createDenied("SystemError");
        }
        for (final AbstractOrderEntryModel orderEntryModel : oeg) {
            if (null == orderEntryModel) {
                LOG.warn("No orderEntryModel found. Object returned was null : " + orderCode);
                return DenialResponse.createDenied("SystemError");
            }
            final ProductModel productModel = orderEntryModel.getProduct();
            if (null == productModel || !(productModel instanceof AbstractTargetVariantProductModel)) {
                LOG.warn("No TargetVariantProductModel found. Object returned was null : " + orderCode);
                return DenialResponse.createDenied("SystemError");
            }
            final AbstractTargetVariantProductModel variantProductModel = (AbstractTargetVariantProductModel)productModel;
            if (null == variantProductModel.getProductPackageDimensions()) {
                LOG.warn("Product Package dimentions not set for the product : " + variantProductModel.getCode());
                return DenialResponse.createDenied("NoProductWeightFound : product=" + variantProductModel.getCode());
            }
            productWeight = variantProductModel.getProductPackageDimensions().getWeight();
            if (null == productWeight) {
                return DenialResponse.createDenied("NoProductWeightFound : product=" + variantProductModel.getCode());
            }
            final Set<ProductTypeModel> restrictedProductTypes = getStoreFulfilmentCapabilitiesService()
                    .getRestrictedProductTypes(tpos);
            final ProductTypeModel orderEntryProductType = variantProductModel.getProductType();
            if (CollectionUtils.isNotEmpty(restrictedProductTypes) && null != orderEntryProductType
                    && restrictedProductTypes.contains(orderEntryProductType)) {
                return DenialResponse.createDenied("ProductTypeExcluded : " + "product="
                        + variantProductModel.getCode() + " productType="
                        + orderEntryProductType.getCode());
            }
            if (productWeight.doubleValue() > maxAllowedWeight.doubleValue()) {
                return DenialResponse.createDenied("ProductWeightMoreThanMaxAllowedWeight : " + "product="
                        + variantProductModel.getCode() + " maxAllowedWeight=" + maxAllowedWeight + " productWeight="
                        + productWeight);
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(" END order=" + orderCode + " store=" + tpos.getStoreNumber());
        }
        return DenialResponse.createNotDenied();
    }

}
