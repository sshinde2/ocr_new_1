/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.StoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * @author smishra1
 *
 */
public class StoreSelectorStrategyImpl implements StoreSelectorStrategy {

    private static final RoutingLogger ROUTING_LOG = new RoutingLogger(StoreSelectorStrategyImpl.class);
    private static final Logger LOG = Logger.getLogger(StoreSelectorStrategyImpl.class);

    private NearestStoreSelectorStrategy nearestStoreSelectorStrategy;

    private TargetStoreLocatorService targetStoreLocatorService;

    private List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies;

    private OEGParameterHelper oegParameterHelper;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetSharedConfigService targetSharedConfigService;


    @Override
    public TargetPointOfServiceModel selectStore(final OrderEntryGroup oeg) {

        Assert.notEmpty(oeg, "OEG cannot be empty");

        final AddressModel deliveryAddress = oegParameterHelper.getDeliveryAddress(oeg);
        final String orderCode = oegParameterHelper.getOrderCode(oeg);

        Assert.notNull(deliveryAddress, "Delivery Address cannot be null");
        Assert.isTrue(StringUtils.isNotEmpty(orderCode), "Order Code can't be empty");

        ROUTING_LOG.logDispatchRulesEntryMessage(orderCode);
        List<TargetPointOfServiceModel> targetPointOfServiceList;
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
            // Get all availabel tpos
            targetPointOfServiceList = targetStoreLocatorService.getAllFulfilmentStores();
            if (CollectionUtils.isEmpty(targetPointOfServiceList)) {
                ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode, "NoEnabledPOS");
                return null;
            }
        }
        else {
            // Get the list of tpos with state boundary
            targetPointOfServiceList = targetStoreLocatorService
                    .getAllFulfilmentStoresInState(deliveryAddress.getDistrict().trim());
            if (CollectionUtils.isEmpty(targetPointOfServiceList)) {
                ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode, "NoEnabledPOSInState");
                return null;
            }
        }




        final List<PointOfServiceDistanceData> sortedStores;
        try {

            if (targetFeatureSwitchService
                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
                //filter out by distance
                final double distance = getStoreDistanceLimit();
                sortedStores = nearestStoreSelectorStrategy
                        .getSortedStoreListByDistanceWithDistanceRestriction(targetPointOfServiceList, deliveryAddress,
                                distance);
                if (CollectionUtils.isEmpty(sortedStores)) {
                    ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode, "NoEligiblePOSInDistanceOfRadius:"
                            + distance + "km");
                    return null;
                }

            }
            else {
                sortedStores = nearestStoreSelectorStrategy
                        .getSortedStoreListByDistance(targetPointOfServiceList, deliveryAddress);
                if (CollectionUtils.isEmpty(sortedStores)) {
                    ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode, "NoEnabledPOSInState");
                    return null;
                }
            }
        }
        catch (final GeoServiceWrapperException e) {
            ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode,
                    "CantGeocodeOrderDeliveryAddress, error=" + e.getLocalizedMessage());
            return null;
        }



        // Apply denial strategies to find one that works
        final TargetPointOfServiceModel tpos = applyDenialStrategies(sortedStores, oeg, orderCode);
        if (tpos == null) {
            return null;
        }

        logAssignMessage(oeg, tpos);
        return tpos;
    }


    private void logAssignMessage(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        final TargetZoneDeliveryModeModel deliveryMode = oegParameterHelper.getDeliveryMode(oeg);
        final String orderCode = oegParameterHelper.getOrderCode(oeg);

        final String reason;
        if (deliveryMode != null && BooleanUtils.isTrue(deliveryMode.getIsDeliveryToStore())) {

            // Must be inter-store since we wouldn't be here for same store cnc
            reason = "InterstoreDelivery";
        }
        else {
            reason = "CustomerDelivery";
        }

        ROUTING_LOG.logStoreAssignMessage(orderCode, getStoreNumberForLogging(tpos), reason);
    }


    /**
     * Apply denial strategies to the list of stores.<br/>
     * Return first store to pass rules, or null if none do.
     * 
     * @param sortedStores
     * @param oeg
     * @param orderCode
     * @return TargetPointOfServiceModel or null if none pass rules
     */
    protected TargetPointOfServiceModel applyDenialStrategies(final List<PointOfServiceDistanceData> sortedStores,
            final OrderEntryGroup oeg, final String orderCode) {

        for (final PointOfServiceDistanceData storeData : sortedStores) {

            final PointOfServiceModel pos = storeData.getPointOfService();
            if (pos == null || !(pos instanceof TargetPointOfServiceModel)) {
                LOG.warn("Invalid pos model found");
                continue;
            }

            final TargetPointOfServiceModel tpos = (TargetPointOfServiceModel)pos;
            if (tpos.getStoreNumber() == null) {
                LOG.warn("Invalid pos storenumber found");
                continue;
            }

            if (storePassesDenialStrategies(oeg, tpos, orderCode)) {
                return tpos;
            }

        }

        // No stores pass the denial tests        
        ROUTING_LOG.logRulesExitMessage(orderCode, "NoStoresPassedRules");
        return null;
    }


    /**
     * Return true if the store passes all of the denial strategies.
     * 
     * @param oeg
     * @param tpos
     * @param orderCode
     * @return true if passes
     */
    protected boolean storePassesDenialStrategies(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos,
            final String orderCode) {

        if (CollectionUtils.isNotEmpty(specificStoreFulfilmentDenialStrategies)) {
            for (final SpecificStoreFulfilmentDenialStrategy denialStrategy : specificStoreFulfilmentDenialStrategies) {
                final DenialResponse denialResponse = denialStrategy.isDenied(oeg, tpos);
                if (denialResponse.isDenied()) {
                    ROUTING_LOG.logStoreExcludedMessage(orderCode, getStoreNumberForLogging(tpos),
                            denialResponse.getReason());
                    return false;
                }
            }
        }

        return true;
    }


    private int getStoreNumberForLogging(final TargetPointOfServiceModel tpos) {

        int storeNum = 0;
        if (tpos != null && tpos.getStoreNumber() != null) {
            storeNum = tpos.getStoreNumber().intValue();
        }
        return storeNum;
    }

    private double getStoreDistanceLimit() {
        return targetSharedConfigService.getDouble(TgtfulfilmentConstants.Config.STORE_DISTANCE_LIMIT, 10000d);
    }

    /**
     * @param targetStoreLocatorService
     *            the targetStoreLocatorService to set
     */
    @Required
    public void setTargetStoreLocatorService(final TargetStoreLocatorService targetStoreLocatorService) {
        this.targetStoreLocatorService = targetStoreLocatorService;
    }

    /**
     * @param nearestStoreSelectorStrategy
     *            the nearestStoreSelectorStrategy to set
     */
    @Required
    public void setNearestStoreSelectorStrategy(final NearestStoreSelectorStrategy nearestStoreSelectorStrategy) {
        this.nearestStoreSelectorStrategy = nearestStoreSelectorStrategy;
    }

    /**
     * @param specificStoreFulfilmentDenialStrategies
     *            the specificStoreFulfilmentDenialStrategies to set
     */
    @Required
    public void setSpecificStoreFulfilmentDenialStrategies(
            final List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies) {
        this.specificStoreFulfilmentDenialStrategies = specificStoreFulfilmentDenialStrategies;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }


    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }



}
