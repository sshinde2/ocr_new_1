/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Check if store has a blackout in operation
 * 
 * @author sbryan6
 *
 */
public class SpecificStoreBlackoutFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(SpecificStoreBlackoutFulfilmentDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {


        final String orderCode = getOegParameterHelper().getOrderCode(oeg);
        Assert.notEmpty(oeg, "orderEntryGroup cannot be empty " + orderCode);
        Assert.notNull(tpos, "targetPointOfService cannot be null " + orderCode);

        if (LOG.isDebugEnabled()) {
            LOG.debug(" START order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        if (getStoreFulfilmentCapabilitiesService().isTargetStoreBlackoutExist(tpos)) {

            return DenialResponse.createDenied("StoreBlackOutPeriod");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(" END order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        return DenialResponse.createNotDenied();
    }

}
