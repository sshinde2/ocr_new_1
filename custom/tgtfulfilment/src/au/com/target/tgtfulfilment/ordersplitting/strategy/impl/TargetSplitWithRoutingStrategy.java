/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.SplittingStrategy;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.fulfilmentservice.TicketSender;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.helper.WarehouseHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.ordersplitting.strategy.ConsignmentToWarehouseAssigner;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SameStoreCncSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.StoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


/**
 * Splitting Strategy for fulfillment. This class will split an order into multiple consignment based on the fulfillment
 * conditions.
 * 
 * @author jjayawa1
 *
 */
public class TargetSplitWithRoutingStrategy implements SplittingStrategy {

    private static final RoutingLogger LOG = new RoutingLogger(TargetSplitWithRoutingStrategy.class);

    private static final String FASLINE_FALCON = "fastlineFalcon";

    private WarehouseHelper warehouseHelper;

    private SameStoreCncSelectorStrategy sameStoreCncSelectorStrategy;

    private StoreSelectorStrategy storeSelectorStrategy;

    private OEGParameterHelper oegParameterHelper;

    private WarehouseSelectorStrategy ntlSelectorStrategy;

    private WarehouseSelectorStrategy externalWarehouseSelectorStrategy;

    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    private ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner;

    private TargetStockService targetStockService;

    private TargetWarehouseService targetWarehouseService;

    private TicketSender ticketSender;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private SameStoreCncSplitConsignmentStrategy sameStoreCncSplitConsignmentStrategy;

    private StoreSplitConsignmentStrategy storeSplitConsignmentStrategy;

    private FastlineSplitConsignmentStrategy fastlineSplitConsignmentStrategy;

    private TargetBusinessProcessService targetBusinessProcessService;

    @Override
    public List<OrderEntryGroup> perform(final List<OrderEntryGroup> orderEntryGroups) {
        // order entry group is empty, returning it without doing anything
        if (CollectionUtils.isEmpty(orderEntryGroups)) {
            return orderEntryGroups;
        }

        Assert.isTrue(orderEntryGroups.size() == 1,
                "As this is the only splitting strategy, order must not be already splitted.");

        final OrderEntryGroup originalOEG = orderEntryGroups.get(0);
        oegParameterHelper.populateParamsFromOrder(originalOEG);

        // for rerouting case, the required qty is already set up in ConsignmentReroutingService
        if (oegParameterHelper.getRequiredQty(originalOEG) == null) {
            // must have this step to set the required qty on OEG, stock look up will rely on this
            oegParameterHelper.setRequiredQtyByOrderEntries(originalOEG);
        }

        // set assigned qty before hand on original OEG only for exception handler using the original one as the assigned oeg
        oegParameterHelper.setAssignedQtyByOrderEntries(originalOEG);
        final String orderCode = oegParameterHelper.getOrderCode(originalOEG);
        final Integer cncStoreNumber = oegParameterHelper.getCncStoreNumber(originalOEG);

        final List<OrderEntryGroup> splittedOEGs = new ArrayList<>();
        OrderEntryGroup nonAssignedOegPostSplit = originalOEG;

        // For any exception, handle it and go to default warehouse rather than exiting the place order process
        try {
            LOG.logRulesEntryMessage(orderCode);

            // External warehouses
            nonAssignedOegPostSplit = this.splitExternalWarehouse(orderCode, originalOEG, splittedOEGs);
            if (CollectionUtils.isEmpty(nonAssignedOegPostSplit)) {
                loggingResult(orderCode, "TargetSplitWithRoutingStrategy", originalOEG, splittedOEGs,
                        nonAssignedOegPostSplit);
                return splittedOEGs;
            }

            // In-store fulfilment 
            if (instoreFulfilmentGlobalRulesChecker.areAllGlobalRulesPassed(nonAssignedOegPostSplit)) {

                if (targetFeatureSwitchService.isFeatureEnabled(FASLINE_FALCON)) {
                    //split same store cnc
                    nonAssignedOegPostSplit = this.splitSameStoreCnc(orderCode, splittedOEGs, nonAssignedOegPostSplit);
                    if (CollectionUtils.isEmpty(nonAssignedOegPostSplit)) {
                        loggingResult(orderCode, "TargetSplitWithRoutingStrategy", originalOEG, splittedOEGs,
                                nonAssignedOegPostSplit);
                        return splittedOEGs;
                    }
                    nonAssignedOegPostSplit = this.splitStores(orderCode, splittedOEGs, nonAssignedOegPostSplit);
                    if (CollectionUtils.isEmpty(nonAssignedOegPostSplit)) {
                        loggingResult(orderCode, "TargetSplitWithRoutingStrategy", originalOEG, splittedOEGs,
                                nonAssignedOegPostSplit);
                        return splittedOEGs;
                    }
                }
                // does not split
                else {
                    TargetPointOfServiceModel tpos = sameStoreCncSelectorStrategy.isValidForSameStoreCnc(
                            nonAssignedOegPostSplit, cncStoreNumber);

                    if (tpos == null
                            && instoreFulfilmentGlobalRulesChecker.areAllDispatchRulesPassed(nonAssignedOegPostSplit)) {
                        tpos = storeSelectorStrategy.selectStore(nonAssignedOegPostSplit);
                    }

                    if (tpos != null) {
                        oegParameterHelper.assignTposToOEG(nonAssignedOegPostSplit, tpos);
                        oegParameterHelper.setAssignedQtyByOrderEntries(nonAssignedOegPostSplit);
                        splittedOEGs.add(nonAssignedOegPostSplit);
                        return splittedOEGs;
                    }
                }

            }
            // NTL check
            // skip NTL lookup if cnc order
            if (cncStoreNumber != null) {
                LOG.logNTLCncSkipMessage(orderCode, "CncOrder");
            }
            else {
                nonAssignedOegPostSplit = this.ntlCheck(orderCode, nonAssignedOegPostSplit, splittedOEGs);
            }
            //route to fastline
            // Don't route to fastline if we have already assigned to fastline in splitStores logic(controlled by feature switch)
            if (targetFeatureSwitchService.isFeatureEnabled(FASLINE_FALCON)) {
                nonAssignedOegPostSplit = this.splitFastline(orderCode, splittedOEGs, nonAssignedOegPostSplit);
                loggingResult(orderCode, "TargetSplitWithRoutingStrategy", originalOEG, splittedOEGs,
                        nonAssignedOegPostSplit);
                if (CollectionUtils.isEmpty(nonAssignedOegPostSplit)) {
                    return splittedOEGs;
                }
                this.setNonAssignedOegIntoSplittedOEGs(nonAssignedOegPostSplit, splittedOEGs);

            }
            else {
                this.legacyRouteToFastline(nonAssignedOegPostSplit, splittedOEGs, originalOEG, orderCode);
            }

        }
        catch (final TargetStoreStockCheckException ex) {
            throw ex;
        }
        catch (final Exception e) {
            LOG.logRulesExitException(orderCode, "UnexpectedException", e);
            if (targetFeatureSwitchService.isFeatureEnabled(FASLINE_FALCON)) {
                LOG.logUnexpectedExceptionMessage(orderCode, "UnexpectedException", e);
                loggingResult(orderCode, "TargetSplitWithRoutingStrategy", originalOEG, splittedOEGs,
                        nonAssignedOegPostSplit);
                this.setNonAssignedOegIntoSplittedOEGs(nonAssignedOegPostSplit, splittedOEGs);
            }
            else {
                ticketSender.sendRoutingException(originalOEG);
                return Collections.singletonList(originalOEG);
            }
        }
        return splittedOEGs;
    }

    @Override
    public void afterSplitting(final OrderEntryGroup orderEntryGroup, final ConsignmentModel createdOne) {
        Assert.isTrue(createdOne instanceof TargetConsignmentModel, "consignment must be TargetConsignmentModel");

        final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)createdOne;

        assignConsignmentToWarehouse(orderEntryGroup, targetConsignment);
    }

    private void loggingResult(final String orderCode, final String action, final OrderEntryGroup originalOEG,
            final List<OrderEntryGroup> splittedOEGs, final OrderEntryGroup nonAssignedOegPostSplit) {
        final String deliveryModeCode = oegParameterHelper.getDeliveryMode(originalOEG) != null ? oegParameterHelper
                .getDeliveryMode(originalOEG).getCode() : StringUtils.EMPTY;
        final String cncStoreNumber = oegParameterHelper.getCncStoreNumber(originalOEG) != null ? oegParameterHelper
                .getCncStoreNumber(originalOEG).toString() : StringUtils.EMPTY;
        LOG.logSplitOegResult(orderCode, deliveryModeCode, cncStoreNumber, action, splittedOEGs,
                nonAssignedOegPostSplit);
    }

    /**
     * set assigned qty from required qty into not assignedOeg and the not assigned flag for creating the cancellation
     * consignment in after split
     * 
     * @param nonAssignedOegPostSplit
     * @param splittedOEGs
     */
    private void setNonAssignedOegIntoSplittedOEGs(final OrderEntryGroup nonAssignedOegPostSplit,
            final List<OrderEntryGroup> splittedOEGs) {
        oegParameterHelper.setAssignedQty(nonAssignedOegPostSplit,
                oegParameterHelper.getRequiredQty(nonAssignedOegPostSplit));
        oegParameterHelper.setNotAssignedOegFlag(nonAssignedOegPostSplit);
        splittedOEGs.add(nonAssignedOegPostSplit);
    }

    /**
     * split to fastline
     * 
     * @param nonAssignedOegs
     * @param splittedOEGs
     * @param originalOEG
     * @param orderCode
     */
    private void legacyRouteToFastline(final OrderEntryGroup nonAssignedOegs, final List<OrderEntryGroup> splittedOEGs,
            final OrderEntryGroup originalOEG, final String orderCode) {
        final List<AbstractOrderEntryModel> refundEntries = new ArrayList<>();
        if (null != nonAssignedOegs) {
            final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
            for (final AbstractOrderEntryModel entry : nonAssignedOegs) {
                try {
                    targetStockService.checkAndGetStockLevel(entry.getProduct(),
                            warehouse);
                }
                catch (final StockLevelNotFoundException e) {
                    refundEntries.add(entry);
                }
            }
            nonAssignedOegs.removeAll(refundEntries);
            if (CollectionUtils.isNotEmpty(nonAssignedOegs)) {
                oegParameterHelper.setAssignedQty(nonAssignedOegs, oegParameterHelper.getRequiredQty(nonAssignedOegs));
                splittedOEGs.add(nonAssignedOegs);
                LOG.logAssignedToFastlineMessage(orderCode, warehouse.getName());
            }
        }
        if (CollectionUtils.isNotEmpty(refundEntries)) {
            LOG.logUnexpectedExceptionMessage(orderCode, "StockNotInDefaultWarehouse", null);
            ticketSender.sendStockNotInDefaultWarehouse(originalOEG);
        }
    }

    private OrderEntryGroup ntlCheck(final String orderCode, final OrderEntryGroup nonAssignedOegPostSplit,
            final List<OrderEntryGroup> splittedOEGs) throws TargetSplitOrderException {
        LOG.logNTLRulesEntryMessage(orderCode);
        final SplitOegResult ntlSplitResult = ntlSelectorStrategy.split(nonAssignedOegPostSplit);
        if (null != ntlSplitResult.getAssignedOEGsToWarehouse()) {
            splittedOEGs.addAll(ntlSplitResult.getAssignedOEGsToWarehouse());
        }
        return ntlSplitResult.getNotAssignedToWarehouse();
    }

    /**
     * split external warehouse
     * 
     * @param originalOEG
     * @param splittedOEGs
     * @return nonAssignedOegPostSplit
     */
    private OrderEntryGroup splitExternalWarehouse(final String orderCode, final OrderEntryGroup originalOEG,
            final List<OrderEntryGroup> splittedOEGs) {
        OrderEntryGroup nonAssignedOegPostSplit = new OrderEntryGroup();
        try {
            final SplitOegResult splitResult = externalWarehouseSelectorStrategy.split(originalOEG);


            final List<OrderEntryGroup> oegsResult = splitResult.getAssignedOEGsToWarehouse();
            if (CollectionUtils.isNotEmpty(oegsResult)) {
                splittedOEGs.addAll(oegsResult);
            }
            nonAssignedOegPostSplit = splitResult.getNotAssignedToWarehouse();
        }
        catch (final TargetSplitOrderException e) {
            LOG.logUnexpectedExceptionMessage(orderCode, "ExternalWarehouseRoutingException", e);
            nonAssignedOegPostSplit = originalOEG;
        }
        return nonAssignedOegPostSplit;
    }

    /**
     * 
     * @param splittedOEGs
     * @param oeg
     * @return nonAssignedOegPostSplit
     */
    private OrderEntryGroup splitSameStoreCnc(final String orderCode, final List<OrderEntryGroup> splittedOEGs,
            final OrderEntryGroup oeg) {
        OrderEntryGroup nonAssignedOegPostSplit = new OrderEntryGroup();
        try {
            final SplitOegResult splitResult = sameStoreCncSplitConsignmentStrategy.split(oeg);
            final List<OrderEntryGroup> oegsResult = splitResult.getAssignedOEGsToWarehouse();
            if (CollectionUtils.isNotEmpty(oegsResult)) {
                splittedOEGs.addAll(oegsResult);
            }
            nonAssignedOegPostSplit = splitResult.getNotAssignedToWarehouse();
        }
        catch (final TargetStoreStockCheckException ex) {
            throw ex;
        }
        catch (final Exception e) {
            LOG.logUnexpectedExceptionMessage(orderCode, "sameStoreCncSplitConsignmentStrategy", e);
            nonAssignedOegPostSplit = oeg;
        }
        return nonAssignedOegPostSplit;
    }

    private OrderEntryGroup splitStores(final String orderCode, final List<OrderEntryGroup> splittedOEGs,
            final OrderEntryGroup oeg) {
        OrderEntryGroup nonAssignedOegPostSplit = new OrderEntryGroup();
        try {
            final SplitOegResult splitResult = storeSplitConsignmentStrategy.split(oeg);
            final List<OrderEntryGroup> oegsResult = splitResult.getAssignedOEGsToWarehouse();
            if (CollectionUtils.isNotEmpty(oegsResult)) {
                splittedOEGs.addAll(oegsResult);
            }
            nonAssignedOegPostSplit = splitResult.getNotAssignedToWarehouse();
        }
        catch (final TargetStoreStockCheckException ex) {
            throw ex;
        }
        catch (final Exception e) {
            LOG.logUnexpectedExceptionMessage(orderCode, "StoreSplitConsignmentStrategy", e);
            nonAssignedOegPostSplit = oeg;
        }
        return nonAssignedOegPostSplit;
    }

    private OrderEntryGroup splitFastline(final String orderCode, final List<OrderEntryGroup> splittedOEGs,
            final OrderEntryGroup oeg) {
        OrderEntryGroup nonAssignedOegPostSplit = oeg;
        if (!oegParameterHelper.containsFastlineOEG(splittedOEGs)) {
            try {
                final SplitOegResult splitResult = fastlineSplitConsignmentStrategy.split(oeg);
                loggingResult(orderCode, "FastlineSplitConsignmentStrategy", oeg, splittedOEGs,
                        nonAssignedOegPostSplit);
                final List<OrderEntryGroup> oegsResult = splitResult.getAssignedOEGsToWarehouse();
                if (CollectionUtils.isNotEmpty(oegsResult)) {
                    splittedOEGs.addAll(oegsResult);
                }
                nonAssignedOegPostSplit = splitResult.getNotAssignedToWarehouse();
            }
            catch (final Exception e) {
                LOG.logUnexpectedExceptionMessage(orderCode, "FastlineSplitConsignmentStrategy", e);
                nonAssignedOegPostSplit = oeg;
            }
        }
        else {
            LOG.logGeneralActionMessage(orderCode, "Skip FastlineSplitConsignmentStrategy",
                    "We have already assigned OEG to fastline warehouse");
        }
        return nonAssignedOegPostSplit;
    }

    /**
     * Given the oeg.warehouse assign the consignment to warehouse and carrier
     * 
     * @param orderEntryGroup
     * @param targetConsignment
     */
    private void assignConsignmentToWarehouse(final OrderEntryGroup orderEntryGroup,
            final TargetConsignmentModel targetConsignment) {
        //assign non warehouse for cancellation consignment
        if (oegParameterHelper.getNotAssignedOegFlag(orderEntryGroup)) {
            consignmentToWarehouseAssigner.assignCancellationWarehouseAndUpdateConsignmentStatus(targetConsignment);
            //trigger orderItemCancel business process
            final TargetOrderCancelEntryList targetOrderCancelEntryList = this
                    .createOrderCancelEntryList(orderEntryGroup);
            if (targetOrderCancelEntryList != null
                    && CollectionUtils.isNotEmpty(targetOrderCancelEntryList.getEntriesToCancel())) {
                final OrderModel order = oegParameterHelper.getOrderFromGivenGroup(orderEntryGroup);
                targetBusinessProcessService.startOrderItemCancelProcess(order, targetOrderCancelEntryList);
            }
            return;
        }

        final WarehouseModel warehouse = oegParameterHelper.getFulfilmentWarehouse(orderEntryGroup);
        TargetZoneDeliveryModeModel deliveryMode = null;
        final DeliveryModeModel delModeOnConsignment = targetConsignment.getDeliveryMode();
        Assert.isTrue(delModeOnConsignment instanceof TargetZoneDeliveryModeModel,
                "DeliveryMode null or not of type TargetZoneDeliveryModeModel");
        deliveryMode = (TargetZoneDeliveryModeModel)delModeOnConsignment;

        if (warehouse == null) {

            consignmentToWarehouseAssigner.assignDefaultWarehouse(targetConsignment);
            return;
        }


        if (BooleanUtils.isTrue(Boolean.valueOf(warehouse.isExternalWarehouse()))) {
            consignmentToWarehouseAssigner
                    .assignConsignmentToExternalWarehouse(deliveryMode, targetConsignment, warehouse);
            return;
        }
        final TargetPointOfServiceModel tpos = warehouseHelper.getAssignedStoreForWarehouse(warehouse);
        //After the check for FL and external warehouses, the consignments would come here would be instore ones. 
        //Log error for tpos null and return.
        if (tpos == null) {
            LOG.logUnexpectedExceptionMessage(targetConsignment.getCode(), "tposNullForInStoreConsignment", null);
            return;
        }
        // Toll type NTL
        if (PointOfServiceTypeEnum.WAREHOUSE.equals(tpos.getType())) {
            consignmentToWarehouseAssigner.assignConsignmentToNTL(targetConsignment, warehouse);
            return;
        }

        // Target store
        final OfcOrderType orderType = oegParameterHelper.getOfcOrderType(orderEntryGroup, tpos.getStoreNumber());
        final String orderCode = oegParameterHelper.getOrderCode(orderEntryGroup);

        consignmentToWarehouseAssigner.assignConsignmentToStore(targetConsignment, warehouse, orderType, deliveryMode,
                orderCode);
    }

    private TargetOrderCancelEntryList createOrderCancelEntryList(final OrderEntryGroup orderEntryGroup) {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        if (CollectionUtils.isNotEmpty(orderEntryGroup)) {
            final List<TargetOrderCancelEntry> targetOrderCancelEntries = new ArrayList<>();
            final Map<String, Long> assignedQty = oegParameterHelper.getAssignedQty(orderEntryGroup);
            for (final AbstractOrderEntryModel entry : orderEntryGroup) {
                final TargetOrderCancelEntry cancelEntry = new TargetOrderCancelEntry();
                cancelEntry.setOrderEntry(entry);
                cancelEntry.setCancelQuantity(assignedQty.get(entry.getProduct().getCode()));
                targetOrderCancelEntries.add(cancelEntry);
            }
            targetOrderCancelEntryList.setEntriesToCancel(targetOrderCancelEntries);
        }
        return targetOrderCancelEntryList;
    }


    /**
     * @param warehouseHelper
     *            the warehouseHelper to set
     */
    @Required
    public void setWarehouseHelper(final WarehouseHelper warehouseHelper) {
        this.warehouseHelper = warehouseHelper;
    }

    /**
     * @param sameStoreCncSelectorStrategy
     *            the sameStoreCncSelectorStrategy to set
     */
    @Required
    public void setSameStoreCncSelectorStrategy(final SameStoreCncSelectorStrategy sameStoreCncSelectorStrategy) {
        this.sameStoreCncSelectorStrategy = sameStoreCncSelectorStrategy;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param storeSelectorStrategy
     *            the storeSelectorStrategy to set
     */
    @Required
    public void setStoreSelectorStrategy(final StoreSelectorStrategy storeSelectorStrategy) {
        this.storeSelectorStrategy = storeSelectorStrategy;
    }


    /**
     * @param ntlSelectorStrategy
     *            the ntlSelectorStrategy to set
     */
    @Required
    public void setNtlSelectorStrategy(final WarehouseSelectorStrategy ntlSelectorStrategy) {
        this.ntlSelectorStrategy = ntlSelectorStrategy;
    }


    /**
     * @param instoreFulfilmentGlobalRulesChecker
     *            the instoreFulfilmentGlobalRulesChecker to set
     */
    @Required
    public void setInstoreFulfilmentGlobalRulesChecker(
            final InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker) {
        this.instoreFulfilmentGlobalRulesChecker = instoreFulfilmentGlobalRulesChecker;
    }


    /**
     * @param consignmentToWarehouseAssigner
     *            the consignmentToWarehouseAssigner to set
     */
    @Required
    public void setConsignmentToWarehouseAssigner(final ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner) {
        this.consignmentToWarehouseAssigner = consignmentToWarehouseAssigner;
    }

    /**
     * @param externalWarehouseSelectorStrategy
     *            the externalWarehouseSelectorStrategy to set
     */
    @Required
    public void setExternalWarehouseSelectorStrategy(
            final WarehouseSelectorStrategy externalWarehouseSelectorStrategy) {
        this.externalWarehouseSelectorStrategy = externalWarehouseSelectorStrategy;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param ticketSender
     *            the ticketSender to set
     */
    @Required
    public void setTicketSender(final TicketSender ticketSender) {
        this.ticketSender = ticketSender;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param sameStoreCncSplitConsignmentStrategy
     *            the sameStoreCncSplitConsignmentStrategy to set
     */
    @Required
    public void setSameStoreCncSplitConsignmentStrategy(
            final SameStoreCncSplitConsignmentStrategy sameStoreCncSplitConsignmentStrategy) {
        this.sameStoreCncSplitConsignmentStrategy = sameStoreCncSplitConsignmentStrategy;
    }

    /**
     * @param storeSplitConsignmentStrategy
     *            the storeSplitConsignmentStrategy to set
     */
    @Required
    public void setStoreSplitConsignmentStrategy(final StoreSplitConsignmentStrategy storeSplitConsignmentStrategy) {
        this.storeSplitConsignmentStrategy = storeSplitConsignmentStrategy;
    }

    /**
     * @param fastlineSplitConsignmentStrategy
     *            the fastlineSplitConsignmentStrategy to set
     */
    @Required
    public void setFastlineSplitConsignmentStrategy(
            final FastlineSplitConsignmentStrategy fastlineSplitConsignmentStrategy) {
        this.fastlineSplitConsignmentStrategy = fastlineSplitConsignmentStrategy;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

}
