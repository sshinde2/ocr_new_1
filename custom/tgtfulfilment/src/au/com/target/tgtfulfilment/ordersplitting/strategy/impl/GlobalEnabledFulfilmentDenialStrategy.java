/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author sbryan6
 *
 */
public class GlobalEnabledFulfilmentDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        if (isInstoreFulfilmentEnabled(capabilitiesModel)) {
            return DenialResponse.createNotDenied();
        }
        return DenialResponse.createDenied("GlobalFulfilmentDisabled");
    }

    private boolean isInstoreFulfilmentEnabled(final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel) {
        return (capabilitiesModel != null && capabilitiesModel.isEnabled());
    }

}
