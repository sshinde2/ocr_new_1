/**
 * 
 */
package au.com.target.tgtfulfilment.dao;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.List;

import au.com.target.tgtfluent.dao.FluentCarrierDao;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author dcwillia
 * 
 */
public interface TargetCarrierDao extends FluentCarrierDao {

    /**
     * Gets the preferred carrier, ie the one with the lowest priority number given the restrictions of the parameters.
     * 
     * @param deliveryMode
     *            which delivery mode are we using
     * @param requireBulky
     *            is available for bulky products required
     * @param enabledInStore
     *            is available for in store delivery
     * @return carrier to use
     */
    List<TargetCarrierModel> getApplicableCarriers(TargetZoneDeliveryModeModel deliveryMode, boolean requireBulky,
            final boolean enabledInStore);

    /**
     * Gets the Null carrier for the given delivery mode
     * 
     * @return null carrier
     */
    TargetCarrierModel getNullCarrier(TargetZoneDeliveryModeModel deliveryMode);

}
