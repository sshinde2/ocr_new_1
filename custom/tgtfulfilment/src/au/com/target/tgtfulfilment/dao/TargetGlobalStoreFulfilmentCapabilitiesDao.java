package au.com.target.tgtfulfilment.dao;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;


/**
 * Interface to get the Global blackout period.
 * 
 * @author Ajit
 */
public interface TargetGlobalStoreFulfilmentCapabilitiesDao {

    /**
     * Gets the global store fulfilment capabilities.
     *
     * This Method will return the BlackoutStart Date & BlackoutEnd Date If not GlobalStoreFulfilmentCapabilities not
     * there then it will return null
     * 
     * @return the global store fulfilment capabilities
     */
    GlobalStoreFulfilmentCapabilitiesModel getGlobalStoreFulfilmentCapabilities();

}