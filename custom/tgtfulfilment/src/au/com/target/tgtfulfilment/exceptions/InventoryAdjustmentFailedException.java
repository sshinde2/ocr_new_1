/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

/**
 * @author pthoma20
 * 
 */
public class InventoryAdjustmentFailedException extends RuntimeException {

    public InventoryAdjustmentFailedException(final String message) {
        super(message);
    }

    public InventoryAdjustmentFailedException(final Throwable throwable) {
        super(throwable);
    }

    public InventoryAdjustmentFailedException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
