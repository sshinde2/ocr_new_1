/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

/**
 * @author mjanarth
 *
 */
public class RefundFailedException extends Exception {

    /**
     * No argument constructor
     */
    public RefundFailedException() {
        super();
    }

    /**
     * @param message
     *            - descriptive message
     */
    public RefundFailedException(final String message) {
        super(message);
    }

    /**
     * @param message
     *            - descriptive message
     * @param cause
     *            - root cause
     */
    public RefundFailedException(final String message, final Exception cause) {
        super(message, cause);
    }

    /**
     * @param cause
     *            - root cause
     */
    public RefundFailedException(final Exception cause) {
        super(cause);
    }
}
