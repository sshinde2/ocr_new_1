/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * Exception to throw when the parameters are invalid.
 * 
 * @author jjayawa1
 *
 */
public class InvalidParameterException extends BusinessException {

    /**
     * @param message
     * @param cause
     */
    public InvalidParameterException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public InvalidParameterException(final String message) {
        super(message);
    }

}
