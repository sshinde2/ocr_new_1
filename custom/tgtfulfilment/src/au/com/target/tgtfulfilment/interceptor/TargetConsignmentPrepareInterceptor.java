/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.Date;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * This prepare interceptor sets up the dates on the consignment model based on the status of the consignment
 * 
 * @author rsamuel3
 *
 */
public class TargetConsignmentPrepareInterceptor implements PrepareInterceptor<TargetConsignmentModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.PrepareInterceptor#onPrepare(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onPrepare(final TargetConsignmentModel targetConsignment,
            final InterceptorContext paramInterceptorContext)
            throws InterceptorException {

        final ConsignmentStatus consignmentStatus = targetConsignment.getStatus();
        if (consignmentStatus != null) {
            final Date currentDate = new Date();
            if ((ConsignmentStatus.CONFIRMED_BY_WAREHOUSE.equals(consignmentStatus) || ConsignmentStatus.SENT_TO_WAREHOUSE
                    .equals(consignmentStatus))
                    && targetConsignment.getSentToWarehouseDate() == null) {
                targetConsignment.setSentToWarehouseDate(currentDate);
            }
            else if (ConsignmentStatus.SHIPPED.equals(consignmentStatus)) {
                if (targetConsignment.getShippingDate() == null) {
                    targetConsignment.setShippingDate(currentDate);
                }
                targetConsignment.setShipConfReceived(Boolean.TRUE);
            }
            else if (ConsignmentStatus.PACKED.equals(consignmentStatus) && targetConsignment.getPackedDate() == null) {
                targetConsignment.setPackedDate(currentDate);
            }
            else if (ConsignmentStatus.PICKED.equals(consignmentStatus)
                    && targetConsignment.getPickConfirmDate() == null) {
                targetConsignment.setPickConfirmDate(currentDate);
            }
            else if (ConsignmentStatus.WAVED.equals(consignmentStatus)
                    && targetConsignment.getWavedDate() == null) {
                targetConsignment.setWavedDate(currentDate);
            }
            else if (ConsignmentStatus.CANCELLED.equals(consignmentStatus)
                    && targetConsignment.getCancelDate() == null) {
                targetConsignment.setCancelDate(currentDate);
            }
        }

    }
}
