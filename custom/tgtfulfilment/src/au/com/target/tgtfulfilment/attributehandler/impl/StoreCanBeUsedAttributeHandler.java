/**
 * 
 */
package au.com.target.tgtfulfilment.attributehandler.impl;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * Attribute handler to check whether store can be available for in-store fulfillment based on the enablity and black
 * out period.
 * 
 * @author siddharam
 * 
 */
public class StoreCanBeUsedAttributeHandler extends
        AbstractDynamicAttributeHandler<Boolean, StoreFulfilmentCapabilitiesModel> {

    private StoreBlackOutService storeBlackOutService;


    @Override
    public Boolean get(final StoreFulfilmentCapabilitiesModel model) {

        //check store is enabled
        if (!model.getEnabled().booleanValue()) {
            return Boolean.FALSE;
        }

        //check blackout period exist
        if (storeBlackOutService.doesBlackOutPeriodApply(model.getBlackoutStart(), model.getBlackoutEnd())) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    /**
     * @param storeBlackOutService
     *            the storeBlackOutService to set
     */
    @Required
    public void setStoreBlackOutService(final StoreBlackOutService storeBlackOutService) {
        this.storeBlackOutService = storeBlackOutService;
    }

}
