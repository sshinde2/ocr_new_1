/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.fulfilmentservice.PickConsignmentUpdater;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;


public class PickConsignmentUpdaterImpl extends AbstractBusinessService implements PickConsignmentUpdater {
    private TargetFulfilmentHelper targetFulfilmentHelper;

    @Override
    public void updateConsignmentShipQuantities(final ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries) {

        Assert.notNull(consignment, "consignment cannot be null");

        for (final PickConfirmEntry pickEntry : pickConfirmEntries) {

            // Pick entry details should not be null here due to earlier validation
            if (pickEntry.getItemCode() != null && pickEntry.getQuantityShipped() != null) {

                final ConsignmentEntryModel consignmentEntry = targetFulfilmentHelper.getConsignmentEntryForItemCode(
                        consignment, pickEntry.getItemCode());

                // We should find the consignment entry 
                if (consignmentEntry != null) {
                    consignmentEntry.setShippedQuantity(Long.valueOf(pickEntry.getQuantityShipped().intValue()));
                    getModelService().save(consignmentEntry);
                    getModelService().refresh(consignmentEntry);
                }
            }
        }
        getModelService().save(consignment);
        getModelService().refresh(consignment);

    }

    @Override
    public void updateConsignmentDetails(final TargetConsignmentModel consignment, final Integer parcelCount,
            final String trackingNumber, final String carrier) {
        consignment.setTrackingID(trackingNumber);
        consignment.setParcelCount(parcelCount);
        consignment.setCarrier(carrier);
        getModelService().save(consignment);
    }

    /**
     * @param targetFulfilmentHelper
     *            the targetFulfilmentHelper to set
     */
    @Required
    public void setTargetFulfilmentHelper(final TargetFulfilmentHelper targetFulfilmentHelper) {
        this.targetFulfilmentHelper = targetFulfilmentHelper;
    }


}
