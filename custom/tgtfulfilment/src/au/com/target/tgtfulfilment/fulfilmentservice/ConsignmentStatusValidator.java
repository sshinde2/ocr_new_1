/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;


/**
 * Validate current consignment status against requested status change.
 *
 */
public interface ConsignmentStatusValidator {


    /**
     * Validate that the consignment is in an acceptable status for the requested change
     * 
     * @param oldStatus
     * @param newStatus
     * @throws ConsignmentStatusValidationException
     */
    void validateConsignmentStatus(ConsignmentStatus oldStatus, ConsignmentStatus newStatus)
            throws ConsignmentStatusValidationException;
}
