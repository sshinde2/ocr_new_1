package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.core.model.order.OrderModel;
//CHECKSTYLE:OFF
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.util.LoggingContext;


//CHECKSTYLE:ON

/**
 * Service to manage the interactions with the warehouse
 */
public interface TargetFulfilmentService {

    /**
     * Process the acknowledgement received on the consignment from the warehouse.
     * 
     * @param orderCode
     * @param consignment
     * @param loggingCtx
     * @throws FulfilmentException
     * @throws ConsignmentStatusValidationException
     */
    void processAckForConsignment(String orderCode, TargetConsignmentModel consignment, final LoggingContext loggingCtx)
            throws FulfilmentException,
            ConsignmentStatusValidationException;


    /**
     * Process the cancel received on the consignment from the warehouse.
     * 
     * @param consignmentCode
     * @param loggingCtx
     * @throws FulfilmentException
     * @throws ConsignmentStatusValidationException
     * @throws NotFoundException
     */
    void processCancelForConsignment(String consignmentCode, final LoggingContext loggingCtx)
            throws FulfilmentException,
            ConsignmentStatusValidationException, NotFoundException;


    /**
     * This method will process the consignment to be completed. It internally calls the ship confirm followed by the
     * pick confirm by mocking the entries.
     * 
     * @param consignmentId
     * @param shippedDate
     * @param trackingNumber
     * @param parcelCount
     * @param loggingCtx
     * @throws FulfilmentException
     * @throws ConsignmentStatusValidationException
     */
    void processCompleteConsignment(final String consignmentId,
            final Date shippedDate, final String carrier, final String trackingNumber, final Integer parcelCount,
            final LoggingContext loggingCtx)
            throws FulfilmentException, ConsignmentStatusValidationException, NotFoundException;


    /**
     * Process the shipment confirmation message received on the order from the warehouse. Will create and start a new
     * {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#ORDER_COMPLETION_PROCESS}
     * 
     * @param orderCode
     * @param consignment
     * @param shippedDate
     * @param loggingCtx
     * @throws FulfilmentException
     * @throws ConsignmentStatusValidationException
     */
    void processShipConfForConsignment(final String orderCode, final TargetConsignmentModel consignment,
            final Date shippedDate, final LoggingContext loggingCtx)
            throws FulfilmentException, ConsignmentStatusValidationException;

    /**
     * Process the pick confirmation received on the order from the warehouse.<br/>
     * Will validate and the pick conf to check invalid or over pick, then trigger the pick process
     * 
     * @param orderCode
     * @param consignment
     * @param carrier
     * @param trackingNumber
     * @param parcelCount
     * @param pickConfirmEntries
     * @param loggingCtx
     * @throws FulfilmentException
     */
    void processPickConfForConsignment(final String orderCode, final TargetConsignmentModel consignment,
            final String carrier,
            final String trackingNumber, final Integer parcelCount,
            List<PickConfirmEntry> pickConfirmEntries, final LoggingContext loggingCtx)
            throws FulfilmentException;

    /**
     * Process a completion sent by in store fulfilment. This method updates pick conf details, retrieves consignment
     * and Validates the consignment
     * 
     * @param consignmentCode
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     */
    void processPickForInstoreFulfilment(String consignmentCode) throws NotFoundException,
            ConsignmentStatusValidationException;

    /**
     * Process a reject sent by in store fulfilment.
     * 
     * @param consignmentCode
     * @param instoreRejectReason
     * @param rejectState
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     * @throws FluentBaseException
     */
    void processRejectInstoreFulfilment(String consignmentCode, String instoreRejectReason,
            ConsignmentRejectState rejectState)
            throws NotFoundException, ConsignmentStatusValidationException, FulfilmentException,
            FluentBaseException;

    /**
     * Process a waved sent by in store fulfilment.
     * 
     * @param consignmentCode
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     * @throws FluentBaseException
     */
    void processWavedForInstoreFulfilment(String consignmentCode) throws NotFoundException,
            ConsignmentStatusValidationException, FluentBaseException;

    /**
     * Process reroute auto timeout consignment.
     *
     * @param consignmentModel
     *            the consignment Model
     * @param reason
     *            the reason
     * @throws NotFoundException
     *             the not found exception
     * @throws ConsignmentStatusValidationException
     * @throws FluentBaseException
     */
    void processRerouteAutoTimeoutConsignment(TargetConsignmentModel consignmentModel,
            ConsignmentRejectReason reason)
            throws NotFoundException, ConsignmentStatusValidationException, FluentBaseException;

    /**
     * Process a consignment for in store fulfilment.
     * 
     * @param consignmentCode
     * @param parcelCount
     * @param consignmentParcels
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     * @throws FluentBaseException
     */
    void processCompleteForInstoreFulfilment(final String consignmentCode, final int parcelCount,
            List<ConsignmentParcelDTO> consignmentParcels) throws NotFoundException,
            ConsignmentStatusValidationException, FulfilmentException, FluentBaseException;

    /**
     * Process auto action based on warehouse.
     * 
     * @param targetConsignment
     * @throws FulfilmentException
     * @throws ConsignmentStatusValidationException
     */
    void processConsignmentAutoActions(final TargetConsignmentModel targetConsignment) throws FulfilmentException,
            ConsignmentStatusValidationException;

    /**
     * process cancellation for items which are rejected because short pick.
     * 
     * @param targetConsigment
     * @param orderModel
     */

    void processCancelOrderItemInConsignmentForShortPick(TargetConsignmentModel targetConsigment,
            OrderModel orderModel) throws FulfilmentException;

    /**
     * Method to get the cancel entry List in a consignment in case of a short pick.
     * 
     * @param consignment
     * @return order cancel entry list
     * @throws FulfilmentException
     */

    TargetOrderCancelEntryList getOrderCancelEntriesForShortPick(final TargetConsignmentModel consignment)
            throws FulfilmentException;

    /**
     * This method will process the consignment to be shipped. It internally calls the ship confirm.
     *
     * @param consignmentId
     * @param trackingNumber
     * @param loggingCtx
     * @throws FulfilmentException
     */
    void processShipConsignment(final String consignmentId, final String trackingNumber,
            final LoggingContext loggingCtx) throws FulfilmentException;
}
