/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import au.com.target.tgtfulfilment.exceptions.TrackingIdGenerationException;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentTrackingIdGenerator;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author smishra1
 *
 */
public class ConsignmentTrackingIdGeneratorImpl implements ConsignmentTrackingIdGenerator {

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.ConsignmentTrackingIdGenerator#generateTrackingIdForConsignment
     * (au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel)
     */
    @Override
    public String generateTrackingIdForConsignment(final StoreFulfilmentCapabilitiesModel storeFulfilmentModel)
            throws TrackingIdGenerationException {
        // Default implementation.
        return null;
    }

}
