/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;


/**
 * Strategy for determining the pick type of a consignment
 * 
 */
public interface PickTypeAscertainerStrategy {

    /**
     * Determine the pick type of the given consignment
     * 
     * @param consignment
     * @param updateData
     * @return Pick Type
     * @throws FulfilmentException
     */
    PickTypeEnum ascertainPickType(ConsignmentModel consignment, PickConsignmentUpdateData updateData)
            throws FulfilmentException;

}
