/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import au.com.target.tgtfulfilment.dto.PickConfirmEntry;


/**
 * Helper for fulfilment service for sending cs tickets.
 * 
 */
public interface PickTicketSender {

    /**
     * @param orderModel
     * @param errorMessage
     */
    void sendTicketInvalidPick(OrderModel orderModel, String errorMessage);

    /**
     * @param order
     * @param consignment
     */
    void sendTicketShortZeroPick(OrderModel order, ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries);

    /**
     * @param order
     * @param consignment
     * @param pickConfirmEntries
     */
    void sendTicketOverPick(OrderModel order, ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries);

    /**
     * @param orderModel
     * @param errorMessage
     */
    void sendTicketFailedRefund(OrderModel orderModel, String errorMessage);

    /**
     * Checks based on the Sales application being kiosk
     * 
     * @param order
     * @return true if the sales application is kiosk and false otherwise
     */
    boolean isAKioskOrder(final OrderModel order);

    /**
     * checks based on the Sales application being eBay
     * 
     * @param order
     * @return true if the sales application is eBay and false otherwise
     */

    boolean isEbayOrder(final OrderModel order);

    /**
     * checks based on the Sales application being TradeMe
     * 
     * @param order
     * @return true if the sales application is TradeMe and false otherwise
     */

    boolean isTradeMeOrder(final OrderModel order);
}
