/**
 * 
 */
package au.com.target.tgtfulfilment.stock.dao;

import de.hybris.platform.ordersplitting.model.WarehouseModel;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.stock.TargetStockLevelDao;


/**
 * @author dcwillia
 * 
 */
public interface TargetFulfilmentStockLevelDao extends TargetStockLevelDao {
    /**
     * for a given product and warehouse gets the stock qty adjustment for the product reserve figure
     * 
     * @param product
     * @param warehouse
     * @return qty to reconcile
     */
    long calculateReservedStockAdjustment(final AbstractTargetVariantProductModel product,
            final WarehouseModel warehouse);
}
