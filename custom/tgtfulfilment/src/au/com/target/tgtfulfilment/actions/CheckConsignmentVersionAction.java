/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractAction;

import java.util.EnumSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;


/**
 * @author smishra1
 *
 */
public class CheckConsignmentVersionAction extends AbstractAction<OrderProcessModel> {
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public String execute(final OrderProcessModel process) throws BusinessProcessException {

        // Get the consignment out of the process parameters
        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(consignment, "Consignment cannot be null");
        if (consignment instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
            if (OfcOrderType.INSTORE_PICKUP.equals(targetConsignment.getOfcOrderType())
                    && null != targetConsignment.getConsignmentVersion()) {
                return Transition.INSTORE_CONSIGNMENT_NEW_VERSION.toString();
            }
            return Transition.DELIVERY_CONSIGNMENT_NEW_VERSION.toString();
        }
        return null;
    }


    public enum Transition {
        INSTORE_CONSIGNMENT_NEW_VERSION, DELIVERY_CONSIGNMENT_NEW_VERSION
    }

    @Override
    public Set<String> getTransitions() {
        return ImmutableSet.copyOf(Iterables.transform(EnumSet.allOf(Transition.class), Functions.toStringFunction()));
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }
}
