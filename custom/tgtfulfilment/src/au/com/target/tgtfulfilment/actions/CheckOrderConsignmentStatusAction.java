/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractAction;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


public class CheckOrderConsignmentStatusAction extends AbstractAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(CheckOrderConsignmentStatusAction.class);

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetWarehouseService targetWarehouseService;

    private List<String> warehousesList;

    private List<ConsignmentStatus> requiredStatusListForWarehouse;

    private List<ConsignmentStatus> requiredStatusListForStore;

    public enum Transition {
        ALL_CANCELLED, ALL_DONE, PENDING
    }

    @Override
    public Set<String> getTransitions() {
        return ImmutableSet.copyOf(Iterables.transform(EnumSet.allOf(Transition.class), Functions.toStringFunction()));
    }

    @Override
    public String execute(final OrderProcessModel process) throws Exception {
        Assert.notNull(process);
        Assert.notNull(process.getOrder());
        return getCombinedConsignmentState(process);
    }

    private String getCombinedConsignmentState(final OrderProcessModel process) {
        final OrderModel order = process.getOrder();
        String result = Transition.ALL_CANCELLED.name();
        int orderEntryUnitCount = 0;
        int completedConsignmentUnitCount = 0;
        if (CollectionUtils.isNotEmpty(order.getEntries())) {
            orderEntryUnitCount = this.getTotalOrderItemCount(order);
            if (orderEntryUnitCount > 0) {
                completedConsignmentUnitCount = this.getTotalCompleteConsignmentQty(order);
                if (orderEntryUnitCount - completedConsignmentUnitCount > 0) {
                    result = Transition.PENDING.name();
                }
                else {
                    result = Transition.ALL_DONE.name();
                }
            }
            else {
                result = Transition.ALL_CANCELLED.name();
            }

        }
        else {
            result = Transition.ALL_CANCELLED.name();
        }

        LOG.info("Check the order status for order=" + order.getCode()
                + " by comparing the total quantity in order entry and consignment for warehouse:"
                + StringUtils.join(warehousesList, ",") + " with status: "
                + StringUtils.join(requiredStatusListForWarehouse, ",")
                + " and consignment for store with status: "
                + StringUtils.join(requiredStatusListForStore, ",") + ", totalOrdeEntryQuantity=" + orderEntryUnitCount
                + ", totalRequiredConsignmentQuantity=" + completedConsignmentUnitCount);
        return result;

    }

    private int getTotalOrderItemCount(final OrderModel order) {
        int orderEntryUnitCount = 0;
        for (final AbstractOrderEntryModel entry : order.getEntries()) {
            final long quantity = entry.getQuantity() != null ? entry.getQuantity().longValue() : 0;
            orderEntryUnitCount += quantity;
        }
        return orderEntryUnitCount;
    }

    private int getTotalCompleteConsignmentQty(final OrderModel order) {
        int count = 0;
        final Set<ConsignmentModel> consignments = order.getConsignments();
        if (CollectionUtils.isNotEmpty(consignments)) {
            for (final ConsignmentModel consignment : consignments) {

                if (consignmentBelongsToDefaultOnlineWarehouseTreatedLikeAStore(consignment)) {
                    count = updateCountForStore(count, consignment);
                }
                else if (consignmentBelongsToWarehouse(consignment)) {
                    count = updateCountForWarehouse(count, consignment);
                }
                else {
                    count = updateCountForStore(count, consignment);
                }
            }
        }
        return count;
    }

    /**
     * @param consignment
     * @return
     */
    private boolean consignmentBelongsToWarehouse(final ConsignmentModel consignment) {
        return warehousesList
                .contains(consignment.getWarehouse().getCode());
    }

    /**
     * @param count
     * @param consignment
     * @return
     */
    private int updateCountForWarehouse(final int count, final ConsignmentModel consignment) {
        int totalShippedQtyOnAssignment = 0;
        if (requiredStatusListForWarehouse.contains(consignment.getStatus())) {

            totalShippedQtyOnAssignment += getTotalShippedQtyOnConsignment(consignment);
        }
        return count + totalShippedQtyOnAssignment;
    }

    /**
     * @param count
     * @param consignment
     * @return
     */
    private int updateCountForStore(final int count, final ConsignmentModel consignment) {
        int totalShippedQtyOnAssignment = 0;
        if (requiredStatusListForStore.contains(consignment.getStatus())) {
            totalShippedQtyOnAssignment += getTotalShippedQtyOnConsignment(consignment);
        }
        return count + totalShippedQtyOnAssignment;
    }

    /**
     * @param consignmentHasDefaultWarehouse
     * @param defaultWarehouseIsForcedToOnlinePOS
     * @return
     */
    private boolean consignmentBelongsToDefaultOnlineWarehouseTreatedLikeAStore(final ConsignmentModel consignment) {
        final boolean consignmentHasDefaultOnlineWarehouse = doesConsignmentBelongToDefaultOnlineWarehouse(consignment);
        final boolean defaultWarehouseIsForcedToOnlinePOS = targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS);

        return consignmentHasDefaultOnlineWarehouse && defaultWarehouseIsForcedToOnlinePOS;
    }

    /**
     * @param consignment
     * @return
     */
    private boolean doesConsignmentBelongToDefaultOnlineWarehouse(final ConsignmentModel consignment) {
        final WarehouseModel defaultOnlineWarehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        return defaultOnlineWarehouse.equals(consignment.getWarehouse());
    }

    private int getTotalShippedQtyOnConsignment(final ConsignmentModel consignment) {
        int count = 0;
        if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
            for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries()) {
                final long shippedQty = entry.getShippedQuantity() != null ? entry.getShippedQuantity().longValue() : 0;
                count += shippedQty;
            }
        }
        return count;
    }

    /**
     * @param warehousesList
     *            the warehousesList to set
     */
    @Required
    public void setWarehousesList(final List<String> warehousesList) {
        this.warehousesList = warehousesList;
    }

    /**
     * @param requiredStatusListForWarehouse
     *            the requiredStatusListForWarehouse to set
     */
    @Required
    public void setRequiredStatusListForWarehouse(final List<ConsignmentStatus> requiredStatusListForWarehouse) {
        this.requiredStatusListForWarehouse = requiredStatusListForWarehouse;
    }

    /**
     * @param requiredStatusListForStore
     *            the requiredStatusListForStore to set
     */
    @Required
    public void setRequiredStatusListForStore(final List<ConsignmentStatus> requiredStatusListForStore) {
        this.requiredStatusListForStore = requiredStatusListForStore;
    }

    /**
     * @return the targetFeatureSwitchService
     */
    public TargetFeatureSwitchService getTargetFeatureSwitchService() {
        return targetFeatureSwitchService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @return the targetWarehouseService
     */
    public TargetWarehouseService getTargetWarehouseService() {
        return targetWarehouseService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }



}
