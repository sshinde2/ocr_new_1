/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtutility.constants.TgtutilityConstants;


/**
 * Returns {@link de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition#OK} when all order
 * consignment status' are in the supplied list,
 * {@link de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition#NOK} otherwise.
 */
public class CheckConsignmentWithStatusAction extends AbstractSimpleDecisionAction<OrderProcessModel> {
    private static final Logger LOG = Logger.getLogger(CheckConsignmentWithStatusAction.class);

    private List<ConsignmentStatus> requiredStatusList = null;

    private TargetConsignmentService targetConsignmentService;

    @Override
    public Transition executeAction(final OrderProcessModel process) throws Exception {
        Assert.notNull(process);
        Assert.notNull(process.getOrder());

        final boolean isAllInList = getAllInList(process);
        return isAllInList ? Transition.OK : Transition.NOK;
    }


    /**
     * Return true if all of the active consignments in the order are the <code>requiredStatusList</code>
     * 
     * @param process
     *            the order process context
     * @return true if all in required status
     */
    protected boolean getAllInList(final OrderProcessModel process) {
        Assert.notNull(process);
        Assert.notNull(process.getOrder());

        final OrderModel order = process.getOrder();

        final List<TargetConsignmentModel> activeConsignments = targetConsignmentService
                .getActiveConsignmentsForOrder(order);
        if (CollectionUtils.isEmpty(activeConsignments)) {
            throw new BusinessProcessException(TgtutilityConstants.ErrorCode.ERR_TGTFULLFILLMENT,
                    "Order has no active consignments");
        }

        for (final TargetConsignmentModel consignment : activeConsignments) {
            if (consignment == null) {
                LOG.warn("Consignment shouldn't be null at this point. " + process.getCode());
                continue;
            }

            if (!requiredStatusList.contains(consignment.getStatus())) {
                return false;
            }
        }

        return true;
    }

    /**
     * This action will require all the consignments to have a status in this list if it will return a response of OK.
     * 
     * @param requiredStatusList
     *            the requiredStatusList to set
     */
    @Required
    public void setRequiredStatusList(final List<ConsignmentStatus> requiredStatusList) {
        this.requiredStatusList = requiredStatusList;
    }


    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }




}
