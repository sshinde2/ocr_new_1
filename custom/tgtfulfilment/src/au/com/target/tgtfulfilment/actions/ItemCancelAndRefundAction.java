/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTicketSender;
import au.com.target.tgtfulfilment.ordercancel.TargetProcessCancelService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
public class ItemCancelAndRefundAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(ItemCancelAndRefundAction.class);

    private TargetProcessCancelService targetProcessCancelService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    private PickTicketSender pickTicketSender;

    @Override
    public Transition executeAction(final OrderProcessModel orderProcess) throws RetryLaterException, Exception {
        Assert.notNull(orderProcess, "order process can not be null");
        final OrderModel order = orderProcess.getOrder();
        Assert.notNull(order, "order can not be null");
        LOG.info("Start item cancellation action");
        final TargetOrderCancelEntryList targetOrderCancelEntryList = orderProcessParameterHelper
                .getTargetOrderCancelEntryList(orderProcess);
        CancelRequestResponse response = null;
        try {
            response = targetProcessCancelService.processCancel(order,
                    targetOrderCancelEntryList);
        }
        catch (final OrderCancelException e) {
            LOG.info("Failed Refund occurred for order:" + order.getCode());
            pickTicketSender.sendTicketFailedRefund(order, e.getMessage());
            return Transition.NOK;
        }

        setOrderProcessParameters(orderProcess, response);
        return Transition.OK;
    }

    private void setOrderProcessParameters(final OrderProcessModel orderProcess, final CancelRequestResponse response) {
        if (response != null) {
            orderProcessParameterHelper.setOrderCancelRequest(orderProcess,
                    (OrderCancelRecordEntryModel)response.getOrderModificationRecordEntryModel());
            orderProcessParameterHelper.setRefundAmount(orderProcess, response.getRefundAmount());
            orderProcessParameterHelper.setCancelType(orderProcess, response.getCancelType());
            orderProcessParameterHelper
                    .setRefundPaymentEntryList(orderProcess, response.getFollowOnRefundPaymentList());
            orderProcessParameterHelper.setRefundAmountRemaining(orderProcess, response.getRefundAmountRemaining());
        }

    }

    /**
     * @param targetProcessCancelService
     *            the targetProcessCancelService to set
     */
    @Required
    public void setTargetProcessCancelService(final TargetProcessCancelService targetProcessCancelService) {
        this.targetProcessCancelService = targetProcessCancelService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param pickTicketSender
     *            the pickTicketSender to set
     */
    @Required
    public void setPickTicketSender(final PickTicketSender pickTicketSender) {
        this.pickTicketSender = pickTicketSender;
    }

}
