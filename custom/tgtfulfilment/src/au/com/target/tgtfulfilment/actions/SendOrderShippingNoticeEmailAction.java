/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.SendEmailAction;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * @author smudumba
 *
 */
public class SendOrderShippingNoticeEmailAction extends SendEmailAction {

    private static final Logger LOG = Logger.getLogger(SendOrderShippingNoticeEmailAction.class);

    private TargetConsignmentService targetConsignmentService;





    @Override
    public Transition executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Process cannot be null");
        if (process instanceof OrderProcessModel) {
            final OrderModel order = ((OrderProcessModel)process).getOrder();

            final List<TargetConsignmentModel> consignmentList = targetConsignmentService
                    .getActiveConsignmentsForOrder(order);

            if (CollectionUtils.isNotEmpty(consignmentList)) {
                for (final TargetConsignmentModel consignment : consignmentList) {

                    if (consignment.getOfcOrderType() != null
                            && (consignment.getOfcOrderType().equals(OfcOrderType.INSTORE_PICKUP)
                                    || consignment.getOfcOrderType().equals(OfcOrderType.INTERSTORE_DELIVERY))) {
                        LOG.info("Skipping sending tax invoice for order fulfilled by store " + order.getCode());
                        return Transition.OK;
                    }
                }
                super.executeAction(process);
            }
        }



        return Transition.OK;

    }



    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }







}
