/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;


/**
 * @author Nandini
 *
 */
public class InterStoreTransferAction extends AbstractProceduralAction<OrderProcessModel> {

    @Override
    public void executeAction(final OrderProcessModel orderProcess) throws RetryLaterException, Exception {
        // skeleton method, original impl is in sub-class
    }

}
