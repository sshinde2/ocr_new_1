/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.fulfilmentservice.PickConsignmentUpdater;


/**
 * Update consignment quantities and details
 * 
 */
public class UpdateConsignmentAction extends AbstractProceduralAction<OrderProcessModel> {

    private OrderProcessParameterHelper orderProcessParameterHelper;
    private PickConsignmentUpdater pickConsignmentUpdater;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process);

        // Get the consignment and update object out of the process parameters
        final TargetConsignmentModel consignment = (TargetConsignmentModel)orderProcessParameterHelper
                .getConsignment(process);
        Assert.notNull(consignment);

        final Object data = orderProcessParameterHelper.getPickConsignmentUpdateData(process);
        Assert.notNull(data);
        Assert.isInstanceOf(PickConsignmentUpdateData.class, data);

        final PickConsignmentUpdateData consignmentUpdateData = (PickConsignmentUpdateData)data;
        pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, consignmentUpdateData.getPickEntries());
        pickConsignmentUpdater.updateConsignmentDetails(consignment, consignmentUpdateData.getParcelCount(),
                consignmentUpdateData.getTrackingNumber(), consignmentUpdateData.getCarrier());

    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param pickConsignmentUpdater
     *            the pickConsignmentUpdater to set
     */
    @Required
    public void setPickConsignmentUpdater(final PickConsignmentUpdater pickConsignmentUpdater) {
        this.pickConsignmentUpdater = pickConsignmentUpdater;
    }

}
