/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import java.util.List;

import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;


/**
 * @author smishra1
 *
 */
public interface TargetStandardParcelService {
    /**
     * Method to call the Data Access Layer to get all the standard parcel details.
     * 
     * @return List of All standard parcel models
     */
    List<TargetStandardParcelModel> getEnabledStandardParcelDetails();
}
