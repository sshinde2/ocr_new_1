/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.manifest.strategy.TargetGenerateManifestCodeStrategy;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetManifestDao;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.logger.InstoreFulfilmentLogger;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.service.TargetManifestService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author Vivek
 *
 */
public class TargetManifestServiceImpl implements TargetManifestService {

    private static final Logger LOG = Logger.getLogger(TargetManifestServiceImpl.class);
    private static final InstoreFulfilmentLogger INSTORE_LOGGER = new InstoreFulfilmentLogger(
            TargetManifestServiceImpl.class);

    private TargetGenerateManifestCodeStrategy targetGenerateManifestCodeStrategy;
    private TargetStoreConsignmentService targetStoreConsignmentService;
    private ModelService modelService;
    private TargetManifestDao targetManifestDao;
    private TargetBusinessProcessService targetBusinessProcessService;
    private TargetFeatureSwitchService targetFeatureSwitchService;



    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetManifestService#createManifest(int)
     */
    @Override
    public TargetManifestModel createManifest(final TargetPointOfServiceModel pointOfService) {

        Assert.notNull(pointOfService, "Point of Service must not be Null");
        Assert.notNull(pointOfService.getStoreNumber(), "Store Number of POS must not be Null");

        final List<TargetConsignmentModel> consignments = targetStoreConsignmentService
                .getConsignmentsNotManifestedForStore(pointOfService.getStoreNumber().intValue());

        if (consignments.isEmpty()) {
            throw new UnsupportedOperationException(
                    "Manifest Cannnot be created as there are no unmanifested consignments for the store.");
        }

        TargetManifestModel manifest = null;

        final Set<TargetConsignmentModel> consignmentSet = new HashSet<>();
        for (final TargetConsignmentModel consignment : consignments) {
            if (OfcOrderType.INSTORE_PICKUP.equals(consignment.getOfcOrderType())) {
                LOG.warn(MessageFormat
                        .format("ConsignmentNotManifested : consignment {0} is of type InStore Pickup",
                                consignment.getCode()));
                continue;
            }
            else if (null != consignment.getManifest()) {
                LOG.warn(MessageFormat
                        .format("ConsignmentNotManifested : consignment {0} is already manifested with the Manifest ID of  {1}",
                                consignment.getCode(), consignment.getManifest().getCode()));
                continue;
            }

            if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
                consignment.setStatus(ConsignmentStatus.SHIPPED);
            }
            consignmentSet.add(consignment);
        }


        if (CollectionUtils.isNotEmpty(consignmentSet)) {

            manifest = modelService.create(TargetManifestModel.class);
            manifest.setCode(targetGenerateManifestCodeStrategy.generateManifestCode(pointOfService.getStoreNumber()
                    .intValue()));
            manifest.setDate(Calendar.getInstance().getTime());
            manifest.setConsignments(consignmentSet);
            modelService.save(manifest);
            modelService.saveAll(consignmentSet);
        }

        return manifest;
    }

    @Override
    public void completeConsignmentsInManifest(final TargetManifestModel manifestModel, final Integer storeNumber) {

        Assert.notNull(manifestModel, "manifestModel must not be Null");

        final Set<TargetConsignmentModel> consignmentSet = manifestModel.getConsignments();
        if (CollectionUtils.isNotEmpty(consignmentSet)) {
            for (final TargetConsignmentModel consignment : consignmentSet) {

                final OrderModel order = (OrderModel)consignment.getOrder();
                Assert.notNull(order, "Order cannot be null for consignment with code : " + consignment.getCode());

                targetBusinessProcessService.startOrderConsignmentProcess(order, consignment,
                        TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
                INSTORE_LOGGER.logStoreConsignmentAction("ManifestCompleteConsignmentSuccess", storeNumber,
                        targetStoreConsignmentService.getOrderCodeFromConsignment(consignment),
                        consignment.getCode(),
                        targetStoreConsignmentService.getDeliverFromStateFromConsignment(consignment),
                        targetStoreConsignmentService.getDeliverToStoreNumberFromConsignment(consignment),
                        targetStoreConsignmentService.getDeliverToStateFromConsignment(consignment));

            }
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetManifestService#getManifestByCode(java.lang.String)
     */
    @Override
    public TargetManifestModel getManifestByCode(final String manifestCode) throws NotFoundException {
        Assert.notNull(manifestCode, "manifestCode can't be null");

        TargetManifestModel manifest = null;

        try {
            manifest = targetManifestDao.getManifestByCode(manifestCode);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            throw new IllegalArgumentException("Error in getting manifest details with manifestCode : " + manifestCode,
                    e);
        }
        catch (final TargetUnknownIdentifierException e) {
            throw new NotFoundException("Error: manifest details not found with manifestCode : " + manifestCode, e);
        }
        return manifest;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetManifestService#getManifestsHistoryForStore(au.com.target.tgtcore.model.TargetPointOfServiceModel)
     */
    @Override
    public List<TargetManifestModel> getManifestsHistoryForStore(final TargetPointOfServiceModel store) {
        Assert.notNull(store, "TargetPointOfService can't be null");
        return targetManifestDao.getManifestsHistoryForStore(store);
    }

    /**
     * @param targetGenerateManifestCodeStrategy
     *            the new target generate manifest code strategy
     */
    @Required
    public void setTargetGenerateManifestCodeStrategy(
            final TargetGenerateManifestCodeStrategy targetGenerateManifestCodeStrategy) {
        this.targetGenerateManifestCodeStrategy = targetGenerateManifestCodeStrategy;
    }

    /**
     * @param targetStoreConsignmentService
     *            the new target store consignment service
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param modelService
     *            the new model service
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetManifestDao
     *            the targetManifestDao to set
     */
    @Required
    public void setTargetManifestDao(final TargetManifestDao targetManifestDao) {
        this.targetManifestDao = targetManifestDao;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(
            final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}
