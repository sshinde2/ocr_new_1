/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.dao.TargetStandardParcelDao;
import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;
import au.com.target.tgtfulfilment.service.TargetStandardParcelService;


/**
 * @author smishra1
 *
 */
public class TargetStandardParcelServiceImpl implements TargetStandardParcelService {

    private TargetStandardParcelDao targetStandardParcelDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TargetStandardParcelModel> getEnabledStandardParcelDetails() {
        return targetStandardParcelDao.getEnabledTargetStandardParcels();
    }

    /**
     * @param targetStandardParcelDao
     *            the targetStandardParcelDao to set
     */
    @Required
    public void setTargetStandardParcelDao(final TargetStandardParcelDao targetStandardParcelDao) {
        this.targetStandardParcelDao = targetStandardParcelDao;
    }
}
