package au.com.target.tgtfulfilment.service;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;


/**
 * The Interface TargetGlobalStoreFulfilmentService. This Interface is used to fetch GlobalStoreFulfilmentCapabilities
 * model
 * 
 * @author ajit
 */
public interface TargetGlobalStoreFulfilmentService {

    /**
     * Method to get GlobalStoreFulfilmentCapabilities model
     * 
     * @return GlobalStoreFulfilmentCapabilitiesModel
     */
    GlobalStoreFulfilmentCapabilitiesModel getGlobalStoreFulfilmentCapabilites();
}
