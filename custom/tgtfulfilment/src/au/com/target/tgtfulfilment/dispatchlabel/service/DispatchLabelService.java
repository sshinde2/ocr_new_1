/**
 * 
 */
package au.com.target.tgtfulfilment.dispatchlabel.service;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;


/**
 * Service interface to retrieve a dispatch label for a consignment from partner carrier
 * 
 * @author sbryan6
 *
 */
public interface DispatchLabelService {

    /**
     * Get the dispatch label for the given consignment and options.
     * 
     * @param pos
     * @param consignment
     * @param layout
     * @param branding
     * @return LabelResponseDTO
     */
    LabelResponseDTO getDispatchLabel(TargetPointOfServiceModel pos, TargetConsignmentModel consignment,
            LabelLayout layout, boolean branding);
}
