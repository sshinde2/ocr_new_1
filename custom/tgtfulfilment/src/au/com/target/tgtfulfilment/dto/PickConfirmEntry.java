/**
 * 
 */
package au.com.target.tgtfulfilment.dto;

import java.io.Serializable;


/**
 * Bean representing a Pick Confirmation Entry
 * 
 */
public class PickConfirmEntry implements Serializable {

    private String itemCode;
    private Integer quantityShipped;


    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the quantityShipped
     */
    public Integer getQuantityShipped() {
        return quantityShipped;
    }

    /**
     * @param quantityShipped
     *            the quantityShipped to set
     */
    public void setQuantityShipped(final Integer quantityShipped) {
        this.quantityShipped = quantityShipped;
    }

}
