/**
 * 
 */
package au.com.target.tgtfulfilment.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 * POJO to represent the data for updating a consignment after a pick.
 * 
 */
public class PickConsignmentUpdateData implements Serializable {

    private List<PickConfirmEntry> pickEntries;

    private Integer parcelCount;
    private String trackingNumber;
    private String carrier;

    /**
     * Get QuantityShipped for the pick entry for the given itemCode
     * 
     * @param itemCode
     * @return quantity shipped
     */
    public long getShippedQuantity(final String itemCode) {

        if (CollectionUtils.isNotEmpty(pickEntries)) {
            for (final PickConfirmEntry pickEntry : pickEntries) {

                // Pick entry details should not be null here due to earlier validation
                if (pickEntry.getItemCode() != null && pickEntry.getItemCode().equals(itemCode)) {

                    // Treat null as 0
                    if (pickEntry.getQuantityShipped() != null) {
                        return pickEntry.getQuantityShipped().longValue();
                    }
                    return 0;
                }
            }
        }

        return 0;
    }

    /**
     * @return the pickEntries
     */
    public List<PickConfirmEntry> getPickEntries() {
        return pickEntries;
    }

    /**
     * @param pickEntries
     *            the pickEntries to set
     */
    public void setPickEntries(final List<PickConfirmEntry> pickEntries) {
        this.pickEntries = pickEntries;
    }

    /**
     * @return the parcelCount
     */
    public Integer getParcelCount() {
        return parcelCount;
    }

    /**
     * @param parcelCount
     *            the parcelCount to set
     */
    public void setParcelCount(final Integer parcelCount) {
        this.parcelCount = parcelCount;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber
     *            the trackingNumber to set
     */
    public void setTrackingNumber(final String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the carrier
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * @param carrier
     *            the carrier to set
     */
    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }


}
