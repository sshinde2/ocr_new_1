/**
 * 
 */
package au.com.target.tgtfulfilment.order.strategies;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.strategies.UpdateAddressDenialStrategy;


/**
 * @author dcwillia
 * 
 */
public class OrderStateUpdateAddressDenialStrategy implements UpdateAddressDenialStrategy {
    private List<OrderStatus> updateDeniedOrderStates;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        Assert.notNull(order);
        return (updateDeniedOrderStates == null)
                || updateDeniedOrderStates.contains(order.getStatus());
    }

    /**
     * @param updateDeniedOrderStates
     *            the updateDeniedOrderStates to set
     */
    @Required
    public void setUpdateDeniedOrderStates(final List<OrderStatus> updateDeniedOrderStates) {
        this.updateDeniedOrderStates = updateDeniedOrderStates;
    }
}
