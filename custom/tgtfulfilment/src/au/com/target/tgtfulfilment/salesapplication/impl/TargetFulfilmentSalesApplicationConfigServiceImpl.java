/**
 * 
 */
package au.com.target.tgtfulfilment.salesapplication.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;
import au.com.target.tgtsale.salesapplication.impl.TargetSalesSalesApplicationConfigServiceImpl;


/**
 * @author ragarwa3
 *
 */
public class TargetFulfilmentSalesApplicationConfigServiceImpl extends TargetSalesSalesApplicationConfigServiceImpl
        implements TargetFulfilmentSalesApplicationConfigService {

    @Override
    public boolean isDenyShortPicks(final SalesApplication salesApp) {

        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isDenyShortPicks()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isSuppressAutoRefund(final SalesApplication salesApp) {
        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isSuppressAutoRefund()) {
            return true;
        }

        return false;
    }

}
