/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.util.Assert;

import au.com.target.tgtcore.order.strategies.TaxInvoiceGenerationDenialStrategy;


public class OrderStatusTaxInvoiceGenerationDenialStrategy implements TaxInvoiceGenerationDenialStrategy {

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.order.strategies.TaxInvoiceGenerateDenialStrategy#isDenied(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        Assert.notNull(order, "Order can't be null!");

        if (OrderStatus.INVOICED.equals(order.getStatus()) || OrderStatus.COMPLETED.equals(order.getStatus())) {
            return false;
        }

        return true;
    }

}
