/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author ayushman
 *
 */
public class SendSmsToStoreForOpenOrdersJob extends AbstractJobPerformable<CronJobModel> {
    private static final Logger LOG = Logger.getLogger(SendSmsToStoreForOpenOrdersJob.class);

    private TargetStoreConsignmentService targetStoreConsignmentService;
    private TargetBusinessProcessService targetBusinessProcessService;

    /**
     * (non-Javadoc)
     * 
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel paramT) {
        final Map<TargetPointOfServiceModel, Integer> posAndOpenOrdersForStores = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();

        if (MapUtils.isNotEmpty(posAndOpenOrdersForStores)) {
            final SendSmsToStoreData sendSmsToStoreData = new SendSmsToStoreData();
            for (final Entry<TargetPointOfServiceModel, Integer> entry : posAndOpenOrdersForStores.entrySet()) {
                final TargetPointOfServiceModel targetPointOfServiceModel = entry.getKey();
                sendSmsToStoreData.setMobileNumber(targetPointOfServiceModel.getFulfilmentCapability()
                        .getMobileNumber());
                sendSmsToStoreData.setOpenOrders(entry.getValue().intValue());
                sendSmsToStoreData.setStoreNumber(String.valueOf(targetPointOfServiceModel.getStoreNumber()));

                try {
                    targetBusinessProcessService.startSendSmsToStoreForOpenOrdersProcess(sendSmsToStoreData);
                }
                catch (final Exception e) {
                    LOG.error(SplunkLogFormatter.formatMessage(
                            "Problem sending SMS to store: " + sendSmsToStoreData.getStoreNumber()
                                    + " with mobile number: " + sendSmsToStoreData.getMobileNumber(),
                            TgtutilityConstants.ErrorCode.ERR_TGTFULLFILLMENT));
                }
            }
        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

}
