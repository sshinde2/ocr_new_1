/**
 * 
 */
package au.com.target.tgtfulfilment.dao.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.jalo.Consignment;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentDaoUtilTest {

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final TargetFulfilmentDaoUtil<ConsignmentModel> consignments = new TargetFulfilmentDaoUtil<>();


    @Test
    public void refreshModelListEmptyList() {
        consignments.refreshModelList(new ArrayList<ConsignmentModel>());
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void refreshModelListNullList() {
        consignments.refreshModelList(null);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test(expected = ClassCastException.class)
    public void refreshModelListNullNonItemModelType() {
        final TargetFulfilmentDaoUtil consignment = new TargetFulfilmentDaoUtil<>();
        consignment.refreshModelList(Collections.singletonList(new Consignment()));
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void refreshModelListValidList() {
        final ArrayList<ConsignmentModel> arrayList = new ArrayList<>();
        arrayList.add(new ConsignmentModel());
        arrayList.add(new ConsignmentModel());
        consignments.refreshModelList(arrayList);
        Mockito.verify(modelService, Mockito.times(2)).refresh(Mockito.any(ConsignmentModel.class));
        Mockito.verifyNoMoreInteractions(modelService);
    }
}
