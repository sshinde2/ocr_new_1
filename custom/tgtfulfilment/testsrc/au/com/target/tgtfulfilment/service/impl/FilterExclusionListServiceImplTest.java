/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;

import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;

import com.google.common.collect.Sets;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FilterExclusionListServiceImplTest {
    private Set<ProductExclusionsModel> input = null;
    private Set<ProductExclusionsModel> result = null;

    private Set<CategoryExclusionsModel> catList = null;

    @Mock
    private ProductExclusionsModel productExclusion1;

    @Mock
    private ProductExclusionsModel productExclusion2;

    @Mock
    private ProductExclusionsModel productExclusion3;

    @Mock
    private CategoryExclusionsModel catExclusion1;

    @Mock
    private CategoryExclusionsModel catExclusion2;

    @Mock
    private CategoryExclusionsModel catExclusion3;

    @Mock
    private CategoryModel categoryModel1;

    @Mock
    private CategoryModel categoryModel2;

    @Mock
    private CategoryModel categoryModel3;


    @InjectMocks
    private final FilterExclusionListServiceImpl filterListService = new FilterExclusionListServiceImpl();


    @Before
    public void setUp() {
        input = Sets.newHashSet(productExclusion1, productExclusion2, productExclusion3);
        catList = Sets.newHashSet(catExclusion1, catExclusion2, catExclusion3);

    }

    @Test
    public void testFilteredProductListWithBackDatedStartDate() {
        //startDate in past and enddate in future
        Mockito.when(productExclusion1.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        Mockito.when(productExclusion1.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        //startDate in past and enddate in past
        Mockito.when(productExclusion2.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(100).toDate());
        Mockito.when(productExclusion2.getExclusionEndDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        //startDate in past and enddate in null
        Mockito.when(productExclusion3.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        Mockito.when(productExclusion3.getExclusionEndDate()).thenReturn(null);

        result = filterListService.filterActiveProductExclusions(input);

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(productExclusion1));
        Assert.assertFalse(result.contains(productExclusion2));
        Assert.assertTrue(result.contains(productExclusion3));
    }

    @Test
    public void testFilteredProductListWithFutureDatedStartDate() {
        //startDate in future and enddate in future
        Mockito.when(productExclusion1.getExclusionStartDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(productExclusion1.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(100).toDate());
        //startDate in future and enddate is null
        Mockito.when(productExclusion2.getExclusionStartDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(productExclusion2.getExclusionEndDate()).thenReturn(null);
        //startDate in past and enddate in future
        Mockito.when(productExclusion3.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        Mockito.when(productExclusion3.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(10).toDate());

        result = filterListService.filterActiveProductExclusions(input);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertFalse(result.contains(productExclusion1));
        Assert.assertFalse(result.contains(productExclusion2));
        Assert.assertTrue(result.contains(productExclusion3));
    }

    @Test
    public void testFilteredProductListWithNulls() {
        //startDate and end date null
        Mockito.when(productExclusion1.getExclusionStartDate()).thenReturn(null);
        Mockito.when(productExclusion1.getExclusionEndDate()).thenReturn(null);
        //startDate is null and end date in future
        Mockito.when(productExclusion2.getExclusionStartDate()).thenReturn(null);
        Mockito.when(productExclusion2.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        //startDate is null and end date in past
        Mockito.when(productExclusion3.getExclusionStartDate()).thenReturn(null);
        Mockito.when(productExclusion3.getExclusionEndDate()).thenReturn(new DateTime().minusMinutes(10).toDate());

        result = filterListService.filterActiveProductExclusions(input);

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(productExclusion1));
        Assert.assertTrue(result.contains(productExclusion2));
        Assert.assertFalse(result.contains(productExclusion3));
    }

    @Test
    public void testFilteredCategoryListWithBackDatedStartDate() {
        //startDate in past and enddate in past
        Mockito.when(catExclusion1.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(100).toDate());
        Mockito.when(catExclusion1.getExclusionEndDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        Mockito.when(catExclusion1.getCategory()).thenReturn(categoryModel1);
        //startDate in past and enddate in future
        Mockito.when(catExclusion2.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        Mockito.when(catExclusion2.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(catExclusion2.getCategory()).thenReturn(categoryModel2);
        //startDate in past and enddate in null
        Mockito.when(catExclusion3.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(10).toDate());
        Mockito.when(catExclusion3.getExclusionEndDate()).thenReturn(null);
        Mockito.when(catExclusion3.getCategory()).thenReturn(categoryModel3);


        final List<CategoryModel> excludeCategories = filterListService.filterActiveCategoryModelsByDateRange(catList);

        Assert.assertNotNull(excludeCategories);
        Assert.assertEquals(2, excludeCategories.size());
        Assert.assertFalse(excludeCategories.contains(categoryModel1));
        Assert.assertTrue(excludeCategories.contains(categoryModel2));
        Assert.assertTrue(excludeCategories.contains(categoryModel3));
    }


    @Test
    public void testFilteredCategoryListWithFutureDatedStartDate() {
        //startDate in past and enddate in future
        Mockito.when(catExclusion1.getExclusionStartDate()).thenReturn(new DateTime().minusMinutes(100).toDate());
        Mockito.when(catExclusion1.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(catExclusion1.getCategory()).thenReturn(categoryModel1);
        //startDate in future and enddate is null
        Mockito.when(catExclusion2.getExclusionStartDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(catExclusion2.getExclusionEndDate()).thenReturn(null);
        Mockito.when(catExclusion2.getCategory()).thenReturn(categoryModel2);
        //startDate in future and enddate in future
        Mockito.when(catExclusion3.getExclusionStartDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(catExclusion3.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(100).toDate());
        Mockito.when(catExclusion3.getCategory()).thenReturn(categoryModel3);

        final List<CategoryModel> excludeCategories = filterListService.filterActiveCategoryModelsByDateRange(catList);

        Assert.assertNotNull(excludeCategories);
        Assert.assertEquals(1, excludeCategories.size());
        Assert.assertFalse(excludeCategories.contains(categoryModel2));
        Assert.assertTrue(excludeCategories.contains(categoryModel1));
        Assert.assertFalse(excludeCategories.contains(categoryModel3));

    }


    @Test
    public void testFilteredCategoryListWithNulls() {
        //startDate is null and end date in future
        Mockito.when(catExclusion1.getExclusionStartDate()).thenReturn(null);
        Mockito.when(catExclusion1.getExclusionEndDate()).thenReturn(new DateTime().plusMinutes(10).toDate());
        Mockito.when(catExclusion1.getCategory()).thenReturn(categoryModel1);
        //startDate and end date null
        Mockito.when(catExclusion2.getExclusionStartDate()).thenReturn(null);
        Mockito.when(catExclusion2.getExclusionEndDate()).thenReturn(null);
        Mockito.when(catExclusion2.getCategory()).thenReturn(categoryModel2);
        //startDate is null and end date in past
        Mockito.when(catExclusion3.getExclusionStartDate()).thenReturn(null);
        Mockito.when(catExclusion3.getExclusionEndDate()).thenReturn(new DateTime().minusMinutes(100).toDate());
        Mockito.when(catExclusion3.getCategory()).thenReturn(categoryModel3);

        final List<CategoryModel> excludeCategories = filterListService.filterActiveCategoryModelsByDateRange(catList);

        Assert.assertNotNull(excludeCategories);
        Assert.assertEquals(2, excludeCategories.size());
        Assert.assertTrue(excludeCategories.contains(categoryModel2));
        Assert.assertTrue(excludeCategories.contains(categoryModel1));
        Assert.assertFalse(excludeCategories.contains(categoryModel3));


    }
}
