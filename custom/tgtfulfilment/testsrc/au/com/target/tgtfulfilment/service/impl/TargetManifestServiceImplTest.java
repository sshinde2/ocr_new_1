/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.manifest.strategy.TargetGenerateManifestCodeStrategy;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetManifestDao;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * Test class for TargetManifestServiceImpl
 * 
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetManifestServiceImplTest {

    private static final int STORE_NUMBER = 1000;

    private static final String MANIFEST_CODE = "testManifest";

    @Mock
    private TargetGenerateManifestCodeStrategy targetGenerateManifestCodeStrategy;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private TargetPointOfServiceModel pointOfService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetManifestDao targetManifestDao;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetManifestModel targetManifestModel;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final TargetManifestServiceImpl targetManifestServiceImpl = new TargetManifestServiceImpl();

    @Before
    public void setUp() {
        given(modelService.create(TargetManifestModel.class)).willReturn(new TargetManifestModel());
        given(pointOfService.getStoreNumber()).willReturn(Integer.valueOf(STORE_NUMBER));
        given(targetGenerateManifestCodeStrategy.generateManifestCode(STORE_NUMBER)).willReturn(
                String.valueOf("1000011001"));
    }

    @Test
    public void testCreateManifestWhenThereAreConsignments() {

        final List<TargetConsignmentModel> consignmentModels = new ArrayList<>();
        final TargetConsignmentModel consignment1 = new TargetConsignmentModel();
        final TargetConsignmentModel consignment2 = new TargetConsignmentModel();
        consignmentModels.add(consignment1);
        consignmentModels.add(consignment2);

        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER)).willReturn(
                consignmentModels);

        final TargetManifestModel manifest = targetManifestServiceImpl.createManifest(pointOfService);

        assertThat(manifest).isNotNull();

        final Set<TargetConsignmentModel> resultCons = manifest.getConsignments();
        assertThat(resultCons).hasSize(2);
        assertThat(manifest.getCode()).isEqualTo("1000011001");
        assertThat(manifest.getDate()).isNotNull();

        for (final TargetConsignmentModel con : resultCons) {
            assertThat(con.getStatus()).isEqualTo(ConsignmentStatus.SHIPPED);
        }
    }

    @Test
    public void testCreateManifestWhenThereAreConsignmentsWithWrongOrderType() {

        final List<TargetConsignmentModel> consignmentModels = new ArrayList<>();

        given(consignment.getOfcOrderType()).willReturn(OfcOrderType.INSTORE_PICKUP);
        consignmentModels.add(consignment);

        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER)).willReturn(
                consignmentModels);

        final TargetManifestModel manifest = targetManifestServiceImpl.createManifest(pointOfService);

        assertThat(manifest).isNull();
    }

    @Test
    public void testCreateManifestWhenThereAreConsignmentsWithManifest() {

        final List<TargetConsignmentModel> consignmentModels = new ArrayList<>();

        given(consignment.getManifest()).willReturn(new TargetManifestModel());
        consignmentModels.add(consignment);

        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER)).willReturn(
                consignmentModels);

        final TargetManifestModel manifest = targetManifestServiceImpl.createManifest(pointOfService);

        assertThat(manifest).isNull();
    }


    @Test
    public void testCreateManifestWhenThereAreNoConsignments() {

        final List<TargetConsignmentModel> consignmentModels = new ArrayList<>();
        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER)).willReturn(
                consignmentModels);

        try {
            targetManifestServiceImpl.createManifest(pointOfService);
        }
        catch (final UnsupportedOperationException e) {
            assertThat(e.getMessage())
                    .isEqualTo("Manifest Cannnot be created as there are no unmanifested consignments for the store.");
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetManifestByCodeNull() throws NotFoundException {
        targetManifestServiceImpl.getManifestByCode(null);
    }

    @Test(expected = RuntimeException.class)
    public void testGetManifestByCodeRuntimeException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, NotFoundException {
        given(targetManifestDao.getManifestByCode(MANIFEST_CODE)).willThrow(
                new TargetAmbiguousIdentifierException(""));

        targetManifestServiceImpl.getManifestByCode(MANIFEST_CODE);
    }

    @Test(expected = NotFoundException.class)
    public void testGetManifestByCodeNotFoundException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, NotFoundException {
        given(targetManifestDao.getManifestByCode(MANIFEST_CODE)).willThrow(
                new TargetUnknownIdentifierException(""));

        targetManifestServiceImpl.getManifestByCode(MANIFEST_CODE);
    }

    @Test
    public void testGetManifestByCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            NotFoundException {
        final TargetManifestModel manifestModel = new TargetManifestModel();
        given(targetManifestDao.getManifestByCode(MANIFEST_CODE)).willReturn(
                manifestModel);
        final TargetManifestModel manifest = targetManifestServiceImpl
                .getManifestByCode(MANIFEST_CODE);
        assertThat(manifest).isEqualTo(manifestModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetManifestsHistoryForStoreNull() {
        targetManifestServiceImpl.getManifestsHistoryForStore(null);
    }

    @Test
    public void testGetManifestsHistoryForStoreNoManifests() {
        given(targetManifestDao.getManifestsHistoryForStore(pointOfService)).willReturn(ListUtils.EMPTY_LIST);
        assertThat(CollectionUtils.isEmpty(targetManifestServiceImpl.getManifestsHistoryForStore(pointOfService)))
                .isTrue();
    }

    @Test
    public void testGetManifestsHistoryForStoreSomeManifests() {
        final TargetManifestModel manifest1 = new TargetManifestModel();
        final TargetManifestModel manifest2 = new TargetManifestModel();
        given(targetManifestDao.getManifestsHistoryForStore(pointOfService)).willReturn(
                Arrays.asList(manifest1, manifest2));

        final List<TargetManifestModel> manifestHistory = targetManifestServiceImpl
                .getManifestsHistoryForStore(pointOfService);
        assertThat(manifestHistory).isNotEmpty();
        assertThat(manifestHistory).hasSize(2);
    }

    @Test
    public void testCompleteConsignmentsInManifestWithConsignments() {

        final OrderModel order = Mockito.mock(OrderModel.class);
        given(consignment.getOrder()).willReturn(order);
        final Set<TargetConsignmentModel> consignmentModels = Collections.singleton(consignment);
        given(targetManifestModel.getConsignments()).willReturn(consignmentModels);
        targetManifestServiceImpl.completeConsignmentsInManifest(targetManifestModel, Integer.valueOf(STORE_NUMBER));
        verify(targetBusinessProcessService).startOrderConsignmentProcess(order, consignment,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
    }

    @Test
    public void testCompleteConsignmentsInManifestWithNoConsignments() {

        given(targetManifestModel.getConsignments()).willReturn(Collections.EMPTY_SET);
        targetManifestServiceImpl.completeConsignmentsInManifest(targetManifestModel, Integer.valueOf(STORE_NUMBER));
        verifyZeroInteractions(targetBusinessProcessService);
    }

}
