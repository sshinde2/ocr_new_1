/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;


/**
 * Unit test for {@link PickCancelServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickCancelServiceImplTest {

    private static final Double FULL_SHIP_REFUND = Double.valueOf(9d);
    private static final String REFUND_TICKET_MESSAGE = " Order Short/Zero pick is failed due to failure of refund | Refund information: {0}.";
    private static final String SHORTPICK_PRODUCT_MSG = " The following products were not fulfilled due to a short/Zero-pick:[]";

    private static final String REFUND_TICKET_HEAD = "Short or Zero pick refund failed";

    @Mock
    private TargetFulfilmentHelper targetFulfilmentHelper;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @Mock
    private TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy;

    @InjectMocks
    private final PickCancelServiceImpl pickCancelService = new PickCancelServiceImpl();

    @Mock
    private OrderModel order;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private PickConsignmentUpdateData data;

    @Mock
    private CancelRequestResponse response;

    @Mock
    private TargetRefundPaymentService targetRefundPaymentService;

    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;

    @Mock
    private TargetBusinessProcessService businessProcess;

    @Mock
    private PaymentTransactionEntryDTO paymentEntry;

    @Mock
    private PaymentTransactionEntryModel entryModel;

    @Mock
    private OrderCancelEntry cancelEntry1;

    @Mock
    private OrderCancelEntry cancelEntry2;

    @Mock
    private OrderEntryModel orderEntry1;

    @Mock
    private OrderEntryModel orderEntry2;

    @Mock
    private ProductModel product1;

    @Mock
    private ProductModel product2;

    @Mock
    private TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @SuppressWarnings("boxing")
    @Before
    public void setup() {
        Mockito.when(order.getCode()).thenReturn("123");

        Mockito.when(
                targetFindDeliveryCostStrategy.deliveryCostOutstandingForOrder(Mockito.any(OrderModel.class)))
                .thenReturn(FULL_SHIP_REFUND);
        // Setup consignment entry
        Mockito.when(consignment.getConsignmentEntries()).thenReturn(Collections.singleton(consignmentEntry));
        Mockito.when(targetFulfilmentHelper.getConsignmentEntryItemCode(consignmentEntry)).thenReturn("product1");
        Mockito.when(consignmentEntry.getOrderEntry()).thenReturn(orderEntry);
        given(cancelEntry1.getOrderEntry()).willReturn(orderEntry1);
        given(cancelEntry2.getOrderEntry()).willReturn(orderEntry2);
        given(orderEntry1.getProduct()).willReturn(product1);
        given(orderEntry2.getProduct()).willReturn(product2);
        given(product1.getCode()).willReturn("CP2012");
        given(product2.getCode()).willReturn("CP2010");
        given(Boolean.valueOf(salesApplicationConfigService.isSuppressAutoRefund(SalesApplication.TRADEME)))
                .willReturn(Boolean.TRUE);
    }

    private void setOrderedQuantity(final long quantity) {
        Mockito.when(consignmentEntry.getQuantity()).thenReturn(Long.valueOf(quantity));
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(quantity));
    }

    @SuppressWarnings("boxing")
    private void setShippedQuantity(final long quantity) {
        Mockito.when(data.getShippedQuantity("product1")).thenReturn(quantity);
    }


    @Test
    public void testGenerateCancelEntriesNoEntries() throws FulfilmentException {

        expectedException.expect(FulfilmentException.class);

        Mockito.when(consignment.getConsignmentEntries()).thenReturn(null);

        pickCancelService.generateCancelEntries(consignment, data, "123", "Cancel note");
    }

    @Test
    public void testGenerateCancelEntriesFullPick() throws FulfilmentException {

        setOrderedQuantity(2);
        setShippedQuantity(2);

        final List<OrderCancelEntry> orderCancelEntries = pickCancelService.generateCancelEntries(consignment, data,
                "123", "Cancel note");

        Assert.assertNotNull(orderCancelEntries);
        Assert.assertTrue(CollectionUtils.isEmpty(orderCancelEntries));
    }

    @Test
    public void testGenerateCancelEntriesShortPick() throws FulfilmentException {

        setOrderedQuantity(3);
        setShippedQuantity(1);

        final List<OrderCancelEntry> orderCancelEntries = pickCancelService.generateCancelEntries(consignment, data,
                "123", "Cancel note");

        Assert.assertNotNull(orderCancelEntries);
        Assert.assertEquals(1, orderCancelEntries.size());

        final OrderCancelEntry entry = orderCancelEntries.get(0);

        Assert.assertEquals(2, entry.getCancelQuantity());
        Assert.assertEquals(CancelReason.OUTOFSTOCK, entry.getCancelReason());
        Assert.assertEquals("Cancel note", entry.getNotes());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGenerateCancelEntriesZeroPick() throws FulfilmentException {

        setOrderedQuantity(3);
        setShippedQuantity(0);

        final List<OrderCancelEntry> orderCancelEntries = pickCancelService.generateCancelEntries(consignment, data,
                "123", "Cancel note");

        Assert.assertNotNull(orderCancelEntries);
        Assert.assertEquals(1, orderCancelEntries.size());

        final OrderCancelEntry entry = orderCancelEntries.get(0);

        Assert.assertEquals(3, entry.getCancelQuantity());
        Assert.assertEquals(CancelReason.OUTOFSTOCK, entry.getCancelReason());
        Assert.assertEquals("Cancel note", entry.getNotes());
    }

    @Test
    public void testCreateCancelRequestNoEntries() throws FulfilmentException {

        final String formattedCancelNote = "CancelNote";

        final PickCancelServiceImpl spy = Mockito.spy(pickCancelService);
        Mockito.doReturn(null).when(spy).generateCancelEntries(Mockito.any(ConsignmentModel.class),
                Mockito.any(PickConsignmentUpdateData.class), Mockito.anyString(), Mockito.anyString());

        final TargetOrderCancelRequest cancelRequest = spy.createCancelRequest(order, consignment, data,
                CancelTypeEnum.SHORT_PICK, formattedCancelNote);
        Assert.assertNull(cancelRequest);
    }

    @Test
    public void testCreateCancelRequestShort() throws FulfilmentException {

        final String formattedCancelNote = "CancelNote";

        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(3));
        Mockito.when(orderEntry.getOrder()).thenReturn(order);

        final OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntry, 1, formattedCancelNote);
        final List<OrderCancelEntry> orderCancelEntries = Collections.singletonList(cancelEntry);

        final PickCancelServiceImpl spy = Mockito.spy(pickCancelService);
        Mockito.doReturn(orderCancelEntries).when(spy).generateCancelEntries(Mockito.any(ConsignmentModel.class),
                Mockito.any(PickConsignmentUpdateData.class), Mockito.anyString(), Mockito.anyString());

        final TargetOrderCancelRequest cancelRequest = spy.createCancelRequest(order, consignment, data,
                CancelTypeEnum.SHORT_PICK, formattedCancelNote);

        Assert.assertNotNull(cancelRequest);
        Assert.assertEquals(orderCancelEntries, cancelRequest.getEntriesToCancel());
        Assert.assertEquals(CancelReason.OUTOFSTOCK, cancelRequest.getCancelReason());
        Assert.assertEquals(formattedCancelNote, cancelRequest.getNotes());
        Assert.assertEquals(order, cancelRequest.getOrder());
    }

    @Test
    public void testCreateCancelRequestZero() throws FulfilmentException {

        final String formattedCancelNote = "CancelNote";

        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(3));
        Mockito.when(orderEntry.getOrder()).thenReturn(order);

        final OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntry, 1, formattedCancelNote);
        final List<OrderCancelEntry> orderCancelEntries = Collections.singletonList(cancelEntry);

        final PickCancelServiceImpl spy = Mockito.spy(pickCancelService);
        Mockito.doReturn(orderCancelEntries).when(spy).generateCancelEntries(Mockito.any(ConsignmentModel.class),
                Mockito.any(PickConsignmentUpdateData.class), Mockito.anyString(), Mockito.anyString());

        final TargetOrderCancelRequest cancelRequest = spy.createCancelRequest(order, consignment, data,
                CancelTypeEnum.ZERO_PICK, formattedCancelNote);

        Assert.assertNotNull(cancelRequest);
        Assert.assertEquals(orderCancelEntries, cancelRequest.getEntriesToCancel());
        Assert.assertEquals(CancelReason.OUTOFSTOCK, cancelRequest.getCancelReason());
        Assert.assertEquals(formattedCancelNote, cancelRequest.getNotes());
        Assert.assertEquals(order, cancelRequest.getOrder());
        Assert.assertEquals(RequestOrigin.WAREHOUSE, cancelRequest.getRequestOrigin());
    }

    @Test
    public void testStartPickCancelProcessTradeMeZeroPick() throws FulfilmentException, OrderCancelException {
        final BigDecimal refundAmountRemaining = BigDecimal.valueOf(15.0d);

        final String formattedCancelNote = "CancelNote";
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(3));
        Mockito.when(orderEntry.getOrder()).thenReturn(order);
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.TRADEME);
        final OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntry, 1, formattedCancelNote);
        final List<OrderCancelEntry> orderCancelEntries = Collections.singletonList(cancelEntry);
        final PickCancelServiceImpl spy = Mockito.spy(pickCancelService);
        Mockito.doReturn(orderCancelEntries).when(spy).generateCancelEntries(Mockito.any(ConsignmentModel.class),
                Mockito.any(PickConsignmentUpdateData.class), Mockito.anyString(), Mockito.anyString());
        Mockito.doReturn(response).when(spy).processCancelRequest(Mockito.any(TargetOrderCancelRequest.class));
        Mockito.doReturn(businessProcess).when(spy).getTargetBusinessProcessService();

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        spy.startPickCancelProcess(order, targetConsignment, data, PickTypeEnum.ZERO);

        verify(businessProcess).startConsignmentPickedCancelProcess(eq(order), eq(targetConsignment), eq(data),
                (OrderCancelRecordEntryModel)isNull(), (BigDecimal)isNull(), eq(CancelTypeEnum.ZERO_PICK), anyList(),
                eq(refundAmountRemaining));
    }

    @Test
    public void testStartPickCancelProcessEbayZeroPick() throws FulfilmentException, OrderCancelException {
        final String formattedCancelNote = "CancelNote";
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(3));
        Mockito.when(orderEntry.getOrder()).thenReturn(order);
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        final OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntry, 1, formattedCancelNote);
        final List<OrderCancelEntry> orderCancelEntries = Collections.singletonList(cancelEntry);
        final PickCancelServiceImpl spy = Mockito.spy(pickCancelService);
        Mockito.doReturn(orderCancelEntries).when(spy).generateCancelEntries(Mockito.any(ConsignmentModel.class),
                Mockito.any(PickConsignmentUpdateData.class), Mockito.anyString(), Mockito.anyString());
        Mockito.doReturn(response).when(spy).processCancelRequest(Mockito.any(TargetOrderCancelRequest.class));
        Mockito.doReturn(businessProcess).when(spy).getTargetBusinessProcessService();

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        spy.startPickCancelProcess(order, targetConsignment, data, PickTypeEnum.ZERO);

        Mockito.verify(targetRefundPaymentService).createRefundedInfo(Collections.EMPTY_LIST, null);
    }


    @Test
    public void testStartPickCancelProcessShortPick() throws FulfilmentException, OrderCancelException {
        final BigDecimal refundAmountRemaining = BigDecimal.valueOf(15.0d);
        final BigDecimal refundAmt = BigDecimal.valueOf(100.0d);
        final List<PaymentTransactionEntryDTO> paymentList = Collections.singletonList(paymentEntry);
        final List<PaymentTransactionEntryModel> listPaymentEntries = Collections.singletonList(entryModel);
        final String formattedCancelNote = String.format("Zero or Short Pick happened on Consignment - {%s}",
                consignment.getCode());
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(3));
        Mockito.when(orderEntry.getOrder()).thenReturn(order);
        Mockito.when(order.getCode()).thenReturn("1234");
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);
        final ArgumentCaptor<TargetOrderCancelRequest> argCancelRequest = ArgumentCaptor
                .forClass(TargetOrderCancelRequest.class);
        final OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntry, 1, formattedCancelNote);
        final List<OrderCancelEntry> orderCancelEntries = Collections.singletonList(cancelEntry);
        final PickCancelServiceImpl spy = Mockito.spy(pickCancelService);
        Mockito.doReturn(orderCancelEntries).when(spy).generateCancelEntries(targetConsignment,
                data, "1234", formattedCancelNote);
        Mockito.when(response.getFollowOnRefundPaymentDataList()).thenReturn(paymentList);
        Mockito.when(response.getRefundAmount()).thenReturn(refundAmt);
        Mockito.when(response.getFollowOnRefundPaymentList()).thenReturn(listPaymentEntries);

        Mockito.doReturn(response).when(spy).processCancelRequest(argCancelRequest.capture());
        Mockito.doReturn(businessProcess).when(spy).getTargetBusinessProcessService();
        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        refundInfo.setMessage("Refund Failed");
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);
        given(targetRefundPaymentService.createRefundedInfo(paymentList, refundAmt)).willReturn(refundInfo);
        spy.startPickCancelProcess(order, targetConsignment, data, PickTypeEnum.SHORT);
        final TargetOrderCancelRequest cancelRequest = argCancelRequest.getValue();
        Assert.assertEquals(formattedCancelNote, cancelRequest.getNotes());
        Assert.assertEquals(CancelReason.OUTOFSTOCK, cancelRequest.getCancelReason());
        Assert.assertEquals(order, cancelRequest.getOrder());
        final String refundMessage = MessageFormat.format(REFUND_TICKET_MESSAGE, refundInfo.getMessage())
                + SHORTPICK_PRODUCT_MSG;

        verify(targetTicketBusinessService).createCsAgentTicket(REFUND_TICKET_HEAD, REFUND_TICKET_HEAD, refundMessage,
                order, TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
        verify(businessProcess).startConsignmentPickedCancelProcess(eq(order), eq(targetConsignment), eq(data),
                (OrderCancelRecordEntryModel)isNull(), eq(refundAmt), eq(CancelTypeEnum.SHORT_PICK),
                eq(listPaymentEntries),
                eq(refundAmountRemaining));
    }

    @Test
    public void testPopulateCancelledProductCodesWithNullList() {
        final List<String> productCodes = pickCancelService.populateCancelledProductCodes(null);
        Assert.assertEquals(productCodes.size(), 0);
    }

    @Test
    public void testPopulateCancelledProductCodesWithEmptyList() {
        final List<String> productCodes = pickCancelService
                .populateCancelledProductCodes(new ArrayList<OrderCancelEntry>());
        Assert.assertEquals(productCodes.size(), 0);
    }

    @Test
    public void testPopulateCancelledProductCodesWithEntries() {
        final List<OrderCancelEntry> entries = new ArrayList<>();
        entries.add(cancelEntry1);
        entries.add(cancelEntry2);
        final List<String> productCodes = pickCancelService
                .populateCancelledProductCodes(entries);
        Assert.assertEquals(productCodes.size(), 2);
        Assert.assertEquals(productCodes.get(0), "CP2012");
        Assert.assertEquals(productCodes.get(1), "CP2010");
    }
}
