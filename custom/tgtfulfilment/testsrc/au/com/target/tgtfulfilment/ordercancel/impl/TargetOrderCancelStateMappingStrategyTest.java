/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.OrderCancelState;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * Unit test for {@link TargetOrderCancelStateMappingStrategy}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderCancelStateMappingStrategyTest {

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @InjectMocks
    private final TargetOrderCancelStateMappingStrategy targetOrderCancelStateMappingStrategy = new TargetOrderCancelStateMappingStrategy();

    @Mock
    private OrderModel order;

    private final List<TargetConsignmentModel> consignments = new ArrayList<>();

    @Mock
    private TargetConsignmentModel con1;

    @Mock
    private TargetConsignmentModel con2;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;


    @Before
    public void setup() {
        final List<OrderStatus> cancelDeniedOrderStates = Collections.singletonList(OrderStatus.COMPLETED);
        final List<ConsignmentStatus> cancelDeniedConsignmentStates = Collections
                .singletonList(ConsignmentStatus.SHIPPED);
        final List<ConsignmentStatus> cancelRequiredConsignmentStates = Collections
                .singletonList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        targetOrderCancelStateMappingStrategy.setCancelDeniedOrderStates(cancelDeniedOrderStates);
        targetOrderCancelStateMappingStrategy.setCancelDeniedConsignmentStates(cancelDeniedConsignmentStates);
        targetOrderCancelStateMappingStrategy.setCancelRequiredConsignmentStates(cancelRequiredConsignmentStates);

        given(targetConsignmentService.getActiveConsignmentsForOrder(order)).willReturn(consignments);
        consignments.add(con1);
        consignments.add(con2);
    }

    @Test
    public void testCancelledDueToOrderStatus() {

        given(order.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(con1.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(con2.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        final OrderCancelState result = targetOrderCancelStateMappingStrategy.getOrderCancelState(order);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(OrderCancelState.CANCELIMPOSSIBLE);

    }

    @Test
    public void testCancelledDueToDeniedConsignmentStatus() {

        given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(con1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(con2.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        final OrderCancelState result = targetOrderCancelStateMappingStrategy.getOrderCancelState(order);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(OrderCancelState.PENDINGORHOLDINGAREA);
    }

    @Test
    public void testCancelledDueToRequiredConsignmentStatus() {

        given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(con1.getStatus()).willReturn(ConsignmentStatus.CREATED);
        given(con2.getStatus()).willReturn(ConsignmentStatus.CREATED);

        final OrderCancelState result = targetOrderCancelStateMappingStrategy.getOrderCancelState(order);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(OrderCancelState.PENDINGORHOLDINGAREA);
    }

    @Test
    public void testCancelledOk() {

        given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(con1.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(con2.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        final OrderCancelState result = targetOrderCancelStateMappingStrategy.getOrderCancelState(order);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(OrderCancelState.PENDINGORHOLDINGAREA);
    }

    @Test
    public void testCancelledOkForWavedStatusWhenFluentOn() {
        given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(con1.getStatus()).willReturn(ConsignmentStatus.WAVED);
        given(con2.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final OrderCancelState result = targetOrderCancelStateMappingStrategy.getOrderCancelState(order);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(OrderCancelState.PENDINGORHOLDINGAREA);

    }

}
