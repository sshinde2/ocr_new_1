/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;


/**
 * Tests for SendToWarehouseConsignmentEntryConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseConsignmentEntryConverterTest {

    @Mock
    private SendToWarehouseProductConverter sendToWarehouseProductConverter;

    @Mock
    private SendToWarehouseRecipientConverter sendToWarehouseRecipientConverter;

    @InjectMocks
    private final SendToWarehouseConsignmentEntryConverter sendToWarehouseConsignmentEntryConverter = new SendToWarehouseConsignmentEntryConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testNullConsignmentEntry() {
        sendToWarehouseConsignmentEntryConverter.convert(null);
    }

    @Test
    public void testEmptyConsignmentEntry() {
        final ConsignmentEntryDTO entryDTO = sendToWarehouseConsignmentEntryConverter
                .convert(new ConsignmentEntryModel());
        Assert.assertNotNull(entryDTO);
        Assert.assertNull(entryDTO.getProduct());
        Assert.assertNull(entryDTO.getRecipients());
    }

    @Test
    public void testWithoutRecipients() {
        final ConsignmentEntryModel consignmentEntry = new ConsignmentEntryModel();
        final AbstractOrderEntryModel orderEntry = new AbstractOrderEntryModel();
        orderEntry.setProduct(new AbstractTargetVariantProductModel());
        consignmentEntry.setOrderEntry(orderEntry);

        BDDMockito.given(sendToWarehouseProductConverter.convert(Mockito.any(AbstractTargetVariantProductModel.class)))
                .willReturn(
                        new ProductDTO());
        final ConsignmentEntryDTO entryDTO = sendToWarehouseConsignmentEntryConverter
                .convert(consignmentEntry);
        Assert.assertNotNull(entryDTO);
        Assert.assertNotNull(entryDTO.getProduct());
        Assert.assertNull(entryDTO.getRecipients());
        Assert.assertNotNull(entryDTO.getProduct());
    }

    @Test
    public void testWithProductsAndRecipients() {
        final ConsignmentEntryModel consignmentEntry = new ConsignmentEntryModel();
        final AbstractOrderEntryModel orderEntry = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel variantProduct = new AbstractTargetVariantProductModel();
        final TargetProductModel baseProduct = new TargetProductModel();
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode("digital");
        baseProduct.setProductType(productType);
        variantProduct.setBaseProduct(baseProduct);
        orderEntry.setProduct(variantProduct);

        final GiftRecipientModel recipient = new GiftRecipientModel();
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        recipients.add(recipient);
        orderEntry.setGiftRecipients(recipients);
        consignmentEntry.setOrderEntry(orderEntry);

        given(sendToWarehouseProductConverter.convert(variantProduct))
                .willReturn(
                        new ProductDTO());
        given(sendToWarehouseRecipientConverter.convert(recipient))
                .willReturn(
                        new RecipientDTO());
        final ConsignmentEntryDTO entryDTO = sendToWarehouseConsignmentEntryConverter
                .convert(consignmentEntry);
        assertThat(entryDTO).isNotNull();
        assertThat(entryDTO.getProduct()).isNotNull();
        assertThat(entryDTO.getRecipients()).isNotNull();
        assertThat(entryDTO.getRecipients()).isNotEmpty();
        assertThat(entryDTO.getRecipients()).hasSize(1);
        assertThat(entryDTO.getRecipients().get(0)).isNotNull();
    }

    @Test
    public void testWithPhysicalGiftcardAndRecipients() {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);

        final AbstractTargetVariantProductModel product = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel model = mock(TargetProductModel.class);
        final ProductTypeModel productType = mock(ProductTypeModel.class);
        final AddressModel deliveryAddres = mock(AddressModel.class);
        final CountryModel countryModel = mock(CountryModel.class);

        given(countryModel.getIsocode()).willReturn("AU");


        given(deliveryAddres.getFirstname()).willReturn("FN");
        given(deliveryAddres.getLastname()).willReturn("LN");
        given(deliveryAddres.getPostalcode()).willReturn("3215");
        given(deliveryAddres.getLine1()).willReturn("line1");
        given(deliveryAddres.getLine2()).willReturn("line2");
        given(deliveryAddres.getTown()).willReturn("geelong");
        given(deliveryAddres.getDistrict()).willReturn("VIC");
        given(deliveryAddres.getCountry()).willReturn(countryModel);

        given(productType.getCode()).willReturn("giftCard");
        given(model.getProductType()).willReturn(productType);
        given(product.getBaseProduct()).willReturn(model);
        given(orderEntry.getProduct()).willReturn(product);
        given(orderEntry.getDeliveryAddress()).willReturn(deliveryAddres);

        final GiftRecipientModel recipient = new GiftRecipientModel();
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        recipients.add(recipient);

        given(orderEntry.getGiftRecipients()).willReturn(recipients);
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);

        consignmentEntry.setOrderEntry(orderEntry);

        given(sendToWarehouseProductConverter.convert(product)).willReturn(new ProductDTO());

        given(sendToWarehouseRecipientConverter.convert(recipient)).willReturn(new RecipientDTO());

        final ConsignmentEntryDTO entryDTO = sendToWarehouseConsignmentEntryConverter.convert(consignmentEntry);

        assertThat(entryDTO).isNotNull();
        assertThat(entryDTO.getProduct()).isNotNull();
        assertThat(entryDTO.getRecipients()).isNotEmpty();
        assertThat(entryDTO.getRecipients()).hasSize(1);
        assertThat(entryDTO.getRecipients().get(0).getFirstName()).isEqualTo("FN");
        assertThat(entryDTO.getRecipients().get(0).getLastName()).isEqualTo("LN");
        assertThat(entryDTO.getRecipients().get(0).getPostCode()).isEqualTo("3215");
        assertThat(entryDTO.getRecipients().get(0).getStreetAddress1()).isEqualTo("line1");
        assertThat(entryDTO.getRecipients().get(0).getStreetAddress2()).isEqualTo("line2");
        assertThat(entryDTO.getRecipients().get(0).getCity()).isEqualTo("geelong");
        assertThat(entryDTO.getRecipients().get(0).getState()).isEqualTo("VIC");
        assertThat(entryDTO.getRecipients().get(0).getCountry()).isEqualTo("AU");
    }
}
