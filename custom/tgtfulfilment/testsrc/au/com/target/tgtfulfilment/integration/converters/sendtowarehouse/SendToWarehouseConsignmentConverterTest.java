/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;


/**
 * Tests for SendToWarehouseConsignmentConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseConsignmentConverterTest {
    @Mock
    private SendToWarehouseConsignmentEntryConverter sendToWarehouseConsignmentEntryConverter;

    @Mock
    private SendToWarehouseOrderConverter sendToWarehouseOrderConverter;

    @Mock
    private TargetProductModel baseProductModel;

    @Mock
    private AbstractTargetVariantProductModel variantProductModel;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private ProductTypeModel productTypeModel;

    @InjectMocks
    private final SendToWarehouseConsignmentConverter sendToWarehouseConsignmentConverter = new SendToWarehouseConsignmentConverter();


    @Test(expected = IllegalArgumentException.class)
    public void testNullConsignment() {
        sendToWarehouseConsignmentConverter.convert(null);
    }

    @Test
    public void testConsignmentWithNoOrdersAndEntriesNoWarehouse() {
        final ConsignmentModel consignment = new ConsignmentModel();
        consignment.setCode("1234");
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO);
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries());
        assertThat(consignmentDTO.getConsignmentEntries()).isEmpty();
        assertThat(consignmentDTO.getOrder()).isNull();
        assertThat(consignmentDTO.getWarehouseId()).isNull();
    }

    @Test
    public void testConsignmentWithNoOrdersAndEntries() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries()).isNotNull();
        assertThat(consignmentDTO.getConsignmentEntries()).isEmpty();
        assertThat(consignmentDTO.getOrder()).isNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
    }

    @Test
    public void testConsignmentWithNoEntries() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        consignment.setOrder(new OrderModel());

        given(sendToWarehouseOrderConverter.convert(any(OrderModel.class))).willReturn(
                new OrderDTO());
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries()).isNotNull();
        assertThat(consignmentDTO.getConsignmentEntries()).isEmpty();
        assertThat(consignmentDTO.getOrder()).isNotNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
    }

    @Test
    public void testConsignmentWithAllFieldsPopulated() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        consignment.setOrder(new OrderModel());

        final ConsignmentEntryModel entry = new ConsignmentEntryModel();
        entry.setQuantity(Long.valueOf(3));
        final Set<ConsignmentEntryModel> entries = new HashSet<>();
        entries.add(entry);
        consignment.setConsignmentEntries(entries);

        given(sendToWarehouseOrderConverter.convert(any(OrderModel.class))).willReturn(
                new OrderDTO());
        given(sendToWarehouseConsignmentEntryConverter.convert(any(ConsignmentEntryModel.class)))
                .willReturn(
                        new ConsignmentEntryDTO());

        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries()).isNotNull();
        assertThat(consignmentDTO.getConsignmentEntries()).isNotEmpty();
        assertThat(consignmentDTO.getConsignmentEntries().size()).isEqualTo(1);
        assertThat(consignmentDTO.getConsignmentEntries().get(0));
        assertThat(consignmentDTO.getOrder()).isNotNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
    }


    @Test
    public void testConsignmentWithDigitalGiftCards() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        consignment.setOrder(new OrderModel());
        given(Integer.valueOf(targetSharedConfigService
                .getInt(TgtfulfilmentConstants.Config.GIFTCARDS_SMALL_SHIPMENT_LIMIT, 2)))
                .willReturn(Integer.valueOf(2));
        consignment.setConsignmentEntries(Collections.singleton(createConsignmentEntryWithProductType("digital", 3)));
        given(sendToWarehouseOrderConverter.convert(any(OrderModel.class))).willReturn(
                new OrderDTO());
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries()).isNotEmpty();
        assertThat(consignmentDTO.getOrder()).isNotNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
        assertThat(consignmentDTO.getShippingMethod()).isEqualTo(TgtfulfilmentConstants.GIFTCARD_SHIPPING_METHOD_EMAIL);
    }


    @Test
    public void testConsignmentWithPhysicalGiftCardsLargeShippingMethod() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        consignment.setOrder(new OrderModel());
        consignment.setConsignmentEntries(Collections.singleton(createConsignmentEntryWithProductType("giftcard", 7)));
        given(Integer.valueOf(targetSharedConfigService
                .getInt(TgtfulfilmentConstants.Config.GIFTCARDS_SMALL_SHIPMENT_LIMIT, 2)))
                .willReturn(Integer.valueOf(2));
        given(sendToWarehouseOrderConverter.convert(any(OrderModel.class))).willReturn(
                new OrderDTO());
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries()).isNotEmpty();
        assertThat(consignmentDTO.getOrder()).isNotNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
        assertThat(consignmentDTO.getShippingMethod()).isEqualTo(TgtfulfilmentConstants.GIFTCARD_SHIPPING_METHOD_LARGE);
    }


    @Test
    public void testConsignmentWithPhysicalGiftCardsSmallShippingMethod() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        consignment.setOrder(new OrderModel());
        given(Integer.valueOf(targetSharedConfigService
                .getInt(TgtfulfilmentConstants.Config.GIFTCARDS_SMALL_SHIPMENT_LIMIT, 2)))
                .willReturn(Integer.valueOf(2));
        consignment.setConsignmentEntries(Collections.singleton(createConsignmentEntryWithProductType("giftcard", 2)));
        given(sendToWarehouseOrderConverter.convert(any(OrderModel.class))).willReturn(
                new OrderDTO());
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries()).isNotEmpty();
        assertThat(consignmentDTO.getOrder()).isNotNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
        assertThat(consignmentDTO.getShippingMethod()).isEqualTo(TgtfulfilmentConstants.GIFTCARD_SHIPPING_METHOD_SMALL);
    }


    @Test
    public void testConsignmentWithBulkyPrds() {
        final ConsignmentModel consignment = new ConsignmentModel();
        final WarehouseModel warehouse = new WarehouseModel();
        warehouse.setCode("InComm");
        consignment.setCode("1234");
        consignment.setWarehouse(warehouse);
        consignment.setOrder(new OrderModel());
        consignment.setConsignmentEntries(Collections.singleton(createConsignmentEntryWithProductType("bulky", 2)));
        given(Integer.valueOf(targetSharedConfigService
                .getInt(TgtfulfilmentConstants.Config.GIFTCARDS_SMALL_SHIPMENT_LIMIT, 2)))
                .willReturn(Integer.valueOf(2));
        given(sendToWarehouseOrderConverter.convert(any(OrderModel.class))).willReturn(
                new OrderDTO());
        final ConsignmentDTO consignmentDTO = sendToWarehouseConsignmentConverter.convert(consignment);
        assertThat(consignmentDTO).isNotNull();
        assertThat(consignmentDTO.getId()).isEqualTo("1234");
        assertThat(consignmentDTO.getConsignmentEntries());
        assertThat(consignmentDTO.getOrder()).isNotNull();
        assertThat(consignmentDTO.getWarehouseId()).isEqualTo("InComm");
        assertThat(consignmentDTO.getShippingMethod()).isNull();
    }

    private ConsignmentEntryModel createConsignmentEntryWithProductType(final String prdType, final long qty) {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final OrderEntryModel orderEntry = mock(OrderEntryModel.class);
        given(productTypeModel.getCode()).willReturn(prdType);
        given(baseProductModel.getProductType()).willReturn(productTypeModel);
        given(variantProductModel.getBaseProduct()).willReturn(baseProductModel);
        given(orderEntry.getProduct()).willReturn(variantProductModel);
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignmentEntry.getQuantity()).willReturn(Long.valueOf(qty));
        return consignmentEntry;
    }
}
