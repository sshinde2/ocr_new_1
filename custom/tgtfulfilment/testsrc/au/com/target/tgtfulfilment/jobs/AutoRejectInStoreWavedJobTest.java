/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AutoRejectInStoreWavedJobTest {

    @InjectMocks
    private final AutoRejectInStoreWavedJob cronJob = new AutoRejectInStoreWavedJob();

    @Mock
    private TargetFulfilmentService targetFulfilmentService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private CronJobModel cronJobModel;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel globalStoreFulfilmentCapabilitiesModel;

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @Before
    public void setUp() {
        given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                globalStoreFulfilmentCapabilitiesModel);
        given(globalStoreFulfilmentCapabilitiesModel
                .getMaxTimeToPick()).willReturn(Integer.valueOf(180));
    }

    /**
     * To test when Consignment list is empty
     */
    @Test
    public void testPerformWithEmptyConsigment() {
        final List<TargetConsignmentModel> consignmentModels = Collections.emptyList();
        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.WAVED))
                .willReturn(consignmentModels);
        final PerformResult performResult = cronJob.perform(cronJobModel);
        verify(targetStoreConsignmentService).getConsignmentsForAllStore(
                ConsignmentStatus.WAVED);
        verifyZeroInteractions(targetFulfilmentService);

        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus().toString());
    }

    @Test
    public void testPerformWithConsignmentsBeforeMaxTime() throws NotFoundException {

        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        consignment.setOrder(orderModel);
        final List<TargetConsignmentModel> consignmentList = Arrays.asList(consignment);
        final Calendar wavedDate = Calendar.getInstance();
        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.WAVED))
                .willReturn(consignmentList);
        wavedDate.add(Calendar.MINUTE, 50);
        given(consignment.getWavedDate()).willReturn(wavedDate.getTime());
        final PerformResult performResult = cronJob.perform(cronJobModel);
        verifyZeroInteractions(targetFulfilmentService);
        assertThat(CronJobStatus.FINISHED.toString()).isEqualTo(performResult.getStatus().toString());
    }


    @Test
    public void testPerformWithExceededTime() throws Exception {

        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        consignment.setOrder(orderModel);
        final List<TargetConsignmentModel> consignmentList = Arrays.asList(consignment);
        final Calendar wavedDate = Calendar.getInstance();

        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.WAVED))
                .willReturn(consignmentList);
        wavedDate.add(Calendar.MINUTE, -182);
        given(consignment.getWavedDate()).willReturn(wavedDate.getTime());
        given(consignment.getStatus()).willReturn(ConsignmentStatus.WAVED);
        given(consignment.getCode()).willReturn("a123456");
        final PerformResult performResult = cronJob.perform(cronJobModel);

        verify(targetFulfilmentService).processRerouteAutoTimeoutConsignment(consignment,
                ConsignmentRejectReason.PICK_TIMEOUT);

        assertThat(CronJobResult.SUCCESS.toString()).isEqualTo(performResult.getResult().toString());
        assertThat(CronJobStatus.FINISHED.toString()).isEqualTo(performResult.getStatus().toString());
    }

    @Test
    public void testPerformWithNullGlobalFulfilment() throws NotFoundException {

        setupConsignment();
        given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                null);
        final PerformResult performResult = cronJob.perform(cronJobModel);
        verifyZeroInteractions(targetFulfilmentService);
        assertThat(CronJobStatus.FINISHED.toString()).isEqualTo(performResult.getStatus().toString());
    }

    @Test
    public void testPerformWithNullWavedDate() throws NotFoundException {

        final List<TargetConsignmentModel> consignmentList = setupConsignment();
        given(consignmentList.get(0).getCode()).willReturn("a1001010");
        given(consignmentList.get(0).getWavedDate()).willReturn(null);
        final PerformResult performResult = cronJob.perform(cronJobModel);
        verifyZeroInteractions(targetFulfilmentService);
        assertThat(CronJobStatus.FINISHED.toString()).isEqualTo(performResult.getStatus().toString());
    }

    @Test
    public void testPerformWithDefaultWarehouse() throws Exception {

        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        consignment.setOrder(orderModel);
        final List<TargetConsignmentModel> consignmentList = Arrays.asList(consignment);
        final Calendar wavedDate = Calendar.getInstance();

        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.WAVED))
                .willReturn(consignmentList);
        wavedDate.add(Calendar.MINUTE, -182);
        given(consignment.getWavedDate()).willReturn(wavedDate.getTime());
        given(consignment.getStatus()).willReturn(ConsignmentStatus.WAVED);
        given(consignment.getCode()).willReturn("a123456");
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        given(consignment.getWarehouse()).willReturn(warehouse);
        given(warehouse.getDefault()).willReturn(Boolean.TRUE);

        final PerformResult performResult = cronJob.perform(cronJobModel);

        verifyZeroInteractions(targetFulfilmentService);
        verify(globalStoreFulfilmentCapabilitiesModel).getMaxTimeToPick();
        verify(targetStoreConsignmentService).getConsignmentsForAllStore(ConsignmentStatus.WAVED);

        assertThat(CronJobResult.SUCCESS.toString()).isEqualTo(performResult.getResult().toString());
        assertThat(CronJobStatus.FINISHED.toString()).isEqualTo(performResult.getStatus().toString());
    }

    @Test
    public void testPerformWithConsignmentServicedByServicePoint() throws Exception {

        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final List<TargetConsignmentModel> consignmentList = Arrays.asList(consignment);

        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.WAVED))
                .willReturn(consignmentList);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentServicedByFluentServicePoint(consignment);
        given(consignment.getStatus()).willReturn(ConsignmentStatus.WAVED);
        final PerformResult performResult = cronJob.perform(cronJobModel);

        verifyZeroInteractions(targetFulfilmentService);

        assertThat(CronJobResult.SUCCESS.toString()).isEqualTo(performResult.getResult().toString());
        assertThat(CronJobStatus.FINISHED.toString()).isEqualTo(performResult.getStatus().toString());
    }

    private List<TargetConsignmentModel> setupConsignment() {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        consignment.setOrder(orderModel);
        final List<TargetConsignmentModel> consignmentList = Arrays.asList(consignment);
        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.WAVED))
                .willReturn(consignmentList);
        return consignmentList;
    }


}
