/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author ayushman
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendSmsToStoreForOpenOrdersJobTest {

    @InjectMocks
    private final SendSmsToStoreForOpenOrdersJob cronJob = new SendSmsToStoreForOpenOrdersJob();
    @Mock
    private TargetStoreConsignmentService mockTargetStoreConsignmentService;
    @Mock
    private TargetBusinessProcessService mockTargetBusinessProcessService;
    @Mock
    private PointOfServiceModel mockPointOfServiceModel;
    @Mock
    private TargetPointOfServiceModel mockTargetPointOfServiceModel;
    @Mock
    private CronJobModel cronjobModel;
    @Resource
    private ModelService modelService;

    @InjectMocks
    private final SendSmsToStoreForOpenOrdersJob sendSmsToStoreForOpenOrdersJob = new SendSmsToStoreForOpenOrdersJob();

    @Test
    public void testPerformWithoutOpenOrders() {
        final PerformResult result = sendSmsToStoreForOpenOrdersJob.perform(null);
        Mockito.verify(mockTargetStoreConsignmentService).getPosAndOpenOrdersForStores();
        Mockito.verifyZeroInteractions(mockTargetBusinessProcessService);
        Assertions.assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        Assertions.assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWitOpenOrders() {

        final Map<TargetPointOfServiceModel, Integer> storeInfo = new HashMap<TargetPointOfServiceModel, Integer>();
        storeInfo.put(mockTargetPointOfServiceModel, Integer.valueOf(2));
        final StoreFulfilmentCapabilitiesModel stortModel = new StoreFulfilmentCapabilitiesModel();
        stortModel.setMobileNumber("4434546789");
        Mockito.when(mockTargetPointOfServiceModel.getFulfilmentCapability()).thenReturn(stortModel);
        Mockito.when(mockTargetStoreConsignmentService.getPosAndOpenOrdersForStores()).thenReturn(storeInfo);
        final PerformResult result = sendSmsToStoreForOpenOrdersJob.perform(null);
        Mockito.verify(mockTargetBusinessProcessService).startSendSmsToStoreForOpenOrdersProcess(
                Mockito.any(SendSmsToStoreData.class));
        Assertions.assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        Assertions.assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }
}
