/**
 * 
 */
package au.com.target.tgtfulfilment.stock.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;
import au.com.target.tgtsale.stock.client.TargetStockUpdateClient;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentStoreStockServiceImplTest {

    private static final String PRODUCT_ONE = "50000001";

    private static final String PRODUCT_TWO = "50000002";


    @Mock
    private TargetStockUpdateClient mockTargetStockUpdateClient;

    @Mock
    private TargetPointOfServiceService mockTargetPointOfServiceService;

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private StockVisibilityClient stockVisibilityClient;

    @InjectMocks
    private final TargetFulfilmentStoreStockServiceImpl fulfilmentStoreStockServiceImpl = new TargetFulfilmentStoreStockServiceImpl();

    private final Integer storeNumber = Integer.valueOf(1234);

    private final Long storeBuffer = Long.valueOf(10L);

    private final String orderCode = "auto10001000";

    private final StockUpdateResponseDto stockUpdateResponse = new StockUpdateResponseDto();

    @Before
    public void setUp() throws Exception {

        //setup store stock responses
        final List<StockUpdateStoreResponseDto> stockUpdateStoreResponseDtos = new ArrayList<>();
        final StockUpdateStoreResponseDto stockUpdateStoreResponseDto = new StockUpdateStoreResponseDto();
        stockUpdateStoreResponseDto.setStoreNumber("1234");
        final List<StockUpdateProductResponseDto> stockUpdateProductResponseDtos = new ArrayList<>();
        StockUpdateProductResponseDto stockUpdateProductResponseDto = new StockUpdateProductResponseDto();
        stockUpdateProductResponseDto.setItemcode(PRODUCT_ONE);
        stockUpdateProductResponseDto.setSoh("30");
        stockUpdateProductResponseDtos.add(stockUpdateProductResponseDto);
        stockUpdateProductResponseDto = new StockUpdateProductResponseDto();
        stockUpdateProductResponseDto.setItemcode(PRODUCT_TWO);
        stockUpdateProductResponseDto.setSoh("20");
        stockUpdateProductResponseDtos.add(stockUpdateProductResponseDto);
        stockUpdateStoreResponseDto.setStockUpdateProductResponseDtos(stockUpdateProductResponseDtos);
        stockUpdateStoreResponseDtos.add(stockUpdateStoreResponseDto);

        stockUpdateResponse.setStockUpdateStoreResponseDtos(stockUpdateStoreResponseDtos);
        given(mockTargetPointOfServiceService.getPOSByStoreNumber(storeNumber)).willReturn(targetPointOfServiceModel);
    }

    @Test
    public void testIsProductInStockWithoutGlobalBufferInStock() {
        // No Global Buffer, Both Products in Stock

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(30L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(20L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);

        assertThat(result).isTrue();
    }

    @Test
    public void testIsProductInStockWithoutGlobalBufferOutOfStock() {
        // No Global Buffer, One Product Out Of Stock

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(30L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(21L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);

        assertThat(result).isFalse();
    }

    @Test
    public void testIsProductInStockWithGlobalBufferInStock() {
        // Global Buffer = 10, Both Product In Stock

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(20L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(10L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);

        given(targetStoreFulfilmentCapabilitiesService.getBufferStock(targetPointOfServiceModel))
                .willReturn(storeBuffer);

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsProductInStockWithGlobalBufferProd2OutOfStock() {
        // Global Buffer = 10, PRODUCT 1- In Stock, Product 2- Out of Stock

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(20L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(11L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);

        given(targetStoreFulfilmentCapabilitiesService.getBufferStock(targetPointOfServiceModel))
                .willReturn(storeBuffer);

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsProductInStockWithGlobalBufferProd1OutOfStock() {
        // Global Buffer = 10, PRODUCT 1- Out of Stock, Product 2- In Stock

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(21L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(10L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);

        given(targetStoreFulfilmentCapabilitiesService.getBufferStock(targetPointOfServiceModel))
                .willReturn(storeBuffer);

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsProductInStockWithGlobalBufferBothProdOutOfStock() {
        // Global Buffer = 10, Both Out of Stock

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(21L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(11L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);

        given(targetStoreFulfilmentCapabilitiesService.getBufferStock(targetPointOfServiceModel))
                .willReturn(storeBuffer);

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsProductInStockWithPendingQuantity() {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry1 = new AbstractOrderEntryModel();
        final AbstractOrderEntryModel orderEntry2 = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel productModel1 = new AbstractTargetVariantProductModel();
        final AbstractTargetVariantProductModel productModel2 = new AbstractTargetVariantProductModel();
        productModel1.setCode(PRODUCT_ONE);
        orderEntry1.setQuantity(Long.valueOf(21L));
        orderEntry1.setProduct(productModel1);
        orderEntries.add(orderEntry1);
        productModel2.setCode(PRODUCT_TWO);
        orderEntry2.setQuantity(Long.valueOf(11L));
        orderEntry2.setProduct(productModel2);
        orderEntries.add(orderEntry2);
        // PRODUCT_TWO: SOH - 20, pendingQuantity - 10, requiredQuantiy - 11; hence can't fulfill
        given(Integer.valueOf(mockTargetPointOfServiceService
                .getFulfilmentPendingQuantity(Integer.valueOf(1234), productModel1)))
                        .willReturn(Integer.valueOf(10));

        final boolean result = fulfilmentStoreStockServiceImpl.isProductInStock(stockUpdateResponse, orderEntries,
                storeNumber, orderCode);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsOrderEntriesInStockAtStoreWithProductInStockWhenLookedUpInCache() {
        //Order for store which has stock

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT);

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);
        oem = new AbstractOrderEntryModel();
        pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_TWO);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);

        final List<String> itemcodes = new ArrayList<>();
        itemcodes.add(PRODUCT_ONE);
        itemcodes.add(PRODUCT_TWO);

        final StockVisibilityItemLookupResponseDto response = new StockVisibilityItemLookupResponseDto();
        final List<StockVisibilityItemResponseDto> responseItemList = new ArrayList<>();
        final StockVisibilityItemResponseDto dtoProduct1 = mockStockVisibilityItemResponseDto(PRODUCT_ONE, "30",
                String.valueOf(storeNumber));
        final StockVisibilityItemResponseDto dtoProduct2 = mockStockVisibilityItemResponseDto(PRODUCT_TWO, "50",
                String.valueOf(storeNumber));
        responseItemList.add(dtoProduct1);
        responseItemList.add(dtoProduct2);
        response.setItems(responseItemList);

        final List<String> storesList = Arrays.asList(String.valueOf(storeNumber));
        given(stockVisibilityClient.lookupStockForItemsInStores(storesList, itemcodes))
                .willReturn(response);

        final boolean result = fulfilmentStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, storeNumber,
                orderCode);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsOrderEntriesInStockWhenOneItemIsOutOfStockViaCache() {
        //Order for store which has stock

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT);

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);
        oem = new AbstractOrderEntryModel();
        pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_TWO);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);

        final List<String> itemcodes = new ArrayList<>();
        itemcodes.add(PRODUCT_ONE);
        itemcodes.add(PRODUCT_TWO);

        final StockVisibilityItemLookupResponseDto response = new StockVisibilityItemLookupResponseDto();
        final List<StockVisibilityItemResponseDto> responseItemList = new ArrayList<>();
        final StockVisibilityItemResponseDto dtoProduct1 = mockStockVisibilityItemResponseDto(PRODUCT_ONE, "10",
                String.valueOf(storeNumber));
        final StockVisibilityItemResponseDto dtoProduct2 = mockStockVisibilityItemResponseDto(PRODUCT_TWO, "50",
                String.valueOf(storeNumber));
        responseItemList.add(dtoProduct1);
        responseItemList.add(dtoProduct2);
        response.setItems(responseItemList);

        final List<String> storesList = Arrays.asList(String.valueOf(storeNumber));
        given(stockVisibilityClient.lookupStockForItemsInStores(storesList, itemcodes))
                .willReturn(response);

        final boolean result = fulfilmentStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, storeNumber,
                orderCode);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsOrderEntriesInStockWhenOneItemIsNotInTheResponseFromStockCheckViaCache() {
        //Order for store which has stock


        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT);


        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);
        oem = new AbstractOrderEntryModel();
        pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_TWO);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);

        final List<String> itemcodes = new ArrayList<>();
        itemcodes.add(PRODUCT_ONE);
        itemcodes.add(PRODUCT_TWO);

        final StockVisibilityItemLookupResponseDto response = new StockVisibilityItemLookupResponseDto();
        final List<StockVisibilityItemResponseDto> responseItemList = new ArrayList<>();
        final StockVisibilityItemResponseDto dtoProduct1 = mockStockVisibilityItemResponseDto(PRODUCT_ONE, "50",
                String.valueOf(storeNumber));
        responseItemList.add(dtoProduct1);
        response.setItems(responseItemList);

        final List<String> storesList = Arrays.asList(String.valueOf(storeNumber));
        given(stockVisibilityClient.lookupStockForItemsInStores(storesList, itemcodes))
                .willReturn(response);

        final boolean result = fulfilmentStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, storeNumber,
                orderCode);
        assertThat(result).isFalse();
    }

    @Test
    public void testFetchProductsSohInStore() {
        final TargetPointOfServiceModel tpos1 = mock(TargetPointOfServiceModel.class);
        final TargetPointOfServiceModel tpos2 = mock(TargetPointOfServiceModel.class);
        final ProductModel product1 = mock(ProductModel.class);
        final ProductModel product2 = mock(ProductModel.class);
        given(tpos1.getStoreNumber()).willReturn(Integer.valueOf(1));
        given(tpos2.getStoreNumber()).willReturn(Integer.valueOf(2));

        final String prod1 = "product1";
        final String prod2 = "product2";
        given(product1.getCode()).willReturn(prod1);
        given(product2.getCode()).willReturn(prod2);

        final StockVisibilityItemLookupResponseDto dto = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item1 = new StockVisibilityItemResponseDto();
        item1.setCode(prod1);
        item1.setSoh("20");
        item1.setStoreNumber("1");
        final StockVisibilityItemResponseDto item2 = new StockVisibilityItemResponseDto();
        item2.setCode(prod2);
        item2.setSoh("10");
        item2.setStoreNumber("1");
        final StockVisibilityItemResponseDto item3 = new StockVisibilityItemResponseDto();
        item3.setCode(prod1);
        item3.setSoh("15");
        item3.setStoreNumber("2");
        final StockVisibilityItemResponseDto item4 = new StockVisibilityItemResponseDto();
        item4.setCode(prod2);
        item4.setSoh("12");
        item4.setStoreNumber("2");
        final Table<Integer, String, Long> pendingQuantityTable = HashBasedTable.create();

        dto.setItems(Arrays.asList(item1, item2, item3, item4));

        final List<ProductModel> products = Arrays.asList(product1, product2);
        final List<String> productCodes = Arrays.asList(prod1, prod2);
        given(stockVisibilityClient.lookupStockForItemsInStores(Arrays.asList("1", "2"), productCodes))
                .willReturn(dto);
        given(mockTargetPointOfServiceService
                .getFulfilmentPendingQuantityByProductCodeForStores(Arrays.asList("1", "2"), products))
                        .willReturn(pendingQuantityTable);
        final Table<Integer, String, Long> sohTable = fulfilmentStoreStockServiceImpl
                .fetchProductsSohInStore(Arrays.asList(tpos1, tpos2), products);
        assertThat(sohTable).isNotNull();
        assertThat(sohTable.get(Integer.valueOf(1), prod1)).isEqualTo(20);
        assertThat(sohTable.get(Integer.valueOf(1), prod2)).isEqualTo(10);
        assertThat(sohTable.get(Integer.valueOf(2), prod1)).isEqualTo(15);
        assertThat(sohTable.get(Integer.valueOf(2), prod2)).isEqualTo(12);

    }

    private StockVisibilityItemResponseDto mockStockVisibilityItemResponseDto(final String code, final String soh,
            final String storeNum) {
        final StockVisibilityItemResponseDto dtoProduct = new StockVisibilityItemResponseDto();
        dtoProduct.setCode(code);
        dtoProduct.setStoreNumber(storeNum);
        dtoProduct.setSoh(soh);
        return dtoProduct;
    }

}
