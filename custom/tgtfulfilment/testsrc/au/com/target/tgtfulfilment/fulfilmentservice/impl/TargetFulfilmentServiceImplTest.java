package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.endsWith;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.enums.WarehouseAutoTrigger;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentParcelUpdater;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentStatusValidator;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentTrackingIdGenerator;
import au.com.target.tgtfulfilment.fulfilmentservice.PickConsignmentUpdater;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTicketSender;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTypeAscertainerStrategy;
import au.com.target.tgtfulfilment.fulfilmentservice.PickValidator;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.ordercancel.PickCancelService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * Unit test for {@link TargetFulfilmentServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentServiceImplTest {
    /**
     * 
     */
    private static final String INSTORE_PICK_NOT_ATTEMPTED = "Pick Not Attempted";

    private static final String AUS_POST_WAREHOUSE_CODE = "AP";

    private static final Integer DEFAULT_CONSIGNMENT_STORE = Integer.valueOf(1234);

    @InjectMocks
    private final TargetFulfilmentServiceImpl fulfilmentServiceImpl = new TargetFulfilmentServiceImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private TargetOrderDao targetOrderDao;

    @Mock
    private ProductModel productModel;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private ConsignmentParcelUpdater consignmentParcelUpdater;

    @Mock
    private PickConsignmentUpdater pickConsignmentUpdater;

    @Mock
    private PickValidator pickValidator;

    @Mock
    private PickTicketSender pickTicketSender;

    @Mock
    private PickTypeAscertainerStrategy pickTypeAscertainerStrategy;

    @Mock
    private PickCancelService pickCancelService;

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private OrderModel orderModel;

    @Mock
    private PurchaseOptionConfigModel poc;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    @Mock
    private PickConfirmEntry pickConfirmEntry;

    @Mock
    private TargetConsignmentModel consignmentModel;

    @Mock
    private TargetCarrierModel ausPostCarrierModel;

    @Mock
    private ConsignmentEntryModel consignmentEntryModel;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private StoreFulfilmentCapabilitiesModel fulfilmentCapabilitiesModel;

    @Mock
    private ConsignmentTrackingIdGenerator consignmentTrackingIdGenerator;

    @Mock
    private ConsignmentStatusValidator instoreFulfilmentConsignmentStatusValidator;

    @Mock
    private ConsignmentStatusValidator fastlineFulfilmentConsignmentStatusValidator;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private FluentFulfilmentService fluentFulfilmentService;


    private List<PickConfirmEntry> pickConfirmEntries;
    private Set<ConsignmentModel> consignments;

    private String consignmentCode = null;

    @Before
    public void setUp() throws Exception {
        given(orderModel.getCode()).willReturn("12345");
        given(targetOrderDao.findOrderModelForOrderId(anyString())).willReturn(orderModel);

        // POC - by default config for buy now
        given(orderModel.getPurchaseOptionConfig()).willReturn(poc);
        given(poc.getAllowMultiplePicks()).willReturn(Boolean.FALSE);
        given(poc.getAllowShipBeforeInvoiced()).willReturn(Boolean.TRUE);
        given(poc.getCode()).willReturn("buynow");

        // Start with an empty list of pick entries
        pickConfirmEntries = new ArrayList<>();

        // One consignment
        given(targetConsignmentService.getConsignmentForCode(anyString())).willReturn(
                consignmentModel);
        consignments = ImmutableSet.of((ConsignmentModel)consignmentModel);

        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(consignmentModel.getTargetCarrier()).willReturn(ausPostCarrierModel);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(ausPostCarrierModel.getWarehouseCode()).willReturn(AUS_POST_WAREHOUSE_CODE);
        given(targetConsignmentService.getActiveConsignmentsForOrder(orderModel))
                .willReturn(Collections.singletonList(consignmentModel));

        given(targetStoreConsignmentService.getAssignedStoreForConsignmentAsInteger(consignmentModel))
                .willReturn(DEFAULT_CONSIGNMENT_STORE);

    }

    @Test
    public void testGetOrderConsignmentNullConsignments() throws FulfilmentException {

        expectedException.expect(FulfilmentException.class);
        given(orderModel.getConsignments()).willReturn(null);
        fulfilmentServiceImpl.getOrderConsignment(orderModel, "xxx");

    }

    @Test
    public void testGetOrderConsignmentEmptyConsignments() throws FulfilmentException {

        expectedException.expect(FulfilmentException.class);
        given(orderModel.getConsignments()).willReturn(Collections.EMPTY_SET);
        fulfilmentServiceImpl.getOrderConsignment(orderModel, "xxx");

    }

    @Test
    public void testGetOrderConsignmentNoMatch() throws FulfilmentException {

        expectedException.expect(FulfilmentException.class);

        given(orderModel.getConsignments()).willReturn(consignments);
        given(consignmentModel.getCode()).willReturn("con1");

        final TargetConsignmentModel cm = fulfilmentServiceImpl.getOrderConsignment(orderModel, "con2");
        assertThat(cm).isNotNull();
    }

    @Test
    public void testGetOrderConsignmentNullConsignmentCode() throws FulfilmentException {

        // In this case we return the first consignment for backwards compatibility
        given(orderModel.getConsignments()).willReturn(consignments);
        given(consignmentModel.getCode()).willReturn("con1");

        final TargetConsignmentModel cm = fulfilmentServiceImpl.getOrderConsignment(orderModel, null);
        assertThat(cm).isNotNull();
        assertThat(cm).isEqualTo(cm);
    }

    @Test
    public void testGetOrderConsignmentFound() throws Exception {

        given(orderModel.getConsignments()).willReturn(consignments);
        given(consignmentModel.getCode()).willReturn("con1");

        final TargetConsignmentModel cm = fulfilmentServiceImpl.getOrderConsignment(orderModel, "con1");
        assertThat(cm).isNotNull();
        assertThat(cm).isEqualTo(cm);
    }

    @Test
    public void testProcessAckForConsignmentWithNullNumber() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("orderCode cannot be null");
        fulfilmentServiceImpl.processAckForConsignment(null, consignmentModel, null);
    }

    @Test
    public void testProcessAckForConsignmentWithNullConsignment() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("consignment cannot be null");
        fulfilmentServiceImpl.processAckForConsignment("12345", null, null);
    }

    @Test
    public void testProcessAckForConsignmentOfFluentOrder() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("order:12345 is a fluent order");
        willReturn("fluentId").given(orderModel).getFluentId();
        willReturn(orderModel).given(consignmentModel).getOrder();

        fulfilmentServiceImpl.processAckForConsignment("12345", consignmentModel, null);
    }

    @Test
    public void testProcessAckForConsignmentWithNullWarehouse() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("warehouse cannot be null");
        given(consignmentModel.getWarehouse()).willReturn(null);
        fulfilmentServiceImpl.processAckForConsignment("12345", consignmentModel, LoggingContext.WAREHOUSE);
    }

    @Test
    public void testProcessAckForConsignmentWithInvalidNumber() throws Exception {
        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage("No order found with code:dummy");

        given(targetOrderDao.findOrderModelForOrderId(anyString())).willReturn(null);

        fulfilmentServiceImpl.processAckForConsignment("dummy", consignmentModel, null);
    }

    @Test
    public void testProcessAckForConsignmentWithInvalidOrderStatus() throws Exception {
        expectedException.expect(FulfilmentException.class);
        expectedException
                .expectMessage(
                        "Order is not in correct status. Expected status list:[INPROGRESS] Current status:CREATED");

        given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);

        fulfilmentServiceImpl.processAckForConsignment("12345", consignmentModel, null);
    }

    @Test
    public void testProcessAckForConsignmentWithInvalidConsignmentStatus() throws Exception {
        expectedException.expect(ConsignmentStatusValidationException.class);

        setupFailFastlineConsignmentStatusValidation();

        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        fulfilmentServiceImpl.processAckForConsignment("12345", consignmentModel, null);
    }

    @Test
    public void testProcessAckForConsignment() throws Exception {

        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        fulfilmentServiceImpl.processAckForConsignment("12345", consignmentModel, null);

        verify(consignmentModel).setStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
    }

    @Test
    public void testProcessShipConfForFluentOrder() throws Exception {
        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage("order:12345 is a fluent order");
        willReturn("fluentId").given(orderModel).getFluentId();
        willReturn(orderModel).given(consignmentModel).getOrder();

        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, null, null);
    }

    @Test
    public void testProcessShipConfForOrderWithNullOrderNumber() throws Exception {
        expectedException.expect(FulfilmentException.class);
        fulfilmentServiceImpl.processShipConfForConsignment(null, consignmentModel, null, null);
    }

    @Test
    public void testProcessShipConfForOrderWithNullShipDate() throws Exception {
        expectedException.expect(FulfilmentException.class);
        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, null, null);
    }

    @Test
    public void testProcessShipConfForOrderWithInvalidNumber() throws Exception {
        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage("No order found");

        given(targetOrderDao.findOrderModelForOrderId(anyString())).willReturn(null);

        fulfilmentServiceImpl.processShipConfForConsignment("dummy", consignmentModel, new Date(), null);
    }

    @Test
    public void testProcessShipConfForOrderWithInvalidOrderStatus() throws Exception {
        expectedException.expect(FulfilmentException.class);
        expectedException
                .expectMessage(
                        "Order is not in correct status. Expected status list:[INPROGRESS, INVOICED] Current status:CREATED");

        given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);

        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, new Date(), null);
    }

    @Test
    public void testProcessShipConfForOrderWithInvalidConsignmentStatus() throws Exception {
        expectedException.expect(ConsignmentStatusValidationException.class);

        setupFailFastlineConsignmentStatusValidation();

        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, new Date(), null);
    }

    @Test
    public void testProcessShipConfForOrderInProgressBuyNow() throws Exception {

        setupOrderWithPickedConsignmentForShip();

        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final Date shippingDate = new Date();
        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, shippingDate, null);

        //verify the required interactions

        verify(consignmentModel).setShippingDate(shippingDate);
        verify(consignmentModel).setStatus(ConsignmentStatus.SHIPPED);

        verify(targetBusinessProcessService).startOrderConsignmentProcess(orderModel, consignmentModel,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
    }

    @Test
    public void testProcessShipConfForOrderInProgressLayby() throws Exception {

        expectedException.expect(FulfilmentException.class);
        expectedException
                .expectMessage("For order of type layby ship conf not allowed for order in status INPROGRESS");

        setupOrderWithPickedConsignmentForShip();

        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(poc.getAllowShipBeforeInvoiced()).willReturn(Boolean.FALSE);
        given(poc.getCode()).willReturn("layby");

        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, new Date(), null);

    }

    @Test
    public void testProcessShipConfForOrderInvoicedBuyNow() throws Exception {

        setupOrderWithPickedConsignmentForShip();

        given(orderModel.getStatus()).willReturn(OrderStatus.INVOICED);

        final Date shippingDate = new Date();
        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, shippingDate, null);

        //verify the required interactions

        verify(consignmentModel).setShippingDate(shippingDate);
        verify(consignmentModel).setStatus(ConsignmentStatus.SHIPPED);

        verify(targetBusinessProcessService).startOrderConsignmentProcess(orderModel, consignmentModel,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
    }

    @Test
    public void testProcessShipConfForOrderInvoicedLayby() throws Exception {

        setupOrderWithPickedConsignmentForShip();

        given(orderModel.getStatus()).willReturn(OrderStatus.INVOICED);
        given(poc.getAllowShipBeforeInvoiced()).willReturn(Boolean.FALSE);
        given(poc.getCode()).willReturn("layby");

        final Date shippingDate = new Date();
        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, shippingDate, null);

        //verify the required interactions

        verify(consignmentModel).setShippingDate(shippingDate);

        verify(targetBusinessProcessService).startOrderConsignmentProcess(orderModel, consignmentModel,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
    }

    @Test
    public void testProcessShipConfForShipBeforePick() throws Exception {

        setupOrderWithPickedConsignmentForShip();
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final Date shippingDate = new Date();
        fulfilmentServiceImpl.processShipConfForConsignment("12345", consignmentModel, shippingDate, null);

        // ship conf and date set
        verify(consignmentModel).setShippingDate(shippingDate);
        verify(consignmentModel).setShipConfReceived(Boolean.TRUE);

        // Status not set yet
        verify(consignmentModel, never()).setStatus(ConsignmentStatus.SHIPPED);

        // Business process is not started
        verify(targetBusinessProcessService, never()).startOrderConsignmentProcess(orderModel,
                consignmentModel,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
    }


    private void setupOrderWithPickedConsignmentForShip() {

        given(consignmentEntryModel.getQuantity()).willReturn(Long.valueOf(10));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getConsignmentEntries()).willReturn(
                Collections.singleton(consignmentEntryModel));
        given(orderModel.getConsignments()).willReturn(consignments);

    }

    @Test
    public void testProcessPickConfForFluentOrder() throws FulfilmentException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("order:12345 is a fluent order");
        willReturn("fluentId").given(orderModel).getFluentId();
        willReturn(orderModel).given(consignmentModel).getOrder();

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(1), null, null);
    }

    @Test
    public void testProcessPickConfWrongOrderStatus() throws FulfilmentException {

        given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(1), null, null);

        final String errorMessage = "Order is not in correct status. Expected status list:[INPROGRESS, INVOICED] Current status:CREATED";
        verify(pickTicketSender).sendTicketInvalidPick(orderModel, errorMessage);
    }

    @Test
    public void testProcessPickConfBuyNowOrderInvoiced() throws FulfilmentException {

        given(orderModel.getStatus()).willReturn(OrderStatus.INVOICED);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(1), null, null);

        final String errorMessage = "Pick received for INVOICED order but multiple picks are not allowed for purchase option buynow";
        verify(pickTicketSender).sendTicketInvalidPick(orderModel, errorMessage);
    }

    @Test
    public void testProcessPickConfWrongConsignmentStatus() throws Exception {

        setupConsignmentForFastline();
        setupFailFastlineConsignmentStatusValidation();

        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CANCELLED);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(1), null, null);

        verify(pickTicketSender).sendTicketInvalidPick(orderModel, "TEST");
    }

    @Test
    public void testProcessPickConfInvalid() throws FulfilmentException {

        setupConsignmentForFastline();

        final String errorMessage = "PickValidator returned error";
        given(pickValidator.isInvalidPick(consignmentModel, pickConfirmEntries)).willReturn(errorMessage);

        // product2 is not in the consignment
        addPickEntry("product2", Integer.valueOf(2));
        addConsignmentEntry();

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, "AP", "trackingId-111",
                Integer.valueOf(1),
                pickConfirmEntries, null);

        verifyZeroInteractions(consignmentParcelUpdater);
        verify(pickTicketSender).sendTicketInvalidPick(orderModel, errorMessage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testProcessPickConfOver() throws FulfilmentException {

        setupConsignmentForFastline();

        given(pickValidator.isOverPick(consignmentModel, pickConfirmEntries)).willReturn(true);

        addPickEntry("product1", Integer.valueOf(3));
        addConsignmentEntry();

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111", Integer.valueOf(1), pickConfirmEntries, null);

        verify(pickTicketSender).sendTicketOverPick(orderModel, consignmentModel, pickConfirmEntries);
    }

    @Test
    public void testProcessPickConfValidFull() throws FulfilmentException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class)))
                        .willReturn(PickTypeEnum.FULL);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_PICKED_PROCESS));
    }

    @Test
    public void testProcessCompleteValidFull()
            throws FulfilmentException, ConsignmentStatusValidationException, NotFoundException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;
        final Date shippingDate = new Date();

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class)))
                        .willReturn(PickTypeEnum.FULL);

        given(targetConsignmentService.getConsignmentForCode("consignment1")).willReturn(consignmentModel);
        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrder.getCode()).willReturn("order1");

        fulfilmentServiceImpl.processCompleteConsignment("consignment1", shippingDate, carrier,
                trackingNumber, parcelCount, null);


        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_PICKED_PROCESS));
        verify(consignmentModel).setShippingDate(shippingDate);
        verify(consignmentModel).setShipConfReceived(Boolean.TRUE);

    }

    @Test
    public void testProcessPickConfValidShort() throws FulfilmentException, OrderCancelException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.SHORT);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(pickCancelService).startPickCancelProcess(eq(orderModel), eq(consignmentModel),
                any(PickConsignmentUpdateData.class), eq(PickTypeEnum.SHORT));
        verify(pickTicketSender).isAKioskOrder(orderModel);
        verifyNoMoreInteractions(pickTicketSender);
    }

    @Test
    public void testProcessPickConfValidZero() throws FulfilmentException, OrderCancelException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(0));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(pickCancelService).startPickCancelProcess(eq(orderModel), eq(consignmentModel),
                any(PickConsignmentUpdateData.class), eq(PickTypeEnum.ZERO));
        verify(pickTicketSender).isAKioskOrder(orderModel);
        verify(pickTicketSender).isEbayOrder(orderModel);
        verify(pickTicketSender).isTradeMeOrder(orderModel);
        verifyNoMoreInteractions(pickTicketSender);
    }


    @Test
    public void testProcessCancelForConsignmentValid()
            throws FulfilmentException, OrderCancelException, NotFoundException, ConsignmentStatusValidationException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addConsignmentEntry();


        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);

        fulfilmentServiceImpl.processCancelForConsignment("con1", null);

        verify(pickCancelService).startPickCancelProcess(eq(orderModel), eq(consignmentModel),
                any(PickConsignmentUpdateData.class), eq(PickTypeEnum.ZERO));
        verify(pickTicketSender).isAKioskOrder(orderModel);
        verify(pickTicketSender).isEbayOrder(orderModel);
        verify(pickTicketSender).isTradeMeOrder(orderModel);
        verifyNoMoreInteractions(pickTicketSender);
    }

    @Test
    public void testProcessCancelForConsignmentMissingCarrier()
            throws FulfilmentException, OrderCancelException, NotFoundException, ConsignmentStatusValidationException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addConsignmentEntry();

        given(consignmentModel.getTargetCarrier()).willReturn(null);
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);

        try {
            fulfilmentServiceImpl.processCancelForConsignment("con1", null);
            fail("Expected to throw fulfilmentExcetpion with message Carrier Missing");
        }
        catch (final FulfilmentException e) {
            assertThat(e.getMessage()).isEqualTo("Carrier Missing for consignment=con1");
        }
    }

    @Test
    public void testProcessCancelForConsignmentMissingOrderAssociation()
            throws FulfilmentException, OrderCancelException, NotFoundException, ConsignmentStatusValidationException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addConsignmentEntry();

        given(consignmentModel.getOrder()).willReturn(null);
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);

        try {
            fulfilmentServiceImpl.processCancelForConsignment("con1", null);
            fail("Expected to throw fulfilmentExcetpion with message Order Missing");
        }
        catch (final FulfilmentException e) {
            assertThat(e.getMessage()).isEqualTo("Order association missing for consignment=con1");
        }
    }

    @Test
    public void testProcessCancelForConsignmentMissingConsignmentEntries()
            throws FulfilmentException, OrderCancelException, NotFoundException, ConsignmentStatusValidationException {

        setupConsignmentForFastline();
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);
        try {
            fulfilmentServiceImpl.processCancelForConsignment("con1", null);
            fail("Expected to throw fulfilmentExcetpion with message Consignment Entries Missing");
        }
        catch (final FulfilmentException e) {
            assertThat(e.getMessage()).isEqualTo("Consignment Entries not present for consignment=con1");
        }
    }

    @Test
    public void testProcessCancelForConsignmentInvalidConsignmentStatus()
            throws FulfilmentException, OrderCancelException, NotFoundException, ConsignmentStatusValidationException {

        setupConsignmentForFastline();

        willThrow(new ConsignmentStatusValidationException("dummy"))
                .given(fastlineFulfilmentConsignmentStatusValidator)
                .validateConsignmentStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.CANCELLED);
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);
        try {
            fulfilmentServiceImpl.processCancelForConsignment("con1", null);
            fail("Expected to throw fulfilmentExcetpion with message Consignment Entries Missing");
        }
        catch (final ConsignmentStatusValidationException e) {
            assertThat(e.getMessage()).isEqualTo("dummy");
        }
    }

    @Test
    public void testProcessPinpadInvalidZeroPick() throws FulfilmentException, OrderCancelException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(0));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(pickTicketSender.isAKioskOrder(orderModel))).willReturn(Boolean.TRUE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verifyNoMoreInteractions(pickCancelService);
        verify(pickTicketSender).isAKioskOrder(orderModel);
        verify(pickTicketSender).isEbayOrder(orderModel);
        verify(pickTicketSender).isTradeMeOrder(orderModel);
        verify(pickTicketSender).sendTicketShortZeroPick(orderModel, consignmentModel, pickConfirmEntries);
        verifyNoMoreInteractions(pickTicketSender);
    }

    @Test
    public void testProcessPickConfShortNotAllowed() throws FulfilmentException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.SHORT);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.FALSE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(pickTicketSender).sendTicketShortZeroPick(orderModel, consignmentModel, pickConfirmEntries);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testProcessPickConfZeroNotAllowed() throws FulfilmentException {

        setupConsignmentForFastline();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(0));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.ZERO);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.FALSE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(pickTicketSender).sendTicketShortZeroPick(orderModel, consignmentModel, pickConfirmEntries);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testSetTrackingIdForConsignmentForInterstoreDelivery() throws NotFoundException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String trackingId = "TGTRB0000001";
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.INTERSTORE_DELIVERY);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(
                tpos);
        given(tpos.getFulfilmentCapability()).willReturn(fulfilmentCapabilitiesModel);
        given(consignmentTrackingIdGenerator
                .generateTrackingIdForConsignment(any(StoreFulfilmentCapabilitiesModel.class)))
                        .willReturn(trackingId);
        final PickConsignmentUpdateData result = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 10);
        assertThat(result.getTrackingNumber()).isEqualTo(trackingId);
    }

    @Test
    public void testNotSetTrackingIdForConsignmentForBulkyWarehouse() throws NotFoundException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.CUSTOMER_DELIVERY);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(
                tpos);
        willReturn(Boolean.TRUE).given(tpos).isBigAndBulky();

        final PickConsignmentUpdateData result = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 10);
        assertThat(result.getTrackingNumber()).isNull();
    }

    @Test
    public void testSetTrackingIdForConsignmentForCustomerDelivery() throws NotFoundException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String trackingId = "TGTRB0000003";
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.CUSTOMER_DELIVERY);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(
                tpos);
        given(tpos.getFulfilmentCapability()).willReturn(fulfilmentCapabilitiesModel);
        given(consignmentTrackingIdGenerator
                .generateTrackingIdForConsignment(any(StoreFulfilmentCapabilitiesModel.class)))
                        .willReturn(trackingId);
        final PickConsignmentUpdateData result = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 10);
        assertThat(result.getTrackingNumber()).isEqualTo(trackingId);
    }

    @Test
    public void testSetTrackingIdForConsignmentForCnc() throws NotFoundException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String trackingId = "TGTRB0000002";
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.INSTORE_PICKUP);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(
                tpos);
        given(tpos.getFulfilmentCapability()).willReturn(fulfilmentCapabilitiesModel);
        given(
                consignmentTrackingIdGenerator
                        .generateTrackingIdForConsignment(any(StoreFulfilmentCapabilitiesModel.class)))
                                .willReturn(trackingId);
        final PickConsignmentUpdateData result = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 10);
        assertThat(result.getTrackingNumber()).isNull();
    }

    @Test
    public void testSetTrackingIdForConsignmentWithNoOrderType() throws NotFoundException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        given(consignmentModel.getOfcOrderType()).willReturn(null);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        final PickConsignmentUpdateData result = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 10);
        assertThat(result.getTrackingNumber()).isNull();
    }

    @Test
    public void testSetTrackingIdForConsignmentForNoPOS() throws NotFoundException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String trackingId = "TGTRB0000004";
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.INSTORE_PICKUP);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);

        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(
                null);
        given(tpos.getFulfilmentCapability()).willReturn(fulfilmentCapabilitiesModel);
        given(consignmentTrackingIdGenerator
                .generateTrackingIdForConsignment(any(StoreFulfilmentCapabilitiesModel.class)))
                        .willReturn(trackingId);
        final PickConsignmentUpdateData result = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 10);
        assertThat(result.getTrackingNumber()).isNull();
    }

    @Test
    public void testProcessPickConfShortOnDealtems() throws FulfilmentException {

        setupConsignmentForInstorePickup();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.SHORT);

        given(poc.getAllowShortPick()).willReturn(Boolean.TRUE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount, pickConfirmEntries, null);

        verify(pickTicketSender).isAKioskOrder(orderModel);
        verifyNoMoreInteractions(pickTicketSender);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testProcessConsignmentAutoActionsAutoPick() throws FulfilmentException, NotFoundException,
            ConsignmentStatusValidationException {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(warehouse.getWarehouseAutoTrigger()).willReturn(WarehouseAutoTrigger.PICK);
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.FULL);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        fulfilmentServiceImpl.processConsignmentAutoActions(consignmentModel);
        verify(fastlineFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.PICKED);
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_PICKED_PROCESS));
    }

    @Test
    public void testProcessConsignmentAutoActionsAutoShip() throws FulfilmentException, NotFoundException,
            ConsignmentStatusValidationException {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(warehouse.getWarehouseAutoTrigger()).willReturn(WarehouseAutoTrigger.SHIP);
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.FULL);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        fulfilmentServiceImpl.processConsignmentAutoActions(consignmentModel);
        verify(fastlineFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.PICKED);
        verify(consignmentModel).setShippingDate(any(Date.class));
        verify(consignmentModel).setShipConfReceived(Boolean.TRUE);
    }

    @Test
    public void testProcessConsignmentAutoActionsAutoAck() throws FulfilmentException, NotFoundException,
            ConsignmentStatusValidationException {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(warehouse.getWarehouseAutoTrigger()).willReturn(WarehouseAutoTrigger.ACK);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        fulfilmentServiceImpl.processConsignmentAutoActions(consignmentModel);
        verify(fastlineFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
    }

    /**
     * Warehouse with trigger:Ack and prdType normal
     * 
     * @throws FulfilmentException
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     */
    @Test
    public void testProcessConsignmentAutoActionsAutoAckNormalPrd() throws FulfilmentException, NotFoundException,
            ConsignmentStatusValidationException {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        mockConsignmentWithPrdAndType(consignmentModel, "normal");
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(warehouse.getWarehouseAutoTrigger()).willReturn(WarehouseAutoTrigger.ACK);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        fulfilmentServiceImpl.processConsignmentAutoActions(consignmentModel);
        verify(fastlineFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        verify(consignmentModel).setStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        verify(modelService).save(consignmentModel);
        verifyNoMoreInteractions(modelService);
    }

    /**
     * Warehouse with trigger:ACK and prdType digital
     * 
     * @throws FulfilmentException
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     */
    @Test
    public void testProcessConsignmentAutoActionsAckForDigitalPrd() throws FulfilmentException, NotFoundException,
            ConsignmentStatusValidationException {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(warehouse.getWarehouseAutoTrigger()).willReturn(WarehouseAutoTrigger.ACK);
        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.FULL);
        given(consignmentModel.getWarehouse()).willReturn(warehouse);
        mockConsignmentWithPrdAndType(consignmentModel, "digital");
        fulfilmentServiceImpl.processConsignmentAutoActions(consignmentModel);
        verify(fastlineFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.PICKED);
        verify(consignmentModel).setShippingDate(any(Date.class));
        verify(consignmentModel).setShipConfReceived(Boolean.TRUE);
    }

    private void mockConsignmentWithPrdAndType(final ConsignmentModel consignment, final String prdTypeStr) {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final OrderEntryModel orderEntry = mock(OrderEntryModel.class);
        final TargetColourVariantProductModel variantModel = mock(TargetColourVariantProductModel.class);
        final TargetProductModel baseProduct = mock(TargetProductModel.class);
        final ProductTypeModel productType = mock(ProductTypeModel.class);
        given(productType.getCode()).willReturn(prdTypeStr);
        given(baseProduct.getProductType()).willReturn(productType);
        given(variantModel.getBaseProduct()).willReturn(baseProduct);
        given(orderEntry.getProduct()).willReturn(variantModel);
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
    }

    @Test
    public void testProcessPickConfRefundFailed() throws FulfilmentException, OrderCancelException {

        setupConsignmentForInstorePickup();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = AUS_POST_WAREHOUSE_CODE;

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class))).willReturn(PickTypeEnum.SHORT);
        given(orderModel.getPurchaseOptionConfig().getAllowShortPick()).willReturn(Boolean.TRUE);
        willThrow(new OrderCancelException("order1", "Refund failed!"))
                .given(pickCancelService).startPickCancelProcess(any(OrderModel.class),
                        any(TargetConsignmentModel.class), any(PickConsignmentUpdateData.class),
                        any(PickTypeEnum.class));

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(pickTicketSender).sendTicketFailedRefund(eq(orderModel), endsWith("Refund failed!"));
    }

    @Test
    public void testProcessPickConfXLCarrier() throws FulfilmentException {

        setupConsignmentForInstorePickup();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = "XL";

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class)))
                        .willReturn(PickTypeEnum.FULL);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);

        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_PICKED_PROCESS));
    }

    @Test(expected = FulfilmentException.class)
    public void testProcessPickConfWrongCarrier() throws FulfilmentException {
        setupConsignmentForInstorePickup();

        // Consignment has two product1's
        addPickEntry("product1", Integer.valueOf(2));
        addConsignmentEntry();

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "trackingId-111";
        final String carrier = "Carrier Pidgion";

        given(pickTypeAscertainerStrategy.ascertainPickType(eq(consignmentModel),
                any(PickConsignmentUpdateData.class)))
                        .willReturn(PickTypeEnum.FULL);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, carrier, trackingNumber,
                parcelCount,
                pickConfirmEntries, null);
    }

    @Test
    public void testValidateCarrier() throws FulfilmentException {
        assertThat(fulfilmentServiceImpl.validateCarrier(consignmentModel, AUS_POST_WAREHOUSE_CODE)).isTrue();
    }

    @Test
    public void testValidateCarrierXLCarrier() throws FulfilmentException {
        assertThat(fulfilmentServiceImpl.validateCarrier(consignmentModel, "XL")).isTrue();
    }

    @Test
    public void testValidateCarrierInvalidCarrier() throws FulfilmentException {
        assertThat(fulfilmentServiceImpl.validateCarrier(consignmentModel, "Postman Pat")).isFalse();
    }

    @Test
    public void testProcessSecondPickAllowed() throws FulfilmentException {

        setupConsignmentForInstorePickup();
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(poc.getAllowMultiplePicks()).willReturn(Boolean.TRUE);

        addPickEntry("product1", Integer.valueOf(2));
        // Consignment has two product1's
        addConsignmentEntry();

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(2),
                pickConfirmEntries, null);

        verify(consignmentParcelUpdater).updateParcelCount(consignmentModel, Integer.valueOf(2));
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testProcessSecondPickAllowedInvoiced() throws FulfilmentException {

        setupConsignmentForInstorePickup();
        given(orderModel.getStatus()).willReturn(OrderStatus.INVOICED);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(poc.getAllowMultiplePicks()).willReturn(Boolean.TRUE);

        addPickEntry("product1", Integer.valueOf(2));
        // Consignment has two product1's
        addConsignmentEntry();

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(2),
                pickConfirmEntries, null);

        verify(consignmentParcelUpdater).updateParcelCount(consignmentModel, Integer.valueOf(2));
        verify(targetBusinessProcessService).startOrderProcess(orderModel,
                TgtbusprocConstants.BusinessProcess.LAYBY_SECOND_PICK_INVOICED);
    }

    @Test
    public void testProcessSecondPickDisallowed() throws FulfilmentException {

        setupConsignmentForInstorePickup();
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(poc.getAllowMultiplePicks()).willReturn(Boolean.FALSE);

        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "trackingId-111",
                Integer.valueOf(1), null, null);

        final String errorMessage = "Second pick received but multiple picks are not allowed for purchase option";

        verify(pickTicketSender).sendTicketInvalidPick(eq(orderModel), startsWith(errorMessage));
        verifyZeroInteractions(consignmentParcelUpdater);
    }

    /**
     * Verifies that empty parcel count in pick confirmation message issues CS ticket.
     * 
     * @throws FulfilmentException
     *             if provided pick confirmation data is in inconsistent state
     */
    @Test(expected = FulfilmentException.class)
    public void testProcessPickConfNullParcelCount() throws FulfilmentException {
        fulfilmentServiceImpl.processPickConfForConsignment("order1", consignmentModel, AUS_POST_WAREHOUSE_CODE,
                "12345", null,
                pickConfirmEntries, null);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testProcessPickForInstoreFulfilment() throws Exception {
        setupConsignmentForInstorePickup();
        given(consignmentEntryModel.getQuantity()).willReturn(Long.valueOf(10));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.WAVED);
        given(consignmentModel.getConsignmentEntries()).willReturn(
                Collections.singleton(consignmentEntryModel));
        given(consignmentEntryModel.getOrderEntry()).willReturn(abstractOrderEntryModel);
        given(consignmentEntryModel.getOrderEntry().getProduct()).willReturn(productModel);
        given(productModel.getCode()).willReturn("123");

        given(consignmentEntryModel.getOrderEntry().getQuantity()).willReturn(10L);
        fulfilmentServiceImpl.processPickForInstoreFulfilment("con1");

        verifyNoMoreInteractions(pickConsignmentUpdater);
        verify(consignmentModel).setStatus(ConsignmentStatus.PICKED);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }


    @SuppressWarnings("boxing")
    @Test(expected = NotFoundException.class)
    public void testProcessPickForInstoreFulfilmentWillCodeNotFound() throws Exception {
        setupConsignmentForInstorePickup();
        given(consignmentEntryModel.getQuantity()).willReturn(Long.valueOf(10));
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getConsignmentEntries()).willReturn(
                Collections.singleton(consignmentEntryModel));
        given(targetConsignmentService.getConsignmentForCode("con2")).willThrow(
                new NotFoundException("No Records"));
        given(consignmentEntryModel.getOrderEntry()).willReturn(abstractOrderEntryModel);
        given(consignmentEntryModel.getOrderEntry().getProduct()).willReturn(productModel);
        given(productModel.getCode()).willReturn("123");
        given(consignmentEntryModel.getOrderEntry().getQuantity()).willReturn(10L);
        fulfilmentServiceImpl.processPickForInstoreFulfilment("con2");
    }

    @Test(expected = ConsignmentStatusValidationException.class)
    public void testProcessPickForInstoreFulfilmentInvalidStatus() throws Exception {

        setupConsignmentForInstorePickup();
        setupFailInstoreConsignmentStatusValidation();
        fulfilmentServiceImpl.processPickForInstoreFulfilment("con1");
    }

    private void setupFailInstoreConsignmentStatusValidation() throws ConsignmentStatusValidationException {

        willThrow(new ConsignmentStatusValidationException("TEST")).given(
                instoreFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                        any(ConsignmentStatus.class), any(ConsignmentStatus.class));
    }

    private void setupFailFastlineConsignmentStatusValidation() throws ConsignmentStatusValidationException {

        willThrow(new ConsignmentStatusValidationException("TEST")).given(
                fastlineFulfilmentConsignmentStatusValidator).validateConsignmentStatus(
                        any(ConsignmentStatus.class), any(ConsignmentStatus.class));
    }

    @Test
    public void testReroutingConsignmentNullConsignmentCode() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment Code cannot be null");
        fulfilmentServiceImpl.processRejectInstoreFulfilment(null, null, null);
        verifyZeroInteractions(targetConsignmentService);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test(expected = NotFoundException.class)
    public void testReroutingConsignmentNull() throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new NotFoundException("No Records"));
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
    }

    @Test(expected = RuntimeException.class)
    public void testReroutingConsignmentAmbiguousIdentifierException() throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new AmbiguousIdentifierException("Too many returned"));
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService.getConsignmentForCode(consignmentCode));
        verifyNoMoreInteractions(targetConsignmentService);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test(expected = NotFoundException.class)
    public void testReroutingConsignmentUnknownIdentifierException() throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new NotFoundException("Not found"));
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);
        verifyNoMoreInteractions(targetConsignmentService);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReroutingConsignmentWithNullOrderAssociatedwithConsignment()
            throws Exception {
        consignmentCode = "1234";
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getOrder()).willReturn(null);

        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
    }

    @Test
    public void testReroutingConsignment()
            throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);
        verify(targetConsignmentService).isConsignmentToDefaultWarehouse(consignmentModel);
        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.REJECTED_BY_STORE, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }

    @Test
    public void testReroutingConsignmentConfirmedByWarehouse()
            throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, INSTORE_PICK_NOT_ATTEMPTED, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setInstoreRejectReason(INSTORE_PICK_NOT_ATTEMPTED);
        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.REJECTED_BY_STORE);

        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.REJECTED_BY_STORE, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }

    @Test
    public void testReroutingConsignmentConfirmedByWarehouseWithZeroStockReported()
            throws NotFoundException, ConsignmentStatusValidationException, FulfilmentException, FluentBaseException {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, INSTORE_PICK_NOT_ATTEMPTED,
                ConsignmentRejectState.ZERO_STOCK_REPORTED);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setInstoreRejectReason(INSTORE_PICK_NOT_ATTEMPTED);
        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.REJECTED_BY_STORE);
        verify(consignmentModel).setRejectState(ConsignmentRejectState.ZERO_STOCK_REPORTED);

        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.REJECTED_BY_STORE, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }

    @Test
    public void testReroutingConsignmentWaved()
            throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.WAVED);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.PICK_FAILED);

        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.PICK_FAILED, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }

    @Test
    public void testReroutingConsignmentWavedWithZeroStockReported()
            throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.WAVED);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null,
                ConsignmentRejectState.ZERO_STOCK_REPORTED);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.PICK_FAILED);
        verify(consignmentModel).setRejectState(ConsignmentRejectState.ZERO_STOCK_REPORTED);

        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.PICK_FAILED, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }

    @Test
    public void testReroutingConsignmentPicked()
            throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.PICK_FAILED);

        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.PICK_FAILED, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
        verifyNoMoreInteractions(targetBusinessProcessService);
    }

    @Test
    public void testUpdateWavedStatusInstoreFulfilmentConsignmentCodeIsNull() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment Code cannot be null");
        fulfilmentServiceImpl.processWavedForInstoreFulfilment(null);
        verifyZeroInteractions(consignmentModel);
        verifyZeroInteractions(targetConsignmentService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(fluentFulfilmentService);

    }

    @Test
    public void testUpdateWavedStatusInstoreFulfilmentConsignmentThrowsRuntimeException() throws Exception {
        consignmentCode = "1234";
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Error getting consignment details for consignment : " + consignmentCode);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new RuntimeException("Error getting consignment details for consignment : " + consignmentCode));
        fulfilmentServiceImpl.processWavedForInstoreFulfilment(consignmentCode);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);
        verifyZeroInteractions(consignmentModel);
        verifyNoMoreInteractions(targetConsignmentService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(fluentFulfilmentService);
    }

    @Test
    public void testUpdateWavedStatusInstoreFulfilmentFluentConsignment() throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);

        fulfilmentServiceImpl.processWavedForInstoreFulfilment(consignmentCode);

        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);
        verify(instoreFulfilmentConsignmentStatusValidator).validateConsignmentStatus(consignmentModel.getStatus(),
                ConsignmentStatus.WAVED);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_WAVED, true);
    }

    @Test
    public void testUpdateWavedStatusInstoreFulfilmentFluentFulfilmentException() throws Exception {
        consignmentCode = "1234";
        expectedException.expect(FluentFulfilmentException.class);
        expectedException.expectMessage("test exception");
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        willThrow(new FluentFulfilmentException("test exception")).given(fluentFulfilmentService)
                .sendConsignmentEventToFluent(consignmentModel, TgtFluentConstants.TargetOfcEvent.OFC_WAVED, true);

        fulfilmentServiceImpl.processWavedForInstoreFulfilment(consignmentCode);
    }

    @Test
    public void testUpdateWavedStatusInstoreFulfilmentConsignmentThrowsNotFoundException() throws Exception {
        consignmentCode = "1234";
        expectedException.expect(NotFoundException.class);
        expectedException.expectMessage("Error consignment details not found for consignment : " + consignmentCode);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new NotFoundException("Error consignment details not found for consignment : " + consignmentCode));
        fulfilmentServiceImpl.processWavedForInstoreFulfilment(consignmentCode);
        verifyZeroInteractions(consignmentModel);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);
        verifyNoMoreInteractions(targetConsignmentService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(fluentFulfilmentService);
    }

    @Test(expected = ConsignmentStatusValidationException.class)
    public void testUpdateWavedStatusInstoreFulfilmentConsignmentValidConsignmentWrongStatus()
            throws Exception {

        setupConsignmentForInstorePickup();
        setupFailInstoreConsignmentStatusValidation();
        fulfilmentServiceImpl.processWavedForInstoreFulfilment("con1");
    }

    @Test
    public void testUpdateWavedStatusInstoreFulfilmentConsignmentValid()
            throws Exception {
        consignmentCode = "1234";
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        fulfilmentServiceImpl.processWavedForInstoreFulfilment(consignmentCode);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);
        verify(targetConsignmentService).isAFluentConsignment(consignmentModel);
        verify(consignmentModel).getStatus();
        verify(consignmentModel).setStatus(ConsignmentStatus.WAVED);
        verifyZeroInteractions(fluentFulfilmentService);

    }

    @Test(expected = ConsignmentStatusValidationException.class)
    public void testReroutingConsignmentWrongConsignmentStatus()
            throws Exception {
        setupConsignmentForInstorePickup();
        setupFailInstoreConsignmentStatusValidation();
        fulfilmentServiceImpl.processRejectInstoreFulfilment("con1", null, null);
    }

    @Test
    public void testReroutingConsignmentWithOnlineWarehouse()
            throws Exception {
        consignmentCode = "1234";
        final ArgumentCaptor<TargetOrderCancelEntryList> orderCancelEntryList = ArgumentCaptor
                .forClass(TargetOrderCancelEntryList.class);
        final WarehouseModel onlineWarehouse = mock(WarehouseModel.class);
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(1)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(onlineWarehouse);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(consignmentModel.getWarehouse()).willReturn(onlineWarehouse);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        given(orderEntry.getProduct()).willReturn(product);

        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.PICK_FAILED);
        verify(targetBusinessProcessService).startOrderItemCancelProcess(eq(orderModel),
                orderCancelEntryList.capture());
        assertThat(orderCancelEntryList.getValue()).isNotNull();
        final List<TargetOrderCancelEntry> orderCancelEntries = orderCancelEntryList.getValue().getEntriesToCancel();
        assertThat(orderCancelEntries).isNotEmpty().hasSize(1);
        assertThat(orderCancelEntries.get(0).getCancelQuantity()).isEqualTo(2);
    }

    @Test
    public void testProcessRejectInstoreFulfilmentWithFluentConsignmentCNCSameStore()
            throws Exception {
        consignmentCode = "1234";
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.INSTORE_PICKUP);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, "someReason", null);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_SS_CNC_SHIPPED, true);
        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(modelService).save(consignmentModel);
    }

    public void testProcessRejectInstoreFulfilmentWithFluentConsignment()
            throws Exception {
        consignmentCode = "1234";
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, "someReason", null);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK, true);
        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(modelService).save(consignmentModel);
    }

    @Test
    public void testReroutingConsignmentWithNonOnlineWarehouse()
            throws Exception {
        consignmentCode = "1234";
        final ArgumentCaptor<TargetOrderCancelEntryList> orderCancelEntryList = ArgumentCaptor
                .forClass(TargetOrderCancelEntryList.class);
        final WarehouseModel onlineWarehouse = mock(WarehouseModel.class);
        final WarehouseModel storeWarehouse = mock(WarehouseModel.class);
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(1)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(onlineWarehouse);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);

        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(consignmentModel.getWarehouse()).willReturn(storeWarehouse);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.PICK_FAILED);
        verify(targetBusinessProcessService, never()).startOrderItemCancelProcess(eq(orderModel),
                orderCancelEntryList.capture());
        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(modelService).save(consignmentModel);
    }

    @Test
    public void testReroutingConsignmentWithOnlineWarehouseFullShipped()
            throws Exception {
        consignmentCode = "1234";
        final ArgumentCaptor<TargetOrderCancelEntryList> orderCancelEntryList = ArgumentCaptor
                .forClass(TargetOrderCancelEntryList.class);
        final WarehouseModel onlineWarehouse = mock(WarehouseModel.class);
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(3)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(onlineWarehouse);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willReturn(consignmentModel);

        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(consignmentModel.getWarehouse()).willReturn(onlineWarehouse);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        fulfilmentServiceImpl.processRejectInstoreFulfilment(consignmentCode, null, null);
        verify(targetConsignmentService).getConsignmentForCode(consignmentCode);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.PICK_FAILED);
        verify(targetBusinessProcessService, never()).startOrderItemCancelProcess(eq(orderModel),
                orderCancelEntryList.capture());
    }



    @Test
    public void testProcessCompleteForInstoreFulfilmentInstorePickup() throws Exception {

        // Picked consignment of type instore pickup
        setupConsignmentForInstorePickup();
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);

        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status shipped and kick off ORDER_COMPLETION_PROCESS
        verify(consignmentModel).setShipConfReceived(Boolean.TRUE);
        verify(targetBusinessProcessService).startOrderConsignmentProcess(eq(orderModel),
                eq(consignmentModel), eq(TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS));
    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentCustomerDelivery() throws Exception {

        setupConsignment(OfcOrderType.CUSTOMER_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);

        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status packed
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);
    }

    @Test
    public void testProcessCompleteForInstoreFulfilment() throws Exception {

        setupConsignment(OfcOrderType.CUSTOMER_DELIVERY);
        final ArgumentCaptor<TargetOrderCancelEntryList> orderCancelEntryList = ArgumentCaptor
                .forClass(TargetOrderCancelEntryList.class);
        final WarehouseModel onlineWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(onlineWarehouse);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getWarehouse()).willReturn(onlineWarehouse);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);

        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status packed
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);
        verify(targetBusinessProcessService, never()).startOrderItemCancelProcess(eq(orderModel),
                orderCancelEntryList.capture());
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel),
                any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS));
    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentShortPick() throws Exception {

        setupConsignment(OfcOrderType.CUSTOMER_DELIVERY);
        final ArgumentCaptor<TargetOrderCancelEntryList> orderCancelEntryList = ArgumentCaptor
                .forClass(TargetOrderCancelEntryList.class);
        final WarehouseModel onlineWarehouse = mock(WarehouseModel.class);
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);

        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(1)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(onlineWarehouse);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getWarehouse()).willReturn(onlineWarehouse);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignmentModel);
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status packed
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);
        verify(targetBusinessProcessService).startOrderItemCancelProcess(eq(orderModel),
                orderCancelEntryList.capture());
        assertThat(orderCancelEntryList.getValue()).isNotNull();
        final List<TargetOrderCancelEntry> orderCancelEntries = orderCancelEntryList.getValue().getEntriesToCancel();
        assertThat(orderCancelEntries).isNotEmpty().hasSize(1);
        assertThat(orderCancelEntries.get(0).getCancelQuantity()).isEqualTo(2);
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel),
                any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS));
    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentInterstore() throws Exception {

        setupConsignment(OfcOrderType.INTERSTORE_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);

        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status packed
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);
    }

    @Test(expected = ConsignmentStatusValidationException.class)
    public void testProcessCompleteForInstoreFulfilmentInvalidConsStatus() throws Exception {
        setupConsignmentForInstorePickup();
        setupFailInstoreConsignmentStatusValidation();

        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);
    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentNoConsignmentfound() throws Exception {
        consignmentCode = "1234";
        expectedException.expect(NotFoundException.class);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new NotFoundException("No Records"));
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment(consignmentCode, 0, null);

    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentBlankConsignment() throws Exception {
        consignmentCode = "";
        expectedException.expect(NotFoundException.class);
        given(targetConsignmentService.getConsignmentForCode(consignmentCode)).willThrow(
                new NotFoundException("No Records"));
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment(consignmentCode, 0, null);

    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentHomeDeliveryWithFluentOnWillNotifyFluent() throws Exception {

        setupConsignment(OfcOrderType.CUSTOMER_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status shipped and kick off Consignment Picked process
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS));
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK, true);
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);


    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentHomeDeliveryWithFluentOffWillNotNotifyFluent()
            throws Exception {

        setupConsignment(OfcOrderType.INTERSTORE_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);
        willReturn(Boolean.FALSE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status shipped and kick off Consignment Picked process
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS));
        verifyZeroInteractions(fluentFulfilmentService);
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);

    }


    @Test
    public void testProcessCompleteForInstoreFulfilmentInterStoreDeliveryWithFluentOnWillNotifyFluent()
            throws Exception {

        setupConsignment(OfcOrderType.INTERSTORE_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status shipped and kick off Consignment Picked process
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS));
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK, true);
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);


    }

    @Test
    public void testProcessCompleteForInstoreFulfilmentInterStoreDeliveryWithFluentOffWillNotNotifyFluent()
            throws Exception {

        setupConsignment(OfcOrderType.CUSTOMER_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);
        willReturn(Boolean.FALSE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

        // Should set status shipped and kick off Consignment Picked process
        verify(targetBusinessProcessService).startOrderConsignmentPickedProcess(eq(orderModel),
                eq(consignmentModel), any(PickConsignmentUpdateData.class),
                eq(TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS));
        verifyZeroInteractions(fluentFulfilmentService);
        verify(consignmentModel).setStatus(ConsignmentStatus.PACKED);

    }

    @Test
    public void testUpdateOfcPickPackStatusInstoreFulfilmentFluentFulfilmentException() throws Exception {

        setupConsignment(OfcOrderType.CUSTOMER_DELIVERY);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(targetConsignmentService.getConsignmentForCode("con1")).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);
        expectedException.expect(FluentFulfilmentException.class);
        expectedException.expectMessage("test exception");

        willThrow(new FluentFulfilmentException("test exception")).given(fluentFulfilmentService)
                .sendConsignmentEventToFluent(consignmentModel, TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK, true);

        fulfilmentServiceImpl.processCompleteForInstoreFulfilment("con1", 1, null);

    }


    @Test
    public void createPickConsignmentUpdateDataForInstoreReturnsCorrectValueWithFalconSwitchOn() {

        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        final OrderEntryModel orderEntry1 = mock(OrderEntryModel.class);
        final OrderEntryModel orderEntry2 = mock(OrderEntryModel.class);
        final Set<ConsignmentEntryModel> consignmentEntryModels = new HashSet<>();
        consignmentEntryModels.add(consignmentEntry1);
        consignmentEntryModels.add(consignmentEntry2);
        final ProductModel product = mock(ProductModel.class);

        given(consignmentModel.getConsignmentEntries()).willReturn(consignmentEntryModels);
        given(consignmentEntry1.getOrderEntry()).willReturn(orderEntry1);
        given(consignmentEntry2.getOrderEntry()).willReturn(orderEntry2);
        given(orderEntry1.getProduct()).willReturn(product);
        given(orderEntry2.getProduct()).willReturn(product);
        given(consignmentEntry1.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry1.getShippedQuantity()).willReturn(null);
        given(consignmentEntry1.getConsignment()).willReturn(consignmentModel);
        given(consignmentEntry2.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry2.getShippedQuantity()).willReturn(new Long(2));
        given(consignmentEntry2.getConsignment()).willReturn(consignmentModel);
        given(product.getCode()).willReturn("product1");

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        final PickConsignmentUpdateData data = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 1);
        final List<PickConfirmEntry> pickEntries = data.getPickEntries();

        Integer shippedQty = Integer.valueOf(0);
        for (final PickConfirmEntry pickEntry : pickEntries) {
            shippedQty = new Integer(shippedQty.intValue() + pickEntry.getQuantityShipped().intValue());
        }

        assertThat(shippedQty).isEqualTo(2);


    }

    @Test
    public void createPickConsignmentUpdateDataForInstoreReturnsCorrectValueWithFluentSwitchOnAndFalconSwitchOff() {

        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        final OrderEntryModel orderEntry1 = mock(OrderEntryModel.class);
        final OrderEntryModel orderEntry2 = mock(OrderEntryModel.class);
        final Set<ConsignmentEntryModel> consignmentEntryModels = new HashSet<>();
        consignmentEntryModels.add(consignmentEntry1);
        consignmentEntryModels.add(consignmentEntry2);
        final ProductModel product = mock(ProductModel.class);

        given(consignmentModel.getConsignmentEntries()).willReturn(consignmentEntryModels);
        given(consignmentEntry1.getOrderEntry()).willReturn(orderEntry1);
        given(consignmentEntry2.getOrderEntry()).willReturn(orderEntry2);
        given(orderEntry1.getProduct()).willReturn(product);
        given(orderEntry2.getProduct()).willReturn(product);
        given(consignmentEntry1.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry1.getShippedQuantity()).willReturn(null);
        given(consignmentEntry1.getConsignment()).willReturn(consignmentModel);
        given(consignmentEntry2.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry2.getShippedQuantity()).willReturn(new Long(2));
        given(consignmentEntry2.getConsignment()).willReturn(consignmentModel);
        given(product.getCode()).willReturn("product1");

        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);


        final PickConsignmentUpdateData data = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 1);
        final List<PickConfirmEntry> pickEntries = data.getPickEntries();

        Integer shippedQty = Integer.valueOf(0);
        for (final PickConfirmEntry pickEntry : pickEntries) {
            shippedQty = new Integer(shippedQty.intValue() + pickEntry.getQuantityShipped().intValue());
        }

        assertThat(shippedQty).isEqualTo(2);


    }

    @Test
    public void createPickConsignmentUpdateDataForInstoreReturnsFullOrderQuantityWithFalconAndFluentTurnedOff() {

        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        final OrderEntryModel orderEntry1 = mock(OrderEntryModel.class);
        final OrderEntryModel orderEntry2 = mock(OrderEntryModel.class);
        final Set<ConsignmentEntryModel> consignmentEntryModels = new HashSet<>();
        consignmentEntryModels.add(consignmentEntry1);
        consignmentEntryModels.add(consignmentEntry2);
        final ProductModel product = mock(ProductModel.class);

        given(consignmentModel.getConsignmentEntries()).willReturn(consignmentEntryModels);
        given(consignmentEntry1.getOrderEntry()).willReturn(orderEntry1);
        given(consignmentEntry2.getOrderEntry()).willReturn(orderEntry2);
        given(orderEntry1.getProduct()).willReturn(product);
        given(orderEntry2.getProduct()).willReturn(product);
        given(orderEntry1.getQuantity()).willReturn(new Long(4));
        given(orderEntry2.getQuantity()).willReturn(new Long(4));
        given(consignmentEntry1.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry1.getShippedQuantity()).willReturn(new Long(2));
        given(consignmentEntry1.getConsignment()).willReturn(consignmentModel);
        given(consignmentEntry2.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry2.getShippedQuantity()).willReturn(new Long(2));
        given(consignmentEntry2.getConsignment()).willReturn(consignmentModel);
        given(product.getCode()).willReturn("product1");

        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);



        final PickConsignmentUpdateData data = fulfilmentServiceImpl
                .createPickConsignmentUpdateDataForInstore(consignmentModel, 1);
        final List<PickConfirmEntry> pickEntries = data.getPickEntries();

        Integer shippedQty = Integer.valueOf(0);
        for (final PickConfirmEntry pickEntry : pickEntries) {
            shippedQty = new Integer(shippedQty.intValue() + pickEntry.getQuantityShipped().intValue());
        }

        assertThat(shippedQty).isEqualTo(8);


    }

    /**
     * Test process reject instore fulfilment null consignment code.
     */
    @Test
    public void testProcessRerouteAutoTimeoutConsignmentsNullConsignmentCode() throws Exception {

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment Model can not be null");
        fulfilmentServiceImpl.processRerouteAutoTimeoutConsignment(null, ConsignmentRejectReason.ACCEPT_TIMEOUT);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(fluentFulfilmentService);

    }

    /**
     * Test process reroute auto timeout consigments null reason.
     *
     * @throws NotFoundException
     *             the not found exception
     */
    @Test
    public void testProcessRerouteAutoTimeoutConsignmentsNullReason() throws Exception {

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Rejection reason can not be null");
        fulfilmentServiceImpl.processRerouteAutoTimeoutConsignment(mock(TargetConsignmentModel.class), null);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(fluentFulfilmentService);

    }

    /**
     * Test process reroute auto timeout consigments with other consignment status reason.
     */
    @Test
    public void testProcessRerouteAutoTimeoutConsignmentsWithOtherReason() {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        try {
            fulfilmentServiceImpl.processRerouteAutoTimeoutConsignment(consignmentModel,
                    ConsignmentRejectReason.ACCEPT_TIMEOUT);
        }
        catch (final Exception e) {
            verifyZeroInteractions(targetBusinessProcessService);
            verifyZeroInteractions(fluentFulfilmentService);
        }
    }

    /**
     * Test process reroute auto timeout consigments.
     *
     * @throws NotFoundException
     *             the not found exception
     */
    @Test
    public void testProcessRerouteAutoTimeoutConsignments() throws Exception {
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        fulfilmentServiceImpl.processRerouteAutoTimeoutConsignment(consignmentModel,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.ACCEPT_TIMEOUT);

        verifyZeroInteractions(fluentFulfilmentService);
        verify(targetBusinessProcessService).startConsignmentRejectionProcess(orderModel, consignmentModel,
                ConsignmentRejectReason.ACCEPT_TIMEOUT, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
    }

    @Test
    public void testProcessRerouteAutoTimeoutFluentConsignments() throws Exception {
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignmentModel);

        fulfilmentServiceImpl.processRerouteAutoTimeoutConsignment(consignmentModel,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);

        verify(consignmentModel).setStatus(ConsignmentStatus.CANCELLED);
        verify(consignmentModel).setRejectReason(ConsignmentRejectReason.ACCEPT_TIMEOUT);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK, true);

        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test(expected = FulfilmentException.class)
    public void testGetOrderCancelEntriesForShortPickWithEmptyEntries() throws FulfilmentException {
        fulfilmentServiceImpl.getOrderCancelEntriesForShortPick(consignmentModel);
    }

    @Test(expected = FulfilmentException.class)
    public void testGetOrderCancelEntriesForShortPickWithNullEntry() throws FulfilmentException {
        given(consignmentModel.getConsignmentEntries()).willReturn(null);
        fulfilmentServiceImpl.getOrderCancelEntriesForShortPick(consignmentModel);
    }


    @Test(expected = FulfilmentException.class)
    public void testGetOrderCancelEntriesForShortPickWithNullOrderEntry() throws FulfilmentException {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        given(consignmentEntry.getOrderEntry()).willReturn(null);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        fulfilmentServiceImpl.getOrderCancelEntriesForShortPick(consignmentModel);
    }

    @Test(expected = FulfilmentException.class)
    public void testGetOrderCancelEntriesForShortPickWithNullOrderProduct() throws FulfilmentException {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        given(consignmentModel.getCode()).willReturn("c1234");
        given(orderEntry.getProduct()).willReturn(null);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(1)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        fulfilmentServiceImpl.getOrderCancelEntriesForShortPick(consignmentModel);
    }

    @Test
    public void testGetOrderCancelEntriesForShortPick() throws FulfilmentException {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(1)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        final TargetOrderCancelEntryList orderCancelEntryList = fulfilmentServiceImpl
                .getOrderCancelEntriesForShortPick(consignmentModel);
        assertThat(orderCancelEntryList).isNotNull();
        assertThat(orderCancelEntryList.getEntriesToCancel()).isNotEmpty();
        assertThat(orderCancelEntryList.getEntriesToCancel().get(0).getOrderEntry()).isEqualTo(orderEntry);
        assertThat(orderCancelEntryList.getEntriesToCancel().get(0).getCancelQuantity()).isEqualTo(2);
    }

    @Test
    public void testGetOrderCancelEntriesForShortPickNoCancelQty() throws FulfilmentException {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(3)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        final TargetOrderCancelEntryList orderCancelEntryList = fulfilmentServiceImpl
                .getOrderCancelEntriesForShortPick(consignmentModel);
        assertThat(orderCancelEntryList).isNotNull();
        assertThat(orderCancelEntryList.getEntriesToCancel()).isEmpty();
    }

    @Test
    public void testProcessCancelOrderItemInConsignmentForShortPick() throws FulfilmentException {
        final ArgumentCaptor<TargetOrderCancelEntryList> orderCancelEntryList = ArgumentCaptor
                .forClass(TargetOrderCancelEntryList.class);
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(1)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        fulfilmentServiceImpl.processCancelOrderItemInConsignmentForShortPick(consignmentModel, orderModel);
        verify(targetBusinessProcessService).startOrderItemCancelProcess(eq(orderModel),
                orderCancelEntryList.capture());
        final TargetOrderCancelEntryList resultList = orderCancelEntryList.getValue();
        assertThat(resultList).isNotNull();
        assertThat(resultList.getEntriesToCancel()).isNotEmpty();
        assertThat(resultList.getEntriesToCancel().get(0).getOrderEntry()).isEqualTo(orderEntry);
        assertThat(resultList.getEntriesToCancel().get(0).getCancelQuantity()).isEqualTo(2);
    }

    @Test
    public void testProcessCancelOrderItemInConsignmentForShortPickNoCancelQty() throws FulfilmentException {
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(orderEntry.getProduct()).willReturn(product);
        willReturn(Long.valueOf(3)).given(consignmentEntry).getQuantity();
        willReturn(Long.valueOf(3)).given(consignmentEntry).getShippedQuantity();
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(consignmentModel.getConsignmentEntries()).willReturn(Sets.newHashSet(consignmentEntry));
        fulfilmentServiceImpl.processCancelOrderItemInConsignmentForShortPick(consignmentModel, orderModel);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testProcessShipConsignmentConsignmentIdNull() throws FulfilmentException {
        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage("Consignment id or tracking number is null.");

        fulfilmentServiceImpl.processShipConsignment(null, "trackingID", LoggingContext.WAREHOUSE);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(targetStoreConsignmentService);
        verifyZeroInteractions(targetConsignmentService);

    }

    @Test
    public void testProcessShipConsignmentTrackingIdNull() throws FulfilmentException {
        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage("Consignment id or tracking number is null.");

        fulfilmentServiceImpl.processShipConsignment("consignmentID", null, LoggingContext.WAREHOUSE);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(targetStoreConsignmentService);
        verifyZeroInteractions(targetConsignmentService);
    }

    @Test
    public void testProcessShipConsignmentWhenNoConsignmentFound() throws Exception {
        willThrow(new NotFoundException("test error")).given(targetConsignmentService)
                .getConsignmentForCode("consignmentID");

        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage("Consignment not found for consignmentId=consignmentID.");

        fulfilmentServiceImpl.processShipConsignment("consignmentID", "trackingId", LoggingContext.WAREHOUSE);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(targetStoreConsignmentService);
        verify(targetConsignmentService).getConsignmentForCode("consignmentID");
    }

    @Test
    public void testProcessShipConsignmentWhenConsignmentStatusInvalid() throws Exception {
        given(targetConsignmentService.getConsignmentForCode("consignmentID")).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignmentModel.getCode()).willReturn("consignmentID");

        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage(
                "Consignment=consignmentID status invalid, expected status is PACKED, but current status is PICKED.");

        fulfilmentServiceImpl.processShipConsignment("consignmentID", "trackingId", LoggingContext.WAREHOUSE);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(targetStoreConsignmentService);
        verify(targetConsignmentService).getConsignmentForCode("consignmentID");
    }

    @Test
    public void testProcessShipConsignmentWhenConsignmentAssignedToInvalidPOS() throws Exception {
        given(targetConsignmentService.getConsignmentForCode("consignmentID")).willReturn(consignmentModel);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INVOICED);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(consignmentModel.getCode()).willReturn("consignmentID");

        final TargetPointOfServiceModel pos = mock(TargetPointOfServiceModel.class);
        willReturn(Boolean.FALSE).given(pos).isBigAndBulky();
        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(pos);

        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage(
                "Consignment=consignmentID is not assigned to big and bulky warehouse, hence cannot proceed.");

        fulfilmentServiceImpl.processShipConsignment("consignmentID", "trackingId", LoggingContext.WAREHOUSE);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetBusinessProcessService);
        verify(targetStoreConsignmentService).getAssignedStoreForConsignment(consignmentModel);
        verify(targetConsignmentService).getConsignmentForCode("consignmentID");
    }

    @Test
    public void testProcessShipConsignmentWhenOrderNull() throws Exception {
        given(targetConsignmentService.getConsignmentForCode("consignmentID")).willReturn(consignmentModel);
        given(consignmentModel.getOrder()).willReturn(null);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(consignmentModel.getCode()).willReturn("consignmentID");

        expectedException.expect(FulfilmentException.class);
        expectedException.expectMessage(
                "No order associated with consignment=consignmentID.");

        fulfilmentServiceImpl.processShipConsignment("consignmentID", "trackingId", LoggingContext.WAREHOUSE);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetBusinessProcessService);
        verifyZeroInteractions(targetStoreConsignmentService);
        verify(targetConsignmentService).getConsignmentForCode("consignmentID");
    }

    @Test
    public void testProcessShipConsignmentSuccess() throws Exception {
        given(targetConsignmentService.getConsignmentForCode("consignmentID")).willReturn(consignmentModel);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(consignmentModel.getCode()).willReturn("consignmentID");

        final TargetPointOfServiceModel pos = mock(TargetPointOfServiceModel.class);
        willReturn(Boolean.TRUE).given(pos).isBigAndBulky();
        given(targetStoreConsignmentService.getAssignedStoreForConsignment(consignmentModel)).willReturn(pos);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INVOICED);

        fulfilmentServiceImpl.processShipConsignment("consignmentID", "trackingId", LoggingContext.WAREHOUSE);

        verify(consignmentModel).setTrackingID("trackingId");
        verify(consignmentModel).setStatus(ConsignmentStatus.SHIPPED);
        verify(consignmentModel).setShippingDate(any(Date.class));
        verify(modelService).save(consignmentModel);
        verify(targetBusinessProcessService).startOrderConsignmentProcess(orderModel, consignmentModel,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
        verify(targetStoreConsignmentService).getAssignedStoreForConsignment(consignmentModel);
        verify(targetConsignmentService).getConsignmentForCode("consignmentID");

    }

    private void setupConsignmentForInstorePickup() {
        setupConsignment(OfcOrderType.INSTORE_PICKUP);
    }

    private void setupConsignmentForFastline() {
        setupConsignment(null);
    }

    private void setupConsignment(final OfcOrderType type) {
        // Default consignment model is in state CONFIRMED_BY_WAREHOUSE, and order in INPROGRESS
        // Order has single consignment con1
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(orderModel.getConsignments()).willReturn(consignments);
        given(consignmentModel.getCode()).willReturn("con1");
        given(consignmentModel.getOfcOrderType()).willReturn(type);
    }

    @SuppressWarnings("boxing")
    private void addPickEntry(final String itemCode, final Integer quantity) {
        final PickConfirmEntry pce = new PickConfirmEntry();
        pce.setItemCode(itemCode);
        pce.setQuantityShipped(quantity);

        pickConfirmEntries.add(pce);
    }

    private void addConsignmentEntry() {
        // consignment has one entry with 2 x product "product1"
        final ConsignmentEntryModel consignmentEntry = mock(ConsignmentEntryModel.class);
        final OrderEntryModel orderEntry = mock(OrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);

        given(consignmentModel.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        given(orderEntry.getProduct()).willReturn(product);
        given(consignmentEntry.getQuantity()).willReturn(new Long(2));
        given(consignmentEntry.getShippedQuantity()).willReturn(new Long(2));
        given(consignmentEntry.getConsignment()).willReturn(consignmentModel);
        given(product.getCode()).willReturn("product1");
    }

}