/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfulfilment.test.actions;

import de.hybris.platform.core.Registry;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;

import java.util.Arrays;

import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;


/**
 *
 */
public class SplitOrder extends TestActionTemp
{
    private int subprocessCount = 1;

    public void setSubprocessCount(final int subprocessCount)
    {
        this.subprocessCount = subprocessCount;
    }

    @Override
    public String execute(final BusinessProcessModel process) throws Exception //NOPMD
    {
        Logger.getLogger(getClass()).info("Process: " + process.getCode() + " in step " + getClass());

        final BusinessProcessParameterModel warehouseCounter = new BusinessProcessParameterModel();
        warehouseCounter.setName(TgtfulfilmentConstants.CONSIGNMENT_COUNTER);
        warehouseCounter.setProcess(process);
        warehouseCounter.setValue(Integer.valueOf(subprocessCount));
        save(warehouseCounter);

        final BusinessProcessParameterModel params = new BusinessProcessParameterModel();
        params.setName(TgtfulfilmentConstants.PARENT_PROCESS);
        params.setValue(process.getCode());

        for (int i = 0; i < subprocessCount; i++)
        {
            final ConsignmentProcessModel consProcess = modelService.create(ConsignmentProcessModel.class);
            consProcess.setParentProcess((OrderProcessModel)process);
            consProcess.setCode(process.getCode() + "_" + i);
            consProcess.setProcessDefinitionName("testConsignmentFulfilmentSubprocess");
            params.setProcess(consProcess);
            consProcess.setContextParameters(Arrays.asList(params));
            consProcess.setState(ProcessState.CREATED);
            modelService.save(consProcess);
            getBusinessProcessService().startProcess(consProcess);
            Logger.getLogger(getClass()).info("Subprocess: " + process.getCode() + "_" + i + " started");
        }

        return "OK";
    }



    /**
     * @return the businessProcessService
     */
    @Override
    public BusinessProcessService getBusinessProcessService()
    {
        return (BusinessProcessService)Registry.getApplicationContext().getBean("businessProcessService");
    }

}
