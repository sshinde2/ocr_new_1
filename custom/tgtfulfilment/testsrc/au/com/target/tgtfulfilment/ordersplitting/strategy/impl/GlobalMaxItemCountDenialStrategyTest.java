/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.List;

import org.fest.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalMaxItemCountDenialStrategyTest {

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private AbstractOrderEntryModel entryNumber1;

    @Mock
    private AbstractOrderEntryModel entryNumber2;

    @InjectMocks
    private final GlobalMaxItemCountDenialStrategy strategy = new GlobalMaxItemCountDenialStrategy();

    @Before
    public void setUp() {
        BDDMockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                capabilitiesModel);
    }

    /**
     * isItemCountLessThanMaximumAllowed - No
     */
    @Test
    public void testGlobalRulesPassedWhenItemsInOrderAreMoreThanMaximumAllowed() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        BDDMockito.given(entryNumber1.getQuantity()).willReturn(Long.valueOf(5));
        BDDMockito.given(entryNumber2.getQuantity()).willReturn(Long.valueOf(6));
        oeg.addAll(Collections.list(entryNumber1, entryNumber2));
        BDDMockito.given(capabilitiesModel.getMaxItemsPerConsignment()).willReturn(Integer.valueOf(10));
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("All global rules are satisfied",
                denialResponse.isDenied());
    }

    /**
     * isItemCountLessThanMaximumAllowed - Yes
     */
    @Test
    public void testGlobalRulesPassedWhenOrderEntryIsNullForAnOrder() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final List<AbstractOrderEntryModel> orderEntriesWithNull = new ArrayList<>();
        orderEntriesWithNull.add(entryNumber1);
        orderEntriesWithNull.add(null);
        oeg.addAll(orderEntriesWithNull);
        BDDMockito.given(capabilitiesModel.getMaxItemsPerConsignment()).willReturn(Integer.valueOf(10));
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesPassedWhenItemsInOrderAreLessThanMaximumAllowed() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        BDDMockito.given(entryNumber1.getQuantity()).willReturn(Long.valueOf(5));
        BDDMockito.given(entryNumber2.getQuantity()).willReturn(Long.valueOf(4));
        oeg.addAll(Collections.list(entryNumber1, entryNumber2));
        BDDMockito.given(capabilitiesModel.getMaxItemsPerConsignment()).willReturn(Integer.valueOf(10));
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesPassedWhenItemsInOrderAreEqualToMaximumAllowed() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        BDDMockito.given(entryNumber1.getQuantity()).willReturn(Long.valueOf(5));
        BDDMockito.given(entryNumber2.getQuantity()).willReturn(Long.valueOf(5));
        oeg.addAll(Collections.list(entryNumber1, entryNumber2));
        BDDMockito.given(capabilitiesModel.getMaxItemsPerConsignment()).willReturn(Integer.valueOf(10));
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesPassedWhenItemsInOrderIsZero() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        BDDMockito.given(entryNumber1.getQuantity()).willReturn(Long.valueOf(0));
        BDDMockito.given(entryNumber2.getQuantity()).willReturn(Long.valueOf(0));
        oeg.addAll(Collections.list(entryNumber1, entryNumber2));
        BDDMockito.given(capabilitiesModel.getMaxItemsPerConsignment()).willReturn(Integer.valueOf(10));
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }
}
