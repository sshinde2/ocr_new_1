/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * Unit tests for SpecificStoreEnabledFulfilmentDenialStrategy.
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpecificStoreEnabledFulfilmentDenialStrategyTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @InjectMocks
    private final SpecificStoreEnabledFulfilmentDenialStrategy strategy = new SpecificStoreEnabledFulfilmentDenialStrategy();

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private OEGParameterHelper oegHelper;

    @Before
    public void setup() {

        given(tpos.getStoreNumber()).willReturn(STORE_NUMBER);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithNullTPOS() {

        final DenialResponse response = strategy.isDenied(oeg, null);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithNullStoreNumber() {

        given(tpos.getStoreNumber()).willReturn(null);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithDenied() {

        given(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isStoreEnabledForFulfilment(tpos)))
                        .willReturn(Boolean.FALSE);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo("StoreNotCapableToFulfillOrder");
    }

    @Test
    public void testWithAllowed() {

        given(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isStoreEnabledForFulfilment(tpos)))
                        .willReturn(Boolean.TRUE);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
    }

}
