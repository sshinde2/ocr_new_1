package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.helper.WarehouseHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.ConsignmentToWarehouseAssigner;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.StoreSelectorStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentReroutingStrategyTest {

    private static final Integer CNC_STORE_NUMBER = Integer.valueOf(1234);

    private static final Integer FULFILMENT_STORE_NUMBER = Integer.valueOf(4321);

    private static final String ORDER_CODE = "testOrder";

    @Spy
    @InjectMocks
    private final ConsignmentReroutingStrategy consignmentReroutingStrategy = new ConsignmentReroutingStrategy();

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private StoreSelectorStrategy storeSelectorStrategy;

    @Mock
    private WarehouseHelper consignmentReroutingHelper;

    @Mock
    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    @Mock
    private ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner;

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetPointOfServiceModel cncStore;

    @Mock
    private TargetPointOfServiceModel fulfilmentStore;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private WarehouseModel warehouse;

    private final OrderModel order = new OrderModel();

    private final OrderEntryGroup orderEntryGroup = new OrderEntryGroup();

    private final List<OrderEntryGroup> orderEntryGroups = new ArrayList<>();

    private final OrderEntryModel orderEntry = new OrderEntryModel();


    @Before
    public void setUp() {
        orderEntryGroup.add(orderEntry);
        orderEntryGroups.add(orderEntryGroup);

        Mockito.doReturn(ORDER_CODE).when(oegParameterHelper).getOrderCode(orderEntryGroup);
        Mockito.doReturn(CNC_STORE_NUMBER).when(oegParameterHelper).getCncStoreNumber(orderEntryGroup);
        Mockito.doReturn(deliveryMode).when(oegParameterHelper).getDeliveryMode(orderEntryGroup);
        Mockito.doReturn(deliveryAddress).when(oegParameterHelper).getDeliveryAddress(orderEntryGroup);
    }

    @Test
    public void testPerformNullGroups() {
        Assert.assertNull(consignmentReroutingStrategy.perform(null));
    }

    @Test
    public void testPerformEmptyGroups() {
        Assert.assertTrue(CollectionUtils.isEmpty(consignmentReroutingStrategy.perform(ListUtils.EMPTY_LIST)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformMultipleGroups() {
        orderEntryGroups.add(new OrderEntryGroup());
        consignmentReroutingStrategy.perform(orderEntryGroups);
    }

    @Test
    public void testPerformAssignCncToAnotherStore() {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCncStoreNumber(CNC_STORE_NUMBER);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        Mockito.doNothing().when(oegParameterHelper).populateParamsFromOrder(orderEntryGroup);
        Mockito.doReturn(fulfilmentStore).when(storeSelectorStrategy).selectStore(orderEntryGroup);
        Mockito.doReturn(OfcOrderType.INTERSTORE_DELIVERY).when(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER);

        consignmentReroutingStrategy.perform(orderEntryGroups);

        Mockito.verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, fulfilmentStore);
    }

    @Test
    public void testPerformAssignDeliveryToStore() {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        Mockito.doNothing().when(oegParameterHelper).populateParamsFromOrder(orderEntryGroup);
        Mockito.doReturn(cncStore).when(storeSelectorStrategy).selectStore(orderEntryGroup);
        Mockito.doReturn(OfcOrderType.CUSTOMER_DELIVERY).when(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER);

        consignmentReroutingStrategy.perform(orderEntryGroups);

        Mockito.verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, cncStore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAfterSplittingNotTargetConsignment() {
        consignmentReroutingStrategy.afterSplitting(Mockito.any(OrderEntryGroup.class),
                Mockito.any(ConsignmentModel.class));
    }

    @Test
    public void testAfterSplittingForStore() {

        Mockito.doReturn(cncStore).when(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        Mockito.doReturn(warehouse).when(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);

        Mockito.doReturn(CNC_STORE_NUMBER).when(cncStore).getStoreNumber();
        Mockito.doReturn(OfcOrderType.INSTORE_PICKUP).when(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, CNC_STORE_NUMBER);

        consignmentReroutingStrategy.afterSplitting(orderEntryGroup, consignment);

        Mockito.verify(consignmentToWarehouseAssigner).assignConsignmentToStore(consignment, warehouse,
                OfcOrderType.INSTORE_PICKUP, deliveryMode, ORDER_CODE);
    }

    @Test
    public void testAfterSplittingDefaultWarehouse() {

        // Warehouse is not set in oeg
        Mockito.doReturn(null).when(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);

        consignmentReroutingStrategy.afterSplitting(orderEntryGroup, consignment);

        Mockito.verify(consignmentToWarehouseAssigner).assignDefaultWarehouse(consignment);
    }

    private void setupGlobalInstoreFulfilmentRulesPassed(final boolean success) {

        Mockito.doReturn(Boolean.valueOf(success)).when(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(orderEntryGroup);
    }

    private void setupDispatchInstoreFulfilmentRulesPassed(final boolean success) {

        Mockito.doReturn(Boolean.valueOf(success)).when(instoreFulfilmentGlobalRulesChecker)
                .areAllDispatchRulesPassed(orderEntryGroup);
    }

}
