/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FastlineSplitConsignmentStrategyTest {

    @InjectMocks
    private final FastlineSplitConsignmentStrategy strategy = new FastlineSplitConsignmentStrategy();

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;


    @Before
    public void before() {
        final OEGParameterHelper helper = new OEGParameterHelper();
        helper.setTargetWarehouseService(targetWarehouseService);
        strategy.setOegParameterHelper(helper);
        given(orderModel.getDeliveryMode()).willReturn(deliveryMode);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouse);
    }

    @Test
    public void testSplit() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String pcode1 = "P1";
        final String pcode2 = "P2";
        final String pcode3 = "P3";
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(3));
        requiredQty.put(pcode2, Long.valueOf(4));
        requiredQty.put(pcode3, Long.valueOf(5));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        given(Integer.valueOf(targetStockService.getStockLevelAmount(p1, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p2, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p3, warehouse))).willReturn(Integer.valueOf(3));

        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p1))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p2))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p3))).willReturn(Integer.valueOf(0));


        given(Integer.valueOf(
                targetSharedConfigService.getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100)))
                .willReturn(Integer.valueOf(100));

        willReturn(Integer.valueOf(5)).given(warehouse).getTotalQtyFulfilledAtFastlineToday();

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap = (Map<String, Long>)assigned.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse = (WarehouseModel)assigned.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap = new HashMap<>();
        expectedAssignedQtyMap.put(pcode1, Long.valueOf(3));
        expectedAssignedQtyMap.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap.put(pcode3, Long.valueOf(3));

        final List<AbstractOrderEntryModel> assignedEntries = assigned;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap).hasSize(3);
        assertThat(assignedQtyMap).isIn(expectedAssignedQtyMap);
        assertThat(assignedWarehouse).isNull();

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(2));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries).containsExactly(entry3);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitDailyFulfilledByFastlineWarehosueRoutToCancel()
            throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String pcode1 = "P1";
        final String pcode2 = "P2";
        final String pcode3 = "P3";
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(3));
        requiredQty.put(pcode2, Long.valueOf(4));
        requiredQty.put(pcode3, Long.valueOf(5));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        given(Integer.valueOf(targetStockService.getStockLevelAmount(p1, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p2, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p3, warehouse))).willReturn(Integer.valueOf(3));

        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p1))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p2))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p3))).willReturn(Integer.valueOf(0));


        given(Integer.valueOf(
                targetSharedConfigService.getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100)))
                .willReturn(Integer.valueOf(5));

        willReturn(Integer.valueOf(5)).given(warehouse).getTotalQtyFulfilledAtFastlineToday();

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        assertThat(result.getAssignedOEGsToWarehouse()).hasSize(0);


        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode1, Long.valueOf(3));
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(4));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(5));
        assertThat(notAssignedEntries).hasSize(3);
        assertThat(notAssignedEntries).contains(entry1, entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(3);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitDailyFulfilledByFastlineWarehosueRoutToCancellAndFulfill()
            throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String pcode1 = "P1";
        final String pcode2 = "P2";
        final String pcode3 = "P3";
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(3));
        requiredQty.put(pcode2, Long.valueOf(4));
        requiredQty.put(pcode3, Long.valueOf(5));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        given(Integer.valueOf(targetStockService.getStockLevelAmount(p1, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p2, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p3, warehouse))).willReturn(Integer.valueOf(3));

        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p1))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p2))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p3))).willReturn(Integer.valueOf(0));

        given(Integer.valueOf(
                targetSharedConfigService.getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100)))
                .willReturn(Integer.valueOf(5));

        willReturn(Integer.valueOf(2)).given(warehouse).getTotalQtyFulfilledAtFastlineToday();

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap = (Map<String, Long>)assigned.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse = (WarehouseModel)assigned.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap = new HashMap<>();
        expectedAssignedQtyMap.put(pcode1, Long.valueOf(3));

        final List<AbstractOrderEntryModel> assignedEntries = assigned;
        assertThat(assignedEntries).hasSize(1);
        assertThat(assignedEntries).containsExactly(entry1);
        assertThat(assignedQtyMap).hasSize(1);
        assertThat(assignedQtyMap).isIn(expectedAssignedQtyMap);
        assertThat(assignedWarehouse).isNull();

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(4));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(5));
        assertThat(notAssignedEntries).hasSize(2);
        assertThat(notAssignedEntries).contains(entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(2);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitDailyFulfilledByFastlineWarehosueRoutToCancellAndFulfillMoreProducts()
            throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String pcode1 = "P1";
        final String pcode2 = "P2";
        final String pcode3 = "P3";
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(3));
        requiredQty.put(pcode2, Long.valueOf(4));
        requiredQty.put(pcode3, Long.valueOf(5));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        given(Integer.valueOf(targetStockService.getStockLevelAmount(p1, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p2, warehouse))).willReturn(Integer.valueOf(5));
        given(Integer.valueOf(targetStockService.getStockLevelAmount(p3, warehouse))).willReturn(Integer.valueOf(3));

        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p1))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p2))).willReturn(Integer.valueOf(1));
        given(
                Integer.valueOf(targetPointOfServiceService.getFulfilmentPendingQuantity(
                        TgtCoreConstants.ONLINE_STORE_NUMBER, p3))).willReturn(Integer.valueOf(0));


        given(Integer.valueOf(
                targetSharedConfigService.getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100)))
                .willReturn(Integer.valueOf(6));

        willReturn(Integer.valueOf(2)).given(warehouse).getTotalQtyFulfilledAtFastlineToday();

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap = (Map<String, Long>)assigned.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse = (WarehouseModel)assigned.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap = new HashMap<>();
        expectedAssignedQtyMap.put(pcode1, Long.valueOf(3));
        expectedAssignedQtyMap.put(pcode2, Long.valueOf(1));

        final List<AbstractOrderEntryModel> assignedEntries = assigned;
        assertThat(assignedEntries).hasSize(2);
        assertThat(assignedEntries).contains(entry1, entry2);
        assertThat(assignedQtyMap).hasSize(2);
        assertThat(assignedQtyMap).isIn(expectedAssignedQtyMap);
        assertThat(assignedWarehouse).isNull();

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(3));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(5));
        assertThat(notAssignedEntries).hasSize(2);
        assertThat(notAssignedEntries).contains(entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(2);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

}
