/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * Unit tests for SpecificStoreDeliveryModeFulfilmentDenialStrategy.
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpecificStoreDeliveryModeFulfilmentDenialStrategyTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);

    private static final Integer ANOTHER_STORE_NUMBER = Integer.valueOf(7002);

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @InjectMocks
    private final SpecificStoreDeliveryModeFulfilmentDenialStrategy strategy = new SpecificStoreDeliveryModeFulfilmentDenialStrategy();

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegHelper;

    @Mock
    private TargetZoneDeliveryModeModel targetDeliveryMode;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel;

    @Before
    public void setup() {

        // Set up tpos by default to have STORE_NUMBER and capabilities
        Mockito.when(tpos.getStoreNumber()).thenReturn(STORE_NUMBER);
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeFulfilmentCapabilitiesModel);
    }

    @Test
    public void testWithNullStoreNumber() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(null);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithDeniedNullDeliveryMode() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(null);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo("DeliveryModeNullInOrder");
    }

    @Test
    public void testWithDeniedWrongDeliveryMode() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode)))
                .thenReturn(Boolean.FALSE);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).contains("DeliveryModeNotAccepted");
    }

    @Test
    public void testWithAllowedHomeDelivery() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode))).thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
    }

    @Test
    public void testWithAllowedHomeDeliveryWithNullIsDeliveryToStore() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode))).thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(null);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
    }

    @Test
    public void testWithNotAllowedStoreFulfilmentCapabilityNull() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode))).thenReturn(Boolean.TRUE);

        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(null);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isNotNull().isNotEmpty().isEqualTo("StoreNotCapableToFulfillOrder");
    }

    @Test
    public void testWithAllowedCNCOrderButOrderCNCStoreNumberIsNull() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(null);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isNotNull().isNotEmpty().isEqualTo("OrderCncStoreNumberMissing");
    }

    @Test
    public void testWithAllowedCNCOrderSameStoreButAllowedToThisStoreIsFalse() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(STORE_NUMBER);
        Mockito.when(storeFulfilmentCapabilitiesModel.getAllowDeliveryToThisStore()).thenReturn(Boolean.FALSE);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isNotNull().isNotEmpty().isEqualTo("NotAllowedToDeliverToThisStore");
        Mockito.verify(storeFulfilmentCapabilitiesModel).getAllowDeliveryToThisStore();
        Mockito.verifyNoMoreInteractions(storeFulfilmentCapabilitiesModel);
    }

    @Test
    public void testWithAllowedCNCOrderSameStoreButAllowedToThisStoreIsNull() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(STORE_NUMBER);
        Mockito.when(storeFulfilmentCapabilitiesModel.getAllowDeliveryToThisStore()).thenReturn(null);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isNotNull().isNotEmpty().isEqualTo("NotAllowedToDeliverToThisStore");
        Mockito.verify(storeFulfilmentCapabilitiesModel).getAllowDeliveryToThisStore();
        Mockito.verifyNoMoreInteractions(storeFulfilmentCapabilitiesModel);
    }

    @Test
    public void testWithAllowedCNCOrderSameStoreButAllowedToThisStoreIsTrue() {

        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(tpos,
                        targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(STORE_NUMBER);
        Mockito.when(storeFulfilmentCapabilitiesModel.getAllowDeliveryToThisStore()).thenReturn(Boolean.TRUE);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
        Mockito.verify(storeFulfilmentCapabilitiesModel).getAllowDeliveryToThisStore();
        Mockito.verifyNoMoreInteractions(storeFulfilmentCapabilitiesModel);
    }

    @Test
    public void testWithAllowedCNCOrderStoreButAllowedToAnotherStoreIsFalse() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(ANOTHER_STORE_NUMBER);
        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(
                        tpos, targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(STORE_NUMBER);
        Mockito.when(storeFulfilmentCapabilitiesModel.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.FALSE);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isNotNull().isNotEmpty()
                .isEqualTo("NotAllowedToDeliverToAnotherStore: " + STORE_NUMBER);
        Mockito.verify(storeFulfilmentCapabilitiesModel).getAllowDeliveryToAnotherStore();
        Mockito.verifyNoMoreInteractions(storeFulfilmentCapabilitiesModel);
    }

    @Test
    public void testWithAllowedCNCOrderStoreButAllowedToAnotherStoreIsNull() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(ANOTHER_STORE_NUMBER);
        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(
                        tpos, targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(STORE_NUMBER);
        Mockito.when(storeFulfilmentCapabilitiesModel.getAllowDeliveryToAnotherStore()).thenReturn(null);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isNotNull().isNotEmpty()
                .isEqualTo("NotAllowedToDeliverToAnotherStore: " + STORE_NUMBER);
        Mockito.verify(storeFulfilmentCapabilitiesModel).getAllowDeliveryToAnotherStore();
        Mockito.verifyNoMoreInteractions(storeFulfilmentCapabilitiesModel);
    }

    @Test
    public void testWithAllowedCNCOrderStoreButAllowedToAnotherStoreIsTrue() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(ANOTHER_STORE_NUMBER);
        Mockito.when(oegHelper.getDeliveryMode(oeg)).thenReturn(targetDeliveryMode);
        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isDeliveryModeAssociatedToStore(
                        tpos, targetDeliveryMode)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(oegHelper.getCncStoreNumber(oeg)).thenReturn(STORE_NUMBER);
        Mockito.when(storeFulfilmentCapabilitiesModel.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.TRUE);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
        Mockito.verify(storeFulfilmentCapabilitiesModel).getAllowDeliveryToAnotherStore();
        Mockito.verifyNoMoreInteractions(storeFulfilmentCapabilitiesModel);
    }
}
