/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SameStoreCncSplitConsignmentStrategyTest {

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @InjectMocks
    private final SameStoreCncSplitConsignmentStrategy strategy = new SameStoreCncSplitConsignmentStrategy();

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetPointOfServiceModel mockTpos;

    @Before
    public void before() {
        final OEGParameterHelper helper = new OEGParameterHelper();
        helper.setTargetWarehouseService(targetWarehouseService);
        strategy.setOegParameterHelper(helper);
        setStrategies(strategy);
        given(orderModel.getDeliveryMode()).willReturn(deliveryMode);
    }

    @Test
    public void testSplit() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String pcode1 = "P1";
        final String pcode2 = "P2";
        final String pcode3 = "P3";
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(3));
        requiredQty.put(pcode2, Long.valueOf(4));
        requiredQty.put(pcode3, Long.valueOf(5));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        final TargetPointOfServiceModel tpos = mock(TargetPointOfServiceModel.class);
        given(targetPointOfServiceService.getPOSByStoreNumber(cncStoreNumber)).willReturn(tpos);
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(tpos)).willReturn(warehouse);
        final Map<String, Long> sohMap = new HashMap<>();
        sohMap.put(pcode1, Long.valueOf(4));
        sohMap.put(pcode2, Long.valueOf(4));
        sohMap.put(pcode3, Long.valueOf(3));
        given(targetStoreStockService.fetchProductsSohInStore(any(TargetPointOfServiceModel.class), anyList()))
                .willReturn(sohMap);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap = (Map<String, Long>)assigned.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse = (WarehouseModel)assigned.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap = new HashMap<>();
        expectedAssignedQtyMap.put(pcode1, Long.valueOf(3));
        expectedAssignedQtyMap.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap.put(pcode3, Long.valueOf(3));

        final List<AbstractOrderEntryModel> assignedEntries = assigned;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap).hasSize(3);
        assertThat(assignedQtyMap).isIn(expectedAssignedQtyMap);
        assertThat(assignedWarehouse).isEqualTo(warehouse);

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(2));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries).containsExactly(entry3);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitNotCnc() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final String pcode1 = "P1";
        final String pcode2 = "P2";
        final String pcode3 = "P3";
        final Integer cncStoreNumber = null;
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(3));
        requiredQty.put(pcode2, Long.valueOf(4));
        requiredQty.put(pcode3, Long.valueOf(5));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        final TargetPointOfServiceModel tpos = mock(TargetPointOfServiceModel.class);
        given(targetPointOfServiceService.getPOSByStoreNumber(cncStoreNumber)).willReturn(tpos);
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(tpos)).willReturn(warehouse);
        final Map<String, Long> sohMap = new HashMap<>();
        sohMap.put(pcode1, Long.valueOf(4));
        sohMap.put(pcode2, Long.valueOf(4));
        sohMap.put(pcode3, Long.valueOf(3));
        given(targetStoreStockService.fetchProductsSohInStore(any(TargetPointOfServiceModel.class), anyList()))
                .willReturn(sohMap);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final List<OrderEntryGroup> assignedList = result.getAssignedOEGsToWarehouse();
        assertThat(assignedList).isEmpty();

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode1, Long.valueOf(3));
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(4));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(5));
        assertThat(notAssignedEntries).hasSize(3);
        assertThat(notAssignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(3);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testGetStoreNumberForLogging() {
        final Integer storeNumber = Integer.valueOf(5001);
        given(mockTpos.getStoreNumber()).willReturn(storeNumber);
        final String result = strategy.getStoreNumberForLogging(mockTpos);
        assertThat(result).isEqualTo(storeNumber.toString());
    }

    @Test
    public void testGetStoreNumberForLoggingWithEmptyStoreNumber() {
        given(mockTpos.getStoreNumber()).willReturn(null);
        final String result = strategy.getStoreNumberForLogging(mockTpos);
        assertThat(result).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testGetStoreStateForLogging() {
        final String district = "VIC";
        final AddressModel mockAddress = mock(AddressModel.class);
        given(mockAddress.getDistrict()).willReturn(district);
        given(mockTpos.getAddress()).willReturn(mockAddress);
        final String result = strategy.getStoreStateForLogging(mockTpos);
        assertThat(result).isEqualTo(district);
    }

    @Test
    public void testGetStoreStateForLoggingWithNullAddress() {
        given(mockTpos.getAddress()).willReturn(null);
        final String result = strategy.getStoreStateForLogging(mockTpos);
        assertThat(result).isEqualTo(StringUtils.EMPTY);
    }

    private void setStrategies(final SameStoreCncSplitConsignmentStrategy _strategy) {
        final SpecificStoreFulfilmentDenialStrategy strategy1 = mock(SpecificStoreFulfilmentDenialStrategy.class);
        given(strategy1.isDenied(any(OrderEntryGroup.class), any(TargetPointOfServiceModel.class))).willReturn(
                DenialResponse.createNotDenied());
        final List<SpecificStoreFulfilmentDenialStrategy> strategies = new ArrayList<>();
        strategies.add(strategy1);
        _strategy.setSpecificStoreFulfilmentDenialStrategies(strategies);
    }

}
