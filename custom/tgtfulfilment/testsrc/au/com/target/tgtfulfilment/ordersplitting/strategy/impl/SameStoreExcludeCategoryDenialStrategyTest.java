/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SameStoreExcludeCategoryDenialStrategyTest {

    @Spy
    @InjectMocks
    private final SameStoreExcludeCategoryDenialStrategy strategy = new SameStoreExcludeCategoryDenialStrategy();

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private TargetStoreFulfilmentCapabilitiesService capabilitiesService;

    @Mock
    private StoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private FilterExclusionListService filterExclusionListService;


    @Mock
    private OEGParameterHelper oegHelper;

    private final CategoryModel excludeCategory1 = new CategoryModel();
    private final CategoryModel excludeCategory2 = new CategoryModel();
    private final CategoryModel excludeCategory3 = new CategoryModel();

    @Before
    public void setUp() {
        BDDMockito.given(tpos.getFulfilmentCapability()).willReturn(
                capabilitiesModel);
        BDDMockito.given(strategy.getOegParameterHelper()).willReturn(oegHelper);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSameStoreExcludeCategoryWhenOegIsNull() {
        final OrderEntryGroup oeg = null;
        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(createExcludeCategoryModelList());
        strategy.isDenied(oeg, tpos);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSameStoreExcludeCategoryWhenOegIsEmpty() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(createExcludeCategoryModelList());
        strategy.isDenied(oeg, tpos);
    }

    @Test
    public void testSameStoreExclusionNotDenied() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(createExcludeCategoryModelList());
        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.FALSE);

        final DenialResponse denialResponse = strategy.isDenied(oeg, tpos);
        Assert.assertFalse("Create not Denied", denialResponse.isDenied());
    }

    @Test
    public void testSameStoreExclusionWithExcludeCategory() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(createExcludeCategoryModelList());

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getCategoryExclusions())).willReturn(getCategoryExclusionList());

        final DenialResponse denialResponse = strategy.isDenied(oeg, tpos);
        Assert.assertTrue("Create Denied", denialResponse.isDenied());
    }

    @Test
    public void testSameStoreRulesNotDeniedWithNoneOftheProductsFromExcludeCategory() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.FALSE);

        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(createExcludeCategoryModelList());

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getCategoryExclusions())).willReturn(getCategoryExclusionList());

        final DenialResponse denialResponse = strategy.isDenied(oeg, tpos);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesNotDeniedWithNullExcludeCategory() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(createExcludeCategoryModelList());

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getCategoryExclusions())).willReturn(null);

        final DenialResponse denialResponse = strategy.isDenied(oeg, tpos);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    @Test
    public void testSameStoreRulesNotDeniedWithNullCategories() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(capabilitiesModel.getCategoryExclusions()).willReturn(null);

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getCategoryExclusions())).willReturn(getCategoryExclusionList());

        final DenialResponse denialResponse = strategy.isDenied(oeg, tpos);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    private Set<CategoryExclusionsModel> createExcludeCategoryModelList() {
        final CategoryExclusionsModel excludeCate1 = new CategoryExclusionsModel();
        excludeCate1.setCategory(excludeCategory1);
        final CategoryExclusionsModel excludeCate2 = new CategoryExclusionsModel();
        excludeCate2.setCategory(excludeCategory2);
        final CategoryExclusionsModel excludeCate3 = new CategoryExclusionsModel();
        excludeCate3.setCategory(excludeCategory3);
        return Sets.newHashSet(excludeCate1, excludeCate2, excludeCate3);
    }

    private List<CategoryModel> getCategoryExclusionList() {
        return Lists.newArrayList(excludeCategory1, excludeCategory2, excludeCategory3);
    }
}
