/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;

import com.google.common.collect.Sets;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpecificStoreProductWeightDenialStrategyTest {
    private static final String ERROR_CODE_SYSTEM_ERROR = "SystemError";
    private static final String ERROR_CODE_PRODUCT_WEIGHT = "NoProductWeightFound";
    private static final String ERROR_CODE_WEIGHT_EXCEEDED = "ProductWeightMoreThanMaxAllowedWeight";
    private static final String ERROR_CODE_PRODUCT_TYPE_EXCLUDED = "ProductTypeExcluded";

    private List<AbstractOrderEntryModel> entryModelList;

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @Mock
    private OrderModel order;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeCapabilityModel;

    @Mock
    private AbstractOrderEntryModel entryModel;

    @Mock
    private AbstractTargetVariantProductModel variantProductModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private ProductTypeModel productType;

    @Mock
    private TargetProductDimensionsModel productDimensionsModel;

    @Mock
    private ProductTypeModel type1;

    @Mock
    private ProductTypeModel type2;

    @Mock
    private ProductTypeModel type3;

    @InjectMocks
    private final SpecificStoreProductWeightDenialStrategy strategy = new SpecificStoreProductWeightDenialStrategy();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON
    @Before
    public void setup() {
        Mockito.when(order.getCode()).thenReturn("UnitTestOrder001");
    }

    @Test
    public void testWithNullTpos() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("POS object must not be null");
        strategy.isDenied(oeg, null);
    }

    @Test
    public void testWhenOrderHasNoOrderEntries() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("OrderEntryGroup must not be empty");
        strategy.isDenied(null, tpos);
    }

    @Test
    public void testScenerioWhenmaxAllowedWeightIsReturnedNull() {
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(null);
        final DenialResponse response = strategy.isDenied(oeg, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isEqualTo(ERROR_CODE_SYSTEM_ERROR);
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testScenerioWhenmaxAllowedWeightIsReturnedAs0() {
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(0));
        final DenialResponse response = strategy.isDenied(oeg, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isNull();
        assertThat(response.isDenied()).isFalse();
    }

    @Test
    public void testWhenOrderHasNoProductModel() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(null);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isEqualTo(ERROR_CODE_SYSTEM_ERROR);
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWhenOrderHasNoProductDimensions() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(variantProductModel);
        Mockito.when(variantProductModel.getCode()).thenReturn("MockModel");
        Mockito.when(variantProductModel.getProductPackageDimensions()).thenReturn(null);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).startsWith(ERROR_CODE_PRODUCT_WEIGHT);
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWhenOrderHasNoProductWeight() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(variantProductModel);
        Mockito.when(variantProductModel.getCode()).thenReturn("MockModel");
        Mockito.when(variantProductModel.getProductPackageDimensions()).thenReturn(productDimensionsModel);
        Mockito.when(variantProductModel.getProductPackageDimensions().getWeight()).thenReturn(null);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).startsWith(ERROR_CODE_PRODUCT_WEIGHT);
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithNoProductIsFromExcludedProductType() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(variantProductModel);
        Mockito.when(variantProductModel.getCode()).thenReturn("MockModel");
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getRestrictedProductTypes(tpos)).thenReturn(
                getProductTypeSet());
        Mockito.when(variantProductModel.getProductType()).thenReturn(type3);
        Mockito.when(variantProductModel.getProductPackageDimensions()).thenReturn(productDimensionsModel);
        Mockito.when(variantProductModel.getProductPackageDimensions().getWeight()).thenReturn(Double.valueOf(1.5));
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(2));
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isNull();
        assertThat(response.isDenied()).isFalse();
    }

    @Test
    public void testWithOneOfTheProductIsFromExcludedProductType() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(variantProductModel);
        Mockito.when(variantProductModel.getCode()).thenReturn("MockModel");
        Mockito.when(variantProductModel.getProductPackageDimensions()).thenReturn(productDimensionsModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getRestrictedProductTypes(tpos)).thenReturn(
                getProductTypeSet());
        Mockito.when(variantProductModel.getProductType()).thenReturn(type1);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).startsWith(ERROR_CODE_PRODUCT_TYPE_EXCLUDED);
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testProductWeightGreaterThanAllowedWeight() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(variantProductModel);
        Mockito.when(variantProductModel.getCode()).thenReturn("MockModel");
        Mockito.when(variantProductModel.getProductPackageDimensions()).thenReturn(productDimensionsModel);
        Mockito.when(variantProductModel.getProductPackageDimensions().getWeight()).thenReturn(Double.valueOf(1.5));
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).startsWith(ERROR_CODE_WEIGHT_EXCEEDED);
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testProductWeightLessThanAllowedWeight() {
        entryModelList = new ArrayList<>();
        entryModelList.add(entryModel);
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(1));
        Mockito.when(entryModel.getProduct()).thenReturn(variantProductModel);
        Mockito.when(variantProductModel.getCode()).thenReturn("MockModel");
        Mockito.when(variantProductModel.getProductPackageDimensions()).thenReturn(productDimensionsModel);
        Mockito.when(variantProductModel.getProductPackageDimensions().getWeight()).thenReturn(Double.valueOf(1.5));
        Mockito.when(targetStoreFulfilmentCapabilitiesService.getMaxAllowedProductWeightForDelivery(tpos))
                .thenReturn(Double.valueOf(2));
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.addAll(entryModelList);
        final DenialResponse response = strategy.isDenied(oeg1, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isNull();
        assertThat(response.isDenied()).isFalse();
    }

    private Set<ProductTypeModel> getProductTypeSet() {
        return Sets.newHashSet(type1, type2);
    }
}
