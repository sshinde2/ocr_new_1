/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.aspectj.asm.IProgramElement.Modifiers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreSelectorStrategyImplTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    private List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies;

    @Mock
    private SpecificStoreFulfilmentDenialStrategy denialStrategy;

    @InjectMocks
    private final StoreSelectorStrategyImpl storeSelectorStrategy = new StoreSelectorStrategyImpl();

    @Mock
    private AbstractOrderModel abstractOrderModel;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private AddressModel deliveryAddressModel;

    @Mock
    private TargetStoreLocatorService targetStoreLocatorService;

    @Mock
    private NearestStoreSelectorStrategy nearestStoreSelectorStrategy;

    @Mock
    private TargetPointOfServiceModel posModel;

    @Mock
    private PointOfServiceDistanceData posData;

    private RoutingLogger routingLoggerMock;

    private List<TargetPointOfServiceModel> posList;

    private List<PointOfServiceDistanceData> sortedStores;

    private final double distanceLimit = 10001.12d;

    @Before
    public void setup() throws Exception {

        given(oegParameterHelper.getDeliveryAddress(oeg)).willReturn(deliveryAddressModel);
        given(oegParameterHelper.getOrderCode(oeg)).willReturn("10001000");

        // default valid delivery address
        given(deliveryAddressModel.getDistrict()).willReturn("VIC");
        given(abstractOrderModel.getDeliveryAddress()).willReturn(deliveryAddressModel);

        // Setup posData returned from targetStoreLocatorService
        posList = Collections.singletonList(posModel);
        given(targetStoreLocatorService.getAllFulfilmentStoresInState(Mockito.anyString()))
                .willReturn(posList);

        given(targetStoreLocatorService.getAllFulfilmentStores())
                .willReturn(posList);

        // Setup sorted data
        sortedStores = Collections.singletonList(posData);
        given(nearestStoreSelectorStrategy.getSortedStoreListByDistance(posList, deliveryAddressModel))
                .willReturn(sortedStores);
        given(
                Double.valueOf(targetSharedConfigService.getDouble(TgtfulfilmentConstants.Config.STORE_DISTANCE_LIMIT,
                        1000d))).willReturn(Double.valueOf(distanceLimit));
        given(
                nearestStoreSelectorStrategy.getSortedStoreListByDistanceWithDistanceRestriction(posList,
                        deliveryAddressModel, distanceLimit))
                .willReturn(sortedStores);
        given(
                Double.valueOf(targetSharedConfigService.getDouble(TgtfulfilmentConstants.Config.STORE_DISTANCE_LIMIT,
                        10000d))).willReturn(Double.valueOf(distanceLimit));

        given(posData.getPointOfService()).willReturn(posModel);

        // Setup posModel
        given(posModel.getStoreNumber()).willReturn(STORE_NUMBER);

        // Denial strategy list with a single strategy
        specificStoreFulfilmentDenialStrategies = new ArrayList<>();
        specificStoreFulfilmentDenialStrategies.add(denialStrategy);
        storeSelectorStrategy.setSpecificStoreFulfilmentDenialStrategies(specificStoreFulfilmentDenialStrategies);

        routingLoggerMock = mock(RoutingLogger.class);
        final Field field = StoreSelectorStrategyImpl.class.getDeclaredField("ROUTING_LOG");
        field.setAccessible(true);
        final Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifiers.FINAL.getBit());
        field.set(null, routingLoggerMock);
    }

    /**
     * Test method to verify that null is returned when the deliveryAddress is not set in the order
     * 
     */
    @Test
    public void testGetDeliveryAddressNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Delivery Address cannot be null");
        given(oegParameterHelper.getDeliveryAddress(oeg)).willReturn(null);
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }

    @Test
    public void testGetOrderCodeEmpty() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Order Code can't be empty");
        given(oegParameterHelper.getOrderCode(oeg)).willReturn(null);
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }


    @Test
    public void testWhenCantGeocode() {
        given(nearestStoreSelectorStrategy.getSortedStoreListByDistance(posList, deliveryAddressModel))
                .willThrow(new GeoServiceWrapperException("Cant geocode"));
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
        verify(routingLoggerMock).logDeliveryAddressRulesExitMessage("10001000",
                "CantGeocodeOrderDeliveryAddress, error=Cant geocode");
    }

    /**
     * Test to verify that Null is returned when the list of eligible stores that can fulfill the order is empty.
     * 
     * 
     */
    @Test
    public void testForEmptyListOrNoEligibleStoresReturned() {
        given(targetStoreLocatorService.getAllFulfilmentStoresInState(Mockito.anyString())).willReturn(
                new ArrayList<TargetPointOfServiceModel>());
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }

    @Test
    public void testForNoSortedStores() {
        given(nearestStoreSelectorStrategy.getSortedStoreListByDistance(posList, deliveryAddressModel))
                .willReturn(Collections.EMPTY_LIST);
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }

    @Test
    public void testWhenStoreDataHasNullPOS() {

        given(posData.getPointOfService()).willReturn(null);
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }

    @Test
    public void testWhenStoreDataHasNoStoreNumber() {

        given(posModel.getStoreNumber()).willReturn(null);
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }

    @Test
    public void testWithDenialStrategyReturningFalse() {

        given(denialStrategy.isDenied(oeg, posModel)).willReturn(
                DenialResponse.createDenied("Denied"));
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNull();
    }

    @Test
    public void testWithDenialStrategyReturningTrue() {

        given(denialStrategy.isDenied(oeg, posModel)).willReturn(
                DenialResponse.createNotDenied());
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNotNull().isEqualTo(posModel);
    }

    @Test
    public void testWithDenialStrategyReturningTrueWithDistanceLimit() {
        given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)))
                .willReturn(Boolean.TRUE);
        given(denialStrategy.isDenied(oeg, posModel)).willReturn(
                DenialResponse.createNotDenied());
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNotNull().isEqualTo(posModel);
    }

    @Test
    public void testIsValidWithNoDenialStrategys() {

        storeSelectorStrategy.setSpecificStoreFulfilmentDenialStrategies(null);
        assertThat(storeSelectorStrategy.selectStore(oeg)).isNotNull().isEqualTo(posModel);
    }

}
