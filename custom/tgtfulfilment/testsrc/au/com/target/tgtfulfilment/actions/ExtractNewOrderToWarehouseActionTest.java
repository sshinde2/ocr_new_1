/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.service.SendToWarehouseService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtractNewOrderToWarehouseActionTest {

    @Mock
    private SendToWarehouseService sendToWarehouseService;

    @InjectMocks
    private final ExtractNewOrderToWarehouseAction action = new ExtractNewOrderToWarehouseAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Before
    public void setup() {
        BDDMockito.given(process.getOrder()).willReturn(order);
    }

    @Test
    public void testExecute() throws Exception {

        action.execute(process);

        Mockito.verify(sendToWarehouseService).sendNewOrder(order);
    }
}
