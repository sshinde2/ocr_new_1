/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckConsignmentShipStatusActionTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetConsignmentService targetConsignmentService;


    @InjectMocks
    private final CheckConsignmentWithStatusAction action = new CheckConsignmentWithStatusAction();

    @Before
    public void setup() {
        // Set up process, order and consignment
        BDDMockito.given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);

        BDDMockito.given(process.getOrder()).willReturn(order);
        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment);

        BDDMockito.given(targetConsignmentService.getActiveConsignmentsForOrder(order)).willReturn(
                Collections.singletonList(consignment));

        action.setRequiredStatusList(Arrays.asList(ConsignmentStatus.SHIPPED, ConsignmentStatus.CANCELLED));
    }

    @Test
    public void testGetAllShippedNoConsignments() {

        expectedException.expect(BusinessProcessException.class);

        BDDMockito.given(targetConsignmentService.getActiveConsignmentsForOrder(order)).willReturn(null);

        final boolean isAllShipped = action.getAllInList(process);
        Assert.assertFalse(isAllShipped);
    }

    @Test
    public void testGetAllShippedCancelledConsignment() {

        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        final boolean isAllShipped = action.getAllInList(process);
        Assert.assertFalse(isAllShipped);
    }

    @Test
    public void testGetAllShippedPickedConsignment() {

        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.PICKED);

        final boolean isAllShipped = action.getAllInList(process);
        Assert.assertFalse(isAllShipped);
    }

    @Test
    public void testGetAllShippedShippedConsignment() {

        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.SHIPPED);

        final boolean isAllShipped = action.getAllInList(process);
        Assert.assertTrue(isAllShipped);
    }

}
