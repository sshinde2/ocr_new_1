package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateConsignmentStatusActionTest {

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final UpdateConsignmentStatusAction action = new UpdateConsignmentStatusAction();

    @Before
    public void setup() {
        action.setConsignmentStatus(ConsignmentStatus.PICKED);
    }

    @Test
    public void testSetConsignmentStatus() throws RetryLaterException, Exception {
        BDDMockito.given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        action.executeAction(process);
        Mockito.verify(consignment).setStatus(ConsignmentStatus.PICKED);
        Mockito.verify(modelService).save(consignment);
    }
}
