/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ForceDefaultWarehouseToOnlinePOSDecisionActionTest {

    @InjectMocks
    private final ForceDefaultWarehouseToOnlinePOSDecisionAction action = new ForceDefaultWarehouseToOnlinePOSDecisionAction();

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private OrderProcessModel process;

    @Mock
    private ConsignmentModel currentConsignment;

    @Mock
    private WarehouseModel warehouse;

    @Before
    public void setUp() {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(currentConsignment);
        given(currentConsignment.getWarehouse()).willReturn(warehouse);
    }

    @Test
    public void testExecuteActionWithDefaultWarehouse() throws RetryLaterException, Exception {
        given(warehouse.getDefault()).willReturn(Boolean.TRUE);
        final Transition result = action.executeAction(process);
        assertThat(result).isEqualTo(Transition.OK);
    }

    @Test
    public void testExecuteActionWithStoreWarehouse() throws RetryLaterException, Exception {
        given(warehouse.getDefault()).willReturn(Boolean.FALSE);
        final Transition result = action.executeAction(process);
        assertThat(result).isEqualTo(Transition.NOK);
    }
}
