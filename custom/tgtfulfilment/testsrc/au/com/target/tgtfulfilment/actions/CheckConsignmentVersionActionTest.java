package au.com.target.tgtfulfilment.actions;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckConsignmentVersionActionTest {

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Spy
    @InjectMocks
    private final CheckConsignmentVersionAction action = new CheckConsignmentVersionAction();

    @Before
    public void setup() {
        Mockito.when(orderProcessParameterHelper.getConsignment(process)).thenReturn(targetConsignment);
    }

    @Test
    public void testWhenConsignmentIsInstorePickupWithVersionNumber() {
        Mockito.when(targetConsignment.getOfcOrderType()).thenReturn(OfcOrderType.INSTORE_PICKUP);
        Mockito.when(targetConsignment.getConsignmentVersion()).thenReturn(Integer.valueOf(2));
        assertEquals(action.execute(process), "INSTORE_CONSIGNMENT_NEW_VERSION");
    }

    @Test
    public void testWhenConsignmentIsHomeDeliveryWithVersionNumber() {
        Mockito.when(targetConsignment.getOfcOrderType()).thenReturn(OfcOrderType.CUSTOMER_DELIVERY);
        Mockito.when(targetConsignment.getConsignmentVersion()).thenReturn(Integer.valueOf(2));
        assertEquals(action.execute(process), "DELIVERY_CONSIGNMENT_NEW_VERSION");
    }

    @Test
    public void testForOldConsignmentsWithNoVersionNumber() {
        Mockito.when(targetConsignment.getOfcOrderType()).thenReturn(OfcOrderType.INSTORE_PICKUP);
        Mockito.when(targetConsignment.getConsignmentVersion()).thenReturn(null);
        assertEquals(action.execute(process), "DELIVERY_CONSIGNMENT_NEW_VERSION");
    }
}
