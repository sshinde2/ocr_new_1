/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.cucumber.holder.DataHolder;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.DeliveryUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



/**
 * @author Pradeep
 *
 */
public class DeliveryPromotionalStep {

    private static final String EXPRESS_DELIVERY_STICKER_MESSAGE = "express sticker";
    private static final String HOME_DELIVERY_STICKER_MESSAGE = "home sticker";
    private static final String CNC_DELIVERY_STICKER_MESSAGE = "cnc sticker";
    private static final String EXPRESS_DELIVERY_PROMO_TEXT = "express";
    private static final String HOME_DELIVERY_PROMO_TEXT = "home";
    private static final String CNC_DELIVERY_PROMO_TEXT = "cnc";
    private static final String STICKER_PRESENT = "STICKER PRESENT";
    private static final String TEXT_PRESENT = "TEXT PRESENT";
    private ProductModel product;
    private Collection<TargetZoneDeliveryModeData> deliveryModeDataList;
    private Collection<TargetZoneDeliveryModeModel> deliveryModes;

    @Given("^a product with '(.*)'$")
    public void fetchProduct(final String productCode) {
        product = ProductUtil.getProductModel(productCode);
    }

    @Given("^delivery promotion ranks order is '(.*)'$")
    public void populateDeliveryPromotionRanks(final String deliveryPromotionRanks) {
        //starting with 20 promotion Display Ranks will be assigned as per order.
        int count = 20;
        final String[] deliveryPromoRanks = deliveryPromotionRanks.trim().split(",");
        for (final String deliveryPromoOrder : deliveryPromoRanks) {
            for (final DeliveryModeModel eachDeliveryMode : product.getDeliveryModes()) {
                if (eachDeliveryMode.getCode().contains(deliveryPromoOrder)) {
                    ((TargetZoneDeliveryModeModel)eachDeliveryMode).setDeliveryPromotionRank(Integer.valueOf(count++));
                    break;
                }
            }
        }
        ServiceLookup.getModelService().saveAll(product.getDeliveryModes());

    }

    @Then("^promotional messages set for the delivery modes are:$")
    public void promotionalDeliveryPromotionalText(
            final List<TargetZoneDeliveryModeData> targetZoneDeliveryModeDataList) {
        for (final TargetZoneDeliveryModeData targetZoneDeliveryModeData : targetZoneDeliveryModeDataList) {
            final List<AbstractTargetZoneDeliveryModeValueModel> availableModeValues = getApplicableDeliveryModeValues(
                    targetZoneDeliveryModeData.getCode());
            if (CollectionUtils.isNotEmpty(availableModeValues)) {
                final RestrictableTargetZoneDeliveryModeValueModel restrictableModel = DeliveryUtil
                        .getHighestPriorityRestriction(availableModeValues);
                if (restrictableModel.getDeliveryMode().getCode()
                        .equals(targetZoneDeliveryModeData.getCode())) {
                    restrictableModel.setIncentiveMetMessage(targetZoneDeliveryModeData.getIncentiveMetMessage());
                    restrictableModel
                            .setPotentialIncentiveMessage(targetZoneDeliveryModeData.getPotentialIncentiveMessage());
                }
                ServiceLookup.getModelService().save(restrictableModel);
            }
        }
    }


    @Given("^express delivery promo display settings are '(.*)'$")
    public void expressDeliveryPromoDisplaySettings(
            final String expressDeliveryPromoSettings) {
        if (!StringUtils.equals(expressDeliveryPromoSettings, "N/A")) {
            final List<AbstractTargetZoneDeliveryModeValueModel> availableModeValues = getApplicableDeliveryModeValues(
                    CheckoutUtil.EXPRESS_DELIVERY_CODE);
            if (CollectionUtils.isNotEmpty(availableModeValues)) {
                final String[] expressDeliveryPromotions = expressDeliveryPromoSettings.trim().split(",");
                final RestrictableTargetZoneDeliveryModeValueModel restrictableModel = DeliveryUtil
                        .getHighestPriorityRestriction(availableModeValues);
                restrictableModel.setPromotionalStickerMessage(StringUtils.equals(expressDeliveryPromotions[0],
                        STICKER_PRESENT) ? EXPRESS_DELIVERY_STICKER_MESSAGE : StringUtils.EMPTY);
                populatePromoText(expressDeliveryPromotions[1], restrictableModel, EXPRESS_DELIVERY_PROMO_TEXT);
                ServiceLookup.getModelService().save(restrictableModel);
            }
        }
    }

    @Given("^pre order message for '(.*)' is '(.*)'$")
    public void expressDeliveryPreOrderMessage(
            final String deliveryMode, final String message) {
        final List<AbstractTargetZoneDeliveryModeValueModel> availableModeValues = getApplicableDeliveryModeValues(
                deliveryMode);
        if (CollectionUtils.isNotEmpty(availableModeValues)) {
            final RestrictableTargetZoneDeliveryModeValueModel restrictableModel = DeliveryUtil
                    .getHighestPriorityRestriction(availableModeValues);
            if (restrictableModel.getDeliveryMode().getCode().equals(deliveryMode)) {
                restrictableModel.setPreOrderMessage(message);
            }
            ServiceLookup.getModelService().save(restrictableModel);
        }

    }

    @Given("^home delivery promo display settings are '(.*)'$")
    public void homeDeliveryPromoDisplaySettings(
            final String homeDeliveryPromoSettings) {
        if (!StringUtils.equals(homeDeliveryPromoSettings, "N/A")) {
            final String[] homeDeliveryPromotions = homeDeliveryPromoSettings.trim().split(",");
            final RestrictableTargetZoneDeliveryModeValueModel restrictableModel = DeliveryUtil
                    .getHighestPriorityRestriction(getApplicableDeliveryModeValues(
                            CheckoutUtil.HOME_DELIVERY_CODE));
            restrictableModel.setPromotionalStickerMessage(StringUtils
                    .equals(homeDeliveryPromotions[0], STICKER_PRESENT) ? HOME_DELIVERY_STICKER_MESSAGE
                            : StringUtils.EMPTY);
            populatePromoText(homeDeliveryPromotions[1], restrictableModel, HOME_DELIVERY_PROMO_TEXT);
            ServiceLookup.getModelService().save(restrictableModel);
        }
    }

    @Given("^cnc delivery promo display settings are '(.*)'$")
    public void cncDeliveryPromoDisplaySettings(
            final String cncDeliveryPromoSettings) {
        if (!StringUtils.equals(cncDeliveryPromoSettings, "N/A")) {
            final String[] cncDeliveryPromotions = cncDeliveryPromoSettings.trim().split(",");
            final RestrictableTargetZoneDeliveryModeValueModel restrictableModel = DeliveryUtil
                    .getHighestPriorityRestriction(getApplicableDeliveryModeValues(
                            CheckoutUtil.CLICK_AND_COLLECT_CODE));
            restrictableModel.setPromotionalStickerMessage(StringUtils
                    .equals(cncDeliveryPromotions[0], STICKER_PRESENT) ? CNC_DELIVERY_STICKER_MESSAGE
                            : StringUtils.EMPTY);
            populatePromoText(cncDeliveryPromotions[1], restrictableModel, CNC_DELIVERY_PROMO_TEXT);
            ServiceLookup.getModelService().save(restrictableModel);
        }
    }


    @Then("^delivery promotion sticker is '(.*)'$")
    public void testDeliveryPromotionSticker(final String deliveryPromoSticker) {
        if (StringUtils.equals(deliveryPromoSticker, "NONE")) {
            Assert.assertTrue(StringUtils.isEmpty(
                    DataHolder.getInstance().getTargetProductData().getProductPromotionalDeliverySticker()));
        }
        else {
            Assert.assertEquals(deliveryPromoSticker,
                    DataHolder.getInstance().getTargetProductData().getProductPromotionalDeliverySticker());
        }
        DeliveryUtil.deleteRestrictionCreated();
    }

    @Then("^delivery promotion text is '(.*)'$")
    public void testDeliveryPromotionText(final String deliveryPromoText) {
        if (StringUtils.equals(deliveryPromoText, "NONE")) {
            Assert.assertTrue(StringUtils.isEmpty(
                    DataHolder.getInstance().getTargetProductData().getProductPromotionalDeliveryText()));
        }
        else {
            Assert.assertEquals(deliveryPromoText,
                    DataHolder.getInstance().getTargetProductData().getProductPromotionalDeliveryText());
        }
    }

    private void populatePromoText(final String promoText,
            final RestrictableTargetZoneDeliveryModeValueModel restrictableModel, final String deliveryPromoText) {
        restrictableModel
                .setDeliveryPromotionMessage(StringUtils.equals(promoText, TEXT_PRESENT) ? deliveryPromoText
                        : StringUtils.EMPTY);

    }

    /**
     * @return list of ZoneDelivery Mode Value
     */
    private List<AbstractTargetZoneDeliveryModeValueModel> getApplicableDeliveryModeValues(
            final String deliveryModeCode) {
        final List<AbstractTargetZoneDeliveryModeValueModel> applicableDeliveryModeValues = ServiceLookup
                .getTargetDeliveryService().getAllApplicableDeliveryValuesForModeAndProduct(
                        (ZoneDeliveryModeModel)getDeliveryMode(deliveryModeCode), product);
        return applicableDeliveryModeValues;
    }

    private DeliveryModeModel getDeliveryMode(final String deliveryModeCode) {
        for (final DeliveryModeModel eachDeliveryMode : product.getDeliveryModes()) {
            if (StringUtils.equalsIgnoreCase(eachDeliveryMode.getCode(), deliveryModeCode)) {
                return eachDeliveryMode;
            }
        }
        return null;
    }

    @Given("^Maximum dead weight restriction for the express delivery is set to (\\d+)kg$")
    public void theMaximumWeightSet(final int maxWeight) {
        DeliveryUtil.setMaxWeightRestriction(maxWeight);
    }

    @Given("^Maximum dimension restriction for the express delivery is set to (\\d+)cms$")
    public void theMaximumDimensionRestriction(final int maxDimension) {
        DeliveryUtil.setMaxDimensionRestriction(maxDimension);
    }

    @Given("^Maximum volume restriction for the express delivery is set to (\\d+) cubic cms.$")
    public void theMaxVolumeRestriction(final int maxVolume) {
        DeliveryUtil.setMaxVolumeRestriction(maxVolume);
    }


    @When("^product details are retrieved$")
    public void productDetailsRetrieved() {
        DataHolder.getInstance().setTargetProductData(
                (TargetProductData)ServiceLookup.getTargetProductFacade().getProductForOptions(product,
                        Arrays.asList(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE, ProductOption.DELIVERY_ZONE)));
    }

    @When("^full product details are retrieved for '(.*)'$")
    public void fullProductDetailsRetrievedForProduct(final String productCode) {
        CatalogUtil.setSessionCatalogVersion();
        DataHolder.getInstance().setTargetProductData((TargetProductData)ServiceLookup.getTargetProductFacade()
                .getBulkyBoardProductForOptions(
                        ProductUtil.getOnlineProductModel(productCode),
                        Arrays.asList(
                                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY,
                                ProductOption.DESCRIPTION, ProductOption.CATEGORIES, ProductOption.REVIEW,
                                ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL,
                                ProductOption.STOCK),
                        null));
    }

    @Then("^delivery modes available for the product are '(.*)'$")
    public void theAvailableDeliveryModesForProduct(final String deliveryModesAvailable) {
        final String[] modes = deliveryModesAvailable.split(",");
        Assert.assertEquals(modes.length,
                countOfActiveDeliveryModes(DataHolder.getInstance().getTargetProductData()));
        DeliveryUtil.deleteRestrictionCreated();
    }

    @Then("^giftcard flag on the product is set to '(true|false)'")
    public void theGiftFlagOnProduct(final boolean isGiftCard) {
        Assert.assertEquals(Boolean.valueOf(isGiftCard),
                Boolean.valueOf(DataHolder.getInstance().getTargetProductData().isGiftCard()));
    }

    @Then("^stock status for the product is '(.*)'$")
    public void verifyProductStockStatus(final String status) {
        assertThat(DataHolder.getInstance().getTargetProductData().getStock()).isNotNull();
        assertThat(DataHolder.getInstance().getTargetProductData().getStock().getStockLevelStatus()).isNotNull();
        final String actualStatus = DataHolder.getInstance().getTargetProductData().getStock()
                .getStockLevelStatus().toString();
        assertThat(actualStatus).isEqualToIgnoringCase(status);
    }

    @When("^the delivery modes for the cart is fetched$")
    public void whenTheDeliveryModesForTheCartIsFetched() {
        // GET CART MODEL AND FETCH APPLICABLE DELIVERY MODES FOR CART
        final CartModel cart = CartUtil.getSessionCartModel();
        deliveryModes = ServiceLookup.getTargetDeliveryService().getAllApplicableDeliveryModesForOrder(cart,
                ServiceLookup.getTargetSalesApplicationService().getCurrentSalesApplication());
        deliveryModeDataList = ((TargetCartData)ServiceLookup.getTargetCheckoutFacade().getCheckoutCart())
                .getDeliveryModes();
    }

    @Then("^delivery modes available for the cart are '(.*)'$")
    public void thenDeliveryModesAvailableForTheCart(final String deliveryModesAvailable) {
        final String[] modes = deliveryModesAvailable.split(",");
        Assert.assertEquals(modes.length, deliveryModes.size());
        DeliveryUtil.deleteRestrictionCreated();
        verifyDeliveryModesDisplayedOnSinglePageCheckout(deliveryModesAvailable);
    }

    @Then("^delivery modes displayed for single page checkout are '(.*)'$")
    public void verifyDeliveryModesDisplayedOnSinglePageCheckout(final String deliveryModesAvailable) {
        final List<String> allModes = ImmutableList.of("express-delivery", "home-delivery", "click-and-collect");
        final String[] modes = deliveryModesAvailable.split(",");
        final List<String> availableModeCodes = Arrays.asList(modes);
        assertThat(deliveryModeDataList.size()).isEqualTo(allModes.size());
        for (final TargetZoneDeliveryModeData deliveryMode : deliveryModeDataList) {
            if (availableModeCodes.contains(deliveryMode.getCode())) {
                assertThat(deliveryMode.isAvailable()).isEqualTo(true);
            }
            else {
                assertThat(deliveryMode.isAvailable()).isEqualTo(false);
            }
            assertThat(allModes.contains(deliveryMode.getCode())).isEqualTo(true);
        }
    }

    @Given("^delivery value with dead weight restriction as (\\d+)kg, dimension restriction as (\\d+)cms and volume restriction as (\\d+) cubic cms for the express delivery$")
    public void givenDeliveryValueWeightDimensionAndVolumeRestriction(final int maxWeight, final int maxDimension,
            final int maxVolume) {
        DeliveryUtil.setMaxWeightDimensionAndVolumeRestriction(maxWeight, maxDimension, maxVolume);
    }

    /**
     * Method to fetch all the available delivery modes for the product
     * 
     * @param targetProductData
     * @return count of available delivery modes
     */
    private int countOfActiveDeliveryModes(final TargetProductData targetProductData) {
        int count = 0;
        if (CollectionUtils.isNotEmpty(targetProductData.getDeliveryModes())) {
            for (final TargetZoneDeliveryModeData deliveryModeData : targetProductData.getDeliveryModes()) {
                if (deliveryModeData.isAvailable()) {
                    count++;
                }
            }
        }
        return count;
    }

}
