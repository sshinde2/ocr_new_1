/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.cucumber.holder.DataHolder;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.DeliveryModeAvailability;
import au.com.target.tgttest.automation.facade.bean.ProductData;
import au.com.target.tgttest.automation.facade.bean.ProductPartialData;
import au.com.target.tgtutility.util.TargetDateUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.sourceforge.pmd.util.StringUtil;


/**
 * @author rmcalave
 *
 */
public class ProductDataSteps {

    private TargetProductData productData;

    private ProductModel redirectedProduct;

    private TargetColourVariantProductModel assortedProduct;

    private TargetProductListerData productListerData;

    private TargetVariantProductListerData colourVariant;

    private List<TargetZoneDeliveryModeData> deliveryModes;





    @When("^the product data with deal message is retrieved using the variant code (\\d+)$")
    public void theProductDataWithDealMessageIsRetrievedUsingTheVariantCode(final String productCode) {
        productData = ServiceLookup.getTargetProductFacade().getProductForCodeAndOptionsWithDealMessage(productCode,
                Arrays.asList(ProductOption.PROMOTIONS));
    }

    @Given("^an (.*) product with code '(.*)'$")
    public void fetchProduct(final String assorted, final String productCode) {
        final ProductModel prdModel = ProductUtil.getProductModel(productCode);
        if (prdModel instanceof TargetColourVariantProductModel) {
            assortedProduct = (TargetColourVariantProductModel)prdModel;
            if (StringUtils.equals(assorted, "assorted")) {
                assortedProduct.setAssorted(true);
            }
            else if (StringUtils.equals(assorted, "non-assorted")) {
                assortedProduct.setAssorted(false);
            }
        }
    }

    @When("^the product is retrieved$")
    public void retrieveProduct() {
        productData = ProductUtil.populateThePriceIntoProductData(assortedProduct.getCode());
    }

    @Then("^the product data returned contains the assorted flag as (.*)$")
    public void containsAssortedFlag(final boolean assortedStatus) {
        assertThat(productData.getCode()).isEqualTo(assortedProduct.getCode());
        assertThat(productData.isAssorted()).isEqualTo(assortedStatus);
    }

    @Then("^the product should be retrieved with the following deal details:$")
    public void theProductShouldBeRetrievedWithTheFollowingDealDetails(
            final List<ProductPartialData> productPartialDataList) {
        for (final ProductPartialData productPartialData : productPartialDataList) {
            assertThat(productData.getCode()).isEqualTo(productPartialData.getProductCode());

            final Collection<PromotionData> potentialPromotions = productData.getPotentialPromotions();

            if ("null".equalsIgnoreCase(productPartialData.getDealDescription())
                    && "null".equalsIgnoreCase(productPartialData.getCouldFireMessage())) {
                assertThat(productData.getDealDescription()).isNull();
                assertThat(potentialPromotions).isNullOrEmpty();
            }
            else {
                if (CollectionUtils.isEmpty(potentialPromotions)) {
                    if (StringUtils.isNotBlank(productData.getDealDescription())) {
                        assertThat(productData.getDealDescription()).isEqualTo(productPartialData.getDealDescription());
                    }
                }
                else {
                    assertThat(potentialPromotions).hasSize(1);

                    final PromotionData potentialPromotion = potentialPromotions.iterator().next();

                    if ("null".equalsIgnoreCase(productPartialData.getDealDescription())) {
                        assertThat(potentialPromotion.getDescription()).isNull();
                    }
                    else {
                        assertThat(potentialPromotion.getDescription()).isEqualTo(
                                productPartialData.getDealDescription());
                    }

                    final List<String> couldFireMessages = potentialPromotion.getCouldFireMessages();
                    if ("null".equalsIgnoreCase(productPartialData.getCouldFireMessage())) {
                        assertThat(couldFireMessages).isNullOrEmpty();
                    }
                    else {
                        assertThat(couldFireMessages).hasSize(1);
                        assertThat(couldFireMessages.get(0)).isEqualTo(productPartialData.getCouldFireMessage());
                    }
                }
            }
        }
    }



    @When("^product to redirect is retrieved for '(.*)'$")
    public void callProductToRedirect(final String productCode) {
        final ProductModel product = ServiceLookup.getTargetProductService()
                .getProductForCode(productCode);
        redirectedProduct = ServiceLookup.getTargetProductFacade().getProductToRedirect(product);
    }

    @When("^product details are requested for new pdp '(.*)'$")
    public void populateProductListerDataFor(final String baseProduct) {
        productListerData = ServiceLookup.getTargetProductListerFacade().getBaseProductDataByCode(baseProduct,
                populateProductOptions());
    }

    @Then("^the product details for the variants are :$")
    public void verifyProductDetails(final List<ProductData> expectedEntries) {
        for (final ProductData prdData : expectedEntries) {
            if (prdData.isColorVariant()) {
                populateColourVariant(prdData);
                verifyProductData(prdData);
            }
        }

    }

    @Then("^product to redirect is '(.*)'$")
    public void checkProductToBeRedirected(final String productCode) {
        assertThat(redirectedProduct).isNotNull();
        assertThat(StringUtils.equals(redirectedProduct.getCode(), productCode));
    }

    @When("^product details are retrieved for '(.*)'$")
    public void productDetailsRetrievedForProduct(final String productCode) {
        CatalogUtil.setSessionCatalogVersion();
        DataHolder.getInstance().setTargetProductData(
                (TargetProductData)ServiceLookup.getTargetProductFacade().getProductForCodeAndOptions(productCode,
                        Arrays.asList(ProductOption.DELIVERY_ZONE)));
    }


    @Then("^the delivery mode values for the product are :$")
    public void verifyDeliveryModeData(final List<DeliveryModeAvailability> deliveryModeList) {

        final Map<String, TargetZoneDeliveryModeData> deliveryModeMap = Maps.uniqueIndex(deliveryModes,
                new Function<TargetZoneDeliveryModeData, String>() {
                    @Override
                    public String apply(final TargetZoneDeliveryModeData arg0) {
                        return arg0.getName();
                    }
                });

        for (final DeliveryModeAvailability deliveryMode : deliveryModeList) {
            final TargetZoneDeliveryModeData actualDeliveryMode = deliveryModeMap.get(deliveryMode.getName());
            assertThat(actualDeliveryMode).isNotNull();
            assertThat(actualDeliveryMode.getShortDescription()).isEqualTo(deliveryMode.getShortDescription());
            assertThat(actualDeliveryMode.getCode()).isEqualTo(deliveryMode.getCode());
        }

    }


    private List<ProductOption> populateProductOptions() {
        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        return productOptions;
    }

    /**
     * Finds the colour variant in the list and set it to the variable colourVariant which will be used for verification
     * 
     * @param prdData
     */
    private void populateColourVariant(final ProductData prdData) {
        final List<TargetVariantProductListerData> variants = productListerData.getTargetVariantProductListerData();
        for (final TargetVariantProductListerData variant : variants) {
            if (variant.getCode().equalsIgnoreCase(prdData.getProductCode()) && prdData.isColorVariant()) {
                colourVariant = variant;
                deliveryModes = colourVariant.getDeliveryModes();
                break;
            }
        }
    }

    private void verifyProductData(final ProductData expectedVariantData) {
        assertThat(colourVariant.getCode()).isEqualTo(expectedVariantData.getProductCode());
        if (StringUtils.isEmpty(expectedVariantData.getProductDisplayType())) {
            assertThat(colourVariant.getProductDisplayType()).isNull();
        }
        else {
            assertThat(colourVariant.getProductDisplayType().getType()).isEqualTo(
                    expectedVariantData.getProductDisplayType());
        }
        if (StringUtils.isEmpty(expectedVariantData.getNormalSalesDate())) {
            assertThat(colourVariant.getNormalSaleStartDate()).isNull();
        }
        else {
            final Date expectedDate = TargetDateUtil.getStringAsDate(expectedVariantData.getNormalSalesDate());
            assertThat(TargetDateUtil.getDateStringForComparison(expectedDate)).isEqualTo(
                    TargetDateUtil.getDateStringForComparison(colourVariant
                            .getNormalSaleStartDate()));
        }

    }

    @Given("^product '(.*)' with max quantity '(.*)'$")
    public void setMaxProductQty(final String productCode, final String qty) throws CommerceCartModificationException {
        if (StringUtil.isNotEmpty(qty)) {
            final ProductModel productModel = ServiceLookup.getTargetProductFacade().getProductForCode(productCode);
            productModel.setMaxOrderQuantity(Integer.valueOf(qty));
            assertThat(productModel.getMaxOrderQuantity()).isEqualTo(Integer.valueOf(qty));
        }
    }

    @Then("excludeForZipPayment is '(.*)'")
    public void verifyZipPaymentExcludeFlag(final Boolean expected) {
        final List<TargetVariantProductListerData> variants = productListerData.getTargetVariantProductListerData();
        assertThat(variants).isNotEmpty();
        assertThat(variants.get(0).isExcludeForZipPayment()).isEqualTo(expected);
    }
}
