/**
 *
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.model.TargetSharedConfigModel;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.PreviousPermanentPriceUtil;
import au.com.target.tgttest.automation.facade.bean.PrevPermanentPriceEntry;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author mjanarth
 * 
 */
public class PreviousPermanentPriceSteps {

    private TargetPreviousPermanentPriceModel previousPermanentPrice;

    private String productId;

    private final TargetPreviousPermanentPriceService targetPreviousPermanentPriceService = ServiceLookup
            .getTgtPreviouspermPriceService();
    private final ModelService modelService = ServiceLookup.getModelService();
    private final FlexibleSearchService flexibleSearchService = ServiceLookup.getFlexibleSearchService();

    @Given("^that a previous permanent price record does not exist for product '(.*)'$")
    public void clearPreviouSPermanentPrice(final String variantCode) {
        PreviousPermanentPriceUtil.clearPreviousPermanentPrice(variantCode);
    }

    @Then("^a previous permanent price record is created:$")
    public void verifyPermanentPriceUpdates(final List<PrevPermanentPriceEntry> prevpermPriceEntries) {
        // Get the variant code out of the first expected row
        assertThat(prevpermPriceEntries).isNotEmpty();
        final String variantCode = prevpermPriceEntries.get(0).getVariantCode();
        final TargetPreviousPermanentPriceModel actualResult = PreviousPermanentPriceUtil
                .getPreviousPermPriceWithNullEndDate(variantCode);

        if (CollectionUtils.isNotEmpty(prevpermPriceEntries)) {
            final PrevPermanentPriceEntry expectedResult = prevpermPriceEntries.get(0);
            Calendar actualStartDate = null;
            Calendar actualEndDate = null;
            assertThat(actualResult.getPrice()).isEqualTo(expectedResult.getPrice());
            assertThat(actualResult.getVariantCode()).isEqualTo(expectedResult.getVariantCode());

            final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
            final Calendar expectedStartDate = dateUtil.interpretStringAsCalendar(expectedResult
                    .getStartDate());
            final Calendar expectedEndDate = dateUtil.interpretStringAsCalendar(expectedResult
                    .getEndDate());
            if (null != actualResult.getStartDate()) {
                actualStartDate = Calendar.getInstance();
                actualStartDate.setTime(actualResult.getStartDate());
            }
            if (null != actualResult.getEndDate()) {
                actualEndDate = Calendar.getInstance();
                actualEndDate.setTime(actualResult.getEndDate());
            }
            assertThat(compareDatesAreSameDay(actualEndDate, expectedEndDate)).isTrue();
            assertThat(compareDatesAreSameDay(actualStartDate, expectedStartDate)).isTrue();


        }

    }

    @Then("^a previous permanent price is not created for product '(.*)'$")
    public void previousPermanentPriceNotUpdated(final String productCode) {
        final TargetPreviousPermanentPriceModel previousPermPrice = PreviousPermanentPriceUtil
                .getPreviousPermPriceWithNullEndDate(productCode);
        assertThat(previousPermPrice).isNull();

    }


    @Given("^that a previous permanent price record exists for product '(.*)'$")
    public void permanentPriceExistsForProduct(final String productCode,
            final List<PrevPermanentPriceEntry> priceEntries) {
        PreviousPermanentPriceUtil.clearPreviousPermanentPrice(productCode);
        productId = productCode;

        for (int i = 0; i < priceEntries.size(); i++) {
            if (i == 0) {
                previousPermanentPrice = PreviousPermanentPriceUtil.createPreviousPermanentPrice(priceEntries
                        .get(i));
            }
            else {
                PreviousPermanentPriceUtil.createPreviousPermanentPrice(priceEntries.get(i));
            }
        }


    }



    @Then("^previous permanent price record is updated with:$")
    public void permanentPriceUpdated(final List<PrevPermanentPriceEntry> prevpermPriceEntries) {

        // Get the variant code out of the first expected row
        assertThat(prevpermPriceEntries).isNotEmpty();
        final String variantCode = prevpermPriceEntries.get(0).getVariantCode();
        final List<TargetPreviousPermanentPriceModel> actualResultList = PreviousPermanentPriceUtil
                .getPreviousPermanentPrices(variantCode);

        assertThat(prevpermPriceEntries.size()).isEqualTo(actualResultList.size());

        int i = 0;
        for (final PrevPermanentPriceEntry expectedResult : prevpermPriceEntries) {
            Calendar actualStartDate = null;
            Calendar actualEndDate = null;
            final TargetPreviousPermanentPriceModel actualResult = actualResultList.get(i);
            assertThat(actualResult.getPrice()).isEqualTo(expectedResult.getPrice());
            assertThat(actualResult.getVariantCode()).isEqualTo(expectedResult.getVariantCode());

            final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
            final Calendar expectedStartDate = dateUtil.interpretStringAsCalendar(expectedResult
                    .getStartDate());
            final Calendar expectedEndDate = dateUtil.interpretStringAsCalendar(expectedResult
                    .getEndDate());
            if (null != actualResult.getStartDate()) {
                actualStartDate = Calendar.getInstance();
                actualStartDate.setTime(actualResult.getStartDate());
            }
            if (null != actualResult.getEndDate()) {
                actualEndDate = Calendar.getInstance();
                actualEndDate.setTime(actualResult.getEndDate());
            }
            assertThat(compareDatesAreSameDay(actualEndDate, expectedEndDate)).isTrue();
            assertThat(compareDatesAreSameDay(actualStartDate, expectedStartDate)).isTrue();
            i++;

        }

    }

    /**
     * @param minimumRecordAge
     */
    @Given("^price has to be advertised continuously for (\\d+) or more days in order to qualify to be shown as was price.$")
    public void priceHasToBeAdvertisedContinuouslyForXOrMoreDaysInOrderToQualifyToBeShownAsWasPrice(
            final int minimumRecordAge) {
        final TargetSharedConfigModel previousPermPriceMinAgeExample = new TargetSharedConfigModel();
        previousPermPriceMinAgeExample.setCode(TgtCoreConstants.Config.PREVIOUS_PERM_PRICE_MIN_AGE);

        TargetSharedConfigModel previousPermPriceMinAge = flexibleSearchService
                .getModelByExample(previousPermPriceMinAgeExample);
        if (previousPermPriceMinAge == null) {
            previousPermPriceMinAge = modelService.create(TargetSharedConfigModel.class);
            previousPermPriceMinAge.setCode(TgtCoreConstants.Config.PREVIOUS_PERM_PRICE_MIN_AGE);
        }

        previousPermPriceMinAge.setValue(String.valueOf(minimumRecordAge));
        modelService.save(previousPermPriceMinAge);
    }

    /**
     * @param maximumCurrentRecordAge
     */
    @Given("^the ACCC number of days product is available at a price during which we can display the was price is (\\d+) or less$")
    public void theAcccNumberOfDaysProductIsAvailableAtAPriceDuringWhichWeCanDisplayTheWasPriceIsXOrLess(
            final int maximumCurrentRecordAge) {
        final TargetSharedConfigModel currentPermPriceMaxAgeExample = new TargetSharedConfigModel();
        currentPermPriceMaxAgeExample.setCode(TgtCoreConstants.Config.CURRENT_PERM_PRICE_MAX_AGE);

        TargetSharedConfigModel currentPermPriceMaxAge = flexibleSearchService
                .getModelByExample(currentPermPriceMaxAgeExample);
        if (currentPermPriceMaxAge == null) {
            currentPermPriceMaxAge = modelService.create(TargetSharedConfigModel.class);
            currentPermPriceMaxAge.setCode(TgtCoreConstants.Config.CURRENT_PERM_PRICE_MAX_AGE);
        }

        currentPermPriceMaxAge.setValue(String.valueOf(maximumCurrentRecordAge));
        modelService.save(currentPermPriceMaxAge);
    }

    @When("^the previous permanent price clean-up job is run$")
    public void thePreviousPermanentPriceCleanUpJobIsRun() {
        targetPreviousPermanentPriceService.cleanupPreviousPermanentPriceRecords();
    }

    @Then("^the previous permanent price record is deleted$")
    public void thePreviousPermanentPriceRecordIsDeleted() {
        final List<TargetPreviousPermanentPriceModel> previousPermanentPrices = PreviousPermanentPriceUtil
                .getPreviousPermanentPrices(productId);

        assertThat(previousPermanentPrices).excludes(previousPermanentPrice);
    }

    @Then("^the previous permanent price record is not deleted$")
    public void thePreviousPermanentPriceRecordIsNotDeleted() {
        final List<TargetPreviousPermanentPriceModel> previousPermanentPrices = PreviousPermanentPriceUtil
                .getPreviousPermanentPrices(productId);

        assertThat(previousPermanentPrices).contains(previousPermanentPrice);
    }

    @Given("^initially there are no previous permanent price records$")
    public void thereAreNoPreviousPermanentPriceRecords() {
        PreviousPermanentPriceUtil.removeAllPrevPermPriceRecords();
    }

    private boolean compareDatesAreSameDay(final Calendar expectedDate, final Calendar actualDate) {
        boolean assertDate = false;
        if (null == expectedDate && null == actualDate) {
            return true;
        }
        else if (null == expectedDate || null == actualDate) {
            return false;
        }
        else if (DateUtils.isSameDay(expectedDate, actualDate)) {
            assertDate = true;
        }
        return assertDate;

    }
}
