/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.jalo.SavedValueEntry;
import de.hybris.platform.hmc.jalo.SavedValues;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.internal.jalo.ServicelayerManager;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;
import java.util.Set;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.customer.impl.DefaultTargetCustomerFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.bean.CustomerAccountData;
import au.com.target.tgttest.automation.facade.bean.ModifiedCustomerModelLoggedInfo;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author bpottass
 *
 */
public class CustomerModelModificationLoggingSteps {

    private ModifiedCustomerModelLoggedInfo modifiedCustomerModelLoggedInfo;

    private final UserService userService = ServiceLookup.getUserService();

    private final DefaultTargetCustomerFacade targetCustomerFacade = ServiceLookup.getTargetCustomerFacade();

    private final ModelService modelService = ServiceLookup.getModelService();

    private CustomerAccountData customerAccountData;

    private CustomerData customerData;

    @Given("^a customer exists with the following CustomerAccountData details:")
    public void customerWithSubscriptionDetails(final List<CustomerAccountData> entries)
            throws DuplicateUidException, InterruptedException {
        customerAccountData = entries.get(0);
        final UserModel userModel = userService.getUserForUID(customerAccountData.getLogin());
        userService.setCurrentUser(userModel);
        customerData = new TargetCustomerData();
        customerData.setUid(customerAccountData.getLogin());
        customerData.setFirstName(customerAccountData.getFirstName());
        customerData.setLastName(customerAccountData.getLastName());
        customerData.setTitle("Mrs");
    }


    @When("^The user logs into his account and changes:$")
    public void customerChangesDetails(final List<ModifiedCustomerModelLoggedInfo> entries)
            throws DuplicateUidException {
        modifiedCustomerModelLoggedInfo = entries.get(0);
        final String changedAttribute = modifiedCustomerModelLoggedInfo.getChangedAttribute();
        final String newValue = modifiedCustomerModelLoggedInfo.getNewValue();
        if ("firstname".equalsIgnoreCase(changedAttribute)) {
            customerData.setFirstName(newValue);
            targetCustomerFacade.updateProfile(customerData);
        }
        else if ("lastname".equalsIgnoreCase(changedAttribute)) {
            customerData.setLastName(newValue);
            targetCustomerFacade.updateProfile(customerData);
        }
        else if ("uid".equals(changedAttribute)) {
            ((CustomerModel)userService.getCurrentUser()).setOriginalUid(changedAttribute);
        }

    }

    @Then("^the changes to customer data will be recorded as follows:$")
    public void changesToCustomerDataRecord(
            final List<ModifiedCustomerModelLoggedInfo> entries) {

        modifiedCustomerModelLoggedInfo = entries.get(0);

        final String changedBy = modifiedCustomerModelLoggedInfo.getChangedBy();
        final String changedAttribute = modifiedCustomerModelLoggedInfo.getChangedAttribute();
        final String oldValue = modifiedCustomerModelLoggedInfo.getOldValue();
        final String newValue = modifiedCustomerModelLoggedInfo.getNewValue();

        final TargetCustomerData targetCustomerData = CustomerUtil.getCurrentCustomer();
        final TargetCustomerModel targetCustomerModel = CustomerUtil.getTargetCustomer(targetCustomerData.getUid());

        final Set<SavedValues> savedValues = ServicelayerManager.getInstance()
                .getSavedValues((Item)modelService.getSource(targetCustomerModel));
        for (final SavedValues savedValue : savedValues) {
            assertThat(savedValue.getUser()).isEqualTo(changedBy);
            final Set<SavedValueEntry> savedValueEntries = savedValue.getSavedValuesEntries();
            for (final SavedValueEntry savedValueEntry : savedValueEntries) {
                assertThat(savedValueEntry.getModifiedAttribute()).isEqualTo(changedAttribute);
                assertThat(savedValueEntry.getOldValue()).isEqualTo(oldValue);
                assertThat(savedValueEntry.getNewValue()).isEqualTo(newValue);
            }
        }
    }




}
