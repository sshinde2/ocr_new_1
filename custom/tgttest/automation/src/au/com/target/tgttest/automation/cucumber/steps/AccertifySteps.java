/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import java.util.List;

import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.facade.bean.AccertifyCardInfo;
import au.com.target.tgttest.automation.facade.bean.AccertifyOrderItem;
import au.com.target.tgttest.automation.facade.bean.AccertifyTransactionInfo;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CreditCardData;
import au.com.target.tgttest.automation.facade.bean.FailZipDetails;
import au.com.target.tgttest.automation.facade.domain.AccertifyTransactions;
import au.com.target.tgttest.automation.mock.MockAccertifyRestOperations;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


/**
 * @author cwijesu1
 * 
 */
public class AccertifySteps {

    @Given("^feature 'accertify.include.billingAddress' is switched '(on|off)'$")
    public void includeBillingAddressInAccertify(final String status) {
        if ("off".equals(status)) {
            ImpexImporter
                    .importCsv("/tgttest/automation-impex/accertify/accertify-billing-address-exclude.impex");
        }
        else {
            ImpexImporter
                    .importCsv("/tgttest/automation-impex/accertify/accertify-billing-address-include.impex");
        }
    }

    @Given("^the order would trigger an Accertify response of '(.*)'$")
    public void givenAccertifyStatus(final String status) {
        MockAccertifyRestOperations.setRecommendation(status);
    }

    @Then("^accertify entries:$")
    public void accertifyEntries(final List<AccertifyTransactionInfo> entries) {
        AccertifyTransactions.getLastTransaction().verifyEntries(entries);
    }

    @Then("^the accertify payment entries:$")
    public void accertifyPaymentEntries(final List<AccertifyCardInfo> cardInfo) {
        AccertifyTransactions.getLastTransaction().verifyPaymentInformation(cardInfo);
    }

    @Then("^the accertify billing information entries:$")
    public void accertifyBillingInfo(final List<AddressEntry> addressEntries) {
        final AddressEntry address = addressEntries.get(0);
        AccertifyTransactions.getLastTransaction().verifyAddressInformation(address);
    }

    @Then("^the accertify billing information is (excluded|included)$")
    public void accertifyBillingInfo(final String included) {
        AccertifyTransactions.getLastTransaction().isBillingAddressIncluded("included".equals(included));
    }

    @Then("^the billing information for the failed payment is (excluded|included)$")
    public void accertifyFailedBillingInfo(final String included) {
        AccertifyTransactions.getLastTransaction().isBillingInfoIncludedInFailedCCDetails("included".equals(included));
    }

    @Then("^the failed credit card attempt is (.*)$")
    public void verifyPaymentFailedAttempts(final int attempts) {
        AccertifyTransactions.getLastTransaction().verifyCCPaymentFailedAttempts(attempts);
    }

    @Then("^the failed billing information entries:$")
    public void verifyFailedCCAddress(final List<AddressEntry> addressEntries) {
        final AddressEntry address = addressEntries.get(0);
        AccertifyTransactions.getLastTransaction().verifyFailedCCBillingInfo(address);
    }

    @Then("^the failed gift card attempt is (.*)$")
    public void verifyGiftCardPaymentFailedAttempts(final int attempts) {
        AccertifyTransactions.getLastTransaction().verifyGCPaymentFailedAttempts(attempts);
    }

    @Then("^the failed credit card details are$")
    public void verifyFailedCardDetails(final List<CreditCardData> cardInfos) {
        AccertifyTransactions.getLastTransaction().verifyFailedCCDetails(cardInfos);
    }

    @Then("^the failed gift card details are$")
    public void verifyFailedGiftCardDetails(final List<CreditCardData> cardInfos) {
        AccertifyTransactions.getLastTransaction().verifyFailedGCDetails(cardInfos);
    }

    @Then("^the order type is (.*)$")
    public void verifyOrderType(final String orderType) {
        AccertifyTransactions.getLastTransaction().verifyOrderType(orderType);
    }

    @Then("^the accertify total amount is (.*)$")
    public void verifyTotalAmount(final String amount) {
        AccertifyTransactions.getLastTransaction().verifyTotalAmount(amount);
    }

    @Then("^the order item entries are$")
    public void verifyOrderItems(final List<AccertifyOrderItem> orderItems) {
        AccertifyTransactions.getLastTransaction().verifyOrderItem(orderItems);
    }

    @Then("^verify accertify failedPaymentInformation:$")
    public void verifyZipFailedItems(final List<FailZipDetails> items) {
        AccertifyTransactions.getLastTransaction().verifyFailedPaymentInformation(items);
    }

}
