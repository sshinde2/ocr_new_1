/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.BatchStatus;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.PreOrderInventory;
import au.com.target.tgtfluent.data.PreOrderItemInventory;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.data.Reference;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkusResponse;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.SkuFluentUpdateStatusModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.FluentClientUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.StepCategoryUtil;
import au.com.target.tgttest.automation.facade.bean.CategoryImportData;
import au.com.target.tgttest.automation.facade.bean.FluentAttributeData;
import au.com.target.tgttest.automation.facade.bean.FluentUpdateStatusData;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttinker.mock.fluent.FluentClientMock;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class FluentSteps {


    private PerformResult result;

    private IntegrationTargetProductCategoryDto integrationTargetProductCategoryDto;

    private IntegrationResponseDto response;

    private Event event;

    @Given("^the job to update store details in Fluent has been configured in the batch/chron scheduler$")
    public void setupLocationFeedJob() {
        // 
    }

    @Given("^the fluent client will respond with responseId '(.+)' for '(.+)'$")
    public void setupSuccessResponse(final String responseId, final String action) {
        final FluentResponse response = new FluentResponse();
        response.setId(responseId);
        switch (action) {
            case "createJob":
                ServiceLookup.getFluentClientMock().setCreateJobResponse(response);
                break;
            case "createBatch":
                ServiceLookup.getFluentClientMock().setCreateBatchResponse(response);
            default:
                break;
        }
    }

    @Given("^the fluent client will respond with errorCode '(.+)' and errorMessage '(.+)' for '(.+)'$")
    public void setupErrorResponse(final String errorCode, final String errorMessage, final String action) {
        final FluentResponse response = new FluentResponse();
        final Error error = new Error();
        error.setCode(errorCode);
        error.setMessage(errorMessage);
        response.setErrors(Arrays.asList(error));
        switch (action) {
            case "createJob":
                ServiceLookup.getFluentClientMock().setCreateJobResponse(response);
                break;
            case "createBatch":
                ServiceLookup.getFluentClientMock().setCreateBatchResponse(response);
                break;
            case "createFulfilmentOption":
                ServiceLookup.getFluentClientMock().setFulfilmentOptionMap(null);
                final FulfilmentOptionResponse fulfilmentOptionResponse = new FulfilmentOptionResponse();
                fulfilmentOptionResponse.setErrors(Arrays.asList(error));
                ServiceLookup.getFluentClientMock().setFulfilmentOptionResponse(fulfilmentOptionResponse);
                break;
            default:
                throw new IllegalStateException("invalid action=" + action);
        }
    }

    @Given("^there exists a jobResponse with jobId '(.+)', batchId '(.+)' and status '(.+)'$")
    public void setUpJobResponse(final String jobId, final String batchId, final FluentBatchStatus status) {
        final LocationFluentBatchResponseModel batchResponse = ServiceLookup.getModelService()
                .create(LocationFluentBatchResponseModel.class);
        batchResponse.setJobId(jobId);
        batchResponse.setBatchId(batchId);
        batchResponse.setStatus(status);
        ServiceLookup.getModelService().save(batchResponse);
    }

    @Given("^batchView api will respond with status '(.+)'$")
    public void setupBatchViewApi(final BatchStatus status) {
        final BatchResponse batchResponse = new BatchResponse();
        batchResponse.setStatus(status);
        ServiceLookup.getFluentClientMock().setBatchResponse(batchResponse);
    }

    @When("^the locations feed job is initiated$")
    public void initiateLocationFeedJob() {
        result = ServiceLookup.getLocationFeedJob().perform(null);
    }

    @When("^the location feed status check job is initiated$")
    public void initiateLocationFeedStatusCheckJob() {
        result = ServiceLookup.getLocationFeedStatusCheckJob().perform(null);
    }

    @Then("^fluent createJob api is invoked with$")
    public void verifyCreateJobApi(final List<Job> jobs) {
        final Job jobStub = ServiceLookup.getFluentClientMock().getJobStub();
        assertThat(jobStub.getName()).startsWith(jobs.get(0).getName());
        assertThat(jobStub.getRetailerId()).isEqualTo(jobs.get(0).getRetailerId());
    }

    @Then("^fluent createBatch api is invoked with$")
    public void verifyCreateBatchApi(final List<Batch> batches) {
        final Batch batchStub = ServiceLookup.getFluentClientMock().getBatchStub();
        assertThat(batchStub.getAction()).isEqualTo(batches.get(0).getAction());
        assertThat(batchStub.getEntityType()).isEqualTo(batches.get(0).getEntityType());
    }

    @Then("^the job result is '(.+)'$")
    public void verifyJobResult(final CronJobResult cronJobResult) {
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(cronJobResult);
    }

    @Then("^the job status is '(.+)'$")
    public void verifyJobStatus(final CronJobStatus cronJobStatus) {
        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(cronJobStatus);
    }

    @Then("the jobId '(.+)' & batchId '(.+)' are logged in '(.+)'$")
    public void verifyFluentBatchResponse(final String jobId, final String batchId, final String typecode) {
        final AbstractFluentBatchResponseModel fluentBatchResponse = ServiceLookup.getFluentBatchResponseService()
                .find(typecode, jobId, batchId);
        assertThat(fluentBatchResponse).isNotNull();
        ServiceLookup.getModelService().remove(fluentBatchResponse);
    }

    @Given("^category '(.+)' has fluentId '(.+)'$")
    public void updateCategoryFluentId(final String code, final String fluentId) {
        final CategoryModel category = ServiceLookup.getCategoryService().getCategoryForCode(code);
        if (category == null) {
            return;
        }
        ((TargetProductCategoryModel)category).setFluentId(fluentId);
        ServiceLookup.getModelService().save(category);
    }

    @Then("the jobResponse status is updated to '(.+)' for jobId '(.+)', batchId '(.+)'")
    public void verifyJobResponse(final FluentBatchStatus status, final String jobId, final String batchId) {
        final AbstractFluentBatchResponseModel batchResponse = ServiceLookup.getFluentBatchResponseService()
                .find(AbstractFluentBatchResponseModel._TYPECODE, jobId, batchId);
        assertThat(batchResponse).isNotNull();
        assertThat(batchResponse.getStatus()).isEqualTo(status);
        ServiceLookup.getModelService().remove(batchResponse);
    }

    @Given("^product '(.+)' has fluentId '(.+)'$")
    public void updateProductFluentId(final String code, final String fluentId) {
        final ProductModel product = ServiceLookup.getProductService().getProductForCode(code);
        if (product == null) {
            return;
        }
        ((TargetProductModel)product).setFluentId(fluentId);
        ServiceLookup.getModelService().save(product);
    }

    @Given("^productVariant '(.+)' has fluentId '(.+)'$")
    public void updateProductVariantFluentId(final String code, final String fluentId) {
        final ProductModel product = ServiceLookup.getProductService().getProductForCode(code);
        if (product == null) {
            return;
        }
        ((AbstractTargetVariantProductModel)product).setFluentId(fluentId);
        ServiceLookup.getModelService().save(product);
    }

    @Given("^fluent order '(.*)' status is '(.*)'$")
    public void setFluentOrderStatus(final String fluentOrderId, final String status) {

        final OrderResponse orderResponse = new OrderResponse();
        orderResponse.setId(fluentOrderId);
        orderResponse.setStatus(status);
        final FluentClientMock fluentClientMock = ServiceLookup.getFluentClientMock();
        fluentClientMock.setViewOrderResponse(orderResponse);
    }

    @Given("^fluent '(.*)' api returns fluent id '(.*)'$")
    public void upsertEntity(final String action, final String fluentId) {
        final FluentResponse fluentResponse = new FluentResponse();
        fluentResponse.setId(fluentId);
        final FluentClientMock fluentClientMock = ServiceLookup.getFluentClientMock();
        if ("createProduct".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateProductResponse(fluentResponse);
        }
        else if ("updateProduct".equalsIgnoreCase(action)) {
            fluentClientMock.setUpdateProductResponse(fluentResponse);
        }
        else if ("createCategory".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateCategoryResponse(fluentResponse);
        }
        else if ("updateCategory".equalsIgnoreCase(action)) {
            fluentClientMock.setUpdateCategoryResponse(fluentResponse);
        }
        else if ("createSku".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateSkuResponse(fluentResponse);
        }
        else if ("updateSku".equalsIgnoreCase(action)) {
            fluentClientMock.setUpdateSkuResponse(fluentResponse);
        }
        else if ("createOrder".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateOrderResponse(fluentResponse);
        }
        else if ("createPreOrder".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateOrderResponse(fluentResponse);
        }
        else if ("searchProduct".equalsIgnoreCase(action)) {
            final ProductsResponse productsResponse = new ProductsResponse();
            final Product product = new Product();
            product.setProductId(fluentId);
            productsResponse.setCount(1);
            productsResponse.setResults(Arrays.asList(product));
            fluentClientMock.setSearchProductResponse(productsResponse);
        }
        else if ("searchSku".equalsIgnoreCase(action)) {
            final SkusResponse skusResponse = new SkusResponse();
            final Sku sku = new Sku();
            sku.setSkuId(fluentId);
            skusResponse.setCount(1);
            skusResponse.setResults(Arrays.asList(sku));
            fluentClientMock.setSearchSkuResponse(skusResponse);
        }
        else if ("searchCategory".equalsIgnoreCase(action)) {
            final CategoryResponse categoryResponse = new CategoryResponse();
            final Category category = new Category();
            category.setId(fluentId);
            categoryResponse.setCount(1);
            categoryResponse.setResults(Arrays.asList(category));
            ServiceLookup.getFluentClientMock().setSearchCategoryResponse(categoryResponse);
        }

    }

    @Given("^the fluent fulfilment response is:$")
    public void upsertFulfilmentEntity(final List<Fulfilment> fulfilments) {
        final FulfilmentsResponse fulfilmentsResponse = new FulfilmentsResponse();

        final FluentClientMock fluentClientMock = ServiceLookup.getFluentClientMock();

        final Fulfilment fulfilment = fulfilments.get(0);
        final Address fromAddress = new Address();
        fromAddress.setLocationRef(fulfilment.getLocationRef());
        fulfilment.setFromAddress(fromAddress);

        final List<FulfilmentItem> items = new ArrayList<>();
        FulfilmentItem item;
        for (final String productCode : ServiceLookup.getTargetOrderEntryService()
                .getAllProductCodeFromOrderEntries(ServiceLookup.getTargetCheckoutFacade().getCart())) {
            item = new FulfilmentItem();
            item.setOrderItemRef(productCode);
            items.add(item);
        }
        fulfilment.setItems(items);
        fulfilmentsResponse.setFulfilments(fulfilments);
        fluentClientMock.setRetrieveFulfilmentsByOrderActive(true);
        fluentClientMock.setFulfilmentsResponse(fulfilmentsResponse);

    }

    @Given("^fluent resume order event is synced:$")
    public void fluentOrderResumeEvent(final List<EventResponse> eventResponses) {
        final EventResponse eventResponse = new EventResponse();
        final FluentClientMock fluentClientMock = ServiceLookup.getFluentClientMock();
        eventResponse.setEventId(eventResponses.get(0).getEventId());
        eventResponse.setEventStatus(eventResponses.get(0).getEventStatus());
        eventResponse.setErrorMessage(eventResponses.get(0).getErrorMessage());
        fluentClientMock.setEventSyncActive(true);
        fluentClientMock.setEventResponse(eventResponse);
    }

    @Given("^fluent '(.*)' api returns error code '(.*)'$")
    public void upsertEntityWithError(final String action, final String errorCode) {
        final FluentResponse fluentResponse = new FluentResponse();
        final Error error = new Error();
        error.setCode(errorCode);
        fluentResponse.setErrors(Arrays.asList(error));
        final FluentClientMock fluentClientMock = ServiceLookup.getFluentClientMock();
        if ("createProduct".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateProductResponse(fluentResponse);
        }
        else if ("updateProduct".equalsIgnoreCase(action)) {
            fluentClientMock.setUpdateProductResponse(fluentResponse);
        }
        else if ("createCategory".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateCategoryResponse(fluentResponse);
        }
        else if ("updateCategory".equalsIgnoreCase(action)) {
            fluentClientMock.setUpdateCategoryResponse(fluentResponse);
        }
        else if ("createSku".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateSkuResponse(fluentResponse);
        }
        else if ("updateSku".equalsIgnoreCase(action)) {
            fluentClientMock.setUpdateSkuResponse(fluentResponse);
        }
        else if ("createOrder".equalsIgnoreCase(action)) {
            fluentClientMock.setCreateOrderResponse(fluentResponse);
        }
        else {
            throw new IllegalArgumentException("Unidentified action:" + action);
        }
    }

    @Given("^fluent '(.*)' update status is:$")
    public void assertFluentUpdatestatus(final String entity,
            final List<FluentUpdateStatusData> expectedUpdateStatusList) {
        final FluentUpdateStatusData expectedUpdateStatus = expectedUpdateStatusList.get(0);
        Class<? extends AbstractFluentUpdateStatusModel> updateStatusClass;
        String updateStatusTypeCode;
        if ("product".equalsIgnoreCase(entity)) {
            updateStatusClass = ProductFluentUpdateStatusModel.class;
            updateStatusTypeCode = ProductFluentUpdateStatusModel._TYPECODE;
        }
        else if ("sku".equalsIgnoreCase(entity)) {
            updateStatusClass = SkuFluentUpdateStatusModel.class;
            updateStatusTypeCode = SkuFluentUpdateStatusModel._TYPECODE;
        }
        else if ("category".equalsIgnoreCase(entity)) {
            updateStatusClass = CategoryFluentUpdateStatusModel.class;
            updateStatusTypeCode = CategoryFluentUpdateStatusModel._TYPECODE;
        }
        else {
            throw new IllegalArgumentException("Unidentified entity:" + entity);
        }
        assertUpdateStatus(expectedUpdateStatus, updateStatusClass, updateStatusTypeCode);
    }

    /**
     * @param expectedUpdateStatus
     * @param updateStatusClass
     * @param updateStatusTypeCode
     */
    private void assertUpdateStatus(final FluentUpdateStatusData expectedUpdateStatus,
            final Class<? extends AbstractFluentUpdateStatusModel> updateStatusClass,
            final String updateStatusTypeCode) {
        final AbstractFluentUpdateStatusModel updateStatus = ServiceLookup.getFluentUpdateStatusService()
                .getFluentUpdateStatus(updateStatusClass, updateStatusTypeCode, expectedUpdateStatus.getCode());
        if (StringUtils.isNotEmpty(expectedUpdateStatus.getFluentId())) {
            if ("null".equals(expectedUpdateStatus.getFluentId())) {
                assertThat(updateStatus.getFluentId()).isNull();
            }
            else {
                assertThat(updateStatus.getFluentId()).isEqualTo(expectedUpdateStatus.getFluentId());
            }
        }
        assertThat(updateStatus.isLastRunSuccessful()).isEqualTo(expectedUpdateStatus.isLastRunSuccessful());
        if (StringUtils.isNotEmpty(expectedUpdateStatus.getErrorCode())) {
            assertThat(updateStatus.getFailureCauses()).isNotEmpty();
            assertThat(updateStatus.getFailureCauses().get(updateStatus.getFailureCauses().size() - 1).getCode())
                    .isEqualTo(expectedUpdateStatus.getErrorCode());
        }
    }

    @Given("^fluent product api gets called with basic data:$")
    public void assertProductData(final List<Product> products) {
        final Product product = products.get(0);
        final Product productStub = ServiceLookup.getFluentClientMock().getProductStub();
        assertThat(productStub.getProductRef()).isEqualTo(product.getProductRef());
        assertThat(productStub.getName()).isEqualTo(product.getName());
        assertThat(productStub.getStatus()).isEqualTo(product.getStatus());
    }

    @Given("^fluent product api gets called with category id:$")
    public void assertProductDataWitCategoryId(final List<String> categoryIdList) {
        final Product productStub = ServiceLookup.getFluentClientMock().getProductStub();
        assertThat(productStub.getCategories()).containsExactly(categoryIdList.toArray());
    }

    @Given("^fluent product api gets called with customized attribute data:$")
    public void assertProductAttributesData(final List<FluentAttributeData> fluentAttributeData) {
        final List<Attribute> attributes = FluentClientUtil.createAttributes(fluentAttributeData);
        verifyAttributes(attributes, ServiceLookup.getFluentClientMock().getProductStub().getAttributes());
    }

    @Then("^fluent sku api gets called with:$")
    public void verifySkuData(final List<Sku> skus) {
        final Sku sku = skus.get(0);
        final Sku skuStub = ServiceLookup.getFluentClientMock().getSkuStub();
        assertThat(skuStub.getSkuRef()).isEqualTo(sku.getSkuRef());
        assertThat(skuStub.getName()).isEqualTo(sku.getName());
        assertThat(skuStub.getStatus()).isEqualTo(sku.getStatus());
    }

    @Then("^fluent sku api gets called with references:$")
    public void verifySkuReferences(final List<Reference> references) {
        final Sku skuStub = ServiceLookup.getFluentClientMock().getSkuStub();

        final List<String> types = new ArrayList<>();
        final List<String> values = new ArrayList<>();

        for (final Reference reference : references) {
            types.add(reference.getType());
            values.add(reference.getValue());
        }

        assertThat(skuStub.getReferences()).onProperty("type").containsExactly(types.toArray());
        assertThat(skuStub.getReferences()).onProperty("value").containsExactly(values.toArray());
    }

    @Then("^fluent sku api gets called with attributes:$")
    public void verifySkuAttributes(final List<FluentAttributeData> fluentAttributeData) {
        final List<Attribute> attributes = FluentClientUtil.createAttributes(fluentAttributeData);
        verifyAttributes(attributes, ServiceLookup.getFluentClientMock().getSkuStub().getAttributes());
    }

    public static void verifyAttributes(final List<Attribute> expectedAttributes,
            final List<Attribute> actualAttributes) {
        assertThat(actualAttributes).hasSize(expectedAttributes.size());
        for (int i = 0; i < expectedAttributes.size(); i++) {
            assertThat(actualAttributes.get(i).getName()).isEqualTo(expectedAttributes.get(i).getName());
            assertThat(actualAttributes.get(i).getType()).as(actualAttributes.get(i).getName())
                    .isEqualTo(expectedAttributes.get(i).getType());
            assertThat(actualAttributes.get(i).getValue()).as(actualAttributes.get(i).getName())
                    .isEqualTo(expectedAttributes.get(i).getValue());
        }
    }

    @Given("^a category:$")
    public void categoryFromStep(final List<CategoryImportData> category) {
        integrationTargetProductCategoryDto = StepCategoryUtil.getIntegrationTargetProductCategoryDto(category.get(0));
    }

    @When("^run STEP category import process$")
    public void stepImportProcessRuns() {
        SessionUtil.userLogin("esb");
        response = ServiceLookup.getTargetCategoryImportIntegrationFacade()
                .persistTargetCategory(integrationTargetProductCategoryDto);
    }

    @Then("^category import response is successful$")
    public void verifyCategoryImportResponseSuccess() {

        assertThat(response).as("category import response").isNotNull();
        assertThat(response.isSuccessStatus()).as("category import response status").isTrue();
        assertThat(response.getMessages()).as("category import error messages").isEmpty();
    }

    @Given("^fluent category api gets called with data:$")
    public void assertCategoryData(final List<Category> categories) {
        final Category category = categories.get(0);
        final Category categoryStub = ServiceLookup.getFluentClientMock().getCategoryStub();
        assertThat(categoryStub.getCategoryRef()).isEqualTo(category.getCategoryRef());
        assertThat(categoryStub.getName()).isEqualTo(category.getName());
        assertThat(categoryStub.getParentCategoryId()).isEqualTo(category.getParentCategoryId());
    }

    @Then("^category has:")
    public void verifyCategoryFluentId(final List<CategoryImportData> categoryDataList) {
        final CategoryImportData categoryData = categoryDataList.get(0);
        final TargetProductCategoryModel category = (TargetProductCategoryModel)ServiceLookup.getCategoryService()
                .getCategoryForCode(integrationTargetProductCategoryDto.getCode());
        if (StringUtils.isNotEmpty(categoryData.getFluentId())) {
            if ("null".equals(categoryData.getFluentId())) {
                assertThat(category.getFluentId()).isNull();
            }
            else {
                assertThat(category.getFluentId()).isEqualTo(categoryData.getFluentId());
            }
        }
    }

    @Given("^fluent event response is:$")
    public void setupEventSyncResponse(final List<EventResponse> eventResponses) {
        ServiceLookup.getFluentClientMock().setEventResponse(eventResponses.get(0));
    }

    @Then("event sent to fluent is:$")
    public void verifyFluentEvent(final List<Event> events) {
        final Event event = events.get(0);
        final Event eventStub = ServiceLookup.getFluentClientMock().getEventStub();
        assertThat(eventStub).isNotNull();
        assertThat(eventStub.getName()).isEqualTo(event.getName());
        assertThat(eventStub.getEntityType()).isEqualTo(event.getEntityType());
        assertThat(eventStub.getEntityId()).isEqualTo(Order.getInstance().getOrderModel().getFluentId());
    }

    @Then("event is not sent to fluent$")
    public void verifyFluentEvent() {
        final Event eventStub = ServiceLookup.getFluentClientMock().getEventStub();
        assertThat(eventStub).isNull();
    }

    @Then("preorder inventory data is:$")
    public void verifyPreOrderInventoryData(final List<PreOrderItemInventory> items) {
        assertThat(event).isNotNull();
        assertThat(event.getAttributes() instanceof PreOrderInventory).isTrue();
        final PreOrderInventory inventory = (PreOrderInventory)event.getAttributes();
        assertThat(inventory.getItems()).hasSize(1);
        final PreOrderItemInventory actual = inventory.getItems().get(0);
        final PreOrderItemInventory expected = items.get(0);
        assertThat(actual.getQuantity()).isEqualTo(expected.getQuantity());
        assertThat(actual.getSkuRef()).isEqualTo(expected.getSkuRef());
    }

    @Then("preorder inventory event sent to fluent is:$")
    public void verifyPreOrderInventoryEvent(final List<Event> events) {
        final Event expectedEvent = events.get(0);
        final Event eventStub = ServiceLookup.getFluentClientMock().getEventStub();
        event = eventStub;
        assertThat(eventStub).isNotNull();
        assertThat(eventStub.getName()).isEqualTo(expectedEvent.getName());
        assertThat(eventStub.getEntityType()).isEqualTo(expectedEvent.getEntityType());
        assertThat(eventStub.getEntitySubtype()).isEqualTo(expectedEvent.getEntitySubtype());
        assertThat(eventStub.getRootEntityType()).isEqualTo(expectedEvent.getRootEntityType());
        assertThat(eventStub.getRootEntityRef()).isEqualTo(TgtFluentConstants.EntityRef.DEFAULT);
        assertThat(eventStub.getEntityRef()).isEqualTo(TgtFluentConstants.EntityRef.DEFAULT);
    }

}
