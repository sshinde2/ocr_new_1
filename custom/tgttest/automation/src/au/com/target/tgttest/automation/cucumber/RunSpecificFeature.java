/**
 * 
 */
package au.com.target.tgttest.automation.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@Cucumber.Options(features = {
        "classpath:au/com/target/tgttest/automation/cucumber/productimport/ProductImportUpsertFluentProduct.feature" }, format = {
        "pretty",
        "html:cuc-report/cucumber-html-report", "json:cuc-report/cucumber.json",
        "junit:cuc-report/cucumber-junit.xml" }, tags = { "~@notAutomated", "~@toFix" })
public class RunSpecificFeature {
    // Cucumber test runner class - specified feature
}
