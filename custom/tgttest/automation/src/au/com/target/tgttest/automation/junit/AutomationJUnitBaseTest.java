/**
 * 
 */
package au.com.target.tgttest.automation.junit;

import org.junit.After;
import org.junit.Before;

import au.com.target.tgttest.automation.AutomationPlatformRunner;
import au.com.target.tgttest.automation.AutomationPlatformRunner.SessionUser;
import au.com.target.tgttest.automation.AutomationTransactionRunner;


/**
 * Base test for JUnit based automation tests on Master tenant
 * 
 */
public abstract class AutomationJUnitBaseTest {

    private final AutomationPlatformRunner apr = new AutomationPlatformRunner();
    private final AutomationTransactionRunner atr = new AutomationTransactionRunner();

    @Before
    public void setup() {

        try {
            apr.testStarted(SessionUser.ANONYMOUS);

            // TODO if we need to, use annotation to determine if transactional
            // TODO pass in the test name
            atr.transactionalTestStarted("test", false);

            apr.logInfo("Finished before");
        }
        catch (final Exception e) {
            apr.logError("Exception in before", e);
        }

        setupData();

    }


    @After
    public void teardown() {
        apr.logInfo("After");

        try {
            apr.testFinished();
        }
        catch (final Exception e) {
            apr.logError("Exception in after running apr.testFinished", e);
        }
        finally {
            atr.transactionalTestFinished("test", false);
        }

        apr.logInfo("Finished After");
    }


    /**
     * Use in tests to set up data
     */
    public abstract void setupData();

}
