/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.StockLevelUtil;
import au.com.target.tgttest.automation.facade.bean.StockLevelData;
import au.com.target.tgttest.automation.facade.bean.StockOnHand;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateResponseDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateProductDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateResponseDto;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author Nandini
 *
 */
public class SohUpdateSteps {

    private IntegrationStockUpdateDto stockUpdateDto;
    private IntegrationStockUpdateResponseDto stockUpdateResponseDto;
    private TargetStockUpdateDto targetStockUpdateDto;
    private TargetStockUpdateResponseDto targetStockUpdateResponseDto;

    @Given("^hybris has products set up:$")
    public void givenStockForProduct(final List<StockLevelData> stockLevels) {

        for (final StockLevelData stockData : stockLevels) {
            StockLevelUtil.updateProductStock(stockData.getProduct(),
                    Integer.valueOf(stockData.getAvailable()).intValue(),
                    stockData.getWarehouseCode());
        }

    }

    @Given("^the max batch size for stock update is (\\d+)$")
    public void setMaxBatchSize(final int maxBatchSize) {
        StockLevelUtil.setMaxBatchSizeForStockUpdate(maxBatchSize);
    }

    @Given("^a SOH file is received for a product$")
    public void givenSohFileReceivedForProduct(final List<StockOnHand> soh) {
        if (stockUpdateDto == null) {
            stockUpdateDto = new IntegrationStockUpdateDto();
            for (final StockOnHand stockOnHand : soh) {
                stockUpdateDto.setItemCode(stockOnHand.getProductCode());
                stockUpdateDto.setItemPrimaryBarcode(stockOnHand.getEan());
                stockUpdateDto.setWarehouse(stockOnHand.getWarehouse());
                final Integer totalQuantity = stockOnHand.getTotalQuantityAvailable() == null ? Integer.valueOf(0)
                        : Integer.valueOf(stockOnHand.getTotalQuantityAvailable());
                stockUpdateDto.setTotalQuantityAvailable(totalQuantity);
                final Integer adjustmentQuantity = stockOnHand.getAdjustmentQuantity() == null ? Integer.valueOf(0)
                        : Integer.valueOf(stockOnHand.getAdjustmentQuantity());
                stockUpdateDto.setAdjustmentQuantity(adjustmentQuantity);
            }
        }
    }

    @Given("^a SOH file is received for a list of products$")
    public void givenSohFileReceivedForProducts(final List<StockOnHand> soh) {
        if (targetStockUpdateDto == null) {
            targetStockUpdateDto = new TargetStockUpdateDto();
            final List<TargetStockUpdateProductDto> items = new ArrayList<>();
            for (final StockOnHand stockOnHand : soh) {
                targetStockUpdateDto.setWarehouse(stockOnHand.getWarehouse());
                final TargetStockUpdateProductDto productDto = new TargetStockUpdateProductDto();
                productDto.setItemCode(stockOnHand.getProductCode());
                productDto.setItemPrimaryBarcode(stockOnHand.getEan());
                final Integer totalQuantity = stockOnHand.getTotalQuantityAvailable() == null ? Integer.valueOf(0)
                        : Integer.valueOf(stockOnHand.getTotalQuantityAvailable());
                productDto.setTotalQuantityAvailable(totalQuantity);
                final Integer adjustmentQuantity = stockOnHand.getAdjustmentQuantity() == null ? Integer.valueOf(0)
                        : Integer.valueOf(stockOnHand.getAdjustmentQuantity());
                productDto.setAdjustmentQuantity(adjustmentQuantity);
                items.add(productDto);
            }
            if (CollectionUtils.isNotEmpty(items)) {
                targetStockUpdateDto.setItems(items);
            }
        }
    }

    @When("^the SOH is assigned to an individual product$")
    public void sohPerformedOnIndividualProduct() {
        stockUpdateResponseDto = ServiceLookup.getStockUpdateIntegrationFacade().updateStock(stockUpdateDto);
    }

    @When("^the stock adjustment is assigned to an individual product$")
    public void stockAdjustmentOnIndividualProduct() {
        stockUpdateResponseDto = ServiceLookup.getStockUpdateIntegrationFacade().adjustStock(stockUpdateDto);
    }

    @When("^the SOH is assigned to a list of product$")
    public void sohPerformedOnListOfProduct() {
        targetStockUpdateResponseDto = ServiceLookup.getStockUpdateIntegrationFacade().updateStockBatch(
                targetStockUpdateDto);
    }

    @When("^the stock adjustment is assigned to a list of product$")
    public void stockAdjustmentOnListOfProduct() {
        targetStockUpdateResponseDto = ServiceLookup.getStockUpdateIntegrationFacade().adjustStockBatch(
                targetStockUpdateDto);
    }

    @Then("^stock is updated in Hybris against the warehouse$")
    public void stockOnHandForProductInWarehouse(final List<StockLevelData> expectedStockLevel) {

        for (final StockLevelData stockData : expectedStockLevel) {
            if (!stockData.getWarehouseCode().isEmpty()) {
                final StockLevelModel stockLevelModel = StockLevelUtil.getStockForWarehouse(stockData.getProduct(),
                        stockData.getWarehouseCode());

                assertThat(stockUpdateResponseDto).isNotNull();
                if (stockUpdateDto.getTotalQuantityAvailable().intValue() > 0) {
                    Assert.assertEquals(stockUpdateResponseDto.getTotalQuantityAvailable(),
                            Integer.valueOf(stockLevelModel.getAvailable()));
                }
                else {
                    Assert.assertEquals(Integer.valueOf(0),
                            Integer.valueOf(stockLevelModel.getAvailable()));
                }

                if (StringUtils.isNotEmpty(stockData.getProduct())) {
                    final ProductModel productModel = ProductUtil.getProductModel(stockData.getProduct());
                    final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)productModel;
                    final Collection<StockLevelModel> stockLevel = ServiceLookup.getTargetStockService()
                            .getAllStockLevels(variantProduct);
                    Assert.assertEquals(1, stockLevel.size());
                }
            }

        }
    }

    @Then("^stock is adjusted in Hybris against the warehouse$")
    public void stockAdjustementForProductInWarehouse(final List<StockLevelData> expectedStockLevel) {

        for (final StockLevelData stockData : expectedStockLevel) {

            if (StringUtils.isNotEmpty(stockData.getProduct())) {
                final ProductModel productModel = ProductUtil.getProductModel(stockData.getProduct());
                StockLevelModel stockLevel = null;
                final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)productModel;
                final Collection<StockLevelModel> stockLevels = ServiceLookup.getTargetStockService()
                        .getAllStockLevels(variantProduct);
                Assert.assertEquals(1, stockLevels.size());
                stockLevel = stockLevels.iterator().next();

                Assert.assertNotNull(stockLevel);
                Assert.assertEquals(stockData.getWarehouseCode(), stockLevel.getWarehouse().getCode());
                Assert.assertEquals(Integer.parseInt(stockData.getAvailable()), stockLevel.getAvailable());
            }
            assertThat(stockUpdateResponseDto).isNotNull();
            Assert.assertEquals(stockUpdateDto.getAdjustmentQuantity(), stockUpdateResponseDto.getAdjustmentQuantity());
            Assert.assertEquals(Integer.valueOf(0), stockUpdateResponseDto.getTotalQuantityAvailable());
            Assert.assertEquals(stockData.getResponseStatus(), stockUpdateResponseDto.getStatusCode());

        }
    }

    @Then("^the stock of the products are updated in Hybris against the warehouse$")
    public void stockUpdateForListOfProductInWarehouse(final List<StockLevelData> expectedStockLevel) {

        for (final StockLevelData stockData : expectedStockLevel) {

            if (StringUtils.isNotEmpty(stockData.getProduct())) {
                final ProductModel productModel = ProductUtil.getProductModel(stockData.getProduct());
                StockLevelModel stockLevel = null;
                final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)productModel;
                final Collection<StockLevelModel> stockLevels = ServiceLookup.getTargetStockService()
                        .getAllStockLevels(variantProduct);
                Assert.assertEquals(1, stockLevels.size());
                stockLevel = stockLevels.iterator().next();
                Assert.assertNotNull(stockLevel);
                Assert.assertEquals(stockData.getWarehouseCode(), stockLevel.getWarehouse().getCode());
                Assert.assertEquals(Integer.parseInt(stockData.getAvailable()), stockLevel.getAvailable());
            }
        }
    }

    @Then("^the SOH response status is '(.*)'$")
    public void checkStockUpdateResponseStatus(final String status) {
        assertThat(targetStockUpdateResponseDto).isNotNull();
        Assert.assertEquals(status, targetStockUpdateResponseDto.getStatusCode());
    }
}
