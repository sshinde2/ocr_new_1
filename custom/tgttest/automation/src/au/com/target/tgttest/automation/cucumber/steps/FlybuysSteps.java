/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;



import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.fest.assertions.Assertions;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemTierData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.order.data.FlybuysDiscountData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.voucher.data.TargetFlybuysLoginData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.bean.ExpectedFlybuysDiscountData;
import au.com.target.tgttest.automation.facade.bean.ExpectedFlybuysRedeemTierData;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.PendingException;


/**
 * @author cwijesu1
 * 
 */
public class FlybuysSteps {

    private static final String FLYBUYS_NUMBER = "6008943218616910";
    private static final Date DOB = new Date();
    private static final String POSTCODE = "3000";
    private FlybuysResponseType authResponse;
    private boolean haveAlreadySetResponse = false;
    private Response flybuysRedeemResponse;
    private Response flybuysRedeemDiscountResponse;

    /**
     * @param status
     */
    @Given("^the user entered flybuys details are '(.*)'$")
    public void setMockResponse(final String status) {
        if (!haveAlreadySetResponse) {
            if (status.equalsIgnoreCase("valid")) {
                ServiceLookup.getFlybuysclientMockService().setMockResponse("SUCCESS", "20000", "SUCCESS", "SUCCESS");
            }
            else {
                ServiceLookup.getFlybuysclientMockService().setMockResponse("INVALID", "0", "INVALID", "INVALID");
            }
        }
    }

    @Given("^customer has valid flybuys number presented$")
    public void setFlybuysIntoCart() {
        CartUtil.setFlybuysNumber(FLYBUYS_NUMBER);
    }

    /**
     * 
     */
    @Given("^that flybuys is offline$")
    public void setFlybuysOffline() {
        haveAlreadySetResponse = true;
        ServiceLookup.getFlybuysclientMockService().setMockResponse("UNAVAILABLE", "0", "INVALID", "INVALID");
    }

    /**
     * 
     */
    @Given("^that Webmethods is offline$")
    public void setWebMethodsOffline() {
        haveAlreadySetResponse = true;
        ServiceLookup.getFlybuysclientMockService().setMockResponse("UNAVAILABLE", "0", "INVALID", "INVALID");
    }

    /**
     * 
     */
    @When("^the customer selects Redeem$")
    public void doFlybuysAuthentication() {
        authResponse = ServiceLookup.getFlybuysDiscountFacade().getFlybuysRedemptionData(FLYBUYS_NUMBER, DOB, POSTCODE)
                .getResponse();
    }

    @When("^the customer logins flybuys in spc$")
    public void loginFlybuys() {
        final TargetFlybuysLoginData flybuysLoginData = new TargetFlybuysLoginData();
        flybuysLoginData.setCardNumber(FLYBUYS_NUMBER);
        flybuysLoginData.setDateOfBirth(DOB);
        flybuysLoginData.setPostCode(POSTCODE);
        flybuysRedeemResponse = ServiceLookup.getCheckoutResponseFacade().loginFlybuys(flybuysLoginData);
    }

    /**
     * @param msg
     */
    @Then("^a message is displayed to the customer '(.*)'$")
    public void validateMessage(final String msg) {
        assertThat(authResponse.toString()).as("Authentication Response message").isEqualTo(msg);
    }

    @Then("^a flybuys login error message is displayed to the customer '(.*)'$")
    public void validateErrorMessage(final String msg) {
        assertThat(flybuysRedeemResponse.getData().getError().getCode()).as("Authentication Response message")
                .isEqualTo(msg);
    }

    @Then("^flybuys login successfully$")
    public void flybuysLoginSuccessfully() {
        assertThat(flybuysRedeemResponse.getData().getError()).isNull();
    }

    /**
     * @param availablePoints
     */
    @Given("^Customer has available points '(.*)'$")
    public void setCustomerAvailablePoints(final String availablePoints) {
        ServiceLookup.getFlybuysclientMockService().setMockResponse("SUCCESS", availablePoints, "SUCCESS", "SUCCESS");
    }

    /**
     * @param expectedTiers
     */
    @When("^the customer will be presented with the redemption options$")
    public void validateResponseTiers(final List<ExpectedFlybuysRedeemTierData> expectedTiers) {
        final List<FlybuysRedeemTierData> responseTiers = CheckoutUtil.getRedeemTiers();

        assertThat(responseTiers.size()).as("Number of retured tiers").isEqualTo(expectedTiers.size());

        for (int i = 0; i < expectedTiers.size(); i++) {
            assertThat(responseTiers.get(i).getDollars().getValue().doubleValue()).as("Tier dollar amount").isEqualTo(
                    expectedTiers.get(i).getDollarAmt().doubleValue());
            assertThat(responseTiers.get(i).getPoints().intValue()).as("Number of retured points").isEqualTo(
                    expectedTiers.get(i).getPoints().intValue());
            assertThat(responseTiers.get(i).getRedeemCode()).as("Redeem code returned").isEqualTo(
                    expectedTiers.get(i).getRedeemCode());
        }

    }

    @When("^the customer will be presented with the redemption options in spc$")
    public void validateResponseTiersInSPC(final List<ExpectedFlybuysRedeemTierData> expectedTiers) {
        final List<FlybuysRedeemTierData> responseTiers = ((CartDetailResponseData)flybuysRedeemResponse.getData())
                .getFlybuysRedemptionData().getRedeemTiers();
        assertThat(responseTiers.size()).as("Number of retured tiers").isEqualTo(expectedTiers.size());

        for (int i = 0; i < expectedTiers.size(); i++) {
            assertThat(responseTiers.get(i).getDollars().getValue().doubleValue()).as("Tier dollar amount").isEqualTo(
                    expectedTiers.get(i).getDollarAmt().doubleValue());
            assertThat(responseTiers.get(i).getPoints().intValue()).as("Number of retured points").isEqualTo(
                    expectedTiers.get(i).getPoints().intValue());
            assertThat(responseTiers.get(i).getRedeemCode()).as("Redeem code returned").isEqualTo(
                    expectedTiers.get(i).getRedeemCode());
        }

    }

    /**
     * @param points
     */
    @When("^the customer selects option for (\\d+) points$")
    public void selectRedeemOptions(final int points) {

        ServiceLookup.getFlybuysDiscountFacade().applyFlybuysDiscountToCheckoutCart("DUMMYREDEEMCODE1");
    }

    /**
     * @param arg1
     */
    @When("^summary shows flybuys discount:$")
    public void validateOrderWithFlybuys(final DataTable arg1) {
        throw new PendingException();
    }

    @Given("^that the customer enters stolen flybuys account details$")
    public void setFlybuysOtherError() {
        haveAlreadySetResponse = true;
        ServiceLookup.getFlybuysclientMockService().setMockResponse("FLYBUYS_OTHER_ERROR", "0", "INVALID", "INVALID");
    }

    @Given("^flybuys config max redeemable amount is (\\d+)$")
    public void setFlybuysConfigMaxRedeemableAmount(final double amount) {

        final FlybuysRedeemConfigModel config = ServiceLookup.getFlybuysRedeemConfigService().getFlybuysRedeemConfig();
        config.setMaxRedeemable(Double.valueOf(amount));
        ServiceLookup.getModelService().save(config);
    }

    @When("^the customer redeems flybuys points with redeem code '(.*)'$")
    public void redeemFlybuys(final String code) {
        flybuysRedeemDiscountResponse = ServiceLookup.getCheckoutResponseFacade().redeemFlybuys(code);
    }

    @When("^the customer will be presented with the redemption summary in spc$")
    public void presentRedemptionSummary(final List<ExpectedFlybuysDiscountData> expectedDiscounts) {
        final FlybuysRedemptionData redemptionData = ((CartDetailResponseData)flybuysRedeemDiscountResponse.getData())
                .getFlybuysRedemptionData();
        final FlybuysDiscountData flybuysDiscountData = ((CartDetailResponseData)flybuysRedeemDiscountResponse
                .getData()).getFlybuysDiscountData();
        assertThat(expectedDiscounts.get(0).getFlybuysCardNumber()).isEqualTo(FLYBUYS_NUMBER);
        if (expectedDiscounts.get(0).getValue().compareTo(BigDecimal.ZERO) == 0) {
            assertThat(flybuysDiscountData).isNull();
        }
        else {
            assertThat(expectedDiscounts.get(0).getValue().doubleValue()).isEqualTo(
                    flybuysDiscountData.getValue().getValue().doubleValue());
            assertThat(expectedDiscounts.get(0).getPointsConsumed()).isEqualTo(flybuysDiscountData.getPointsConsumed());
        }

        assertThat(expectedDiscounts.get(0).getAvailablePoints()).isEqualTo(redemptionData.getAvailablePoints());
    }

    @When("^the customer will be presented with the redemption error message in spc$")
    public void presentRedemptionError() {
        final BaseResponseData responseData = flybuysRedeemDiscountResponse.getData();
        responseData.getError().getCode().equals("ERR_FLYBUYS_REDEMPTION");
    }

    @When("^the customer selects change button to fetch the flybuys redemption options$")
    public void selectFlybuysChangeButton() {
        flybuysRedeemResponse = ServiceLookup.getCheckoutResponseFacade().showFlybuysRedemptionOption();
    }

    @When("^the change flybuys redemption response constains flybuys canRedeemPoints '(.*)'$")
    public void verifyCanRedeem(final String canRedeemFlybuysPoints) {
        final CartDetailResponseData cartDetails = (CartDetailResponseData)flybuysRedeemResponse.getData();
        Assertions.assertThat(cartDetails.getFlybuysData().isCanRedeemPoints()).isEqualTo(
                canRedeemFlybuysPoints.equals("true"));
    }

    @Then("^customer is not allowed to redeem flybuys$")
    public void verifyErrorWhileRedeemingFlybuys() {
        assertThat(flybuysRedeemDiscountResponse.getData().getError().getCode())
                .isEqualTo("ERR_FLYBUYS_REDEMPTION_NOT_AVAILABLE_FOR_PREORDER");
        assertThat(flybuysRedeemDiscountResponse.getData().getError().getMessage())
                .isEqualTo("Flybuys redemption not available for pre-orders.");
    }
}
