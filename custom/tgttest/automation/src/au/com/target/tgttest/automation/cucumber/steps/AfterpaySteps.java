/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;

import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.OrderUtil;
import au.com.target.tgttest.automation.facade.bean.AfterpayConfigurationData;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Given;


/**
 * @author bhuang3
 *
 */
public class AfterpaySteps {

    private PerformResult performResult;

    @Given("^afterpay get configuration api returns the configuration:$")
    public void setAfterConfigurationMock(final List<AfterpayConfigurationData> configurationDataList) {
        ServiceLookup.getTargetAfterpayClientMock()
                .setGetConfigurationResponse(createConfigurationResponse(configurationDataList.get(0)));
    }

    @Given("^no afterpay configuration is in system$")
    public void removeAfterpayconfiguration() {
        final List<AfterpayConfigModel> configs = ServiceLookup.getAfterpayConfigDaoImpl().find();
        ServiceLookup.getModelService().removeAll(configs);
    }

    @Given("^the afterpay configuration cronjob is triggered and is '(successful|failed)'$")
    public void triggerAfterpayConfigCronjob(final String status) {
        final CronJobModel cronJob = ServiceLookup.getModelService().create(CronJobModel.class);
        performResult = ServiceLookup.getTargetAfterpayConfigurationJob().perform(cronJob);
        if (StringUtils.equalsIgnoreCase("successful", status)) {
            assertThat(performResult.getResult()).isEqualTo(CronJobResult.SUCCESS);
            assertThat(performResult.getStatus()).isEqualTo(CronJobStatus.FINISHED);
        }
        else {
            assertThat(performResult.getResult()).isEqualTo(CronJobResult.ERROR);
            assertThat(performResult.getStatus()).isEqualTo(CronJobStatus.ABORTED);
        }
    }

    @Given("^afterpay configuration is:$")
    public void checkAfterpayConfiguration(final List<AfterpayConfigurationData> configurationDataList) {
        final AfterpayConfigurationData expectedConfig = configurationDataList.get(0);
        final AfterpayConfigModel configModel = ServiceLookup.getAfterpayConfigServiceImpl().getAfterpayConfig();
        assertThat(configModel.getPaymentType()).isEqualTo(expectedConfig.getType());
        assertThat(configModel.getDescription()).isEqualTo(expectedConfig.getDescription());
        assertThat(configModel.getMaximumAmount().setScale(2, BigDecimal.ROUND_HALF_UP)).isEqualTo(
                convertStringToBigDecimal(expectedConfig.getMax()));
        assertThat(configModel.getMinimumAmount().setScale(2, BigDecimal.ROUND_HALF_UP)).isEqualTo(
                convertStringToBigDecimal(expectedConfig.getMin()));
    }

    @Given("^an afterpay configuration is:$")
    public void setDefaultConfigurationModel(final List<AfterpayConfigurationData> configurationDataList) {
        this.removeAfterpayconfiguration();
        final AfterpayConfigurationData config = configurationDataList.get(0);
        final AfterpayConfigModel model = ServiceLookup.getModelService().create(AfterpayConfigModel.class);
        model.setPaymentType(config.getType());
        model.setDescription(config.getDescription());
        model.setMaximumAmount(convertStringToBigDecimal(config.getMax()));
        model.setMinimumAmount(convertStringToBigDecimal(config.getMin()));
        ServiceLookup.getModelService().save(model);
        ServiceLookup.getModelService().refresh(model);
    }

    @Given("^Afterpay configuration API is unavailable$")
    public void setAfterpayConfigUnavailable() {
        ServiceLookup.getTargetAfterpayClientMock().setGetConfigurationException(true);
        ServiceLookup.getTargetAfterpayClientMock().setGetConfigurationActive(true);
    }

    @Given("^Afterpay ping API is unavailable$")
    public void setAfterpayPingUnavailable() {
        ServiceLookup.getTargetAfterpayClientMock().setThrowExpection(true);
        ServiceLookup.getTargetAfterpayClientMock().setPingActive(true);
    }

    @Given("^Afterpay createOrder API is unavailable$")
    public void setAfterpayCreateOrderUnavailable() {
        ServiceLookup.getTargetAfterpayClientMock().setCreateOrderException(true);
    }

    @Given("^afterpay check availability is successful$")
    public void setAfterpayAvailability() {
        // NO-OP
    }

    @Given("^Afterpay createOrder api returns:$")
    public void setCreateOrderResponse(final List<CreateOrderResponse> createOrderResponses) {
        ServiceLookup.getTargetAfterpayClientMock().setCreateOrderResponse(createOrderResponses.get(0));
    }

    @Given("^afterpay rejects the payment$")
    public void rejectCapturePayment() {
        CheckoutUtil.rejectAfterpayCapturePayment();
    }

    @Given("^afterpay capture payment api is down$")
    public void capturePaymentApiDown() {
        ServiceLookup.getTargetAfterpayClientMock().setCapturePaymentException(true);
    }

    @Given("^afterpay get payment api is down$")
    public void getPaymentApiDown() {
        ServiceLookup.getTargetAfterpayClientMock().setGetPaymentException(true);
    }

    @Given("^afterpay get payment succeeds$")
    public void getPaymentSucceeds() {
        final GetPaymentResponse response = new GetPaymentResponse();
        response.setStatus("APPROVED");
        ServiceLookup.getTargetAfterpayClientMock().setGetPaymentResponse(response);
    }

    @Given("^afterpay capture payment api succeeds on retry$")
    public void capturePaymentSucceedsOnRetry() {
        ServiceLookup.getTargetAfterpayClientMock().setCapturePaymentActiveOnRetry(true);
    }

    @Given("^afterpay get order api is down$")
    public void getOrderApiDown() {
        ServiceLookup.getTargetAfterpayClientMock().setGetOrderExpection(true);
    }

    @Given("^the afterpay refund will '(succeed|fail)' for the order$")
    public void mockAfterpayRefund(final String status) {
        if ("fail".equals(status)) {
            final CreateRefundResponse response = new CreateRefundResponse();
            response.setErrorStatus(HttpStatus.CONFLICT);
            ServiceLookup.getTargetAfterpayClientMock().setCreateRefundResponse(response);
        }
    }

    @Given("^the afterpay refund has been trigerred for the order with amount '(.*)'$")
    public void verifyAfterpayRefundRequest(final double amount) {
        final CreateRefundRequest request = ServiceLookup.getTargetAfterpayClientMock().getCreateRefundRequestCaptor();
        assertThat(Double.valueOf(request.getAmount().getAmount())).isEqualTo(Double.valueOf(amount));
        assertThat(OrderUtil.isRefundValueMatch(Order.getInstance(), Double.valueOf(amount))).as("Refund value match")
                .isTrue();
    }

    @Given("^the afterpay refund has been '(succeeded|failed)'$")
    public void verifyAfterpayRefundEntry(final String status) {
        if ("succeeded".equals(status)) {
            assertThat(OrderUtil.checkRefundSuccss(Order.getInstance())).isTrue();
        }
        else {
            assertThat(OrderUtil.checkRefundSuccss(Order.getInstance())).isFalse();
        }
    }

    private GetConfigurationResponse createConfigurationResponse(final AfterpayConfigurationData configurationData) {
        final GetConfigurationResponse response = new GetConfigurationResponse();
        final Money max = new Money();
        max.setCurrency("AUD");
        max.setAmount(configurationData.getMax());
        response.setMaximumAmount(max);
        final Money min = new Money();
        min.setCurrency("AUD");
        min.setAmount(configurationData.getMin());
        response.setMinimumAmount(min);
        response.setDescription(configurationData.getDescription());
        response.setType(configurationData.getType());
        return response;
    }

    private BigDecimal convertStringToBigDecimal(final String amount) {
        return new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
