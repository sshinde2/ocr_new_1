/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.processengine.enums.ProcessState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.target.tgtmail.dto.TargetCustomerChildDetailsDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CustomerSubscriptionUtil;
import au.com.target.tgttest.automation.facade.bean.CustomerChildDetailsEntry;
import au.com.target.tgttest.automation.facade.bean.CustomerSubscriptionData;
import au.com.target.tgttest.automation.facade.domain.BusinessProcess;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author bhuang3
 * 
 */
public class CustomerSubscriptionSteps {


    private TargetCustomerSubscriptionRequestDto subscriptionData;

    private TargetCustomerSubscriptionResponseDto responseDto;


    @Given("^customer subscription retry interval is set to (\\d+) times every (\\d+) milliseconds$")
    public void setDefaultValueForCustomerSubscription(final int times, final int intervalTime) {
        BusinessProcessUtil.setBusinessProcssRetryTimeForCustomerNewsletterSubscription(times, intervalTime);
        CustomerSubscriptionUtil.setResponse(TargetCustomerSubscriptionResponseType.SUCCESS, "123", 0);
    }

    @Given("^a new customer record with the email is created in ExactTarget consolidated customer database$")
    public void createNewExactTargetRecord() throws InterruptedException {
        CustomerSubscriptionUtil.setResponse(TargetCustomerSubscriptionResponseType.SUCCESS, "123", 0);
        if (subscriptionData == null) {
            setDefaultSubscriptionData();
        }
        subscriptionData.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        responseDto = ServiceLookup.getTargetCustomerSubscriptionFacade().performCustomerSubscription(
                subscriptionData);
    }

    @Given("^customer with subscription details:$")
    public void customerWithSubscriptionDetails(final List<CustomerSubscriptionData> entries) {
        subscriptionData = new TargetCustomerSubscriptionRequestDto();
        final CustomerSubscriptionData entry = entries.get(0);
        subscriptionData.setCustomerEmail(entry.getEmail());
        subscriptionData.setLastName(entry.getLastName());
        subscriptionData.setFirstName(entry.getFirstName());
        subscriptionData.setTitle(entry.getTitle());
    }


    @Given("^customer personal details subscription retry interval is set to (\\d+) times every (\\d+) milliseconds$")
    public void setDefaultValueForCustomerPersonalDetailsSubscription(final int times, final int intervalTime) {
        BusinessProcessUtil.setBusinessProcssRetryTimeForCustomerSubscription(times, intervalTime);
        CustomerSubscriptionUtil.setResponse(TargetCustomerSubscriptionResponseType.SUCCESS, "123", 0);
    }

    @When("^customer subscribes to newsletter$")
    public void customerSubscribes() throws InterruptedException {
        if (subscriptionData == null) {
            setDefaultSubscriptionData();
        }
        subscriptionData.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        responseDto = ServiceLookup.getTargetCustomerSubscriptionFacade().performCustomerSubscription(
                subscriptionData);
        Thread.sleep(2000);
        BusinessProcess.setBusinessProcessModel(BusinessProcessUtil.getLatestCreatedBusinessProcess());
    }

    @Then("^customer subscription process outcome is 'success'$")
    public void subscriptionProcessOutCome() throws InterruptedException {
        verifyBusinessProcess("customerNewsletterSubscriptionProcess");
    }

    @And("^customer gets message for subscription '(.*)'$")
    public void assertCustomerResponseMessage(final String message) {
        assertThat(responseDto.getResponseType().getResponse().equalsIgnoreCase(message));
    }

    @Given("^customer is already subsribed$")
    public void setWebmethodsStatusAlreadyExists() {
        CustomerSubscriptionUtil
                .setResponse(TargetCustomerSubscriptionResponseType.ALREADY_EXISTS, "123", -4);
        setDefaultSubscriptionData();
    }

    @Then("^customer subscription business process fails with retry exceeded$")
    public void businessProcessRetryExceeded() throws InterruptedException {
        verifyBusinessProcessRetryExceeded("customerNewsletterSubscriptionProcess");
    }


    @Given("^an exact target account exists for customer '(.*)'$")
    public void setExactTargetAccForExistingCustomer(final String email) {
        subscriptionData = new TargetCustomerSubscriptionRequestDto();
        setCustomerEmail(email);
    }

    @Given("^customer with personal details$")
    public void setCustomerPerosnalDetails(final List<CustomerSubscriptionData> entries) {
        populateCustomerPersonalDetails(entries);
    }

    @Given("^customer child details$")
    public void setCustomerChildDetails(final List<CustomerChildDetailsEntry> entries) {
        populateChildDetails(entries);
    }

    @When("^customer subscribes to Mumshub$")
    public void customerSubscribesMumsHub() throws InterruptedException {
        ServiceLookup.getTargetCustomerSubscriptionFacade().processCustomerPersonalDetails(subscriptionData);
        Thread.sleep(2000);
        BusinessProcess.setBusinessProcessModel(BusinessProcessUtil.getLatestCreatedBusinessProcess());
        assertThat(BusinessProcess.getInstance()).isNotNull();
    }

    @Then("^customer personal details subscription process outcome is 'success'$")
    public void personalDetailsSubscriptionProcessOutCome() throws InterruptedException {
        verifyBusinessProcess("customerPersonalDetailsSubscriptionProcess");
    }

    @Given("^an exact target account does not exists for customer '(.*)'$")
    public void setExactTargetAccForNewCustomer(final String email) {
        subscriptionData = new TargetCustomerSubscriptionRequestDto();
        setCustomerEmail(email);
    }

    @Then("^customer personal details subscription process fails with retry exceeded$")
    public void customerPersonalSubscriptionBusinessProcessRetry() throws InterruptedException {
        verifyBusinessProcessRetryExceeded("customerPersonalDetailsSubscriptionProcess");
    }


    private void setCustomerEmail(final String email) {
        subscriptionData.setCustomerEmail(email);
    }

    private void populateCustomerPersonalDetails(final List<CustomerSubscriptionData> entries) {
        final CustomerSubscriptionData entry = entries.get(0);
        if (null == subscriptionData) {
            subscriptionData = new TargetCustomerSubscriptionRequestDto();
        }
        subscriptionData.setLastName(entry.getLastName());
        subscriptionData.setFirstName(entry.getFirstName());
        subscriptionData.setTitle(entry.getTitle());
        subscriptionData.setDueDate(new Date());
        subscriptionData.setBirthday(new Date());
        subscriptionData.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.MUMSHUB);
        subscriptionData.setGender(entry.getGender());
        subscriptionData.setBabyGender(entry.getBabyGender());
        subscriptionData.setUpdateCustomerPersonalDetails(true);


    }

    private void populateChildDetails(final List<CustomerChildDetailsEntry> childDetails) {
        final List<TargetCustomerChildDetailsDto> childDetailsList = new ArrayList<>();
        if (null != childDetails) {
            for (final CustomerChildDetailsEntry entry : childDetails) {
                final TargetCustomerChildDetailsDto childDetailsDto = new TargetCustomerChildDetailsDto();
                childDetailsDto.setBirthday(new Date());
                childDetailsDto.setFirstName(entry.getChildName());
                childDetailsDto.setGender(entry.getChildGender());
                childDetailsList.add(childDetailsDto);
            }
            subscriptionData.setChildrenDetails(childDetailsList);
        }


    }

    private void setDefaultSubscriptionData() {
        subscriptionData = new TargetCustomerSubscriptionRequestDto();
        subscriptionData.setCustomerEmail("john@gmail.com");
        subscriptionData.setFirstName("john");
        subscriptionData.setTitle("Mr");
        subscriptionData.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
    }

    private void verifyBusinessProcess(final String processName) throws InterruptedException {
        assertThat(BusinessProcess.getInstance().getProcessDefinitionName())
                .isEqualToIgnoringCase(processName);
        final ProcessState state = BusinessProcessUtil.waitForProcessToFinish(BusinessProcess.getInstance());
        assertThat(state).isEqualTo(ProcessState.SUCCEEDED);
        BusinessProcessUtil.teardownBusinessProcess(BusinessProcess.getInstance());
    }

    private void verifyBusinessProcessRetryExceeded(final String processName) throws InterruptedException {
        assertThat(BusinessProcess.getInstance().getProcessDefinitionName())
                .isEqualToIgnoringCase(processName);
        final ProcessState state = BusinessProcessUtil.waitForProcessToFinish(BusinessProcess.getInstance());
        assertThat(state).isEqualTo(ProcessState.FAILED);
        BusinessProcessUtil.teardownBusinessProcess(BusinessProcess.getInstance());
    }
}
