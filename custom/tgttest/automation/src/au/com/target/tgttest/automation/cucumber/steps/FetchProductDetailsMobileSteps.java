/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;


import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.ProductListerData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author pthoma20
 * 
 */
public class FetchProductDetailsMobileSteps {

    private TargetProductListerData targetProductListerData;

    @Given("^the product (\\w+) exists with (\\d+) colour variants? and (\\d+) size variants?$")
    public void theProductExistsWithColourVariantsAndSizeVariants(final String productCode,
            final int colourVariantCount, final int sizeVariantCount) {
        verifyNumberOfColourAndSizeVariants(productCode, colourVariantCount, sizeVariantCount);
    }

    @When("^the product is fetched using the colourvariant code (\\w+)$")
    public void whenTheProductForCodeIsFetched(final String code) {
        Assertions.assertThat(code).isNotEmpty();

        targetProductListerData = ServiceLookup.getTargetProductListerFacade().getBaseProductDataByCode(code,
                productOptions());
    }

    @Then("^the product and the colour variant should be retrieved with the following details:$")
    public void theProductAndTheColourVariantShouldBeRetrievedWithTheFollowingDetails(
            final List<ProductListerData> products) {
        final Map<String, TargetVariantProductListerData> colourVariantMap = new HashMap<String, TargetVariantProductListerData>();
        for (final TargetVariantProductListerData colourVariant : targetProductListerData
                .getTargetVariantProductListerData()) {
            colourVariantMap.put(colourVariant.getCode(), colourVariant);
        }

        for (final ProductListerData product : products) {
            assertThat(targetProductListerData.getCode()).isEqualTo(product.getProductCode());
            if (product.isVideoUrlPresent()) {
                assertThat(targetProductListerData.getYouTubeLink()).isNotEmpty();
            }
            final TargetVariantProductListerData colourVariant = colourVariantMap.get(product.getColourVariantCode());
            assertThat(colourVariant).isNotNull();

            assertThat(colourVariant.getName()).isEqualTo(product.getName());

            if (StringUtils.isNotBlank(product.getDisplayPriceAtColourLevel())) {
                assertThat(colourVariant.getDisplayPrice()).isEqualTo(product.getDisplayPriceAtColourLevel());
            }
            else {
                assertThat(colourVariant.getDisplayPrice()).isEqualTo(product.getDisplayPrice());
            }

            if (StringUtils.isNotBlank(product.getDisplayWasPriceAtColourLevel())) {
                assertThat(colourVariant.getDisplayWasPrice()).isEqualTo(product.getDisplayWasPriceAtColourLevel());
            }
            else {
                if ("null".equalsIgnoreCase(product.getDisplayWasPrice())) {
                    assertThat(colourVariant.getDisplayWasPrice()).isNull();
                }
                else {
                    assertThat(colourVariant.getDisplayWasPrice()).isEqualTo(product.getDisplayWasPrice());
                }
            }

            if (product.getColour() != null) {
                assertThat(colourVariant.getColourName()).isEqualTo(product.getColour());
            }
            if (product.getSwatchColour() != null) {
                assertThat(colourVariant.getSwatchColour()).isEqualTo(product.getSwatchColour());
            }

            if (product.isImageUrlsPresent()) {
                assertThat(colourVariant.getImages()).isNotEmpty();
            }
            else {
                assertThat(colourVariant.getImages()).isNullOrEmpty();
            }

            if (product.isAssorted()) {
                assertThat(colourVariant.getAssorted()).isTrue();
            }

            assertThat(colourVariant.getVariants()).hasSize(product.getSizeVariantCount());
            if (product.getSizeVariantCount() > 0) {
                final Map<String, TargetVariantProductListerData> sizeVariantMap = new HashMap<String, TargetVariantProductListerData>();
                for (final TargetVariantProductListerData sizeVariant : colourVariant.getVariants()) {
                    sizeVariantMap.put(sizeVariant.getCode(), sizeVariant);
                }

                final TargetVariantProductListerData sizeVariant = sizeVariantMap.get(product.getSizeVariantCode());
                assertThat(sizeVariant.getDisplayPrice()).isEqualTo(product.getDisplayPrice());
                assertThat(sizeVariant.getDisplayWasPrice()).isEqualTo(product.getDisplayWasPrice());
                assertThat(sizeVariant.getSize()).isEqualTo(product.getSize());
                assertThat(sizeVariant.isInStock()).isEqualTo(product.isInStock());
            }

            assertThat(targetProductListerData.isGiftCard()).isEqualTo(products.get(0).isGiftCard());

        }
    }

    @Then("^the size variant with code (\\w+) is not fetched as it is unapproved$")
    public void theSizeVariantWithCodeIsNotFetchAsItIsUunapproved(final String code) {
        for (final TargetVariantProductListerData colourVariant : targetProductListerData
                .getTargetVariantProductListerData()) {
            for (final TargetVariantProductListerData sizeVariant : colourVariant.getVariants()) {
                assertThat(sizeVariant.getCode()).isNotEqualTo(code);
            }
        }
    }

    private void verifyNumberOfColourAndSizeVariants(final String productCode, final int colourVariantCount,
            final int sizeVariantCount) {
        final ProductModel productModel = ProductUtil.getProductModel(productCode);
        Assertions.assertThat(productModel).isInstanceOf(TargetProductModel.class);
        final TargetProductModel targetProductModel = (TargetProductModel)productModel;
        final Collection<VariantProductModel> variantProductModelList = targetProductModel.getVariants();
        Assertions.assertThat(variantProductModelList.size()).isEqualTo(colourVariantCount);
        int countOfSizeVariantsRetrieved = 0;
        for (final VariantProductModel variantProductModel : variantProductModelList) {
            Assertions.assertThat(variantProductModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel targetColourVariantModel = (TargetColourVariantProductModel)variantProductModel;
            if (CollectionUtils.isNotEmpty(targetColourVariantModel.getVariants())) {
                countOfSizeVariantsRetrieved = countOfSizeVariantsRetrieved
                        + targetColourVariantModel.getVariants().size();
            }
        }
        Assertions.assertThat(countOfSizeVariantsRetrieved).isEqualTo(sizeVariantCount);

    }

    private List<ProductOption> productOptions() {
        final List<ProductOption> productOptions = new ArrayList<>();

        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);

        return productOptions;
    }

}
