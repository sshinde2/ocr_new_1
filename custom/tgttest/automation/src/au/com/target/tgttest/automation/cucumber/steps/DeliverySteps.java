/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.DeliveryUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for tests related to delivery
 * 
 * @author jjayawa1
 * 
 */
public class DeliverySteps {

    private String postCode;

    @When("^post code is '(\\d+)'$")
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
        ServiceLookup.getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, postCode);
    }

    @Then("^express delivery available '(.*)'$")
    public void expressDeliveryAvailability(final Boolean value) {

        assertThat(
                DeliveryUtil.isDeliveryModePostCodeCombinationValid(CheckoutUtil.EXPRESS_DELIVERY_CODE, getPostCode()))
                        .isEqualTo(value);
        verifyExpressDeliveryStateForResponseFacade(value);

    }

    @Then("^delivery fee displayed for the '(.*)' is '(.*)'$")
    public void assertDeliveryCostForDeliveryMode(final String deliveryMode, final String deliveryFee) {
        final CheckoutOptionsResponseData result = (CheckoutOptionsResponseData)ServiceLookup
                .getTargetCheckoutResponseFacade().getApplicableDeliveryModes().getData();
        boolean foundDelivery = false;
        if ("Not available".equals(deliveryMode)) {
            return;
        }
        final BigDecimal expectedDeliveryCost = BigDecimal.valueOf(Double.parseDouble(deliveryFee));

        for (final TargetZoneDeliveryModeData deliveryModeData : result.getDeliveryModes()) {
            if (deliveryModeData.getCode().equals(deliveryMode)) {
                assertThat(deliveryModeData.getDeliveryCost().getValue()).isEqualTo(expectedDeliveryCost);
                foundDelivery = true;
            }
        }
        assertThat(foundDelivery).isTrue();
    }


    @Then("^shipster membership status is '(.*)'$")
    public void setShipsterMember(final Boolean value) {
        CartUtil.setShipsterMember(value.booleanValue());
    }

    @SuppressWarnings("boxing")
    private void verifyExpressDeliveryStateForResponseFacade(final Boolean expectedAvaiability) {
        final List<TargetZoneDeliveryModeData> deliveryModes = CartUtil.getApplicableDeliveryModesFromResponseFacade();
        for (final TargetZoneDeliveryModeData deliveryMode : deliveryModes) {
            if (deliveryMode.getCode().equals(CheckoutUtil.EXPRESS_DELIVERY_CODE)) {
                assertThat(deliveryMode.isAvailable()).isEqualTo(expectedAvaiability);
                assertThat(deliveryMode.isPostCodeNotSupported()).isEqualTo(!expectedAvaiability);
            }
        }

    }

    @Then("^promotional messages retrieved for the concerned delivery modes are:$")
    public void verifyOrderPromotionalMessage(final List<TargetZoneDeliveryModeData> deliveryModesToCompare) {
        final List<TargetZoneDeliveryModeData> targetZoneDeliveryModeDataList = ServiceLookup.getTargetCheckoutFacade()
                .getDeliveryModes();
        final Map<String, TargetZoneDeliveryModeData> deliveryModeMap = Maps.uniqueIndex(targetZoneDeliveryModeDataList,
                new Function<TargetZoneDeliveryModeData, String>() {
                    /* (nonJavadoc)
                     * @see com.google.common.base.Function#apply(java.lang.Object)
                     */
                    @Override
                    public String apply(final TargetZoneDeliveryModeData targetZoneDeliveryModeData) {
                        return targetZoneDeliveryModeData.getCode();
                    }
                });
        for (final TargetZoneDeliveryModeData deliveryMode : deliveryModesToCompare) {
            final TargetZoneDeliveryModeData actualDeliveryMode = deliveryModeMap.get(deliveryMode.getCode());
            assertThat(actualDeliveryMode.getCode()).isNotNull().isEqualTo(deliveryMode.getCode());
            if ("null".equals(deliveryMode.getIncentiveMetMessage())) {
                assertThat(actualDeliveryMode.getIncentiveMetMessage()).isNull();
            }
            else {
                assertThat(actualDeliveryMode.getIncentiveMetMessage()).isNotNull()
                        .isEqualTo(deliveryMode.getIncentiveMetMessage());
            }
            if ("null".equals(deliveryMode.getPotentialIncentiveMessage())) {
                assertThat(actualDeliveryMode.getPotentialIncentiveMessage()).isNull();
            }
            else {
                assertThat(actualDeliveryMode.getPotentialIncentiveMessage()).isNotNull()
                        .isEqualTo(deliveryMode.getPotentialIncentiveMessage());
            }
        }
    }

    @And("^express delivery mode fee is '(.*)'$")
    public void setExpressDeliveryModeFee(final String fee) {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = ServiceLookup
                .getModelService().create(RestrictableTargetZoneDeliveryModeValueModel.class);

        final ZoneDeliveryModeModel deliverMode = (ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE);
        deliverMode.setValues(null);
        ServiceLookup.getModelService().save(deliverMode);
        ServiceLookup.getModelService().refresh(deliverMode);
        restrictableTargetZoneDeliveryModeValueModel.setDeliveryMode((ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE));

        restrictableTargetZoneDeliveryModeValueModel.setPriority(Integer.valueOf(0));
        restrictableTargetZoneDeliveryModeValueModel
                .setZone(ServiceLookup.getZoneDeliveryModeService().getZoneForCode(
                        "express"));
        restrictableTargetZoneDeliveryModeValueModel.setCurrency(ServiceLookup.getCommonI18nService()
                .getCurrency("AUD"));
        restrictableTargetZoneDeliveryModeValueModel.setMinimum(Double.valueOf(1));
        restrictableTargetZoneDeliveryModeValueModel.setValue(Double.valueOf(fee));
        ServiceLookup.getModelService().save(restrictableTargetZoneDeliveryModeValueModel);
        ServiceLookup.getModelService().refresh(restrictableTargetZoneDeliveryModeValueModel);
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }
}
