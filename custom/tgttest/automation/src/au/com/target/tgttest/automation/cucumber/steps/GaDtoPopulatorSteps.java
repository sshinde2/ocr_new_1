/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import java.util.List;

import junit.framework.Assert;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.GAProductDisplayPageDto;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author smishra1
 *
 */
public class GaDtoPopulatorSteps {
    private TargetProductData targetProdcutData;

    @When("^the '(\\w+)' is retrieved$")
    public void shreeram(final String productCode) {
        targetProdcutData = ProductUtil.populateThePriceIntoProductData(productCode);
    }

    @Then("the product details are:")
    public void shreeram1(final List<GAProductDisplayPageDto> pdpDtoList) {
        for (final GAProductDisplayPageDto pdpDto : pdpDtoList) {
            verifyPdpDataInDto(pdpDto, targetProdcutData);
        }
    }

    private void verifyPdpDataInDto(final GAProductDisplayPageDto pdpDto, final TargetProductData prodcutData) {
        Assert.assertEquals(prodcutData.getBrand(), pdpDto.getBrand());
        Assert.assertEquals(prodcutData.getBaseName(), pdpDto.getBaseName());
        Assert.assertEquals(prodcutData.getCode(), pdpDto.getCode());
        Assert.assertEquals(prodcutData.getBaseProductCode(), pdpDto.getBaseCode());
        Assert.assertEquals(prodcutData.getTopLevelCategory(), pdpDto.getDepartmentCategory());
        Assert.assertEquals(prodcutData.getPrice().getFormattedValue(), pdpDto.getPrice());
    }
}
