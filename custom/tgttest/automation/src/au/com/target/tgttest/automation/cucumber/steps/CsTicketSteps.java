/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ticket.events.model.CsTicketEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketService;

import java.util.List;

import au.com.target.tgtcore.ticket.service.impl.TestTicketBusinessServiceImpl;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Then;


/**
 * @author rsamuel3
 * 
 */
public class CsTicketSteps {

    private static final String NOT_APPLICABLE = "N/A";

    private CsTicketModel ticket;


    @Then("^a CS ticket is created")
    public void verifyCsTicketCreated() {

        // there may not be an order but we can get the ticket id from the test ticket service
        final String lastTicketId = TestTicketBusinessServiceImpl.getTicketId();
        assertThat(lastTicketId).as("id of last ticket sent").isNotEmpty();
        final TicketService ticketService = ServiceLookup.getTicketService();
        final CsTicketModel ticketModel = ticketService.getTicketForTicketId(lastTicketId);
        assertThat(ticketModel).as("last ticket").isNotNull();
        ticket = ticketModel;
    }

    @Then("^a CS ticket is created for the order")
    public void verifyCsTicketCreatedForOrder() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final TicketService ticketService = ServiceLookup.getTicketService();
        final List<CsTicketModel> ticketModel = ticketService.getTicketsForOrder(orderModel);
        assertThat(ticketModel).isNotNull().isNotEmpty().hasSize(1);
        ticket = ticketModel.get(0);
    }

    @Then("^a CS ticket is not created for the order")
    public void verifyCsTicketNotCreatedForOrder() {
        verifyCSTicketNotCreated();
    }

    @Then("^CS ticket headline is '(.*)'$")
    public void verifyHeadline(final String headline) {
        assertThat(ticket.getHeadline()).isNotNull().isNotEmpty().isEqualTo(headline);
    }

    @Then("^CS ticket subject is '(.*)'$")
    public void verifySubject(final String subject) {
        final List<CsTicketEventModel> events = ServiceLookup.getTicketService().getEventsForTicket(ticket);
        assertThat(events).isNotNull().isNotEmpty().hasSize(1);
        final CsTicketEventModel event = events.get(0);
        assertThat(event.getSubject()).isNotNull().isNotEmpty().isEqualTo(subject);
    }


    @Then("^the CS ticket group is (.*)$")
    public void verifyCsTicketGroup(final String grpName) {
        if (NOT_APPLICABLE.equalsIgnoreCase(grpName)) {
            verifyCSTicketNotCreated();
        }
        else {
            final OrderModel orderModel = Order.getInstance().getOrderModel();
            final TicketService ticketService = ServiceLookup.getTicketService();
            final List<CsTicketModel> ticketModelList = ticketService.getTicketsForOrder(orderModel);
            assertThat(ticketModelList).isNotEmpty();
            assertThat(ticketModelList.get(0).getAssignedGroup()).isNotNull();
            assertThat(ticketModelList.get(0).getAssignedGroup().getUid()).as("CsAgent group").isEqualTo(grpName);

        }
    }

    @Then("^the last CS ticket group is '(.*)'$")
    public void verifyLastCsTicketGroup(final String grpName) {
        assertThat(ticket.getAssignedGroup()).isNotNull();
        assertThat(ticket.getAssignedGroup().getUid()).as("CsAgent group").isEqualTo(grpName);

    }

    @Then("^CS ticket body is '(.*)'$")
    public void verifyBody(final String body) {
        final List<CsTicketEventModel> events = ServiceLookup.getTicketService().getEventsForTicket(ticket);
        assertThat(events).isNotNull().isNotEmpty().hasSize(1);
        final CsTicketEventModel event = events.get(0);
        assertThat(event.getText()).isNotNull().isNotEmpty().startsWith(body);
    }

    @Then("the amount in the cs ticket is '(.*)'")
    public void verifyAmountInBody(final String amount) {
        final List<CsTicketEventModel> events = ServiceLookup.getTicketService().getEventsForTicket(ticket);
        assertThat(events).isNotNull().isNotEmpty().hasSize(1);
        final CsTicketEventModel event = events.get(0);
        assertThat(event.getText()).isNotNull().isNotEmpty().contains("$" + amount + " (receipt");
    }

    private void verifyCSTicketNotCreated() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final TicketService ticketService = ServiceLookup.getTicketService();
        final List<CsTicketModel> ticketModel = ticketService.getTicketsForOrder(orderModel);
        assertThat(ticketModel).isNullOrEmpty();
    }

}
