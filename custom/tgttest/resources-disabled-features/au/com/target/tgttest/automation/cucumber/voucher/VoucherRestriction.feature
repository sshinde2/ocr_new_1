@voucherRestrictionData
Feature: Vouchers - Vouchers can be restricted to specific products or categories, and excluded products shouldn't count towards the order total.

  Scenario: A voucher is applied to a cart containing only products that are not included in the voucher.
    Given cart entries:
      | product       | qty | price |
      | P4002_black_S | 1   | 10    |
      | P4002_black_M | 1   | 10    |
    When apply voucher to cart VOUCHER-TV3
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing only products that are not excluded from the voucher.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV4
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing only products from an included category.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV5
    Then cart total is 10

  Scenario: A voucher is applied to a cart containing only products that are explicitly included.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV6
    Then cart total is 10

  Scenario: A voucher is applied to a cart containing only products from an excluded category.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV7
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing only products that are explicitly excluded.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV8
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing products from an included category that are also in an excluded category, where the eligible item total is over the required order total.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
      | CP7018  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV9
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing products from an included category that are also explicitly excluded, where the eligible item total is over the required order total.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7007  | 1   | 10    |
      | CP7008  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV10
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing products that are explicitly included that are also in an excluded category, where the eligible item total is over the required order total.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
      | CP7018  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV11
    Then cart total is 20

  Scenario: A voucher is applied to a cart containing products from an included category that are also in an excluded category, where the eligible item total is below the required order total.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7019  | 1   | 10    |
      | CP7018  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV9
    Then cart total is 30

  Scenario: A voucher is applied to a cart containing products from an included category that are also explicitly excluded, where the eligible item total is below the required order total.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7005  | 1   | 10    |
      | CP7007  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV10
    Then cart total is 30

  Scenario: A voucher is applied to a cart containing products that are explicitly included that are also in an excluded category, where the eligible item total is below the required order total.
    Given cart entries:
      | product | qty | price |
      | CP7004  | 1   | 10    |
      | CP7019  | 1   | 10    |
      | CP7018  | 1   | 10    |
    When apply voucher to cart VOUCHER-TV11
    Then cart total is 30
