@tmddata
Feature: Appling TMD on a cart
  Team member discounts should be applied regardeless of the order being part of a another deal

  Scenario: Placing an  order with home delivery, no deals or discounts.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 39.0  | 30.0     | 9.0          |          | 0.0            |
    And tlog total tender is 3900
    And accertify entries:
      | targetStaffDiscountCard | totalAmount | promotionAmount |
      |                         | 39.00       | 0.0             |

  Scenario: Placing an order with click-and-collect, no deals or discounts.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'click-and-collect'
    When place order
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 35.0  | 30.0     | 5.0          |          | 0.0            |
    And tlog total tender is 3500
    And accertify entries:
      | targetStaffDiscountCard | totalAmount | promotionAmount |
      |                         | 35.00       | 0.0             |

  Scenario: Placing an order with TMD only.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 37.50 | 28.50    | 9.0          |          | 0.0            |
    Then tlog entries are:
      | code          | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | P4025_black_S | 2   | 1500  | 1500      | staff            | 5               | 150                |
    And tlog total tender is 3750
    And transDiscount entries are:
      | type  | code          | pct | amount |
      | staff | 2771166666666 | 5   | 150    |
    And accertify entries:
      | targetStaffDiscountCard | totalAmount | promotionAmount |
      | 2771166666666           | 37.50       | 1.50            |

  Scenario: Placing an Home delivery order with deal items and TMD.
    Given cart entries:
      | product  | qty | price |
      | 10000001 | 2   | 10    |
    And quantity break deal product '10000001'
    And quantity break deal break points:
      | qty | unitPrice |
      | 2   | 8         |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 24.20 | 15.20    | 9.0          |          | 4.80           |
    Then tlog entries are:
      | code     | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000001 | 1   | 1000  | 1000      | R            | 5002       | 1                | 200              | staff            | 5               | 40                 |
      | 10000001 | 1   | 1000  | 1000      | R            | 5002       | 1                | 200              | staff            | 5               | 40                 |
    And tlog total tender is 2420
    And transDeal entries are:
      | type | id   | instance | markdown |
      | 5    | 5002 | 1        | 400      |
    And transDiscount entries are:
      | type  | code          | pct | amount |
      | staff | 2771166666666 | 5   | 480    |
    And accertify entries:
      | targetStaffDiscountCard | totalAmount | promotionAmount |
      | 2771166666666           | 24.20       | 4.80            |
