@pinpadData
Feature: Placing order with pinpad creates order but waits for update
  from the pinpad to the hybris web service before finishing the order.

  Scenario: Placing an order with pinpad leaves payment in review.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When start order with pinpad store 5098
    Then cart payment is in status 'REVIEW'
    And cart pinpad payment info is empty

  Scenario: Placing an order with pinpad and then getting successful webservice update completes the order.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    Then place order result is 'SUCCESS'
    And business process completes
    And order total is 39.0
    And order payment is in status 'ACCEPTED'
    And order pinpad payment info is populated
    And order store number is 5098
    And tlog type is 'sale'
    And tlog pinpad payment info matches

  # CheckOrderStatusAction will wait for retry - so we reduce the retry interval to 10s by updating StandardPaymentModel in an impex
  Scenario: Placing an order with pinpad but out of order so finish is done before payment update sent.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When start order with pinpad store 5098
    And finish order
    And pinpad sends payment update with success 'true'
    Then place order result is 'SUCCESS'
    And business process completes
    And order total is 39.0
    And order payment is in status 'ACCEPTED'
    And order pinpad payment info is populated
    And order store number is 5098
    And tlog type is 'sale'
    And tlog pinpad payment info matches

  Scenario: Placing an order with pinpad and getting declined.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When start order with pinpad store 5098
    And pinpad sends payment update with success 'false'
    And finish order
    Then place order result is 'PAYMENT_FAILURE'
    And cart payment is in status 'REJECTED'

 Scenario: Placing an order with pinpad and getting declined, but finish is called before hybris update.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When start order with pinpad store 5098
    And finish order
    And pinpad sends payment update with success 'false'
    Then payment failed business process completes
    And place order result is 'SUCCESS'
    And order is in status 'CANCELLED'
    And order payment is in status 'REJECTED'
        