Feature: Place Order - Place B2B order in CS Cockpit
    As online customer service agent
    I want to place both b2b and normal order on behalf of a customer in CS Cockpit

  Scenario: Placing B2B order by selecting B2B order check box.
    Given any cart
    And B2B Order flag is set to 'true'
    When order is placed in cscockpit
    Then sales application of order is 'B2B'

  Scenario: Placing normal order by not selecting B2B order check box.
    Given any cart
    And B2B Order flag is set to 'false'
    When order is placed in cscockpit
    Then sales application of order is 'CallCenter'
