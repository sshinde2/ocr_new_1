@voucherData
Feature: Orders with expired vouchers should be able to be refunded.
  when refunding a order even if the voucher is expired correct amount should be refunded.
  
  Scenario: full cancel an order with a expired voucher.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order with voucher 'VOUCHER-TV3'
    And expire the voucher 'VOUCHER-TV3'
    And the order is fully cancelled
    Then refund value should be '20.00'

  Scenario: partial cancel an order with a expired voucher
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order with voucher 'VOUCHER-TV3'
    And expire the voucher 'VOUCHER-TV3'
    And cancel entries
      | product       | qty |
      | P4025_black_S | 1   |
    Then refund value should be '5.00'
    
    
