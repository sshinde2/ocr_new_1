/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.AbstractConcurrentJaloSessionCallable;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao;


/**
 * 
 * Integration test for {@link ShopTheLookDao}
 * 
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetShopTheLookDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private ShopTheLookDao shopTheLookDao;

    @Resource
    private ModelService modelService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtmarketing/test/test-looks.impex", Charsets.UTF_8.name());
    }

    @Test
    public void getExistingShopTheLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetShopTheLookModel shopTheLookModel = shopTheLookDao.getShopTheLookForCode("STL02");
        Assert.assertNotNull(shopTheLookModel);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testUnknownShopTheLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        shopTheLookDao.getShopTheLookForCode("STL01");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShopTheLookMissingCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        shopTheLookDao.getShopTheLookForCode(null);
    }

    @Test
    public void testConcurrentCreateDoesNotProduceDuplicateShopTheLooks()
            throws InterruptedException, ExecutionException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final int noOfThreads = 2;
        // barrier to ensure both threads start at exactly the same time
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final String id = "STL01";
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final AbstractConcurrentJaloSessionCallable<TargetShopTheLookModel> createShopTheLook = new AbstractConcurrentJaloSessionCallable() {

            @Override
            public TargetShopTheLookModel execute() throws InterruptedException, BrokenBarrierException,
                    TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
                final TargetShopTheLookModel shopTheLook = modelService.create(TargetShopTheLookModel.class);
                shopTheLook.setId(id);
                shopTheLook.setName("Look Collection 1");

                barrier.await();
                try {
                    modelService.save(shopTheLook);
                }
                catch (final ModelSavingException e) {
                    //ignore this
                }
                return shopTheLookDao.getShopTheLookForCode(id);
            }
        };

        final List<Future<TargetShopTheLookModel>> futures = new ArrayList<>();
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(createShopTheLook));
        }

        for (int i = 1; i < noOfThreads; i++) {
            Assert.assertEquals(futures.get(i).get(), futures.get(i - 1).get());
        }

        // if a concurrency issue does occur, a subsequent invocation would return null coz of ambiguous results
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(id);
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(shopTheLook, futures.get(0).get());
    }


    @Test
    public void testGetListOfLookForShopTheLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final List<TargetLookModel> targetLookModelList = shopTheLookDao.getListOfLooksForShopTheLookUsingCode("STL02");
        Assertions.assertThat(targetLookModelList).isNotEmpty();
        Assertions.assertThat(targetLookModelList).hasSize(4);
        Assertions.assertThat(targetLookModelList.get(0).getId()).isEqualTo("womenSpringCasualLook6");
        Assertions.assertThat(targetLookModelList.get(1).getId()).isEqualTo("womenSpringCasualLook1");
        Assertions.assertThat(targetLookModelList.get(2).getId()).isEqualTo("womenSpringCasualLook4");
        Assertions.assertThat(targetLookModelList.get(3).getId()).isEqualTo("casualWearLook1");
    }

    @Test
    public void testGetInspirationByCategoryCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetShopTheLookModel shopTheLookModel = shopTheLookDao.getShopTheLookByCategoryCode("N330009");
        assertThat(shopTheLookModel.getId()).isEqualTo("STL02");
        assertThat(shopTheLookModel.getName()).isEqualTo("Women's Inspiration");
        assertThat(shopTheLookModel.getCategory().getCode()).isEqualTo("N330009");
    }
}
