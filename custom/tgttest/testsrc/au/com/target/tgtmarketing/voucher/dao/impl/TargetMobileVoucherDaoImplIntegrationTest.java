/**
 * 
 */
package au.com.target.tgtmarketing.voucher.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.TargetMobileOfferHeadingsTestHelper;
import au.com.target.tgtmarketing.voucher.TargetMobileVoucherTestHelper;
import au.com.target.tgtmarketing.voucher.dao.TargetMobileVoucherDao;


/**
 * @author paul
 *
 */

/**
 * Integration test for {@link TargetMobileVoucherDao}
 */
@IntegrationTest
public class TargetMobileVoucherDaoImplIntegrationTest extends ServicelayerTransactionalTest {


    @Resource
    private TargetMobileVoucherDao targetMobileVoucherDao;

    private final List<PromotionVoucherModel> promotionVoucherModelList = new ArrayList<>();

    private final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList =
            new ArrayList<>();

    @Before
    public void setup() {

        targetMobileOfferHeadingModelList.add(TargetMobileOfferHeadingsTestHelper
                .setupTargetMobileOfferHeadingsData("entertainment", "ENTERTAINMENT", "#4C4D74"));
        promotionVoucherModelList.add(TargetMobileVoucherTestHelper.setupActivePromotionVoucherModel("901",
                Boolean.TRUE, "VOUCH1", null,
                Boolean.FALSE, Boolean.TRUE, targetMobileOfferHeadingModelList));
        promotionVoucherModelList.add(TargetMobileVoucherTestHelper.setupActivePromotionVoucherModel("902",
                Boolean.FALSE, null, Long.valueOf(2721020559900L), Boolean.TRUE, Boolean.TRUE,
                targetMobileOfferHeadingModelList));
        promotionVoucherModelList.add(TargetMobileVoucherTestHelper.setupActivePromotionVoucherModel("903",
                Boolean.TRUE, "VOUCH2", Long.valueOf(2721020669906L), Boolean.TRUE, Boolean.TRUE,
                targetMobileOfferHeadingModelList));
    }

    @Test
    public void testGetAllActiveVouchers() {

        final List<PromotionVoucherModel> activeMobileVouchers = targetMobileVoucherDao.getAllActiveMobileVouchers();

        Assertions.assertThat(activeMobileVouchers).isNotEmpty();
        final List<String> expectedVouchers = new ArrayList<>();
        for (final PromotionVoucherModel expectedPromotionVoucherModel : promotionVoucherModelList) {
            expectedVouchers.add(expectedPromotionVoucherModel.getCode());
        }
        for (final PromotionVoucherModel promotionVoucherModel : activeMobileVouchers) {
            Assertions.assertThat(expectedVouchers).contains(promotionVoucherModel.getCode());
        }
    }

    @After
    public void tearDown() {
        TargetMobileVoucherTestHelper.removePromotionVoucherModel(promotionVoucherModelList);
        TargetMobileOfferHeadingsTestHelper.removeTargetMobileOfferHeadingsData(targetMobileOfferHeadingModelList);
    }
}
