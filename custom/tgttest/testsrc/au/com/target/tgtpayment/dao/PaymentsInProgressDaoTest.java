/**
 * 
 */
package au.com.target.tgtpayment.dao;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtpayment.dao.impl.PaymentsInProgressDaoImpl;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;


@IntegrationTest
public class PaymentsInProgressDaoTest extends ServicelayerTransactionalTest {

    @Resource
    private PaymentsInProgressDaoImpl paymentsInProgressDao;

    @Resource
    private ModelService modelService;

    @Before
    public void setup() {
        //
    }

    @Test
    public void testFetchPaymentsInProgressAfterServerCrash() {
        setupTestData4FetchPaymentsInProgressAfterServerCrash();

        final List<PaymentsInProgressModel> carts = paymentsInProgressDao
                .fetchPaymentsInProgressAfterServerCrash(DateTime
                        .now()
                        .minusMinutes(45).toDate());

        Assert.assertEquals(0, carts.size());

        final List<PaymentsInProgressModel> carts2 = paymentsInProgressDao
                .fetchPaymentsInProgressAfterServerCrash(DateTime
                        .now()
                        .minusMinutes(15).toDate());

        Assert.assertEquals(1, carts2.size());
    }

    public void setupTestData4FetchPaymentsInProgressAfterServerCrash() {
        final CartModel cart = modelService.create(CartModel.class);
        cart.setCode("test123");
        cart.setDate(new Date());
        cart.setCurrency(getCurrencyTestData());
        cart.setUser(getUser());
        modelService.save(cart);

        final PaymentsInProgressModel paymentsInProgress = modelService.create(PaymentsInProgressModel.class);
        paymentsInProgress.setAbstractOrder(cart);
        paymentsInProgress.setPaymentInitiatedTime(DateTime.now().minusMinutes(30).toDate());
        paymentsInProgress.setPaymentCaptureType(PaymentCaptureType.PLACEORDER);
        modelService.save(paymentsInProgress);
    }


    private CurrencyModel getCurrencyTestData() {
        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("TEST");
        currency.setSymbol("TEST");
        modelService.save(currency);

        return currency;
    }

    private UserModel getUser() {
        final UserModel user = modelService.create(UserModel.class);
        user.setUid("user");
        modelService.save(user);
        return user;
    }
}
