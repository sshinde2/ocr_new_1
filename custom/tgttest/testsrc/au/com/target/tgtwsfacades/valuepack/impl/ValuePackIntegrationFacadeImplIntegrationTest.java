/**
 * 
 */
package au.com.target.tgtwsfacades.valuepack.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.enums.DealTypeEnum;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.valuepack.TargetValuePackService;
import au.com.target.tgtcore.valuepack.TargetValuePackTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackItem;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackItemList;
import au.com.target.tgtwsfacades.valuepack.ValuePackIntegrationFacade;


/**
 * @author mmaki
 * 
 */
@IntegrationTest
public class ValuePackIntegrationFacadeImplIntegrationTest extends ServicelayerTransactionalTest {

    private static final String CHILD_SKU = "1000003";
    private static final String EXISTING_CHILD_SKU1 = "1000001";
    private static final String EXISTING_CHILD_SKU2 = "1000002";
    private static final String EXISTING_LEAD_SKU = "1100001";
    private static final String DESCRIPTION = "Description";
    private static final String DESCRIPTION2 = "Description2";
    private static final Integer QUANTITY = new Integer(1);
    private static final Double PRICE = new Double(20.5);

    @Resource
    private ValuePackIntegrationFacade valuePackIntegrationFacade;

    @Resource
    private ModelService modelService;

    @Resource
    private TargetValuePackService valuePackService;

    @Resource
    private TargetValuePackTypeService valuePackTypeService;


    @Before
    public void setupBefore() throws ImpExException {
        final TargetValuePackModel vp = modelService.create(TargetValuePackModel.class);
        vp.setChildSKU(EXISTING_CHILD_SKU1);
        vp.setChildSKUDescription(DESCRIPTION);
        vp.setValuePackDealType(DealTypeEnum.VALUEBUNDLE);
        vp.setPrice(PRICE);
        modelService.save(vp);

        final TargetValuePackModel vp2 = modelService.create(TargetValuePackModel.class);
        vp2.setChildSKU(EXISTING_CHILD_SKU2);
        vp2.setChildSKUDescription(DESCRIPTION);
        vp2.setValuePackDealType(DealTypeEnum.VALUEBUNDLE);
        vp2.setPrice(PRICE);
        modelService.save(vp2);

        final TargetValuePackTypeModel type = modelService.create(TargetValuePackTypeModel.class);
        type.setLeadSKU(EXISTING_LEAD_SKU);
        type.setValuePackListQualifier(new ArrayList<TargetValuePackModel>());
        type.getValuePackListQualifier().add(vp);
        type.getValuePackListQualifier().add(vp2);
        modelService.save(type);
    }

    @Test
    public void testUpdateValuePack() {
        Assert.assertNotNull(findTargetValuePack(EXISTING_CHILD_SKU1));
        Assert.assertNotNull(findTargetValuePack(EXISTING_CHILD_SKU2));
        Assert.assertNull(findTargetValuePack(CHILD_SKU));
        Assert.assertNotNull(findTargetValuePackType(EXISTING_LEAD_SKU));

        final IntegrationValuePackDto dto = new IntegrationValuePackDto();
        dto.setLeadSKU(EXISTING_LEAD_SKU);
        dto.setValuePackItems(new IntegrationValuePackItemList());
        dto.getValuePackItems().setValuePackItems(new ArrayList<IntegrationValuePackItem>());
        final IntegrationValuePackItem item = new IntegrationValuePackItem();
        item.setChildSKU(EXISTING_CHILD_SKU1);
        item.setDealType("Value Bundle");
        item.setChildSKUDescription(DESCRIPTION2);
        item.setPrice(PRICE);
        item.setQuantity(QUANTITY);
        dto.getValuePackItems().getValuePackItems().add(item);
        final IntegrationValuePackItem item2 = new IntegrationValuePackItem();
        item2.setChildSKU(CHILD_SKU);
        item2.setDealType("Value Bundle");
        item2.setChildSKUDescription(DESCRIPTION);
        item2.setPrice(PRICE);
        item2.setQuantity(QUANTITY);
        dto.getValuePackItems().getValuePackItems().add(item2);

        final IntegrationResponseDto response = valuePackIntegrationFacade.updateValuePack(dto);

        Assert.assertTrue("Status not correct", response.isSuccessStatus());
        Assert.assertNotNull(findTargetValuePack(CHILD_SKU));
        final TargetValuePackModel updatedValuePack = findTargetValuePack(EXISTING_CHILD_SKU1);
        Assert.assertNotNull(updatedValuePack);
        Assert.assertEquals("Description not updated", DESCRIPTION2, updatedValuePack.getChildSKUDescription());

        final TargetValuePackTypeModel updatedType = findTargetValuePackType(EXISTING_LEAD_SKU);
        Assert.assertEquals("Associations not correct", 2, updatedType.getValuePackListQualifier().size());
        Assert.assertEquals("Associations not correct", EXISTING_CHILD_SKU1, updatedType.getValuePackListQualifier()
                .get(0).getChildSKU());
        Assert.assertEquals("Associations not correct", CHILD_SKU, updatedType.getValuePackListQualifier().get(1)
                .getChildSKU());
    }

    private TargetValuePackModel findTargetValuePack(final String childSKU) {
        try {
            return valuePackService.getByChildSku(childSKU);
        }
        catch (final ModelNotFoundException e) {
            return null;
        }
    }

    private TargetValuePackTypeModel findTargetValuePackType(final String leadSKU) {
        try {
            return valuePackTypeService.getByLeadSku(leadSKU);
        }
        catch (final ModelNotFoundException e) {
            return null;
        }
    }

}
