/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;

import com.google.common.base.Charsets;


/**
 * @author fkratoch
 * 
 */
@IntegrationTest
public class TargetProductImportIntegrationFacadeMoveBetweenStylegroupFeatureSwitchTest extends TargetProductImportIntegrationFacadeTest {

    private static final String BASE_PRODUCT_CODE = "P123456789";
    private static final String NEW_BASE_PRODUCT_CODE = "P987654321";

    @Resource
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    @Before
    public void setupBefore() throws ImpExException {
        super.setupBefore();
        importCsv("/tgtcore/test/feature-move-between-stylegroup.impex", Charsets.UTF_8.name());

    }

    @Test
    public void testCanUpdateExistingColourVariantWasNotUnderStyleGroupButNowWithNewBaseProductUnderStylegroup() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsStylegroup(Boolean.FALSE);
        dto.setColour("Red Colour");
        dto.setSwatch("Red Swatch");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // check new colour variant
        assertTrue("Updated to new base product", BASE_PRODUCT_CODE.equals(variant1.getBaseProduct().getCode()));
        assertTrue("Can update existing colour variant", response1.isSuccessStatus());
        assertEquals("Can update existing colour variant - variant status", ArticleApprovalStatus.APPROVED,
                variant1.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());

        // Update variant - updating colour will create new Colour Variant !!!
        dto.setApprovalStatus("Inactive");
        dto.setIsStylegroup(Boolean.TRUE);
        dto.setProductCode(NEW_BASE_PRODUCT_CODE);
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // Check updated variant
        assertTrue("Updated to new base product", NEW_BASE_PRODUCT_CODE.equals(variant2.getBaseProduct().getCode()));
        assertTrue("Can update existing colour variant", response2.isSuccessStatus());
        assertEquals("Can update existing colour variant - updated variant status", ArticleApprovalStatus.UNAPPROVED,
                variant2.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());
    }

    @Test
    public void testCanUpdateExistingColourVariantInStylegroupWithNewBaseProduct() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Red Colour");
        dto.setSwatch("Red Swatch");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // check new colour variant
        assertTrue("Updated to new base product", BASE_PRODUCT_CODE.equals(variant1.getBaseProduct().getCode()));
        assertTrue("Can update existing colour variant", response1.isSuccessStatus());
        assertEquals("Can update existing colour variant - variant status", ArticleApprovalStatus.APPROVED,
                variant1.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());

        // Update variant - updating colour will create new Colour Variant !!!
        dto.setApprovalStatus("Inactive");
        dto.setProductCode(NEW_BASE_PRODUCT_CODE);
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // Check updated variant
        assertTrue("Updated to new base product", NEW_BASE_PRODUCT_CODE.equals(variant2.getBaseProduct().getCode()));
        assertTrue("Can update existing colour variant", response2.isSuccessStatus());
        assertEquals("Can update existing colour variant - updated variant status", ArticleApprovalStatus.UNAPPROVED,
                variant2.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());
    }

    @Test
    public void testCanUpdateSizeVariantNExistingColourVariantWithNewBaseProduct() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setColour("Red Colour");
        dto.setSwatch("Red Swatch");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // check new colour variant
        assertTrue("Can update existing colour variant", response1.isSuccessStatus());
        assertEquals("Can update existing colour variant - variant status", ArticleApprovalStatus.APPROVED,
                variant1.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());

        // Update variant - updating colour will create new Colour Variant !!!
        dto.setApprovalStatus("Inactive");
        dto.setProductCode(NEW_BASE_PRODUCT_CODE);
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // Check updated variant
        assertTrue("Can update existing colour variant", response2.isSuccessStatus());
        assertTrue("Can update existing base product",
                NEW_BASE_PRODUCT_CODE.equals(variant2.getBaseProduct().getCode()));
        assertEquals("Can update existing colour variant - updated variant status", ArticleApprovalStatus.UNAPPROVED,
                variant2.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());
    }


    @Test
    public void testOnlyOneColourVariantCreatedIfNewBaseProductButNewBaseProducthasSameSwatchColourCombination() {
        final IntegrationProductDto dto1 = createTestDto(true); // dto1 contains sizes 10 and 11
        dto1.setColour("Red");
        dto1.setSwatch("Crimson Red");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto1);

        final IntegrationProductDto dto2 = createTestDto(false);
        dto2.setProductCode(NEW_BASE_PRODUCT_CODE);
        dto2.setVariantCode("V987654322"); // different variant code
        dto2.setColour("Red");
        dto2.setSwatch("Crimson Red");// same swatch colour

        final IntegrationVariantDto varDto1 = new IntegrationVariantDto();
        varDto1.setVariantCode("S44332211");
        varDto1.setBaseProduct(BASE_PRODUCT_CODE);
        varDto1.setApprovalStatus("Active");
        varDto1.setSize("12");

        final IntegrationVariantDto varDto2 = new IntegrationVariantDto();
        varDto2.setVariantCode("S88775566");
        varDto2.setBaseProduct(BASE_PRODUCT_CODE);
        varDto2.setApprovalStatus("Active");
        varDto2.setSize("13");

        dto2.getVariants().add(varDto1);
        dto2.getVariants().add(varDto2);

        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto2);

        dto1.setProductCode(NEW_BASE_PRODUCT_CODE);
        final IntegrationResponseDto response3 = productImportIntegrationFacade.persistTargetProduct(dto1);

        assertTrue("Only one colour variant created  - first insert", response1.isSuccessStatus());
        assertTrue("Only one colour variant created  - second insert", response2.isSuccessStatus());
        assertTrue("Only one colour variant created  - third insert", response3.isSuccessStatus());
        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(dto1
                .getProductCode());

        final Collection<VariantProductModel> colourVariants = baseProduct.getVariants();
        assertEquals(1, colourVariants.size());
        final VariantProductModel colourVariant = colourVariants.iterator().next();
        final Collection<VariantProductModel> sizeVariants = colourVariant.getVariants();
        assertEquals(4, sizeVariants.size());

        final TargetProductModel oldbaseProduct = (TargetProductModel)productService
                .getProductForCode(BASE_PRODUCT_CODE);

        assertTrue(CollectionUtils.isEmpty(oldbaseProduct.getVariants()));

    }
}
