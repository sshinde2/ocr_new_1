package au.com.target.tgtcore.product;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.AbstractConcurrentJaloSessionCallable;
import au.com.target.tgtcore.model.BrandModel;
import junit.framework.Assert;


@IntegrationTest
public class TargetBrandServiceIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private BrandService brandService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testBrands.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetForExistingBrand() {
        final BrandModel brand = brandService.getBrandForFuzzyName("ferrari", false);
        Assert.assertNotNull(brand);
        Assert.assertEquals("ferrari", brand.getName());
    }

    @Test
    public void testGetForNonExistingBrandAllowingCreate() {
        final BrandModel brand = brandService.getBrandForFuzzyName("dummy", true);
        Assert.assertNotNull(brand);
        Assert.assertEquals("dummy", brand.getName());
    }

    @Test
    public void testGetForNonExistingBrand() {
        final BrandModel brand = brandService.getBrandForFuzzyName("dummy", false);
        Assert.assertNull(brand);
    }

    @Test
    public void testConcurrentCreateDoesNotProduceDuplicateBrands() throws InterruptedException, ExecutionException {
        final int noOfThreads = 2;
        // barrier to ensure both threads start at exactly the same time
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final String brandName = "lambo";
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final AbstractConcurrentJaloSessionCallable<BrandModel> createBrand = new AbstractConcurrentJaloSessionCallable() {

            @Override
            public BrandModel execute() throws InterruptedException, BrokenBarrierException {
                barrier.await();
                return brandService.getBrandForFuzzyName(brandName, true);
            }
        };

        final List<Future<BrandModel>> futures = new ArrayList<>();
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(createBrand));
        }

        for (int i = 1; i < noOfThreads; i++) {
            Assert.assertEquals(futures.get(i).get(), futures.get(i - 1).get());
        }

        // if a concurrency issue does occur, a subsequent invocation would return null coz of ambiguous results
        final BrandModel brand = brandService.getBrandForFuzzyName(brandName, true);
        Assert.assertNotNull(brand);
        Assert.assertEquals(brand, futures.get(0).get());
    }

}
