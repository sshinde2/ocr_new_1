/**
 * 
 */
package au.com.target.tgtcore.config.daos.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.config.daos.TargetSharedConfigDao;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSharedConfigModel;

import com.google.common.base.Charsets;


/**
 * @author bhuang3
 * 
 */
@IntegrationTest
public class TargetSharedConfigDaoTest extends ServicelayerTransactionalTest {

    private static final String CODE = "previousPermPriceMinAge";

    @Resource
    private UserService userService;

    @Resource
    private TargetSharedConfigDao targetSharedConfigDao;


    /**
     * Initializes test cases before run.
     * 
     * @throws ImpExException
     *             if import of test data fails
     */
    @Before
    public void setUp() throws ImpExException {
        // sets the necessary user & catalog version attributes into JaloSession,
        // which are implicitly used by search restrictions service
        userService.setCurrentUser(userService.getAdminUser());
        importCsv("/tgtcore/test/testTargetSharedConfig.impex", Charsets.UTF_8.name());
    }


    @Test
    public void testGetSharedConfigByCode() throws ImpExException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSharedConfigModel result = targetSharedConfigDao.getSharedConfigByCode(CODE);
        assertEquals("14", result.getValue());
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetSharedConfigByCodeNoMatch() throws ImpExException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        targetSharedConfigDao.getSharedConfigByCode("not match");
    }




}
