package au.com.target.tgtwebcore.servicelayer.services.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.restrictions.CMSTimeRestrictionModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.SizePageModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSizeTypeRestrictionModel;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


/**
 * Integration test for {@link TargetCMSPageServiceImpl}
 * 
 */
@IntegrationTest
public class TargetCMSPageServiceImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetCMSPageService targetCMSPageService;

    @Resource
    private ModelService modelService;

    @Resource
    private CatalogVersionService catalogVersionService;

    private BrandModel brandModel;

    private PageTemplateModel pageTemplateModel;

    private CatalogVersionModel catalogVersion;

    private PageTemplateModel sizePageTemplate;

    private SizeTypeModel sizeTypeModel;

    @Before
    public void setUp() {
        brandModel = createBrand("target");
        sizeTypeModel = createSizeType("deafultTargetSize");
        setUpCatalogVersion();
        pageTemplateModel = createPageTemplate("BrandLandingPage");
        sizePageTemplate = createPageTemplate("SizePageTemplate");
    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetPageForBrandWithNoBrandPage() throws CMSItemNotFoundException {
        targetCMSPageService.getPageForBrand(brandModel);
    }

    @Test
    public void testGetPageForBrandWithSingleBrandPage() throws CMSItemNotFoundException {

        final BrandPageModel brandPage = createBrandPage("TargetBrandLandingPage");

        final BrandPageModel result = targetCMSPageService.getPageForBrand(brandModel);

        Assert.assertNotNull(result);
        Assert.assertEquals(brandPage, result);
    }

    @Test
    public void testGetPageForBrandDefaultBrandPage() throws CMSItemNotFoundException {

        final BrandPageModel defaultBrandPage = createBrandPage("DefaultBrandLandingPage");
        defaultBrandPage.setDefaultPage(Boolean.TRUE);
        modelService.save(defaultBrandPage);

        createBrandPage("AppleBrandLandingPage");
        createBrandPage("LegoBrandLandingPage");

        final BrandPageModel result = targetCMSPageService.getPageForBrand(brandModel);

        Assert.assertNotNull(result);
        Assert.assertEquals(defaultBrandPage, result);
    }

    @Test
    public void testGetPageForBrandWithRestrictedBrand() throws CMSItemNotFoundException {

        final BrandPageModel targetBrandPage = createBrandPage("TargetBrandLandingPage");
        createCMSBrandRestriction(brandModel, targetBrandPage);

        final BrandPageModel result = targetCMSPageService.getPageForBrand(brandModel);

        Assert.assertNotNull(result);
        Assert.assertEquals(targetBrandPage, result);

    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetPageForBrandWithSingleRestrictedPage() throws CMSItemNotFoundException {

        final BrandPageModel targetBrandPage = createBrandPage("TargetBrandLandingPage");
        createCMSBrandRestriction(brandModel, targetBrandPage);

        final BrandModel appleBrand = createBrand("apple");

        targetCMSPageService.getPageForBrand(appleBrand);
    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetPageForSizeTypeWithNoSizePage() throws CMSItemNotFoundException {
        targetCMSPageService.getPageForSize(sizeTypeModel);
    }

    @Test
    public void testGetPageForSizeTypeWithSingleSizePage() throws CMSItemNotFoundException {

        final SizePageModel sizePage = createSizePage("TargetSizePage");

        final SizePageModel result = targetCMSPageService.getPageForSize(sizeTypeModel);

        Assert.assertNotNull(result);
        Assert.assertEquals(sizePage, result);
    }

    @Test
    public void testGetPageForSizeWithRestrictedSizeType() throws CMSItemNotFoundException {

        final SizePageModel sizePage = createSizePage("TargetSizePage");
        createCMSSizeRestriction(sizeTypeModel, sizePage);

        final SizePageModel result = targetCMSPageService.getPageForSize(sizeTypeModel);

        Assert.assertNotNull(result);
        Assert.assertEquals(sizePage, result);

    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetPageForSizeWithSingleRestrictedPage() throws CMSItemNotFoundException {

        final SizePageModel sizePage = createSizePage("TargetSizePage");
        createCMSSizeRestriction(sizeTypeModel, sizePage);

        final SizeTypeModel sizeType = createSizeType("targetSize");

        targetCMSPageService.getPageForSize(sizeType);
    }

    @Test
    public void testGetPageForIdWithRestrictions() throws CMSItemNotFoundException {
        final String pageId = "ContentPage";

        final ContentPageModel contentPage = createContentPage(pageId, false);

        final DateTime startDate = DateTime.now().minusDays(1);
        final DateTime endDate = DateTime.now().plusDays(1);
        createCMSTimeRestriction(contentPage, startDate.toDate(), endDate.toDate());

        final ContentPageModel retrievedPage = targetCMSPageService.getPageForIdWithRestrictions(pageId);

        assertThat(retrievedPage).isEqualTo(contentPage);
    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetPageForIdWithRestrictionsNotValidYet() throws CMSItemNotFoundException {
        final String pageId = "ContentPage";

        final ContentPageModel contentPage = createContentPage(pageId, false);

        final DateTime startDate = DateTime.now().plusDays(1);
        final DateTime endDate = DateTime.now().plusDays(2);
        createCMSTimeRestriction(contentPage, startDate.toDate(), endDate.toDate());

        targetCMSPageService.getPageForIdWithRestrictions(pageId);
    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetPageForIdWithRestrictionsNoLongerValid() throws CMSItemNotFoundException {
        final String pageId = "ContentPage";

        final ContentPageModel contentPage = createContentPage(pageId, false);

        final DateTime startDate = DateTime.now().minusDays(2);
        final DateTime endDate = DateTime.now().minusDays(1);
        createCMSTimeRestriction(contentPage, startDate.toDate(), endDate.toDate());

        targetCMSPageService.getPageForIdWithRestrictions(pageId);
    }

    private void setUpCatalogVersion() {
        final CatalogModel contentCatalog = modelService.create(CatalogModel.class);
        contentCatalog.setId("content");

        catalogVersion = modelService.create(CatalogVersionModel.class);
        catalogVersion.setVersion("Staged");
        catalogVersion.setCatalog(contentCatalog);

        modelService.saveAll();

        catalogVersionService.addSessionCatalogVersion(catalogVersion);
    }

    private BrandModel createBrand(final String code) {
        final BrandModel brand = modelService.create(BrandModel.class);
        brand.setCode(code);
        brand.setName(code);
        modelService.save(brand);
        return brand;
    }

    private void createCMSBrandRestriction(final BrandModel brand, final BrandPageModel brandPage) {
        final CMSBrandRestrictionModel cmsBrandRestrictionModel = modelService.create(CMSBrandRestrictionModel.class);
        cmsBrandRestrictionModel.setBrands(Collections.singletonList(brand));

        final Collection<AbstractPageModel> pages = new ArrayList<>();
        pages.add(brandPage);

        cmsBrandRestrictionModel.setPages(pages);
        cmsBrandRestrictionModel.setUid(brand.getCode() + "-" + brandPage.getUid());
        cmsBrandRestrictionModel.setCatalogVersion(catalogVersion);

        modelService.save(cmsBrandRestrictionModel);
    }

    private BrandPageModel createBrandPage(final String uid) {
        final BrandPageModel brandPage = new BrandPageModel();
        brandPage.setUid(uid);
        brandPage.setMasterTemplate(pageTemplateModel);
        brandPage.setCatalogVersion(catalogVersion);
        modelService.save(brandPage);
        return brandPage;
    }

    private void createCMSSizeRestriction(final SizeTypeModel sizeType, final SizePageModel sizePage) {
        final CMSSizeTypeRestrictionModel cmsSizeTypeRestrictionModel = modelService
                .create(CMSSizeTypeRestrictionModel.class);
        cmsSizeTypeRestrictionModel.setSizeTypes(Collections.singletonList(sizeType));

        final Collection<AbstractPageModel> pages = new ArrayList<>();
        pages.add(sizePage);

        cmsSizeTypeRestrictionModel.setPages(pages);
        cmsSizeTypeRestrictionModel.setUid(sizeType.getCode() + "-" + sizePage.getUid());
        cmsSizeTypeRestrictionModel.setCatalogVersion(catalogVersion);

        modelService.save(cmsSizeTypeRestrictionModel);
    }

    private void createCMSTimeRestriction(final ContentPageModel page, final Date startDate, final Date endDate) {
        final CMSTimeRestrictionModel timeRestriction = modelService.create(CMSTimeRestrictionModel.class);
        timeRestriction.setActiveFrom(startDate);
        timeRestriction.setActiveUntil(endDate);

        timeRestriction.setUid(page.getUid() + "-time-restriction");
        timeRestriction.setCatalogVersion(catalogVersion);

        final List<AbstractPageModel> pages = new ArrayList<>();
        pages.add(page);

        timeRestriction.setPages(pages);

        modelService.save(timeRestriction);
    }

    private SizePageModel createSizePage(final String uid) {
        final SizePageModel sizePage = new SizePageModel();
        sizePage.setUid(uid);
        sizePage.setMasterTemplate(sizePageTemplate);
        sizePage.setCatalogVersion(catalogVersion);
        modelService.save(sizePage);
        return sizePage;
    }

    private PageTemplateModel createPageTemplate(final String uid) {
        final PageTemplateModel pageTemplate = modelService.create(PageTemplateModel.class);
        pageTemplate.setUid(uid);
        pageTemplate.setCatalogVersion(catalogVersion);
        modelService.save(pageTemplate);
        return pageTemplate;
    }

    private SizeTypeModel createSizeType(final String code) {
        final SizeTypeModel sizeType = modelService.create(SizeTypeModel.class);
        sizeType.setCode(code);
        sizeType.setName(code);
        modelService.save(sizeType);
        return sizeType;
    }

    private ContentPageModel createContentPage(final String id, final boolean defaultPage) {
        final ContentPageModel contentPage = modelService.create(ContentPageModel.class);
        contentPage.setUid(id);
        contentPage.setDefaultPage(Boolean.valueOf(defaultPage));
        contentPage.setCatalogVersion(catalogVersion);
        contentPage.setMasterTemplate(pageTemplateModel);
        modelService.save(contentPage);
        return contentPage;
    }
}
