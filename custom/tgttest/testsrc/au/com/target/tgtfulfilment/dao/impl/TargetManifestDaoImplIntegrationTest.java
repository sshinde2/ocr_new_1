package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFactory;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.util.TargetFulfilmentDaoUtil;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * Integration test for {@link TargetManifestDaoImpl}
 */
@IntegrationTest
public class TargetManifestDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    private final TargetManifestDaoImpl dao = new TargetManifestDaoImpl();

    @Resource
    private ModelService modelService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Resource
    private TargetDBSpecificQueryFactory targetDBSpecificQueryFactory;

    @Resource
    private TargetFulfilmentDaoUtil<TargetManifestModel> targetFulfilmentDaoUtil;

    private AddressModel address;

    private WarehouseModel warehouse;

    private TargetPointOfServiceModel store;

    @Before
    public void setup() throws ImpExException {
        dao.setFlexibleSearchService(flexibleSearchService);
        dao.setTargetDBSpecificQueryFactory(targetDBSpecificQueryFactory);
        dao.setTargetFulfilmentDaoUtil(targetFulfilmentDaoUtil);
        dao.setManifestHistoryDays(7);

        final VendorModel vendor = modelService.create(VendorModel.class);
        vendor.setCode("target");
        modelService.save(vendor);

        warehouse = modelService.create(WarehouseModel.class);
        warehouse.setCode("target");
        warehouse.setVendor(vendor);
        warehouse.setIntegrationMethod(IntegrationMethod.NONE);
        modelService.save(warehouse);

        store = modelService.create(TargetPointOfServiceModel.class);
        store.setWarehouses(Arrays.asList(warehouse));
        store.setName("targetStore");
        store.setStoreNumber(Integer.valueOf(1234));
        store.setType(PointOfServiceTypeEnum.TARGET);
        modelService.save(store);

        address = modelService.create(AddressModel.class);
        address.setOwner(warehouse);
        modelService.save(address);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetManifestByCodeForStoreNoManifest() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        dao.getManifestByCode("testManifest1");
    }

    @Test
    public void testGetManifestByCodeForStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetManifestModel manifest1 = modelService.create(TargetManifestModel.class);
        manifest1.setCode("testManifest1");
        modelService.save(manifest1);

        Assert.assertEquals(manifest1, dao.getManifestByCode("testManifest1"));
    }

    @Test
    public void testGetManifestsHistoryByDateNoManifests() {
        Assert.assertTrue(CollectionUtils.isEmpty(dao.getManifestsHistoryForStore(store)));
    }

    @Test
    public void testGetManifestsHistoryByDateWithManifests() {
        createManifests();

        final List<TargetManifestModel> manifestsHistory = dao.getManifestsHistoryForStore(store);
        Assert.assertFalse(CollectionUtils.isEmpty(manifestsHistory));
        Assert.assertEquals(2, manifestsHistory.size());
    }

    @Test
    public void testGetNotTransmittedManifests() {
        createNotTransmittedManifests();
        final List<TargetManifestModel> notTransmittedManifests = dao.getNotTransmittedManifests();
        Assert.assertFalse(CollectionUtils.isEmpty(notTransmittedManifests));
        Assert.assertEquals(2, notTransmittedManifests.size());
    }

    private void createNotTransmittedManifests() {
        final TargetManifestModel manifest1 = modelService.create(TargetManifestModel.class);
        manifest1.setCode("testManifestFailed1");
        manifest1.setDate(new Date());
        manifest1.setSent(false);
        modelService.save(manifest1);

        final TargetManifestModel manifest2 = modelService.create(TargetManifestModel.class);
        manifest2.setCode("testManifestFailed2");
        manifest2.setDate(new Date());
        manifest2.setSent(false);
        modelService.save(manifest2);

        final TargetManifestModel manifest3 = modelService.create(TargetManifestModel.class);
        manifest3.setCode("testManifestSuccess1");
        manifest3.setDate(new Date());
        manifest3.setSent(true);
        modelService.save(manifest3);
    }

    private void createManifests() {
        final Date currDate = new Date();

        final TargetManifestModel manifest1 = modelService.create(TargetManifestModel.class);
        manifest1.setCode("testManifestInRange1");
        manifest1.setDate(currDate);
        modelService.save(manifest1);
        createConsignment(manifest1, "cons1");

        final TargetManifestModel manifest2 = modelService.create(TargetManifestModel.class);
        manifest2.setCode("testManifestInRange2");
        manifest2.setDate(DateUtils.addDays(currDate, -5));
        modelService.save(manifest2);
        createConsignment(manifest2, "cons2");
        createConsignment(manifest2, "cons3");

        final TargetManifestModel manifest3 = modelService.create(TargetManifestModel.class);
        manifest3.setCode("testManifestNotInRange1");
        manifest3.setDate(DateUtils.addDays(currDate, -8));
        modelService.save(manifest3);
        createConsignment(manifest3, "cons4");
    }

    private void createConsignment(final TargetManifestModel manifest, final String consCode) {

        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setWarehouse(warehouse);
        consignment.setManifest(manifest);
        consignment.setStatus(ConsignmentStatus.SHIPPED);
        consignment.setCode(consCode);
        consignment.setShippingAddress(address);
        modelService.save(consignment);
    }
}
