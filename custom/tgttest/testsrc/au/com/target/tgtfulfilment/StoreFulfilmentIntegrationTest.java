/**
 * 
 */
package au.com.target.tgtfulfilment;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class StoreFulfilmentIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    private final List<StoreFulfilmentCapabilitiesModel> storeFulfilmentCapabilitiesModels = new ArrayList<>();

    private final List<ProductTypeModel> productTypeModels = new ArrayList<>();

    @Before
    public void setup() {
        // pseudo @BeforeClass
        if (storeFulfilmentCapabilitiesModels.isEmpty()) {
            final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities1 = modelService
                    .create(StoreFulfilmentCapabilitiesModel.class);
            storeFulfilmentCapabilities1.setEnabled(Boolean.TRUE);
            storeFulfilmentCapabilities1.setAllowDeliveryToThisStore(Boolean.FALSE);
            storeFulfilmentCapabilities1.setAllowDeliveryToAnotherStore(Boolean.FALSE);
            modelService.save(storeFulfilmentCapabilities1);
            final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities2 = modelService
                    .create(StoreFulfilmentCapabilitiesModel.class);
            storeFulfilmentCapabilities2.setEnabled(Boolean.TRUE);
            storeFulfilmentCapabilities2.setAllowDeliveryToThisStore(Boolean.FALSE);
            storeFulfilmentCapabilities2.setAllowDeliveryToAnotherStore(Boolean.FALSE);
            modelService.save(storeFulfilmentCapabilities2);

            storeFulfilmentCapabilitiesModels.add(storeFulfilmentCapabilities1);
            storeFulfilmentCapabilitiesModels.add(storeFulfilmentCapabilities2);
        }

        if (productTypeModels.isEmpty()) {
            final ProductTypeModel productType1 = modelService.create(ProductTypeModel.class);
            productType1.setCode("normal");
            productType1.setName("normal");
            productType1.setBulky(Boolean.FALSE);
            modelService.save(productType1);
            final ProductTypeModel productType2 = modelService.create(ProductTypeModel.class);
            productType2.setCode("bulky1");
            productType2.setName("Bulky1");
            productType2.setBulky(Boolean.TRUE);
            modelService.save(productType2);
            final ProductTypeModel productType3 = modelService.create(ProductTypeModel.class);
            productType3.setCode("bulky2");
            productType3.setName("Bulky2");
            productType3.setBulky(Boolean.FALSE);
            modelService.save(productType3);

            productTypeModels.add(productType1);
            productTypeModels.add(productType2);
            productTypeModels.add(productType3);
        }
    }

    @Test
    public void testManyToManyStoreFulfilment2ProductTypesRelation() {
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities1 = storeFulfilmentCapabilitiesModels.get(0);
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities2 = storeFulfilmentCapabilitiesModels.get(1);

        final Set<ProductTypeModel> productTypes1 = new HashSet<>();
        productTypes1.add(productTypeModels.get(0));
        productTypes1.add(productTypeModels.get(1));
        storeFulfilmentCapabilities1.setProductTypes(productTypes1);
        modelService.save(storeFulfilmentCapabilities1);

        final Set<ProductTypeModel> productTypes2 = new HashSet<>();
        productTypes2.add(productTypeModels.get(1));
        productTypes2.add(productTypeModels.get(2));
        storeFulfilmentCapabilities2.setProductTypes(productTypes2);
        modelService.save(storeFulfilmentCapabilities2);

        modelService.refresh(storeFulfilmentCapabilities1);
        modelService.refresh(storeFulfilmentCapabilities2);

        Assert.assertEquals(storeFulfilmentCapabilities1.getProductTypes().size(), 2);
        Assert.assertEquals(storeFulfilmentCapabilities2.getProductTypes().size(), 2);
    }
}
