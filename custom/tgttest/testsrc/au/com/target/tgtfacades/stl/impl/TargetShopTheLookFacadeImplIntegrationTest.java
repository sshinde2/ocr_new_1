/**
 * 
 */
package au.com.target.tgtfacades.stl.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetShopTheLookFacadeImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetShopTheLookFacade targetShopTheLookFacade;

    @Resource
    private ModelService modelService;

    @Test
    public void testGetVisibleLooksForCollection() {
        final String code = "LC01";
        final TargetLookCollectionModel collection = modelService.create(TargetLookCollectionModel.class);
        collection.setId(code);
        collection.setName("Look Collection 1");
        modelService.save(collection);

        final TargetLookModel look1 = modelService.create(TargetLookModel.class);
        look1.setId("L01");
        look1.setName("Look 1");
        look1.setEnabled(true);
        look1.setCollection(collection);
        modelService.save(look1);

        final List<TargetLookModel> looks = targetShopTheLookFacade.getVisibleLooksForCollection(code);
        Assert.assertNotNull(looks);
        Assert.assertEquals(1, looks.size());
    }

    @Test
    public void testGetVisibleLooksForCollectionMissing() {
        final List<TargetLookModel> looks = targetShopTheLookFacade.getVisibleLooksForCollection("dummy");
        Assert.assertNotNull(looks);
        Assert.assertEquals(0, looks.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetVisibleLooksForCollectionNullCode() {
        targetShopTheLookFacade.getVisibleLooksForCollection(null);
    }
}
