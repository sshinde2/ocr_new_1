/**
 * 
 */
package au.com.target.tgtfraud;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtbusproc.util.ProcessParameterTestUtil;
import au.com.target.tgtfraud.impl.FraudReportServiceImpl;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;


@IntegrationTest
public class FraudReportServiceIntTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private FraudReportServiceImpl fraudReportService;

    @Test
    public void testCreateFraudReport() {

        // Test that a FraudReportModel is created with no errors
        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);

        final TargetFraudServiceResponse response = new TargetFraudServiceResponse("ACCERTIFY");
        response.setRecommendation(RecommendationTypeEnum.REVIEW);

        final FraudReportModel report = fraudReportService.createFraudReport(response, order);

        Assert.assertNotNull(report);
        Assert.assertEquals(FraudStatus.CHECK, report.getStatus());
    }
}
