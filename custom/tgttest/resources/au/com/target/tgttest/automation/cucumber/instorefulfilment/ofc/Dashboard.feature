@dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Dashboard
  In order to track OFC orders effectively
  As a store user
  I want to see the total number of orders filtered out according to the status, for today and yesterday.

  Background: 
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | createdDate | waveDate  | pickDate  | shipDate  | cancelDate | packDate       | rejectReason          |
      | auto11000001    | CONFIRMED_BY_WAREHOUSE | older       | null      | null      | null      | null       | null           | null                  |
      | auto11000002    | CONFIRMED_BY_WAREHOUSE | yesterday   | null      | null      | null      | null       | null           | null                  |
      | auto11000003    | CONFIRMED_BY_WAREHOUSE | today       | null      | null      | null      | null       | null           | null                  |
      | auto11000004    | SHIPPED                | older       | older     | older     | older     | null       | null           | null                  |
      | auto11000005    | SHIPPED                | older       | older     | older     | yesterday | null       | null           | null                  |
      | auto11000006    | SHIPPED                | older       | older     | today     | today     | null       | null           | null                  |
      | auto11000007    | SHIPPED                | yesterday   | yesterday | yesterday | yesterday | null       | null           | null                  |
      | auto11000008    | SHIPPED                | yesterday   | yesterday | today     | today     | null       | null           | null                  |
      | auto11000009    | SHIPPED                | today       | today     | today     | today     | null       | null           | null                  |
      | auto11000010    | CANCELLED              | older       | null      | null      | null      | older      | null           | REJECTED_BY_STORE     |
      | auto11000011    | CANCELLED              | older       | older     | null      | null      | yesterday  | null           | CANCELLED_BY_CUSTOMER |
      | auto11000012    | CANCELLED              | older       | older     | null      | null      | today      | null           | PICK_FAILED           |
      | auto11000013    | CANCELLED              | yesterday   | yesterday | null      | null      | yesterday  | null           | REJECTED_BY_STORE     |
      | auto11000014    | CANCELLED              | yesterday   | today     | null      | null      | today      | null           | CANCELLED_BY_CUSTOMER |
      | auto11000015    | CANCELLED              | today       | null      | null      | null      | today      | null           | ACCEPT_TIMEOUT        |
      | auto11000016    | WAVED                  | older       | older     | null      | null      | null       | null           | null                  |
      | auto11000017    | WAVED                  | yesterday   | yesterday | null      | null      | null       | null           | null                  |
      | auto11000018    | WAVED                  | yesterday   | today     | null      | null      | null       | null           | null                  |
      | auto11000019    | WAVED                  | today       | today     | null      | null      | null       | null           | null                  |
      | auto11000020    | PICKED                 | older       | older     | older     | null      | null       | null           | null                  |
      | auto11000021    | PICKED                 | older       | yesterday | yesterday | null      | null       | null           | null                  |
      | auto11000022    | PICKED                 | older       | yesterday | today     | null      | null       | null           | null                  |
      | auto11000023    | PICKED                 | yesterday   | yesterday | today     | null      | null       | null           | null                  |
      | auto11000024    | PICKED                 | today       | today     | today     | null      | null       | null           | null                  |
      | auto11000025    | PACKED                 | older       | older     | older     | null      | null       | 3 days ago     | null                  |
      | auto11000026    | PACKED                 | older       | yesterday | yesterday | null      | null       | 1 day ago      | null                  |
      | auto11000027    | PACKED                 | older       | yesterday | today     | null      | null       | today at 09:00 | null                  |
      | auto11000028    | PACKED                 | yesterday   | yesterday | today     | null      | null       | today at 12:00 | null                  |
      | auto11000029    | PACKED                 | today       | today     | today     | null      | null       | today at 16:00 | null                  |

  Scenario: Run OFC dashboard today function for store '1000' having no consignments for today.
    And the 'ofc.dashboard.performance' feature switch is disabled
    When todays dashboard function is run for store '1000'
    Then todays dashboard stats are:
      | dashboardStatus | total |
      | open            | 0     |
      | in progress     | 0     |
      | picked          | 0     |
      | completed       | 0     |
      | rejected        | 0     |
      | packed          | 0     |
      | total           | 0     |

  Scenario: Run OFC dashboard yesterday function for store '1000' having no consignments for yesterday.
    And the 'ofc.dashboard.performance' feature switch is disabled
    When yesterdays dashboard function is run for store '1000'
    Then yesterdays dashboard stats are:
      | dashboardStatus | total |
      | open            | 0     |
      | in progress     | 0     |
      | picked          | 0     |
      | completed       | 0     |
      | rejected        | 0     |
      | packed          | 0     |
      | total           | 0     |

  Scenario: Run OFC dashboard today function for store '2000' having some consignments for today.
    And the 'ofc.dashboard.performance' feature switch is disabled
    When todays dashboard function is run for store '2000'
    Then todays dashboard stats are:
      | dashboardStatus | total | comment                                                  |
      | open            | 3     | null                                                     |
      | in progress     | 4     | null                                                     |
      | picked          | 5     | null                                                     |
      | completed       | 3     | null                                                     |
      | rejected        | 2     | auto11000014 excluded as order was cancelled by customer |
      | packed          | 5     | displays all consignments in the packed status           |
      | total           | 22    | null                                                     |

  Scenario: Run OFC dashboard yesterday function for store '2000' having some consignments for yesterday.
    And the 'ofc.dashboard.performance' feature switch is disabled
    When yesterdays dashboard function is run for store '2000'
    Then yesterdays dashboard stats are:
      | dashboardStatus | total | comment                                                  |
      | open            | 0     | null                                                     |
      | in progress     | 0     | null                                                     |
      | picked          | 0     | null                                                     |
      | completed       | 2     | null                                                     |
      | rejected        | 1     | auto11000011 excluded as order was cancelled by customer |
      | packed          | 0     | not populating                                           |
      | total           | 3     | null                                                     |
 
 
 
  Scenario: Run OFC dashboard today function for store '1000' having no consignments for today.
    And the 'ofc.dashboard.performance' feature switch is enabled
    When todays dashboard function is run for store '1000'
    Then todays dashboard stats are:
      | dashboardStatus | total |
      | open            | 0     |
      | in progress     | 0     |
      | picked          | 0     |
      | completed       | 0     |
      | rejected        | 0     |
      | packed          | 0     |
      | total           | 0     |

  Scenario: Run OFC dashboard yesterday function for store '1000' having no consignments for yesterday.
    And the 'ofc.dashboard.performance' feature switch is enabled
    When yesterdays dashboard function is run for store '1000'
    Then yesterdays dashboard stats are:
      | dashboardStatus | total |
      | open            | 0     |
      | in progress     | 0     |
      | picked          | 0     |
      | completed       | 0     |
      | rejected        | 0     |
      | packed          | 0     |
      | total           | 0     |

  Scenario: Run OFC dashboard today function for store '2000' having some consignments for today.
    And the 'ofc.dashboard.performance' feature switch is enabled
    When todays dashboard function is run for store '2000'
    Then todays dashboard stats are:
      | dashboardStatus | total | comment                                                  |
      | open            | 3     | null                                                     |
      | in progress     | 4     | null                                                     |
      | picked          | 5     | null                                                     |
      | completed       | 3     | null                                                     |
      | rejected        | 2     | auto11000014 excluded as order was cancelled by customer |
      | packed          | 5     | displays all consignments in the packed status           |
      | total           | 22    | null                                                     |

  Scenario: Run OFC dashboard yesterday function for store '2000' having some consignments for yesterday.
    And the 'ofc.dashboard.performance' feature switch is enabled
    When yesterdays dashboard function is run for store '2000'
    Then yesterdays dashboard stats are:
      | dashboardStatus | total | comment                                                  |
      | open            | 0     | null                                                     |
      | in progress     | 0     | null                                                     |
      | picked          | 0     | null                                                     |
      | completed       | 2     | null                                                     |
      | rejected        | 1     | auto11000011 excluded as order was cancelled by customer |
      | packed          | 0     | not populating                                           |
      | total           | 3     | null                                                     |
      