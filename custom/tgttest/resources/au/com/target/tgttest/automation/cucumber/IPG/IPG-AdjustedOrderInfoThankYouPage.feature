@notAutomated
Feature: IPG - Display adjusted order information on order completion
  In order to understand adjusted order information
  As an online customer
  I want to see adjusted order information upon order completion

  Background: 
    Given IPG payment feature is on

  Scenario Outline: Display adjusted order information on order completion with single product in cart
    Given the customer has added <QuantityAdded> from product <Product>
    And submit payment from IPG iFrame
    And stock of product <Product> changes to <QuantityAvailable>
    When order completion page is loaded
    Then adjusted order information message is displayed
    And payment for product <Product> is calculated for <QuantityAvailable>

    Examples: 
      | Product  | QuantityAdded | QuantityAvailable |
      | 10000011 | 5             | 3                 |

  Scenario: Display adjusted order information on order completion with multiple products in cart
    Given cart with entries
      | product  | quanity |
      | 10000011 | 5       |
      | 10000111 | 5       |
    And submit payment from IPG iFrame
    And stock available during payment is
      | product  | quanity |
      | 10000011 | 5       |
      | 10000111 | 0       |
    Then order completion page is loaded
    And adjusted order information message is displayed
    And order has products
      | product  | quantity |
      | 10000011 | 5        |

  Scenario: Continue to shop link available with adjusted order message
    Given order completion page is loaded
    And adjusted stock message is available
    When customer clicks on 'Continue Shopping'
    Then customer is redirected to home page
