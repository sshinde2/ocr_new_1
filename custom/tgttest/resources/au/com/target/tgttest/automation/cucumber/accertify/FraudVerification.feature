@cartProductData
Feature: Accertify fraud checking
  In order to avoid financial loss, As Target we want to check orders for fraud.
  
  When an order is placed, a request is sent to the third party Accertify service,
  and the order status is updated according to the response.

  Background: 
    Given feature 'accertify.include.billingAddress' is switched 'on'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |

  Scenario Outline: Accertify response is Review.
    Given the order would trigger an Accertify response of 'REVIEW'
    And payment method is '<payment_method>'
    When order is placed
    Then order is in status 'REVIEW'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 1        | 10        |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Accertify response is Reject.
    Given the order would trigger an Accertify response of 'REJECT'
    And payment method is '<payment_method>'
    When order is placed
    Then order is in status 'REJECTED'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Accertify response is Accept.
    Given the order would trigger an Accertify response of 'ACCEPT'
    And payment method is '<payment_method>'
    When order is placed
    Then order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 1        | 10        |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |
