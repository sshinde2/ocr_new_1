@notAutomated
Feature: IPG - Transition Page to Thank you page
  As an online customer
  I want to know that I am being redirected to the Thank You page
  So that I know the payment is in progress and do not try to exit the flow

  Scenario: Display iFrame thinking page when redirecting from IPG
    Given the customer has entered valid credit card details into the iFrame
    When the customer submits their payment
    Then an iFrame thinking page is displayed

  Scenario: iFrame thinking page displays waiting message and display loading animation
    Given the customer has successfully submitted payment
    When an iFrame thinking page is displayed
    Then "We're processing your payment. Please do not close or leave this page until complete." message is displayed
    And a loading animation is displayed

  Scenario: Navigating away from review page before redirect will not take payment
    Given the customer has submitted a payment in the iFrame
    And the customer is still on the review page
    When the customer navigates away from the review page/refresh
    Then payment will not be taken
    And the customer will continue with navigation/refresh

  Scenario: Payment is only taken once when customer refreshes transition page
    Given the customer has been redirected to the transition page
    And the payment is successful
    When the customer refreshes the transition page
    Then payment will not be taken again
    And the customer will remain on the transition page
