@cartProductData @cleanOrderData @cleanConsignmentData
Feature: Full Cancellation - add rejection reason for order cancelled by CS agents
   As online customer service agent
   I want to add a rejection reason when orders are cancelled on behalf of a customer in CS Cockpit
   So that I can use the reason to filter customer-cancelled orders from view in the online fulfilment centre

  Scenario Outline: Customer Service agent fully cancels an order assigned to store
    Given a consignment sent to a store
    When the order is fully cancelled with the reason '<cancelReason>'
    Then consignment status is 'Cancelled'
    And reject reason is 'Cancelled_By_Customer'

    Examples: 
      | cancelReason       |
      | FRAUDCHECKREJECTED |
      | OUTOFSTOCK         |
      | CUSTOMERREQUEST    |

  Scenario Outline: Customer Service agent fully cancels an order assigned to fastline
    Given a consignment is sent to fastline
    When the order is fully cancelled with the reason '<cancelReason>'
    Then consignment status is 'Cancelled'
    And reject reason is 'Cancelled_By_Customer'

    Examples: 
      | cancelReason       |
      | CUSTOMERREQUEST    |
      | OUTOFSTOCK         |
      | RECALL             |

  Scenario: Full cancellation due to Zero Pick
    Given a consignment is sent to fastline
    When the order is zero picked
    Then consignment status is 'Cancelled'
    And reject reason is 'Zero_Pick_By_Warehouse'
    And no cockpit group is assigned
