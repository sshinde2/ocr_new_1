@postCodeGroupData @cartProductData @deliveryModeData
Feature: Restrict Delivery fee value by post code catchment area

  In order to restrict delivery fee
  As a customer I want to enter post code in the text field to get the exact delivery fee value

  Notes:
  * Products are setup as:
       product   productType
       10000011  normal
       10000411  bulky

  Background: 
    Given Postcodes allocated for the catchment area and also delivery fee set up based on product type and quantity.
      | postCode | catchmentArea | productType | qty       | deliveryFee | lowestDeliveryFee |
      | 3002     | auto-Metro    | bulky1      | 1-2       | $15         |                   |
      | 3003     | auto-Metro    | bulky1      | 3 or more | $30         |                   |
      | 3222     | auto-Regional | bulky1      | 1-2       | $25         |                   |
      | 3223     | auto-Regional | bulky1      | 3 or more | $50         |                   |
      | 4875     | auto-Rural    | bulky1      | 1-2       | $45         |                   |
      | 4876     | auto-Rural    | bulky1      | 3 or more | $80         |                   |
      | n/a      | n/a           | normal      | 1 or more | $9          |                   |
      | n/a      | n/a           | bulky1      | 1-2       | $59         | $15               |
      | n/a      | n/a           | bulky1      | 3 or more | $79         | $30               |

  Scenario Outline: Customer with a post code and different types of products in a cart to verify the delivery fee
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    And the postcode allocated for the cart is '<postCode>'
    When the delivery fee for the cart is fetched
    Then delivery fee displayed for the home delivery mode is '<homeDeliveryFee>'

    Examples: 
      | productsInCart    | qty | postCode | homeDeliveryFee |
      | 10000011          | 2   | 3001     | $9              |
      | 10000411          | 1   | 3002     | $15             |
      | 10000411          | 3   | 3003     | $30             |
      | 10000011          | 4   | 3221     | $9              |
      | 10000411          | 2   | 3222     | $25             |
      | 10000411          | 7   | 3223     | $50             |
      | 10000411          | 10  | 4874     | $80             |
      | 10000411          | 1   | 4875     | $45             |
      | 10000411          | 5   | 4876     | $80             |
      | 10000011,10000411 | 1,1 | 3002     | $15             |
      | 10000011,10000411 | 4,3 | 3003     | $30             |
      | 10000011,10000411 | 5,1 | 3222     | $25             |
      | 10000011,10000411 | 2,5 | 3223     | $50             |
      | 10000011,10000411 | 3,3 | 4875     | $80             |
      | 10000011,10000411 | 6,6 | 4876     | $80             |
      | 10000011          | 5   | 1046     | $9              |
      | 10000411          | 6   | 2024     | $79             |

  Scenario Outline: : As a customer with products in basket I want to know whether a delivery mode has a fee range.
    And a cart with product '<productsInCart>' and with quantity '<qty>'
    And the postcode allocated for the cart is '<postCode>'
    When delivery modes for the cart is fetched
    Then delivery fee for '<deliveryMode>' is '<homeDeliveryFee>' and it has '<hasRange>'

    Examples: 
      | productsInCart    | qty | postCode | deliveryMode  | hasRange     | homeDeliveryFee |
      | 10000011          | 2   | 3001     | home-delivery | no fee range | $9              |
      | 10000411          | 1   | 3002     | home-delivery | no fee range | $15             |
      | 10000411          | 3   | 3003     | home-delivery | no fee range | $30             |
      | 10000011          | 4   | 3221     | home-delivery | no fee range | $9              |
      | 10000411          | 2   | 3222     | home-delivery | no fee range | $25             |
      | 10000411          | 7   | 3223     | home-delivery | no fee range | $50             |
      | 10000411          | 10  | 4874     | home-delivery | no fee range | $80             |
      | 10000411          | 1   | 4875     | home-delivery | no fee range | $45             |
      | 10000411          | 5   | 4876     | home-delivery | no fee range | $80             |
      | 10000011,10000411 | 1,1 | 3002     | home-delivery | no fee range | $15             |
      | 10000011,10000411 | 4,3 | 3003     | home-delivery | no fee range | $30             |
      | 10000011,10000411 | 5,1 | 3222     | home-delivery | no fee range | $25             |
      | 10000011,10000411 | 2,5 | 3223     | home-delivery | no fee range | $50             |
      | 10000011,10000411 | 3,3 | 4875     | home-delivery | no fee range | $80             |
      | 10000011,10000411 | 6,6 | 4876     | home-delivery | no fee range | $80             |
      | 10000011          | 5   | 1046     | home-delivery | no fee range | $9              |
      | 10000411          | 6   | 2024     | home-delivery | no fee range | $79             |
      | 10000411          | 1   |          | home-delivery | fee range    | $15             |
      | 10000411          | 3   |          | home-delivery | fee range    | $30             |
      | 10000411          | 2   |          | home-delivery | fee range    | $15             |
      | 10000411          | 7   |          | home-delivery | fee range    | $30             |
      | 10000411          | 10  |          | home-delivery | fee range    | $30             |
      | 10000411          | 1   |          | home-delivery | fee range    | $15             |
      | 10000411          | 5   |          | home-delivery | fee range    | $30             |
      | 10000011,10000411 | 1,1 |          | home-delivery | fee range    | $15             |
      | 10000011,10000411 | 4,3 |          | home-delivery | fee range    | $30             |
      | 10000011,10000411 | 5,1 |          | home-delivery | fee range    | $15             |
      | 10000011,10000411 | 2,5 |          | home-delivery | fee range    | $30             |
      | 10000011,10000411 | 3,3 |          | home-delivery | fee range    | $30             |
      | 10000011,10000411 | 6,6 |          | home-delivery | fee range    | $30             |
      | 10000411          | 6   |          | home-delivery | fee range    | $30             |
      | 10000011          | 2   |          | home-delivery | no fee range | $9              |
      | 10000011          | 5   |          | home-delivery | no fee range | $9              |
      | 10000011          | 4   |          | home-delivery | no fee range | $9              |
