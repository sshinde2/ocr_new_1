@notAutomated
Feature: Outfits - WCMSCollections
  In order to have better shopping experience for customers As a CMS user I want to create and maintain a Collection as a set of Looks in WCMS

  Scenario: Adding Collection to top level Category
    Given the collection with following details
      | name             | title             | description           |
      | SummerCollection | Summer Collection | Latest Summer Outfits |
    When the Collection gets added for the category topLevelCategory 'women'
    Then the new Collection is visible among the set of Collections in the WCMS

  Scenario Outline: Adding new looks to Collection
    Given the Collection with name 'Spring Collection'
    And the current Looks in the Collection are <currentLooks>
    When new look  <addedLook>  is added to the Collection in the  position <lookPosition>
    And provides a Look Id/Name
    And provides a Hero image for the Look
    And sets the Look to enabled or disabled
    Then the looks in the Collection are now <updatedLooks>

    Examples: 
      | currentLooks       | addedLook | lookPosition | updatedLooks            |
      | none               | Look1     | 1            | Look1                   |
      | Look1              | Look2     | 2            | Look1,Look2             |
      | Look1, Look2       | Look3     | 1            | Look3,Look2,Look1       |
      | Look3, Look2,Look1 | Look4     | 3            | Look3,Look1,Look4,Look2 |

  Scenario Outline: Changing order of Looks in Collection
    Given the Collection with name 'Spring Collection'
    And the current Looks in the Collection are <currentLooks>
    When the position  of Look <updatedLook> is updated with <newposition> in the Collection
    Then the Looks in the Collection are now <updatedLooks>

    Examples: 
      | currentLooks        | updatedLook | newPosition | updatedLooks        |
      | Look1, Look2        | Look1       | 2           | Look2,Look1         |
      | Look1, Look2, Look3 | Look3       | 1           | Look3, Look1, Look2 |

  Scenario Outline: Removing Look from Collection
    Given the Collection with name 'Spring Collection'
    And the current Looks in the Collection are <currentLooks>
    When the user removes Look <removedLook> from the Collection
    Then the Looks in the Collection are now <updatedLooks>

    Examples: 
      | currentLooks        | removedLook | updatedLooks |
      | Look1, Look2, Look3 | Look2       | Look1, Look3 |
      | Look1, Look3        | Look3       | Look1        |
      | Look1               | Look1       | none         |

  Scenario: Hide Look in Collection
    Given the Collection with name 'SpringCollection'
    When the visibility of the Look is set  to 'false'
    Then the look is 'invisible' for the collection

  Scenario: Removing Collection
    Given the Collection with name 'SpringCollection'
    And the the current Looks in the Collection are 'Look1,Look2'
    When the collection is removed
    Then the Collection and any contained Looks are removed

  Scenario: Hide Collection in Category
    Given the top level category 'women' with collection navigationNode 'Summerlook'
    When the visibility of the navigationNode is set as 'false'
    Then the Collection is  'invisible'

  Scenario: Updating Collection attributes
    Given the Collection with name 'SpringCollection'
    When collection title is updated as 'New title'
    And the collection description as 'New Description'
    And the new hero image is added
    Then the new title,descption and image is retrieved for the collection

  Scenario: Updating Look attributes
    Given the look with name 'SummerLook'
    When look title is updated as 'New title'
    And the look description as 'New Description'
    Then the new title,descption and image is retrieved for the look
