@cartProductData @currencyConversionFactorsData
Feature: Import orders from channel advisor
  In order to fulfil order from various sales channels
  As Target Online
  I want to import orders from channel advisor.

  Scenario Outline: Order imported for different sales channels.
    Given a new order in webmethods with:
      | Sales Channel  | Payment Type  | Currency   | Country   | Fulfillment Type  | StoreNumber   | Delivery Address  |
      | <salesChannel> | <paymentType> | <currency> | <country> | <fulfillmentType> | <storeNumber> | <deliveryAddress> |
    When the order is imported in hybris
    Then a new order in hybris is created
    And sales channel is set to '<orderSalesChannel>'
    And currency is set to '<currency>'
    And country is set to '<orderCountry>'
    And delivery mode is set to '<orderDeliverMode>'
    And delivery address is '<orderDeliveryAddress>'
    And order store is set to '<orderStoreNumber>'
    And total tax is set to '<totalTax>'
    And tax group is set to '<taxGroup>'
    And payment mode is '<paymentInfoModel>'
    And tlog currency is 'AUD'
    And tlog sales channel is '<orderSalesChannel>'
    And all items in order extract will have the Allows short pick flag set to '<allowShortPick>'
    And carrier id in order extract is '<carrier>' and carrierService in order extract is '<carrierService>'
    And partner inventory update should be '<triggerdstatus>'

    Examples: 
      | salesChannel | paymentType | fulfillmentType | currency | country | storeNumber | deliveryAddress                             | orderSalesChannel | orderCountry | orderDeliverMode       | orderStoreNumber | orderDeliveryAddress                        | totalTax | taxGroup | paymentInfoModel | carrier | carrierService | allowShortPick | triggerdstatus |
      | eBay         | PayPal      | Ship            | AUD      | AU      | Null        | 11,Coleman Street,Koonibba,SA,5690          | eBay              | AU           | eBay-delivery          | Null             | 11,Coleman Street,Koonibba,SA,5690          | notZero  | notEmpty | paypal           | AP      | S1             | Y              | not triggered  |
      | eBay         | PayPal      | ShipToStore     | AUD      | AU      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 | eBay              | AU           | ebay-click-and-collect | 7126             | 19,Robina Town Centre Drive,Robina,QLD,4226 | notZero  | notEmpty | paypal           | TOLL    | X              | Y              | triggered      |
      | eBay         | PayPal      | Pickup          | AUD      | AU      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 | eBay              | AU           | ebay-click-and-collect | 7126             | 19,Robina Town Centre Drive,Robina,QLD,4226 | notZero  | notEmpty | paypal           | TOLL    | X              | Y              | triggered      |
      | eBay         | PayPal      | Null            | AUD      | AU      | Null        | 11,Coleman Street,Koonibba,SA,5690          | eBay              | AU           | eBay-delivery          | Null             | 11,Coleman Street,Koonibba,SA,5690          | notZero  | notEmpty | paypal           | AP      | S1             | Y              | not triggered  |
      | TradeMe      | TradeMe     | Ship            | NZD      | NZ      | Null        | 7,Waterloo Quay,pipitea,Wellington,6011     | TradeMe           | NZ           | trademe-delivery       | Null             | 7,Waterloo Quay,pipitea,Wellington,6011     | zero     | empty    | paynow           | AP      | ECM8           | N              | not triggered  |

  Scenario: Order imported with spaces in the email address
    Given a new order in webmethods with:
      | Sales Channel  | Payment Type  | Currency   | Country   | Fulfillment Type  | StoreNumber   | Delivery Address  |
      | eBay | PayPal | AUD | AU | Ship | Null | 11,Coleman Street,Koonibba,SA,5690 |
    And an email address of '     test@email.com     '
    When the order is imported in hybris
    Then a new order in hybris is created
    And sales channel is set to 'eBay'
    And currency is set to 'AUD'
    And country is set to 'AU'
    And delivery mode is set to 'eBay-delivery'
    And delivery address is '11,Coleman Street,Koonibba,SA,5690'
    And order store is set to 'Null'
    And total tax is set to 'notZero'
    And tax group is set to 'notEmpty'
    And payment mode is 'paypal'
    And tlog currency is 'AUD'
    And tlog sales channel is 'eBay'
    And all items in order extract will have the Allows short pick flag set to 'Y'
    And carrier id in order extract is 'AP' and carrierService in order extract is 'S1'
    And partner inventory update should be 'not triggered'
    And order email address is 'test@email.com'

  Scenario Outline: Order imported with invalid details should fail
    Given a new order in webmethods with:
      | Sales Channel  | Payment Type  | Currency   | Country   | Fulfillment Type  | StoreNumber   | Delivery Address  |
      | <salesChannel> | <paymentType> | <currency> | <country> | <fulfillmentType> | <storeNumber> | <deliveryAddress> |
    When the order is imported in hybris
    Then a new order in hybris is not created

    Examples: 
      | salesChannel | paymentType | fulfillmentType | currency | country | storeNumber | deliveryAddress                             |
      | eBay         | PayPal      | ShipToStore     | AUD      | AU      | Null        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | Invalid         | AUD      | AU      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | Pickup          | AUD      | AU      | Null        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | ShipToStore     | AUD      | AU      | 234         | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | Invalid      | PayPal      | ShipToStore     | AUD      | AU      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | ShipToStore     | ABC      | AU      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | ShipToStore     | AUD      | US      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | Ship            | AUD      | Null    | Null        | 19,Robina Town Centre Drive,Robina,QLD,4226 |
      | eBay         | PayPal      | ShipToStore     | AUD      | @$      | 7126        | 19,Robina Town Centre Drive,Robina,QLD,4226 |

  Scenario: Order imported without sales channel and currency, default values should be used.
    Given a new order in webmethods
    And country as 'AU'
    And payment type is 'PayPal'
    When the order is imported in hybris
    Then a new order in hybris is created
    And sales channel is set to 'eBay'
    And currency is set to 'AUD'
    And delivery mode is set to 'eBay-delivery'
    And total tax is set to 'notZero'
    And tax group is set to 'notEmpty'
    And payment mode is 'paypal'
    And tlog currency is 'AUD'
    And tlog sales channel is 'eBay'

  Scenario Outline: Order imported for valid sales channel, with phone number validation
    Given a new order in webmethods
    And sales channel as '<webMethodsSalesChannel>'
    And country as '<country>'
    And payment type is 'PayPal'
    And the phone number is '<phoneNumber>'
    When the order is imported in hybris
    Then a new order in hybris is created
    And the phone number is stored as '<savedNumber>'

    Examples: 
      | 3rdPartyOrderId | webMethodsSalesChannel | country | phoneNumber       | savedNumber       | comment                                               |
      | TP001           | TradeMe                | NZ      | +64912345678      | +64912345678      |                                                       |
      | TP002           | TradeMe                | NZ      | (09) 1234-5678    | (09) 1234-5678    |                                                       |
      | TP003           | TradeMe                | NZ      | +64 9 1234-5678   | +64 9 1234-5678   |                                                       |
      | TP004           | TradeMe                | NZ      | 64 9 1234-5678    | 64 9 1234-5678    |                                                       |
      | TP005           | TradeMe                | AU      | +61312345678      | +61312345678      | Australian phone number on Trade Me order.            |
      | TP006           | TradeMe                | AU      | (03) 1234-5678    | (03) 1234-5678    | Australian phone number on Trade Me order.            |
      | TP007           | eBay                   | AU      | +61312345678      | +61312345678      | eBay order.                                           |
      | TP008           | eBay                   | AU      | (03) 1234-5678    | (03) 1234-5678    | eBay order.                                           |
      | TP009           | TradeMe                | NZ      | 12345678          | 12345678          | 8 digits.                                             |
      | TP009a          | eBay                   | NZ      | 1234567           | 1234567           | Less than 8 digits. Log error. Still import order.    |
      | TP010           | eBay                   | AU      | 123456789-123456  | 123456789-123456  | 16 digits.                                            |
      | TP010a          | eBay                   | AU      | 123456789-1234567 | 123456789-1234567 | More than 16 digits. Log error. Import order.         |
      | TP011           | eBay                   | NZ      | 0123 4&5 6789     | 0123 4&5 6789     | Contains invalid characters. Log error. Import order. |
      | TP011a          | eBay                   | AU      | 12345678          | 12345678          | Less than 9 digits. Log error. Still import order.    |
      | TP012           | TradeMe                | NZ      | 123456789         | 123456789         | 9 digits.                                             |

  Scenario Outline: Order imported for different sales channels.
    Given a new order in webmethods with order id '<orderId>'
    And sales channel as '<salesChannel>'
    And payment type is 'PayPal'
    And currency as 'AUD'
    And country as 'AU'
    When the order is imported in hybris
    Then a new order in hybris is <orderCreation>

    Examples: 
      | orderId | salesChannel | orderCreation |
      | 400001  | eBay         | created       |
      | 400002  | TradeMe      | created       |
      | 400001  | eBay         | not created   |
      | 400002  | TradeMe      | not created   |
      | 400001  |              | not created   |
      | 400002  |              | created       |
      | 400002  |              | not created   |

  Scenario: Order Extract contains the address details.
    Given a new order in webmethods
    And sales channel as 'TradeMe'
    And payment type is 'TradeMe'
    And currency as 'NZD'
    And delivery address details are:
      | addressLine1    | town           | postalCode | region     | country |
      | 14 Emert street | Wentworthville | 5013       | Wellington | NZ      |
    When the order is imported in hybris
    Then a new order in hybris is created
    And order extract address details are:
      | addressLine1    | town           | postalCode | region     | country     |
      | 14 Emert street | WENTWORTHVILLE | 5013       | Wellington | NEW ZEALAND |

  Scenario Outline: Order Extract contains the address details for Ebay orders with different delivery modes.
    Given a new order in webmethods
    And sales channel as 'eBay'
    And payment type is 'PayPal'
    And currency as 'AUD'
    And delivery address details are:
      | Address Line1               | Town   | Postal Code | Region | Country | Shipping Class  |
      | 19 Robina Town Centre Drive | Robina | 4226        | QLD    | AU      | <shippingClass> |
    When the order is imported in hybris
    Then a new order in hybris is created
    And delivery mode is set to '<orderDeliverMode>'

    Examples: 
      | shippingClass | orderDeliverMode      |
      | STD           | eBay-delivery         |
      | EXP           | ebay-express-delivery |

  Scenario Outline: Store exchange rate (Trade Me only) and 3rd party order date/time on Channel Advisor orders.
    Given a new order in webmethods with:
      | Sales Channel  | Payment Type  | Currency   | Country   | Created Date  | Delivery Address  |
      | <salesChannel> | <paymentType> | <currency> | <country> | <createdDate> | <deliveryAddress> |
    When the order is imported in hybris
    Then a new order in hybris is created
    And date is set to '<orderDate>'
    And exchange rate is set to '<exchangeRate>'

    Examples: 
      | salesChannel | paymentType | currency | country | createdDate         | orderDate           | exchangeRate | comment                                       | deliveryAddress                         |
      | TradeMe      | TradeMe     | NZD      | NZ      | 2015-12-08T04:05:00 | 2015-12-08T04:05:00 | 1.08971      | valid TradeMe order                           | 7,Waterloo Quay,pipitea,Wellington,6011 |
      | TradeMe      | TradeMe     | NZD      | NZ      | 2015-12-08T05:05:00 | 2015-12-08T05:05:00 | 1.08971      | valid TradeMe order                           | 7,Waterloo Quay,pipitea,Wellington,6011 |
      | TradeMe      | PayPal      | NZD      | NZ      | 2015-12-09T04:05:00 | 2015-12-09T04:05:00 | 1.0889       | cross values order for TradeMe but acceptable | 7,Waterloo Quay,pipitea,Wellington,6011 |
      | TradeMe      | TradeMe     | AUD      | AU      | 2015-12-09T05:05:00 | 2015-12-09T05:05:00 | null         | cross values order for TradeMe but acceptable | 7,Waterloo Quay,pipitea,Wellington,6011 |
      | eBay         | TradeMe     | NZD      | NZ      | 2015-12-08T05:05:00 | 2015-12-08T05:05:00 | null         | cross values order for eBay but acceptable    | 7,Waterloo Quay,pipitea,Wellington,6011 |
      | eBay         | TradeMe     | AUD      | AU      | 2015-12-10T05:05:00 | 2015-12-10T05:05:00 | null         | cross values order for eBay but acceptable    | 7,Waterloo Quay,pipitea,Wellington,6011 |
      | eBay         | PayPal      | AUD      | AU      | 2015-12-10T05:05:00 | 2015-12-10T05:05:00 | null         | valid eBay order                              | 7,Waterloo Quay,pipitea,Wellington,6011 |
