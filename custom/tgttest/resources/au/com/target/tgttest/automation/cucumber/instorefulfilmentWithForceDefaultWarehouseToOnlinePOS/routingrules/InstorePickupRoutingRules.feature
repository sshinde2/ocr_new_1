@cartProductData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Instore Pickup CnC Routing Rules
  As the online store, I want to be able to apply routing rules to a consignment, so that I can identify which warehouse(s) could fulfil a given consignment
  
  If an order is click-and-collect or ebay-click-and-collect, then routing rules will check if it is eligible for instore pickup.
  
  a) The order is click and collect
  b) The store is enabled for in-store delivery
  ie allows click-and-collect mode and allowDeliveryToSameStore setting
  c) Store has enough stock to fulfil the entire order
  d) Store blackout is not in place
  
  If an order does not satisfy all criteria, it will not be eligible for instore pickup.
  
  Note: by default the global rules allow instore pickup orders.

  Background: 
    Given stores with fulfilment capability and stock:
      | store | instoreEnabled | inStock | deliveryModesAllowed                     | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | 7126  | Yes            | Yes     | click-and-collect,ebay-click-and-collect | Yes                      | No                          |
      | 7049  | Yes            | No      | click-and-collect,ebay-click-and-collect | Yes                      | No                          |
      | 7032  | Yes            | Yes     | home-delivery,eBay-delivery              | No                       | No                          |
      | 7043  | Yes            | Yes     | click-and-collect,ebay-click-and-collect | No                       | No                          |
      | 7001  | No             | na      | na                                       | na                       | na                          |
      | 7037  | Yes            | Yes     | click-and-collect                        | Yes                      | No                          |
    And any cart
    And delivery modes allowed for instore fulfilment are 'click-and-collect,ebay-click-and-collect,home-delivery,eBay-delivery'
    
  Scenario Outline: CnC orders placed against stores with different fulfilment capabilities
    Given order cnc store is '<cnc_store>'
    And delivery mode is '<deliveryMode>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | cnc_store | warehouse | deliveryMode           | comment                                         |
      | 7126      | Robina    | click-and-collect      | Store can fulfil                                |
      | 7049      | Fastline  | click-and-collect      | Not enough stock                                |
      | 7032      | Fastline  | click-and-collect      | Delivery mode not allowed                       |
      | 7001      | Fastline  | click-and-collect      | Store not instore enabled                       |
      | 7043      | Fastline  | click-and-collect      | Not allowDeliveryToSameStore                    |
      | 7126      | Robina    | ebay-click-and-collect | Store can fulfil                                |
      | 7037      | Fastline  | ebay-click-and-collect | ebay-click-and-collect deliveryMode not allowed |

  Scenario Outline: Cnc orders placed against store depending on store blackout config
    Given order cnc store is '7126'
    And delivery mode is '<deliveryMode>'
    And Store-wide blackout start time is '<storeBlackoutStart>' and end time is '<storeBlackoutEnd>' for store '7126'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | storeBlackoutStart | storeBlackoutEnd | deliveryMode           | warehouse | comment                       |
      | NOT POPULATED      | AFTER NOW        | click-and-collect      | Fastline  | Store blackout current        |
      | EXACTLY NOW        | AFTER NOW        | click-and-collect      | Fastline  | Store blackout current        |
      | AFTER NOW          | AFTER NOW        | click-and-collect      | Robina    | Store blackout in future      |
      | AFTER NOW          | NOT POPULATED    | click-and-collect      | Robina    | Store blackout in future      |
      | NOT POPULATED      | NOT POPULATED    | click-and-collect      | Robina    | Store blackout not configured |
      | BEFORE NOW         | BEFORE NOW       | click-and-collect      | Robina    | Store blackout in past        |
      | BEFORE NOW         | NOT POPULATED    | click-and-collect      | Fastline  | Store blackout current        |
      | NOT POPULATED      | BEFORE NOW       | click-and-collect      | Robina    | Store Blackout in past        |
