@cartProductData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Automatic re-routing of non-picked orders
  
  In order to timely fulfil the orders
  As the online store
  I want to automatically timeout an accepted order that has not been picked by a store within the set expiry period
  
  As part of this feature if order is not acknowledged by the store.
  a) the consignment associated with the store needs to be Cancelled.
  b) a reason needs to be set on the consignment
  c) a new consignment needs to be created with warehouse as fastline

  Scenario: Non picked orders are re routed to another warehouse if they are not picked within accepted orders timeout period.
    Given some existing orders routed to store:
      | order       | warehouse   | consignmentStatus      | minutesSinceWaved |
      | auto1000001 | Robina      | Waved                  | 181               |
      | auto1000002 | Robina      | Waved                  | 180               |
      | auto1000003 | Robina      | Waved                  | 179               |
      | auto1000004 | Robina      | CONFIRMED_BY_WAREHOUSE | N/A               |
      | auto1000005 | Robina      | Shipped                | 181               |
      | auto1000006 | Robina      | Cancelled              | 181               |
      | auto1000007 | Rockhampton | Waved                  | 181               |
      | auto1000008 | Rockhampton | Waved                  | 179               |
      | auto1000009 | Fastline    | SENT_TO_WAREHOUSE      | N/A               |
      | auto1000010 | Fastline    | CONFIRMED_BY_WAREHOUSE | N/A               |
      | auto1000011 | Fastline    | Picked                 | N/A               |
      | auto1000012 | Fastline    | Shipped                | N/A               |
      | auto1000013 | Fastline    | Cancelled              | N/A               |
    And accepted orders timeout period is 180 minutes
    When accepted orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus   | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000001 | Cancelled              | PICK_TIMEOUT         | SENT_TO_WAREHOUSE    | Fastline                |
      | auto1000002 | Waved                  | NULL                 | N/A                  | Robina                  |
      | auto1000003 | Waved                  | NULL                 | N/A                  | Robina                  |
      | auto1000004 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Robina                  |
      | auto1000005 | Shipped                | NULL                 | N/A                  | Robina                  |
      | auto1000006 | Cancelled              | NULL                 | N/A                  | Robina                  |
      | auto1000007 | Cancelled              | PICK_TIMEOUT         | SENT_TO_WAREHOUSE    | Fastline                |
      | auto1000008 | Waved                  | NULL                 | N/A                  | Rockhampton             |
      | auto1000009 | SENT_TO_WAREHOUSE      | NULL                 | N/A                  | Fastline                |
      | auto1000010 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Fastline                |
      | auto1000011 | Picked                 | NULL                 | N/A                  | Fastline                |
      | auto1000012 | Shipped                | NULL                 | N/A                  | Fastline                |
      | auto1000013 | Cancelled              | NULL                 | N/A                  | Fastline                |

  Scenario: Non picked orders are not re routed to another warehouse accepted orders timeout period is set to ZERO.
    Given some existing orders routed to store:
      | order       | warehouse   | consignmentStatus | minutesSinceWaved |
      | auto1000015 | Robina      | Waved             | 1                 |
      | auto1000016 | Rockhampton | Waved             | 181               |
      | auto1000017 | Fastline    | SENT_TO_WAREHOUSE | N/A               |
    And accepted orders timeout period is 0 minutes
    When accepted orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000015 | Waved                | NULL                 | N/A                  | Robina                  |
      | auto1000016 | Waved                | NULL                 | N/A                  | Rockhampton             |
      | auto1000017 | SENT_TO_WAREHOUSE    | NULL                 | N/A                  | Fastline                |

  Scenario: Non picked orders are not re routed to another warehouse accepted orders timeout period is not configured.
    Given some existing orders routed to store:
      | order       | warehouse   | consignmentStatus      | minutesSinceWaved |
      | auto1000018 | Robina      | Waved                  | 60                |
      | auto1000019 | Rockhampton | Waved                  | 1                 |
      | auto1000020 | Fastline    | CONFIRMED_BY_WAREHOUSE | N/A               |
    And accepted orders timeout period is not configured
    When accepted orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus   | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000018 | Waved                  | NULL                 | N/A                  | Robina                  |
      | auto1000019 | Waved                  | NULL                 | N/A                  | Rockhampton             |
      | auto1000020 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Fastline                |
