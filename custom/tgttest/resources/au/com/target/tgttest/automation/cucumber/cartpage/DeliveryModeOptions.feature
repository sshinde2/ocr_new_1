@notAutomated
Feature: Delivery mode options - Basket Page
  In order to compare delivery costs
  As a customer I want to compare delivery costs by selecting the available delivery mode options in Basket page.

  Background: 
    Given delivery fees are set up as:
      | deliveryMode      | fee |
      | express-delivery  | 9   |
      | home-delivery     | 8   |
      | click-and-collect | 0   |

  Scenario Outline: Customer with cart and all the delivery modes enabled through configuration
    Given a Customer with cart
    And all the Delivery modes are Enabled through configuration
    And Delivery modes active for a cart are '<activeDeliveryModesForCart>'
    When the basket page is browsed
    Then Delivery Modes enabled in Basket page are '<enabledDeliveryModes>'

    Examples: 
      | activeDeliveryModesForCart                       | enabledDeliveryModes                             |
      | express-delivery,home-delivery,click-and-collect | express-delivery,home-delivery,click-and-collect |
      | express-delivery,home-delivery,click-and-collect | express-delivery,home-delivery,click-and-collect |
      | express-delivery,home-delivery,click-and-collect | express-delivery,home-delivery,click-and-collect |
      | express-delivery,home-delivery                   | express-delivery,home-delivery                   |
      | express-delivery,click-and-collect               | express-delivery,click-and-collect               |
      | home-delivery,click-and-collect                  | click-and-collect, home-delivery                 |
      | home-delivery                                    | home-delivery                                    |
      | express-delivery                                 | express-delivery                                 |
      | click-and-collect                                | click-and-collect                                |
      | None                                             | None                                             |

  Scenario Outline: Customer with cart and all the delivery modes enabled through configuration and selected delivery mode
    Given a Customer with cart
    And all the Delivery modes are Enabled through configuration
    When the basket page is browsed
    And delivery mode selected by customer is '<deliveryModeSelected>'
    Then Delivery price displayed in payment info section is '<deliveryPrice>'
    And total price displayed in payment info section is '<totalPrice>'

    Examples: 
      | deliveryModeSelected | deliveryPrice | totalPrice    |
      | express-delivery     | 9             | Sub total + 9 |
      | home-delivery        | 8             | Sub total + 8 |
      | click-and-collect    | Free          | Sub total + 0 |
      | home-delivery        | 8             | Sub total + 8 |
      | express-delivery     | 9             | Sub total + 9 |
      | click-and-collect    | Free          | Sub total + 0 |
      | home-delivery        | 8             | Sub total + 8 |
      | express-delivery     | 9             | Sub total + 9 |
      | click-and-collect    | Free          | Sub total + 0 |
      | None                 | N/A           | N/A           |

  Scenario Outline: Customer with cart and delivery modes enabled/disabled through configuration
    Given a Customer with cart
    And the Delivery modes disabled through configuration are '<deliveryModesDisabled>'
    When the basket page is browsed
    Then Delivery modes displaying in cart page are '<deliveryModesDisplayed>'

    Examples: 
      | deliveryModesDisabled              | deliveryModesDisplayed                           |
      | express-delivery                   | home-delivery,click-and-collect                  |
      | home-delivery                      | express-delivery, click-and-collect              |
      | click-and-collect                  | express-delivery,home-delivery                   |
      | express-delivery,home-delivery     | click-and-collect                                |
      | express-delivery,click-and-collect | home-delivery                                    |
      | home-delivery, click-and-collect   | express-delivery                                 |
      | None                               | express-delivery,home-delivery,click-and-collect |
