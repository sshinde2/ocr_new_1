Feature: As a target online customer
  I should be able use the Zippay payment mode

  Scenario: Selecting Zippay as payment method in checkout and when its available
    Given any cart
    And Zippay createCheckout api returns:
      | id                        | uri                                                                                                          |
      | co_8EVtW6efOqscHHSrlxRYl0 | https://account.sandbox.zipmoney.com.au/?co=co_8EVtW6efOqscHHSrlxRYl0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e |
    When the payment mode is 'zip'
    Then the Zippay reponse is successful

  Scenario: Selecting Zippay as payment method in checkout and when its unavailable
    Given any cart
    And Zippay createCheckout API is unavailable
    When the payment mode is 'zip'
    Then the Zippay reponse is unsuccessful
