@esbPriceUpdate @cartProductData
Feature: Was now prices - Flag for promotional markdowns
  In order to Flag promotional markdowns
  As Online Operations
  I want to flag if product price is permanent or promotional

  Scenario Outline: promoEvent flag
    Given product <productId> in Hybris price update has promoEvent <promoeventFlag>
    When Hybris POS price update runs
    Then the Hybris TargetPriceRow promoEvent flag is set <setFlag>

    Examples: 
      | productId | promoeventFlag | setFlag |
      | 10000011  | true           | true    |
      | 10000011  | false          | false   |
