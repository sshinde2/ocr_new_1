@notAutomated
Feature: "Google Analytics Event" for Assorted Products

  Scenario: Target Admin can see the assorted dimension
    Given I am a Target Admin on Google Analytics
    When I am viewing the product funnel
    Then I am able to add a secondary dimension "assorted"

  #Note: There are a lot of places a dimension can be used - listing all may be difficult.
  ## Add to basket
  Scenario: Track add to basket for assorted product
    Given A Target user is viewing an assorted product page
    When The Target user adds the product to their basket
    Then The analytics event will have the attribute dimension17
    And The attribute dimension17 value will be true

  Scenario: Track add to basket for non-assorted product
    Given A Target user is viewing a non-assorted product page
    When The Target user adds the product to their basket
    Then The analytics event will have the attribute dimension17
    And The attribute dimension17 value will be false

  # Impressions
  Scenario: Impressions for an assorted product
    Given A Target user
    When The Target engages in an impression for an assorted product <see table>
    Then The analytics event for ec:impression will fire
    And The attribute dimension17 value will be true

  Scenario Outline: Impressions for a non-assorted product
    Given A Target user
    When The Target engages in an impression for a non-assorted product<forms of impressions>
    Then The analytics event for ec:impression will fire
    And The attribute dimension17 value will be false

    Examples: 
      | Forms of Impressions             |
      | Bought/Bought                    |
      | Recently Viewed                  |
      | Recommended products             |
      | You may also like                |
      | Products on a look               |
      | Search results in a listing page |
