@notAutomated
Feature: Assorted Products in Favourites Page
  As a target online user
  I should be able to distinguish between assorted products and regular products in my favourite page
  so that I'm aware before adding it to the cart.

  Scenario: Assortment disclaimer underneath the favorite product
    Given I have added an Assorted product to favorites as a target online user
    When I go to favorites page
    Then I see the message "This item will be randomly selected" in orange underneath the assorted product
    And I see the add to basket button in orange

  Scenario: Adding the assorted product from favorites to the basket
    Given I am in the favorites page as a target online user
    And I have an assorted favourite product
    When I click the orange add to basket button
    Then I see a confirmation message "Item has been added to your basket - View Basket" in green
    And The item has been added to the basket

  Scenario: Adding a non-assorted product to Favorites
    Given I have added a non-assorted/regular product to favorites as a target online user
    When I go to favorites page
    Then I don't see the message "This item will be randomly selected" in orange underneath the product
    And I see the add to basket button in green