Feature: Get the ProductDTO and ensure that all the attributes are populated with the correct values to be used for Google Analytics
  
  Description: The products used for testing the scenarios has been setup during 
  initialization process using the following impex-
  /tgtinitialdata/import/productCatalogs/targetProductCatalog/automationTest/automation-fetch-products-setup.impex

  Scenario Outline: retrieve different products for product display page
    When the '<product>' is retrieved
    Then the product details are:
      | baseCode   | baseName   | departmentCategory   | brand   | price   | code   |
      | <baseCode> | <baseName> | <departmentCategory> | <brand> | <price> | <code> |

    Examples: 
      | product  | baseCode | baseName       | departmentCategory | brand  | price  | code     | comment                      |
      | 10000914 | W100009  | Test product 9 | Baby               | Target | $15.00 | 10000914 | product in single category   |
      | 10000020 | W100000  | Test product 0 | Baby               | Target | $15.00 | 10000020 | product in multiple category |