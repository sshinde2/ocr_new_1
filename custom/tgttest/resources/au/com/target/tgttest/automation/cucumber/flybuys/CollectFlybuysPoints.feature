@wip @rollback @cartProductData
Feature: Flybuys - Collect Flybuys points
  In order to provide better customer satisfaction
  As Online Store
  I want to provide means to collect flybuys points

  Scenario Outline: Customer placing an order with flybuys
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    And customer presents flybuys card '6008943218616910'
    And payment method is '<payment_method>'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 79.0  | 70.0     | 9.0          | 7.18     |
    And Order should contain flybuys number
    And Accertify feed should contain flybuys number
    And flybuy number in tlog is:
      | flybuysNumber    |
      | 6008943218616910 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: flybuys number should get saved against a user
    Given any order with flybuys number
    And registered user checkout
    And payment method is '<payment_method>'
    When order is placed
    Then fluybuys number should get saved against the user

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario: Fullfill a order with Flybuys
    Given a order with flybuys card number
    When order is fullfilled
    Then order is:
      | status    | invoice   | consignmentStatus |
      | completed | generated | Shiped            |
