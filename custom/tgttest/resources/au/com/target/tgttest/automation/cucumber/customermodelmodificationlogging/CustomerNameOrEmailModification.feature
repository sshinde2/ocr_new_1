@registeredCustomers
Feature: Security Improvements - Logging of changes to customer first name, last name and email
    For security audit
    As a CS Agent I want to know when customer details such as first name, last name and email address are changed and by whom

  Scenario: User changes first name and last name in his account
    Given a customer exists with the following CustomerAccountData details:
      | login                | firstName | lastName |
      | jane.smith@auto.test | jane      | simith   |
    When The user logs into his account and changes:
      | changedAttribute | newValue |
      | firstname        | joe      |
      | lastname         | smith    |
    Then the changes to customer data will be recorded as follows:
      | changedBy            | changedAttribute | oldValue | newValue |
      | jane.smith@auto.test | firstname        | jane     | joe      |
      | jane.smith@auto.test | lastname         | simith   | smith    |

  Scenario: User changes email id in his account
    Given a customer exists with the following CustomerAccountData details:
      | login                | firstName | lastName |
      | jane.smith@auto.test | jane      | simith   |
    When The user logs into his account and changes:
      | changedAttribute | newValue             |
      | uid              | john.smith@auto.test |
    Then the changes to customer data will be recorded as follows:
      | changedBy            | changedAttribute | oldValue             | newValue             |
      | john.smith@auto.test | uid              | jane.smith@auto.test | john.smith@auto.test |
