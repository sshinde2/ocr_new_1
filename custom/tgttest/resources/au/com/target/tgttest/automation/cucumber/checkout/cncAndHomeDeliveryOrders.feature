@cartProductData 
Feature: Checkout - Click N Collect and Home Delivery options
In order to avoid splitting deliveries, 
as the online store we want all products in an order to be eligible for the selected delivery mode.

Note: two scenarios fail since facade layer allows invalid combinations, see OCR-8953
  
  Scenario: Order with an item eligible for CNC and HD with home-delivery option
    Given product delivery modes:
      | product  | deliveryMode                    |
      | 10000011 | home-delivery,click-and-collect |
    And a cart with entries:
      | product  | qty |
      | 10000011 | 2   |
    And delivery mode is 'home-delivery'
    When order is placed
    Then place order result is 'SUCCESS'

  Scenario: Order with an item eligible for CNC and HD with click-and-collect option
    Given product delivery modes:
      | product  | deliveryMode                    |
      | 10000011 | home-delivery,click-and-collect |
    And a cart with entries:
      | product  | qty |
      | 10000011 | 2   |
    And delivery mode is 'click-and-collect'
    When order is placed
    Then place order result is 'SUCCESS'

  @toFix
  Scenario: Order with products not all eligible for home-delivery option
    Given product delivery modes:
      | product  | deliveryMode      |
      | 10000211 | home-delivery     |
      | 10000311 | click-and-collect |
    And a cart with entries:
      | product  | qty |
      | 10000211 | 2   |
      | 10000311 | 2   |
    And delivery mode is 'home-delivery'
    When order is placed
    Then place order result is not 'SUCCESS'

  @toFix
  Scenario: Order with products not all eligible for click-and-collect option
    Given product delivery modes:
      | product  | deliveryMode      |
      | 10000211 | home-delivery     |
      | 10000311 | click-and-collect |
    And a cart with entries:
      | product  | qty |
      | 10000211 | 2   |
      | 10000311 | 2   |
    And delivery mode is 'click-and-collect'
    When order is placed
    Then place order result is not 'SUCCESS'
