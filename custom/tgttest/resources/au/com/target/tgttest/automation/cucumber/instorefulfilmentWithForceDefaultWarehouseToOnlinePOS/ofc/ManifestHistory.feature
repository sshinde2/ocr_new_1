@dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Manifest - Manifest history
  
  In Order to find out which Manifest a consignment belongs to
  As a Store Agent
  I want to view Manifest by Date/Time

  Background: 
    Given store number for ofc portal is '1000'
    And manifest history days is set to '7'

  Scenario: No manifest exists in the system and ofc manifest history function is run.
    Given empty list of manifests for the store
    When ofc manifest history function is run
    Then manifest history for the store is empty

  Scenario: No manifest exists in the system for past '7' days and ofc manifest history function is run.
    Given list of manifests for the store is:
      | manifestID | manifestDate   | manifestTime |
      | x0000123   | 25 days before |              |
      | x0000124   | 10 days before |              |
      | x0000125   | 8 days before  |              |
    When ofc manifest history function is run
    Then manifest history for the store is empty

  Scenario: Manifests history shows only past '7' days of manifests when ofc manifest history function is run.
    Given list of manifests for the store is:
      | manifestID | manifestDate   | manifestTime |
      | x0000123   | 25 days before |              |
      | x0000124   | 10 days before |              |
      | x0000125   | 8 days before  |              |
      | x0000126   | 7 days before  | 23:59        |
      | x0000127   | 7 days before  | 00:01        |
      | x0000128   | yesterday      |              |
      | x0000129   | today          |              |
    And list of consignments for the store is:
      | consignmentCode | manifestID |
      | autox11000123   | x0000123   |
      | autox11000124   | x0000124   |
      | autox11000125   | x0000125   |
      | autox11000126   | x0000126   |
      | autox11000127   | x0000127   |
      | autox11000128   | x0000128   |
      | autox11000129   | x0000129   |
    When ofc manifest history function is run
    Then manifest history for the store is:
      | manifestID | manifestDate  | manifestTime |
      | x0000129   | today         |              |
      | x0000128   | yesterday     |              |
      | x0000127   | 7 days before | 00:01        |
      | x0000126   | 7 days before | 23:59        |

  Scenario: View a manifest details when user selects a manifest.
    Given list of manifests for the store is:
      | manifestID | manifestDate  | manifestTime |
      | x0000124   | 5 days before | 10:10        |
      | x0000128   | yesterday     | 11:11        |
      | x0000129   | today         | 12:12        |
    And list of consignments for the store is:
      | consignmentCode | manifestID | boxes | destination  | customer | pickDate  |
      | autox11000124   | x0000124   | 1     | Seaford, VIC | DD DDDD  | Yesterday |
      | autox11000125   | x0000124   | 2     | Tarneit, VIC | FF FFFF  | Today     |
      | autox11000126   | x0000124   | 3     | Geelong, VIC | JJ JJJJ  | Today     |
      | autox11000127   | x0000128   | 4     | Geelong, VIC | HH HHHH  | Today     |
      | autox11000128   | x0000128   | 2     | Geelong, VIC | II IIII  | Today     |
      | autox11000129   | x0000129   | 3     | Geelong, VIC | PP PPPP  | Today     |
    When user selects manifest 'x0000124' to view
    Then list of manifest consignments for the store is:
      | consignmentCode | boxes | destination  | customer | pickDate  |
      | autox11000124   | 1     | Seaford, VIC | DD DDDD  | Yesterday |
      | autox11000125   | 2     | Tarneit, VIC | FF FFFF  | Today     |
      | autox11000126   | 3     | Geelong, VIC | JJ JJJJ  | Today     |
    And manifest date is '5 days before at 10:10'
    And number of boxes in manifest is '6'
