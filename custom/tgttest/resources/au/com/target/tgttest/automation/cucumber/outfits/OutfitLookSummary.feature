@notAutomated
Feature: Outfit Look summary page - storefront
    In order to view, customise collection of products as a whole outfit
    As an online customer 
    I want to see Look summary

  Scenario Outline: Online customer can see look summary
    Given Online customer navigates to collection <collection> page
    And look <look> is visible
    When customer chooses to view the look <look>
    Then look <look> page is displayed
    And breadcrumb is visible as <look> under <collection> under <topLevelCategory>
    And customer can see look name <name>
    And look hero image <heroImage> is displayed
    And number of products <numberOfProducts> for look <look> is displayed
    And from price <fromPrice> is available for look <look>
    And number of products <numberOfProducts> for look <look> is displayed
    And from price <fromPrice> is available for look <look>

    Examples: 
      | look       | name               | heroImage        | collection       | topLevelCategory | productsInLook                                   | numberOfProducts | fromPrice |
      | officeLook | Summer Office Look | summerOfficeHero | SummerCollection | Women            | 10000011, 10000121, 10000211, 10001010, 10000912 | 5                | 75        |
      | partyLook  | Summer Party Look  | N/A              | SummerCollection | Women            | 10000011, 10000121, 10000211                     | 3                | 125       |
      | casualLook | Summer Casual Look | summerCasualHero | SummerCollection | Women            | 10001010, 10000121, 10000011, 10000912           | 4                | N/A       |
      | casualLook | Winder Casual Look | winderCasualHero | WinterCollection | Women            | N/A                                              | 0                | N/A       |

  Scenario Outline: Online customer can see images for each product
    Given Online customer navigates to 'Spring Collection'
    When customer chooses to view the look 'Casual Look'
    Then customer can see image <image> for product <product>

    Examples: 
      | product  | productImage   |
      | 10000011 | image_10000011 |
      | 10000121 | image_10000121 |
      | 10000211 | image_10000211 |
      | 10001010 | image_10001010 |
      | 10000912 | image_10000912 |

  Scenario Outline: Online customer can see out of stock indicator for products
    Given Online customer navigates to 'Spring Collection'
    When customer chooses to view the look 'Casual Look'
    Then out of stock indicator <outOfStockIndicator> displayed for product <product> having no stock

    Examples: 
      | look       | product  | outOfStockIndicator |
      | officeLook | 10000011 | yes                 |
      | officeLook | 10001010 | no                  |