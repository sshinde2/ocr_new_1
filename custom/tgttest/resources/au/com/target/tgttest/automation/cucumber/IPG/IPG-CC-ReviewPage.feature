@notAutomated
Feature: IPG - For Credit Cards Navigate From Payment Page to Review Page on Continue

  Background: 
    Given IPG payment feature is on

  Scenario: Review Order button is displayed on the Payment Page
    Given I am navigating through the payment flow
    When I land on the Payment Page
    Then the Review Order button will be displayed and disabled

  Scenario: Choose credit card payment method enables review Order button
    Given I land on the Payment Page
    When I select credit card payment method
    Then the Review Order button will be enabled

  Scenario: Clicking Review Order navigates to the review page if Credit Card payment method is selected
    Given I have selected the Credit Card payment method
    When I click/tap on the Review Order
    And no validation error
    Then I will navigate to the Review Page

  Scenario: Review page displays current information without payment details
    Given I have clicked on the Review Order button
    And Credit Card payment method selected
    When I land on the Review Page
    Then I will see the My Delivery Address details, My Billing Address details, Collect and Redeem Flybuys button, Promo Code and Discount button, back to payment link
    And the Order summary will maintain existing functionality
    And the Pay Securely button will be removed
