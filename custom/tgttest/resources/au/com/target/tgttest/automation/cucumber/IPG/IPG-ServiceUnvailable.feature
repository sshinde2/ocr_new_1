@notAutomated
Feature: IPG - Return to Payment Page if IPG is Unavailable
  As a Target Customer
  I want to go back to the Payment Page if IPG is unavailable
  So that I can attempt a different payment method

  Scenario Outline: User clicks Reivew Order on payment page for credit card payment
    Given user is on the Payment page
    And user has selected the credit card payment option
    And <ipg_status>
    When user clicks Review Order
    Then user will stay on the payment page
    And the Credit Card payment method will show as disabled on selection
    And the disabled message will read "Paying by credit card is currently unavailable. Please select a different payment method."
    And the global error message will read "Paying by credit card is currently unavailable. Please select a different payment method."

    Examples: 
      | ipg_status                          |
      | IPG is unavailable                  |
      | IPG gives error when fetching token |
      
  Scenario Outline: User clicks Reivew Order on payment page for gift card payment
    Given user is on the Payment page
    And user has selected the gift card payment option
    And <ipg_status>
    When user clicks Review Order
    Then user will stay on the payment page
    And the Gift Card payment method will show as disabled on selection
    And the disabled message will read "Paying by gift card is currently unavailable. Please select a different payment method."
    And the global error message will read "Paying by gift card is currently unavailable. Please select a different payment method."

    Examples: 
      | ipg_status                          |
      | IPG is unavailable                  |
      | IPG gives error when fetching token |

  Scenario: User navigates back to the payment page
    Given the credit card payment method was hidden
    And user navigated away from the payment page
    When user navigates back to the payment page
    Then the Credit Card payment method will be available
    
  Scenario Outline: Displaying Error message when Update credit card session token is not obtained
    Given User is on the My Order Details
    And the available order is pre-oder
    And <ipg_status>
    When user clicks Update credit card
    Then user will stay on the order-details page
    And the error message will read "Sorry, a connection error occurred, please try again."
    
     Examples: 
      | ipg_status                          |
      | IPG is unavailable                  |
      | IPG gives error when fetching token |

   Scenario Outline: Displaying Error message when the user clicks try again for Update credit card session token is not obtained
    Given User is on the My Order Details
    And the available order is pre-oder
    And <ipg_status>
    When user clicks try again link from the error message 
    Then user will stay on the order-details page
    And the error message will read "Sorry, a connection error occurred, please try again."
    
     Examples: 
      | ipg_status                          |
      | IPG is unavailable                  |
      | IPG gives error when fetching token |
      