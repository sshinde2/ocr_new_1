@cartProductData @voucherData
Feature: Vouchers can be applied to reduce order total
  In order to drive sales, vouchers can be set up and given to users
  who can apply them to carts to get a discount.
  
  Test Voucher TV1 has value $10 and restriction on order value at least $11

  Scenario Outline: Placing an order with a voucher.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And delivery mode is 'home-delivery'
    And customer presents voucher 'VOUCHER-TV1'
    And payment method is '<payment_method>'
    When order is placed
    Then order is:
      | subtotal | deliveryCost | total | totalTax |
      | 30.0     | 9.0          | 29.0  |          |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |
