Feature: As a target online customer
    stock check should be done against cart modifications I try
    to make sure they can be checked out

  Background: 
    Given products with the following stock exists against fastline warehouse:
      | product  | available | reserved |
      | 10000011 | 10        | 0        |
      | 10000012 | 0         | 0        |
      | 10000021 | 2         | 0        |
      | 10000022 | 2         | 0        |
    And products with the following ats values in fluent:
      | product          | CC | HD | ED | PO |
      | 10000011         | 10 | 10 | 10 | 0  |
      | 10000012         | 0  | 0  | 0  | 0  |
      | 10000021         | 2  | 2  | 0  | 0  |
      | 10000022         | 0  | 2  | 0  | 0  |
      | V1111_preOrder_1 | 0  | 0  | 0  | 10 |
      | V2222_preOrder_2 | 0  | 0  | 20 | 0  |

  Scenario Outline: Adding products to cart should check stock
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    And the 'fluent' feature switch is <fluentEnabled>
    And product '<productsInCart>' with max quantity '<maxOrderQuantity>'
    When product '<product>' with quantity '<newQty>' is added
    Then cart modification status is '<expectedStatus>'

    Examples: 
      | fluentEnabled | productsInCart   | qty | product          | newQty | expectedStatus        | maxOrderQuantity |
      | enabled       | 10000011         | 1   | 10000011         | 9      | success               |                  |
      | enabled       | 10000011         | 10  | 10000011         | 1      | noStock               |                  |
      | enabled       | 10000011         | 10  | 10000012         | 1      | noStock               |                  |
      | enabled       | 10000011         | 10  | 10000021         | 2      | success               |                  |
      | enabled       | 10000011         | 10  | 10000022         | 2      | success               |                  |
      | enabled       | V1111_preOrder_1 | 1   | V1111_preOrder_1 | 1      | success               |                  |
      | enabled       | V2222_preOrder_2 | 1   | V2222_preOrder_2 | 1      | success               |                  |
      | disabled      | 10000011         | 1   | 10000011         | 9      | success               |                  |
      | disabled      | 10000011         | 10  | 10000011         | 1      | noStock               |                  |
      | disabled      | 10000011         | 10  | 10000012         | 1      | noStock               |                  |
      | disabled      | 10000011         | 10  | 10000021         | 2      | success               |                  |
      | disabled      | 10000011         | 10  | 10000022         | 2      | success               |                  |
      | disabled      | 10000011         | 5   | 10000011         | 2      | itemsThresholdReached | 5                |

  Scenario Outline: Updating products in cart should check stock
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    And the 'fluent' feature switch is <fluentEnabled>
    When product '<product>' is updated with quantity '<newQty>'
    Then cart modification status is '<expectedStatus>'
    And preOrder attributes '<preOrderAttributes>' in cart

    Examples: 
      | fluentEnabled | productsInCart   | qty | product          | newQty | expectedStatus         | preOrderAttributes |
      | enabled       | 10000011         | 1   | 10000011         | 10     | success                | notpresent         |
      | enabled       | 10000011         | 10  | 10000011         | 11     | noModificationOccurred | notpresent         |
      | enabled       | 10000021         | 1   | 10000021         | 2      | success                | notpresent         |
      | enabled       | 10000021         | 2   | 10000021         | 3      | noModificationOccurred | notpresent         |
      | enabled       | 10000021         | 1   | 10000021         | 0      | success                | notpresent         |
      | enabled       | V1111_preOrder_1 | 1   | V1111_preOrder_1 | 2      | success                | present            |
      | enabled       | V2222_preOrder_2 | 1   | V2222_preOrder_2 | 2      | success                | present            |
      | enabled       | V1111_preOrder_1 | 2   | V1111_preOrder_1 | 1      | success                | present            |
      | enabled       | V2222_preOrder_2 | 2   | V2222_preOrder_2 | 1      | success                | present            |
      | enabled       | V1111_preOrder_1 | 1   | V1111_preOrder_1 | 0      | success                | notpresent         |
      | disabled      | 10000011         | 1   | 10000011         | 10     | success                | notpresent         |
      | disabled      | 10000011         | 10  | 10000011         | 11     | noModificationOccurred | notpresent         |
      | disabled      | 10000021         | 1   | 10000021         | 2      | success                | notpresent         |
      | disabled      | 10000021         | 2   | 10000021         | 3      | noModificationOccurred | notpresent         |
      | disabled      | 10000021         | 1   | 10000021         | 0      | success                | notpresent         |
      | disabled      | V2222_preOrder_2 | 1   | V2222_preOrder_2 | 0      | success                | notpresent         |
      
  Scenario Outline: Stock verification on backet page and checkout
    Given a cart with product '10000011' and with quantity '<qty>'
    And products with the following stock exists against fastline warehouse:
      | product  | available | reserved |
      | 10000011 | 2         | 0        |
    And products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000011 | 2  | 2  | 2  |
    And the 'fluent' feature switch is <fluentEnabled>
    When a soh check is done on the cart
    Then cart modification quantities are:
      | <modifiedQty> |
    And cart modification statuses are:
      | <modificationStatus> |
   And preOrder attributes 'notpresent' in cart

    Examples: 
      | fluentEnabled | qty | modifiedQty | modificationStatus     |
      | enabled       | 2   | 2           | noModificationOccurred |
      | disabled      | 2   | 2           | noModificationOccurred |
      | enabled       | 3   | 2           | lowStock               |
      | disabled      | 3   | 2           | lowStock               |
