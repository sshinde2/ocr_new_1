@cartProductData @sessionCatalogVersion @initPreOrderProducts @zipFeatureSwitch
Feature: Check available payment methods based on products in cart.

  Background: 
    Given an afterpay configuration is:
      | type               | description   | max  | min |
      | PAY_BY_INSTALLMENT | Pay over time | 1000 | 50  |

  Scenario: Mixed cart with giftcard and physical items.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName    | email                               | messageText   |
      | PGC1000_iTunes_10 | Janandith | Jayawardena | janandith.jayawardena@target.com.au | Test Checkout |
    And registered user checkout
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason                                            | retryRequired |
      | ipg      | true      | null                                              | false         |
      | giftcard | false     | Items in your basket prevent this payment option. | false         |
      | paypal   | true      | null                                              | false         |
      | afterpay | false     | Items in your basket prevent this payment option. | false         |
      | zip      | true      | null                                              | false         |

  Scenario: Cart has only physical items.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason                            | retryRequired |
      | ipg      | true      | null                              | false         |
      | giftcard | true      | null                              | false         |
      | paypal   | true      | null                              | false         |
      | afterpay | false     | Available on orders $50 to $1,000 | false         |
      | zip      | true      | null                              | false         |

  Scenario: Cart has only physical items and over afterpay max threshold.
    Given a cart with entries over the afterpay max threshold:
      | product  | qty | price |
      | 10000011 | 70  | 15    |
    And registered user checkout
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason                            | retryRequired |
      | ipg      | true      | null                              | false         |
      | giftcard | true      | null                              | false         |
      | paypal   | true      | null                              | false         |
      | afterpay | false     | Available on orders $50 to $1,000 | false         |
      | zip      | true      | null                              | false         |

  Scenario: Cart has only giftcard.
    Given a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName    | email                               | messageText   |
      | PGC1000_iTunes_10 | Janandith | Jayawardena | janandith.jayawardena@target.com.au | Test Checkout |
    And registered user checkout
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason                                            | retryRequired |
      | ipg      | true      | null                                              | false         |
      | giftcard | false     | Items in your basket prevent this payment option. | false         |
      | paypal   | true      | null                                              | false         |
      | afterpay | false     | Items in your basket prevent this payment option. | false         |
      | zip      | true      | null                                              | false         |

  Scenario: Cart eligible for afterpay but ping to after pay fails.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 6   | 15    |
    And registered user checkout
    And Afterpay ping API is unavailable
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason | retryRequired |
      | ipg      | true      | null   | false         |
      | giftcard | true      | null   | false         |
      | paypal   | true      | null   | false         |
      | afterpay | false     | null   | true          |
      | zip      | true      | null   | false         |

  Scenario: Cart eligible for afterpay and ping to afterpay is successful.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 6   | 15    |
    And registered user checkout
    And afterpay check availability is successful
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason | retryRequired |
      | ipg      | true      | null   | false         |
      | giftcard | true      | null   | false         |
      | paypal   | true      | null   | false         |
      | afterpay | true      | null   | false         |
      | zip      | true      | null   | false         |

  Scenario: Cart is only eligible for ipg for pre-order product
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 200   |
    And registered user checkout
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason                                            | retryRequired |
      | ipg      | true      | null                                              | false         |
      | giftcard | false     | Items in your basket prevent this payment option. | false         |
      | paypal   | false     | Items in your basket prevent this payment option. | false         |
      | afterpay | false     | Items in your basket prevent this payment option. | false         |
      | zip      | false     | Items in your basket prevent this payment option. | false         |

  Scenario: Cart is not eligible for Zip(Cart contains zip excluded product).
    Given a cart with entries:
      | product   | qty | price |
      | 100011121 | 6   | 15    |
    And registered user checkout
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason                                            | retryRequired |
      | ipg      | true      | null                                              | false         |
      | giftcard | true      | null                                              | false         |
      | paypal   | true      | null                                              | false         |
      | afterpay | true      | null                                              | false         |
      | zip      | false     | Items in your basket prevent this payment option. | false         |

  Scenario: Cart is eligible for zippay but ping to zippay api is unsuccessful.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 6   | 15    |
    And registered user checkout
    And Zippay ping API is unavailable
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason | retryRequired |
      | ipg      | true      | null   | false         |
      | giftcard | true      | null   | false         |
      | paypal   | true      | null   | false         |
      | afterpay | true      | null   | false         |
      | zippay   | false     | null   | true          |

  Scenario: Cart is eligible for zippay and ping to zippay api is successful.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 6   | 15    |
    And registered user checkout
    And Zippay ping API is available
    When get available payment methods for SPC
    Then available payment method details are:
      | name     | available | reason | retryRequired |
      | ipg      | true      | null   | false         |
      | giftcard | true      | null   | false         |
      | paypal   | true      | null   | false         |
      | afterpay | true      | null   | false         |
      | zippay   | true      | null   | false         |
