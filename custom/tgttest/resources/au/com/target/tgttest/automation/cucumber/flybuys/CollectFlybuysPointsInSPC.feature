@rollback @cartProductData @voucherData
Feature: Flybuys - Collect Flybuys points
  In order to provide better customer satisfaction
  As Online Store
  I want to provide means to collect flybuys points

  Scenario: Guest user adds valid flybuys number to cart
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    When customer presents flybuys card '6008943218616910' in spc
    Then cart contains flybuys number '6008943218616910'
    And cart constains flybuys canRedeemPoints 'false'

  Scenario: Registered user adds valid flybuys number to cart
    Given a registered customer with a cart
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    When customer presents flybuys card '6008943218616910' in spc
    Then cart contains flybuys number '6008943218616910'
    And cart constains flybuys canRedeemPoints 'true'

  Scenario: Registered user adds valid flybuys number to cart with the cart total less than redeem threshold
    Given a registered customer with a cart
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 2     |
    When customer presents flybuys card '6008943218616910' in spc
    Then cart contains flybuys number '6008943218616910'
    And cart constains flybuys canRedeemPoints 'false'

  Scenario: Registered user adds valid flybuys number to cart with the voucher applied already
    Given a registered customer with a cart
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    And apply voucher VOUCHER-TV1 to cart in spc
    When customer presents flybuys card '6008943218616910' in spc
    Then cart contains flybuys number '6008943218616910'
    And cart constains flybuys canRedeemPoints 'false'

  Scenario: Customer adds partial flybuys number to cart
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    When customer presents flybuys card '600894321861691' in spc
    Then cart does not contain flybuys number

  Scenario: Customer adds invalid flybuys number to cart
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    When customer presents flybuys card '7008943218616910' in spc
    Then cart does not contain flybuys number
