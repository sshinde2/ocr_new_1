@cartProductData @deliveryModeData
Feature: Gift cards - product detail page
  
  As the online store
  I want to prevent all delivery options other than digital to show on PDP
  
  Notes:
  * Products are setup in impex as:
      | product           | deliveryModes for product                      |
      | PGC1000_iTunes_10 | digital-gift-card                              |
      | W1000001          |express-delivery,home-delivery,click-and-collect|
   
  10000010 has variants 10000011, 10000012
  10000020 has variants 10000021, 10000022

  Scenario Outline: Available delivery modes on product details page and quick view Page for digital e-cards
    When product details are retrieved for '<productCode>'
    Then delivery modes available for the product are '<availableDeliveryModesForProduct>'
    And giftcard flag on the product is set to '<isGiftCardFlag>'

    Examples: 
      | productCode       | availableDeliveryModesForProduct                 | isGiftCardFlag |
      | PGC1000_iTunes_10 | digital-gift-card                                | true           |
      | W1000001          | express-delivery,home-delivery,click-and-collect | false          |

  Scenario Outline: Stock status on product details page
    Given set fastline stock:
      | product  | available | reserved |
      | 10000011 | 100       | 0        |
      | 10000012 | 0         | 0        |
      | 10000021 | 0         | 0        |
      | 10000022 | 0         | 0        |
    And set Incomm gift cards in stock:
      | product               | available |
      | PGC1003_adventure_50  | 100       |
      | PGC1003_adventure_100 | NONE      |
      | PGC1003_jetboat_50    | NONE      |
      | PGC1003_jetboat_100   | NONE      |
    When full product details are retrieved for '<productCode>'
    Then stock status for the product is '<stockStatus>'

    Examples: 
      | productCode           | stockStatus | Comment              |
      | PGC1003_adventure_50  | Instock     |                      |
      | PGC1003_adventure_100 | Outofstock  |                      |
      | PGC1003_adventure     | Instock     | Since one variant is |
      | PGC1003_jetboat_50    | Outofstock  |                      |
      | PGC1003_jetboat_100   | Outofstock  |                      |
      | PGC1003_jetboat       | Outofstock  | Since no variant is  |
      | 10000011              | Instock     |                      |
      | 10000012              | Outofstock  |                      |
      | 10000010              | Instock     | Since one variant is |
      | 10000021              | Outofstock  |                      |
      | 10000022              | Outofstock  |                      |
      | 10000020              | Outofstock  | Since no variant is  |
