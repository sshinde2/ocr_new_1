Feature: Search by Post Code or Suburb for nearest stores for in store stock availability check

  Background: 
    Given geolocation setup for certain postcode/suburb are:
      | locationText        | latitude    | longitude   | locality            | postcode | state |
      | Brisbane            | -27.4697707 | 153.0251235 | Brisbane            | 4000     | QLD   |
      | 4000                | -27.4660994 | 153.0235880 | Spring Hill         | 4000     | QLD   |
      | Bega                | -36.6889097 | 149.8416404 | Bega                | 2550     | NSW   |
      | Gibson Desert North | -22.8131697 | 127.7412446 | Gibson Desert North | 0872     | WA    |
      | 0872                | -23.5886632 | 131.2808577 |                     | 0872     | NT    |
      | Augusta             | -34.2932460 | 115.1392450 | Augusta             | 6290     | WA    |
      | x                   | -25.2743980 | 133.7751360 |                     |          |       |
      | 1                   | -37.8850955 | 145.0050045 | Elsternwick         | 3185     | VIC   |
    And stock threshold config are set to limitedStockThreshold:8,noStockThreshold:0
    
  @notAutomated
  Scenario: Open Find In Store accordion item
    Given a Target Online user
    And the user has navigated to the Product Details Page (PDP) for a product
    And that product is a sellable variant
    And Find In Store is available for the product
    And the Find In Store accordion item is currently closed
    When the user clicks on the Find In Store accordion item
    Then the Find In Store accordion item is opened
    And the Post Code / Suburb field label and field are displayed
    And placeholder text is populated in the field
    And the "Check" button is displayed

  Scenario: Return error when 'inStoreStockVisibility' feature is disabled
    Given the 'inStoreStockVisibility' feature switch is disabled
    When the store stock API gets called
    Then it will return an error: 'ERR_CHECK_STOCK_SERVICE_ERROR', 'Sorry, the store stock service is disabled.'

  Scenario Outline: User enters Post Code / Suburb string and submits with fluent feature enabled
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the 'fluent' feature switch is enabled
    And the following stock levels are configured:
      | code         | storeNumber | soh |
      | P1026_blue_L | 7184        | 8   |
      | P1026_blue_L | 7059        | 7   |
      | P1026_blue_L | 7380        | 13  |
      | P1026_blue_L | 7229        | 7   |
      | P1026_blue_L | 7084        | 9   |
      | P1026_blue_L | 8331        | 4   |
      | P1026_blue_L | 8562        | 9   |
      | P1026_blue_L | 8330        | 5   |
      | P1026_blue_L | 8376        | 12  |
      | P1026_blue_L | 7162        | 11  |
      | P1026_blue_L | 7213        | 13  |
      | P1026_blue_L | 8574        | 9   |
      | P1026_blue_L | 7291        | 6   |
      | P1026_blue_L | 8573        | 9   |
      | P1026_blue_L | 7193        | 10  |
      | P1026_blue_L | 8191        | 12  |
      | P1026_blue_L | 8572        | 14  |
      | P1026_blue_L | 8555        | 9   |
      | P1026_blue_L | 8553        | 8   |
      | P1026_blue_L | 7173        | 7   |
      | P1026_blue_L | 8551        | 6   |
      | P1026_blue_L | 7159        | 7   |
      | P1026_blue_L | 8504        | 10  |
      | P1026_blue_L | 7289        | 6   |
      | P1026_blue_L | 7092        | 7   |
      | P1026_blue_L | 8086        | 9   |
      | P1026_blue_L | 7190        | 13  |
      | P1026_blue_L | 7087        | 9   |
      | P1026_blue_L | 7032        | 10  |
    And product details are retrieved for 'P1026_blue_L'
    And the Post Code/Suburb field has been displayed
    When the user searches for stock in '<searchString>'
    Then the location summary will be '<locationSummary>'
    Then the availability time is present
    And the closest stores will be '<closestStores>'
    And the stock levels will be '<stockLevels>'

    Examples: 
      | description                                                                   | searchString        | locationSummary              | closestStores                                             | stockLevels                 |
      | Standard suburb search                                                        | Brisbane            | Brisbane 4000, QLD           | Brisbane, Buranda, Indooroopilly, Toombul,  Brookside     | low, low, high, low, high  |
      | Standard postcode search                                                      | 4000                | Spring Hill 4000, QLD        | Brisbane, Buranda, Indooroopilly, Toombul,  Brookside     | low, low, high, low, high  |
      | Suburb search covering a larger area                                          | Bega                | Bega 2550, NSW               | Bega, Merimbula, Cooma, Batemans Bay, Tuggeranong         | low, high, low, high, high  |
      | Suburb search with a postcode that covers a very large area across two states | Gibson Desert North | Gibson Desert North 0872, WA | Alice Springs, Kununurra, Broome, Katherine, Kalgoorlie   | high, high, low, high, high |
      | Postcode that covers a very large area across two states                      | 0872                | 0872, NT                     | Alice Springs, Kununurra, Katherine, Broome, Port Augusta | high, high, high, low, high |
      | Standard suburb search                                                        | Augusta             | Augusta 6290, WA             | Margaret River, Busselton, Manjimup, Bunbury, Collie      | high, high, low, low, low  |
      | Nonsensical letter search that returns no location information                | x                   | x                            | Alice Springs, Port Augusta, Whyalla, Port Pirie, Kadina  | high, high, low, high, low  |
      | Nonsensical number search that does return location information               | 1                   | Elsternwick 3185, VIC        | Malvern, Bentleigh, Chapel Street, Chadstone, Camberwell  | low, high, high, high, high |

  Scenario Outline: User enters Post Code / Suburb string and submits
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the 'fluent' feature switch is disabled
    And the following stock levels are configured:
      | code         | storeNumber | soh |
      | P1026_blue_L | 7184        | 8   |
      | P1026_blue_L | 7059        | 7   |
      | P1026_blue_L | 7380        | 13  |
      | P1026_blue_L | 7229        | 7   |
      | P1026_blue_L | 7084        | 9   |
      | P1026_blue_L | 8331        | 4   |
      | P1026_blue_L | 8562        | 9   |
      | P1026_blue_L | 8330        | 5   |
      | P1026_blue_L | 8376        | 12  |
      | P1026_blue_L | 7162        | 11  |
      | P1026_blue_L | 7213        | 13  |
      | P1026_blue_L | 8574        | 9   |
      | P1026_blue_L | 7291        | 6   |
      | P1026_blue_L | 8573        | 9   |
      | P1026_blue_L | 7193        | 10  |
      | P1026_blue_L | 8191        | 12  |
      | P1026_blue_L | 8572        | 14  |
      | P1026_blue_L | 8555        | 9   |
      | P1026_blue_L | 8553        | 8   |
      | P1026_blue_L | 7173        | 7   |
      | P1026_blue_L | 8551        | 6   |
      | P1026_blue_L | 7159        | 7   |
      | P1026_blue_L | 8504        | 10  |
      | P1026_blue_L | 7289        | 6   |
      | P1026_blue_L | 7092        | 7   |
      | P1026_blue_L | 8086        | 9   |
      | P1026_blue_L | 7190        | 13  |
      | P1026_blue_L | 7087        | 9   |
      | P1026_blue_L | 7032        | 10  |
    And product details are retrieved for 'P1026_blue_L'
    And the Post Code/Suburb field has been displayed
    When the user searches for stock in '<searchString>'
    Then the location summary will be '<locationSummary>'
    Then the availability time is present
    And the closest stores will be '<closestStores>'
    And the stock levels will be '<stockLevels>'

    Examples: 
      | description                                                                   | searchString        | locationSummary              | closestStores                                             | stockLevels                 |
      | Standard suburb search                                                        | Brisbane            | Brisbane 4000, QLD           | Brisbane, Buranda, Indooroopilly, Toombul,  Brookside     | low, low, high, low, high  |
      | Standard postcode search                                                      | 4000                | Spring Hill 4000, QLD        | Brisbane, Buranda, Indooroopilly, Toombul,  Brookside     | low, low, high, low, high  |
      | Suburb search covering a larger area                                          | Bega                | Bega 2550, NSW               | Bega, Merimbula, Cooma, Batemans Bay, Tuggeranong         | low, high, low, high, high  |
      | Suburb search with a postcode that covers a very large area across two states | Gibson Desert North | Gibson Desert North 0872, WA | Alice Springs, Kununurra, Broome, Katherine, Kalgoorlie   | high, high, low, high, high |
      | Postcode that covers a very large area across two states                      | 0872                | 0872, NT                     | Alice Springs, Kununurra, Katherine, Broome, Port Augusta | high, high, high, low, high |
      | Standard suburb search                                                        | Augusta             | Augusta 6290, WA             | Margaret River, Busselton, Manjimup, Bunbury, Collie      | high, high, low, low, low  |
      | Nonsensical letter search that returns no location information                | x                   | x                            | Alice Springs, Port Augusta, Whyalla, Port Pirie, Kadina  | high, high, low, high, low  |
      | Nonsensical number search that does return location information               | 1                   | Elsternwick 3185, VIC        | Malvern, Bentleigh, Chapel Street, Chadstone, Camberwell  | low, high, high, high, high |

  Scenario: User enters suburb string and submits, closest stores not in stock, first in stock store at the top of the list
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the following stock levels are configured:
      | code         | storeNumber | soh |
      | P1026_blue_L | 7184        | 0   |
      | P1026_blue_L | 7059        | 0   |
      | P1026_blue_L | 7380        | 0   |
      | P1026_blue_L | 7229        | 0   |
      | P1026_blue_L | 7084        | 9   |
    And product details are retrieved for 'P1026_blue_L'
    And the Post Code/Suburb field has been displayed
    When the user searches for stock in 'Brisbane'
    Then the location summary will be 'Brisbane 4000, QLD'
    Then the availability time is present
    And the closest stores will be 'Brookside, Brisbane, Buranda, Indooroopilly, Toombul'
    And the stock levels will be 'high,no,no,no,no'

  Scenario Outline: User enters Post Code / Suburb string and submits without a product code
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the Post Code/Suburb field has been displayed
    When the user searches for stores in '<searchString>'
    Then the location summary will be '<locationSummary>'
    Then the availability time is present
    And the closest stores will be '<closestStores>'
    And the stock levels will be missing

    Examples: 
      | description                                                                   | searchString        | locationSummary              | closestStores                                             |
      | Standard suburb search                                                        | Brisbane            | Brisbane 4000, QLD           | Brisbane, Buranda, Indooroopilly, Toombul,  Brookside     |
      | Standard postcode search                                                      | 4000                | Spring Hill 4000, QLD        | Brisbane, Buranda, Indooroopilly, Toombul,  Brookside     |
      | Suburb search covering a larger area                                          | Bega                | Bega 2550, NSW               | Bega, Merimbula, Cooma, Batemans Bay, Tuggeranong         |
      | Suburb search with a postcode that covers a very large area across two states | Gibson Desert North | Gibson Desert North 0872, WA | Alice Springs, Kununurra, Broome, Katherine, Kalgoorlie   |
      | Postcode that covers a very large area across two states                      | 0872                | 0872, NT                     | Alice Springs, Kununurra, Katherine, Broome, Port Augusta |
      | Standard suburb search                                                        | Augusta             | Augusta 6290, WA             | Margaret River, Busselton, Manjimup, Bunbury, Collie      |
      | Nonsensical letter search that returns no location information                | x                   | x                            | Alice Springs, Port Augusta, Whyalla, Port Pirie, Kadina  |
      | Nonsensical number search that does return location information               | 1                   | Elsternwick 3185, VIC        | Malvern, Bentleigh, Chapel Street, Chadstone, Camberwell  |

  Scenario: User searches near another store without a product code
    Given the 'inStoreStockVisibility' feature switch is enabled
    When the user searches for stores near store '7184'
    Then the location summary will be blank
    And the availability time is present
    And the closest stores will be 'Brisbane, Buranda, Indooroopilly, Toombul,  Brookside'
    And the stock levels will be missing

  Scenario: User searches near another store
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the following stock levels are configured:
      | code         | storeNumber | soh |
      | P1026_blue_L | 8331        | 4   |
      | P1026_blue_L | 8562        | 9   |
      | P1026_blue_L | 8330        | 5   |
      | P1026_blue_L | 8376        | 12  |
      | P1026_blue_L | 7162        | 11  |
    And product details are retrieved for 'P1026_blue_L'
    When the user searches for stock in stores near store '8331'
    Then the location summary will be blank
    And the availability time is present
    And the closest stores will be 'Bega, Merimbula, Cooma, Batemans Bay, Tuggeranong'
    And the stock levels will be 'low, high, low, high, high'

  @notAutomated
  Scenario: Choose to "Change" post code/suburb
    Given Scenario 2 above
    When the user chooses "Change"
    Then all of the following elements are hidden:
      | Location summary (i.e.. "Stores near ...") | Change link. | Current time display. | "Closest stores near you" heading. | All information about the 5 closest stores. |
    And a Google Analytics event is recorded
    And the Post Code / Suburb field label and field are re-displayed
    And the search field is cleared
    And the user is able to re-submit a search with new postcode/suburb

  @notAutomated
  Scenario Outline: Display of stock levels for the 5 closest stores
    Given a Target Online user
    And the user has navigated to the Product Details Page (PDP) for product <productId>
    And that product is a sellable variant
    And Find In Store is available for the product
    And the limited stock threshold is set to <threshold>
    And the Post Code / Suburb field label and field are displayed
    And the following stock levels are configured:
      | store/productId | SKU001 | SKU002 | SKU003 |
      | Geelong         | 20     | 20     | 20     |
      | Waurn Ponds     | 9      | 6      | 1      |
      | Ocean Grove     | 8      | 5      | 0      |
      | Werribee        | 7      | 4      | 0      |
      | Point Cook      | 0      | 0      | N/A    |
    When the user enters "Geelong" or "3220" in the field and submits
    And the search is successful and includes results
    And if on mobile the keyboard closes
    And the Post Code / Suburb field label and field are hidden
    And the location summary is displayed as "Stores near Geelong 3220, VIC"
    And a "Change" link is displayed
    And the current time is displayed in format "Availability as at \{dd/mm/yy h:mm\[am\|pm\]\} AEST" (For example: "Availability as at 14/07/16 5:43pm AEST")
    #*_To be confirmed:_* Which time is being used.
    And a heading displayed for: "Closest stores near you"
    And the 5 closest stores to the location, <closestStores> are displayed in order of distance from the location
    And for each displayed store the following items are displayed:
      | Store name                                                                                                         |
      | Store phone number                                                                                                 |
      | Store distance from the location                                                                                   |
      | Stock level, which is one of: In stock, Check with store (if low), No stock (if zero stock or not ranged by store) |

    Examples: 
      | productId | threshold | closestStores                                                                                                                 |
      | SKU001    | 5         | Geelong (In stock), Waurn Ponds (In stock), Ocean Grove (In stock), Werribee (In stock), Point Cook (No stock)                |
      | SKU002    | 5         | Geelong (In stock), Waurn Ponds (In stock), Ocean Grove (Limited stock), Werribee (Limited stock), Point Cook (No stock)      |
      | SKU003    | 5         | Geelong (In stock), Waurn Ponds (Limited stock), Ocean Grove (No stock), Werribee (No stock), Point Cook (No stock)           |
      | SKU001    | not set*  | Geelong (In stock), Waurn Ponds (In stock), Ocean Grove (Limited stock), Werribee (Limited stock), Point Cook (No stock)      |
      | SKU002    | not set*  | Geelong (In stock), Waurn Ponds (Limited stock), Ocean Grove (Limited stock), Werribee (Limited stock), Point Cook (No stock) |
      | SKU003    | not set*  | Geelong (In stock), Waurn Ponds (Limited stock), Ocean Grove (No stock), Werribee (No stock), Point Cook (No stock)           |
      | SKU001    | 0         | Geelong (In stock), Waurn Ponds (In stock), Ocean Grove (In stock), Werribee (In stock), Point Cook (No stock)                |
      | SKU002    | 0         | Geelong (In stock), Waurn Ponds (In stock), Ocean Grove (In stock), Werribee (In stock), Point Cook (No stock)                |
      | SKU003    | 0         | Geelong (In stock), Waurn Ponds (In stock), Ocean Grove (No stock), Werribee (No stock), Point Cook (No stock)                |

  @notAutomated
  Scenario Outline: Default to most recent location used to check In Store Stock Availability
    Given a Target Online user
    And previously the user has  performed a Find in Store search with '<location>'
    And the search was '<searchStatus>'
    And cookies have not been cleared
    And the user has then navigated to the Product Details Page (PDP) for a different or same  product
    And the user has selected a sellable variant
    And the product is available for Find in Store
    When the user chooses  Find in Store
    And the Post Code / Suburb field  is displayed
    Then the Post Code / Suburb field is pre-populated with '<prepopulatedLocation>'

    Examples: 
      | location            | searchStatus | prepopulatedLocation         |
      | Geelong             | successful   | Geelong 3220  VIC            |
      | 3220                | successful   | Geelong 3220  VIC            |
      | 3030                | successful   | Derrimut 3030  VIC           |
      | Bega                | successful   | Bega 2550  NSW               |
      | Gibson Desert North | successful   | Gibson Desert North 0872  WA |
      | 0872                | successful   | 0872  NT                     |
      | Augusta             | successful   | Augusta 6290  WA             |
      | x                   | successful   | x                            |
      | 1                   | successful   | 1                            |
      | abcdefg             | unsuccessful |                              |

  @notAutomated
  Scenario Outline: Kiosk mode feature switch*
    Given a Target Online user using user agent <userAgent>
    And the Store Stock Search Kiosk mode feature flag is <kioskMode>
    When the user navigates to the Product Details Page (PDP) for a sellable variant product that is available for Find In Store
    Then the "Find In Store" section is <sectionDisplay>
    And when the user submits a search then the store stock service used is the <serviceUsed>
    And logging is performed both for the request and response for the store stock service call

    Examples: 
      | userAgent   | kioskMode | sectionDisplay | serviceUsed |
      | Kiosk       | ON        | displayed      | old service |
      | Web Desktop | ON        | not displayed  | n/a         |
      | Web Mobile  | ON        | not displayed  | n/a         |
      | Android App | ON        | not displayed  | n/a         |
      | iOS App     | ON        | not displayed  | n/a         |
      | Kiosk       | OFF       | displayed      | new service |
      | Web Desktop | OFF       | displayed      | new service |
      | Web Mobile  | OFF       | displayed      | new service |
      | Android App | OFF       | displayed      | new service |
      | iOS App     | OFF       | not displayed* | n/a         |

  @notAutomated
  Scenario Outline: Display link to store directions
    Given a Target Online Store user
    And they are using user agent <userAgent>
    And they are on the Product Details Page for a product
    And that product is a sellable variant
    And Find In Store is available for the product
    And the link to store directions feature is <storeDirectionsFeature>
    When they successfully perform a "Find in Store" search
    Then the nearest/closest stores are listed
    And each store listed has a distance from here label
    And whether the distance from here label is a link is <isLink>

    Examples: 
      | userAgent       | storeDirectionsFeature | isLink |
      | desktop browser | on                     | yes    |
      | mobile browser  | on                     | yes    |
      | Android app     | on                     | yes    |
      | kiosk           | on                     | no     |
      | desktop browser | off                    | no     |
      | mobile browser  | off                    | no     |
      | Android app     | off                    | no     |
      | kiosk           | off                    | no     |

  @notAutomated
  Scenario Outline: Select link to store directions
    Given a Target Online Store user
    And they are using user agent <userAgent>
    And the Find in Store search done was: <searchDone>
    And the distance from here label IS a link
    When the user selects a distance from here label/link
    Then the action taken is <action>
    And a Google Analytics event is recorded
    And the From location displayed is <fromLocation>
    And the To location displayed is <toLocation>
    And directions are displayed and mapped
      | userAgent       | searchDone           | action                                                     | fromLocation                             | toLocation                             |
      | desktop browser | Use Current Location | a new tab is opened with a map and directions to the store | current location geolocation coordinates | "Target <store name>, <store address>" |
      | desktop browser | submit "Geelong"     | a new tab is opened with a map and directions to the store | Geelong 3220, VIC                        | "Target <store name>, <store address>" |
      | mobile browser  | submit "Geelong"     | device dependent*, with a map and directions to the store  | Geelong 3220, VIC                        | "Target <store name>, <store address>" |
      | Android app     | submit "Geelong"     | device dependent*, with a map and directions to the store  | Geelong 3220, VIC                        | "Target <store name>, <store address>" |
      | desktop browser | submit "2550"        | a new tab is opened with a map and directions to the store | Bega 2550, NSW                           | "Target <store name>, <store address>" |
      | desktop browser | submit "0872"        | a new tab is opened with a map and directions to the store | 0872, NT                                 | "Target <store name>, <store address>" |
      | desktop browser | submit "x"           | a new tab is opened with a map and directions to the store | x                                        | "Target <store name>, <store address>" |
      | desktop browser | submit "1"           | a new tab is opened with a map and directions to the store | 1                                        | "Target <store name>, <store address>" |
