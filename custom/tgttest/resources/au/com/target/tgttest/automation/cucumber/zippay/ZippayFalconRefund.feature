@cartProductData @deliveryModeData @falconFeatureSwitch
Feature: Falcon Refund for Zip
  In order to have better shopping experience
  As an Online Store
  I want to refund the amount paid by zip as quickly as possible if the Falcon system doesnt fulfil the consignment for any reasons

  Background: 
    Given set fastline stock:
      | product   | reserved | available |
      | 100011122 | 2        | 2         |
      | 10000012  | 0        | 2         |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And set product stock:
      | product   | available | warehouseCode              |
      | 100011122 |  1        | ConsolidatedStoreWarehouse |
        
  Scenario: Placing an Order using zip when the falcon is turned on,unable to fulfil the consignment, refund gets triggered and processed sucessfully
    Given a cart with entries:
      | product   | qty | price |
      | 100011122 | 2   | 15    |
    And zip create refund api returns:
      | id                        |
      | rf_H5J24ZnqpKMByItvF2Jh85 |
    And payment method is 'zippay'
    When order is placed
    And place order result is 'SUCCESS'
    And the zip refund has been trigerred for the order with amount '24'
    And the zip refund has been 'succeeded'
    Then a CS ticket is not created for the order
    And the order status is CANCELLED

  Scenario: Placing an Order using zip when the falcon is turned on,unable to fulfil the consignment, refund gets triggered and has been failed
    Given a cart with entries:
      | product   | qty | price |
      | 100011122 | 2   | 15    |
    And the zip refund will 'fail' for the order 
    And payment method is 'zippay'
    When order is placed
    And place order result is 'SUCCESS'
    And the zip refund has been trigerred for the order with amount '24'
    And the zip refund has been 'failed'
    Then a CS ticket is created for the order
    And the order status is CANCELLED
   
