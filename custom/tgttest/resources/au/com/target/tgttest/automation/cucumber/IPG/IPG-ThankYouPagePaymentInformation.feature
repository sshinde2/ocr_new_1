@notAutomated
Feature: IPG - Payment information in thank you page
  As an online customer
  I want to know details of payments I used on the Thank You page
  So that I can be certain on payments I made

  Scenario Outline: Display PayPal payment details on thank you page
    Given cart with total <totalAmount>
    And customer has choosen PayPal as payment method
    And customer completes payment successfully with <paypalAccount>
    And paypal receipt number is <receiptNumber>
    When thank you page is displayed
    Then <paypalAccount> is displayed in My Payment Details section
    And <totalAmount> is displayed in My Payment Details section
    And <receiptNumber> is displayed in My Payment Details section

    Examples: 
      | totalAmount | paypalAccount     | receiptNumber |
      | $10.00      | paypal@paypal.com | PP1346789     |

  Scenario Outline: Display credit card payment details on thank you page
    Given cart with total <totalAmount>
    And customer has choosen credit card as payment method
    And customer completes payment successfully with credit card; <cardType>, <cardNumber>, <cardExpiry>
    And payment receipt number is <receiptNumber>
    When thank you page is displayed
    Then <cardType> is displayed in My Payment Details section
    And <maskedCreditCardNumber> is displayed in My Payment Details section
    And <cardExpiry> is displayed in My Payment Details section
    And <totalAmount> is displayed in My Payment Details section
    And <receiptNumber> is displayed in My Payment Details section

    Examples: 
      | totalAmount | cardType    | cardNumber       | maskedCardNumber | cardExpiry | receiptNumber |
      | $10.00      | Diners Club | 3676541234560036 | 367654******0036 | 02/17      | 123465789     |

  Scenario Outline: Display gift card payment details on thank you page
    Given cart with total <totalAmount>
    And customer has choosen gift card as payment method
    And customer completes payment successfully with gift card having card number <cardNumber>
    And payment receipt number is <receiptNumber>
    When thank you page is displayed
    Then <maskedCardNumber> is displayed in My Payment Details section
    And <totalAmount> is displayed in My Payment Details section
    And <receiptNumber> is displayed in My Payment Details section

    Examples: 
      | totalAmount | cardNumber       | maskedCardNumber | receiptNumber |
      | $10.00      | 6273451234568796 | 627345******8796 | 123465789     |

  Scenario Outline: Display split payment details on thank you page
    And customer has choosen gift card as payment method
    And customer completes payment successfully by splitting payment
      | payment1   | payment2   |
      | <payment1> | <payment2> |
    When thank you page is displayed
    Then payment information is displayed in My Payment Details section
      | payment1   | payment2   |
      | <payment1> | <payment2> |

    Examples: 
      | payment1                             | payment2                             |
      | GC1,627345******8796,$10.00,receipt1 | CC1,424242******4242,$15.00,receipt2 |
