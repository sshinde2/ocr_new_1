@cartProductData @storeFulfilmentCapabilitiesData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Check pending consignments before accepting new consignments to ensure there is enough stock on hand
  As the online store, I want to be able to check pending consignments before accepting new consignments to ensure there is enough stock on hand to fulfill new consignments.

  Background:
    Given stores with fulfilment capability:
      | store       | instoreEnabled | deliveryModesAllowed            | allowDeliveryToSameStore | allowDeliveryToAnotherStore | clearProductExclusions | clearCategoryExclusions |
      | Robina      | Yes            | click-and-collect,home-delivery | Yes                      | yes                         | true                   | true                    |
    And delivery modes allowed for instore fulfilment are 'click-and-collect,home-delivery'
    And stores with stock:
      | store | stock         |
      | 7126  | 5 of 10000011 |
    And list of consignments for store 'Robina' are:
      | consignmentCode | products      | consignmentStatus      |
      | autox11000121   | 1 of 10000011 | created                |
      | autox11000122   | 1 of 10000011 | sent_to_warehouse      |
      | autox11000123   | 1 of 10000011 | confirmed_by_warehouse |
      | autox11000124   | 1 of 10000011 | waved                  |
      | autox11000125   | 1 of 10000011 | shipped                |

  Scenario Outline: Orders are only sent to store if store has the right capability.
    Given the quantity added to cart for the '<product>' is '<qty>'
    And delivery mode is '<delivery-mode>'
    And the delivery address is '<address>'
    And order cnc store is '<cnc_store>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples:
      | product  | qty | delivery-mode          | address                                 | cnc_store | warehouse | comment                        |
      | 10000011 | 1   | home-delivery          | 446,Ann Street,Brisbane City,QLD,4000   | na        | Robina    | Robina has SOH 5 and pending 4 |
      | 10000011 | 2   | home-delivery          | 446,Ann Street,Brisbane City,QLD,4000   | na        | Fastline  | Robina cannot fulfill 2 more   |
      | 10000011 | 1   | click-and-collect      | na                                      | 7126      | Robina    | Robina has SOH 5 and pending 4 |
      | 10000011 | 2   | click-and-collect      | na                                      | 7126      | Fastline  | Robina cannot fulfill 2 more   |
