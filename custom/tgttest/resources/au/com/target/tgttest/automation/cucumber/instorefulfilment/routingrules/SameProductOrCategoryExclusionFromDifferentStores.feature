@cartProductData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Same Product Routing Rules from different stores exclusion

  Background: 
    Given stores with fulfilment capability and stock:
      | store       | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore | clearProductExclusions | clearCategoryExclusions |
      | Robina      | Yes     | QLD   | Yes            | Yes                      | Yes                         | true                   | true                    |
      | Rockhampton | Yes     | QLD   | Yes            | Yes                      | Yes                         | true                   | true                    |

  Scenario Outline: Store product exclusion for date and timestamp
    Given the store '<store>' product exclusion is '<productCode>' with start date '<exclusionStartDate>' and end date '<exclusionEndDate>'
    When a consignment sent to a store '<store>' with product:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | productCode | exclusionStartDate | exclusionEndDate | warehouse   | comment                                            | store |
      | W100000     | EXACTLY NOW        | NOT POPULATED    | Rockhampton | store product exclusion is active without end date |  7126 |
      | W100000     | EXACTLY NOW        | NOT POPULATED    | Robina      | store product exclusion is active without end date |  7049 |

  Scenario Outline: Store category exclusion for date and timestamp
    Given the store '<store>' category exclusion is '<categoryCode>' with start date '<exclusionStartDate>' and end date '<exclusionEndDate>'
    When a consignment sent to a store '<store>' with product:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | categoryCode | exclusionStartDate | exclusionEndDate | warehouse   | comment                                             | store |
      | W93746       | EXACTLY NOW        | NOT POPULATED    | Rockhampton | store category exclusion is active without end date |  7126 |
      | W93746       | EXACTLY NOW        | NOT POPULATED    | Robina      | store category exclusion is active without end date |  7049 |
