@notAutomated
Feature: Set Online From Date in hybris if stock is more then 0 and status is active and price is defined
  As a target online customer 
  I want to view products that are newly arrived on the product list page by setting OnlineFrom Date
  So that customer can differentiate new arrivals

  Background: 
    Given list of sellable colour variants which does not have size variants
      | productId | stock | status     | price       | onlineFromDate |
      | SKU001    | 0     | approved   | present     | not set        |
      | SKU002    | 5     | unapproved | present     | not set        |
      | SKU003    | 5     | approved   | not present | not set        |
      | SKU004    | 0     | approved   | present     | set            |
      | SKU005    | 5     | unapproved | present     | set            |
      | SKU006    | 5     | approved   | not present | set            |
      | SKU007    | 0     | unapproved | present     | not set        |
      | SKU008    | 0     | approved   | not present | not set        |
      | SKU009    | 0     | unapproved | present     | not set        |
      | SKU010    | 5     | unapproved | not present | not set        |
      | SKU011    | 0     | approved   | not present | not set        |
      | SKU012    | 5     | unapproved | not present | not set        |
    And list of sellable colour variant with size variants
      | cvProductId | svProductId | stock | status     | price       | onlineFromDate |
      | SKU100      |             |       | approved   |             | not set        |
      | SKU100      | SKU101      | 0     | approved   | present     |                |
      | SKU100      | SKU102      | 0     | approved   | present     |                |
      | SKU110      |             |       |            |             | not set        |
      | SKU110      | SKU111      | 5     | unapproved | present     |                |
      | SKU110      | SKU112      | 5     | unapproved | present     |                |
      | SKU120      |             |       |            |             | not set        |
      | SKU120      | SKU121      | 5     | approved   | not present |                |
      | SKU120      | SKU122      | 5     | approved   | not present |                |
      | SKU130      |             |       | approved   |             | set            |
      | SKU130      | SKU131      | 0     | approved   | present     | -              |
      | SKU130      | SKU132      | 0     | approved   | present     |                |
      | SKU140      |             |       |            |             | set            |
      | SKU140      | SKU141      | 5     | unapproved | present     |                |
      | SKU140      | SKU142      | 5     | unapproved | present     |                |
      | SKU150      |             |       |            |             | set            |
      | SKU150      | SKU151      | 5     | approved   | not present |                |
      | SKU150      | SKU152      | 5     | approved   | not present |                |
      | SKU160      |             |       | approved?  |             | not set        |
      | SKU160      | SKU161      | 0     | unapproved | present     |                |
      | SKU160      | SKU162      | 0     | unapproved | present     |                |
      | SKU170      |             |       | approved   |             | not set        |
      | SKU170      | SKU171      | 0     | approved   | not present |                |
      | SKU170      | SKU172      | 0     | approved   | not present |                |
      | SKU180      |             |       |            |             | not set        |
      | SKU180      | SKU181      | 0     | unapproved | present     |                |
      | SKU180      | SKU182      | 0     | unapproved | present     |                |
      | SKU190      |             |       |            |             | not set        |
      | SKU190      | SKU191      | 5     | unapproved | not present |                |
      | SKU190      | SKU192      | 5     | unapproved | not present |                |
      | SKU200      |             |       | active     |             | not set        |
      | SKU200      | SKU201      | 0     | approved   | not present |                |
      | SKU200      | SKU202      | 0     | approved   | not present |                |
      | SKU210      |             |       |            |             | not set        |
      | SKU210      | SKU211      | 5     | unapproved | not present |                |
      | SKU210      | SKU212      | 5     | unapproved | not present |                |

  Scenario Outline: 
    Given a sellable colour variant <productId>
    When <event> occurs for that product
    Then the action taken to the online from date is <action>
    And the online from date value for the product is <onlineFromDate>

    Examples: 
      | productId | event                   | action        | onlineFromDate |
      | SKU001    | stock becomes > 0       | set date      | now            |
      | SKU002    | status becomes approved | set date      | now            |
      | SKU003    | price becomes present   | set date      | now            |
      | SKU004    | stock becomes > 0       | do not change | unchanged      |
      | SKU005    | status becomes approved | do not change | unchanged      |
      | SKU006    | price becomes present   | do not change | unchanged      |
      | SKU007    | stock becomes > 0       | do not change | unchanged      |
      | SKU008    | stock becomes > 0       | do not change | unchanged      |
      | SKU009    | status becomes approved | do not change | unchanged      |
      | SKU010    | status becomes approved | do not change | unchanged      |
      | SKU011    | price becomes present   | do not change | unchanged      |
      | SKU012    | price becomes present   | do not change | unchanged      |

  Scenario Outline: 
    Given product <cvProductId>
    And that product is a colour variant that has sellable size variants
    When event <event> occurs for size variant product <svProductId>
    Then the action taken to the online from date on <cvProductId> is <action>
    And the online from date value for product <cvProductId> is <resultOnlineFromDate>

    Examples: 
      | cvProductId | svProductId | event                   | action        | resultOnlineFromDate |
      | SKU100      | SKU101      | stock becomes > 0       | set date      | now                  |
      | SKU110      | SKU111      | status becomes approved | set date      | now                  |
      | SKU120      | SKU121      | price becomes present   | set date      | now                  |
      | SKU130      | SKU131      | stock becomes > 0       | do not change | unchanged            |
      | SKU140      | SKU141      | status becomes approved | do not change | unchanged            |
      | SKU150      | SKU151      | price becomes present   | do not change | unchanged            |
      | SKU160      | SKU161      | stock becomes > 0       | do not change | unchanged            |
      | SKU170      | SKU171      | stock becomes > 0       | do not change | unchanged            |
      | SKU180      | SKU181      | status becomes approved | do not change | unchanged            |
      | SKU190      | SKU191      | status becomes approved | do not change | unchanged            |
      | SKU200      | SKU201      | price becomes present   | do not change | unchanged            |
      | SKU210      | SKU211      | price becomes present   | do not change | unchanged            |
