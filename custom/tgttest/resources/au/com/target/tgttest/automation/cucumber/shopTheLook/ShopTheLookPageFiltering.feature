@notAutomated
Feature: Shop The Look Page - Filtering
    As an online ShopTheLook customer, I need to add filter to what Looks I see

  Scenario: Filters available on ShopTheLook Page
    Given I am viewing the Target.com.au webpage as a Target online user
    When I click on ShopTheLook under a particular Category
    Then I see all the collection's filter associated with that category

  Scenario: Selecting ONE filter on ShopTheLook page
    Given I am on ShopTheLook page as a Target online user
    And I am viewing all the looks associated with that category
    When I select a filter on ShopTheLook page
    Then only the looks matching the filter are visible

  Scenario: Categories with (Less than TWO) ONE Collection Filter only
    Given I am on ShopTheLook page as a Target online user
    When any particular category has (Less than TWO) ONLY ONE Collection 
    Then There should be NO collection filter on ShopTheLook page

  Scenario: No more than ONE selection
    Given I am on ShopTheLook page as a Target online user
    And I have selected a collection
    When I try to select another collection
    Then The first one goes off
    And ShopTheLook page is filtered by recent selection

  Scenario: Filter names to have, numbers associated
    Given I am on ShopTheLook page as a Target online user
    When I select a filter collection
    Then the number of looks visible should match the count that was displayed on the filter
