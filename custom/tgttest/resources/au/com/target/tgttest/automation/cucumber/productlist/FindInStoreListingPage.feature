@notAutomated
Feature: Displaying Find in Store Icon in listing pages
  As a Target customer 
  I should be able to identify products which are sold out online/unavailable online but are available in store 
  So that I can find the product availability in stores via find a store option

  Scenario Outline: Find in store icon displayed for product having only colorvariants sellable(no size variants)
    Given a customer is on listing page
    And has the base product <basproductCode> of colorvariants <sellablevariants>
    And the product stocklevel is storestock <storestockstatus> ,onlineStock <onlineStockStatus>
    When the customer is viewing the product
    Then the find in store icon will be <displayStatus> as per the below scenario

    Examples: 
      | baseProduct | sellableVariants | onlineStockStatus     | StoreStockStatus      | displayStatus |
      | P1000       | CP1000,CP1001    | instock,instock       | instock,instock       | not displayed |
      | P1001       | CP1002,CP1003    | instock,outofstock    | instock,instock       | not displayed |
      | P1002       | CP1004,CP1005    | outofstock,outofstock | instock,instock       | displayed     |
      | P1003       | CP1006,CP1007    | outofstock,outofstock | instock,outofstock    | displayed     |
      | P1004       | CP1006,CP1007    | outofstock,outofstock | outofstock,outofstock | not displayed |

  Scenario Outline: Find in store icon displayed for product having size variants sellable ,single colorvariant (product is available in 1 color and multiple sizes)
    Given a customer is on listing page
    And has the base product <basproductCode> of colorvariant <colorvariant> and size variants <sizeVariants>
    And the product stocklevel is storestock <storestockstatus> ,onlineStock <onlineStockStatus>
    When the customer is viewing the product
    Then the find in store icon will be <displayStatus> as per the below scenario

    Examples: 
      | baseProduct | colorvariant | sizeVariant     | onlineStockStatus     | storeStockStatus      | displayStatus |
      | P1000       | CP1000       | SP10001,SP10002 | instock,instock       | instock,instock       | not displayed |
      | P1001       | CP1002       | SP10003,SP10004 | instock,outofstock    | instock,instock       | not displayed |
      | P1002       | CP1004       | SP10005,SP10006 | outofstock,outofstock | instock,instock       | displayed     |
      | P1003       | CP1005       | SP10008,SP10007 | outofstock,outofstock | instock,outofstock    | displayed     |
      | P1004       | CP1006       | SP10009,SP10010 | outofstock,outofstock | outofstock,outofstock | not displayed |

  Scenario Outline: Find in store icon displayed for product having size variants sellable for a multi-color variant product (product is available in multiple sizes and color combination)
    Given a customer is on listing page
    And has the base product <basproductCode> of color variants <colorvariants> and size variants <sizeVariants>
    And the product stocklevel is storestock <storestockstatus> ,onlineStock <onlineStockStatus>
    When the customer is viewing the product
    Then the find in store icon will be <displayStatus> as per the below scenario
      | baseProduct | colorvariants | sizeVariants                     | onlineStockStatus                             | storeStockStatus                              | displayStatus |
      | P1          | CP1, CP2      | CP1-S1,CP1-SP2, CP2-SP1, CP2-SP2 | instock,instock, instock, instock             | instock,instock, instock, instock             | not displayed |
      | P2          | CP1, CP2      | CP1-S1,CP1-SP2, CP2-SP1, CP2-SP2 | instock,instock, instock, outofstock          | instock,instock,instock, instock              | not displayed |
      | P3          | CP1, CP2      | CP1-S1,CP1-SP2, CP2-SP1, CP2-SP2 | outofstock,outofstock, outofstock, outofstock | instock,instock, outofstock, outofstock       | displayed     |
      | P4          | CP1, CP2      | CP1-S1,CP1-SP2, CP2-SP1, CP2-SP2 | outofstock,outofstock, outofstock, outofstock | instock,outofstock, outofstock, outofstock    | displayed     |
      | P5          | CP1, CP2      | CP1-S1,CP1-SP2, CP2-SP1, CP2-SP2 | outofstock,outofstock, outofstock, outofstock | outofstock,outofstock, outofstock, outofstock | not displayed |

  Scenario Outline: Find in store icon displayed for product
    Given a customer is on listing page
    And has the base product <basproductCode> of colorvariant <colorvariants> and size variants <sizeVariants>
    And facet is selected from the <colorvariants> say the product has 2 colors and one color "CP1000" is selected <selectedFacet>
    And the product stocklevel is storestock <storestockstatus> ,onlineStock <onlineStockStatus>
    When the customer is viewing the product
    Then the find in store icon will be <displayStatus> as per the below scenario where the stock of CP2000 is not considered

    Examples: 
      | baseProduct | colorvariants  | sizeVariants | selectedFacet | onlineStockStatus     | storeStockStatus      | displayStatus |
      | P1000       | CP1000, CP2000 | SP11,SP12    | CP1000        | instock,instock       | instock,instock       | not displayed |
      | P1000       | CP1000, CP2000 | SP13,SP14    | CP1000        | instock,outofstock    | instock,instock       | not displayed |
      | P1000       | CP1000, CP2000 | SP15,SP16    | CP1000        | outofstock,outofstock | instock,instock       | displayed     |
      | P1000       | CP1000, CP2000 | SP17,SP18    | CP1000        | outofstock,outofstock | instock,outofstock    | displayed     |
      | P1000       | CP1000, CP2000 | SP19,S20     | CP1000        | outofstock,outofstock | outofstock,outofstock | not displayed |
