@stepImportData
Feature: Import ebay Express Delivery option from STEP
  In order to ensure ebay Express Delivery option available on apparel and non-apparel products
  As Target Online
  I want to specify in STEP whether a product can be delivered via the eBay Express Delivery method or not

  Scenario Outline: Import ebay Express Delivery option as part of STEP import.
    Given a product in STEP with code '<productCode>'
    And available on ebay flag in STEP is '<availableOnEbay>'
    And ebay express delivery flag in STEP is '<ebayExpressDeliveryFlag>'
    When run STEP product import process
    Then product import response is successful
    And ebay express delivery mode in hybris is '<ebayExpressDeliveryMode>'
    And ebay Home delivery mode in hybris is '<ebayHomeDeliveryMode>'

    Examples: 
      | productCode | availableOnEbay | ebayExpressDeliveryFlag | ebayExpressDeliveryMode | ebayHomeDeliveryMode |
      | 1110001     | Y               | Y                       | added                   | added                |
      | 1110002     | Y               | N                       | not added               | added                |
      | 1110003     | N               | Y                       | not added               | not added            |
      | 1110004     | N               | N                       | not added               | not added            |
