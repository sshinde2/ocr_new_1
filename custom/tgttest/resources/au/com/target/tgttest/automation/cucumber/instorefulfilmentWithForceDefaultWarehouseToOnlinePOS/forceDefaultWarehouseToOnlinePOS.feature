@cartProductData @deliveryModeData @storeFulfilmentCapabilitiesData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Force default warehouse to online POS

  Scenario: fastline order completes instore
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    When a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And Switch the user to be admin
    Then the assigned warehouse will be 'Fastline'
    And consignment carrier is 'AustraliaPostInstoreCNC'
    And the consignment OFC order type is 'INTERSTORE_DELIVERY'
    And consignment is picked by Fastline without order extract
    When the consignment is completed instore
    Then inter store transfer is not triggered
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 8         |

  Scenario: Throw expection when place a express delivery order to fastline when the forceDefaultWarehouseToOnlinePOS enabled
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    When delivery mode is 'express-delivery'
    Then order is placed with errors

  Scenario: No open order pull back for default warehouse
    Given some existing orders routed to store:
      | order       | warehouse | consignmentStatus      | minutesSinceCreated |
      | auto1000010 | Fastline  | CONFIRMED_BY_WAREHOUSE | 81                  |
    And order age tolerance is set to 80 Minutes
    And the excludedStates are 'NULL' and includedStates are 'NULL' for the pullback job
    When the open orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus   | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000010 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Fastline                |
    And clear states config in the pullback job

  Scenario: No waved order pull back for default warehouse
    Given some existing orders routed to store:
      | order       | warehouse | consignmentStatus | minutesSinceWaved |
      | auto1000010 | Fastline  | WAVED             | 181               |
    And accepted orders timeout period is 180 minutes
    When accepted orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000010 | WAVED                | NULL                 | N/A                  | Fastline                |
