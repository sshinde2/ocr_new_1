@notAutomated
Feature: IPG - Handle payment cancel
  As an online customer
  I want to be able to cancel split payment after entering at least one card details
  So that I can restart entering card information again
  
  Scenario : Customer is able to cancel payment after entering card details in split payment
    Given customer chose to pay with multiple cards
    And cutomer pays with a gift card
      | cardNumber        | amount |
      | 62734500000000002 | $10.00 |
    And payment summary section is displayed
    When customer clicks on 'Cancel' button
    Then review order page is displayed
    And new iframe is displayed
    And card details entered previously are cleared
    
