@cartProductData @voucherData @deliveryModeData @currencyConversionFactorsData @initPreOrderProducts 
Feature: TLOG - Placing orders creates tlog sale entries
  In order to integrate with POS, as Target online store we want
  SALE TLOG entries to be created for placed orders.
  
  Use 'valid' or 'invalid' for valid/invalid tmd numbers
  Use code='tmd' for the expected code in transDiscount, to verify against supplied tmd.

  Background: 
    Given giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |

  Scenario Outline: Placing an order with home-delivery, no deals or discounts.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 1500  | 1500      |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 3900
    And tlog credit card payment info matches
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with click-and-collect, no deals or discounts.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And delivery mode is 'click-and-collect'
    And payment method is '<payment_method>'
    When order is placed
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 1500  | 1500      |
    And tlog type is 'sale'
    And tlog shipping is 500
    And tlog total tender is 3500
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with TMD only.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And payment method is '<payment_method>'
    When order is placed
    Then order total is 33.0
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000011 | 2   | 1500  | 1500      | staff            | 20              | 600                |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 3300
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And transDiscount entries are:
      | type  | code | pct | amount |
      | staff | tmd  | 20  | 600    |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with buy get deal items.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    Then order total is 130.0
    And tlog entries are:
      | code     | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown |
      | 10000011 | 1   | 5000  | 5000      | Q            | 9110       | 1                | 0                |
      | 10000011 | 1   | 5000  | 5000      | Q            | 9110       | 1                | 0                |
      | 10000012 | 1   | 2000  | 2000      | R            | 9110       | 1                | 1000             |
      | 10000012 | 1   | 2000  | 2000      |              |            |                  |                  |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 13000
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And transDeal entries are:
      | type | id   | instance | markdown |
      | 1    | 9110 | 1        | 1000     |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with buy get deal and tmd.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And payment method is '<payment_method>'
    When order is placed
    Then order total is 104
    And tlog entries are:
      | code     | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000011 | 1   | 5000  | 5000      | Q            | 9110       | 1                | 0                | staff            | 20              | 1000               |
      | 10000011 | 1   | 5000  | 5000      | Q            | 9110       | 1                | 0                | staff            | 20              | 1000               |
      | 10000012 | 1   | 2000  | 2000      | R            | 9110       | 1                | 1000             | staff            | 20              | 200                |
      | 10000012 | 1   | 2000  | 2000      |              |            |                  |                  | staff            | 20              | 400                |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 10400
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And transDeal entries are:
      | type | id   | instance | markdown |
      | 1    | 9110 | 1        | 1000     |
    And transDiscount entries are:
      | type  | code | pct | amount |
      | staff | tmd  | 20  | 2600   |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with a voucher.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And delivery mode is 'home-delivery'
    And customer presents voucher 'VOUCHER-TV1'
    And payment method is '<payment_method>'
    When order is placed
    Then order total is 29
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 1500  | 1500      |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 2900
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And transDiscount entries are:
      | type  | code | style      | amount |
      | promo | TV1  | dollar off | 1000   |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario: Placing an order with home-delivery, no deals or discounts, split payment with a failed payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry | declined |
      | GC1,40       | false    |
      | CC1,60       | true     |
    And order is placed
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry | declined |
      | GC2,40       | false    |
      | CC2,60       | false    |
    When order is placed
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 5000  | 5000      |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 10000
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And tlog tender entries are:
      | cardNumber        | approved | amount |
      | 62779575000000000 | true     | 40     |
      | 5587654321010013  | true     | 60     |

  Scenario: Send sales Tlog after placing a TradeMe order(Currency Exchange rate is 1.07439)
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | TradeMe       | TradeMe      | NZD      | NZ      | 10            | 2016-01-06T04:05:00 |
    And the order is imported in hybris
    When a new order in hybris is created
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 1612  | 1612      |
    And tlog type is 'sale'
    And tlog shipping is 1074
    And tlog total tender is 4298
    And tlog currency is 'AUD'
    And tlog sales channel is 'TradeMe'
    And tlog tender type is 'CASH'

  Scenario: Place an order using Afterpay
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And payment method is 'afterpay'
    When order is placed
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 5000  | 5000      |
    And tlog type is 'sale'
    And tlog currency is 'AUD'
    And tlog shipping is 0
    And tlog total tender is 10000
    And tlog sales channel is 'Web'
    And tlog tender type is 'AFTERPAY'
    And tlog afterpay sale info is valid

  Scenario: Place an order using zip payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And payment method is 'zippay'
    When order is placed
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 2   | 5000  | 5000      |
    And tlog type is 'sale'
    And tlog currency is 'AUD'
    And tlog shipping is 0
    And tlog total tender is 10000
    And tlog sales channel is 'Web'
    And tlog tender type is 'ZIP'
    And tlog sale info is valid for 'zip'

  Scenario: Placing an order with pre order product, home-delivery without TMD
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 |   1 |    36 |
    And delivery mode is 'home-delivery'
    And the delivery address is '14 Emert street,Wentworthville,NSW,2145'
    And payment method is 'creditcard'
    When pre order is placed
    Then tlog entries are:
      | code             | qty | price | filePrice |
      | V5555_preOrder_5 |   1 |  3600 |      3600 |
    And tlog type is 'laybyCreate'
    And tlog layby type is 'P'
    And tlog layby version is '4'
    And tlog shipping is 900
    And tlog payment amount is equal to initial deposit
		And tlog dueDate is equal to order normalSalesDate    
    And tlog layby fee is 0
    And tlog total tender is 1000
    And tlog credit card payment info matches
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And tlog customer information are:
      | firstName | lastName | emailAddress              |
      | firstname | lastname | test.guest1@target.com.au |
    And tlog customer Addresses are:
      | addressLine1      | town           | postalCode | region         | country   | shippingAddress | billingAddress |
      | 1 Aberdeen Street | Newtown        |       3220 | Newtown        | Australia | false           | true           |
      | 14 Emert street   | Wentworthville |       2145 | Wentworthville | Australia | true            | false          |

  Scenario: Placing an order with pre order product, home-delivery with TMD
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 |   1 |    36 |
    And delivery mode is 'home-delivery'
    And the delivery address is '14 Emert street,Wentworthville,NSW,2145'   
    And customer presents tmd 'valid'
    And payment method is 'creditcard'
    When pre order is placed
    Then tlog entries are:
      | code             | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | V5555_preOrder_5 |   1 |  3600 |      3600 | staff            |              20 |                720 |
    And tlog type is 'laybyCreate'
    And tlog layby type is 'P'
    And tlog layby version is '4'
    And tlog shipping is 900
    And tlog payment amount is equal to initial deposit
		And tlog dueDate is equal to order normalSalesDate    
    And tlog layby fee is 0
    And tlog total tender is 1000
    And tlog credit card payment info matches
    And tlog currency is 'AUD'
    And tlog sales channel is 'Web'
    And transDiscount entries are:
      | type  | code | pct | amount |
      | staff | tmd  |  20 |    720 |
    And tlog customer information are:
      | firstName | lastName | emailAddress              |
      | firstname | lastname | test.guest1@target.com.au |
    And tlog customer Addresses are:
      | addressLine1      | town           | postalCode | region         | country   | shippingAddress | billingAddress |
      | 1 Aberdeen Street | Newtown        |       3220 | Newtown        | Australia | false           | true           |
      | 14 Emert street   | Wentworthville |       2145 | Wentworthville | Australia | true            | false          |
    