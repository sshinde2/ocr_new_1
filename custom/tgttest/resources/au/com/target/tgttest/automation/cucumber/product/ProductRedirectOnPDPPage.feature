@cartProductData
Feature: Select Product when loading PDP page.
  
         As a customer, 
         I want the Variant product automatically selected if I am viewing the PDP for an item with only one available variants,
         so I can more quickly and easily add the item to my basket.
  
  Notes:
  * Note instock means available-reserved > 0
  * feature productvaraint.defaultselect should be ON
  * The product details are setup as:
  
  | baseProduct | ColourVariantCode | SizeVariantCodes                    | 
  | W100000     | 10000010,10000020 | 10000011,10000012;10000021,10000022 | 
  | W100001     | 10000110,10000120 | 10000111,10000112;10000121,10000122 | 
  | W100002     | 10000210,10000220 | 10000211,10000212;10000221,10000222 | 
  | W100003     | 10000310,10000320 | 10000311,10000312;10000321,10000322 | 
  | W100016     | 10001031,10001032 | NA                                  | 
  | W100017     | 10001033,10001034 | NA                                  | 
  | W100018     | 10001035,10001036 | NA                                  |

  Scenario Outline: Colour variants with size variants some in stock
    Given sellable Variants stock availablity as below:
      | sellableVariants                    | stockAvailability |
      | 10001031,10001032                   | n,y               |
      | 10001033,10001034                   | n,n               |
      | 10001035,10001036                   | y,y               |
      | 10000011,10000012,10000021,10000022 | y,y,y,y           |
      | 10000111,10000112,10000121,10000122 | y,y,n,y           |
      | 10000211,10000212,10000221,10000222 | n,n,y,y           |
      | 10000311,10000312,10000321,10000322 | n,y,y,y           |
    When product to redirect is retrieved for '<prodCode>'
    Then product to redirect is '<redirect>'

    Examples: 
      | prodCode | redirect | comment                                                         |
      | W100000  | 10000010 | More than one size variant in stock on the cv so cv is returned |
      | W100001  | 10000110 | Base product has two colour variants and redirects to first cv  |
      | W100002  | 10000220 | Base in, first cv not in stock second has two sv in stock       |
      | W100003  | 10000312 | Base in, redirects straight down to sv                          |
      | W100016  | 10001032 | Base in, colour variants are sellable, only one in stock        |
      | W100017  | 10001033 | Base in, no colour variants in stock so first is returned       |
      | W100018  | 10001035 | Base in, just show the first cv in stock                        |
      | 10000020 | 10000020 | Colour variant has two size variants in stock                   |
      | 10000120 | 10000122 | Colour variant has two size variants only second is in stock    |
      | 10000210 | 10000210 | Colour variant has no svs in stock                              |
      | 10000121 | 10000121 | Size variant passed in                                          |
