@cartProductData @ntlFulfilmentData
Feature: Assign partial order to NTL Warehouse if partial fulfilment is possible after Lookup for Order Stock Level
  As a supply chain team member
  I want the order to be partially fulfilled, where possible, by the Toll RFC in the same state as the delivery destination
  So that the delivery costs are minimised
  
  Notes:
    NTLs with status and stock:
      | ntl          | enabled | stock                                       |
      | NSWONLINEBNB | yes     | 2 of 10000411, 0 of 10000510, 5 of 10000021 |
      | QLDONLINEBNB | yes     | 1 of 10000411, 1 of 10000510, 3 of 10000021 |
      | WAONLINEBNB  | no      | 5 of 10000411, 0 of 10000510, 3 of 10000021 |

  Background: 
    Given set fastline stock:
      | product  | available | reserved |
      | 10000411 | 10        | 0        |
      | 10000510 | 10        | 0        |
      | 10000021 | 10        | 0        |
      | 10000011 | 10        | 0        |
    And stores with fulfilment capability and stock:
      | store  | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | Robina | No      | QLD   | Yes            | Yes                      | Yes                         |
      | Morley | Yes     | WA    | Yes            | Yes                      | Yes                         |

  Scenario Outline: Assign order or line item to NTL according to fulfilment capability and stock
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    And global order routing is <orderRoutingSetting>
    And delivery mode is '<deliveryMode>'
    And the delivery address is '<deliveryAddress>'
    When order is placed
    Then consignmentOne assigned warehouse will be '<warehouseOne>' containing product '<productInConsignmentOne>' and assigned carrier '<carrierOne>'
    And consignmentTwo assigned warehouse will be '<warehouseTwo>' containing product '<productInConsignmentTwo>' and assigned carrier '<carrierTwo>'

    Examples: 
      | productsInCart    | qty | orderRoutingSetting | deliveryMode      | deliveryAddress                           | warehouseOne | productInConsignmentOne | carrierOne | warehouseTwo | productInConsignmentTwo | carrierTwo | comment                                                |
      | 10000411,10000510 | 2,1 | Disabled            | click-and-collect | '8/15, Helles Ave, Moorebank, NSW, 2170'  | Fastline     | 10000411,10000510       | TollCnC    | N/A          | N/A                     | N/A        | All CNC goes to Fastline                               |
      | 10000411,10000510 | 1,3 | Disabled            | home-delivery     | '8/15, Helles Ave, Moorebank, NSW, 2170'  | NSWONLINEBNB | 10000411                | Toll       | Fastline     | 10000510                | Toll       | NTL fulfil partially. Order Splitted                   |
      | 10000021          | 1,2 | Disabled            | express-delivery  | '160, Terrace Place, Murarrie, QLD, 4172' | QLDONLINEBNB | 10000021                | StarTrack  | N/A          | N/A                     | N/A        | NTL fulfil entire order                                |
      | 10000411,10000021 | 1,1 | Disabled            | home-delivery     | '180, Drake Street, Morley, WA, 6062'     | Fastline     | 10000411,10000021       | Toll       | N/A          | N/A                     | N/A        | NTL disabled. Fastline fulfils                         |
      | 10000411,10000510 | 1,1 | Enabled             | home-delivery     | '180, Drake Street, Morley, WA, 6062'     | Fastline     | 10000411,10000510       | Toll       | N/A          | N/A                     | N/A        | Instore order fulfilled by fastline                    |
      | 10000411,10000510 | 3,3 | Disabled            | home-delivery     | '8/15, Helles Ave, Moorebank, NSW, 2170'  | Fastline     | 10000411,10000510       | Toll       | N/A          | N/A                     | N/A        | No Instore, No Stock in NTL - No split, Fastline       |
      | 10000411,10000021 | 2,4 | Enabled             | home-delivery     | '160, Terrace Place, Murarrie, QLD, 4172' | Fastline     | 10000411,10000021       | Toll       | N/A          | N/A                     | N/A        | No Stock Instore, No Stock in NTL - No split, Fastline |
      | 10000021,10000411 | 1,1 | Enabled             | home-delivery     | '160, Terrace Place, Murarrie, QLD, 4172' | QLDONLINEBNB | 10000021,10000411       | Toll       | N/A          | N/A                     | N/A        | No Stock Instore, NTL has all stock - No split, NTL    |
      | 10000510,10000411 | 1,2 | Enabled             | home-delivery     | '160, Terrace Place, Murarrie, QLD, 4172' | QLDONLINEBNB | 10000510                | Toll       | Fastline     | 10000411                | Toll       | No Stock Instore, NTL has partial stock - Split        |
