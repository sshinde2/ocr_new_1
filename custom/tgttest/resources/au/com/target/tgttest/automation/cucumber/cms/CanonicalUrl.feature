@notAutomated
Feature: Canonical URL tag to specific content pages
    In order to be able to identify content pages with duplicate content to search engine spiders
    As a content producer
    I want canonical URL tag specific to content pages

  Scenario Outline: Configure canonical URL from CMS
    Given A <page_type>
    When the page is configured from CMS
    Then canonical URL field should be <populated_status>

    Examples: 
      | page_type     | populated_status |
      | content page  | populated        |
      | landing page  | not populated    |
      | listing page  | not populated    |
      | blog page     | populated        |
      | catalog page  | not populated    |
      | category page | not populated    |
      | email page    | not populated    |
      | product page  | not populated    |
      | size page     | not populated    |
      | sms page      | not populated    |

  Scenario Outline: Validate the URL
    Given user is editing a content page
    When user enter the canonical URL as <url>
    Then error message <status> display

    Examples: 
      | url                       | status     |
      | http://www.target.com.au  | should not |
      | https://www.target.com.au | should not |
      | www.target.com.au         | should     |
      | test                      | should     |

  Scenario Outline: Generating canonical URL
    Given A content page
    And Canonical URL field is <status>
    When search engine spider crawls the page
    Then canonical URL should be <populated_status>

    Examples: 
      | status      | populated_status |
      | present     | populated        |
      | not present | not populated    |
