@cartProductData @deliveryModeData @initPreOrderProducts
Feature: As a target online customer
    I need to be shown order information on Basket Page. So that I can see the information set for order product.

  Scenario: Verify promotional messages for pre-order product in basket
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 |   1 |   400 |
    And a product with 'P55555_preOrder_5'
    And delivery promotion ranks order is 'express'
    And promotional messages set for the delivery modes are:
      | code              | incentiveMetMessage                            | potentialIncentiveMessage                            |
      | click-and-collect | IncentiveMetMessage Test for click and collect | PotentialIncentiveMessage Test for click and collect |
      | home-delivery     | IncentiveMetMessage Test for home delivery     | PotentialIncentiveMessage Test for home delivery     |
      | express-delivery  | IncentiveMetMessage Test for express delivery  | PotentialIncentiveMessage Test for express delivery  |
    When get cart data
    Then promotional messages retrieved for the concerned delivery modes are:
      | code              | incentiveMetMessage | potentialIncentiveMessage |
      | express-delivery  | null                | null                      |

  Scenario: Verify promotional messages for normal product in basket
    Given a cart with entries:
      | product   | qty | price |
      | 100010901 |   1 |   400 |
    And a product with 'W1000001'
    And promotional messages set for the delivery modes are:
      | code              | incentiveMetMessage                            | potentialIncentiveMessage                            |
      | click-and-collect | IncentiveMetMessage Test for click and collect | PotentialIncentiveMessage Test for click and collect |
      | home-delivery     | IncentiveMetMessage Test for home delivery     | PotentialIncentiveMessage Test for home delivery     |
      | express-delivery  | IncentiveMetMessage Test for express delivery  | PotentialIncentiveMessage Test for express delivery  |
    When get cart data
    Then promotional messages retrieved for the concerned delivery modes are:
      | code              | incentiveMetMessage                            | potentialIncentiveMessage |
      | click-and-collect | IncentiveMetMessage Test for click and collect | null                      |
      | home-delivery     | IncentiveMetMessage Test for home delivery     | null                      |
      | express-delivery  | IncentiveMetMessage Test for express delivery  | null                      |

  Scenario: Verify preOrder deposit and outstanding amount on basket page
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 |   1 |   400 |
    When customer goes to basket page
    Then the basket page for pre-order product includes:
      | preOrderDepositAmount | preOrderOutstandingAmount |
      |                  10.0 |                     390.0 |
      
  Scenario: preOrder deposit and outstanding amount not available for normal products
    Given a cart with entries:
      | product          | qty | price |
      | 100010901        |   1 |   20  |
    When customer goes to basket page
    Then the basket page for normal product includes:
      |totalPrice | preOrderDepositAmount | preOrderOutstandingAmount |
      | 20        |                       |                           |
