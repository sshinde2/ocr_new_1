@notAutomated
Feature: "Google Analytics Event" on listing page
  Analytics of customer interactions on the listing pages

  Scenario: Facets will track the event
    Given A target customer on a listing page
    When I click on a facet option in the left hand menu that is not selected
    Then An analytics event fires with the label "Facet", the category & selection

  Scenario: Facet will track whether it is select or deselect
    Given A target customer on a listing page
    When I click on a facet option in the left hand menu that is not selected
    Then An analytics event fires with the label "Facet Select", the category & selection
    When I click on a facet option in the left hand menu that is selected
    Then An analytics event fires with the label "Facet Deselect", the category & selection

  Scenario: Facet will count how many times it is interacted with
    Given A target customer on a listing page
    When I click on a facet option in the left hand menu that is not selected
    Then An analytics event fires with the label "Facet", "Count" & the count of how many times I've clicked on a facet

  Scenario Outline: Changing sort method will fire an event
    Given A target customer on a listing page
    When I change the sort option to <selection>
    Then An analytics event fires for <label>

    Examples: 
      | selection      | label                   |
      | Latest         | Sort by: Latest         |
      | Highest Rating | Sort by: Highest Rating |
      | Price High-Low | Sort by: Price High-Low |
      | Price Low-High | Sort by: Price Low-High |
      | Product A-Z    | Sort by: Product A-Z    |
      | Product Z-A    | Sort by: Product Z-A    |
      | Brand          | Sort by: Brand          |
      | Most Popular   | Sort by: Most Popular   |

  Scenario Outline: Changing page from top of screen
    Given A target customer on a listing page
    When I click the <change button> button
    Then An analytics event fires for <label>

    Examples: 
      | change button | label                            |
      | Next          | Change page: Next Page - Top     |
      | Previous      | Change page: Previous Page - Top |

  Scenario Outline: Changing page from bottom of screen
    Given A target customer on a listing page
    When I click the <change button> button
    Then An analytics event fires for <label>

    Examples: 
      | change button | label                               |
      | Next          | Change page: Next Page - Bottom     |
      | Previous      | Change page: Previous Page - Bottom |

  Scenario Outline: Changing number of results per page
    Given A target customer on a listing page
    When I click the select <range> results per page
    Then An analytics event fires for <label>

    Examples: 
      | range | label                |
      | 30    | Results per page: 30 |
      | 60    | Results per page: 60 |
      | 90    | Results per page: 90 |

  Scenario: Clicking Refine Button
    Given A target customer on a listing page
    When I click the refine button on mobile
    Then An analytics event fires for "Open Refine"

  Scenario: Closing Refine Menu
    Given A target customer on a listing page
    When I click the refine button on mobile
    Then An analytics event fires for "Close Refine"

  Scenario: Clear All Facets
    Given A target customer on a listing page
    And I have selected a number of facets
    When I click the clear all button
    Then An analytics event fires for "Clear"
