@cartProductData @cleanReverseGiftcardProcessData
Feature: IPG - Use ipg to place gift card / mixed card order
  In order to purchase products
  As an online customer
  I want to use make payment via ipg using both giftcards and creditcards up to 4 different combinations, 
  and the order will be placed according to payment result and stock level.

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | GC3       | Giftcard   | 62779575000000001 | 01/20      | 123456789 |             | 32  |
      | GC4       | Giftcard   | 62733500000000003 | 01/20      | 123456789 |             | 33  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |
      | CC3       | AMEX       | 349876543210010   | 03/20      | 123456789 | 01/03/2020  | 37  |
      | CC4       | DINERS     | 36765432100028    | 04/20      | 123456789 | 01/04/2020  | 38  |
    And reverse giftcard process retry interval is set to 2 times every 20000 milliseconds

  Scenario Outline: Make successful payment with combination of credit cards and gift cards
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And the payment details of the transaction will be recorded correctly

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,10   | GC2,10   | GC3,10   | GC4,9    |
      | GC2,10   | GC3,10   | GC4,19   |          |
      | GC3,20   | GC4,19   |          |          |
      | GC4,39   |          |          |          |
      | CC1,10   | CC2,10   | CC3,10   | CC4,9    |
      | CC2,10   | CC3,10   | CC4,19   |          |
      | CC3,10   | CC4,29   |          |          |
      | CC4,39   |          |          |          |
      | GC1,10   | CC1,29   |          |          |
      | GC1,10   | CC1,10   | CC2,19   |          |
      | GC1,10   | CC1,10   | CC2,10   | CC3,9    |
      | GC1,10   | GC2,10   | CC1,19   |          |
      | GC1,10   | GC2,10   | CC1,10   | CC2,9    |
      | GC1,10   | GC2,10   | GC3,10   | CC1,9    |

  Scenario Outline: Reverse gift card payments if credit card is declined
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And credit card 'CC1' is declined
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    And all successful payment should be reversed

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,10   | CC1,29   |          |          |
      | GC1,10   | CC2,10   | CC1,19   |          |
      | GC1,10   | CC2,10   | CC1,10   | CC3,9    |
      | GC1,10   | GC2,10   | CC1,19   |          |
      | GC1,10   | GC2,10   | CC1,10   | CC2,9    |
      | GC1,10   | GC2,10   | GC3,10   | CC1,9    |

  Scenario Outline: Handle the unsuccessful giftcard reversal, if the credit card is declined
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And credit card 'CC1' is declined
    And refund for gift card 'GC1' is declined
    And order is placed
    When reverse giftcard process is completed
    Then a CS ticket is created
    And place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    And The payment of amount '10' is not reversed

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,10   | CC1,29   |          |          |
      | GC1,10   | CC2,10   | CC1,19   |          |
      | GC1,10   | CC2,10   | CC1,10   | CC3,9    |
      | GC1,10   | GC2,10   | CC1,19   |          |
      | GC1,10   | GC2,10   | CC1,10   | CC2,9    |
      | GC1,10   | GC2,10   | GC3,10   | CC1,9    |

  Scenario Outline: Handle the unsuccessful giftcard reversal, if the cart value changes after customer submits the Ipg payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And the quantity added to cart for the '10000012' is '2'
    And refund for gift card 'GC1' is declined
    And there are gift card payment attempts:
      | cardReceipt | cardAuthorizedAmount | isPaymentSuccess |
      | 65824152    | 20.65                | true             |
    And order is placed
    When reverse giftcard process is completed
    Then a CS ticket is created
    And place order result is 'PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,10   | CC1,29   |          |          |
      | GC1,10   | CC2,10   | CC1,19   |          |
      | GC1,10   | CC2,10   | CC1,10   | CC3,9    |
      | GC1,10   | GC2,10   | CC1,19   |          |
      | GC1,10   | GC2,10   | CC1,10   | CC2,9    |
      | GC1,10   | GC2,10   | GC3,10   | CC1,9    |

  Scenario Outline: Handle the unsuccessful giftcard reversal, if the stock changes after customer submits the Ipg payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And the product '10000011' is out of stock before place order
    And refund for gift card 'GC1' is declined
    And there are gift card payment attempts:
      | cardReceipt | cardAuthorizedAmount | isPaymentSuccess |
      | 65824152    | 10.0000000000        | true             |
    And order is placed
    When reverse giftcard process is completed
    Then a CS ticket is created
    And the amount in the cs ticket is '10.00'
    And place order result is 'PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

    Examples: 
      | payment1        | payment2 | payment3 | payment4 |
      | GC1,10.00000000 | CC1,29   |          |          |
      | GC1,10.000      | CC2,10   | CC1,19   |          |
      | GC1,10.00       | CC2,10   | CC1,10   | CC3,9    |
      | GC1,10.0        | GC2,10   | CC1,19   |          |
      | GC1,10          | GC2,10   | CC1,10   | CC2,9    |
      | GC1,10          | GC2,10   | GC3,10   | CC1,9    |
