@cartProductData @storeFulfilmentCapabilitiesData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Routing Rules for Store based on product and category exclusions
  As the online store, I want to be able to route the consignment based on the exclusion list that would contain either a list of Products
  or Category in a given date range.
  
  This rule would be checked at global as well as store levels.

  Background: 
    Given stores with fulfilment capability:
      | store  | instoreEnabled | deliveryModesAllowed            | allowDeliveryToSameStore | allowDeliveryToAnotherStore | inStock |
      | Robina | Yes            | click-and-collect,home-delivery | Yes                      | yes                         | yes     |

  Scenario Outline: Orders sent to store based on global product exclusion list
    Given a cart with entries '<cartEntries>'
    And the '<exclusionStartDate>' and '<exclusionEndDate>' are set for the '<product>'
    And delivery mode is 'home-delivery'
    And the delivery address is '446,Ann Street,Brisbane City,QLD,4000'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | product  | cartEntries                   | exclusionStartDate | exclusionEndDate | warehouse | comment                                                                                         |
      | 10000011 | 1 of 10000011                 | BEFORE NOW         | NOT POPULATED    | Fastline  | start date is in past and no endDate set, so product excluded indefinately                      |
      | 10000011 | 1 of 10000011                 | BEFORE NOW         | AFTER NOW        | Fastline  | Product ordered is excluded at the time of place order                                          |
      | 10000011 | 1 of 10000011                 | BEFORE NOW         | BEFORE NOW       | Robina    | both StartDate and EndDate in past, product not excluded                                        |
      | 10000011 | 1 of 10000012                 | AFTER NOW          | NOT POPULATED    | Robina    | startDate in future and no EndDate, product will not be excluded                                |
      | 10000011 | 1 of 10000011                 | AFTER NOW          | AFTER NOW        | Robina    | Both startDate and endDate in future, product not excluded                                      |
      | 10000011 | 1 of 10000011                 | NOT POPULATED      | NOT POPULATED    | Fastline  | startDate and endDate not set, product would be excluded indefinately                           |
      | 10000011 | 1 of 10000011                 | NOT POPULATED      | AFTER NOW        | Fastline  | no startDate set and endDate in future, product will be excluded                                |
      | 10000011 | 1 of 10000011                 | NOT POPULATED      | BEFORE NOW       | Robina    | no startDate set and endDate in past, product will not be excluded                              |
      | 10000011 | 1 of 10000011,  1 of 10000012 | BEFORE NOW         | AFTER NOW        | Fastline  | one of the product in the cart is in the exclusion list, order will not be sent to the store    |
      | 10000011 | 1 of 10000011                 | EXACTLY NOW        | AFTER NOW        | Fastline  | Edge case : startDate is at the time of placing order, the order will be part of exclusion list |
