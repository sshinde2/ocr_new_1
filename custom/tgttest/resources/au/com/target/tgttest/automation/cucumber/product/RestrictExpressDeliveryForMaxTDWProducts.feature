@cartProductData
Feature: Restrict express delivery for products with max TDW
  
  
  In order to restrict express delivery 
  As the online store
  I want to prevent express delivery for products that weigh more than a maximum weight
  
  Notes:
  * Products are setup in impex as:
      | product  | productWeight(kgs) |
      | W1000001 | 5                  |
      | W1000011 | 4                  |
      | W1000021 | 3                  |
      | W1000031 | 6                  |
      | W1000041 | 2                  |
      | W1000051 | 10                 |
      | W1000061 | 1                  |
      | W1000071 | 11                 |
      | W1000081 | N/A                |
      | W1000091 | 0                  |
   
   * All the Delivery modes are Enabled through configuration

  Background: 
    Given Maximum dead weight restriction for the express delivery is set to 5kg

  Scenario Outline: Available delivery modes on product details page and quick view Page based on weight of the product
    Given a product with '<productCode>'
    When product details are retrieved
    Then delivery modes available for the product are '<availableDeliveryModesForProduct>'

    Examples: 
      | productCode | availableDeliveryModesForProduct                 |
      | W1000001    | express-delivery,home-delivery,click-and-collect |
      | W1000011    | express-delivery,home-delivery,click-and-collect |
      | W1000021    | express-delivery,home-delivery,click-and-collect |
      | W1000031    | home-delivery, click-and-collect                 |
      | W1000041    | express-delivery,home-delivery,click-and-collect |
      | W1000051    | home-delivery, click-and-collect                 |
      | W1000061    | express-delivery,home-delivery,click-and-collect |
      | W1000071    | home-delivery,click-and-collect                  |
      | W1000081    | home-delivery,click-and-collect                  |
      | W1000091    | express-delivery,home-delivery,click-and-collect |

  Scenario Outline: delivery promotion text on product details page and quick view Page based on weight of the product
    Given a product with '<productCode>'
    And delivery promotion ranks order is '<deliveryPromotionRank>'
    And express delivery promo display settings are '<expressDeliveryPromo>'
    And home delivery promo display settings are '<homeDeliveryPromo>'
    And cnc delivery promo display settings are '<CnCPromo>'
    When product details are retrieved
    Then delivery promotion sticker is '<deliveryPromotionSticker>'
    And delivery promotion text is '<deliveryPromotionText>'

    Examples: 
      | productCode | deliveryPromotionRank          | expressDeliveryPromo                 | homeDeliveryPromo                    | CnCPromo                             | deliveryPromotionSticker | deliveryPromotionText  |
      | W1000001    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | express sticker          | express or home or cnc |
      | W1000011    | click-and-collect,home,express | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc or home or express |
      | W1000021    | express,home,click-and-collect | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | NONE                   |
      | W1000031    | express,click-and-collect,home | STICKER PRESENT,TEXT PRESENT         | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | home sticker             | home                   |
      | W1000041    | home,click-and-collect,express | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc                    |
      | W1000051    | click-and-collect,express,home | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | NONE                   |
      | W1000061    | home,express,click-and-collect | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | home or express        |
      | W1000071    | home,express,click-and-collect | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER NOT-PRESENT,TEXT NOT-PRESENT | home sticker             | NONE                   |
      | W1000081    | click-and-collect,express,home | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT     | cnc sticker              | NONE                   |
      | W1000091    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT         | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | express sticker          | express or home        |
