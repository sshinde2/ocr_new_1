@notAutomated
Feature: Display newly arrived products on the product list page
  As a target online customer 
  I want to view products that are newly arrived on the product list page
  So that customer can differentiate new arrivals

  Background: 
    Given list of product categories with day configured
      | category     | newToOnlineConfigDays |
      | All Products | 21                    |
      | Women        | 7                     |
      | Kids         | 14                    |
      | Baby         | 28                    |
      | Men          | Blank                 |
    And list of product with properties from hybris
      | productId | category | subCategory | daysSinceCreated | newArrival |
      | SKU101    | women    | tops        | <7               | yes        |
      | SKU102    | women    | tops        | =7               | yes        |
      | SKU103    | women    | tops        | >7               | no         |
      | SKU104    | women    | dresses     | <7               | yes        |
      | SKU105    | women    | dresses     | =7               | yes        |
      | SKU106    | women    | dresses     | >7               | no         |
      | SKU201    | Kids     | any         | <14              | yes        |
      | SKU202    | Kids     | any         | =14              | yes        |
      | SKU203    | Kids     | any         | >14              | no         |
      | SKU301    | Men      | any         | <21              | yes        |
      | SKU302    | Men      | any         | =21              | yes        |
      | SKU303    | Men      | any         | >21              | no         |
      | SKU401    | Baby     | any         | <21              | yes        |
      | SKU403    | Baby     | any         | <28              | yes        |
      | SKU404    | Baby     | any         | =28              | yes        |
      | SKU405    | Baby     | any         | >28              | no         |

  Scenario Outline: Navigate to Product List page for Category
    Given a user in product list page of <category> category
    And New Arrivals facet encoded value is <encodedValue> in the URL
    When product list page is displayed
    Then <products> products displayed in products list page

    Examples: 
      | category       | encodedValue | products                                       |
      | women          | present      | SKU101, SKU102, SKU104, SKU105                 |
      | women          | not present  | SKU101, SKU102, SKU103, SKU104, SKU105, SKU106 |
      | womens tops    | present      | SKU101, SKU102                                 |
      | womens tops    | not present  | SKU101, SKU102, SKU103                         |
      | womens dresses | present      | SKU104, SKU105                                 |
      | womens dresses | not present  | SKU104, SKU105, SKU106                         |
      | kids           | present      | SKU201, SKU202                                 |
      | kids           | not present  | SKU201, SKU202, SKU203                         |
      | men            | present      | SKU301, SKU302                                 |
      | men            | not present  | SKU301, SKU302, SKU303                         |
      | baby           | present      | SKU401, SKU403, SKU404                         |
      | baby           | not present  | SKU401, SKU403, SKU404, SKU405                 |

  Scenario Outline: Navigate to Product List page for search result
    Given a user search is navigating to the Product List page with search results <searchResults>
    And New Arrivals facet encoded value is <encodedValue> in the URL
    When product list page is displayed
    Then <products> products displayed in products list page

    Examples: 
      | searchResult                                                                                                                   | encodedValue | products                                                                                                                       |
      | SKU101, SKU102, SKU103, SKU104, SKU105, SKU106, SKU201, SKU202, SKU203, SKU301, SKU302, SKU303, SKU401, SKU403, SKU404, SKU405 | present      | SKU101, SKU102, SKU104, SKU105, SKU201, SKU202, SKU301, SKU302, SKU401, SKU403, SKU404                                         |
      | SKU101, SKU102, SKU103, SKU104, SKU105, SKU106, SKU201, SKU202, SKU203, SKU301, SKU302, SKU303, SKU401, SKU403, SKU404, SKU405 | not present  | SKU101, SKU102, SKU103, SKU104, SKU105, SKU106, SKU201, SKU202, SKU203, SKU301, SKU302, SKU303, SKU401, SKU403, SKU404, SKU405 |

  Scenario Outline: Navigate to Product List page by 'New In' link on the Mega Menu
    Given a Target Content Manager has configured a 'New In' link for <category> category
    And New Arrivals facet encoded value is <encodedValue> in the URL
    When user click 'New In' link from <category> category
    Then product list page is displayed
    And <products> products displayed in products list page

    Examples: 
      | category       | encodedValue | products               |
      | womens tops    | present      | SKU101, SKU102         |
      | womens dresses | present      | SKU104, SKU105         |
      | kids           | present      | SKU201, SKU202         |
      | men            | present      | SKU301, SKU302         |
      | baby           | present      | SKU401, SKU403, SKU404 |
