@notAutomated
Feature: IPG - display payment details for succesful orders
  As an online customer
  I want to see a thank you page after I have made my purchase 
  So that I know my payment has gone through successfully and my order is on the way

  Background: 
    Given a successful ipg order is created

  Scenario Outline: Display payment details on Thank you page
    Given payment method captured via iframe is
      | CardType   | CardNumber   | CardExpiry   | Flybuys   |
      | <CardType> | <CardNumber> | <CardExpiry> | <Flybuys> |
    When user lands on the Thank you page
    Then payment details section will be displayed correctly on the Thank You page
    And "Name on card" will be hidden

    Examples: 
      | CardType   | CardNumber       | CardExpiry | Flybuys          |
      | MasterCard | 5587654321010013 | 12/2020    | N/A              |
      | AMEX       | 349876543210010  | 12/2017    | 6008943218616910 |
      | Visa       | 4123456789010335 | 12/2017    | 6008943218616910 |

  Scenario Outline: Display payment details on order detail page
    Given payment method captured via iframe is
      | CardType   | CardNumber   | CardExpiry   | Flybuys   |
      | <CardType> | <CardNumber> | <CardExpiry> | <Flybuys> |
    When user click the order via My Orders
    Then payment details section will be displayed correctly on the My Order Details page
    And "Name on card" will be hidden

    Examples: 
      | CardType   | CardNumber       | CardExpiry | Flybuys          |
      | MasterCard | 5587654321010013 | 12/2020    | N/A              |
      | AMEX       | 349876543210010  | 12/2017    | 6008943218616910 |
      | Visa       | 4123456789010335 | 12/2017    | 6008943218616910 |
