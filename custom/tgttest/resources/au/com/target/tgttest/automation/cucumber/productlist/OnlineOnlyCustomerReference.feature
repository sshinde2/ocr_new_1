@notAutomated
Feature: "Online Only" customer preference set in session
  As a customer
  I want to save my preference when I apply "Available Online" facet
  so I don't need to click it again when I browse the website
  
  Scenario : Save user preference when user apply "Available Online" facet
  Given user is on product listing page
  When user click to apply "Available Online" facet
  Then the preference is saved for current session

  Scenario: Remove your preference when user removes the "Available Online" facet
    Given user is on a listing page with "Available Online" facet applied
    When user click to remove the facet
    Then the "Available Online" preference will be removed from current session

  Scenario Outline: Automatically apply "Available Online" facet
    Given user has previously choose to apply "Available Online" facet to any listing result
    When user browse another <listingPage> within current session
    Then user will see "Available Online" facet is selected by default

    Examples: 
      | listingPage        |
      | category page      |
      | brand page         |
      | search result page |

  Scenario: preference expires when user logs out
    Given user is logged in and has "Available Online" preference saved
    When user logs out
    And browse to any category/brand/search result page
    Then the "Available Online" facet will not applied by default

  Scenario: preference expires when session times out
    Given user has "Available Online" preference saved
    When user idles on the website for more than 30 minutes (current configured session timeout)
    And user browses to any category/brand/search result page
    Then the "Available Online" will not applied by default

  Scenario: Preference will remain when guest user log in
    Given user has "Available Online" preference saved as a guest
    When user log in to the site
    And user browses to another listing page
    Then "Available Online" facet is applied by default for all devices/browsers
