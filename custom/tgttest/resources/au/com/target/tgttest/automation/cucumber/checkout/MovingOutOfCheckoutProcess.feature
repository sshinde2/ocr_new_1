@notAutomated
Feature: Moving out of the checkout process
  As a target online store
  I want to handle the user moving out of checkout scenarios for both in the same session and session timeout
  
  Checkout information - Detail information user input in the checkout process like delivery mode, 
  delivery address, payment address, vouchers/flybuys, TMD card
  Saved checkout information - Delivery mode, delivery address, payment mode, payment details

  Scenario: Soft login user login and checkout then move out of the checkout process
    Given a soft login user
    And the user has the cart with a registered user
    And the user goes into checkout process with password as a login user
    When the user goes out of the checkout process
    Then the user has the cart with a registered cart user
    And the user has all previous items in the cart
    And the user has all checkout information in the cart

  Scenario: Soft login user checkout as guest then move out of the checkout process
    Given a soft login user
    And the user has the cart with a registered user
    And the user goes into checkout process without password as a guest user
    And the user has the cart with a temporary guest user
    When the user goes out of the checkout process
    Then the temporary guest user for checkout is removed
    And the user has the cart with a registered cart user
    And the user has all previous items in the cart
    And the checkout details are cleaned up

  Scenario: Anonymous user login and checkout then move out of the checkout process
    Given an anonymous user
    And the user has the cart with an anonymous user
    And the user goes into checkout process with password as a login user
    When the user goes out of the checkout process
    Then the user has the cart with a registered cart user
    And the user has all previous items in the cart
    And the user has all checkout information in the cart

  Scenario: Anonymous user checkout as guest then move out of the checkout process
    Given an anonymous user
    And the user has the cart with an anonymous user
    And the user goes into checkout process without password as a guest user
    And the user has the cart with a temporary guest user
    When the user goes out of the checkout process
    Then the temporary guest user for checkout is removed
    And the user has the cart with an anonymous cart user
    And the user has all previous items in the cart
    And the checkout details are cleaned up

  Scenario: Soft login user login and checkout then move out of the checkout process after session timeout
    Given a soft login user
    And the user has the cart with a registered user
    And the user goes into checkout process with password as a login user
    When the user goes out of the checkout process after session timeout
    Then the user has the cart with a registered cart user
    And the user has all previous items in the cart
    And the checkout details are cleaned up

  Scenario: Soft login user checkout as guest then move out of the checkout process after session timeout
    Given a soft login user
    And the user has the cart with a registered user
    And the user goes into checkout process without password as a guest user
    And the user has the cart with a temporary guest user
    When the user goes out of the checkout process after session timeout
    Then the user has the cart with a registered user
    And the user has all previous items in the cart
    And the temporary guest user for checkout is removed
    And the checkout details are cleaned up

  Scenario: Anonymous user login and checkout then move out of the checkout process after session timeout
    Given an anonymous user
    And the user has the cart with an anonymous user
    And the user goes into checkout process with password as a login user
    When the user goes out of the checkout process after session timeout
    Then the user has the cart with a registered user
    And the user has all previous items in the cart
    And the checkout details are cleaned up

  Scenario: Anonymous user checkout as guest then move out of the checkout process after session timeout
    Given an anonymous user
    And the user has the cart with an anonymous user
    And the user goes into checkout process without password as a guest user
    And the user has the cart with a temporary guest user
    When the user goes out of the checkout process after session timeout
    Then the user has the cart with an anonymous user
    And the user has all previous items in the cart
    And the checkout details are cleaned up
    And the temporary guest user for checkout is removed
