@initPreOrderProducts @deliveryModeData @cartProductData
Feature: Checks when viewing product details page

   Description: The products used for testing the scenarios has been setup during with hook initPreOrderProducts
   which uses impex preOrder-products.impex

  Scenario Outline: check stock availability on pdp
    When product details are requested for '<productCode>'
    Then stock count for the product is '<stockCount>'

    Examples:
      | productCode     | stockCount | comments                           |
      | P1060           | null       | in stock                           |
      | P1063_noColor   | null       | has size variants and out of stock |
      | P1060_red       | null       | has size variants in stock         |
      | P1061_brown     | 4          | sellable in stock                  |
      | P1060_red_M     | 121        | sellable variant in stock          |
      | P1063_noColor_S | 0          | sellable variant no stock          |

  Scenario: Populate the product display type for new pdp
    When product details are requested for new pdp 'P44444_preOrder_4'
    Then the product details for the variants are :
      | productCode                                     | productDisplayType | sizeVariant | colorVariant | normalSalesDate     |
      | V4444_preOrder_endDatePassedEmbargoDtNotArrived | comingSoon         | false       | true         | 2099-05-01T11:01:01 |
      | V4444_preOrder_noStAndEndDate                   | comingSoon         | false       | true         | 2099-05-01T11:01:01 |
      | V4444_preOrder_stDateNotArrived                 | comingSoon         | false       | true         | 2099-05-01T11:01:01 |

  Scenario: Populate the product display type for new pdp,Prodcut display type preOrderAvailable
    When product details are requested for new pdp 'V1111_preOrder_1'
    Then the product details for the variants are :
      | productCode      | productDisplayType | sizeVariant | colorVariant | normalSalesDate     |
      | V1111_preOrder_1 | preOrderAvailable  | false       | true         | 2099-05-01T11:01:01 |

  Scenario: Populate the product display type for new pdp, Prodcut display type availableForSale
    When product details are requested for new pdp 'W100000'
    Then the product details for the variants are :
      | productCode | productDisplayType | sizeVariant | colorVariant | normalSalesDate |
      | 10000010    | availableForSale   | false       | true         |                 |

  Scenario: Populate the product display type for new pdp for preview product,Prodcut display type comingSoon
    When product details are requested for new pdp 'P66666_preview_6'
    Then the product details for the variants are :
      | productCode     | productDisplayType | sizeVariant | colorVariant | normalSalesDate |
      | V6666_preview_6 | comingSoon         | false       | true         |                 |

  Scenario: Populate the preorder delivery message for new pdp.
    Given a product with 'P55555_preOrder_5'
    And delivery promotion ranks order is 'express'
    And pre order message for 'express-delivery' is 'Testing pre order product express delivery messages'
    When product details are requested for new pdp 'P55555_preOrder_5'
    Then the product details for the variants are :
      | productCode      | productDisplayType | sizeVariant | colorVariant | normalSalesDate     |
      | V5555_preOrder_5 | preOrderAvailable  | false       | true         | 2099-05-01T11:01:01 |
    And the delivery mode values for the product are :
      | name             | shortDescription                                    | code 		     |
      | Express Delivery | Testing pre order product express delivery messages | express-delivery|


  Scenario Outline: view Zip payment information on new PDP
    When product details are requested for new pdp '<product>'
    Then excludeForZipPayment is '<excludeForZipPayment>'
    Examples:
      |product       |excludeForZipPayment |
      |P1002_black   |false                |
      |P1002_black_L |false                |
      |10000910      |true                 |
      |10000911      |true                 |
      |PGC1010       |false                |