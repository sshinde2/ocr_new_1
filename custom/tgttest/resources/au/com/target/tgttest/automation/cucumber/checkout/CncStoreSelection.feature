@cartProductData
Feature: Display the stores for cnc order by geolocation, post code or city.

  Background: 
    Given a customer is located with coordinate data of latitude '-38.1083132' and longitude '144.3357845'
    And the max number of stores shown is 5
    And the Top 10 closest stores to the coordinate of latitude '-38.1083132' and longitude '144.3357845':
      | storeName        | storeNumber | productTypes             | acceptCNC | closed |
      | Geelong          | 7001        | bulky1,bulky2,normal,mhd | true      | false  |
      | Waurn Ponds UAT  | 7103        | bulky1,bulky2,normal,mhd | true      | true   |
      | Ocean Grove      | 8593        | bulky1,bulky2,normal,mhd | false     | false  |
      | Hoppers Crossing | 7064        | bulky1,bulky2,normal,mhd | true      | false  |
      | Point Cook       | 7269        | normal                   | true      | false  |
      | Bacchus Marsh    | 8558        | bulky1,bulky2,normal,mhd | true      | false  |
      | Caroline Springs | 7120        | normal                   | true      | false  |
      | Brimbank Central | 7158        | bulky1,bulky2,normal,mhd | false     | false  |
      | Rosebud          | 7274        | bulky1,bulky2,normal,mhd | true      | false  |
      | Watergardens     | 7160        | bulky1,bulky2,normal,mhd | true      | false  |
    And geolocation setup for certain postcode/suburb are:
      | locationText | latitude    | longitude   |
      | 3220         | -38.1083132 | 144.3357845 |

  Scenario: Customer is presented with maximum 5 CNC stores which are the closest to the location based on the geolocation
    Given the customer has the cart with non bulky product
    When the customer opts to use his/her location
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber |
      | Geelong          | 7001        |
      | Waurn Ponds UAT  | 7103        |
      | Hoppers Crossing | 7064        |
      | Point Cook       | 7269        |
      | Bacchus Marsh    | 8558        |

  Scenario: Customer is presented with maximum 5 CNC stores which are the closest to the location and accept bulky products based on the geolocation
    Given the customer has the cart with bulky product
    When the customer opts to use his/her location
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber |
      | Geelong          | 7001        |
      | Waurn Ponds UAT  | 7103        |
      | Hoppers Crossing | 7064        |
      | Bacchus Marsh    | 8558        |
      | Rosebud          | 7274        |

  Scenario: Customer is presented with maximum 5 CNC stores which are the closest to the location based on the postcode
    Given the customer has the cart with non bulky product
    When the customer submits a postcode 3220
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber |
      | Geelong          | 7001        |
      | Waurn Ponds UAT  | 7103        |
      | Hoppers Crossing | 7064        |
      | Point Cook       | 7269        |
      | Bacchus Marsh    | 8558        |

  Scenario: Customer is presented with maximum 5 CNC stores which are the closest to the location and available for cnc orders based on the postcode
    Given the customer has the cart with bulky product
    When the customer submits a postcode 3220
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber |
      | Geelong          | 7001        |
      | Waurn Ponds UAT  | 7103        |
      | Hoppers Crossing | 7064        |
      | Bacchus Marsh    | 8558        |
      | Rosebud          | 7274        |

  Scenario: Customer cannot see any stores when the named location cannot be geocoded
    Given a customer makes a CNC store selection with the named location cannot be geocoded
    When the customer submits a named location
    Then the customer will not see any stores
