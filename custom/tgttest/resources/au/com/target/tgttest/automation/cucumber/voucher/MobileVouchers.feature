Feature: Vouchers Available for Mobile
  In order to provide customers with better visibility of offers on mobile
   As a Customer
  	I want to retrieve all the vouchers applicable instore and online, to get a consolidated and detailed view of applicable vouchers on mobile.
  
  Description: The vouchers are going to be setup using an impex (/tgttest/automation-impex/voucher/vouchers-mobile-auto-test.impex)
    The impex contains vouchers with id from 951 to 956.
    The voucher having id 955 is disabled for mobile and 956 has a future start date.

  Scenario: Fetching all Vouchers which are available for Mobile
    Given the vouchers are setup in the system running the mentioned impex
    When the vouchers applicable for mobile are being fetched
    Then the retrieved data will contain the following vouchers:
      | voucherId | pageTitle          | longTitle                  | voucherCode | enabledOnline | enabledInStore | barcode       | availableForMobile | offerHeading   |
      | 951       | 951_name_PageTitle | 951_Description_LongTitle  | VOUCH951    | true          | false          |               | true               | kids           |
      | 952       | 952_name_PageTitle | 952_Description_LongTitle  |             | false         | true           | 2721000951908 | true               | womens         |
      | 953       | 953_name_PageTitle | 953_Description_LongTitle  | VOUCH953    | true          | true           | 2721000909909 | true               | kids,lifestyle |
      | 954       | 954_name_PageTitle | 954_Description_LongTitle  | VOUCH954    | true          | true           | 2721000559906 | true               | mens           |
    But will not contain the following vouchers with id:
      | 955 |
      | 956 |
