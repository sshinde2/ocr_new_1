@cartProductData
Feature: Sync the favourites from client to server
  Users add favourites to their local storage and a sync is called at regular intervals to save to server
  As Target Online
  I want to sync all favourites from my local storage to server.

  Scenario: As a new user, my favourites in my local storage are saved to server on login.
    Given the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
    And customer 'test.user1@target.com.au' having no favourites
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |

  Scenario: As a user who already has saved favourites, and add more before I sign in.
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000211            | W100002         | 06-11-2015 18:20:35 |
      | P4000_blue          | P4000           | 14-03-2015 11:15:00 |
    And the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
      | 10000211            | W100002         | 06-11-2015 18:20:35 |
      | P4000_blue          | P4000           | 14-03-2015 11:15:00 |

  Scenario: As a user who already has saved favourites, and re-add the same ones.
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 08-10-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 09:30:00 |
    And the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |

  Scenario: As a user who already has saved favourites, and add a different variant.
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 10000112            | W100001         | 28-11-2015 09:30:00 |
    And the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000010            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000010            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |

  Scenario: As a user who already has saved favourites, and local storage is empty.
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 10000112            | W100001         | 28-11-2015 09:30:00 |
    And the local storage is empty
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 10000112            | W100001         | 28-11-2015 09:30:00 |

  Scenario: As a user who already has saved max favourites, and user adds another one.
    Given the max a user can add to his favourites is '4'
    And the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
      | 10000211            | W100002         | 06-11-2015 18:20:35 |
      | P4000_blue          | P4000           | 14-03-2015 11:15:00 |
    And the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000322            | W100003         | 28-11-2015 08:05:00 |
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
      | 10000211            | W100002         | 06-11-2015 18:20:35 |
      | 10000322            | W100003         | 28-11-2015 08:05:00 |

  Scenario: A user who has an invalid favourite entry should still be able to see the remaining favourites.
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 10000112            | W100001         | 28-11-2015 09:30:00 |
      | 100011121           | W1000101        | 06-11-2015 18:20:35 |
    And the local storage is empty
    And the product association for 'W1000101' is removed
    When customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 10000112            | W100001         | 28-11-2015 09:30:00 |

@notAutomated
  Scenario: As an anonymous user who has favourited few variants and one of the variants are moved to a different style group.
    Given the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
    When the following variants are moved to a different style group:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000111            | W100002         | 15-05-2017 16:12:00 |
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |

@notAutomated
  Scenario: An anonymous user who has favourited few variants and one of the variants are moved to a different style group before user signs in.
    Given the local storage has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 09:30:00 |  
    And the following variants are moved to a different style group:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000111            | W100002         | 15-05-2017 16:12:00 |  
    When the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 100011121           | W1000101        | 06-11-2015 18:20:35 |
    And customer 'test.user1@target.com.au' logs in
    Then the favourites for the customer has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000012            | W100000         | 08-10-2015 08:05:00 |
      | 100011121           | W1000101        | 06-11-2015 18:20:35 |

