/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgttest.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Initialization;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.util.JspContext;
import de.hybris.platform.util.Utilities;
import de.hybris.platform.util.localization.TypeLocalization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import au.com.target.tgttest.constants.TgttestConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgttestConstants.EXTENSIONNAME)
public class TestDataSystemSetup extends AbstractSystemSetup
{
    private static final Logger LOG = Logger.getLogger(TestDataSystemSetup.class);

    @SuppressWarnings({ "deprecation" })
    public static void yunitEssentialData() {
        try {
            Utilities.setJUnitTenant();
            Initialization.createEssentialData(createDummyYunitContext());
            TypeLocalization.getInstance().localizeTypes();
        }
        catch (final Exception e) {
            LOG.error("Could not create essential data", e);
        }
    }

    private static JspContext createDummyYunitContext() {
        // create mock http request that will simulate an update selecting only to create essential data
        final java.util.Map requestParams = new java.util.HashMap();
        requestParams.put("init", "Go");
        requestParams.put("initmethod", "update");
        requestParams.put("essential", "true");
        requestParams.put("localizetypes", "true");
        requestParams.put("droptables", "false");
        requestParams.put("clearhmc", "false");

        final List<Extension> extensionList = de.hybris.platform.core.Initialization.getCreators();
        for (final Extension ext : extensionList) {
            final String creatorName = String.valueOf(ext.getCreatorName());
            requestParams.put((new StringBuilder(creatorName)).append("_sample").toString(), "true");
            final Collection<String> creatorparams = ext.getCreatorParameterNames();
            if (CollectionUtils.isNotEmpty(creatorparams)) {
                for (final String param : creatorparams) {
                    final String defaultValue = ext.getCreatorParameterDefault(param);
                    if (StringUtils.isNotBlank(defaultValue)) {
                        requestParams.put((new StringBuilder(creatorName)).append("_")
                                .append(param).toString(), defaultValue);
                    }
                }

            }
        }

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameters(requestParams);
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final JspContext jspcontext = new JspContext(null, request, response);
        return jspcontext;
    }

    /**
     * Generates the Dropdown and Multi-select boxes for the projectdata import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions()
    {
        final List<SystemSetupParameter> params = new ArrayList<>();

        return params;
    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system
     * initialization.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        // Initial test data
        importImpexFile(context, "/tgttest/automation-impex/user/initial-test-users.impex");
    }

}
