/**
 * 
 */
package au.com.target.tgttest;

import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;


public class DummyRetryAction extends RetryActionWithSpringConfig<BusinessProcessModel> {

    private static final Logger LOG = Logger.getLogger(DummyRetryAction.class);

    public enum Transition
    {
        OK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    public String executeInternal(final BusinessProcessModel process) throws RetryLaterException, Exception {
        LOG.info("testing is working");
        final RetryLaterException rle = new RetryLaterException();
        rle.setDelay(3000);
        throw rle;
    }
}
