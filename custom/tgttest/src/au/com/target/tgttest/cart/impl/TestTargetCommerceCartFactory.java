/**
 * 
 */
package au.com.target.tgttest.cart.impl;

import de.hybris.platform.core.model.order.CartModel;

import au.com.target.tgtlayby.cart.impl.TargetCommerceCartFactory;
import au.com.target.tgttest.constants.TgttestConstants;


/**
 * Extends TargetCommerceCartFactory to customize the default cart code to differentiate cart and order created by
 * automation tests.
 * 
 * @author jjayawa1
 *
 */
public class TestTargetCommerceCartFactory extends TargetCommerceCartFactory {

    private boolean active;

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.cart.impl.TargetCommerceCartFactory#createCartInternal()
     */
    @Override
    protected CartModel createCartInternal() {
        final CartModel cart = super.createCartInternal();
        if (active) {
            cart.setCode(TgttestConstants.ORDER_PREFIX + cart.getCode());
        }
        return cart;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }
}
