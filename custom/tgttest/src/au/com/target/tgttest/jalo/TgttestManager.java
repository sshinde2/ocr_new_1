/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgttest.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.JspContext;

import java.util.Map;

import org.apache.log4j.Logger;

import au.com.target.tgttest.constants.TgttestConstants;



/**
 * This is the extension manager of the Tgttest extension.
 */
public class TgttestManager extends GeneratedTgttestManager
{
    /** Edit the local|project.properties to change logging behavior (properties 'log4j.*'). */
    private static final Logger LOG = Logger.getLogger(TgttestManager.class.getName());

    /*
     * Some important tips for development:
     * 
     * Do NEVER use the default constructor of manager's or items. => If you want to do something whenever the manger is
     * created use the init() or destroy() methods described below
     * 
     * Do NEVER use STATIC fields in your manager or items! => If you want to cache anything in a "static" way, use an
     * instance variable in your manager, the manager is created only once in the lifetime of a "deployment" or tenant.
     */


    /**
     * Never call the constructor of any manager directly, call getInstance() You can place your business logic here -
     * like registering a jalo session listener. Each manager is created once for each tenant.
     */
    public TgttestManager()
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("constructor of TgttestManager called.");
        }
    }


    /**
     * Get the valid instance of this manager.
     * 
     * @return the current instance of this manager
     */
    public static TgttestManager getInstance()
    {
        return (TgttestManager)Registry.getCurrentTenant().getJaloConnection().getExtensionManager().getExtension(
                TgttestConstants.EXTENSIONNAME);
    }

    /**
     * Implement this method to create initial objects. This method will be called by system creator during
     * initialization and system update. Be sure that this method can be called repeatedly.
     * 
     * An example usage of this method is to create required cronjobs or modifying the type system (setting e.g some
     * default values)
     * 
     * @param params
     *            the parameters provided by user for creation of objects for the extension
     * @param jspc
     *            the jsp context; you can use it to write progress information to the jsp page during creation
     */
    @Override
    public void createEssentialData(final Map<String, String> params, final JspContext jspc)
    {
        // implement here code creating essential data
    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system
     * initialization.
     * 
     * An example use is to import initial data like currencies or languages for your project from an csv file.
     * 
     * @param params
     *            the parameters provided by user for creation of objects for the extension
     * @param jspc
     *            the jsp context; you can use it to write progress information to the jsp page during creation
     */
    @Override
    public void createProjectData(final Map<String, String> params, final JspContext jspc)
    {
        // implement here code creating project data
    }
}
