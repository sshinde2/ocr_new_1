/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rmcalave
 *
 */
public class StoreHours {
    private int storeNumber;
    private String storeName;
    private String openingDay;
    private String openingTime;
    private String closingTime;

    /**
     * @return the storeNumber
     */
    public int getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final int storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the openingDay
     */
    public String getOpeningDay() {
        return openingDay;
    }

    /**
     * @param openingDay
     *            the openingDay to set
     */
    public void setOpeningDay(final String openingDay) {
        this.openingDay = openingDay;
    }

    /**
     * @return the openingTime
     */
    public String getOpeningTime() {
        return openingTime;
    }

    /**
     * @param openingTime
     *            the openingTime to set
     */
    public void setOpeningTime(final String openingTime) {
        this.openingTime = openingTime;
    }

    /**
     * @return the closingTime
     */
    public String getClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime
     *            the closingTime to set
     */
    public void setClosingTime(final String closingTime) {
        this.closingTime = closingTime;
    }
}
