/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import java.util.List;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttinker.mock.instore.MockTargetStockUpdateClient;
import au.com.target.tgttinker.mock.stockvisibility.StockVisibilityClientMock;


/**
 * @author rmcalave
 *
 */
public final class InStoreStockUtil {

    private InStoreStockUtil() {
    }

    public static void enableMocks(final List<StockVisibilityItemResponseDto> itemResponses) {
        final StockVisibilityClientMock stockVisibilityClientMock = ServiceLookup.getStockVisibilityClientMock();

        stockVisibilityClientMock.setActive(true);
        stockVisibilityClientMock.setStockVisibilityItemResponseDtoList(itemResponses);

        final MockTargetStockUpdateClient mockTargetStockUpdateClient = ServiceLookup.getTargetStockUpdateClient();
        mockTargetStockUpdateClient.setActive(true);
        for (final StockVisibilityItemResponseDto stockVisibilityItemResponseDto : itemResponses) {
            mockTargetStockUpdateClient.addStoreStock(stockVisibilityItemResponseDto.getStoreNumber(),
                    stockVisibilityItemResponseDto.getCode(), stockVisibilityItemResponseDto.getSoh());
        }
    }

}
