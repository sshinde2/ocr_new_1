/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import au.com.target.tgtsale.product.client.TargetPOSProductClient;
import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgtsale.product.impl.TargetPOSProductServiceImpl;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Mock for TargetPOSProductClient captures request and delivers configured response
 * 
 */
public final class MockTargetPOSProductClient {

    private static final Logger LOG = Logger.getLogger(MockTargetPOSProductClient.class);

    private static ProductRequestDto request;
    private static Map<String, ProductResponseDto> responses = new HashMap<String, ProductResponseDto>();

    private MockTargetPOSProductClient() {
        // util
    }

    public static void setMock() {

        final TargetPOSProductClient mockClient = new TargetPOSProductClient() {

            @Override
            public ProductResponseDto getPOSProductDetails(final ProductRequestDto requestDto) {

                LOG.info("Mock TargetPOSProductClient ");
                MockTargetPOSProductClient.request = requestDto;

                // Ean exists in the store
                if (responses.containsKey(requestDto.getEan())) {
                    return responses.get(requestDto.getEan());
                }

                final ProductResponseDto dto = new ProductResponseDto();
                dto.setErrorMessage("Itemcode not found");
                dto.setSuccess(false);
                return dto;
            }
        };

        final TargetPOSProductServiceImpl service = (TargetPOSProductServiceImpl)ServiceLookup
                .getTargetPOSProductService();
        service.setTargetPOSProductClient(mockClient);

        // Clear the configured captured request and responses
        request = null;
        responses.clear();

    }



    public static ProductRequestDto getRequest() {
        return request;
    }

    public static void addResponse(final String ean, final ProductResponseDto response) {
        responses.put(ean, response);
    }


}
