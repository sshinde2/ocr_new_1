/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represents pinpad payment info in tlog
 * 
 */
public class TlogPinPadPaymentInfo {

    private final String cardNumber;
    private final String rrn;
    private final boolean approved;

    /**
     * @param cardNumber
     * @param rrn
     * @param approved
     */
    public TlogPinPadPaymentInfo(final String cardNumber, final String rrn, final boolean approved) {
        super();
        this.cardNumber = cardNumber;
        this.rrn = rrn;
        this.approved = approved;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getRrn() {
        return rrn;
    }

    public boolean isApproved() {
        return approved;
    }


}
