/**
 *
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.PickEntry;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Order;


/**
 * @author knemalik
 * 
 */
public final class CncOrderUtil {

    private CncOrderUtil() {
        // utility
    }

    /**
     * creates click and collect order
     * 
     * @throws Exception
     */
    public static void createCncOrder() throws Exception {
        CheckoutUtil.createAnyCart();
        Checkout.getInstance().setUserCheckoutMode(CheckoutUtil.GUEST_CHECKOUT);
        Checkout.getInstance().setDeliveryMode("click-and-collect");
        final OrderModel order = CheckoutUtil.placeOrder();
        Order.setOrder(order);

    }

    public static void fulfillOrder() throws Exception {
        final PickEntry pickEntry = new PickEntry(CheckoutUtil.ANY_PRD_CODE, CheckoutUtil.ANY_PRD_QTY);
        final List<PickEntry> pickEntries = new ArrayList<>();
        pickEntries.add(pickEntry);
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        final TargetConsignmentModel consignment = (TargetConsignmentModel)consignments.iterator().next();

        FulfilmentFacade.processPickFile(orderModel.getCode(), consignment.getCode(), consignment.getTargetCarrier()
                .getWarehouseCode(), "123456", Integer.valueOf(1), pickEntries);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("consignmentPickedProcess");

        FulfilmentFacade.processShipConfirm(orderModel.getCode(), new Date());
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCompletionProcess");
    }

    public static void setMockResponseStatus(final String smsResponse) {
        ServiceLookup.getTargetSendSmsClient().setMockResponse(smsResponse);
    }
}
