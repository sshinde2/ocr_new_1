/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rsamuel3
 * 
 */
public class PickEntry {
    private final String product;
    private final int qty;

    public PickEntry(final String product, final int quantity) {
        this.product = product;
        this.qty = quantity;
    }

    /**
     * @return the productCode
     */
    public String getProduct() {
        return product;
    }


    /**
     * @return the quantity
     */
    public int getQty() {
        return qty;
    }

}
