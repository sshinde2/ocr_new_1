/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.domain.Order;


/**
 * Utility methods for Orders.
 * 
 * @author jjayawa1
 *
 */
public final class OrderUtil {

    private OrderUtil() {

    }

    /**
     * Remove all orders.
     */
    public static void removeAllOrders() {
        ServiceLookup.getOrderCreationHelper().removeAllOrders();
    }

    /**
     * Set the order for the consignment in the domain model
     * 
     * @param consignmentCode
     */
    public static void setOrderDomainModel(final String consignmentCode) {

        final TargetConsignmentModel consignment = ConsignmentUtil.getConsignmentForCode(consignmentCode);
        assertThat(consignment).isNotNull();
        assertThat(consignment.getOrder()).isNotNull();
        Order.setOrder((OrderModel)consignment.getOrder());
    }

    /**
     * verify the order refund amount in order entry
     * 
     * @param order
     * @param amount
     * @return boolean
     */
    public static boolean isRefundValueMatch(final Order order, final Double amount) {
        final ModelService modelService = ServiceLookup.getModelService();
        for (final PaymentTransactionModel paymentTrans : order.getOrderModel().getPaymentTransactions()) {
            modelService.refresh(paymentTrans);
            final List<PaymentTransactionEntryModel> entries = paymentTrans.getEntries();
            for (final PaymentTransactionEntryModel entry : entries) {
                if (PaymentTransactionType.REFUND_FOLLOW_ON.equals(entry.getType())
                        && entry.getAmount().compareTo(BigDecimal.valueOf(amount.doubleValue())) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkRefundSuccss(final Order order) {
        final ModelService modelService = ServiceLookup.getModelService();
        for (final PaymentTransactionModel paymentTrans : order.getOrderModel().getPaymentTransactions()) {
            modelService.refresh(paymentTrans);
            final List<PaymentTransactionEntryModel> entries = paymentTrans.getEntries();
            for (final PaymentTransactionEntryModel entry : entries) {
                if (PaymentTransactionType.REFUND_FOLLOW_ON.equals(entry.getType())
                        && "ACCEPTED".equals(entry.getTransactionStatus())) {
                    return true;
                }
            }
        }
        return false;
    }

}
