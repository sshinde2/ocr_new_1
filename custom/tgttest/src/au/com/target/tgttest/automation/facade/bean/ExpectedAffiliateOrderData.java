/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;



/**
 * Expected affiliate order data.
 *
 */
public class ExpectedAffiliateOrderData {

    private String codes;
    private String quantities;
    private String prices;
    private String comment;

    /**
     * @return the codes
     */
    public String getCodes() {
        return codes;
    }

    /**
     * @param codes
     *            the codes to set
     */
    public void setCodes(final String codes) {
        this.codes = codes;
    }

    /**
     * @return the quantities
     */
    public String getQuantities() {
        return quantities;
    }

    /**
     * @param quantities
     *            the quantities to set
     */
    public void setQuantities(final String quantities) {
        this.quantities = quantities;
    }

    /**
     * @return the prices
     */
    public String getPrices() {
        return prices;
    }

    /**
     * @param prices
     *            the prices to set
     */
    public void setPrices(final String prices) {
        this.prices = prices;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }


}
