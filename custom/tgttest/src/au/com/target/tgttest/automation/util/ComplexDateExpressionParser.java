/**
 * 
 */
package au.com.target.tgttest.automation.util;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalTime;


/**
 * @author rmcalave
 *
 */
public class ComplexDateExpressionParser {

    private static final Logger LOG = Logger.getLogger(ComplexDateExpressionParser.class);

    private static final String NOW = "now";
    private static final String TODAY = "today";
    private static final String YESTERDAY = "yesterday";
    private static final String OLDER = "older";
    private static final String TOMORROW = "tomorrow";
    private static final String DAY_AFTER_TOMORROW = "day after tomorrow";

    private static final String HOURS_AGO = "(\\d+) hour(s)? ago";

    private static final String DAYS_AGO = "(\\d+) day(s)? ago";
    private static final String DAYS_AGO_AT_TIME = "(\\d+) day(s)? ago at (\\d\\d):(\\d\\d)";
    private static final String YESTERDAY_AT_TIME = "yesterday at (\\d\\d):(\\d\\d)";

    private static final String TODAY_AT_TIME = "today at (\\d\\d):(\\d\\d)";

    private static final String PLUS_MINUS_SECS = "(plus|minus) (\\d+) second(s)?";

    private static final String PLUS_MINUS_MINS = "(plus|minus) (\\d+) minute(s)?";

    private static final String TIME_WITH_OFFSET = "(\\d\\d+) plus (\\d+) hours ago";

    private static final Pattern DAY_TIME_PATTERN = Pattern.compile("(\\d{1,2})[.:](\\d{2,2})(AM|PM)");

    /**
     * This will take string as input and convert it into calendar
     * 
     * @param dateAsString
     * @return Calendar
     */
    public Calendar interpretStringAsCalendar(final String dateAsString) {

        final Date date = interpretStringAsDate(dateAsString);
        if (date != null) {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal;
        }

        return null;
    }

    /**
     * Take the given date/time phrase and convert to a Date
     * 
     * @param date
     * @return Date
     */
    public Date interpretStringAsDate(final String date) {

        LOG.info("Parsing date: " + date);

        if (StringUtils.isBlank(date) || compareRelaxed(date, "null") || compareRelaxed(date, "n/a")) {
            return null;
        }
        if (compareRelaxed(date, TODAY) || StringUtils.equalsIgnoreCase(date, NOW)) {
            return new Date();
        }
        if (Pattern.compile(TIME_WITH_OFFSET).matcher(date).matches()) {
            return new ComplexDateExpressionParser().dateWithOffset(date);
        }
        if (Pattern.compile(HOURS_AGO, Pattern.CASE_INSENSITIVE).matcher(date).matches()) {
            return new ComplexDateExpressionParser().hoursAgo(date);
        }
        if (compareRelaxed(date, YESTERDAY)) {
            return nowMinusDays(1);
        }
        if (compareRelaxed(date, OLDER)) {
            return nowMinusDays(2);
        }
        if (compareRelaxed(date, TOMORROW)) {
            return nowPlusDays(1);
        }
        if (compareRelaxed(date, DAY_AFTER_TOMORROW)) {
            return nowPlusDays(2);
        }

        final DateInterpreterResult parseResult = parseHistoricalDateString(date);
        if (parseResult != null) {

            if (parseResult.hasTime) {
                return daysAgoAtTime(parseResult.getDaysAgo(), parseResult.getHour(), parseResult.getMin());
            }

            return addSeconds(nowMinusDays(parseResult.getDaysAgo()), parseResult.getSecsDifference());
        }

        throw new IllegalArgumentException("Could not interpret as date: " + date);
    }



    /**
     * Method to parse the day string and time string to date.
     *
     * @param dateString
     *            the date string
     * @param timeString
     *            the time string
     * @return the date
     */
    public Date parseDayAndTimeToDate(final String dateString, final String timeString) {
        final Matcher matcher = DAY_TIME_PATTERN.matcher(timeString);

        if (!matcher.matches()) {
            throw new IllegalArgumentException(timeString + " is not a valid time value");
        }

        final String[] groups = new String[matcher.groupCount()];
        for (int i = 0; i < matcher.groupCount(); i++) {
            groups[i] = matcher.group(i + 1);
        }
        int hour = Integer.valueOf(groups[0]).intValue();
        if ("PM".equals(groups[2])) {
            hour += 12;
        }

        final int minutes = Integer.valueOf(groups[1]).intValue();
        final LocalTime time = new LocalTime(hour, minutes);

        Date date = time.toDateTimeToday().toDate();

        if (StringUtils.equalsIgnoreCase(dateString, "Yesterday")) {
            date = DateUtils.addDays(date, -1);
        }
        else if (StringUtils.equalsIgnoreCase(dateString, "Tomorrow")) {
            date = DateUtils.addDays(date, 1);
        }
        else if (StringUtils.equalsIgnoreCase(dateString, "Day after Tomorrow")) {
            date = DateUtils.addDays(date, 2);
        }

        return date;
    }

    private Date dateWithOffset(final String date) {
        final Pattern offSetTimePattern = Pattern.compile(TIME_WITH_OFFSET, Pattern.CASE_INSENSITIVE);
        final Matcher offSetTimeMatcher = offSetTimePattern.matcher(date);
        if (offSetTimeMatcher.find()) {
            final int offset = Integer.parseInt(offSetTimeMatcher.group(1));
            final int hoursAgo = Integer.parseInt(offSetTimeMatcher.group(2));
            return DateUtils.addHours(new Date(), -(offset + hoursAgo));
        }
        return null;
    }

    private Date hoursAgo(final String date) {
        final Pattern hoursAgoPattern = Pattern.compile(HOURS_AGO, Pattern.CASE_INSENSITIVE);
        final Matcher hoursAgoMatcher = hoursAgoPattern.matcher(date);
        if (hoursAgoMatcher.find()) {
            final int hoursAgo = Integer.parseInt(hoursAgoMatcher.group(1));
            return DateUtils.addHours(new Date(), -hoursAgo);
        }
        return null;
    }

    /**
     * Parse a complex historical date expression
     * 
     * @param date
     * @return DateInterpreterResult
     */
    protected DateInterpreterResult parseHistoricalDateString(final String date) {

        final Pattern yesterdayAtTimePattern = Pattern.compile(YESTERDAY_AT_TIME, Pattern.CASE_INSENSITIVE);
        final Matcher yesterdayAtTimeMatcher = yesterdayAtTimePattern.matcher(date);

        final Pattern daysAgoAtTimePattern = Pattern.compile(DAYS_AGO_AT_TIME, Pattern.CASE_INSENSITIVE);
        final Matcher daysAgoAtTimeMatcher = daysAgoAtTimePattern.matcher(date);

        final Pattern daysAgoPattern = Pattern.compile(DAYS_AGO, Pattern.CASE_INSENSITIVE);
        final Matcher daysAgoMatcher = daysAgoPattern.matcher(date);

        final Pattern todayAtTimePattern = Pattern.compile(TODAY_AT_TIME, Pattern.CASE_INSENSITIVE);
        final Matcher todayAtTimeMatcher = todayAtTimePattern.matcher(date);

        if (yesterdayAtTimeMatcher.find()) {
            final int hour = Integer.parseInt(yesterdayAtTimeMatcher.group(1));
            final int min = Integer.parseInt(yesterdayAtTimeMatcher.group(2));
            return new DateInterpreterResult(true, 1, hour, min);
        }

        if (daysAgoAtTimeMatcher.find()) {
            final int daysAgo = Integer.parseInt(daysAgoAtTimeMatcher.group(1));
            final int hour = Integer.parseInt(daysAgoAtTimeMatcher.group(3));
            final int min = Integer.parseInt(daysAgoAtTimeMatcher.group(4));
            return new DateInterpreterResult(true, daysAgo, hour, min);
        }

        if (daysAgoMatcher.find()) {
            final int daysAgo = Integer.parseInt(daysAgoMatcher.group(1));
            final DateInterpreterResult result = new DateInterpreterResult(false, daysAgo, 0, 0);

            final Integer secs = parseDateAsSecsDifference(date);
            if (secs != null) {
                result.setSecsDifference(secs.intValue());
            }
            final Integer minutes = parseDateAsMinsDifference(date);
            if (minutes != null) {
                result.setSecsDifference(minutes.intValue() * 60);
            }
            return result;
        }

        if (todayAtTimeMatcher.find()) {
            final int hour = Integer.parseInt(todayAtTimeMatcher.group(1));
            final int min = Integer.parseInt(todayAtTimeMatcher.group(2));
            return new DateInterpreterResult(true, 1, hour, min);
        }

        return null;
    }

    public int getRelativeHoursFromNowHoursDatePattern(String relativeTime) {
        relativeTime = relativeTime.replaceAll("now", "");
        relativeTime = relativeTime.replaceAll("hours", "");
        relativeTime = relativeTime.trim();
        final String[] parts = relativeTime.split(" ");
        final String sign = parts[0];
        final String hours = parts[1];
        int hoursValue = Integer.valueOf(hours).intValue();
        if (StringUtils.equals("-", sign)) {
            hoursValue = -hoursValue;
        }
        return hoursValue;
    }

    private Integer parseDateAsSecsDifference(final String date) {

        final Pattern plusMinusSecondsPattern = Pattern.compile(PLUS_MINUS_SECS, Pattern.CASE_INSENSITIVE);
        final Matcher plusMinusSecondsMatcher = plusMinusSecondsPattern.matcher(date);

        if (plusMinusSecondsMatcher.find()) {
            final String plusMinus = plusMinusSecondsMatcher.group(1);
            final int seconds = Integer.parseInt(plusMinusSecondsMatcher.group(2));

            if ("minus".equalsIgnoreCase(plusMinus)) {
                return Integer.valueOf(-seconds);
            }

            return Integer.valueOf(seconds);
        }

        return null;
    }

    private Integer parseDateAsMinsDifference(final String date) {

        final Pattern plusMinusSecondsPattern = Pattern.compile(PLUS_MINUS_MINS, Pattern.CASE_INSENSITIVE);
        final Matcher plusMinusSecondsMatcher = plusMinusSecondsPattern.matcher(date);

        if (plusMinusSecondsMatcher.find()) {
            final String plusMinus = plusMinusSecondsMatcher.group(1);
            final int minutes = Integer.parseInt(plusMinusSecondsMatcher.group(2));

            if ("minus".equalsIgnoreCase(plusMinus)) {
                return Integer.valueOf(-minutes);
            }

            return Integer.valueOf(minutes);
        }

        return null;
    }

    private boolean compareRelaxed(final String from, final String to) {
        if (from == null && to == null) {
            return true;
        }

        if (from == null || to == null) {
            return false;
        }
        return StringUtils.equalsIgnoreCase(from.trim(), to);
    }

    private Date nowMinusDays(final int daysAgo) {
        return DateUtils.addDays(new Date(), -daysAgo);
    }

    private Date nowPlusDays(final int daysAgo) {
        return DateUtils.addDays(new Date(), daysAgo);
    }

    private Date addSeconds(final Date date, final int secs) {
        return DateUtils.addSeconds(date, secs);
    }

    private Date daysAgoAtTime(final int daysAgo, final int hour, final int min) {

        final Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.HOUR, hour);
        cal.set(Calendar.MINUTE, min);

        cal.add(Calendar.DAY_OF_MONTH, -daysAgo);

        return cal.getTime();
    }


    public class DateInterpreterResult {

        private final boolean hasTime;
        private final int hour;
        private final int min;
        private final int daysAgo;
        private int secsDifference;

        /**
         * @param hasTime
         * @param hour
         * @param min
         * @param daysAgo
         */
        public DateInterpreterResult(final boolean hasTime, final int daysAgo, final int hour, final int min) {
            super();
            this.hasTime = hasTime;
            this.hour = hour;
            this.min = min;
            this.daysAgo = daysAgo;
        }

        /**
         * @return the hasTime
         */
        public boolean isHasTime() {
            return hasTime;
        }

        /**
         * @return the hour
         */
        public int getHour() {
            return hour;
        }

        /**
         * @return the min
         */
        public int getMin() {
            return min;
        }

        /**
         * @return the daysAgo
         */
        public int getDaysAgo() {
            return daysAgo;
        }

        /**
         * @return the secsDifference
         */
        public int getSecsDifference() {
            return secsDifference;
        }

        /**
         * @param secsDifference
         *            the secsDifference to set
         */
        public void setSecsDifference(final int secsDifference) {
            this.secsDifference = secsDifference;
        }


    }



}
