/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author ajit
 *
 */
public class SavedUserPreferences {

    private final String creditCard;
    private final String deliveryMode;
    private final String cncDetails;
    private final String defaultDeliveryAddress;

    /**
     * @param creditCard
     * @param deliveryMode
     * @param cncDetails
     */
    public SavedUserPreferences(final String creditCard, final String deliveryMode, final String cncDetails,
            final String defaultDeliveryAddress) {
        super();
        this.creditCard = creditCard;
        this.deliveryMode = deliveryMode;
        this.cncDetails = cncDetails;
        this.defaultDeliveryAddress = defaultDeliveryAddress;
    }

    /**
     * @return the creditCard
     */
    public String getCreditCard() {
        return creditCard;
    }

    /**
     * @return the deliveryMode
     */
    public String getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * @return the cncDetails
     */
    public String getCncDetails() {
        return cncDetails;
    }

    /**
     * @return the defaultDeliveryAddress
     */
    public String getDefaultDeliveryAddress() {
        return defaultDeliveryAddress;
    }

}
