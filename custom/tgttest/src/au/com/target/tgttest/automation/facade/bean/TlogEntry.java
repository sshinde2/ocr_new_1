/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent an expected TlogEntry
 * 
 */
public class TlogEntry {

    private final String code;
    private final int qty;
    private final int price;
    private final int filePrice;
    private final String itemDiscountType;
    private final Integer itemDiscountPct;
    private final Integer itemDiscountAmount;
    private final String itemDealType;
    private final String itemDealId;
    private final Integer itemDealInstance;
    private final Integer itemDealMarkdown;
    private final String itemPromotionalDiscountType;
    private final Integer itemDiscountPromoAmount;
    private String reason;



    /**
     * @param code
     * @param qty
     * @param price
     * @param filePrice
     * @param itemDiscountType
     * @param itemDiscountPct
     * @param itemDiscountAmount
     * @param itemDealType
     * @param itemDealId
     * @param itemDealInstance
     * @param itemDealMarkdown
     * @param itemDiscountPromoAmount
     * @param itemPromotionalDiscountType
     * @param reason
     */
    public TlogEntry(final String code, final int qty, final int price, final int filePrice,
            final String itemDiscountType, final Integer itemDiscountPct,
            final Integer itemDiscountAmount, final String itemDealType, final String itemDealId,
            final Integer itemDealInstance, final Integer itemDealMarkdown, final Integer itemDiscountPromoAmount,
            final String itemPromotionalDiscountType, final String reason) {
        super();
        this.code = code;
        this.qty = qty;
        this.price = price;
        this.filePrice = filePrice;
        this.itemDiscountType = itemDiscountType;
        this.itemDiscountPct = itemDiscountPct;
        this.itemDiscountAmount = itemDiscountAmount;
        this.itemDealType = itemDealType;
        this.itemDealId = itemDealId;
        this.itemDealInstance = itemDealInstance;
        this.itemDealMarkdown = itemDealMarkdown;
        this.itemPromotionalDiscountType = itemPromotionalDiscountType;
        this.itemDiscountPromoAmount = itemDiscountPromoAmount;
        this.reason = reason;
    }

    /**
     * @return the itemPromotionalDiscountType
     */
    public String getItemPromotionalDiscountType() {
        return itemPromotionalDiscountType;
    }

    /**
     * @return the itemDealPromoAmount
     */
    public Integer getItemDiscountPromoAmount() {
        return itemDiscountPromoAmount;
    }

    public int getFilePrice() {
        return filePrice;
    }

    public String getCode() {
        return code;
    }

    public int getQty() {
        return qty;
    }

    public int getPrice() {
        return price;
    }

    public String getItemDiscountType() {
        return itemDiscountType;
    }

    public Integer getItemDiscountPct() {
        return itemDiscountPct;
    }

    public Integer getItemDiscountAmount() {
        return itemDiscountAmount;
    }

    public String getItemDealType() {
        return itemDealType;
    }

    public String getItemDealId() {
        return itemDealId;
    }

    public Integer getItemDealInstance() {
        return itemDealInstance;
    }

    public Integer getItemDealMarkdown() {
        return itemDealMarkdown;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(final String reason) {
        this.reason = reason;
    }

}
