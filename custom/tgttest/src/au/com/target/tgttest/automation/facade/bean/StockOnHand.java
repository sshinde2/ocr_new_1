/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Nandini
 *
 */
public class StockOnHand {
    private String productCode;
    private String ean;
    private String warehouse;
    private String store;
    private String totalQuantityAvailable;
    private String adjustmentQuantity;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse
     *            the warehouse to set
     */
    public void setWarehouse(final String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the totalQuantityAvailable
     */
    public String getTotalQuantityAvailable() {
        return totalQuantityAvailable;
    }

    /**
     * @param totalQuantityAvailable
     *            the totalQuantityAvailable to set
     */
    public void setTotalQuantityAvailable(final String totalQuantityAvailable) {
        this.totalQuantityAvailable = totalQuantityAvailable;
    }

    /**
     * @return the adjustmentQuantity
     */
    public String getAdjustmentQuantity() {
        return adjustmentQuantity;
    }

    /**
     * @param adjustmentQuantity
     *            the adjustmentQuantity to set
     */
    public void setAdjustmentQuantity(final String adjustmentQuantity) {
        this.adjustmentQuantity = adjustmentQuantity;
    }

    /**
     * @return the store
     */
    public String getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final String store) {
        this.store = store;
    }



}
