/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Map;

import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Utility class for Refund functionality
 * 
 * @author pratik
 *
 */
public final class RefundUtil {

    private RefundUtil() {

    }

    /**
     * Method to check if delivery fee is refundable for order
     * 
     * @param orderModel
     * @return boolean
     */
    public static boolean isDeliveryFeeRefundable(final OrderModel orderModel) {
        return ServiceLookup.getTargetRefundService().isDeliveryCostRefundable(orderModel);
    }

    /**
     * Fetch all returnable entries for the order
     * 
     * @param orderModel
     * @return returnableEntries
     */
    public static Map<AbstractOrderEntryModel, Long> getAllReturnableEntries(final OrderModel orderModel) {
        return ServiceLookup.getTargetOrderReturnService().getAllReturnableEntries(orderModel);
    }

    /**
     * Method to get remaining refundable delivery fee for order.
     * 
     * @param orderModel
     * @return refundableDeliveryFee
     */
    public static double getRefundableDeliveryFee(final OrderModel orderModel) {
        return Math.max(
                orderModel.getInitialDeliveryCost().doubleValue()
                        - ServiceLookup.getFindOrderRefundedShippingStrategy().getRefundedShippingAmount(orderModel),
                0d);
    }

}
