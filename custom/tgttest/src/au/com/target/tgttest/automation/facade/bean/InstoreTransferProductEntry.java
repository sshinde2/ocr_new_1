/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Vivek
 *
 */
public class InstoreTransferProductEntry {

    private final String product;
    private final long qty;

    /**
     * @param productCode
     * @param qty
     */
    public InstoreTransferProductEntry(final String productCode, final long qty) {
        this.product = productCode;
        this.qty = qty;
    }

    /**
     * @return the productCode
     */
    public String getProduct() {
        return product;
    }

    /**
     * @return the qty
     */
    public long getQty() {
        return qty;
    }
}
