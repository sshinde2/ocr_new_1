/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.BulkyboardStockUpdate;


/**
 * Utils for bulkyboard tests
 * 
 */
public final class BulkyboardFacade {

    private BulkyboardFacade() {
        // util
    }

    public static final StockUpdateResponseDto generateResponseDto(final List<BulkyboardStockUpdate> updates) {
        final Map<Integer, List<BulkyboardStockUpdate>> storeMap = new HashMap<Integer, List<BulkyboardStockUpdate>>();

        // Sort into stores
        for (final BulkyboardStockUpdate update : updates) {

            final Integer store = Integer.valueOf(update.getStore());

            if (!storeMap.containsKey(store)) {
                storeMap.put(store, new ArrayList<BulkyboardStockUpdate>());
            }
            storeMap.get(store).add(update);
        }

        final StockUpdateResponseDto response = new StockUpdateResponseDto();

        final List<StockUpdateStoreResponseDto> storeResponses = new ArrayList<>();
        for (final Integer store : storeMap.keySet()) {
            final StockUpdateStoreResponseDto storeResponse = new StockUpdateStoreResponseDto();
            storeResponse.setStoreNumber(store.toString());
            storeResponse.setSuccess(true);

            final List<BulkyboardStockUpdate> storeUpdates = storeMap.get(store);
            final List<StockUpdateProductResponseDto> productResponses = new ArrayList<>();
            for (final BulkyboardStockUpdate update : storeUpdates) {
                final StockUpdateProductResponseDto productResponse = new StockUpdateProductResponseDto();
                productResponse.setItemcode(update.getProduct());
                productResponse.setSoh(Integer.toString(update.getSoh()));
                productResponse.setMessage(update.getMessage());
                productResponse.setSuccess(update.isSuccess());

                productResponses.add(productResponse);
            }
            storeResponse.setStockUpdateProductResponseDtos(productResponses);

            storeResponses.add(storeResponse);
        }

        response.setStockUpdateStoreResponseDtos(storeResponses);
        return response;
    }

    /**
     * Generate the xml that would be supplied via webmethods for the given product stock levels
     * 
     * @param updates
     * @return xml string
     */
    public static final String generateXml(final List<BulkyboardStockUpdate> updates) {

        final Map<Integer, List<BulkyboardStockUpdate>> storeMap = new HashMap<Integer, List<BulkyboardStockUpdate>>();

        // Sort into stores
        for (final BulkyboardStockUpdate update : updates) {

            final Integer store = Integer.valueOf(update.getStore());

            if (!storeMap.containsKey(store)) {
                storeMap.put(store, new ArrayList<BulkyboardStockUpdate>());
            }
            storeMap.get(store).add(update);
        }

        // Append xml for each store
        StringBuilder storeElements = new StringBuilder();
        for (final Integer store : storeMap.keySet()) {
            final List<BulkyboardStockUpdate> storeUpdates = storeMap.get(store);

            StringBuilder productElements = new StringBuilder();
            for (final BulkyboardStockUpdate update : storeUpdates) {

                final String productElement = getProductElementTemplate()
                        .replace("KEYCODE", update.getProduct())
                        .replace("SOH", Integer.toString(update.getSoh()))
                        .replace("MESSAGE", update.getMessage());
                productElements = productElements.append(productElement);
            }

            final String storeElement = getStoreElementTemplate()
                    .replace("STORENUMBER", store.toString())
                    .replace("PRODUCTLIST", productElements);


            storeElements = storeElements.append(storeElement);
        }

        final String xml = getMainTemplate().replace("STORELIST", storeElements);
        return xml;
    }

    private static String getMainTemplate() {
        return "<?xml version=\"1.0\"?>\n<integration-stockUpdateResponse>\n<stores>\n"
                + "STORELIST"
                + "</stores>\n</integration-stockUpdateResponse>";
    }

    private static String getStoreElementTemplate() {
        return "<store>\n<storeNumber>STORENUMBER</storeNumber>\n"
                + "PRODUCTLIST"
                + "</store>\n";
    }

    private static String getProductElementTemplate() {
        return "<product>\n<keycode>KEYCODE</keycode>\n"
                + "<soh>SOH</soh>\n"
                + "<message>MESSAGE</message>\n"
                + "</product>\n";
    }


    /**
     * Get actual stock level of product for store
     * 
     * @param store
     * @param code
     * @return amount
     * @throws Exception
     */
    public static int getStockForProductAtStore(final int store, final String code) throws Exception {

        final ProductModel product = ProductUtil.getStagedProductModel(code);

        final TargetPointOfServiceModel pointOfService = ServiceLookup.getTargetPointOfServiceService()
                .getPOSByStoreNumber(Integer.valueOf(store));
        final WarehouseModel warehouse = ServiceLookup.getTargetWarehouseService().getWarehouseForPointOfService(
                pointOfService);

        final StockLevelModel stockLevel = ServiceLookup.getStockService().getStockLevel(product, warehouse);
        return stockLevel.getAvailable();
    }
}
