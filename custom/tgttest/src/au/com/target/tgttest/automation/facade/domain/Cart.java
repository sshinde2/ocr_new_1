/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;



/**
 * Represents singleton instance of the cart - global per scenario. <br/>
 * Note: session and checkout cart models are now available from CartUtil and CheckoutUtil, not stored here.
 * 
 */
public final class Cart {

    private static Cart theCart = new Cart();


    private Cart() {
        // Empty
    }

    public static Cart getInstance() {
        return theCart;
    }

}
