/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

public class CreditCardData {
    //this is to be used as key in the map
    private String paymentId;
    private String cardNumber;
    private String cardType;
    private String cardExpiry;
    private String token;
    private String tokenExpiry;
    private String bin;
    private Boolean defaultCard;
    //amount is used with split payments
    private String amount;
    private Boolean cardOnFile;
    private Boolean firstCredentialStorage;

    public Boolean getCardOnFile() {
        return cardOnFile;
    }

    public void setCardOnFile(final Boolean cardOnFile) {
        this.cardOnFile = cardOnFile;
    }

    public Boolean getFirstCredentialStorage() {
        return firstCredentialStorage;
    }

    public void setFirstCredentialStorage(final Boolean firstCredentialStorage) {
        this.firstCredentialStorage = firstCredentialStorage;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardExpiry
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * @param cardExpiry
     *            the cardExpiry to set
     */
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the tokenExpiry
     */
    public String getTokenExpiry() {
        return tokenExpiry;
    }

    /**
     * @param tokenExpiry
     *            the tokenExpiry to set
     */
    public void setTokenExpiry(final String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    /**
     * @return the bin
     */
    public String getBin() {
        return bin;
    }

    /**
     * @param bin
     *            the bin to set
     */
    public void setBin(final String bin) {
        this.bin = bin;
    }

    /**
     * @return the defaultCard
     */
    public Boolean isDefaultCard() {
        return defaultCard;
    }

    /**
     * @param defaultCard
     *            the defaultCard to set
     */
    public void setDefaultCard(final Boolean defaultCard) {
        this.defaultCard = defaultCard;
    }


    /**
     * @return the paymentId
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId
     *            the paymentId to set
     */
    public void setPaymentId(final String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }


}