package au.com.target.tgttest.automation.datasetup;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.constants.TgttestConstants;


/**
 * Helper to create orders.
 * 
 * @author jjayawa1
 *
 */
public class OrderCreationHelper {

    private static final String ORDER_SEARCH_QUERY = "SELECT {" + OrderModel._TYPECODE + "." + OrderModel.PK
            + "} FROM {" + OrderModel._TYPECODE
            + "} WHERE {"
            + OrderModel._TYPECODE + "." + OrderModel.CODE + "} LIKE ?orderPrefix";

    private static final String DISCOUNT_SEARCH_QUERY = "SELECT {" + DiscountModel._TYPECODE + "." + DiscountModel.PK
            + "} FROM {" + DiscountModel._TYPECODE
            + "} WHERE {"
            + DiscountModel._TYPECODE + "." + DiscountModel.CODE + "} LIKE ?orderPrefix";

    @Resource
    private ModelService modelService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private PurchaseOptionConfigService purchaseOptionConfigService;

    private Map<String, String> consignmentToOrderStatusMap;

    public OrderModel createOrder(final String deliveryMode, final String orderCode, final boolean isRegisteredUser,
            final Integer cncStoreNumber, final Double deliveryFee, final String salesChannel)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        final OrderModel order = modelService.create(OrderModel.class);

        // Default delivery mode to click and collect
        String delMode = deliveryMode;
        if (delMode == null) {
            delMode = CheckoutUtil.CLICK_AND_COLLECT_CODE;
        }
        final DeliveryModeModel zoneDeliveryMode = ServiceLookup.getTargetDeliveryService().getDeliveryModeForCode(
                delMode);

        order.setCode(orderCode);
        order.setCurrency(commonI18NService.getCurrentCurrency());
        if (StringUtils.equals("eBay", salesChannel)) {
            order.setSalesApplication(SalesApplication.EBAY);
        }
        else {
            order.setSalesApplication(SalesApplication.WEB);
        }
        order.setDeliveryMode(zoneDeliveryMode);
        order.setCncStoreNumber(cncStoreNumber);
        order.setInitialDeliveryCost(deliveryFee);
        final PurchaseOptionConfigModel purchaseOption = purchaseOptionConfigService
                .getPurchaseOptionConfigByCode("buynowConfig");
        order.setPurchaseOptionConfig(purchaseOption);

        final TargetAddressModel paymentAddress = modelService.create(TargetAddressModel.class);
        final TitleModel title = ServiceLookup.getModelService().create(TitleModel.class);
        title.setCode("mr");
        final TitleModel paymentAddressTitle = ServiceLookup.getFlexibleSearchService().getModelByExample(title);
        paymentAddress.setTitle(paymentAddressTitle);

        final TargetAddressModel deliveryAddress = modelService.create(TargetAddressModel.class);
        deliveryAddress.setFirstname("FirstName");
        deliveryAddress.setLastname("LastName");
        deliveryAddress.setPhone1("0420000000");

        if (isRegisteredUser) {
            final CustomerModel loggedInUser = CustomerUtil
                    .getTargetCustomer(CustomerUtil.getStandardTestCustomerUid());
            order.setUser(loggedInUser);
            paymentAddress.setOwner(loggedInUser);
            order.setPaymentAddress(paymentAddress);
            deliveryAddress.setOwner(loggedInUser);
            order.setDeliveryAddress(deliveryAddress);
        }
        else {
            final CustomerModel guestUser = CustomerUtil.getDefaultGuestUserModel();
            order.setUser(guestUser);
            paymentAddress.setOwner(guestUser);
            order.setPaymentAddress(paymentAddress);
            deliveryAddress.setOwner(guestUser);
            order.setDeliveryAddress(deliveryAddress);
        }
        order.setPurchaseOptionConfig(ServiceLookup.getPurchaseOptionConfigService().getCurrentPurchaseOptionConfig());
        final OrderEntryModel orderEntry = ServiceLookup.getModelService().create(OrderEntryModel.class);
        final ProductModel prod = ProductUtil.getProductModel(CheckoutUtil.ANY_PRD_CODE);
        orderEntry.setQuantity(Long.valueOf(2));
        orderEntry.setProduct(prod);
        orderEntry.setUnit(prod.getUnit());
        orderEntry.setOrder(order);
        orderEntry.setBasePrice(Double.valueOf(CheckoutUtil.ANY_PRD_PRICE));
        order.setSubtotal(Double.valueOf(30.0d));
        order.setDate(new Date());
        order.setStatus(OrderStatus.INPROGRESS);
        modelService.save(orderEntry);
        modelService.save(order);
        return order;
    }

    public void removeAllOrders() {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(ORDER_SEARCH_QUERY);
        searchQuery.addQueryParameter("orderPrefix", "%" + TgttestConstants.ORDER_PREFIX + "%");
        final SearchResult<OrderModel> searchResult = ServiceLookup.getFlexibleSearchService().search(
                searchQuery);

        if (CollectionUtils.isNotEmpty(searchResult.getResult())) {
            for (final OrderModel order : searchResult.getResult()) {

                ServiceLookup.getModelService().refresh(order);
                ServiceLookup.getModelService().remove(order);
            }
        }

        removeAllOrderDiscounts();
    }

    private void removeAllOrderDiscounts() {

        // Remove refund vouchers
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(DISCOUNT_SEARCH_QUERY);
        searchQuery.addQueryParameter("orderPrefix", TgttestConstants.ORDER_PREFIX + "%");
        final SearchResult<DiscountModel> searchResult = ServiceLookup.getFlexibleSearchService().search(
                searchQuery);

        if (CollectionUtils.isNotEmpty(searchResult.getResult())) {
            for (final DiscountModel discount : searchResult.getResult()) {

                ServiceLookup.getModelService().remove(discount);
            }
        }
    }

    /**
     * return the mapping order status for a consignment status
     * 
     * @param consignmentStatus
     * @return Order Status
     */
    public OrderStatus getOrderStatus(final String consignmentStatus) {
        if (consignmentStatus != null && consignmentToOrderStatusMap.containsKey(consignmentStatus.toUpperCase())) {
            return OrderStatus.valueOf(consignmentToOrderStatusMap.get(consignmentStatus.toUpperCase()));
        }
        return OrderStatus.INPROGRESS;
    }

    /**
     * @param consignmentToOrderStatusMap
     *            the consignmentToOrderStatusMap to set
     */
    @Required
    public void setConsignmentToOrderStatusMap(final Map<String, String> consignmentToOrderStatusMap) {
        this.consignmentToOrderStatusMap = consignmentToOrderStatusMap;
    }



}
