/**
 * 
 */
package au.com.target.tgttest.automation;

import au.com.target.tgtcore.ticket.service.impl.TestTicketBusinessServiceImpl;
import au.com.target.tgttest.automation.facade.CncOrderUtil;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Customer;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.mock.MockAccertifyRestOperations;
import au.com.target.tgttest.automation.mock.MockGeoWebServiceWrapper;
import au.com.target.tgttest.automation.mock.MockGlobalStoreFulfilmentCapabilitiesInterceptor;
import au.com.target.tgttest.automation.mock.MockISTMessageConverter;
import au.com.target.tgttest.automation.mock.MockInventoryAdjustmentClientService;
import au.com.target.tgttest.automation.mock.MockJmsMessageDispatcher;
import au.com.target.tgttest.automation.mock.MockStoreFulfilmentCapabilitiesInterceptor;
import au.com.target.tgttest.automation.mock.MockTLOGMessageConverter;
import au.com.target.tgttest.automation.mock.MockTargetCaInventoryService;
import au.com.target.tgttest.automation.mock.MockTargetCaShippingService;
import au.com.target.tgttest.automation.mock.MockTargetPOSProductClient;
import au.com.target.tgttest.automation.mock.MockTaxInvoiceService;
import au.com.target.tgttinker.mock.payment.IpgClientMock;


/**
 * Performs initialisation of the test system<br/>
 * Note this is used by both tgttest facade tests and the Selenium tests.
 *
 */
public final class TestInitialiser {

    private TestInitialiser() {
        // util
    }


    /**
     * Reset domain objects before each scenario
     */
    public static void resetDomainObjects() {
        Order.reset();
        Customer.setStandardCustomer();
        Customer.getInstance().initialize();
        Checkout.initialise();
    }


    /**
     * Initialize mocks
     *
     * @throws Exception
     */
    public static void setMocks() throws Exception {
        MockTLOGMessageConverter.setMock();
        MockJmsMessageDispatcher.setMock();
        MockTargetPOSProductClient.setMock();
        MockAccertifyRestOperations.setMock();
        MockGeoWebServiceWrapper.setMock();
        MockISTMessageConverter.setMock();
        MockTargetCaShippingService.setMock();
        MockTargetCaInventoryService.setMock();
        MockInventoryAdjustmentClientService.setMock();

        ServiceLookup.getGenerateTaxInvoiceAction().setTaxInvoiceService(MockTaxInvoiceService.getInstance());
        ServiceLookup.getGlobalStoreFulfilmentInterceptorMapping().setInterceptor(
                MockGlobalStoreFulfilmentCapabilitiesInterceptor.getInstance());
        ServiceLookup.getStoreFulfillmentCapabilitiesInterceptor().setInterceptor(
                MockStoreFulfilmentCapabilitiesInterceptor.getMockInstance());
        ServiceLookup.getTargetCommerceCartFactory().setActive(true);
        ServiceLookup.getTargetPartnerOrderService().setActive(true);
        ServiceLookup.getTargetCustomerSubscriptionClientMock().setActive(true);
        ServiceLookup.getPaypalServiceMock().setActive(true);
        ServiceLookup.getTargetWmSendToWarehouseLogger().setActive(true);
        ServiceLookup.getStockVisibilityClientMock().setActive(true);
        ServiceLookup.getTargetAfterpayClientMock().setActive(true);
        ServiceLookup.getTargetAfterpayClientMock().setCapturePaymentActive(true);
        ServiceLookup.getTargetAfterpayClientMock().setGetOrderActive(true);
        ServiceLookup.getTargetZippayClientMock().activateMocks();
        ServiceLookup.getFluentClientMock().setActive(true);
        ServiceLookup.getEndecaProductQueryBuilder().setActive(true);
    }


    /**
     * Reset mocks before each scenario
     */
    public static void resetMocks() {
        MockAccertifyRestOperations.setDefaultRecommendation();

        TestTicketBusinessServiceImpl.clearTicketId();
        CncOrderUtil.setMockResponseStatus("SUCCESS");

        // TNS mock as APPROVED by default
        ServiceLookup.getTargetMockCcpayment().setMockResponse("APPROVED", 0, "APPROVED");

        MockJmsMessageDispatcher.clean();
        MockISTMessageConverter.clear();
        MockTLOGMessageConverter.clear();
        MockTargetCaShippingService.clear();
        MockTargetCaInventoryService.clear();

        ServiceLookup.getTargetBusinessProcessService().setActive(false);

        ServiceLookup.getTargetStoreStockService().clearTestData();
        ServiceLookup.getTargetStoreStockService().setActive(false);

        ServiceLookup.getTargetMockCcpayment().setActive(true);

        resetIPG();

        ServiceLookup.getFlybuysclientMockService().setActive(true);
        ServiceLookup.getTargetSendSmsClient().setActive(true);

        ServiceLookup.getTargetStockUpdateClient().setActive(true);
        ServiceLookup.getTargetStockUpdateClient().clearStoreStock();

        ServiceLookup.getTransmitManifestClient().setActive(true);
        ServiceLookup.getTransmitManifestClient().reset();

        ServiceLookup.getDispatchLabelClient().setActive(true);
        ServiceLookup.getDispatchLabelClient().reset();

        ServiceLookup.getSendToWarehouseRestClient().setActive(true);
        ServiceLookup.getSendToWarehouseRestClient().reset();

        ServiceLookup.getPaypalServiceMock().reset();
        ServiceLookup.getPaypalServiceMock().setActive(true);

        ServiceLookup.getTargetWmSendToWarehouseLogger().setActive(true);
        ServiceLookup.getTargetWmSendToWarehouseLogger().reset();

        ServiceLookup.getTargetAfterpayClientMock().resetDefault();

        ServiceLookup.getTargetAfterpayClientMock().activateMocks();

        ServiceLookup.getTargetZippayClientMock().resetDefault();

        ServiceLookup.getTargetZippayClientMock().activateMocks();

        ServiceLookup.getMockAuspostShipsterClient().resetDefault();

        ServiceLookup.getMockAuspostShipsterClient().activateMocks();

        ServiceLookup.getFluentClientMock().reset();
    }


    /**
     * 
     */
    public static void resetIPG() {
        final IpgClientMock ipgClientMock = ServiceLookup.getIpgClientMock();
        ipgClientMock.reset();
        ipgClientMock.setActive(true);
        ipgClientMock.setGiftCardPayment(false);
        ipgClientMock.setSinglePayResponseCode("0");
        ipgClientMock.setCheckResponseCode("1");
        ipgClientMock.resetSavedCards();
        ipgClientMock.setCustomerCardData(null);
        ipgClientMock.setCardResultsData(null);
        ipgClientMock.setSinglePayDeclinedCode(null);
        ipgClientMock.setSinglePayDeclinedMsg(null);
        ipgClientMock.setMockFetchToken(true);
        ipgClientMock.setMockSubmitSinglePayment(true);
        ipgClientMock.setMockQuery(true);
        ipgClientMock.setMockRefundResponse("Success");
    }


}
