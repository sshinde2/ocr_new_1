/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author pthoma20
 *
 */
public class ShopTheLookLooksData {

    private String lookId;

    private String lookName;

    private String url;

    private String description;

    private String collectionId;

    private String collectionName;

    /**
     * @return the lookId
     */
    public String getLookId() {
        return lookId;
    }

    /**
     * @param lookId
     *            the lookId to set
     */
    public void setLookId(final String lookId) {
        this.lookId = lookId;
    }

    /**
     * @return the lookName
     */
    public String getLookName() {
        return lookName;
    }

    /**
     * @param lookName
     *            the lookName to set
     */
    public void setLookName(final String lookName) {
        this.lookName = lookName;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the collectionId
     */
    public String getCollectionId() {
        return collectionId;
    }

    /**
     * @param collectionId
     *            the collectionId to set
     */
    public void setCollectionId(final String collectionId) {
        this.collectionId = collectionId;
    }

    /**
     * @return the collectionName
     */
    public String getCollectionName() {
        return collectionName;
    }

    /**
     * @param collectionName
     *            the collectionName to set
     */
    public void setCollectionName(final String collectionName) {
        this.collectionName = collectionName;
    }

}