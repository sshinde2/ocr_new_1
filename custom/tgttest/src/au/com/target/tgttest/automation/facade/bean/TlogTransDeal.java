/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * represent an expected transaction deal element
 * 
 */
public class TlogTransDeal {

    private final String type;
    private final String id;
    private final int instance;
    private final int markdown;

    /**
     * @param type
     * @param id
     * @param instance
     * @param markdown
     */
    public TlogTransDeal(final String type, final String id, final int instance, final int markdown) {
        super();
        this.type = type;
        this.id = id;
        this.instance = instance;
        this.markdown = markdown;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public int getInstance() {
        return instance;
    }

    public int getMarkdown() {
        return markdown;
    }

}
