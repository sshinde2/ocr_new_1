/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealBreakPointModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.DealBreakPointEntry;
import au.com.target.tgttest.automation.facade.bean.DealParameters;
import au.com.target.tgttest.automation.facade.bean.DealReferenceData;


/**
 * Facade for updating deals
 * 
 */
public final class DealUtil {

    private DealUtil() {
        // util
    }

    public static void initialise() {

        // Initialise deals 
        ImpexImporter.importCsv("/tgttest/automation-impex/deal/quantity-break-deal.impex");
    }

    public static void updateDealParameters(final DealParameters dealParams) {

        CatalogUtil.setSessionCatalogVersion();

        final AbstractSimpleDealModel deal = getDealModelByName(dealParams.getName());

        disableDeal(deal);

        if (dealParams.getRewardValue() != null) {
            deal.setRewardValue(dealParams.getRewardValue());
        }

        if (dealParams.getRewardType() != null) {
            deal.setRewardType(DealRewardTypeEnum.valueOf(dealParams.getRewardType().toUpperCase()));
        }

        if (dealParams.getRewardMaxQty() != null) {
            deal.setRewardMaxQty(dealParams.getRewardMaxQty());
        }

        enableDeal(deal);
    }

    public static void setDealQualifiers(final String dealName, final List<String> productCodes) {

        final DealReferenceData data = getDealData(dealName);
        final String qualCat = data.getQualifierCat();
        setDealCategoryProducts(qualCat, productCodes);
    }

    public static void setDealQualifiers(final String dealName, final List<String> productCodes, final int minQty) {
        final DealReferenceData data = getDealData(dealName);
        final String qualCat = data.getQualifierCat();
        setDealCategoryProducts(qualCat, productCodes);
        if (minQty > 0) {
            final AbstractSimpleDealModel deal = getDealModelByName(dealName);
            final List<DealQualifierModel> qualifiers = deal.getQualifierList();
            for (final DealQualifierModel qualifier : qualifiers) {
                qualifier.setMinQty(Integer.valueOf(minQty));
            }
        }
    }

    public static void setDealRewards(final String dealName, final List<String> productCodes) {

        final DealReferenceData data = getDealData(dealName);
        final String rewardCat = data.getRewardCat();
        setDealCategoryProducts(rewardCat, productCodes);
    }

    private static void setDealCategoryProducts(final String catCode, final List<String> productCodes) {

        CatalogUtil.setSessionCatalogVersion();

        final CategoryModel category = ServiceLookup.getCategoryService().getCategoryForCode(catCode);
        final List<ProductModel> existingProducts = ServiceLookup.getProductService().getOnlineProductsForCategory(
                category);

        for (final ProductModel product : existingProducts) {
            removeProductFromCategory(product, category);
        }

        for (final String productCode : productCodes) {
            final ProductModel product = ServiceLookup.getProductService().getProductForCode(productCode);
            addProductToCategory(product, category);
        }

    }

    private static void removeProductFromCategory(final ProductModel product, final CategoryModel category) {

        final Collection<CategoryModel> prodCats = product.getSupercategories();
        final Collection<CategoryModel> newCats = new ArrayList<>();

        for (final CategoryModel cat : prodCats) {
            if (!cat.getCode().equals(category.getCode())) {
                newCats.add(cat);
            }
        }
        product.setSupercategories(newCats);
        ServiceLookup.getModelService().save(product);
    }

    private static void addProductToCategory(final ProductModel product, final CategoryModel category) {

        final Collection<CategoryModel> prodCats = product.getSupercategories();
        final Collection<CategoryModel> newCats = new ArrayList<>();

        newCats.addAll(prodCats);
        newCats.add(category);

        product.setSupercategories(newCats);
        ServiceLookup.getModelService().save(product);
    }

    /**
     * Set the given quantity breaks on the quantity break deal
     * 
     * @param breakPoints
     */
    public static void setQuantityBreakDealBreaks(final String dealCode, final List<DealBreakPointEntry> breakPoints) {
        final QuantityBreakDealModel deal = getQuantityBreakDealModel(dealCode);

        // disable the deal so we can change the breaks
        disableDeal(deal);

        // remove any existing break points for the deal
        ServiceLookup.getModelService().removeAll(deal.getBreakPoints());
        ServiceLookup.getModelService().refresh(deal);


        // New list of breaks on the deal

        // Guid is to avoid clashes with breaks on existing orders
        final String breakCodeStem = deal.getCode() + "qtybrk" + new Long(new Date().getTime()).toString() + "_";

        final List<DealBreakPointModel> listBreaks = new ArrayList<>();
        int i = 0;
        for (final DealBreakPointEntry data : breakPoints) {

            i++;
            listBreaks.add(createDealBreak(data, deal, breakCodeStem + i));
        }

        ServiceLookup.getModelService().saveAll(listBreaks);
        ServiceLookup.getModelService().refresh(deal);


        // Now the breakpoints are set, we can enable the deal
        enableDeal(deal);
    }

    private static DealBreakPointModel createDealBreak(final DealBreakPointEntry data,
            final QuantityBreakDealModel deal, final String breakCode) {

        final DealBreakPointModel breakPoint = ServiceLookup.getModelService().create(DealBreakPointModel.class);
        breakPoint.setCode(breakCode);
        breakPoint.setDeal(deal);
        breakPoint.setQty(Integer.valueOf(data.getQty()));
        breakPoint.setUnitPrice(Double.valueOf(data.getUnitPrice()));
        return breakPoint;
    }


    private static QuantityBreakDealModel getQuantityBreakDealModel(final String dealCode) {

        // There may be clones on orders having immutable hash populated 

        final QuantityBreakDealModel example = new QuantityBreakDealModel();
        example.setCode(dealCode);
        final List<QuantityBreakDealModel> list = ServiceLookup.getFlexibleSearchService().getModelsByExample(example);

        QuantityBreakDealModel model = null;
        for (final QuantityBreakDealModel deal : list) {
            if (StringUtils.isEmpty(deal.getImmutableKeyHash())) {
                model = deal;
            }
        }

        return model;
    }

    private static AbstractSimpleDealModel getDealModelByName(final String dealName) {

        final DealReferenceData data = getDealData(dealName);
        return getDealModelByCode(data.getDealCode());
    }

    public static AbstractSimpleDealModel getDealModelByCode(final String dealCode) {

        // There may be clones on orders having immutable hash populated 

        final AbstractSimpleDealModel example = new AbstractSimpleDealModel();
        example.setCode(dealCode);
        final List<AbstractSimpleDealModel> list = ServiceLookup.getFlexibleSearchService().getModelsByExample(example);

        AbstractSimpleDealModel model = null;
        for (final AbstractSimpleDealModel deal : list) {
            if (StringUtils.isEmpty(deal.getImmutableKeyHash())) {
                model = deal;
            }
        }

        return model;
    }


    private static void disableDeal(final AbstractDealModel deal) {

        deal.setEnabled(Boolean.FALSE);
        ServiceLookup.getModelService().save(deal);
    }

    private static void enableDeal(final AbstractDealModel deal) {

        deal.setEnabled(Boolean.TRUE);
        ServiceLookup.getModelService().save(deal);
    }

    private static DealReferenceData getDealData(final String dealName) {

        final DealReferenceData data = DealReferenceData.getDealData(dealName);
        if (data == null) {
            throw new IllegalArgumentException("Can't find deal with name: " + dealName);
        }

        return data;
    }

    public static TargetMobileOfferHeadingModel findOfferHeadingForCode(final String code) {
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelExample = new TargetMobileOfferHeadingModel();
        targetMobileOfferHeadingModelExample.setCode(code);
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = ServiceLookup.getFlexibleSearchService()
                .getModelByExample(targetMobileOfferHeadingModelExample);
        return targetMobileOfferHeadingModel;
    }

}
