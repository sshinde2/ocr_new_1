/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;

import au.com.target.tgtauspost.model.AusPostChargeZoneModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.PostcodeData;
import au.com.target.tgtutility.util.TimeZoneUtils;


/**
 * @author rsamuel3
 *
 */
public class AuspostUtil {

    /**
     * 
     */
    private static final String AUTO_AREA_NAME = "Auto area name";
    private static final String DEFAULT_STATE = "VIC";

    private AuspostUtil() {

    }

    public static void insertUpdatePostcodeEntry(final PostcodeData postcode) {
        Assertions.assertThat(postcode.getPostcode()).isNotNull().isNotEmpty();
        PostCodeModel postcodeModel = getPostCodeModel(postcode.getPostcode());
        if (postcodeModel == null) {
            postcodeModel = createNewPostcodeModel(postcode);
        }
        updatePostCodeModel(postcode, postcodeModel);
    }

    /**
     * @param postcode
     * @return new PostCodeModel
     */
    public static PostCodeModel createNewPostcodeModel(final PostcodeData postcode) {
        final PostCodeModel postcodeModel = ServiceLookup.getModelService().create(PostCodeModel.class);
        postcodeModel.setPostCode(postcode.getPostcode());
        return postcodeModel;
    }

    /**
     * @param postcode
     * @param postcodeModel
     */
    public static void updatePostCodeModel(final PostcodeData postcode, final PostCodeModel postcodeModel) {
        String state = DEFAULT_STATE;
        if (StringUtils.isNotBlank(postcode.getState())) {
            state = postcode.getState();
        }
        postcodeModel.setTimeZone(TimeZoneUtils.getTimeZoneFromState(state));
        if (StringUtils.isNotBlank(postcode.getChargeZone())) {
            final AusPostChargeZoneModel chargeZoneModel = ServiceLookup.getModelService().create(
                    AusPostChargeZoneModel.class);
            chargeZoneModel.setChargeCode(postcode.getChargeZone());
            chargeZoneModel.setAreaName(AUTO_AREA_NAME);
            postcodeModel.setAuspostChargeZone(chargeZoneModel);
        }
        ServiceLookup.getModelService().save(postcodeModel);
    }

    /**
     * 
     * @param postcode
     * @return PostCodeModel
     */
    public static PostCodeModel getPostCodeModel(final String postcode) {
        return ServiceLookup.getTargetPostCodeService().getPostCode(postcode);
    }
}
