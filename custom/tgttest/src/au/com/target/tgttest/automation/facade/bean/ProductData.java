package au.com.target.tgttest.automation.facade.bean;

public class ProductData {
    private String productCode;
    private boolean displayOnly;
    private String approvedStatus;
    private String onlineDate;
    private double price;
    private String originalCategory;
    private String catalogVersionName;
    private String productDisplayType;
    private boolean colorVariant;
    private boolean sizeVariant;
    private String normalSalesDate;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    public boolean isDisplayOnly() {
        return displayOnly;
    }

    public void setDisplayOnly(final boolean displayOnly) {
        this.displayOnly = displayOnly;
    }

    public String getApprovedStatus() {
        return approvedStatus;
    }

    public void setApprovedStatus(final String approvedStatus) {
        this.approvedStatus = approvedStatus;
    }

    public String getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(final String onlineDate) {
        this.onlineDate = onlineDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    /**
     * @return the originalCategory
     */
    public String getOriginalCategory() {
        return originalCategory;
    }

    /**
     * @param originalCategory
     *            the originalCategory to set
     */
    public void setOriginalCategory(final String originalCategory) {
        this.originalCategory = originalCategory;
    }

    /**
     * @return the catalogVersionName
     */
    public String getCatalogVersionName() {
        return catalogVersionName;
    }

    /**
     * @param catalogVersionName
     *            the catalogVersionName to set
     */
    public void setCatalogVersionName(final String catalogVersionName) {
        this.catalogVersionName = catalogVersionName;
    }

    /**
     * @return the productDisplayType
     */
    public String getProductDisplayType() {
        return productDisplayType;
    }

    /**
     * @param productDisplayType
     *            the productDisplayType to set
     */
    public void setProductDisplayType(final String productDisplayType) {
        this.productDisplayType = productDisplayType;
    }

    /**
     * @return the colorVariant
     */
    public boolean isColorVariant() {
        return colorVariant;
    }

    /**
     * @param colorVariant
     *            the colorVariant to set
     */
    public void setColorVariant(final boolean colorVariant) {
        this.colorVariant = colorVariant;
    }

    /**
     * @return the sizeVariant
     */
    public boolean isSizeVariant() {
        return sizeVariant;
    }

    /**
     * @param sizeVariant
     *            the sizeVariant to set
     */
    public void setSizeVariant(final boolean sizeVariant) {
        this.sizeVariant = sizeVariant;
    }

    /**
     * @return the normalSalesDate
     */
    public String getNormalSalesDate() {
        return normalSalesDate;
    }

    /**
     * @param normalSalesDate
     *            the normalSalesDate to set
     */
    public void setNormalSalesDate(final String normalSalesDate) {
        this.normalSalesDate = normalSalesDate;
    }

}
