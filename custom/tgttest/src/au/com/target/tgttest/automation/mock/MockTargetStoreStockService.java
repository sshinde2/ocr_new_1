/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtfulfilment.stock.service.impl.TargetFulfilmentStoreStockServiceImpl;


/**
 * Mock for TargetStoreStockService - can set a list of stores for which isOrderInStockAtStore returns true.
 * 
 * @author jjayawa1
 *
 */
public class MockTargetStoreStockService extends TargetFulfilmentStoreStockServiceImpl {

    private boolean active = false;

    private final Map<Integer, Boolean> instockMap = new HashMap<Integer, Boolean>();


    /**
     * Adds mock data
     * 
     * @param storeNumber
     * @param instock
     */
    public void addMockData(final Integer storeNumber, final Boolean instock) {
        instockMap.put(storeNumber, instock);
    }

    /**
     * Clears mock data
     */
    public void clearTestData() {
        instockMap.clear();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.stock.service.impl.TargetStoreStockServiceImpl#isOrderInStockAtStore(java.lang.String, de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public boolean isOrderEntriesInStockAtStore(final List<AbstractOrderEntryModel> orderEntries, final Integer store,
            final String orderCode) {

        if (active) {
            return instockMap.get(store).booleanValue();
        }

        return super.isOrderEntriesInStockAtStore(orderEntries, store, orderCode);

    }

    /**
     * Set this mock active or not
     * 
     * @param active
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

}
