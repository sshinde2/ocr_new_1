/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author mjanarth
 *
 */
public class AccertifyOrderItem {

    private String itemNumber;
    private int quantity;
    private String recipientEmailAddresses;
    private String category;

    /**
     * @return the itemNumber
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * @param itemNumber
     *            the itemNumber to set
     */
    public void setItemNumber(final String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * @return the recipientEmailAddresses
     */
    public String getRecipientEmailAddresses() {
        return recipientEmailAddresses;
    }

    /**
     * @param recipientEmailAddresses
     *            the recipientEmailAddresses to set
     */
    public void setRecipientEmailAddresses(final String recipientEmailAddresses) {
        this.recipientEmailAddresses = recipientEmailAddresses;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category
     *            the category to set
     */
    public void setCategory(final String category) {
        this.category = category;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

}
