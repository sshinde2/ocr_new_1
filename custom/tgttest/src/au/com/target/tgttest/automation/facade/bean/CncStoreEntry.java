/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bhuang3
 *
 */
public class CncStoreEntry {

    private String storeName;

    private String storeNumber;

    private String productTypes;

    private boolean acceptCNC;

    private boolean closed;

    private boolean availableForPickup = true;

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the productTypes
     */
    public String getProductTypes() {
        return productTypes;
    }

    /**
     * @param productTypes
     *            the productTypes to set
     */
    public void setProductTypes(final String productTypes) {
        this.productTypes = productTypes;
    }

    /**
     * @return the acceptCNC
     */
    public boolean isAcceptCNC() {
        return acceptCNC;
    }

    /**
     * @param acceptCNC
     *            the acceptCNC to set
     */
    public void setAcceptCNC(final boolean acceptCNC) {
        this.acceptCNC = acceptCNC;
    }

    /**
     * @return the closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed
     *            the closed to set
     */
    public void setClosed(final boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the availableForPickup
     */
    public boolean isAvailableForPickup() {
        return availableForPickup;
    }

    /**
     * @param availableForPickup
     *            the availableForPickup to set
     */
    public void setAvailableForPickup(final boolean availableForPickup) {
        this.availableForPickup = availableForPickup;
    }

}
