/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rmcalave
 *
 */
public class CustomerRegistrationData {
    private String firstName;

    private String lastName;

    private String password;

    private boolean optIntoMarketing;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the optIntoMarketing
     */
    public boolean isOptIntoMarketing() {
        return optIntoMarketing;
    }

    /**
     * @param optIntoMarketing
     *            the optIntoMarketing to set
     */
    public void setOptIntoMarketing(final boolean optIntoMarketing) {
        this.optIntoMarketing = optIntoMarketing;
    }
}
