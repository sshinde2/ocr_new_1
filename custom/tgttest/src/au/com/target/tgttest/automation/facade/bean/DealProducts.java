/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.util.ArrayList;
import java.util.List;


/**
 * @author sbryan6
 * 
 */
public class DealProducts {

    private final String dealName;
    private final List<String> qualifiers = new ArrayList<>();
    private final List<String> rewards = new ArrayList<>();

    /**
     * @param dealName
     */
    public DealProducts(final String dealName) {
        super();
        this.dealName = dealName;
    }

    public void addQualifier(final String product) {
        qualifiers.add(product);
    }

    public void addReward(final String product) {
        rewards.add(product);
    }

    public String getDealName() {
        return dealName;
    }

    public List<String> getQualifiers() {
        return qualifiers;
    }

    public List<String> getRewards() {
        return rewards;
    }

}
