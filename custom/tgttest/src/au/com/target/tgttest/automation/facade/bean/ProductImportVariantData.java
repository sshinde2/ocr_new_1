/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rsamuel3
 *
 */
public class ProductImportVariantData {
    private String code;
    private String approvalStatus;
    private String size;
    private String merchProductStatus;

    /**
     * @return the approvalStatus
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * @param approvalStatus
     *            the approvalStatus to set
     */
    public void setApprovalStatus(final String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the merchProductStatus
     */
    public String getMerchProductStatus() {
        return merchProductStatus;
    }

    /**
     * @param merchProductStatus
     *            the merchProductStatus to set
     */
    public void setMerchProductStatus(final String merchProductStatus) {
        this.merchProductStatus = merchProductStatus;
    }
}
