/**
 * 
 */
package au.com.target.tgttest.automation.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import au.com.target.tgttest.automation.TestInitialiser;


/**
 * Initialisation required for testing via selenium
 * 
 */
@Path("/testinitialisation")
public class TestInitialisationResource {


    @GET
    @Produces("text/plain")
    @Path("init")
    public String initialise() throws Exception {

        TestInitialiser.resetDomainObjects();
        TestInitialiser.setMocks();
        TestInitialiser.resetMocks();

        return "success";
    }

    @GET
    @Produces("text/plain")
    @Path("reset")
    public String reset() {

        TestInitialiser.resetDomainObjects();
        TestInitialiser.resetMocks();

        return "success";
    }
}
