/**
 * 
 */
package au.com.target.tgttest.automation.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgttest.automation.facade.bean.PosProductData;
import au.com.target.tgttest.automation.mock.MockTargetPOSProductClient;


/**
 * Web Service to inject mocks
 * 
 */
@Path("/injectmocks")
public class InjectMocksResource {

    @GET
    @Produces("text/plain")
    @Path("posProductClient")
    public String posProductClient() {

        MockTargetPOSProductClient.setMock();

        // Product not in hybris
        addResponse(new PosProductData(7001, "5030949110350", "55555555",
                "Test pos description", Integer.valueOf(500), Integer.valueOf(800), null));

        // Product in hybris
        addResponse(new PosProductData(7001, "9345973983075", "P1000_black_S",
                "Baby shoes", Integer.valueOf(500), null, null));

        // Error
        addResponse(new PosProductData(7001, "746775078065", "",
                "", null, null, "POS error"));


        return "success";
    }


    private void addResponse(final PosProductData posData) {

        final ProductResponseDto response = new ProductResponseDto();
        response.setDescription(posData.getDesc());
        response.setItemcode(posData.getItemcode());
        response.setPrice(posData.getPrice());
        response.setWasPrice(posData.getWas());
        response.setErrorMessage(posData.getErrorCode());
        response.setSuccess(StringUtils.isEmpty(posData.getErrorCode()));

        MockTargetPOSProductClient.addResponse(posData.getEan(), response);
    }
}
