/**
 * 
 */
package au.com.target.tgtutility.format;

import java.util.Formatter;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public final class PhoneFormat {

    private static final String PHONE_FORMAT = "0%1$s %2$s%3$s%4$s%5$s %6$s%7$s%8$s%9$s";

    private static final String MOBILE_FORMAT = "0%1$s%2$s%3$s %4$s%5$s%6$s %7$s%8$s%9$s";

    private static final Pattern AUS_PHONE = Pattern.compile("\\+61(2|3|7|8)\\d{8}");

    private static final Pattern AUS_MOBILE = Pattern.compile("\\+61(4|5)\\d{8}");

    private static final Pattern AUS_MOBILE_SMS = Pattern.compile("\\+?61(4|5)\\d{8}");

    private static final Pattern AUS_MOBILE_SMS_OTHER = Pattern.compile("(61)?0?(4|5)\\d{8}");

    private PhoneFormat() {
        // avoid construction
    }

    /**
     * format phone number if it's an Australian phone or mobile, if not simply return the given phone number Valid
     * phone : +61276458765 Valid mobile : +61463748304
     * 
     * @param value
     * @return a formatted phone number
     */
    public static String formatNumber(final String value) {
        if (StringUtils.isBlank(value)) {
            return value;
        }
        if (AUS_PHONE.matcher(value).matches()) {
            return formatPhoneNumber(value);
        }
        if (AUS_MOBILE.matcher(value).matches()) {
            return formatMobileNumber(value);
        }
        return value;
    }

    /**
     * 
     * @param phone
     * @return a formated phone number
     */
    private static String formatPhoneNumber(final String phone) {
        final Character[] digits = ArrayUtils.toObject(phone.toCharArray());

        final StringBuilder formattedPhone = new StringBuilder();
        final Formatter formater = new Formatter(formattedPhone);
        formater.format(PHONE_FORMAT, digits[3], digits[4], digits[5], digits[6], digits[7], digits[8], digits[9],
                digits[10], digits[11]);
        formater.close();

        return formattedPhone.toString();
    }

    /**
     * 
     * @param phone
     * @return a formated mobile number
     */
    private static String formatMobileNumber(final String phone) {
        final Character[] digits = ArrayUtils.toObject(phone.toCharArray());
        final StringBuilder formattedPhone = new StringBuilder();
        final Formatter formater = new Formatter(formattedPhone);
        formater.format(MOBILE_FORMAT, digits[3], digits[4], digits[5], digits[6], digits[7], digits[8], digits[9],
                digits[10], digits[11]);
        formater.close();

        return formattedPhone.toString();
    }

    /**
     * 
     * @param value
     * @return a formated mobile number without plus for sms, return null if it is not a valid mobile number
     */
    public static String validateAndformatPhoneNumberForSms(final String value) {
        String formattedNumber = null;
        if (!StringUtils.isBlank(value)) {
            if (AUS_MOBILE_SMS.matcher(value).matches()) {
                formattedNumber = StringUtils.removeStart(value, "+");
            }
            else if (AUS_MOBILE_SMS_OTHER.matcher(value).matches()) {
                formattedNumber = value.replaceFirst("(61)?0?", "61");
            }
        }
        return formattedNumber;
    }
}
