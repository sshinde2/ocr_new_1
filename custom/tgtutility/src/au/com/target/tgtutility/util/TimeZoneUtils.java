/**
 * 
 */
package au.com.target.tgtutility.util;

import java.text.DecimalFormat;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author bhuang3
 * 
 */
public final class TimeZoneUtils {

    public static final String TIMEZONE_GMT = "GMT";

    public static final String TIMEZONE_UTC = "UTC";


    private static final Logger LOG = Logger.getLogger(TimeZoneUtils.class);
    private static final int MINUTES_IN_AN_HOUR = 60;
    private static final int SECONDS_IN_A_MINUTE = 60;

    private TimeZoneUtils() {
        //
    }

    /**
     * return the timeZone from the state
     * 
     * @param state
     * @return timezone of the state, null if not match
     */
    public static TimeZone getTimeZoneFromState(final String state) {

        if (StringUtils.equalsIgnoreCase(state, "VIC") || StringUtils.equalsIgnoreCase(state, "Victoria")) {
            return TimeZone.getTimeZone("Australia/Victoria");
        }
        if (StringUtils.equalsIgnoreCase(state, "NSW") || StringUtils.equalsIgnoreCase(state, "New South Wales")) {
            return TimeZone.getTimeZone("Australia/NSW");
        }
        if (StringUtils.equalsIgnoreCase(state, "QLD") || StringUtils.equalsIgnoreCase(state, "Queensland")) {
            return TimeZone.getTimeZone("Australia/Queensland");
        }
        if (StringUtils.equalsIgnoreCase(state, "SA") || StringUtils.equalsIgnoreCase(state, "South Australia")) {
            return TimeZone.getTimeZone("Australia/South");
        }
        if (StringUtils.equalsIgnoreCase(state, "TAS") || StringUtils.equalsIgnoreCase(state, "Tasmania")) {
            return TimeZone.getTimeZone("Australia/Tasmania");
        }
        if (StringUtils.equalsIgnoreCase(state, "WA") || StringUtils.equalsIgnoreCase(state, "Western Australia")) {
            return TimeZone.getTimeZone("Australia/West");
        }
        if (StringUtils.equalsIgnoreCase(state, "ACT")
                || StringUtils.equalsIgnoreCase(state, "Australian Capital Territory")) {
            return TimeZone.getTimeZone("Australia/ACT");
        }
        if (StringUtils.equalsIgnoreCase(state, "NT") || StringUtils.equalsIgnoreCase(state, "Northern Territory")) {
            return TimeZone.getTimeZone("Australia/North");
        }
        LOG.error("cannot get the time zone from state: " + state);
        return null;
    }

    /**
     * format the time zone for sms. Eg: +1100
     * 
     * @param timeZone
     * @return formated string
     */
    public static String formatTimeZoneOffsetForSms(final TimeZone timeZone) {
        if (timeZone != null) {
            String sign = "+";
            int totalSeconds = timeZone.getRawOffset() / 1000;
            if (totalSeconds < 0) {
                totalSeconds = -totalSeconds;
                sign = "-";
            }
            final int totalMinutes = totalSeconds / SECONDS_IN_A_MINUTE;
            final int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
            final int hours = totalMinutes / MINUTES_IN_AN_HOUR;
            final DecimalFormat df = new DecimalFormat("00");
            final String hour = df.format(hours);
            final String minute = df.format(minutes);
            return sign + hour + minute;
        }
        return null;
    }
}
