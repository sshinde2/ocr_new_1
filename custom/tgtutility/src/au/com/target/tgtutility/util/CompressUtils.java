/**
 * 
 */
package au.com.target.tgtutility.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author phi.tran
 * 
 */
public final class CompressUtils {

    private static final Integer BUFFER_SIZE = Integer.valueOf(1024);

    private CompressUtils() {

    }

    public static void compressToZipFormat(final String filePath, final String dest) throws IOException {
        if (StringUtils.isNotBlank(filePath) && StringUtils.isNotBlank(dest)) {
            final File input = new File(filePath);

            if (input.exists()) {
                final String fileName = FilenameUtils.removeExtension(input.getName());

                final ZipOutputStream zipOutputStream;
                zipOutputStream = new ZipOutputStream(new FileOutputStream(dest + "/" + fileName + ".zip"));


                zipOutputStream.putNextEntry(new ZipEntry(input.getName()));

                final BufferedInputStream bis = new BufferedInputStream(new FileInputStream(input));

                final byte[] bytesIn = new byte[BUFFER_SIZE.intValue()];
                int read = 0;

                while ((read = bis.read(bytesIn)) != -1) {
                    zipOutputStream.write(bytesIn, 0, read);
                }

                bis.close();

                zipOutputStream.closeEntry();
                zipOutputStream.flush();
                zipOutputStream.close();

            }
        }

    }


}
