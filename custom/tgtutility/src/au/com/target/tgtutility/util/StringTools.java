/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;


/**
 * @author fkratoch
 * 
 */
public final class StringTools {

    private static final Pattern REGEX_ALL_LEADING_TRAILING_WHITESPACE = Pattern.compile("(^\\s*|\\s*$)");

    private StringTools() {
    }

    /**************************************************************************
     * Remove zeros from the start of a string, if the sting is then empty, 0 will be returned.
     * 
     * @param toTrim
     * @return trimmed string.
     */
    public static String trimLeadingZeros(final String toTrim)
    {
        int start = 0;
        final int len = toTrim.length();
        while ((start < len) && (toTrim.charAt(start) == '0'))
        {
            start++;
        }
        if (start == 0)
        {
            return toTrim;
        }
        else if (start == len)
        {
            return "0";
        }
        else
        {
            return toTrim.substring(start);
        }
    }

    /**
     * Pad zeros from the start of a string to make it of size 8 when it is below 8 and of size 13 when it is above 8
     * digits and below 12.And keep 12 digit as it is.
     * 
     * @param toPad
     * @return trimmed string.
     */
    public static String padWithLeadingZeros(String toPad) {

        final int len = toPad.length();

        if (len < 8) {
            toPad = StringUtils.leftPad(toPad, 8, '0');
        }
        else if (len > 8 && len < 12) {
            toPad = StringUtils.leftPad(toPad, 13, '0');
        }

        return toPad;
    }

    /**
     * trim all leading and trailing white space (\t\n\x0b\r\f)
     * 
     * @param toTrim
     * @return clean string without any leading or trailing whitespace
     */
    public static String trimAllWhiteSpaces(final String toTrim) {
        if (toTrim == null) {
            return null;
        }
        final Matcher replace = StringTools.REGEX_ALL_LEADING_TRAILING_WHITESPACE.matcher(toTrim);
        return replace.replaceAll(StringUtils.EMPTY);
    }

}
