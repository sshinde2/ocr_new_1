/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.SerializationConfig;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;


/**
 * @author salexa10
 *
 */
public class TargetMappingJacksonHttpMessageConverter extends MappingJacksonHttpMessageConverter {
    private static final Logger LOG = Logger.getLogger(TargetMappingJacksonHttpMessageConverter.class);

    @Required
    public void setMixins(final Map<String, String> mixins) {
        final SerializationConfig serializationConfig = getObjectMapper().getSerializationConfig();
        final DeserializationConfig deserializationConfig = getObjectMapper().getDeserializationConfig();
       getObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
        serializationConfig.setMixInAnnotations(null);
        deserializationConfig.setMixInAnnotations(null);
        if (mixins != null) {
            for (final Entry<String, String> mixin : mixins.entrySet()) {
                try {
                    serializationConfig.addMixInAnnotations(Class.forName(mixin.getKey()),
                            Class.forName(mixin.getValue()));
                    deserializationConfig.addMixInAnnotations(Class.forName(mixin.getKey()),
                        Class.forName(mixin.getValue()));
                }
                catch (final ClassNotFoundException e) {
                    LOG.warn("Failed to config jackson mixin for target[" + mixin.getKey() + "]", e);
                }
            }
        }
    }
}
