/**
 * 
 */
package au.com.target.tgtutility.util;

import org.apache.commons.lang.StringUtils;


public final class MarkupUtils {

    private MarkupUtils() {
        // prevent construction
    }

    public static String stripFormValidation(final String value) {
        if (value != null && value.contains("~")) {
            final int removeSquiggle = value.indexOf('~') + 1;
            return removeSquiggle < value.length() ? value.substring(removeSquiggle) : "";
        }

        return value;
    }

    public static String stripHtmlTagsFromText(final String htmlText) {
        if (StringUtils.isBlank(htmlText)) {
            return htmlText;
        }

        String retText = htmlText;
        retText = retText.replaceAll("\\<.*?>", "");
        retText = retText.replaceAll("&nbsp;", "");
        retText = retText.replaceAll("&amp;", "");
        return retText;
    }

}
