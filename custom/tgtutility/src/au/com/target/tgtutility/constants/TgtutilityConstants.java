/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtutility.constants;

/**
 * Global class for all Tgtutility constants. You can add global constants for your extension into this class.
 */
public final class TgtutilityConstants {
    public static final String EXTENSIONNAME = "tgtutility";

    private TgtutilityConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
    public interface ErrorMessageFormat {
        String GENERIC_MESSAGE_FORMAT = "ERROR Message: [ %s ], ERROR Code: [ %s ]";
        String EXTENDED_MESSAGE_FORMAT = "ERROR Message: [ %s ], ERROR Code: [ %s ], Additional Information: [ %s ]";
        String ORDER_MESSAGE_FORMAT = "ERROR Message: [ %s ], ERROR Code: [ %s ], OrderCode:[ %s ]";
    }

    public interface InfoMessageFormat {
        String GENERIC_MESSAGE_FORMAT = "INFO Message=[ %s ], INFO Code=[ %s ]";
    }

    public interface InfoCode {
        String INFO_GIFTCARD_PAYMENT = "INFO_GIFTCARD_PAYMENT";
        String REQUEST_URI = "REQUEST_URI";
        String TOTAL_RECORDS_RETURNED = "TOTAL_RECORDS_RETURNED";
        String QUERY_STRING = "QUERY_STRING";
        String SHIPSTER_SEND_ORDER_CONFIRMATION_ACTION = "SHIPSTER_SEND_ORDER_CONFIRMATION_ACTION";
        String SHIPSTER_SEND_ORDER_CONFIRMATION_ACTION_RETRY = "SHIPSTER_SEND_ORDER_CONFIRMATION_ACTION_RETRY";
        String INFO_ORDERPAYMENTCAPTURE = "INFO_ORDERPAYMENTCAPTURE";
    }

    public interface ErrorCode {
        String ERR_GENERIC = "ERR_GENERIC";
        String ERR_TGTCORE = "ERR_TGTCORE";
        String WARN_TGTCORE = "WARN_TGTCORE";
        String ERR_TGTFULLFILLMENT = "ERR_TGTFULLFILLMENT";
        String ERR_TGTLAYBY = "ERR_TGTLAYBY";
        String WARN_TGTLAYBY = "WARN_TGTLAYBY";
        String ERR_TAXINVOICE = "ERR_TAXINVOICE";
        String ERR_PAYMENT = "ERR_PAYMENT";
        String ERR_CHECKOUT = "ERR_CHECKOUT";
        String WARN_PAYMENT = "WARN_PAYMENT";
        String ERR_REMOVE_PRODUCT = "ERR_REMOVE_PRODUCT";
        String ERR_TGTSALE = "ERR_TGTSALE";
        String ERR_TGTWEBMETHODS = "ERR_WEBMETHODS";
        String ERR_TGTEBAY = "ERR_EBAY_ORDERS";
        String WARN_PRODUCT_DIMENSIONS_NOT_FOUND = "WARN_PRODUCT_DIMENSIONS_NOT_FOUND";
        String WARN_TGTEBAY = "WARN_EBAY_ORDERS";
        String ERR_PINPADPAYMENT = "ERR_PINPADPAYMENT";
        String ERR_SHORTPICK_ON_FLYBUYS_REDEEM = "ERR_SHORTPICK_ON_FLYBUYS_REDEEM";
        String ERR_SEARCH = "ERR_SEARCH";
        String INFO_RETRY = "INFO_RETRY";
        String INFO_ORDERPAYMENTCAPTURE = "INFO_ORDERPAYMENTCAPTURE";
        String INFO_REFUND = "INFO_REFUND";
        String INVALID_CARD = "INVALID_CARD";
        String INFO_PAYMENT = "INFO_PAYMENT";
        String INFO_CANCELORDER = "INFO_CANCELORDER";
        String WARN_EMAIL_SEND_FAIL = "WARN_EMAIL_SEND_FAIL";
        String INFO_EMAIL_ATTACHMENT = "INFO_EMAIL_ATTACHMENT";
        String INFO_EMAIL_TAX_INVOICE = "INFO_EMAIL_TAX_INVOICE";
        String ERR_TGTSTORE_PRODUCT_SEARCH = "ERR_TGTSTORE_PRODUCT_SEARCH";
        String ERR_TGTSTORE_CATEGORY_SEARCH = "ERR_TGTSTORE_CATEGORY_SEARCH";
        String ERR_TGTSTORE_PRODUCTS_API_SEARCH = "ERR_TGTSTORE_PRODUCTS_API_SEARCH";
        String ERR_TGTSTORE_TEXT_SEARCH = "ERR_TGTSTORE_TEXT_SEARCH";
        String ERR_TGTSTORE_SESSION_ID = "ERR_TGTSTORE_SESSION_ID";
        String ERR_TGTSTORE_AUTO_LOGIN = "ERR_TGTSTORE_AUTO_LOGIN";
        String ERR_TGTFACADE_URL_ERROR = "ERR_TGTFACADE_URL_ERROR";
        String ERR_TGTOFCWS_ERROR = "ERR_TGTOFCWS_ERROR";
        String WARN_SMS_SEND_FAIL = "WARN_SMS_SEND_FAIL";
        String WARN_CUSTOMER_SUBSCRIPTION_SEND_FAIL = "WARN_CUSTOMER_SUBSCRIPTION_SEND_FAIL";
        String ERR_CUSTOMER_SUBSCRIPTION_SEND_FAIL = "ERR_CUSTOMER_SUBSCRIPTION_SEND_FAIL";
        String ERR_CONSIGNMENT_REROUTE_FAILED = "ERR_CONSIGNMENT_REROUTE_FAILED";
        String ERR_CLEANUP_CRONJOB_LONGSEARCH_VALUE_MISSING = "ERR_CLEANUP_CRONJOB_LONGSEARCH_VALUE_MISSING";
        String ERR_CLEANUP_CRONJOB_INTERPRETER_SETUP = "ERR_CLEANUP_CRONJOB_INTERPRETER_SETUP";
        String ERR_CLEANUP_CRONJOB_INTERPRETER_EVAL = "ERR_CLEANUP_CRONJOB_INTERPRETER_EVAL";
        String ERR_CLEANUP_CRONJOB_INVALID_INSTANCE = "ERR_CLEANUP_CRONJOB_INVALID_INSTANCE";
        //IPG Payment Gateway errorcode
        String ERR_IPGPAYMENT = "ERR_IPGPAYMENT";
        String ERR_EMAIL_BLACKOUT_TIME = "ERR_EMAIL_BLACKOUT_TIME";
        String ERR_SHIPSTER_SEND_ORDER_CONFIRMATION = "ERR_SHIPSTER_SEND_ORDER_CONFIRMATION";
        String ERR_FLUENT_CREATE_ORDER = "ERR_FLUENT_CREATE_ORDER";

    }

    public interface EndecaErrorCodes {

        String ENDECA_ASSEMBLER_EXCEPTION = "ENDECA-ASSEMBLER-EXCEPTION";
        String ENDECA_GENERIC_ASSEMBLER_EXCEPTION = "ENDECA-GENERIC-ASSEMBLE-EXCEPTION";
        String ENDECA_ENE_QUERY_EXCEPTION = "ENDECA_ENE_QUERY_EXCEPTION";
    }

}
