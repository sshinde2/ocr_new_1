/**
 * 
 */
package au.com.target.tgtutility.util;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;



/**
 * @author fkratoch
 * 
 */
@UnitTest
public class StringToolsTest {

    @Test
    public void trimLeadingZeros() {
        assertEquals("Can remove leading zeros", "123", StringTools.trimLeadingZeros("00123"));
        assertEquals("Can remove leading zeros", "123", StringTools.trimLeadingZeros("123"));
        assertEquals("Can remove leading zeros", "1230123", StringTools.trimLeadingZeros("01230123"));
        assertEquals("Can remove leading zeros", "1230", StringTools.trimLeadingZeros("1230"));
        assertEquals("Can remove leading zeros", "1230", StringTools.trimLeadingZeros("01230"));
        assertEquals("Can remove leading zeros", "12301230", StringTools.trimLeadingZeros("012301230"));
        assertEquals("Can remove leading zeros", "0", StringTools.trimLeadingZeros("0"));
        assertEquals("Can remove leading zeros", "0", StringTools.trimLeadingZeros("00"));
    }

    @Test
    public void testPadWithLeadingZeros() {

        assertEquals("00 1234 ", StringTools.padWithLeadingZeros(" 1234 "));
        assertEquals("0123 456", StringTools.padWithLeadingZeros("123 456"));
        assertEquals("00  123  45  ", StringTools.padWithLeadingZeros("  123  45  "));
        assertEquals(" 123 4  568  ", StringTools.padWithLeadingZeros(" 123 4  568  "));

        assertEquals("00001234", StringTools.padWithLeadingZeros("1234"));
        assertEquals("12345678", StringTools.padWithLeadingZeros("12345678"));
        assertEquals("0000123456789", StringTools.padWithLeadingZeros("123456789"));
        assertEquals("1234567890123", StringTools.padWithLeadingZeros("1234567890123"));
        assertEquals("123456789012345", StringTools.padWithLeadingZeros("123456789012345"));
        assertEquals("123456789123", StringTools.padWithLeadingZeros("123456789123"));
        assertEquals("0012345678912", StringTools.padWithLeadingZeros("12345678912"));
        assertEquals("1234567891234", StringTools.padWithLeadingZeros("1234567891234"));
        assertEquals("000000my", StringTools.padWithLeadingZeros("my"));
        assertEquals("my name ", StringTools.padWithLeadingZeros("my name "));
        assertEquals("000my name is", StringTools.padWithLeadingZeros("my name is"));
        assertEquals("my name is xx", StringTools.padWithLeadingZeros("my name is xx"));
        assertEquals("00my nam is x", StringTools.padWithLeadingZeros("my nam is x"));
        assertEquals("my name is x", StringTools.padWithLeadingZeros("my name is x"));
    }

    @Test
    public void trimAllWhiteSpaces() {
        assertEquals("benoit vanalder", StringTools.trimAllWhiteSpaces(" benoit vanalder   "));
        assertEquals("benoit vanalder", StringTools.trimAllWhiteSpaces(" benoit vanalder    \r   "));
        assertEquals("benoit vanalder  michel",
                StringTools.trimAllWhiteSpaces("     benoit vanalder  michel   \t\n\r\f    "));
        assertEquals(null, StringTools.trimAllWhiteSpaces(null));
        assertEquals(StringUtils.EMPTY, StringTools.trimAllWhiteSpaces(StringUtils.EMPTY));
    }

}
