/**
 * 
 */
package au.com.target.tgtutility.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;


/**
 * @author salexa10
 *
 */
@UnitTest
public class TargetMappingJacksonHttpMessageConverterTest {

    private final TargetMappingJacksonHttpMessageConverter converter = new TargetMappingJacksonHttpMessageConverter();


    @Test
    public void testSetMixinsWithNullForSerialization() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("java.lang.Object", "java.lang.String");
        converter.setMixins(map);
        converter.setMixins(null);
        assertNull(converter.getObjectMapper().getSerializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithWrongClassForSerialization() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("target", "mixin");
        converter.setMixins(map);
        assertNull(converter.getObjectMapper().getSerializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithGoodClassForSerialization() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("java.lang.Object", "java.lang.String");
        converter.setMixins(map);
        assertEquals(String.class,
                converter.getObjectMapper().getSerializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithNullForDeserialization() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("java.lang.Object", "java.lang.String");
        converter.setMixins(map);
        converter.setMixins(null);
        assertNull(converter.getObjectMapper().getDeserializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithWrongClassForDeserialization() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("target", "mixin");
        converter.setMixins(map);
        assertNull(converter.getObjectMapper().getDeserializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithGoodClassForDeserialization() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("java.lang.Object", "java.lang.String");
        converter.setMixins(map);
        assertEquals(String.class,
                converter.getObjectMapper().getDeserializationConfig().findMixInClassFor(Object.class));
    }
}
