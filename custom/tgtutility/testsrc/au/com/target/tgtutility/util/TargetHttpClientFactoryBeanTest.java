package au.com.target.tgtutility.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtutility.http.factory.CustomHeaderFactory;
import au.com.target.tgtutility.http.factory.HttpClientFactory;
import au.com.target.tgtutility.http.factory.UserAgentHeaderFactory;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetHttpClientFactoryBeanTest {

    @Mock
    private HttpClientFactory httpClientFactory;

    @Mock
    private HttpClientBuilder httpClientBuilder;

    @Spy
    @InjectMocks
    private final TargetHttpClientFactoryBean targetHttpClientFactoryBean = new TargetHttpClientFactoryBean();

    @Mock
    private UserAgentHeaderFactory userAgentHeaderFactory;

    @Mock
    private CustomHeaderFactory customHeaderFactory;

    @Test
    public void testAddUserAgentHeader() throws Exception {
        final BasicHeader header = new BasicHeader(HttpHeaders.USER_AGENT, "TEST");
        given(userAgentHeaderFactory.createHeader()).willReturn(header);
        final List<Header> defaultHeaders = new ArrayList<>();
        targetHttpClientFactoryBean.addUserAgentHeader(defaultHeaders);
        assertThat(defaultHeaders).containsExactly(header);
    }

    public void testAddCustomHeader() throws Exception {
        final BasicHeader header = new BasicHeader("Zip-Version", "2017-03-01");
        final List<BasicHeader> headers = new ArrayList<BasicHeader>();
        headers.add(header);
        given(customHeaderFactory.createHeader()).willReturn(headers);
        final List<Header> defaultHeaders = new ArrayList<>();
        targetHttpClientFactoryBean.addCustomHeader(defaultHeaders);
        assertThat(defaultHeaders).containsExactly(header);
    }

    @Test
    public void testCreateInstance() throws Exception {
        given(httpClientFactory.createHttpClientBuilderWithProxy()).willReturn(httpClientBuilder);

        final BasicHeader header = new BasicHeader(HttpHeaders.USER_AGENT, "TEST");
        given(userAgentHeaderFactory.createHeader()).willReturn(header);
        targetHttpClientFactoryBean.createInstance();
        final ArgumentCaptor<List> headersCaptor = ArgumentCaptor.forClass(List.class);
        verify(targetHttpClientFactoryBean).addUserAgentHeader(headersCaptor.capture());
        assertThat(headersCaptor.getValue()).containsExactly(header);
    }
}
