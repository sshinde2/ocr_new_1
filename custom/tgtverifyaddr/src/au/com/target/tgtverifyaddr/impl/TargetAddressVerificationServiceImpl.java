/**
 * 
 */
package au.com.target.tgtverifyaddr.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.qas.ondemand_2011_03.EngineEnumType;

import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;
import au.com.target.tgtverifyaddr.provider.AddressVerificationClient;


/**
 * @author vmuthura
 * 
 */
public class TargetAddressVerificationServiceImpl implements TargetAddressVerificationService {

    /**
     * 
     */
    private static final int VERIFIFIED_SCORE_MIN = 95;

    private static final Logger LOG = Logger.getLogger(TargetAddressVerificationServiceImpl.class);

    @Resource(name = "addressVerificationClient")
    private AddressVerificationClient addressVerificationClient;

    @Override
    public List<AddressData> verifyAddress(final String address, final CountryEnum country)
            throws NoMatchFoundException,
            RequestTimeOutException, TooManyMatchesFoundException, ServiceNotAvailableException {
        return search(address, country, null);
    }

    @Override
    public FormattedAddressData formatAddress(final AddressData unformattedAddress)
            throws ServiceNotAvailableException {
        if (unformattedAddress == null) {
            throw new IllegalArgumentException("Provide a non null address");
        }

        if (unformattedAddress.getMoniker() == null) {
            throw new IllegalArgumentException("Provide a valid address");
        }

        if (unformattedAddress.getMoniker().length() <= 0) {
            throw new IllegalArgumentException("Provide a valid moniker");
        }

        return addressVerificationClient.formatAddress(unformattedAddress);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.TargetAddressVerificationService#isAddressVerified(java.lang.String, au.com.target.tgtverifyaddr.data.CountryEnum)
     */
    @Override
    public boolean isAddressVerified(final String address) {
        boolean isVerified = false;
        try {
            final List<AddressData> suggestions = addressVerificationClient.search(address,
                    CountryEnum.AUSTRALIA.getCode(), EngineEnumType.INTUITIVE.value());
            if (suggestions != null) {
                if (suggestions.size() == 1) {
                    final AddressData result = suggestions.get(0);
                    isVerified = StringUtils.isNotEmpty(result.getMoniker())
                            && result.getScore() > VERIFIFIED_SCORE_MIN;
                }
                if (!isVerified) {
                    LOG.warn("Cannot verify address, match doesn't return single result, size=" + suggestions.size());
                }
            }
        }
        catch (final RequestTimeOutException | ServiceNotAvailableException ex) {
            LOG.warn("Cannot verify address due to qas service error:" + ex.getMessage());
        }
        catch (final TooManyMatchesFoundException e) {
            LOG.warn("Cannot verify address: too many matches" + address);
        }
        return isVerified;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.TargetAddressVerificationService#searchAddress(java.lang.String, au.com.target.tgtverifyaddr.data.CountryEnum)
     */
    @Override
    public List<AddressData> searchAddress(final String address, final CountryEnum country)
            throws ServiceNotAvailableException,
            RequestTimeOutException, NoMatchFoundException, TooManyMatchesFoundException {
        return search(address, country, EngineEnumType.INTUITIVE.value());
    }


    /**
     * @return the addressVerificationClient
     */
    public AddressVerificationClient getAddressVerificationClient() {
        return addressVerificationClient;
    }

    /**
     * @param addressVerificationClient
     *            the addressVerificationClient to set
     */
    public void setAddressVerificationClient(final AddressVerificationClient addressVerificationClient) {
        this.addressVerificationClient = addressVerificationClient;
    }

    /**
     * @param address
     * @param country
     * @return List<AddressData>
     * @throws ServiceNotAvailableException
     * @throws RequestTimeOutException
     * @throws TooManyMatchesFoundException
     * @throws NoMatchFoundException
     */
    private List<AddressData> search(final String address, final CountryEnum country, final String engineType)
            throws ServiceNotAvailableException, RequestTimeOutException, TooManyMatchesFoundException,
            NoMatchFoundException {
        if (StringUtils.isBlank(address) || country == null) {
            throw new IllegalArgumentException("Provide a address/country");
        }

        final List<AddressData> suggestions = addressVerificationClient.search(address, country.getCode(), engineType);

        if (CollectionUtils.isEmpty(suggestions)) {
            throw new NoMatchFoundException("Search did not return any result");
        }

        return suggestions;
    }
}
