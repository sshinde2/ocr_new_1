/**
 * 
 */
package au.com.target.tgtverifyaddr.data;

/**
 * @author vmuthura
 * 
 */
public class AddressData
{

    private String moniker;
    private String partialAddress;
    private String postCode;
    private int score;

    /**
     * @param moniker
     *            Unique moniker for this address
     * @param partialAddress
     *            Formatted address string
     * @param postCode
     *            postal code for the address
     */
    public AddressData(final String moniker, final String partialAddress, final String postCode)
    {
        this.moniker = moniker;
        this.partialAddress = partialAddress;
        this.postCode = postCode;
    }

    /**
     * @return the moniker Unique moniker for this address
     */
    public String getMoniker()
    {
        return moniker;
    }

    /**
     * @param moniker
     *            the moniker to set
     */
    public void setMoniker(final String moniker)
    {
        this.moniker = moniker;
    }

    /**
     * @return the partialAddress Formatted address string
     */
    public String getPartialAddress()
    {
        return partialAddress;
    }

    /**
     * @param partialAddress
     *            the partialAddress to set
     */
    public void setPartialAddress(final String partialAddress)
    {
        this.partialAddress = partialAddress;
    }

    /**
     * @return the postCode
     */
    public String getPostCode()
    {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode)
    {
        this.postCode = postCode;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score
     *            the score to set
     */
    public void setScore(final int score) {
        this.score = score;
    }



}
