/**
 * 
 */
package au.com.target.tgtverifyaddr.data;

/**
 * @author vmuthura
 * 
 */
public enum CountryEnum
{
    AUSTRALIA("AUE"), NEWZEALAND("NZL"), UK("GBR"), SINGAPORE("SGF");

    private String code;

    /**
     * @param code
     *            - Country code (in QAS format)
     */
    private CountryEnum(final String code)
    {
        this.code = code.intern();
    }

    /**
     * @return - the country code (in QAS format)
     */
    public String getCode()
    {
        return code;
    }

}
