/**
 * 
 */
package au.com.target.tgtverifyaddr.exception;

/**
 * @author vmuthura
 * 
 */
public class NoMatchFoundException extends AddressVerificationException
{
    /**
     * @param message
     *            Meaningful message to accompany the exception
     */
    public NoMatchFoundException(final String message)
    {
        super(message);
    }

    /**
     * @param exception
     *            Exception to be wrapped
     */
    public NoMatchFoundException(final Exception exception)
    {
        super(exception);
    }

    /**
     * @param message
     *            Meaningful message to accompany the exception
     * @param exception
     *            Exception to be wrapped
     */
    public NoMatchFoundException(final String message, final Exception exception)
    {
        super(message, exception);
    }

}
