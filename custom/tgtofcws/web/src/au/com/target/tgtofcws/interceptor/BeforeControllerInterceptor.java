/**
 * 
 */
package au.com.target.tgtofcws.interceptor;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.constants.CatalogConstants;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.UrlPathHelper;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author rsamuel3
 *
 */
public class BeforeControllerInterceptor extends HandlerInterceptorAdapter {
    public static final String EMPLOYEE_STORE = "EMPLOYEE_STORE";

    private static final Logger LOG = Logger.getLogger(BeforeControllerInterceptor.class);


    private UserService userService;

    private RedirectStrategy redirectStrategy;

    private UrlPathHelper urlPathHelper;

    private Set<String> excludeUrls;

    private CatalogVersionService catalogVersionService;

    private SessionService sessionService;

    private SearchRestrictionService searchRestrictionService;

    private String redirectUrl = "/login";

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
            throws Exception
    {
        final String path = getUrlPathHelper().getServletPath(request);
        if (!getExcludeUrls().contains(path)) {
            final UserModel user = userService.getCurrentUser();
            if (!isValidSession(request, user)) {
                redirectStrategy.sendRedirect(request, response, getRedirectUrl());
                return false;
            }
            boolean redirect = false;

            if (!(user instanceof StoreEmployeeModel))
            {
                LOG.warn("user not a store employee, redirecting");
                redirect = true;
            }
            else {
                final StoreEmployeeModel storeEmployee = (StoreEmployeeModel)user;
                final TargetPointOfServiceModel store = storeEmployee.getStore();
                if (store == null) {
                    LOG.warn("no store associated with the user, redirecting");
                    redirect = true;
                }
                else {
                    final Integer storeNumber = store.getStoreNumber();
                    request.setAttribute(EMPLOYEE_STORE, storeNumber);
                    final CatalogVersionModel catalogVersions = getCatalogVersion();
                    sessionService.setAttribute(CatalogConstants.SESSION_CATALOG_VERSIONS,
                            Collections.unmodifiableCollection(Collections.singleton(catalogVersions)));
                    sessionService.getCurrentSession().setAttribute(EMPLOYEE_STORE, storeNumber);

                }
            }
            if (redirect)
            {
                redirectStrategy.sendRedirect(request, response, getRedirectUrl());
                return false;

            }
        }

        return true;
    }

    /**
     * Get the catalog version
     * 
     * @return CatalogVersion
     */
    protected CatalogVersionModel getCatalogVersion() {
        final CatalogVersionModel catalogVersions = sessionService
                .executeInLocalView(new SessionExecutionBody()
                {

                    @Override
                    public Object execute()
                    {
                        try
                        {
                            searchRestrictionService.disableSearchRestrictions();
                            return catalogVersionService.getCatalogVersion(
                                    TgtCoreConstants.Catalog.PRODUCTS,
                                    TgtCoreConstants.Catalog.ONLINE_VERSION);
                        }
                        finally
                        {
                            searchRestrictionService.enableSearchRestrictions();
                        }
                    }

                });
        return catalogVersions;
    }

    /**
     * compares the suer in authentication with the current user in userService
     * 
     * @param request
     * @param user
     * @return true if it is a valid session and false otherwise
     */
    protected boolean isValidSession(final HttpServletRequest request, final UserModel user) {
        final Authentication authentication = getSecurityContext().getAuthentication();
        if (authentication != null && user != null)
        {
            final Object principal = authentication.getPrincipal();
            if (principal instanceof String)
            {
                final String springSecurityUserId = (String)principal;

                final String hybrisUserId = user.getUid();
                if (!springSecurityUserId.equals(hybrisUserId))
                {
                    LOG.error("User miss-match springSecurityUserId [" + springSecurityUserId
                            + "] hybris session user [" + hybrisUserId + "]. Invalidating session.");

                    // Invalidate session and redirect to the root page
                    request.getSession().invalidate();
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return SecurityContext
     */
    protected SecurityContext getSecurityContext() {
        return SecurityContextHolder.getContext();
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param redirectStrategy
     *            the redirectStrategy to set
     */
    @Required
    public void setRedirectStrategy(final RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }



    /**
     * @return the urlPathHelper
     */
    public UrlPathHelper getUrlPathHelper() {
        return urlPathHelper;
    }

    /**
     * @param urlPathHelper
     *            the urlPathHelper to set
     */
    @Required
    public void setUrlPathHelper(final UrlPathHelper urlPathHelper) {
        this.urlPathHelper = urlPathHelper;
    }

    /**
     * @return the excludeUrls
     */
    public Set<String> getExcludeUrls() {
        return excludeUrls;
    }

    /**
     * @param excludeUrls
     *            the excludeUrls to set
     */
    @Required
    public void setExcludeUrls(final Set<String> excludeUrls) {
        this.excludeUrls = excludeUrls;
    }

    /**
     * @return the redirectUrl
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * @param redirectUrl
     *            the redirectUrl to set
     */
    public void setRedirectUrl(final String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }



    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param searchRestrictionService
     *            the searchRestrictionService to set
     */
    @Required
    public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }
}
