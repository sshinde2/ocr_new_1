/**
 * 
 */
package au.com.target.tgtofcws.controllers.consignments;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtofcws.controllers.BaseController;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcels;


/**
 * @author rsamuel3
 *
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/store")
public class ConsignmentsController extends BaseController {

    private static final int DEFAULT_VALUE_PAGINATION = 0;
    private static final int DEFAULT_VALUE_LASTXDAYS = 60;
    private static final String DISPATCH_LABEL_PDF = "dispatchLabel.pdf";

    @Resource
    private TargetInStoreIntegrationFacade targetInStoreIntegrationFacade;

    /**
     * Get consignments for Store number
     *
     * @param request
     *            request
     * @return Response with the list of consignments for Store Number
     */
    @RequestMapping(value = "consignments", method = RequestMethod.GET)
    @ResponseBody
    public Response getConsignmentsForStore(@RequestParam(value = "perpage", required = false) final String perPage,
            @RequestParam(value = "offset", required = false) final String offset,
            @RequestParam(value = "lastXDays", required = false) final Integer lastXDays,
            @RequestParam(value = "searchText", required = false) final String searchText,
            @RequestParam(value = "status", required = false) final String status,
            @RequestParam(value = "sortKey", required = false) final String sortKey,
            @RequestParam(value = "sortDirection", required = false) final String sortDirection,
            final HttpServletRequest request) {

        return targetInStoreIntegrationFacade.getConsignmentsForStore(getStoreNumber(request),
                getValueForPagination(offset),
                getValueForPagination(perPage), getLastXDays(lastXDays), searchText, status, sortKey, sortDirection);
    }

    private int getLastXDays(final Integer lastXDays) {
        if (lastXDays == null || lastXDays.intValue() == 0) {
            return DEFAULT_VALUE_LASTXDAYS;
        }
        return lastXDays.intValue();
    }

    /**
     * Get detail of the consignments based on the code
     * 
     * @param consignmentIds
     * @return Response with the list of consignments and their details
     */
    @RequestMapping(value = "consignments/{consignmentIds}", method = RequestMethod.GET)
    @ResponseBody
    public Response getConsignmentDetailsForStore(@PathVariable final List<String> consignmentIds,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getConsignmentsByCodes(consignmentIds, getStoreNumber(request));
    }

    /**
     * Return the mapped status for the given consignment
     * 
     * @param consignmentCode
     * @return status
     */
    @RequestMapping(value = "consignments/{consignmentCode}/status", method = RequestMethod.GET)
    @ResponseBody
    public Response getConsignmentStatus(@PathVariable final String consignmentCode, final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getConsignmentStatusForInstore(consignmentCode, getStoreNumber(request));
    }

    /**
     * Method to set consignment to complete with parcel details.
     *
     * @param request
     *            HttpServletRequest
     * @param consignmentCode
     *            ConsignmentCode
     * @param parcels
     *            dimensions of the parcels
     * @return Response
     */
    @RequestMapping(value = "consignments/{consignmentCode}/complete", method = RequestMethod.POST)
    @ResponseBody
    public Response updateConsignmentCompleteInStore(@PathVariable final String consignmentCode,
            @RequestBody final Parcels parcels,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.completeConsignmentInstore(consignmentCode,
                getStoreNumber(request), null, parcels);
    }

    @RequestMapping(value = "consignments/{consignmentCode}/stock", method = RequestMethod.GET)
    @ResponseBody
    public Response getStockLevelsForProduct(@PathVariable final String consignmentCode,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getStockLevelsForProductsInConsignment(
                consignmentCode, getStoreNumber(request));
    }


    /**
     * Method to set consignment to complete with parcel count.
     *
     * @param request
     *            HttpServletRequest
     * @param consignmentCode
     *            ConsignmentCode
     * @param parcelCount
     *            parcel count
     * @return Response
     */
    @RequestMapping(value = "consignments/{consignmentCode}/complete", method = RequestMethod.GET)
    @ResponseBody
    public Response updateConsignmentCompleteInStore(@PathVariable final String consignmentCode,
            @RequestParam(value = "parcelCount") final Integer parcelCount,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.completeConsignmentInstore(consignmentCode,
                getStoreNumber(request), parcelCount, null);
    }

    /**
     * Method to set consignment to picked for packing in store.
     *
     * @param consignmentCode
     * @return Response
     */
    @RequestMapping(value = "consignments/{consignmentCode}/packing", method = RequestMethod.POST)
    @ResponseBody
    public Response updateConsignmentPickedInStore(@PathVariable final String consignmentCode,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.pickForPackConsignmentInstore(consignmentCode, getStoreNumber(request));
    }

    /**
     * Reject the consignment.
     * 
     * @param consignmentCode
     * @param instoreRejectReasonCode
     * @param rejectState
     * @param request
     * @return Response
     */
    @RequestMapping(value = "consignments/{consignmentCode}/reject", method = RequestMethod.POST)
    @ResponseBody
    public Response updateConsignmentRejectedInStore(@PathVariable final String consignmentCode,
            @RequestParam(value = "instoreRejectReasonCode") final String instoreRejectReasonCode,
            @RequestParam(value = "rejectState") final ConsignmentRejectState rejectState,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.rejectConsignmentForInstore(consignmentCode, getStoreNumber(request),
                instoreRejectReasonCode, rejectState);
    }

    /**
     * Update the shipped quantity
     * 
     * @param consignmentCode
     * @param consignment
     * @return Response
     */
    @RequestMapping(value = "consignments/{consignmentCode}/updateShippedQty", method = RequestMethod.POST)
    @ResponseBody
    public Response updateShippedQuantityInConsignmentEntries(@PathVariable final String consignmentCode,
            @RequestBody final Consignment consignment,
            final HttpServletRequest request) {
        consignment.setCode(consignmentCode);
        return targetInStoreIntegrationFacade.updateShippedQty(consignment, getStoreNumber(request));
    }

    /**
     * Return the mapped status for the change of consignment status to waved.
     * 
     * @param consignmentCode
     * @return status
     */
    @RequestMapping(value = "consignments/{consignmentCode}/picking", method = RequestMethod.POST)
    @ResponseBody
    public Response setConsignmentsStatusToWaved(@PathVariable final String consignmentCode,
            final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.waveConsignmentForInstore(consignmentCode, getStoreNumber(request));
    }

    @RequestMapping(value = "consignments/{consignmentCode}/" + DISPATCH_LABEL_PDF, method = RequestMethod.GET)
    public ResponseEntity consignmentDispatchLabel(@PathVariable final String consignmentCode,
            @RequestParam(value = "layout", required = false) final String layout,
            @RequestParam(value = "branding", required = false) final Boolean branding,
            final HttpServletRequest request) {

        final byte[] data = targetInStoreIntegrationFacade.getConsignmentDispatchLabel(consignmentCode,
                getStoreNumber(request), layout, branding);

        if (data != null) {
            return createPDFResponse(data, DISPATCH_LABEL_PDF);
        }

        return create404Response();

    }

    private ResponseEntity<byte[]> createPDFResponse(final byte[] data, final String filename) {

        Assert.notNull(data, "data should not be null");

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Content-Disposition", "inline;filename=" + filename);
        headers.setCacheControl("must-revalidate");
        headers.setContentLength(data.length);
        headers.setExpires(0);
        final ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        return response;
    }

    private ResponseEntity<String> create404Response() {

        return new ResponseEntity<String>("Not Found", HttpStatus.NOT_FOUND);
    }

    private int getValueForPagination(final String value) {

        if (value == null) {
            return DEFAULT_VALUE_PAGINATION;
        }

        try {
            return Integer.parseInt(value);
        }
        catch (final NumberFormatException e) {
            return DEFAULT_VALUE_PAGINATION;
        }
    }
}
