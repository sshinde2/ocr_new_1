/**
 * 
 */
package au.com.target.tgtofcws.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.MockSession;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.util.UrlPathHelper;

import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BeforeControllerInterceptorTest {

    @Mock
    private SecurityContext securityContextMock;

    @Mock
    private UserService userService;

    @Mock
    private RedirectStrategy redirectStrategy;

    @Mock
    private UrlPathHelper urlPathHelper;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @Mock
    private Object handler;

    @Mock
    private Authentication authentication;

    @Mock
    private UserModel userModel;

    @Mock
    private StoreEmployeeModel storeEmployee;

    @Mock
    private TargetPointOfServiceModel pointOfService;

    @Mock
    private SessionService sessionService;

    private final Set<String> excludeUrls = new HashSet<>();

    @InjectMocks
    private final TestBeforeControllerInterceptor beforeControllerInterceptor = new TestBeforeControllerInterceptor();

    @Before
    public void setUp() {
        excludeUrls.add("/login");
        excludeUrls.add("/login/failed");
        excludeUrls.add("/logout");
        excludeUrls.add("/logout/success");
        beforeControllerInterceptor.setExcludeUrls(excludeUrls);
    }

    @Test
    public void testLoginUrl() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/login");
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isTrue();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verifyZeroInteractions(securityContextMock);
        Mockito.verifyZeroInteractions(userService);
        Mockito.verifyZeroInteractions(redirectStrategy);
        Mockito.verifyZeroInteractions(authentication);
        Mockito.verifyZeroInteractions(request);
        Mockito.verifyZeroInteractions(session);
    }



    @Test
    public void testAuthenticationIsNull() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/authnull");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(null);
        BDDMockito.given(userService.getCurrentUser()).willReturn(userModel);
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isFalse();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verify(redirectStrategy).sendRedirect(request, response, "/login");
        Mockito.verifyNoMoreInteractions(redirectStrategy);
        Mockito.verifyZeroInteractions(authentication);
        Mockito.verifyZeroInteractions(request);
        Mockito.verifyZeroInteractions(session);
    }

    @Test
    public void testUserIsNull() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/authnull");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(null);
        BDDMockito.given(userService.getCurrentUser()).willReturn(null);
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isFalse();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verify(redirectStrategy).sendRedirect(request, response, "/login");
        Mockito.verifyNoMoreInteractions(redirectStrategy);
        Mockito.verifyZeroInteractions(authentication);
        Mockito.verifyZeroInteractions(request);
        Mockito.verifyZeroInteractions(session);
    }

    @Test
    public void testAuthenticationPrincipalIsInteger() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/auth");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(authentication);
        BDDMockito.given(authentication.getPrincipal()).willReturn(Integer.valueOf(1));
        BDDMockito.given(userService.getCurrentUser()).willReturn(userModel);
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isFalse();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verify(redirectStrategy).sendRedirect(request, response, "/login");
        Mockito.verifyNoMoreInteractions(redirectStrategy);
        Mockito.verify(authentication).getPrincipal();
        Mockito.verifyNoMoreInteractions(authentication);
        Mockito.verifyZeroInteractions(request);
        Mockito.verifyZeroInteractions(session);
    }

    @Test
    public void testAuthenticationUserIdsdontMatch() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/auth");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(authentication);
        BDDMockito.given(authentication.getPrincipal()).willReturn("user1");
        BDDMockito.given(userService.getCurrentUser()).willReturn(userModel);
        BDDMockito.given(request.getSession()).willReturn(session);
        BDDMockito.given(userModel.getUid()).willReturn("wronguser");
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isFalse();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verify(redirectStrategy).sendRedirect(request, response, "/login");
        Mockito.verifyNoMoreInteractions(redirectStrategy);
        Mockito.verify(authentication).getPrincipal();
        Mockito.verifyNoMoreInteractions(authentication);
        Mockito.verify(request).getSession();
        Mockito.verifyNoMoreInteractions(request);
        Mockito.verify(session).invalidate();
        Mockito.verifyNoMoreInteractions(session);
    }

    @Test
    public void testUserNotOfStoreEmployeeType() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/auth");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(authentication);
        BDDMockito.given(authentication.getPrincipal()).willReturn("user1");
        BDDMockito.given(userService.getCurrentUser()).willReturn(userModel);
        BDDMockito.given(userModel.getUid()).willReturn("user1");
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isFalse();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verify(redirectStrategy).sendRedirect(request, response, "/login");
        Mockito.verifyNoMoreInteractions(redirectStrategy);
        Mockito.verify(authentication).getPrincipal();
        Mockito.verifyNoMoreInteractions(authentication);
        Mockito.verifyZeroInteractions(request);
        Mockito.verifyZeroInteractions(session);
    }

    @Test
    public void testStoreAssignedToStoreEmployeeNull() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/auth");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(authentication);
        BDDMockito.given(authentication.getPrincipal()).willReturn("user1");
        BDDMockito.given(userService.getCurrentUser()).willReturn(storeEmployee);
        BDDMockito.given(storeEmployee.getUid()).willReturn("user1");
        BDDMockito.given(storeEmployee.getStore()).willReturn(null);
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isFalse();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verify(redirectStrategy).sendRedirect(request, response, "/login");
        Mockito.verifyNoMoreInteractions(redirectStrategy);
        Mockito.verify(authentication).getPrincipal();
        Mockito.verifyNoMoreInteractions(authentication);
        Mockito.verifyZeroInteractions(request);
        Mockito.verifyZeroInteractions(session);
        Mockito.verify(storeEmployee).getStore();
        Mockito.verify(storeEmployee).getUid();
        Mockito.verifyNoMoreInteractions(storeEmployee);
    }

    @Test
    public void testSuccess() throws Exception {
        BDDMockito.given(urlPathHelper.getServletPath(request)).willReturn("/test/auth");
        BDDMockito.given(securityContextMock.getAuthentication()).willReturn(authentication);
        BDDMockito.given(authentication.getPrincipal()).willReturn("user1");
        BDDMockito.given(userService.getCurrentUser()).willReturn(storeEmployee);
        BDDMockito.given(storeEmployee.getUid()).willReturn("user1");
        BDDMockito.given(storeEmployee.getStore()).willReturn(pointOfService);
        final Session localSession = new MockSession();
        BDDMockito.given(sessionService.getCurrentSession()).willReturn(localSession);
        final Integer storeNumber = Integer.valueOf(5234);
        BDDMockito.given(pointOfService.getStoreNumber()).willReturn(storeNumber);
        final boolean success = beforeControllerInterceptor.preHandle(request, response, handler);
        Assertions.assertThat(success).isTrue();
        Mockito.verify(urlPathHelper).getServletPath(request);
        Mockito.verifyNoMoreInteractions(urlPathHelper);
        Mockito.verify(securityContextMock).getAuthentication();
        Mockito.verifyNoMoreInteractions(securityContextMock);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verifyZeroInteractions(redirectStrategy);
        Mockito.verify(authentication).getPrincipal();
        Mockito.verifyNoMoreInteractions(authentication);
        Mockito.verifyZeroInteractions(session);
        Mockito.verify(storeEmployee).getStore();
        Mockito.verify(storeEmployee).getUid();
        Mockito.verifyNoMoreInteractions(storeEmployee);
        Mockito.verify(pointOfService).getStoreNumber();
        Mockito.verifyNoMoreInteractions(pointOfService);
        Mockito.verify(request).setAttribute(BeforeControllerInterceptor.EMPLOYEE_STORE, storeNumber);
        Mockito.verifyNoMoreInteractions(request);
        Mockito.verify(sessionService).getCurrentSession();
    }

    private class TestBeforeControllerInterceptor extends BeforeControllerInterceptor {
        @Override
        protected SecurityContext getSecurityContext() {
            return securityContextMock;
        }
    }
}
