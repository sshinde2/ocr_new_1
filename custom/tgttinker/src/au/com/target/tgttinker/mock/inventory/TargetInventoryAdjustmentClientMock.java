package au.com.target.tgttinker.mock.inventory;

import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwebmethods.stock.client.impl.TargetInventoryAdjustmentClientImpl;


/**
 * @author pthoma20
 *
 */
public class TargetInventoryAdjustmentClientMock extends TargetInventoryAdjustmentClientImpl implements MockedByTarget {

    private static final Logger LOG = Logger.getLogger(TargetInventoryAdjustmentClientMock.class);

    private boolean isActive;

    private boolean success;

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return isActive;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.isActive = active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwishlist.sendWishLists.SendWishListClient#sendWishLists(au.com.target.tgtwishlist.data.TargetWishListSendRequestDto)
     */
    @Override
    public boolean sendInventoryAdjustment(final List<InventoryAdjustmentData> inventoryAdjustmentDataList)
            throws TargetIntegrationException {
        if (!isActive) {
            return super.sendInventoryAdjustment(inventoryAdjustmentDataList);
        }

        if (success) {
            LOG.info("Mock Active - Replying sucess");
            return true;
        }

        LOG.info("Mock Active - Replying Failure");
        return false;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }
}
