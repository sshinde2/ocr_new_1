/**
 * 
 */
package au.com.target.tgttinker.mock.payment;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentResponseDetailsType;
import urn.ebay.apis.eBLBaseComponents.GetExpressCheckoutDetailsResponseDetailsType;
import urn.ebay.apis.eBLBaseComponents.PayerInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentStatusCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentTransactionType;
import urn.ebay.apis.eBLBaseComponents.RefundInfoType;
import au.com.target.tgtpaymentprovider.paypal.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.exception.ValidationFailedException;
import au.com.target.tgtpaymentprovider.paypal.impl.PayPalServiceImpl;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author Pradeep
 *
 */
public class PayPalServiceMock extends PayPalServiceImpl implements MockedByTarget {

    private static final String CORRELATIONID = "e561115d1a1b7";
    private static final String TOKEN = "EC-8AE77999L1493492S";

    private boolean active = false;
    private boolean failedToCapture = false;
    private boolean downWhileCapture = false;
    private boolean downWhileRetrieve = false;
    private String orderAmount;

    @Override
    public SetExpressCheckoutResponseType setExpressCheckout(final SetExpressCheckoutReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException {
        if (active) {
            orderAmount = StringUtils.remove(request.getSetExpressCheckoutRequest()
                    .getSetExpressCheckoutRequestDetails()
                    .getMaxAmount()
                    .getValue(), '$');
            final SetExpressCheckoutResponseType response = new SetExpressCheckoutResponseType();
            response.setToken(TOKEN);
            response.setAck(AckCodeType.SUCCESS);
            response.setCorrelationID(CORRELATIONID);
            return response;
        }
        return super.setExpressCheckout(request);
    }

    @Override
    public GetExpressCheckoutDetailsResponseType getCheckoutDetails(
            final GetExpressCheckoutDetailsReq request)
            throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {
        if (active) {
            final GetExpressCheckoutDetailsResponseType response = new GetExpressCheckoutDetailsResponseType();
            response.setAck(AckCodeType.SUCCESS);
            final GetExpressCheckoutDetailsResponseDetailsType getExpressCheckoutDetailsResponseDetails = new GetExpressCheckoutDetailsResponseDetailsType();
            final AddressType billingAddress = new AddressType();
            billingAddress.setName("Pradeep tester");
            billingAddress.setStreet1("12-14");
            billingAddress.setStreet2("Thompson Road");
            billingAddress.setStateOrProvince("VIC");
            billingAddress.setCityName("North Geelong");
            billingAddress.setCountry(CountryCodeType.AU);
            billingAddress.setPostalCode("3215");
            getExpressCheckoutDetailsResponseDetails.setBillingAddress(billingAddress);
            final PayerInfoType payerInfo = new PayerInfoType();
            payerInfo.setPayer("tester@gmail.com");
            getExpressCheckoutDetailsResponseDetails.setPayerInfo(payerInfo);
            response.setGetExpressCheckoutDetailsResponseDetails(getExpressCheckoutDetailsResponseDetails);
            return response;
        }
        return super.getCheckoutDetails(request);
    }

    @Override
    public DoExpressCheckoutPaymentResponseType doExpressCheckout(final DoExpressCheckoutPaymentReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException {
        if (active) {
            final DoExpressCheckoutPaymentResponseType response = new DoExpressCheckoutPaymentResponseType();
            final DoExpressCheckoutPaymentResponseDetailsType doExpressCheckoutPaymentResponseDetails = new DoExpressCheckoutPaymentResponseDetailsType();
            doExpressCheckoutPaymentResponseDetails.setRedirectRequired("false");
            final List<PaymentInfoType> paymentInfo = new ArrayList<>();
            final PaymentInfoType paymentInfoType = new PaymentInfoType();
            paymentInfoType.setTransactionID("7H20847567170290T");
            paymentInfoType.setPaymentStatus(PaymentStatusCodeType.COMPLETED);
            final BasicAmountType grossAmount = new BasicAmountType();
            grossAmount.setValue(request.getDoExpressCheckoutPaymentRequest()
                    .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails().get(0).getOrderTotal().getValue());
            paymentInfoType.setGrossAmount(grossAmount);
            paymentInfo.add(paymentInfoType);
            doExpressCheckoutPaymentResponseDetails.setPaymentInfo(paymentInfo);
            doExpressCheckoutPaymentResponseDetails.setToken("EC-2W024615KF128553Y");
            response.setDoExpressCheckoutPaymentResponseDetails(doExpressCheckoutPaymentResponseDetails);
            response.setCorrelationID(CORRELATIONID);
            orderAmount = grossAmount.getValue();
            if (downWhileCapture) {
                throw new PayPalServiceException("paypal is down");
            }
            if (failedToCapture) {
                response.setAck(AckCodeType.FAILURE);
            }
            else {
                response.setAck(AckCodeType.SUCCESS);
            }
            return response;
        }
        return super.doExpressCheckout(request);
    }


    @Override
    public RefundTransactionResponseType refund(final RefundTransactionReq request)
            throws InvalidRequestException,
            ValidationFailedException, PayPalServiceException {
        if (active) {
            final RefundTransactionResponseType response = new RefundTransactionResponseType();
            response.setAck(AckCodeType.SUCCESS);
            response.setCorrelationID(CORRELATIONID);
            final BasicAmountType basicAmount = request.getRefundTransactionRequest().getAmount();
            orderAmount = basicAmount.getValue();
            response.setGrossRefundAmount(basicAmount);
            final RefundInfoType refundInfo = new RefundInfoType();
            refundInfo.setRefundStatus(PaymentStatusCodeType.COMPLETED);
            response.setRefundInfo(refundInfo);
            response.setMsgSubID(TOKEN);
            response.setRefundTransactionID(TOKEN);
            return response;
        }
        return super.refund(request);
    }

    @Override
    public GetTransactionDetailsResponseType retrieve(final GetTransactionDetailsReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException {
        if (active) {
            final GetTransactionDetailsResponseType response = new GetTransactionDetailsResponseType();
            response.setAck(AckCodeType.SUCCESS);
            response.setCorrelationID(CORRELATIONID);
            final PaymentTransactionType paymentDetails = new PaymentTransactionType();
            final PaymentInfoType paymentInfoType = new PaymentInfoType();
            paymentInfoType.setTransactionID(request.getGetTransactionDetailsRequest().getTransactionID());
            paymentInfoType.setPaymentStatus(PaymentStatusCodeType.COMPLETED);
            final BasicAmountType grossAmount = new BasicAmountType();
            grossAmount.setValue(orderAmount);
            paymentInfoType.setGrossAmount(grossAmount);
            paymentDetails.setPaymentInfo(paymentInfoType);
            final PayerInfoType payerInfo = new PayerInfoType();
            payerInfo.setPayerID("VBK7PWEXG4C9W");
            paymentDetails.setPayerInfo(payerInfo);
            response.setPaymentTransactionDetails(paymentDetails);
            if (downWhileRetrieve) {
                throw new PayPalServiceException("paypal is down");
            }
            return response;
        }
        return super.retrieve(request);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @return the failedToCapture
     */
    public boolean isFailedToCapture() {
        return failedToCapture;
    }

    /**
     * @return the downWhileCapture
     */
    public boolean isDownWhileCapture() {
        return downWhileCapture;
    }

    /**
     * @param downWhileCapture
     *            the downWhileCapture to set
     */
    public void setDownWhileCapture(final boolean downWhileCapture) {
        this.downWhileCapture = downWhileCapture;
    }

    /**
     * @param failedToCapture
     *            the failedToCapture to set
     */
    public void setFailedToCapture(final boolean failedToCapture) {
        this.failedToCapture = failedToCapture;
    }

    /**
     * @return the downWhileRetrieve
     */
    public boolean isDownWhileRetrieve() {
        return downWhileRetrieve;
    }

    /**
     * @param downWhileRetrieve
     *            the downWhileRetrieve to set
     */
    public void setDownWhileRetrieve(final boolean downWhileRetrieve) {
        this.downWhileRetrieve = downWhileRetrieve;
    }

    public void reset() {
        active = false;
        failedToCapture = false;
        downWhileCapture = false;
    }

}
