package au.com.target.tgttinker.mock.flybuys;

import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.flybuys.client.impl.TargetFlybuysClientImpl;


/**
 * @author cwijesu1
 * 
 */
public class FlybuysClientMock extends TargetFlybuysClientImpl implements MockedByTarget {

    // Mock authentication related responses
    public static final String MOCK_APPROVED = FlybuysResponseType.SUCCESS.toString();
    public static final String MOCK_DECLINED = FlybuysResponseType.INVALID.toString();
    public static final String MOCK_OFFLINE = FlybuysResponseType.UNAVAILABLE.toString();
    public static final String MOCK_FLYBUYS_ERROR = FlybuysResponseType.FLYBUYS_OTHER_ERROR.toString();
    public static final String DEFAULT_RESPONDS_CODE = MOCK_APPROVED;
    public static final List<String> VALID_MOCK_RESPONSE = Collections
            .unmodifiableList(Arrays.asList(MOCK_APPROVED, MOCK_DECLINED, MOCK_OFFLINE, MOCK_FLYBUYS_ERROR));

    // Mock redeem related response
    public static final String MOCK_REDEEM_APPROVED = FlybuysResponseType.SUCCESS.toString();
    public static final String MOCK_REDEEM_DECLINE = FlybuysResponseType.INVALID.toString();
    public static final String MOCK_REDEEM_OFFLINE = FlybuysResponseType.UNAVAILABLE.toString();
    public static final String MOCK_REDEEM_ERROR = FlybuysResponseType.FLYBUYS_OTHER_ERROR.toString();
    public static final List<String> VALID_MOCK_REDEEM_RESPONSE = Collections
            .unmodifiableList(Arrays.asList(MOCK_REDEEM_APPROVED, MOCK_REDEEM_DECLINE, MOCK_REDEEM_OFFLINE));

    public static final String DEFAULT_REDEEM_RESPONDE_CODE = MOCK_REDEEM_APPROVED;
    public static final String DEFAULT_AVAILABLE_POINTS = "10000";


    private static final Logger LOG = Logger.getLogger(FlybuysClientMock.class);

    private static final int MAX_REDEEM_POINT = 10000;
    private static final float POINTS_PER_DOLLAR = 200.0F;
    private static final int CONSUME_REMAINING_POINTS = 10000; //For consume response
    private static final int CONSUME_RESPONSE_CODE = 1;//For consume response

    private static final String SECURITY_TOKEN = "DUMMYSECURITYTOKEN";
    private static final String MOCK_REDEEM_CONFIRMATION_CODE = "DUMMYCONFIRMATIONCODE";
    private static final String MOCK_REDEEM_CODE = "DUMMYREDEEMCODE";

    private boolean active = Config.getBoolean("mock.flybuys.default.active", false);

    private String mockResponseCode = DEFAULT_RESPONDS_CODE;
    private String mockAvailablePoints = DEFAULT_AVAILABLE_POINTS;

    private String mockRedeemRespondeCode = DEFAULT_REDEEM_RESPONDE_CODE;
    private String mockRedeemConfirmationCode = MOCK_REDEEM_CONFIRMATION_CODE;

    private String mockRefundRespondeCode = DEFAULT_REDEEM_RESPONDE_CODE;

    /**
     * @return the mockRefundRespondeCode
     */
    public String getMockRefundRespondeCode() {
        return mockRefundRespondeCode;
    }

    /**
     * @param mockRefundRespondeCode
     *            the mockRefundRespondeCode to set
     */
    public void setMockRefundRespondeCode(final String mockRefundRespondeCode) {
        this.mockRefundRespondeCode = mockRefundRespondeCode;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;

    }

    /**
     * @return the mockRedeemRespondeCode
     */
    public String getMockRedeemRespondeCode() {
        return mockRedeemRespondeCode;
    }

    /**
     * @param mockRedeemRespondeCode
     *            the mockRedeemRespondeCode to set
     */
    public void setMockRedeemRespondeCode(final String mockRedeemRespondeCode) {
        this.mockRedeemRespondeCode = mockRedeemRespondeCode;
    }

    /**
     * @return the mockRedeemConfirmationCode
     */
    public String getMockRedeemConfirmationCode() {
        return mockRedeemConfirmationCode;
    }

    /**
     * @param mockRedeemConfirmationCode
     *            the mockRedeemConfirmationCode to set
     */
    public void setMockRedeemConfirmationCode(final String mockRedeemConfirmationCode) {
        this.mockRedeemConfirmationCode = mockRedeemConfirmationCode;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#flyBuysAuthenticate(java.lang.String, java.util.Date, java.lang.String)
     */
    @Override
    public FlybuysAuthenticateResponseDto flybuysAuthenticate(final String flybuysNumber, final Date dob,
            final String postcode) {
        if (!active) {
            return super.flybuysAuthenticate(flybuysNumber, dob, postcode);
        }
        else {
            final FlybuysAuthenticateResponseDto mockResponse = new FlybuysAuthenticateResponseDto();
            if (MOCK_APPROVED.equals(this.mockResponseCode)) {
                mockResponse.setAvailPoints(mockAvailablePoints);
                final List<FlybuysRedeemTierDto> mockTiersAvailabel = getTiers(mockAvailablePoints);
                mockResponse.setRedeemTiers(mockTiersAvailabel);
                mockResponse.setSecurityToken(SECURITY_TOKEN);
                mockResponse.setResponse(FlybuysResponseType.SUCCESS);
            }
            else if (MOCK_DECLINED.equals(this.mockResponseCode)) {
                mockResponse.setAvailPoints("0");
                mockResponse.setErrorMessage("Declined from the Flybuys");
                mockResponse.setResponse(FlybuysResponseType.INVALID);
            }
            else if (MOCK_OFFLINE.equals(this.mockResponseCode)) {
                mockResponse.setAvailPoints("0");
                mockResponse.setErrorMessage("Flybuys unavailable");
                mockResponse.setResponse(FlybuysResponseType.UNAVAILABLE);

            }
            else if (MOCK_FLYBUYS_ERROR.equals(this.mockResponseCode)) {
                mockResponse.setAvailPoints("0");
                mockResponse.setErrorMessage("Flybuys Error Contact Flybuys");
                mockResponse.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
            }
            else {
                mockResponse.setAvailPoints("0");
                mockResponse.setErrorMessage("Invalid Details");
                mockResponse.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
            }
            return mockResponse;
        }


    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtwebmethods.flybuys.client.impl.TargetFlybuysClientImpl#consumeFlybuysPoints(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.math.BigDecimal, java.lang.String, java.lang.String)
     */
    @Override
    public FlybuysConsumeResponseDto consumeFlybuysPoints(final String flybuysCardNumber, final String securityToken,
            final String storeNumber, final Integer points, final BigDecimal dollars, final String redeemCode,
            final String transactionId) {
        if (!active) {
            return super.consumeFlybuysPoints(flybuysCardNumber, securityToken, storeNumber, points, dollars,
                    redeemCode,
                    transactionId);
        }

        LOG.info(MessageFormat
                .format("consumeFlybuysPoints :: flybuysCardNumber = {0}, securityToken = {1}, storeNumber = {2}, points = {3}, dollars = {4}, redeemCode = {5}, transactionId = {6}",
                        flybuysCardNumber, securityToken, storeNumber, points.toString(), dollars.toString(),
                        redeemCode, transactionId));

        final FlybuysConsumeResponseDto mockResponse = new FlybuysConsumeResponseDto();
        if (MOCK_REDEEM_APPROVED.equals(this.mockRedeemRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.SUCCESS);
            mockResponse.setConfimationCode(MOCK_REDEEM_CONFIRMATION_CODE);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));// This is a static value as this is not being used anywhere in the code no need to calculate
            mockResponse.setAvailPoints(Integer.valueOf(CONSUME_REMAINING_POINTS)); // This is a static value as this is not being used anywhere in the code no need to calculate
        }
        else if (MOCK_REDEEM_DECLINE.equals(this.mockRedeemRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.INVALID);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        else if (MOCK_REDEEM_OFFLINE.equals(this.mockRedeemRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        else if (MOCK_REDEEM_ERROR.equals(this.mockRedeemRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        else {
            mockResponse.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        return mockResponse;
    }

    @Override
    public FlybuysRefundResponseDto refundFlybuysPoints(final String flybuysCardNumber, final String securityToken,
            final String storeNumber,
            final Integer points,
            final BigDecimal dollars, final String timeStamp, final String transactionId) {
        if (!active) {
            return super.refundFlybuysPoints(flybuysCardNumber, securityToken, storeNumber, points, dollars, timeStamp,
                    transactionId);
        }
        LOG.info(MessageFormat
                .format("refundFlybuysPoints :: flybuysCardNumber = {0}, securityToken = {1}, storeNumber = {2}, points = {3}, dollars = {4}, timeStamp = {5}, transactionId = {6}",
                        flybuysCardNumber, securityToken, storeNumber, points.toString(), dollars.toString(),
                        timeStamp, transactionId));
        final FlybuysRefundResponseDto mockResponse = new FlybuysRefundResponseDto();

        if (MOCK_REDEEM_APPROVED.equals(this.mockRefundRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.SUCCESS);
            mockResponse.setConfimationCode(MOCK_REDEEM_CONFIRMATION_CODE);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));// This is a static value as this is not being used anywhere in the code no need to calculate
            mockResponse.setResponseMessage("success"); // 
        }
        else if (MOCK_REDEEM_DECLINE.equals(this.mockRefundRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.INVALID);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        else if (MOCK_REDEEM_OFFLINE.equals(this.mockRefundRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        else if (MOCK_REDEEM_ERROR.equals(this.mockRefundRespondeCode)) {
            mockResponse.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        else {
            mockResponse.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
            mockResponse.setResponseCode(Integer.valueOf(CONSUME_RESPONSE_CODE));
        }
        return mockResponse;
    }

    /**
     * @param avlbflyBuyPoints
     * @return list of flybuys redeem tiers
     */
    private List<FlybuysRedeemTierDto> getTiers(final String avlbflyBuyPoints) {
        final List<FlybuysRedeemTierDto> tiersAvailable = new ArrayList<>();
        int points = 2000;
        int currentTier = 1;
        while (points <= Integer.parseInt(avlbflyBuyPoints) && points <= MAX_REDEEM_POINT) {

            final FlybuysRedeemTierDto redeemTier = new FlybuysRedeemTierDto();
            redeemTier.setDollarAmt(new BigDecimal(Float.toString(points / POINTS_PER_DOLLAR)));
            redeemTier.setPoints(points);
            redeemTier.setRedeemCode(MOCK_REDEEM_CODE + currentTier);

            tiersAvailable.add(redeemTier);

            points += 2000;
            currentTier++;
        }
        return tiersAvailable;

    }

    /**
     * @return the mockResponse
     */
    public String getMockResponse() {
        return mockResponseCode;

    }

    /**
     * @return the mockAvailablePoints
     */
    public String getMockAvailablePoints() {
        return mockAvailablePoints;
    }

    /**
     * @param mockResponseStr
     * @param availableFlybuysPoints
     */
    public void setMockResponse(final String mockResponseStr, final String availableFlybuysPoints,
            final String consumeResponse, final String refundResponse) {
        for (final String validResponse : VALID_MOCK_RESPONSE) {
            if (validResponse.equalsIgnoreCase(mockResponseStr)) {
                this.mockResponseCode = validResponse;
                this.mockAvailablePoints = availableFlybuysPoints;
                this.mockRedeemRespondeCode = consumeResponse;
                this.mockRefundRespondeCode = refundResponse;
                return;
            }
        }
    }

}
