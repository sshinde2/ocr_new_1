/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.PosPriceCheckSummaryData;


/**
 * @author rmcalave
 *
 */
public interface PosPriceCheckMockFacade extends CommonMockFacade {

    @Override
    PosPriceCheckSummaryData getSummary();

    void setMockResponse(PosPriceCheckSummaryData posPriceCheckSummaryData);
}
