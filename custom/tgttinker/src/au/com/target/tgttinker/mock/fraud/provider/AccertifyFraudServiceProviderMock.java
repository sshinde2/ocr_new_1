package au.com.target.tgttinker.mock.fraud.provider;

import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.impl.FraudSymptom;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;
import au.com.target.tgtfraud.provider.AccertifyFraudServiceProvider;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 */
public class AccertifyFraudServiceProviderMock extends AccertifyFraudServiceProvider implements MockedByTarget {
    private static final Logger LOG = Logger.getLogger(AccertifyFraudServiceProviderMock.class);

    private static final String MOCK_OK = "OK";
    private static final String MOCK_FRAUD = "FRAUD";
    private static final String MOCK_CHECK = "CHECK";
    private static final String MOCK_EXCEPTION = "EXCEPTION";
    private static final String MOCK_DEFAULT = MOCK_OK;
    //CHECKSTYLE:OFF need the public to be after the private, because it references the private details
    public static final List<String> VALID_MOCK_RESPONSE = Collections.unmodifiableList(Arrays.asList(
            MOCK_OK, MOCK_FRAUD, MOCK_CHECK, MOCK_EXCEPTION));
    //CHECKSTYLE:ON
    private static final String PROVIDER_NAME = "mock";

    private boolean active = Config.getBoolean("mock.acertify.default.active", false);
    private String mockResponse = MOCK_DEFAULT;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * Get the current mock response
     * 
     * @return mock response
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * Set the response from future mocking requests
     * 
     * @param mockResponseStr
     *            response to give
     */
    public void setMockResponse(final String mockResponseStr) {
        for (final String validResponse : VALID_MOCK_RESPONSE) {
            if (validResponse.equalsIgnoreCase(mockResponseStr)) {
                this.mockResponse = validResponse;
                return;
            }
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected FraudServiceResponse performFraudCheck(final Transaction transaction)
            throws FraudCheckServiceUnavailableException {

        if (!active) {
            return super.performFraudCheck(transaction);
        }

        LOG.info("Mocking Credit Card capture");

        // Run the fraud check
        final TargetFraudServiceResponse response = new TargetFraudServiceResponse("this is a mock response",
                PROVIDER_NAME);
        if (mockResponse.equals(MOCK_CHECK)) {
            LOG.info("Acertify mock respose with a check");
            response.setRecommendation(RecommendationTypeEnum.REVIEW);
        }
        else if (mockResponse.equals(MOCK_FRAUD)) {
            LOG.info("Acertify mock respose with a reject");
            response.setRecommendation(RecommendationTypeEnum.REJECT);
        }
        else if (mockResponse.equals(MOCK_EXCEPTION)) {
            LOG.info("Acertify mock respose with an exception");
            throw new FraudCheckServiceUnavailableException("Failed to perform accertify check: ");
        }
        else {
            LOG.info("Acertify mock respose with a accept");
            response.setRecommendation(RecommendationTypeEnum.ACCEPT);
        }
        response.addSymptom(new FraudSymptom("Nothing to see here", 0.0));

        return response;
    }
}
