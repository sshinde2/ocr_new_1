/**
 * 
 */
package au.com.target.tgttinker.mock.ordersplitting;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.StoreSelectorStrategyImpl;


/**
 * @author gsing236
 *
 */
public class MockStoreSelectorStrategyImpl extends StoreSelectorStrategyImpl {



    @Override
    public TargetPointOfServiceModel selectStore(final OrderEntryGroup oeg) {
        final TargetPointOfServiceModel model = new TargetPointOfServiceModel();

        model.setStoreNumber(new Integer(7049));
        model.setAcceptLayBy(Boolean.FALSE);


        return model;
    }



}
