/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.PinPadPaymentData;


/**
 *
 */
public interface PinPadPaymentMockFacade extends CommonMockFacade {

    void setMockResponse(final String mockResponse);

    String updatePayment(final PinPadPaymentData pinPadPaymentData);

}
