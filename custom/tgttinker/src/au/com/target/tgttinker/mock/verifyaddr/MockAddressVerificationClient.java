/**
 * 
 */
package au.com.target.tgttinker.mock.verifyaddr;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.InitializationFailedException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;
import au.com.target.tgtverifyaddr.provider.impl.QASAddressVerificationClient;


/**
 * 
 * @author htan3
 *
 */
public class MockAddressVerificationClient extends QASAddressVerificationClient implements MockedByTarget {

    private boolean active = false;

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.provider.AddressVerificationClient#init()
     */
    @Override
    public void init() throws InitializationFailedException {
        super.init();

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.provider.AddressVerificationClient#isSearchAvailable(java.lang.String)
     */
    @Override
    public boolean isSearchAvailable(final String countryId) throws ServiceNotAvailableException {
        if (!active) {
            return super.isSearchAvailable(countryId);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.provider.AddressVerificationClient#search(java.lang.String, java.lang.String)
     */
    @Override
    public List<AddressData> search(final String address, final String countryId) throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        if (!active) {
            return super.search(address, countryId);
        }
        switch (address) {
            case "ServiceNotAvailable":
                throw new ServiceNotAvailableException(address);
            case "RequestTimeOut":
                throw new RequestTimeOutException(address);
            case "TooManyMatchesFound":
                throw new TooManyMatchesFoundException(address);
            default:
                return new ArrayList<>();
        }
    }

    @Override
    public List<AddressData> search(final String address, final String countryId, final String searchType)
            throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        if (!active) {
            return super.search(address, countryId, searchType);
        }
        return search(address, countryId);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.provider.AddressVerificationClient#formatAddress(au.com.target.tgtverifyaddr.data.AddressData)
     */
    @Override
    public FormattedAddressData formatAddress(final AddressData unformattedAddress) throws ServiceNotAvailableException {
        if (!active) {
            return super.formatAddress(unformattedAddress);
        }
        final String moniker = unformattedAddress.getMoniker();
        final FormattedAddressData formattedAddressData = new FormattedAddressData();
        if (moniker == null) {
            return formattedAddressData;
        }
        switch (moniker) {
            case "ServiceNotAvailable":
                throw new ServiceNotAvailableException(moniker);
            default:
                formattedAddressData.setAddressLine1(unformattedAddress.getPartialAddress());
                formattedAddressData.setCity("North Geelong");
                formattedAddressData.setState("VIC");
                formattedAddressData.setPostcode("3215");
                return formattedAddressData;
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        // YTODO Auto-generated method stub
        return active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

}
