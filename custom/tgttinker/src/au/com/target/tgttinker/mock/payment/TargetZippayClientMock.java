/**
 * 
 */
package au.com.target.tgttinker.mock.payment;

import org.apache.log4j.Logger;

import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionError;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionRejectedException;
import au.com.target.tgtpaymentprovider.zippay.client.impl.TargetZippayClientImpl;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateChargeRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateCheckoutRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtutility.logging.JsonLogger;


/**
 * @author salexa10
 *
 */
public class TargetZippayClientMock extends TargetZippayClientImpl implements MockedByTarget {

    private static final Logger LOG = Logger.getLogger(TargetZippayClientMock.class);

    private boolean throwExpection = false;

    private boolean createCheckoutActive = false;

    private boolean createCheckoutException = false;

    private CreateCheckoutResponse createCheckoutResponse;

    private boolean active = false;

    private boolean pingActive = false;

    private boolean retrieveCheckoutActive = false;

    private boolean retrieveCheckoutException = false;

    private CheckoutResponse retrieveCheckoutResponse;

    private boolean captureRequestActive = false;

    private boolean captureRequestException = false;

    private CreateChargeResponse captureRequestResponse;

    private CreateRefundRequest createRefundRequestCaptor;

    private boolean refundRequestActive;

    private boolean refundRequestException;

    private CreateRefundResponse mockRefundResponse;

    private boolean refundRejectException;

    private boolean refundTransErrorException;

    private boolean captureTransactionError;

    /**
     * Method to reset to defaults
     */
    public void resetDefault() {
        active = false;
        throwExpection = false;
        createCheckoutActive = false;
        pingActive = false;
        createCheckoutException = false;
        createCheckoutResponse = null;

        //retrieve
        retrieveCheckoutActive = false;
        retrieveCheckoutException = false;
        retrieveCheckoutResponse = null;

        //capture
        captureRequestActive = false;
        captureRequestException = false;
        captureRequestResponse = null;
        captureTransactionError = false;

        //refund
        refundRequestActive = false;
        refundRequestException = false;
        mockRefundResponse = null;
        refundRejectException = false;
        refundTransErrorException = false;
    }

    /**
     * Method to activate mocks for all zip api methods
     */
    public void activateMocks() {
        active = true;
        createCheckoutActive = true;
        pingActive = true;
        retrieveCheckoutActive = true;
        captureRequestActive = true;
        refundRequestActive = true;
    }

    @Override
    public boolean isZippayConnectionAvailable() {
        if (pingActive) {
            if (throwExpection) {
                return false;
            }
            else {
                return true;
            }
        }
        return super.isZippayConnectionAvailable();
    }

    @Override
    public CreateCheckoutResponse createCheckout(final CreateCheckoutRequest request)
            throws TargetZippayClientException {
        if (createCheckoutActive) {
            if (createCheckoutException) {
                throw new TargetZippayClientException("service unavailable");
            }
            else {
                return createCheckoutResponse;
            }

        }
        return super.createCheckout(request);
    }

    @Override
    public CheckoutResponse retrieveCheckout(final String checkoutId) throws TargetZippayClientException {
        if (retrieveCheckoutActive) {
            if (retrieveCheckoutException) {
                throw new TargetZippayClientException("Retrieve Checkout Service not available!");
            }
            JsonLogger.logJson(LOG, retrieveCheckoutResponse);
            return retrieveCheckoutResponse;
        }

        return super.retrieveCheckout(checkoutId);
    }

    @Override
    public CreateChargeResponse createCharge(final CreateChargeRequest request) throws TargetZippayClientException {
        if (captureRequestActive) {
            if (captureRequestException) {
                throw new TargetZippayClientException("Capture Payment Failed!");
            } else if(captureTransactionError){
                throw new TargetZippayTransactionError("Capture Transaction Failed at server");
            }
            JsonLogger.logJson(LOG, captureRequestResponse);
            return captureRequestResponse;
        }

        return super.createCharge(request);
    }

    @Override
    public CreateRefundResponse createRefund(final CreateRefundRequest createRefundRequest)
            throws TargetZippayClientException {
        if (refundRequestActive) {
            createRefundRequestCaptor = createRefundRequest;
            if (refundRequestException) {
                throw new TargetZippayClientException("Service not available");
            }
            else if (refundTransErrorException) {
                throw new TargetZippayTransactionError("Payment Error");
            }
            else if (refundRejectException) {
                throw new TargetZippayTransactionRejectedException("Payment Rejected!");
            }
            JsonLogger.logJson(LOG, mockRefundResponse);
            return mockRefundResponse;
        }
        return super.createRefund(createRefundRequest);
    }

    public boolean isRefundRejectException() {
        return refundRejectException;
    }

    public void setRefundRejectException(final boolean refundRejectException) {
        this.refundRejectException = refundRejectException;
    }

    public boolean isRefundTransErrorException() {
        return refundTransErrorException;
    }

    public void setRefundTransErrorException(final boolean refundTransErrorException) {
        this.refundTransErrorException = refundTransErrorException;
    }

    public CreateRefundResponse getMockRefundResponse() {
        return mockRefundResponse;
    }

    public void setMockRefundResponse(final CreateRefundResponse mockRefundResponse) {
        this.mockRefundResponse = mockRefundResponse;
    }

    public boolean isRefundRequestActive() {
        return refundRequestActive;
    }

    public void setRefundRequestActive(final boolean refundRequestActive) {
        this.refundRequestActive = refundRequestActive;
    }

    public boolean isRefundRequestException() {
        return refundRequestException;
    }

    public void setRefundRequestException(final boolean refundRequestException) {
        this.refundRequestException = refundRequestException;
    }

    /**
     * @param captureRequestActive
     *            the captureRequestActive to set
     */
    public void setCaptureRequestActive(final boolean captureRequestActive) {
        this.captureRequestActive = captureRequestActive;
    }

    /**
     * @param captureRequestException
     *            the captureRequestException to set
     */
    public void setCaptureRequestException(final boolean captureRequestException) {
        this.captureRequestException = captureRequestException;
    }

    /**
     * @return the captureRequestResponse
     */
    public CreateChargeResponse getCaptureRequestResponse() {
        return captureRequestResponse;
    }

    /**
     * @param captureRequestResponse
     *            the captureRequestResponse to set
     */
    public void setCaptureRequestResponse(final CreateChargeResponse captureRequestResponse) {
        this.captureRequestResponse = captureRequestResponse;
    }

    /**
     * @return the captureRequestActive
     */
    public boolean isCaptureRequestActive() {
        return captureRequestActive;
    }

    /**
     * @return the captureRequestException
     */
    public boolean isCaptureRequestException() {
        return captureRequestException;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @return the createCheckoutResponse
     */
    public CreateCheckoutResponse getCreateCheckoutResponse() {
        return createCheckoutResponse;
    }

    /**
     * @param createCheckoutResponse
     *            the createCheckoutResponse to set
     */
    public void setCreateCheckoutResponse(final CreateCheckoutResponse createCheckoutResponse) {
        this.createCheckoutResponse = createCheckoutResponse;
    }

    /**
     * @return the throwExpection
     */
    public boolean isThrowExpection() {
        return throwExpection;
    }

    /**
     * @param throwExpection
     *            the throwExpection to set
     */
    public void setThrowExpection(final boolean throwExpection) {
        this.throwExpection = throwExpection;
    }


    /**
     * @return the createCheckoutException
     */
    public boolean isCreateCheckoutException() {
        return createCheckoutException;
    }

    /**
     * @param createCheckoutException
     *            the createCheckoutException to set
     */
    public void setCreateCheckoutException(final boolean createCheckoutException) {
        this.createCheckoutException = createCheckoutException;
    }

    /**
     * @return the createCheckoutActive
     */
    public boolean isCreateCheckoutActive() {
        return createCheckoutActive;
    }

    /**
     * @param createCheckoutActive
     *            the createCheckoutActive to set
     */
    public void setCreateCheckoutActive(final boolean createCheckoutActive) {
        this.createCheckoutActive = createCheckoutActive;
    }

    /**
     * @return the pingActive
     */
    public boolean isPingActive() {
        return pingActive;
    }

    /**
     * @param pingActive
     *            the pingActive to set
     */
    public void setPingActive(final boolean pingActive) {
        this.pingActive = pingActive;
    }

    /**
     * @return the retrieveCheckoutException
     */
    public boolean isRetrieveCheckoutException() {
        return retrieveCheckoutException;
    }

    /**
     * @param retrieveCheckoutException
     *            the retrieveCheckoutException to set
     */
    public void setRetrieveCheckoutException(final boolean retrieveCheckoutException) {
        this.retrieveCheckoutException = retrieveCheckoutException;
    }

    /**
     * @return the retrieveCheckoutResponse
     */
    public CheckoutResponse getRetrieveCheckoutResponse() {
        return retrieveCheckoutResponse;
    }

    /**
     * @param retrieveCheckoutResponse
     *            the retrieveCheckoutResponse to set
     */
    public void setRetrieveCheckoutResponse(final CheckoutResponse retrieveCheckoutResponse) {
        this.retrieveCheckoutResponse = retrieveCheckoutResponse;
    }

    /**
     * @return the retrieveCheckoutActive
     */
    public boolean isRetrieveCheckoutActive() {
        return retrieveCheckoutActive;
    }

    /**
     * @param retrieveCheckoutActive
     *            the retrieveCheckoutActive to set
     */
    public void setRetrieveCheckoutActive(final boolean retrieveCheckoutActive) {
        this.retrieveCheckoutActive = retrieveCheckoutActive;
    }

    /**
     * @return the createRefundRequestCaptor
     */
    public CreateRefundRequest getCreateRefundRequestCaptor() {
        return createRefundRequestCaptor;
    }

    /**
     * @param createRefundRequestCaptor
     *            the createRefundRequestCaptor to set
     */
    public void setCreateRefundRequestCaptor(final CreateRefundRequest createRefundRequestCaptor) {
        this.createRefundRequestCaptor = createRefundRequestCaptor;
    }

    public void setCaptureTransactionError(final boolean captureTransactionError) {
        this.captureTransactionError = captureTransactionError;
    }
}
