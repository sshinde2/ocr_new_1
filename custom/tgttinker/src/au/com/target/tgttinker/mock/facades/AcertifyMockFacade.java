/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.AcertifySummaryData;


/**
 *
 */
public interface AcertifyMockFacade extends CommonMockFacade {
    @Override
    AcertifySummaryData getSummary();

    void setMockResponse(final String mockResponse);
}
