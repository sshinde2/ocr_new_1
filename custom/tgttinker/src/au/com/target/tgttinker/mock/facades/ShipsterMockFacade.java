/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

/**
 * @author rmcalave
 *
 */
public interface ShipsterMockFacade extends CommonMockFacade {
    void updateVerifyEmail(boolean verifyEmailMockEnable, boolean verifyEmailServiceAvailable);

    void updateConfirmOrder(boolean confirmOrderMockEnable, boolean confirmOrderServiceAvailable,
            boolean confirmOrderTimeOut);

    void verifyEmail(String verifyEmailResponse);

    void confirmOrder(String confirmOrderResponse);
}