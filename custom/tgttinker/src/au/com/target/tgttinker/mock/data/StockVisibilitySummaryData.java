/**
 * 
 */
package au.com.target.tgttinker.mock.data;

import java.util.List;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;


/**
 * @author rmcalave
 *
 */
public class StockVisibilitySummaryData extends CommonSummaryData {
    private List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtos;

    private String responseCode;

    private String exception;

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the exception
     */
    public String getException() {
        return exception;
    }

    /**
     * @param exception
     *            the exception to set
     */
    public void setException(final String exception) {
        this.exception = exception;
    }

    /**
     * @return the stockVisibilityItemResponseDtos
     */
    public List<StockVisibilityItemResponseDto> getStockVisibilityItemResponseDtos() {
        return stockVisibilityItemResponseDtos;
    }

    /**
     * @param stockVisibilityItemResponseDtos
     *            the stockVisibilityItemResponseDtos to set
     */
    public void setStockVisibilityItemResponseDtos(
            final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtos) {
        this.stockVisibilityItemResponseDtos = stockVisibilityItemResponseDtos;
    }

}
