/**
 * 
 */
package au.com.target.tgttinker.mock.instore;

import java.util.List;


/**
 * 
 * Store information data object to store dummy store information.
 * 
 * @author jjayawa1
 *
 */
public class StoreStockData {

    private String store;
    private String stock;

    private List<ProductData> products;

    /**
     * @return the store
     */
    public String getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final String store) {
        this.store = store;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(final List<ProductData> products) {
        this.products = products;
    }

    /**
     * @return the products
     */
    public List<ProductData> getProducts() {
        return products;
    }

    /**
     * @return the stock
     */
    public String getStock() {
        return stock;
    }

    /**
     * @param stock
     *            the stock to set
     */
    public void setStock(final String stock) {
        this.stock = stock;
    }

}
