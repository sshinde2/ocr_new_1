/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author gsing236
 *
 */
public class AfterpaySummaryData extends CommonSummaryData {

    private boolean mockActive;
    private boolean throwException;
    private boolean createOrderActive;
    private boolean capturePaymentException;
    private boolean getOrderExpection;
    private boolean getOrderActive;
    private boolean capturePaymentActive;
    private boolean createOrderException;
    private boolean pingActive;
    private boolean getPaymentActive;
    private boolean getPaymentException;
    private boolean createRefundActive;
    private boolean createRefundException;
    private boolean getConfigurationActive;
    private boolean getConfigurationException;


    private String mockedGetConfigurationResponse;
    private String mockedGetOrderResponse;
    private String mockedCreateOrderResponse;
    private String mockedCapturePaymentResponse;
    private String mockedGetPaymentResponse;
    private String mockedCreateRefundResponse;

    /**
     * @return the mockActive
     */
    @Override
    public boolean isMockActive() {
        return mockActive;
    }

    /**
     * @param mockActive
     *            the mockActive to set
     */
    @Override
    public void setMockActive(final boolean mockActive) {
        this.mockActive = mockActive;
    }

    /**
     * @return the throwException
     */
    public boolean isThrowException() {
        return throwException;
    }

    /**
     * @param throwException
     *            the throwException to set
     */
    public void setThrowException(final boolean throwException) {
        this.throwException = throwException;
    }

    /**
     * @return the mockedGetConfigurationResponse
     */
    public String getMockedGetConfigurationResponse() {
        return mockedGetConfigurationResponse;
    }

    /**
     * @param mockedGetConfigurationResponse
     *            the mockedGetConfigurationResponse to set
     */
    public void setMockedGetConfigurationResponse(final String mockedGetConfigurationResponse) {
        this.mockedGetConfigurationResponse = mockedGetConfigurationResponse;
    }

    /**
     * @return the mockedGetOrderResponse
     */
    public String getMockedGetOrderResponse() {
        return mockedGetOrderResponse;
    }

    /**
     * @param mockedGetOrderResponse
     *            the mockedGetOrderResponse to set
     */
    public void setMockedGetOrderResponse(final String mockedGetOrderResponse) {
        this.mockedGetOrderResponse = mockedGetOrderResponse;
    }

    /**
     * @return the mockedCreateOrderResponse
     */
    public String getMockedCreateOrderResponse() {
        return mockedCreateOrderResponse;
    }

    /**
     * @param mockedCreateOrderResponse
     *            the mockedCreateOrderResponse to set
     */
    public void setMockedCreateOrderResponse(final String mockedCreateOrderResponse) {
        this.mockedCreateOrderResponse = mockedCreateOrderResponse;
    }

    /**
     * @return the mockedCapturePaymentResponse
     */
    public String getMockedCapturePaymentResponse() {
        return mockedCapturePaymentResponse;
    }

    /**
     * @param mockedCapturePaymentResponse
     *            the mockedCapturePaymentResponse to set
     */
    public void setMockedCapturePaymentResponse(final String mockedCapturePaymentResponse) {
        this.mockedCapturePaymentResponse = mockedCapturePaymentResponse;
    }

    /**
     * @return the createOrderActive
     */
    public boolean isCreateOrderActive() {
        return createOrderActive;
    }

    /**
     * @param createOrderActive
     *            the createOrderActive to set
     */
    public void setCreateOrderActive(final boolean createOrderActive) {
        this.createOrderActive = createOrderActive;
    }


    /**
     * @return the getOrderExpection
     */
    public boolean isGetOrderExpection() {
        return getOrderExpection;
    }

    /**
     * @param getOrderExpection
     *            the getOrderExpection to set
     */
    public void setGetOrderExpection(final boolean getOrderExpection) {
        this.getOrderExpection = getOrderExpection;
    }

    /**
     * @return the getOrderActive
     */
    public boolean isGetOrderActive() {
        return getOrderActive;
    }

    /**
     * @param getOrderActive
     *            the getOrderActive to set
     */
    public void setGetOrderActive(final boolean getOrderActive) {
        this.getOrderActive = getOrderActive;
    }

    /**
     * @return the capturePaymentActive
     */
    public boolean isCapturePaymentActive() {
        return capturePaymentActive;
    }

    /**
     * @param capturePaymentActive
     *            the capturePaymentActive to set
     */
    public void setCapturePaymentActive(final boolean capturePaymentActive) {
        this.capturePaymentActive = capturePaymentActive;
    }

    /**
     * @return the createOrderException
     */
    public boolean isCreateOrderException() {
        return createOrderException;
    }

    /**
     * @param createOrderException
     *            the createOrderException to set
     */
    public void setCreateOrderException(final boolean createOrderException) {
        this.createOrderException = createOrderException;
    }

    /**
     * @return the capturePaymentException
     */
    public boolean isCapturePaymentException() {
        return capturePaymentException;
    }

    /**
     * @param capturePaymentException
     *            the capturePaymentException to set
     */
    public void setCapturePaymentException(final boolean capturePaymentException) {
        this.capturePaymentException = capturePaymentException;
    }

    /**
     * @return the pingActive
     */
    public boolean isPingActive() {
        return pingActive;
    }

    /**
     * @param pingActive
     *            the pingActive to set
     */
    public void setPingActive(final boolean pingActive) {
        this.pingActive = pingActive;
    }

    /**
     * @return the mockedGetPaymentResponse
     */
    public String getMockedGetPaymentResponse() {
        return mockedGetPaymentResponse;
    }

    /**
     * @param mockedGetPaymentResponse
     *            the mockedGetPaymentResponse to set
     */
    public void setMockedGetPaymentResponse(final String mockedGetPaymentResponse) {
        this.mockedGetPaymentResponse = mockedGetPaymentResponse;
    }

    /**
     * @return the getPaymentActive
     */
    public boolean isGetPaymentActive() {
        return getPaymentActive;
    }

    /**
     * @param getPaymentActive
     *            the getPaymentActive to set
     */
    public void setGetPaymentActive(final boolean getPaymentActive) {
        this.getPaymentActive = getPaymentActive;
    }

    /**
     * @return the getPaymentException
     */
    public boolean isGetPaymentException() {
        return getPaymentException;
    }

    /**
     * @param getPaymentException
     *            the getPaymentException to set
     */
    public void setGetPaymentException(final boolean getPaymentException) {
        this.getPaymentException = getPaymentException;
    }

    /**
     * @return the createRefundActive
     */
    public boolean isCreateRefundActive() {
        return createRefundActive;
    }

    /**
     * @param createRefundActive
     *            the createRefundActive to set
     */
    public void setCreateRefundActive(final boolean createRefundActive) {
        this.createRefundActive = createRefundActive;
    }

    /**
     * @return the createRefundException
     */
    public boolean isCreateRefundException() {
        return createRefundException;
    }

    /**
     * @param createRefundException
     *            the createRefundException to set
     */
    public void setCreateRefundException(final boolean createRefundException) {
        this.createRefundException = createRefundException;
    }

    /**
     * @return the mockedCreateRefundResponse
     */
    public String getMockedCreateRefundResponse() {
        return mockedCreateRefundResponse;
    }

    /**
     * @param mockedCreateRefundResponse
     *            the mockedCreateRefundResponse to set
     */
    public void setMockedCreateRefundResponse(final String mockedCreateRefundResponse) {
        this.mockedCreateRefundResponse = mockedCreateRefundResponse;
    }

    /**
     * @return the getConfigurationActive
     */
    public boolean isGetConfigurationActive() {
        return getConfigurationActive;
    }

    /**
     * @param getConfigurationActive
     *            the getConfigurationActive to set
     */
    public void setGetConfigurationActive(final boolean getConfigurationActive) {
        this.getConfigurationActive = getConfigurationActive;
    }

    /**
     * @return the getConfigurationException
     */
    public boolean isGetConfigurationException() {
        return getConfigurationException;
    }

    /**
     * @param getConfigurationException
     *            the getConfigurationException to set
     */
    public void setGetConfigurationException(final boolean getConfigurationException) {
        this.getConfigurationException = getConfigurationException;
    }

}
