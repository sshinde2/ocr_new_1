/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author gsing236
 *
 */
public class PollConsignmentSummaryData extends CommonSummaryData {

    private boolean success;

    private boolean serviceUnavailable;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the serviceUnavailable
     */
    public boolean isServiceUnavailable() {
        return serviceUnavailable;
    }

    /**
     * @param serviceUnavailable
     *            the serviceUnavailable to set
     */
    public void setServiceUnavailable(final boolean serviceUnavailable) {
        this.serviceUnavailable = serviceUnavailable;
    }
}
