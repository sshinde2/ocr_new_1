/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtfulfilment.client.PollConsignmentWarehouseRestClient;
import au.com.target.tgttinker.client.impl.MockPollConsignmentWarehouseRestClient;
import au.com.target.tgttinker.mock.data.PollConsignmentSummaryData;
import au.com.target.tgttinker.mock.facades.PollConsignmentMockFacade;


/**
 * @author gsing236
 *
 */
public class PollConsignmentMockFacadeImpl implements PollConsignmentMockFacade {

    private PollConsignmentWarehouseRestClient pollConsignmentRestClient;

    private final PollConsignmentSummaryData summaryData = new PollConsignmentSummaryData();

    @Override
    public void setActive(final boolean enable) {
        if (getPollConsignmentRestClient() instanceof MockPollConsignmentWarehouseRestClient) {
            final MockPollConsignmentWarehouseRestClient pollConsignmentRestClientMock = (MockPollConsignmentWarehouseRestClient)getPollConsignmentRestClient();
            pollConsignmentRestClientMock.setActive(enable);
            summaryData.setMockActive(enable);
        }
    }

    @Override
    public PollConsignmentSummaryData getSummary() {
        return summaryData;
    }

    @Override
    public void setMockSuccess(final String parameter) {
        if (getPollConsignmentRestClient() instanceof MockPollConsignmentWarehouseRestClient) {
            final MockPollConsignmentWarehouseRestClient pollConsignmentRestClientMock = (MockPollConsignmentWarehouseRestClient)getPollConsignmentRestClient();
            final boolean mockSuccess = BooleanUtils.toBoolean(parameter);
            pollConsignmentRestClientMock.setMockSuccess(mockSuccess);
            summaryData.setSuccess(mockSuccess);
        }
    }

    @Override
    public void setMockServiceUnavailable(final String parameter) {
        if (getPollConsignmentRestClient() instanceof MockPollConsignmentWarehouseRestClient) {
            final MockPollConsignmentWarehouseRestClient pollConsignmentRestClientMock = (MockPollConsignmentWarehouseRestClient)getPollConsignmentRestClient();
            final boolean mockServiceUnavailable = BooleanUtils.toBoolean(parameter);
            pollConsignmentRestClientMock.setMockServiceUnavailable(mockServiceUnavailable);
            summaryData.setServiceUnavailable(mockServiceUnavailable);
        }
    }

    @Override
    public void resetDefault() {
        if (getPollConsignmentRestClient() instanceof MockPollConsignmentWarehouseRestClient) {
            ((MockPollConsignmentWarehouseRestClient)getPollConsignmentRestClient()).resetDefault();
        }
    }

    /**
     * @return the pollConsignmentRestClient
     */
    public PollConsignmentWarehouseRestClient getPollConsignmentRestClient() {
        if (pollConsignmentRestClient == null) {
            pollConsignmentRestClient = (PollConsignmentWarehouseRestClient)Registry.getApplicationContext()
                    .getBean("pollConsignmentWarehouseRestClient");
        }
        return pollConsignmentRestClient;
    }
}
