/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author pthoma20
 *
 */
public class InventoryAdjustmentSummaryData extends CommonSummaryData {

    private boolean success;



    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

}
