/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import java.util.List;

import au.com.target.tgttinker.mock.data.NavData;


/**
 * @author dcwillia
 * 
 */
public interface NavFacade {
    /**
     * Get a list of available mocked services and their urls.
     * 
     * @return list of navigation mocking data
     */
    List<NavData> getNavList();
}
