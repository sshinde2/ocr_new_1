/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.NavData;
import au.com.target.tgttinker.mock.facades.NavFacade;


/**
 * @author dcwillia
 * 
 */
public class NavFacadeImpl implements NavFacade {

    /* (non-Javadoc)
     * @see au.com.target.web.facades.NavFacade#getNavList()
     */
    @Override
    public List<NavData> getNavList() {
        final List<NavData> mocks = new LinkedList<>();
        mocks.add(new NavData("Sms Harness", "smsHarness"));
        mocks.add(new NavData("Email Harness", "emailHarness"));
        mocks.add(new NavData("InStore Wifi", "access-wifi"));
        mocks.add(new NavData("Store Hours", "store-hours"));
        mocks.add(new NavData("Http Session", "targetHttpSessionManager.action"));

        final Map<String, NavData> navDataMap = new LinkedHashMap<>();
        navDataMap.put("targetCreditCardPaymentMethod", new NavData("TNS", "tns.action"));
        navDataMap.put("ipgClient", new NavData("IPG", "ipg.action"));
        navDataMap.put("payPalService", new NavData("PayPal", "paypal.action"));
        navDataMap.put("accertifyFraudServiceProvider", new NavData("Accertify", "acertify.action"));
        navDataMap.put("flybuysClient", new NavData("Flybuys", "flybuy.action"));
        navDataMap.put("targetSendSmsClient", new NavData("Sms Notifications", "sendSms.action"));
        navDataMap.put("targetCustomerSubscriptionClient",
                new NavData("Customer Subscription", "customerSubscription.action"));
        navDataMap.put("addressVerificationClient", new NavData("Address Verification", "verifyAddress.action"));
        navDataMap.put("targetPOSProductClient", new NavData("POS Price Check", "posPriceCheck.action"));
        navDataMap.put("shareWishListClient", new NavData("Customer Share WishList", "sharewishlist.action"));
        navDataMap.put("stockVisibilityClient", new NavData("Stock Visibility", "stockVisibility"));
        navDataMap.put("inventoryAdjustmentClient", new NavData("Inventory Adjustment", "inventoryadjustment.action"));
        navDataMap.put("targetAfterpayClient", new NavData("Afterpay", "afterpay.action"));
        navDataMap.put("mockAusPostShipsterClient", new NavData("Shipster", "shipster"));
        navDataMap.put("mockPollConsignmentWarehouseRestClient",
                new NavData("Poll Consignment", "pollConsignment.action"));
        navDataMap.put("fluentClient", new NavData("Fluent", "fluent.action"));
        navDataMap.put("targetZippayClient", new NavData("Zip Payment", "zippay.action"));

        for (final Entry<String, NavData> navDataEntry : navDataMap.entrySet()) {
            if (Registry.getApplicationContext().getBean(navDataEntry.getKey()) instanceof MockedByTarget) {
                mocks.add(navDataEntry.getValue());
            }
        }
        return mocks;
    }
}
