/**
 * 
 */
package au.com.target.tgttinker.mock.endeca.infront.querybuilder;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.MENEQueryResults;
import com.endeca.navigation.MNavigation;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author mgazal
 *
 */
public class EndecaProductQueryBuilderMock extends EndecaProductQueryBuilder implements MockedByTarget {

    private boolean active;

    private ERecList eRecs;

    @Override
    public ENEQueryResults getQueryResults(final EndecaSearchStateData searchStateData,
            final List<ProductModel> productModels, final int maxSize) {
        if (isActive() && eRecs != null) {
            final MENEQueryResults queryResults = new MENEQueryResults();
            final MNavigation navigation = new MNavigation();
            queryResults.setNavigation(navigation);
            navigation.setERecs(eRecs);
            return queryResults;
        }
        return super.getQueryResults(searchStateData, productModels, maxSize);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @param eRecs
     *            the eRecs to set
     */
    public void seteRecs(final ERecList eRecs) {
        this.eRecs = eRecs;
    }
}
