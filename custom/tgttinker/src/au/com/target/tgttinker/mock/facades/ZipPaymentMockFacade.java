/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.ZipPaymentSummaryData;


/**
 * @author hsing179
 *
 */
public interface ZipPaymentMockFacade extends CommonMockFacade {

    @Override
    ZipPaymentSummaryData getSummary();

    void setThrowException(boolean active);

    void setCreateCheckoutException(boolean active);

    void setPingActive(boolean active);

    void setCreateCheckoutActive(boolean active);

    void setMockCreateCheckoutResponse(String mock);

    void resetDefault();

    void setMockRetrieveCheckoutResponse(String retrieveCheckoutResponse);

    void setMockCaptureRequestResponse(String captureRequestResponse);

    void setRetrieveCheckoutActive(boolean retrieveCheckoutActive);

    void setRetrieveCheckoutException(boolean retrieveCheckoutException);

    void setCaptureRequestActive(boolean captureRequestActive);

    void setCaptureRequestException(boolean captureRequestException);

    void setRefundRequestActive(boolean refundRequestActive);

    void setRefundRequestException(boolean refundRequestException);

    void setRefundRejectException(boolean refundRejectException);

    void setRefundTransErrorException(boolean refundTransErrorException);

    void setMockRefundResponse(String mockRefundResponse);

    void setCaptureTransactionError(boolean captureTransactionError);
}
