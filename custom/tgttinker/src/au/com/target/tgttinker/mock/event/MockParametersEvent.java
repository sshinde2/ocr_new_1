/**
 * 
 */
package au.com.target.tgttinker.mock.event;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.Map;


/**
 * @author mgazal
 *
 */
public class MockParametersEvent extends AbstractEvent implements ClusterAwareEvent {

    private final MockParameterEventType eventType;

    private final Map<String, String> parameterMap;

    /**
     * @param mock
     * @param parameterMap
     */
    public MockParametersEvent(final MockParameterEventType mock, final Map<String, String> parameterMap) {
        super();
        this.eventType = mock;
        this.parameterMap = parameterMap;
    }

    @Override
    public boolean publish(final int sourceNodeId, final int targetNodeId) {
        return sourceNodeId != targetNodeId;
    }

    /**
     * @return the eventType
     */
    public MockParameterEventType getEventType() {
        return eventType;
    }

    /**
     * @return the parameterMap
     */
    public Map<String, String> getParameterMap() {
        return parameterMap;
    }

}
