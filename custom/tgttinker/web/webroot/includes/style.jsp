<!--
	Loads css style sheets.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="${request.contextPath}/_ui/css" var="cssroot"/>
<link type="text/css" href="${cssroot}/main.css" rel="stylesheet" media="screen" />
<link href="${cssroot}/bootstrap.min.css" rel="stylesheet">
<link href="${cssroot}/bootstrap-theme.min.css" rel="stylesheet">

<c:url value="${request.contextPath}/_ui/js" var="jsroot"/>
<script src="${jsroot}/jquery-3.1.0.min.js"></script>
<script src="${jsroot}/bootstrap.min.js"></script>