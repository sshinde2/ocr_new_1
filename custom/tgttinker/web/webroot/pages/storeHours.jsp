<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Stock Hours Screen</title></head>

<c:url value="${request.contextPath}/store-hours" var="storeHoursUrl"/>

<body>
	<div id="status">
	    <p> 
	        <form:form id="regenerateStoreHours" method="POST" action="${storeHoursUrl}">
	            <button type="submit" class="btn btn-default">
	                <c:out value="Regenerate store hours" />
	            </button>
	        </form:form>
	    </p>
	</div>
</body>
</html>