<!--
    Container for a categories_loop widget.
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Zip Payment Mock</title></head>
<body>

<script type="text/javascript">

jQuery(document).ready(function(){

    $("#createCheckoutException").change(function(){
        if(this.checked){
            $("#createCheckoutActive").prop("checked", true);
        }
    });

    $("#createCheckoutActive").change(function(){
        if(!this.checked){
            $("#createCheckoutException").prop("checked", false);
        }
     });

     $("#pingException").change(function(){
            if(this.checked){
               $("#pingActive").prop("checked", true);
           }
      });
    $("#pingActive").change(function(){
        if(!this.checked){
            $("#pingException").prop("checked", false);
        }
     });

     $("#retrieveCheckoutException").change(function(){
            if(this.checked){
               $("#retrieveCheckoutActive").prop("checked", true);
           }
      });
    $("#retrieveCheckoutActive").change(function(){
        if(!this.checked){
            $("#retrieveCheckoutException").prop("checked", false);
        }
     });


     $("#captureRequestException, #captureTransactionError").change(function(){
            if(this.checked){
               $("#captureRequestActive").prop("checked", true);
           }
      });
    $("#captureRequestActive").change(function(){
        if(!this.checked){
            $("#captureRequestException").prop("checked", false);
            $("#captureTransactionError").prop("checked", false);
        }
     });


     $("#refundRequestException, #refundTransErrorException, #refundRejectException").change(function(){
            if(this.checked){
               $("#refundRequestActive").prop("checked", true);
           }
      });
    $("#refundRequestActive").change(function(){
        if(!this.checked){
            $("#refundRequestException").prop("checked", false);
            $("#refundTransErrorException").prop("checked", false);
            $("#refundRejectException").prop("checked", false);
        }
     });

});

</script>
<div class="notification">
    <div class="error" style="font-color:red; font-weight:bold">${error}</div>
</div>
<div id="status">
    <p>
        <c:url value="${request.contextPath}/zippay.action" var="zippayUrl"/>
        <c:choose>
            <c:when test="${zippay.mockActive}">
              Active
               <form:form id='enablemock' method="POST" action="?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
            </c:when>
            <c:otherwise>
            Inactive
                <form:form id='enablemock' method="POST" action="?enable=true">
                    <button type="submit">Enable</button>
                </form:form>
            </c:otherwise>
        </c:choose>
    </p>
	    <c:if test="${zippay.mockActive}">
	        <form:form id="setMockValues" method="POST" action="${zippayUrl}" commandName="zippay">
	           <table border="2">
	               <tr>
	                   <th style="width:200">API</th>
	                   <th style="width:250">Mock response (Yes/No) <span style="font-weight: normal"> (* if selecting this true, remember to change below json as per your scenario)</span></th>
	                   <th style="width:250">Throw Exception (Yes/No) <span style="font-weight: normal"> (* if selecting this true, make sure 'Mock Response' of respective operation is also selected as true)</span></th>
	               </tr>
	               <tr>
                       <td>CheckoutActive</td>
                       <td align="center"> <form:checkbox id="createCheckoutActive" path="createCheckoutActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="createCheckoutException" path="createCheckoutException"/> <br/><br/></td>
                   </tr>
	               <tr>
	                   <td>Ping</td>
	                   <td align="center"> <form:checkbox id="pingActive" path="pingActive" /> <br/><br/></td>
	                   <td align="center"><form:checkbox id="pingException" path="throwException"/> <br/><br/></td>
	               </tr>
	               <tr>
	                   <td>Retrieve Checkout</td>
	                   <td align="center"> <form:checkbox id="retrieveCheckoutActive" path="retrieveCheckoutActive" /> <br/><br/></td>
	                   <td align="center"><form:checkbox id="retrieveCheckoutException" path="retrieveCheckoutException"/> <br/><br/></td>
	               </tr>
	               <tr>
	                   <td>Capture Request</td>
	                   <td align="center"> <form:checkbox id="captureRequestActive" path="captureRequestActive" /> <br/><br/></td>
	                   <td align="center">
	                    <div>Client Exception<form:checkbox id="captureRequestException" path="captureRequestException"/></div>
	                    <div>Transaction Error<form:checkbox id="captureTransactionError" path="captureTransactionError"/></div>
	                    <br/><br/></td>
	               </tr>
	               <tr>
	                   <td>Refund Request</td>
	                   <td align="center"> <form:checkbox id="refundRequestActive" path="refundRequestActive" /> <br/><br/></td>
	                   <td align="left">
                           <div >Service Exception:<form:checkbox id="refundRequestException" path="refundRequestException"/></div>
                           <div>Transaction Exception: <form:checkbox id="refundTransErrorException" path="refundTransErrorException"/></div>
                           <div>Reject Exception: <form:checkbox id="refundRejectException" path="refundRejectException"/></div>
	                        <br/><br/>
	                   </td>

	               </tr>
	           </table>
	           <br/><br/>
	           <button type="submit">Update</button>
	           <br/><br/>
		        <table>
		           <tr>
		               <td style="width:350"><b>Mocked response for createCheckout</b></td><br/>
		               <td style="width:100"><form:textarea rows="15" cols="65" path="createCheckoutResponse" /></td>
		           </tr>
		           <tr>
		               <td style="width:350"><b>Mocked response for Retrieve Checkout</b></td><br/>
		               <td style="width:100"><form:textarea rows="15" cols="65" path="retrieveCheckoutResponse" /></td>
		           </tr>
		           <tr>
		               <td style="width:350"><b>Mocked response for Capture Request Response</b></td><br/>
		               <td style="width:100"><form:textarea rows="15" cols="65" path="captureRequestResponse" /></td>
		           </tr>
		           <tr>
		               <td style="width:350"><b>Mocked response for Refund Request Response</b></td><br/>
		               <td style="width:100"><form:textarea rows="15" cols="65" path="mockRefundResponse" /></td>
		           </tr>
		        </table>
                <br/><br/>
	        </form:form>
	    </c:if>
</div>