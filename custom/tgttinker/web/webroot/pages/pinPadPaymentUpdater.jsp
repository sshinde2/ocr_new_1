<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<form:form method="post" commandName="pinPadPaymentDataForm">

	<form:errors id="" path="*" element="div" />
	<div id="orderShippingFields" style="border: 1px solid;">
		<h3>For Pin Pad Payment</h3>
		<br />
		<table border="1">
			<tr>
				<td>Order Number:</td>
				<td><form:input path="orderNo" /></td>
			</tr>
			<tr>
				<td>RRN:</td>
				<td><form:input path="rrn" /></td>
			</tr>
			<tr>
				<td>Card Type:</td>
				<td><form:input path="cardType" /></td>
			</tr>
			<tr>
				<td>Card Number:</td>
				<td><form:input path="maskedCardNumber" /></td>
			</tr>
			<tr>
				<td>Resp Ascii:(Success/Failure)</td>
				<td><form:input path="respAscii" /></td>
			</tr>
			<tr>
				<td>Account Type:</td>
				<td><form:input path="accountType" /></td>
			</tr>
			<tr>
				<td>Journ Roll:</td>
				<td><form:input path="journRoll" /></td>
			</tr>
			<tr>
				<td>Auth Code:</td>
				<td><form:input path="authCode" /></td>
			</tr>
			<tr>
				<td>Journal:</td>
				<td><form:input path="journal" /></td>
			</tr>
		</table>
	</div>
	<br />

	<input type="submit" value="Send PinPad Response" />
	<br />	<br /> <br/>
	<c:if test="${Message}!=null">
		<div><h3>${Message}</h3></div>
	</c:if>
</form:form>