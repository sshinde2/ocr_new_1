/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtfacades.process.email.data.ClickAndCollectEmailData;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author bhuang3
 * 
 */
public class CncNotificationEmailSender extends AbstractEmailSender {

    private static final Logger LOG = Logger.getLogger(CncNotificationEmailSender.class);

    @Override
    public boolean send(final EmailHarnessForm emailHarnessForm) {

        final OrderModel order = getOrderService().findOrderModelForOrderId(emailHarnessForm.getOrderNumber());

        if (order == null)
        {
            LOG.error("No order found for order code: " + emailHarnessForm.getOrderNumber());
            return false;
        }

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.SEND_CLICK_AND_COLLECT_NOTIFICATION_PROCESS;
        final String processCode = processDefinitionName + "-" + order.getCode()
                + "-" + System.currentTimeMillis();
        final OrderProcessModel processModel = getTargetBusinessProcessService().createProcess(processCode,
                processDefinitionName);

        processModel.setOrder(order);
        getModelService().save(processModel);

        final ClickAndCollectEmailData clickAndCollectEmailData = new ClickAndCollectEmailData();
        clickAndCollectEmailData.setStoreNumber(emailHarnessForm.getStoreNumber());
        clickAndCollectEmailData.setLetterType(emailHarnessForm.getLetterType());

        getOrderProcessParameterHelper().setCncNotificationData(processModel, clickAndCollectEmailData);

        try
        {
            getHybrisEmailStrategy().sendEmail(processModel, "CncNotificationEmailTemplate");
        }
        catch (final BusinessProcessEmailException e)
        {
            LOG.error("Problem With BusinessProcessEmail ", e);
            return false;
        }
        catch (final IllegalArgumentException ie) {

            LOG.error("Problem With Illegal Argument", ie);
            return false;
        }

        return true;

    }

}
