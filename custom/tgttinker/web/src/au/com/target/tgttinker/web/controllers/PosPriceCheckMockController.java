/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgttinker.mock.data.PosPriceCheckSummaryData;
import au.com.target.tgttinker.mock.facades.PosPriceCheckMockFacade;
import au.com.target.tgttinker.web.forms.PosPriceCheckDataForm;


/**
 * @author rmcalave
 *
 */
@Controller
public class PosPriceCheckMockController {
    private static final String POS_PRICE_CHECK_ACTION = "/posPriceCheck.action";

    private static final String SUMMARY_VIEW = "/pages/posPriceCheckSummary";

    @Autowired
    private PosPriceCheckMockFacade posPriceCheckMockFacade;

    @RequestMapping(value = POS_PRICE_CHECK_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        final PosPriceCheckSummaryData posPriceCheckSummaryData = posPriceCheckMockFacade.getSummary();
        model.addAttribute("posPriceCheckData", posPriceCheckSummaryData);

        final PosPriceCheckDataForm posPriceCheckDataForm = new PosPriceCheckDataForm();
        posPriceCheckDataForm.setDescription(posPriceCheckSummaryData.getDescription());
        posPriceCheckDataForm.setErrorMessage(posPriceCheckSummaryData.getErrorMessage());
        posPriceCheckDataForm.setErrorSource(posPriceCheckSummaryData.getErrorSource());
        posPriceCheckDataForm.setItemcode(posPriceCheckSummaryData.getItemcode());
        posPriceCheckDataForm.setPrice(posPriceCheckSummaryData.getPrice());
        posPriceCheckDataForm.setSuccess(posPriceCheckSummaryData.isSuccess());
        posPriceCheckDataForm.setTimeout(posPriceCheckSummaryData.isTimeout());
        posPriceCheckDataForm.setWasPrice(posPriceCheckSummaryData.getWasPrice());

        model.addAttribute("posPriceCheckDataForm", posPriceCheckDataForm);
        return SUMMARY_VIEW;
    }

    @RequestMapping(value = POS_PRICE_CHECK_ACTION, method = RequestMethod.POST)
    public String updateMock(@RequestParam(value = "enable", required = false) final String enable,
            final ModelMap model,
            final PosPriceCheckDataForm posPriceCheckDataForm) {
        final boolean isEnable = BooleanUtils.toBoolean(enable);
        posPriceCheckMockFacade.setActive(isEnable);

        if (isEnable) {
            final PosPriceCheckSummaryData summaryData = new PosPriceCheckSummaryData();
            summaryData.setDescription(posPriceCheckDataForm.getDescription());
            summaryData.setErrorMessage(posPriceCheckDataForm.getErrorMessage());
            summaryData.setErrorSource(posPriceCheckDataForm.getErrorSource());
            summaryData.setItemcode(posPriceCheckDataForm.getItemcode());
            summaryData.setPrice(posPriceCheckDataForm.getPrice());
            summaryData.setSuccess(posPriceCheckDataForm.isSuccess());
            summaryData.setTimeout(posPriceCheckDataForm.isTimeout());
            summaryData.setWasPrice(posPriceCheckDataForm.getWasPrice());
            posPriceCheckMockFacade.setMockResponse(summaryData);
        }
        else {
            posPriceCheckMockFacade.setMockResponse(null);
        }

        return getMockSummary(model);
    }

    @RequestMapping(value = POS_PRICE_CHECK_ACTION, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PosPriceCheckSummaryData updateMock(@RequestParam(value = "enable", required = false) final String enable,
            @RequestBody final PosPriceCheckSummaryData summaryData) {
        final boolean isEnable = BooleanUtils.toBoolean(enable);
        posPriceCheckMockFacade.setActive(isEnable);

        posPriceCheckMockFacade.setMockResponse(summaryData);

        return getMockSummaryJson();
    }

    @RequestMapping(value = POS_PRICE_CHECK_ACTION
            + "/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PosPriceCheckSummaryData getMockSummaryJson() {
        final PosPriceCheckSummaryData posPriceCheckSummaryData = posPriceCheckMockFacade.getSummary();
        return posPriceCheckSummaryData;
    }
}
