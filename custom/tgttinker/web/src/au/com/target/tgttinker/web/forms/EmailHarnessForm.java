/**
 * 
 */
package au.com.target.tgttinker.web.forms;

import de.hybris.platform.validation.annotations.NotBlank;


/**
 * 
 * @author asingh78
 * 
 */
public class EmailHarnessForm
{
    @NotBlank
    private String toAddress;
    private String orderNumber;
    private String productCode;
    private String emails;
    private String forgottenPasswordCustomerEmail;
    private String letterType;
    private String storeNumber;

    /**
     * @return the emails
     */
    public String getEmails()
    {
        return emails;
    }

    /**
     * @param emails
     *            the emails to set
     */
    public void setEmails(final String emails)
    {
        this.emails = emails;
    }

    /**
     * @return the toAddress
     */
    public String getToAddress()
    {
        return toAddress;
    }

    /**
     * @param toAddress
     *            the toAddress to set
     */
    public void setToAddress(final String toAddress)
    {
        this.toAddress = toAddress;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber()
    {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the productCode
     */
    public String getProductCode()
    {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }

    /**
     * @return the forgottenPasswordCustomerEmail
     */
    public String getForgottenPasswordCustomerEmail()
    {
        return forgottenPasswordCustomerEmail;
    }

    /**
     * @param forgottenPasswordCustomerEmail
     *            the forgottenPasswordCustomerEmail to set
     */
    public void setForgottenPasswordCustomerEmail(final String forgottenPasswordCustomerEmail)
    {
        this.forgottenPasswordCustomerEmail = forgottenPasswordCustomerEmail;
    }

    /**
     * @return the letterType
     */
    public String getLetterType() {
        return letterType;
    }

    /**
     * @param letterType
     *            the letterType to set
     */
    public void setLetterType(final String letterType) {
        this.letterType = letterType;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }



}
