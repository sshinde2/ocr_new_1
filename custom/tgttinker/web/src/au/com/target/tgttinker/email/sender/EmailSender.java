/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public interface EmailSender
{
    boolean send(EmailHarnessForm emailHarnessForm);
}
