/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.AfterpayMockFacade;


/**
 * @author gsing236
 *
 */
@Controller
public class AfterpayMockController {

    /** URL to categories action. */
    private static final String AFTERPAY_ACTION = "/afterpay.action";

    /** Categories form view name. */
    private static final String SUMMARY_VIEW = "/pages/afterpaySummary";

    @Autowired
    private AfterpayMockFacade afterpayMockFacade;

    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = AFTERPAY_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        model.addAttribute("afterpay", afterpayMockFacade.getSummary());
        return SUMMARY_VIEW;
    }


    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = AFTERPAY_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable,
            final ModelMap model) {
        if (enable != null) {
            final boolean enableFlag = BooleanUtils.toBoolean(enable);
            afterpayMockFacade.setActive(enableFlag);
            // if mock not enabled then reset all data
            if (!enableFlag) {
                afterpayMockFacade.resetDefault();
            }
        }

        afterpayMockFacade.setThrowException(request.getParameter("throwException"));

        afterpayMockFacade.setCapturePaymentException(request.getParameter("capturePaymentException"));

        afterpayMockFacade.setGetOrderExpection(request.getParameter("getOrderExpection"));

        afterpayMockFacade.setCreateOrderException(request.getParameter("createOrderException"));

        afterpayMockFacade.setGetOrderActive(request.getParameter("getOrderActive"));

        afterpayMockFacade.setCapturePaymentActive(request.getParameter("capturePaymentActive"));

        afterpayMockFacade.setCreateOrderActive(request.getParameter("createOrderActive"));

        afterpayMockFacade.setPingActive(request.getParameter("pingActive"));

        afterpayMockFacade.setGetPaymentActive(request.getParameter("getPaymentActive"));

        afterpayMockFacade.setGetPaymentException(request.getParameter("getPaymentException"));

        afterpayMockFacade.setCreateRefundActive(request.getParameter("createRefundActive"));

        afterpayMockFacade.setCreateRefundException(request.getParameter("createRefundException"));

        afterpayMockFacade.setGetConfigurationActive(request.getParameter("getConfigurationActive"));

        afterpayMockFacade.setGetConfigurationException(request.getParameter("getConfigurationException"));

        final String getConfigurationMock = request.getParameter("mockedGetConfigurationResponse");
        if (getConfigurationMock != null) {
            afterpayMockFacade.mockGetConfigurationResponse(getConfigurationMock);
        }
        final String getOrderMock = request.getParameter("mockedGetOrderResponse");
        if (getOrderMock != null) {
            afterpayMockFacade.mockGetOrderResponse(getOrderMock);
        }
        final String createOrderMock = request.getParameter("mockedCreateOrderResponse");
        if (createOrderMock != null) {
            afterpayMockFacade.mockCreateOrderResponse(createOrderMock);
        }
        final String capturePaymentMock = request.getParameter("mockedCapturePaymentResponse");
        if (capturePaymentMock != null) {
            afterpayMockFacade.mockCapturePaymentResponse(capturePaymentMock);
        }

        final String getPaymentMock = request.getParameter("mockedGetPaymentResponse");
        if (getPaymentMock != null) {
            afterpayMockFacade.mockGetPaymentResponse(getPaymentMock);
        }

        final String createRefundMock = request.getParameter("mockedCreateRefundResponse");
        if (createRefundMock != null) {
            afterpayMockFacade.mockCreateRefundResponse(createRefundMock);
        }

        return getMockSummary(model);
    }
}
