/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgttinker.sms.TargetSmsSendTestService;
import au.com.target.tgttinker.web.forms.SmsHarnessForm;


/**
 * @author bhuang3
 * 
 */

@Controller
@Scope("request")
@Lazy(true)
@RequestMapping(value = "/smsHarness")
public class SmsTestHarnessController {

    private static final Logger LOG = Logger.getLogger(SmsTestHarnessController.class);

    @Resource(name = "targetSmsSendTestService")
    private TargetSmsSendTestService targetSmsSendTestService;

    @RequestMapping(method = RequestMethod.GET)
    public String get(final Model model)
    {
        final Map<String, String> smsList = new LinkedHashMap<String, String>();
        smsList.put("CNCAvailableForCollectSMSTemplate", "cnc notification sms");
        model.addAttribute("smsList", smsList);
        final SmsHarnessForm form = new SmsHarnessForm();
        model.addAttribute("smsHarnessForm", form);
        return "pages/harness/smsHarnessPage";
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public String post(final Model model, final SmsHarnessForm SmsHarnessForm)
    {
        final Map<String, String> smsList = new LinkedHashMap<String, String>();
        smsList.put("CNCAvailableForCollectSMSTemplate", "cnc Notification sms");
        model.addAttribute("smsList", smsList);
        model.addAttribute("smsHarnessForm", SmsHarnessForm);

        if (StringUtils.isNotBlank(SmsHarnessForm.getSmsList()))
        {
            LOG.info("selected Sms = " + SmsHarnessForm.getSmsList());
            model.addAttribute("smsMessage", targetSmsSendTestService.generateSmsMessage(SmsHarnessForm));
        }

        return "pages/harness/smsHarnessPage";
    }
}
