/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtinitialdata.constants;


/**
 * Global class for all TgtInitialData constants.
 */
@SuppressWarnings("deprecation")
public final class TgtInitialDataConstants extends GeneratedTgtInitialDataConstants
{
    public static final String EXTENSIONNAME = "tgtinitialdata";

    private TgtInitialDataConstants()
    {
        // prevent construction
    }
}
