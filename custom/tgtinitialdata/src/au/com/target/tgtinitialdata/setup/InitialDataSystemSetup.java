/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtinitialdata.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgtinitialdata.constants.TgtInitialDataConstants;
import au.com.target.tgtinitialdata.processor.InitializeProductsDepartmentDeliveryMode;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup {
    private static final String DUMMY_STORE = "dummyStoreno";
    private static final String DUMMY_STORE_COUNTRY = "dummyStorenoCountry";
    private static final String IMPORT_SAMPLE_STORE_AND_PRODUCT_DATA = "importSampleStoreAndProductData";
    private static final String IMPORT_SAMPLE_PRODUCT_DATA = "importSampleProductData";
    private static final String IMPORT_SAMPLE_CMS_CONTENT = "importSampleCMSContent";
    private static final String IMPORT_KIOSK_SAMPLE_DATA = "importKioskSampleData";
    private static final String SAMPLE_DATA_IMPORT_FOLDER = "tgtinitialdata";
    private static final String IMPORT_CMS_TESTS = "importCMSTemplateTests";
    private static final String TARGET_CONTENT_CATALOG = "targetContentCatalog";
    private static final String SAMPLE_CONTENT_PATH = "/tgtinitialdata/import/contentCatalogs/"
            + TARGET_CONTENT_CATALOG + "/";
    private static final String KIOSK_SAMPLE_DATA_PATH = "/tgtinitialdata/import/kiosk/";
    private InitializeProductsDepartmentDeliveryMode initializeProductsDepartmentDeliveryMode;

    /**
     * Generates the Dropdown and Multi-select boxes for the project data import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();

        params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_STORE_AND_PRODUCT_DATA,
                "Import Sample Stores", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_PRODUCT_DATA,
                "Import Sample Products (Requires Sample Stores)", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_CMS_CONTENT, "Import Sample CMS Content", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_CMS_TESTS, "Import CMS Test Template", false));
        params.add(createBooleanSystemSetupParameter(IMPORT_KIOSK_SAMPLE_DATA, "Import Kiosk Sample Data", true));

        return params;
    }

    /**
     * Implement this method to create initial objects. This method will be called by system creator during
     * initialization and system update. Be sure that this method can be called repeatedly.
     *
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        // Add Essential Data here as you require
    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system
     * initialization.
     *
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.INIT)
    public void createProjectData(final SystemSetupContext context) {
        if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_CMS_CONTENT)) {
            importCMSData(context, TARGET_CONTENT_CATALOG);
        }

        if (getBooleanSystemSetupParameter(context, IMPORT_CMS_TESTS)) {
            importCMSTemplateTests(context, TARGET_CONTENT_CATALOG);
        }

        if (getBooleanSystemSetupParameter(context, IMPORT_KIOSK_SAMPLE_DATA)) {
            importKioskSampleData(context, TARGET_CONTENT_CATALOG);
        }

        if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_STORE_AND_PRODUCT_DATA)) {
            importCommonData(context, SAMPLE_DATA_IMPORT_FOLDER);
            importStoreInitialData(context, SAMPLE_DATA_IMPORT_FOLDER, "target", "target",
                    Collections.singletonList("target"));
        }
    }

    /**
     * Use this method to import a standard setup store.
     *
     * @param context
     *            the context provides the selected parameters and values
     * @param storeName
     *            the name of the store
     * @param productCatalog
     *            the name of the product catalog
     * @param contentCatalogs
     *            the list of content catalogs
     */
    protected void importStoreInitialData(final SystemSetupContext context, final String importDirectory,
            final String storeName,
            final String productCatalog, final List<String> contentCatalogs) {
        logInfo(context, "Begin importing store [" + storeName + "]");

        importProductCatalog(context, importDirectory, productCatalog);

        logInfo(context, "Begin importing advanced personalization rules for [" + storeName + "]");

        final String importRoot = "/" + importDirectory + "/import";

        importImpexFile(context, importRoot + "/stores/" + storeName + "/btg.impex", false);
        importImpexFile(context, importRoot + "/stores/" + storeName + "/target-opening-schedules.impex", false);

        removeAllStoreHours(context);
        createStoreHours(context);

        importImpexFile(context, importRoot + "/stores/" + storeName + "/target-points-of-service.impex", false);
        importImpexFile(context, importRoot + "/stores/" + storeName + "/ntl-stores.impex", false);
        // Instore fulfilment
        importImpexFile(context, importRoot + "/instoreFulfilment/global-store-fulfilment-capabilities.impex");
        importImpexFile(context, importRoot + "/instoreFulfilment/target-standard-parcel-details.impex");

        importImpexFile(context, importRoot + "/common/hotcards.impex", false);
        importImpexFile(context, importRoot + "/common/vouchers.impex", false);
        importImpexFile(context, importRoot + "/common/vouchers-cents.impex", false);

        if (getBooleanSystemSetupParameter(context, IMPORT_KIOSK_SAMPLE_DATA)) {
            importImpexFile(context, KIOSK_SAMPLE_DATA_PATH + "bulky-board-store-warehouses.impex", true);
            importImpexFile(context, KIOSK_SAMPLE_DATA_PATH + "bulky-board-products.impex", true);
        }

        // Load media (Shop the Look)
        importImpexFile(context, importRoot + "/productCatalogs/targetProductCatalog/shopTheLook/media.impex");

        // create product and content sync jobs
        synchronizeProductCatalog(context, productCatalog, false);

        // perform product sync job
        boolean productSyncSuccess = synchronizeProductCatalog(context, productCatalog, true);
        if (!productSyncSuccess) {
            logInfo(context, "Product catalog synchronization for [" + productCatalog
                    + "] did not complete successfully, that's ok, we will rerun it after the content catalog sync.");
        }

        if (!productSyncSuccess) {
            // Rerun the product sync if required
            logInfo(context, "Rerunning product catalog synchronization for [" + productCatalog + "]");
            productSyncSuccess = synchronizeProductCatalog(context, productCatalog, true);
            if (!productSyncSuccess) {
                logError(context, "Rerunning product catalog synchronization for [" + productCatalog
                        + "], failed please consult logs for more details.", null);
            }
        }

        if (productSyncSuccess) {
            final String impexPath = "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/";
            importImpexFile(context, impexPath + "automationTest/contract-testing-deals-enable.impex", false);

            // Load looks (Shop the Look)
            importImpexFile(context, importRoot + "/productCatalogs/targetProductCatalog/shopTheLook/looks.impex");
        }

        logInfo(context, "Done importing store [" + storeName + "]");

        //Is sample products are imported, then import reviews, price and tax after product sync (to both the catalogs)
        if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_PRODUCT_DATA)) {
            // Load reviews after synchronization is done
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/reviews.impex", false);
            //Create product price
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/productprice.impex", true);

            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/tax/productTax.impex", true);

            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/target-price-row-was-now-promo.impex", true);

            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/automationTest/automation-fetch-products-price-setup.impex", true);

            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/automationTest/contract-testing-pos-products-price.impex", true);

            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/automationTest/contract-testing-pos-products-ratings.impex", true);

            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                    + "ProductCatalog/automationTest/contract-testing-giftcard-price-setup.impex", true);

        }

        importImpexFile(context, importRoot + "/carriers/targetCarriers.impex");
        importImpexFile(context, importRoot + "/common/post-code-group.impex");
        importImpexFile(context, importRoot + "/common/delivery-modes-base.impex", false);
        importImpexFile(context, importRoot + "/common/delivery-modes-default.impex", false);

        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + productCatalog
                + "ProductCatalog/" + "test-delivery-modes-colour-variant-products.impex", false);

        importImpexFile(context, importRoot + "/marketplaces/sales-application-configs.impex");

        importImpexFile(context, importRoot + "/physicalGiftcard/physical-giftcard-sharedconfig.impex");


    }

    protected void importProductCatalog(final SystemSetupContext context, final String importDirectory,
            final String catalogName) {
        logInfo(context, "Begin importing Product Catalog [" + catalogName + "]");

        // Load Units
        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                + "ProductCatalog/classifications-units.impex", false);

        //Load Colours
        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                + "ProductCatalog/colour-setup.impex", false);

        // Load Categories

        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                + "ProductCatalog/categories-classifications.impex", false);

        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                + "ProductCatalog/categories-giftcards.impex", false);

        // Load medias for Categories as Suppliers loads some new Categories
        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                + "ProductCatalog/categories-media.impex", false);
        // Load the gift card prd type before importing the prds
        importImpexFile(context, "/" + importDirectory + "/import/physicalGiftcard/physical-giftcard-config.impex");

        importImpexFile(context, "/" + importDirectory + "/import/preorder/preOrderWarehouse.impex");
        // Load Products

        importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                + "ProductCatalog/products-classifications.impex", false);

        if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_PRODUCT_DATA)) {
            // Target sample products
            final String impexPath = "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/";
            importImpexFile(context, impexPath + "brands.impex", false);
            importImpexFile(context, impexPath + "baby-products.impex", false);
            importImpexFile(context, impexPath + "body-beauty-products.impex", false);
            importImpexFile(context, impexPath + "home-products.impex", false);
            importImpexFile(context, impexPath + "kids-products.impex", false);
            importImpexFile(context, impexPath + "men-products.impex", false);
            importImpexFile(context, impexPath + "school-products.impex", false);
            importImpexFile(context, impexPath + "toys-products.impex", false);
            importImpexFile(context, impexPath + "women-products.impex", false);
            importImpexFile(context, impexPath + "body-beauty-hot-products.impex", false);
            importImpexFile(context, impexPath + "product_media.impex", false);
            importImpexFile(context, impexPath + "baby-products_with_images.impex", false);
            importImpexFile(context, impexPath + "physical-giftcard-products.impex", false);
            importImpexFile(context, impexPath + "giftcard-products.impex", false);
            importImpexFile(context, impexPath + "giftcard-media.impex", false);
            importImpexFile(context, impexPath + "searchable-products.impex", false);
            importImpexFile(context, impexPath + "sortable-search-products.impex", false);
            importImpexFile(context, impexPath + "test-category.impex", false);
            importImpexFile(context, impexPath + "test-deal-colour-variant-products.impex", false);
            importImpexFile(context, impexPath + "automationTest/testProducts.impex", false);
            importImpexFile(context, impexPath + "automationTest/automation-fetch-products-setup.impex", false);

            // Load Products Relations
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/products-relations.impex", false);

            // Load Prices
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/products-prices.impex", false);

            // Product - Merch Department relation
            // similar to prod departments
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/target-merch-department-data.impex", false);
            // unique departments for testing purposes
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/products-merchdepartment.impex", false);

            // Load TMD (and is categories)
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/tmd-prefix.impex", false);
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/promotions-tmd.impex", false);

            // Sample deals
            importImpexFile(context, impexPath + "products-deals.impex", false);
            importImpexFile(context, impexPath + "automationTest/generic-deals.impex", false);

            // map odbms departments to Rakuten data for affiliate offers
            importImpexFile(context, "/" + importDirectory + "/import/productCatalogs/" + catalogName
                    + "ProductCatalog/affiliateoffers/affiliateoffers-category.impex", false);

            // Run this one in legacy mode to bypass the unique requirement of qualifier ID
            // Cloned deals clone the qualifier as well, but retain the same ID.
            importImpexFile(context, impexPath + "automationTest/contract-testing-deals.impex", false, true);

            importImpexFile(context, impexPath + "automationTest/contract-testing-vouchers.impex", false);

            importImpexFile(context, impexPath + "automationTest/contract-testing-vouchers-enable.impex", false);

            importImpexFile(context, impexPath + "automationTest/contract-testing-pos-products.impex", false);

            importImpexFile(context, impexPath + "automationTest/contract-testing-giftcards.impex", false);

            importImpexFile(context, impexPath + "automationTest/contract-testing-giftcard-products.impex", false);

            importImpexFile(context, impexPath + "automationTest/contract-testing-giftcard-media.impex", false);

            initializeProductsDepartmentDeliveryMode.performProcessJob();

        }
    }

    protected boolean synchronizeProductCatalog(final SystemSetupContext context, final String catalogName,
            final boolean sync) {

        logInfo(context, "Begin synchronizing Product Catalog [" + catalogName + "] - "
                + (sync ? "synchronizing" : "initializing job"));

        createProductCatalogSyncJob(context, catalogName + "ProductCatalog");

        boolean result = true;

        if (sync) {
            final PerformResult syncCronJobResult = executeCatalogSyncJob(context, catalogName + "ProductCatalog");
            if (isSyncRerunNeeded(syncCronJobResult)) {
                logInfo(context, "Product catalog [" + catalogName + "] sync has issues.");
                result = false;
            }
        }

        logInfo(context, "Done " + (sync ? "synchronizing" : "initializing job") + " Product Catalog [" + catalogName
                + "]");


        return result;
    }

    private void importCMSData(final SystemSetupContext context, final String catalogName) {
        logInfo(context, "Begin populating catalog [" + catalogName + "]");

        importImpexFile(context, SAMPLE_CONTENT_PATH + "home-page.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "support-pages.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "support-modals.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "corporate-pages.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "content-navigation.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "blog-pages.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "footer-content.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "donate-request-content.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "spc-page.impex", true);
        // TODO use dev-content.impex
        importImpexFile(context, SAMPLE_CONTENT_PATH + "devContent/style-guide.impex", true);

        // System pages runs after Content Navigation so
        // My Account landing page has access to FAQ links
        importImpexFile(context, SAMPLE_CONTENT_PATH + "system-pages.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "category-landing-pages.impex", true);

        // Import category links
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/categories-base.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/kids.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/baby.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/body-beauty.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/school.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/toys.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/women.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "categoryLinks/contentSlotForSearchEmptyPage.impex", true);

        // Import size chart test
        importImpexFile(context, SAMPLE_CONTENT_PATH + "size-chart-test.impex", true);

        importImpexFile(context, SAMPLE_CONTENT_PATH + "mobile-app/cms-mobile-app-page-setup.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "mobile-app/cms-smart-app-banners.impex",
                true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "mobile-app/mobile-app-home-screen.impex", true);

        synchronizeContentCatalog(context, catalogName, true);
    }


    private void importCMSTemplateTests(final SystemSetupContext context, final String catalogName) {

        logInfo(context, "Begin importing CMS Template Tests");

        // Import CMS test tools
        importImpexFile(context, SAMPLE_CONTENT_PATH + "cms-page-tests-template.impex", true);
        importImpexFile(context, SAMPLE_CONTENT_PATH + "cms-page-tests.impex", true);

        synchronizeContentCatalog(context, catalogName, true);
    }

    private void importKioskSampleData(final SystemSetupContext context, final String catalogName) {
        logInfo(context, "Begin importing Kiosk Sample Data");
        importImpexFile(context, KIOSK_SAMPLE_DATA_PATH + "kiosk-landing-page.impex", true);
        importImpexFile(context, KIOSK_SAMPLE_DATA_PATH + "pinpad-content.impex", true);

        synchronizeContentCatalog(context, catalogName, true);
    }

    protected boolean synchronizeContentCatalog(final SystemSetupContext context, final String catalogName,
            final boolean sync) {
        logInfo(context, "Begin synchronizing Content Catalog [" + catalogName + "] - "
                + (sync ? "synchronizing" : "initializing job"));

        createContentCatalogSyncJob(context, catalogName);

        boolean result = true;

        if (sync) {
            final PerformResult syncCronJobResult = executeCatalogSyncJob(context, catalogName);
            if (isSyncRerunNeeded(syncCronJobResult)) {
                logInfo(context, "Catalog catalog [" + catalogName + "] sync has issues.");
                result = false;
            }
        }

        logInfo(context, "Done " + (sync ? "synchronizing" : "initializing job") + " Content Catalog [" + catalogName
                + "]");
        return result;
    }

    /**
     * Imports Common Data
     */
    protected void importCommonData(final SystemSetupContext context, final String importDirectory) {
        logInfo(context, "Importing Common Data...");

        final String importRoot = "/" + importDirectory + "/import";

        importImpexFile(context, importRoot + "/common/user-groups.impex", false);
        importImpexFile(context, importRoot + "/common/promotions.impex", false);
        importImpexFile(context, importRoot + "/common/feature-switches.impex", false);
        importImpexFile(context, importRoot + "/common/giftcards.impex", false);
        importImpexFile(context, importRoot + "/common/size-types.impex", false);
        importImpexFile(context, importRoot + "/common/sizegroup.impex", false);
        importImpexFile(context, importRoot + "/common/contract-testing-social-media-products.impex", false);
        importImpexFile(context, importRoot + "/common/targetSharedConfig.impex", false);
        importImpexFile(context, importRoot + "/personalisation/user-segments.impex", false);
        importImpexFile(context, importRoot + "/personalisation/customers-segmented.impex", false);


        final List<String> extensionNames = Registry.getCurrentTenant().getTenantSpecificExtensionNames();
        if (extensionNames.contains("cmscockpit")) {
            importImpexFile(context, importRoot + "/cockpits/cmscockpit/cmscockpit-users.impex");
        }

        if (extensionNames.contains("productcockpit")) {
            importImpexFile(context, importRoot + "/cockpits/productcockpit/productcockpit-users.impex");
        }

        if (extensionNames.contains("reportcockpit")) {
            importImpexFile(context, importRoot + "/cockpits/reportcockpit/reportcockpit-users.impex");
            importImpexFile(context, importRoot + "/cockpits/reportcockpit/reportcockpit-mcc-links.impex");
        }
    }


    private void removeAllStoreHours(final SystemSetupContext context) {
        logInfo(context, "Removing all store hours...");

        final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");
        final FlexibleSearchService flexibleSearchService = (FlexibleSearchService)Registry.getApplicationContext()
                .getBean("flexibleSearchService");

        final SearchResult<OpeningDayModel> result = flexibleSearchService.<OpeningDayModel> search("SELECT "
                + OpeningDayModel.PK + " FROM {" + OpeningDayModel._TYPECODE + "}");
        final List<OpeningDayModel> allOpeningDays = result.getResult();
        if (CollectionUtils.isNotEmpty(allOpeningDays)) {
            modelService.removeAll(allOpeningDays);
        }

        logInfo(context, "Store hours removed.");
    }

    private void createStoreHours(final SystemSetupContext context) {
        logInfo(context, "Creating store hours for Target and Target Country...");

        final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

        final FlexibleSearchService flexibleSearchService = (FlexibleSearchService)Registry.getApplicationContext()
                .getBean("flexibleSearchService");
        final TargetOpeningDayService targetOpeningDayService = (TargetOpeningDayService)Registry
                .getApplicationContext().getBean("targetOpeningDayService");
        final LocalDate startOfThisWeek = LocalDate.now().withDayOfWeek(DateTimeConstants.MONDAY);

        final LocalTime targetOpeningTime = new LocalTime(8, 0, 0);
        final LocalTime targetClosingTime = new LocalTime(20, 0, 0);
        final LocalTime targetCountryOpeningTime = new LocalTime(9, 0, 0);
        final LocalTime targetCountryClosingTime = new LocalTime(17, 0, 0);


        final OpeningScheduleModel targetOpeningScheduleExample = new OpeningScheduleModel();
        targetOpeningScheduleExample.setCode("target-hours-0");
        final OpeningScheduleModel targetOpeningSchedule = flexibleSearchService
                .getModelByExample(targetOpeningScheduleExample);

        final OpeningScheduleModel targetCountryOpeningScheduleExample = new OpeningScheduleModel();
        targetCountryOpeningScheduleExample.setCode("target-country-hours-0");
        final OpeningScheduleModel targetCountryOpeningSchedule = flexibleSearchService
                .getModelByExample(targetCountryOpeningScheduleExample);

        for (int i = 0; i < 14; i++) {

            final TargetOpeningDayModel targetOpeningDay = modelService.create(TargetOpeningDayModel.class);
            targetOpeningDay.setOpeningSchedule(targetOpeningSchedule);
            targetOpeningDay.setOpeningTime(startOfThisWeek.plusDays(i).toDateTime(targetOpeningTime).toDate());
            targetOpeningDay.setClosingTime(startOfThisWeek.plusDays(i).toDateTime(targetClosingTime).toDate());
            targetOpeningDay.setTargetOpeningDayId(targetOpeningDayService.getTargetOpeningDayId(DUMMY_STORE + i,
                    targetOpeningDay.getOpeningTime()));
            modelService.save(targetOpeningDay);

            final TargetOpeningDayModel targetCountryOpeningDay = modelService.create(TargetOpeningDayModel.class);
            targetCountryOpeningDay.setOpeningSchedule(targetCountryOpeningSchedule);
            targetCountryOpeningDay.setOpeningTime(startOfThisWeek.plusDays(i).toDateTime(targetCountryOpeningTime)
                    .toDate());
            targetCountryOpeningDay.setClosingTime(startOfThisWeek.plusDays(i).toDateTime(targetCountryClosingTime)
                    .toDate());
            targetCountryOpeningDay.setTargetOpeningDayId(targetOpeningDayService.getTargetOpeningDayId(
                    DUMMY_STORE_COUNTRY + i, targetOpeningDay.getOpeningTime()));
            modelService.save(targetCountryOpeningDay);
        }

        logInfo(context, "Store hours created.");
    }



    /**
     * @param initializeProductsDepartmentDeliveryMode
     *            the initializeProductsDepartmentDeliveryMode to set
     */
    @Required
    public void setInitializeProductsDepartmentDeliveryMode(
            final InitializeProductsDepartmentDeliveryMode initializeProductsDepartmentDeliveryMode) {
        this.initializeProductsDepartmentDeliveryMode = initializeProductsDepartmentDeliveryMode;
    }


}
