/**
 * 
 */
package au.com.target.tgtlayby.order.strategies.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * Unit test for {@link AllowPaymentDueReplaceOrderDenialStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AllowPaymentDueReplaceOrderDenialStrategyTest {
    private final AllowPaymentDueReplaceOrderDenialStrategy replaceOrderDenialStrategy = new AllowPaymentDueReplaceOrderDenialStrategy();
    private OrderModel order;

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        order = Mockito.mock(OrderModel.class);
    }

    /**
     * test null order
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNullOrder() {
        replaceOrderDenialStrategy.isDenied(null);
    }

    /**
     * 
     */
    @Test
    public void testIsDeniedForNotBuynow() {
        final PurchaseOptionConfigModel config = Mockito.mock(PurchaseOptionConfigModel.class);
        when(config.getAllowPaymentDues()).thenReturn(Boolean.TRUE);
        when(order.getPurchaseOptionConfig()).thenReturn(config);
        final boolean denied = replaceOrderDenialStrategy.isDenied(order);
        Assert.assertTrue(denied);
    }

    /**
     * 
     */
    @Test
    public void testIsDeniedForBuyNow() {
        final PurchaseOptionConfigModel config = Mockito.mock(PurchaseOptionConfigModel.class);
        when(config.getAllowPaymentDues()).thenReturn(Boolean.FALSE);
        when(order.getPurchaseOptionConfig()).thenReturn(config);
        final boolean denied = replaceOrderDenialStrategy.isDenied(order);
        Assert.assertFalse(denied);
    }
}
