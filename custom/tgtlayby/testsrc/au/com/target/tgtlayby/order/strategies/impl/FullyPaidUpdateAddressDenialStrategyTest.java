/**
 * 
 */
package au.com.target.tgtlayby.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FullyPaidUpdateAddressDenialStrategyTest {
    @InjectMocks
    private final FullyPaidUpdateAddressDenialStrategy strategy = new FullyPaidUpdateAddressDenialStrategy();

    @Mock
    private OrderOutstandingStrategy orderOutstandingStrategy;

    @Test(expected = IllegalArgumentException.class)
    public void testIsDeniedNullOrder() {
        strategy.isDenied(null);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsDeniedFullyPaid() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(orderOutstandingStrategy.calculateAmt(order)).willReturn(Double.valueOf(0.0));
        Assert.assertTrue(strategy.isDenied(order));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsDeniedNotFullyPaid() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(orderOutstandingStrategy.calculateAmt(order)).willReturn(Double.valueOf(10.0));
        Assert.assertFalse(strategy.isDenied(order));
    }

}
