/**
 * 
 */
package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PaymentDueConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("boxing")
public class PaymentDueConfigHmcLabelProviderTest {

    @Mock
    private PaymentDueConfigModel model;

    @InjectMocks
    @Spy
    private final PaymentDueConfigHmcLabelProvider provider = new PaymentDueConfigHmcLabelProvider();

    @Test
    public void testInitialPayment() {
        BDDMockito.given(model.getDueDate()).willReturn(null);
        BDDMockito.given(model.getDueDays()).willReturn(null);
        BDDMockito.given(model.getDuePercentage()).willReturn(10.0);
        final String result = provider.get(model);
        Assert.assertEquals("Initial payment of 10.0% is due", result);
    }

    @Test
    public void testIntermediatePayment() {
        BDDMockito.given(model.getDueDate()).willReturn(null);
        BDDMockito.given(model.getDueDays()).willReturn(28);
        BDDMockito.given(model.getDuePercentage()).willReturn(50.0);
        final String result = provider.get(model);
        Assert.assertEquals("50.0% is due in 28 days", result);
    }

    @Test
    public void testFinalPayment() throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final Date date = sdf.parse("01/08/2013");
        BDDMockito.given(model.getDueDate()).willReturn(date);
        BDDMockito.given(model.getDueDays()).willReturn(null);
        BDDMockito.given(model.getDuePercentage()).willReturn(100.0);
        final String result = provider.get(model);
        Assert.assertEquals("100.0% is due on 01/08/2013", result);
    }


    @Test
    public void testFormatDate() throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date date = sdf.parse("17/06/2013 00:00:00");
        final String result = provider.formatDate(date);
        Assert.assertEquals("17/06/2013", result);
    }
}
