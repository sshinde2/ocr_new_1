package au.com.target.tgtlayby.ordercancel.impl.denialstrategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * Unit test for {@link PurchaseOptionConfigAwareDenialStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PurchaseOptionConfigAwareDenialStrategyTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    private final PurchaseOptionConfigAwareDenialStrategy purchaseOptionConfigAwareDenialStrategy = new PurchaseOptionConfigAwareDenialStrategy();

    @Mock
    private OrderCancelDenialReason orderCancelDenialReason;

    @Mock
    private OrderModel orderModel;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModel;

    @Test
    public void testGetCancelDenialReasonForNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderModel cannot be null");
        purchaseOptionConfigAwareDenialStrategy.getCancelDenialReason(null, null, null, false, false);
    }

    @Test
    public void testGetCancelDenialReasonForOrderWithNOPOConfig() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No PurchaseOptionConfig found on the order");
        purchaseOptionConfigAwareDenialStrategy.getCancelDenialReason(null, orderModel, null, false, false);
    }

    @Test
    public void testGetCancelDenialReasonForOrderWithPOConfigNullCancellableFlag() {
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        BDDMockito.given(purchaseOptionConfigModel.getAllowOrderCancellation()).willReturn(null);

        final OrderCancelDenialReason result = purchaseOptionConfigAwareDenialStrategy.getCancelDenialReason(null,
                orderModel, null, false, false);

        Assert.assertNotNull(result);
        Assert.assertEquals(result, orderCancelDenialReason);
    }

    @Test
    public void testGetCancelDenialReasonForOrderWithPOConfigFalseCancellableFlag() {
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);

        final OrderCancelDenialReason result = purchaseOptionConfigAwareDenialStrategy.getCancelDenialReason(null,
                orderModel, null, false, false);

        Assert.assertNotNull(result);
        Assert.assertEquals(result, orderCancelDenialReason);
    }

    @Test
    public void testGetCancelDenialReasonForOrderWithPOConfigAllowingCancellation() {
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        BDDMockito.given(purchaseOptionConfigModel.getAllowOrderCancellation()).willReturn(Boolean.TRUE);

        final OrderCancelDenialReason result = purchaseOptionConfigAwareDenialStrategy.getCancelDenialReason(null,
                orderModel, null, false, false);

        Assert.assertNull(result);
    }
}
