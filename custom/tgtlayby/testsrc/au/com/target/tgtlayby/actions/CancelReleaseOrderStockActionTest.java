/**
 * 
 */
package au.com.target.tgtlayby.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CancelReleaseOrderStockActionTest {

    @Mock
    private ProductModel product;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private OrderCancelRequest orderCancelRequest;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private BusinessProcessParameterModel bppm;

    @Mock
    private OrderEntryModel entry;

    @Mock
    private OrderEntryCancelRecordEntryModel cancelEntry;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @InjectMocks
    private final CancelReleaseOrderStockAction action = new CancelReleaseOrderStockAction();

    @Test(expected = IllegalArgumentException.class)
    public void testStockReleaseNullParameters() throws RetryLaterException, Exception {
        // final int amount = 10;
        BDDMockito.given(orderProcessParameterHelper.getOrderCancelRequest(process)).willReturn(null);
        action.executeAction(process);
        // Mockito.verify(targetStockService, Mockito.never()).release(product, purchaseOption, amount, "testing");
    }

    @Test
    public void testStockReleaseFullCancel() throws RetryLaterException, Exception {
        final List<AbstractOrderEntryModel> list = new ArrayList<>();
        list.add(entry);
        BDDMockito.given(order.getEntries()).willReturn(list);
        BDDMockito.given(entry.getProduct()).willReturn(product);
        BDDMockito.given(order.getPurchaseOption()).willReturn(purchaseOption);
        final long amount = 10;
        BDDMockito.given(entry.getQuantity()).willReturn(Long.valueOf(amount));
        final int qty = (int)amount;
        BDDMockito.given(product.getName()).willReturn("Test");
        action.releaseAllStockForOrder(order);
        Mockito.verify(targetStockService).release(product, qty,
                amount + " stock has been released for "
                        + product.getName() + " from order number is null");
    }

    @Test
    public void testStockReleasePartialCancel() throws TargetUnknownIdentifierException {
        final List<OrderEntryModificationRecordEntryModel> list = new ArrayList<>();
        list.add(cancelEntry);
        BDDMockito.given(cancelEntry.getOrderEntry()).willReturn(entry);
        BDDMockito.given(entry.getProduct()).willReturn(product);
        BDDMockito.given(order.getPurchaseOption()).willReturn(purchaseOption);
        final Integer amount = Integer.valueOf(10);
        BDDMockito.given(cancelEntry.getCancelRequestQuantity()).willReturn(amount);
        final int qty = amount.intValue();
        BDDMockito.given(product.getName()).willReturn("Test");
        action.releaseStockForLaybyCancel(order, list);
        Mockito.verify(targetStockService).release(product, qty,
                amount + " stock has been released for "
                        + product.getName() + " from order number is null");
    }

}
