/**
 * 
 */
package au.com.target.tgtlayby.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import junit.framework.Assert;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckOrderStatusActionTest {

    private static final Integer NUM_RETRIES = Integer.valueOf(3);
    private static final Integer RETRY_INTERVAL = Integer.valueOf(28800000);

    @Mock
    private OrderProcessModel process;
    @Mock
    private OrderModel order;
    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntry;
    @Mock
    private PaymentTransactionEntryModel retryPaymentTransactionEntry;
    @Mock
    private StandardPaymentModeModel standardPaymentModeModel;
    @Mock
    private PurchaseOptionConfigService purchaseOptionConfigService;
    @Mock
    private TargetPaymentService targetPaymentService;
    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;
    @Mock
    private ModelService modelService;
    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    private ZippayPaymentInfoModel zippayPaymentInfoModel;

    @InjectMocks
    @Spy
    private final CheckOrderStatusAction action = new CheckOrderStatusAction();

    @Before
    public void setup() {
        given(process.getOrder()).willReturn(order);
        given(orderProcessParameterHelper.getPaymentTransactionEntry(process)).willReturn(
                paymentTransactionEntry);

        // By default there is a payment entry of type CAPTURE
        given(paymentTransactionEntry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(order.getCode()).willReturn("test");
        given(order.getCurrency()).willReturn(new CurrencyModel());
        given(action.getModelService()).willReturn(modelService);

        // Set retry values in the payment mode - the values are mandatory so no need to check for nulls
        given(order.getPaymentMode()).willReturn(standardPaymentModeModel);
        given(standardPaymentModeModel.getAllowedPaymentRetries()).willReturn(NUM_RETRIES);
        given(standardPaymentModeModel.getPaymentRetryIntervalMillis()).willReturn(RETRY_INTERVAL);


    }

    @Test
    public void testGetTransitionForPaymentAccepted() throws InterruptedException, TargetUnknownIdentifierException,
            RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("ACCEPTED");
        given(retryPaymentTransactionEntry.getAmount()).willReturn(BigDecimal.ZERO);
        final String result = action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry,
                order, process);
        assertEquals("OK", result);
    }

    @Test(expected = RetryLaterException.class)
    public void testGetTransitionForPaymentReview() throws InterruptedException, TargetUnknownIdentifierException,
            RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("REVIEW");
        action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry, order, process);
    }

    @Test
    public void testGetTransitionForPaymentRejected() throws InterruptedException, TargetUnknownIdentifierException,
            RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("REJECTED");
        final String result = action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry,
                order, process);
        assertEquals("REJECTED", result);
    }

    @Test
    public void testGetTransitionForPaymentError() throws InterruptedException, TargetUnknownIdentifierException,
        RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("ERROR");
        final String result = action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry,
            order, process);
        assertEquals("REJECTED", result);
    }
    @Test
    public void testForNoPTEM() throws RetryLaterException, Exception {
        // In this case there is no PTEM
        given(orderProcessParameterHelper.getPaymentTransactionEntry(process)).willReturn(null);
        final String result = action.executeInternal(process);
        verifyZeroInteractions(targetPaymentService);
        assertEquals("OK", result);
    }

    @Test
    public void testRetryForNonPendingPayment() throws RetryLaterException, Exception {
        // In this case there is no entry in REVIEW status
        given(paymentTransactionEntry.getTransactionStatus()).willReturn("ACCEPTED");
        given(paymentTransactionEntry.getAmount()).willReturn(BigDecimal.ZERO);
        final String result = action.executeInternal(process);
        verifyZeroInteractions(targetPaymentService);
        Assert.assertEquals("OK", result);
    }

    @Test(expected = RetryLaterException.class)
    public void testRetryWhenTransactionEntryExists() throws RetryLaterException, Exception {

        given(paymentTransactionEntry.getTransactionStatus()).willReturn("REVIEW");

        final String result = action.executeInternal(process);

        verify(targetPaymentService).retrieveTransactionEntry(paymentTransactionEntry);
        assertEquals("OK", result);
    }

    @Test(expected = RetryLaterException.class)
    public void testRecaptureForZipPayment() throws Exception {
        given(paymentTransactionEntry.getTransactionStatus()).willReturn("REVIEW");
        given(order.getPaymentInfo()).willReturn(zippayPaymentInfoModel);

        final String result = action.executeInternal(process);
        verify(targetPaymentService,never()).retrieveTransactionEntry(paymentTransactionEntry);
        assertEquals("OK", result);
    }

    @Test
    public void testGetAllowedRetries() {

        assertEquals(NUM_RETRIES, Integer.valueOf(action.getAllowedRetries(process)));
    }

    @Test
    public void testGetRetryInterval() {

        assertEquals(RETRY_INTERVAL, Integer.valueOf(action.getRetryInterval(process)));
    }

    @Test
    public void testGetTransitionForPaymentInsufficient() throws InterruptedException,
            TargetUnknownIdentifierException,
            RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("ACCEPTED");
        given(order.getTotalPrice()).willReturn(Double.valueOf(15.00));
        given(retryPaymentTransactionEntry.getAmount()).willReturn(BigDecimal.valueOf(14.99));
        final String result = action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry,
                order, process);
        assertEquals("INSUFFICIENT", result);
    }

    @Test
    public void testGetTransitionForPreOrderPaymentInsufficientGreaterThanThreshold() throws InterruptedException,
            TargetUnknownIdentifierException,
            RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("ACCEPTED");
        given(retryPaymentTransactionEntry.getAmount()).willReturn(BigDecimal.valueOf(9.99));

        given(order.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(order.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.00));

        final String result = action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry,
                order, process);
        assertThat(result).isEqualTo("INSUFFICIENT");
    }

    @Test
    public void testGetTransitionForPreOrderPaymentInsufficientLessThanThreshold() throws InterruptedException,
            TargetUnknownIdentifierException,
            RetryLaterException {
        given(retryPaymentTransactionEntry.getTransactionStatus()).willReturn("ACCEPTED");
        given(retryPaymentTransactionEntry.getAmount()).willReturn(BigDecimal.valueOf(9.996));

        given(order.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(order.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.00));

        final String result = action.doActionAndGetTransitionBasedOnTransactionStatus(retryPaymentTransactionEntry,
                order, process);
        assertThat(result).isEqualTo("OK");
    }

    @Test
    public void testExecuteInternalPartnerOrders() throws Exception {
        doReturn(Boolean.TRUE).when(salesApplicationConfigService).isSkipFraudCheck(SalesApplication.EBAY);
        doReturn(SalesApplication.EBAY).when(order).getSalesApplication();

        assertEquals(CheckOrderStatusAction.Transition.OK.toString(), action.executeInternal(process));
    }

}
