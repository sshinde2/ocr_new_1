/**
 * 
 */
package au.com.target.tgtlayby.order.strategies.impl;

import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.strategies.UpdateAddressDenialStrategy;
import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;


/**
 * @author dcwillia
 * 
 */
public class FullyPaidUpdateAddressDenialStrategy implements UpdateAddressDenialStrategy {
    private OrderOutstandingStrategy orderOutstandingStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        Assert.notNull(order);
        return orderOutstandingStrategy.calculateAmt(order) == 0;
    }

    /**
     * @param orderOutstandingStrategy
     *            the orderOutstandingStrategy to set
     */
    @Required
    public void setOrderOutstandingStrategy(final OrderOutstandingStrategy orderOutstandingStrategy) {
        this.orderOutstandingStrategy = orderOutstandingStrategy;
    }
}
