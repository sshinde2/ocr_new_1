/**
 * 
 */
package au.com.target.tgtlayby.purchase.dao;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * 
 * Purchase Option Config DAO
 * 
 */
public interface PurchaseOptionConfigDao {

    /**
     * Get Purchase Option by code
     * 
     * @param code
     * @return PurchaseOptionConfigModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    PurchaseOptionConfigModel getPurchaseOptionConfigByCode(String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * Get Purchase Option by purchaseOption
     * 
     * @param purchaseOptionModel
     * @return PurchaseOptionConfigModel
     * @throws TargetUnknownIdentifierException
     */
    PurchaseOptionConfigModel getActivePurchaseOptionConfigByPurchaseOption(PurchaseOptionModel purchaseOptionModel)
            throws TargetUnknownIdentifierException;

    /**
     * get the active purchase option config at the time a order was placed
     * 
     * @param purchaseOption
     * @param orderDate
     * @return PurchaseOptionConfigModel
     * @throws TargetUnknownIdentifierException
     */
    PurchaseOptionConfigModel getPurchaseOptionConfigForOrderPlacedDate(PurchaseOptionModel purchaseOption,
            Date orderDate) throws TargetUnknownIdentifierException;

    /**
     * Get All Purchase Option Configs
     * 
     * @param purchaseOption
     * @return {@link List}
     */
    List<PurchaseOptionConfigModel> getAllPurchaseOptionConfigs(PurchaseOptionModel purchaseOption);
}
