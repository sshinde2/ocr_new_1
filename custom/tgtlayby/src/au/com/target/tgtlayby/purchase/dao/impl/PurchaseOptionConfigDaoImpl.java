/**
 * 
 */
package au.com.target.tgtlayby.purchase.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao;



public class PurchaseOptionConfigDaoImpl extends DefaultGenericDao implements PurchaseOptionConfigDao {

    private static final Logger LOG = Logger.getLogger(PurchaseOptionConfigDaoImpl.class);

    private static final String QUERY_ACTIVE_PURCHASE_OPTION_CONFIG = "SELECT {" + PurchaseOptionConfigModel.PK
            + "} FROM {" + PurchaseOptionConfigModel._TYPECODE + "} WHERE {" + PurchaseOptionConfigModel.PURCHASEOPTION
            + "} = ?purchaseOption AND ({" + PurchaseOptionConfigModel.VALIDFROM + "} IS NULL OR {"
            + PurchaseOptionConfigModel.VALIDFROM + "} <= ?today) AND ({"
            + PurchaseOptionConfigModel.VALIDTO + "} IS NULL OR {" + PurchaseOptionConfigModel.VALIDTO
            + "}  >= ?today)";

    private static final String QUERY_PURCHASE_OPTION_CONFIG_FOR_ORDER_DATE = "SELECT {" + PurchaseOptionConfigModel.PK
            + "} FROM {" + PurchaseOptionConfigModel._TYPECODE + "} WHERE {" + PurchaseOptionConfigModel.PURCHASEOPTION
            + "} = ?purchaseOption AND ({" + PurchaseOptionConfigModel.VALIDFROM + "} IS NULL OR {"
            + PurchaseOptionConfigModel.VALIDFROM + "} <= ?orderDate) AND ({"
            + PurchaseOptionConfigModel.VALIDTO + "} IS NULL OR {" + PurchaseOptionConfigModel.VALIDTO
            + "}  >= ?orderDate)";

    public PurchaseOptionConfigDaoImpl() {
        super(PurchaseOptionConfigModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao#getPurchaseOptionConfignByCode(java.lang.String)
     */
    @Override
    public PurchaseOptionConfigModel getPurchaseOptionConfigByCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Map<String, String> params = new HashMap<String, String>();
        params.put(PurchaseOptionConfigModel.CODE, code);
        final List<PurchaseOptionConfigModel> configs = find(params);

        TargetServicesUtil.validateIfSingleResult(configs, PurchaseOptionConfigModel.class,
                PurchaseOptionConfigModel.CODE, code);

        return configs.get(0);

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao#getActivePurchaseOptionConfigByPurchaseOption(java.lang.String)
     */
    @Override
    public PurchaseOptionConfigModel getActivePurchaseOptionConfigByPurchaseOption(
            final PurchaseOptionModel purchaseOptionModel)
            throws TargetUnknownIdentifierException {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(PurchaseOptionConfigModel.PURCHASEOPTION, purchaseOptionModel);
        params.put("today", DateUtils.round(new Date(), Calendar.MINUTE)); // round to give some hope of database optimisation
        return getPurchaseOptionConfigForParamsAndQuery(QUERY_ACTIVE_PURCHASE_OPTION_CONFIG, params);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao#getPurchaseOptionConfigForOrderPlacedDate(au.com.target.tgtlayby.model.PurchaseOptionModel, java.util.Date)
     */
    @Override
    public PurchaseOptionConfigModel getPurchaseOptionConfigForOrderPlacedDate(
            final PurchaseOptionModel purchaseOption,
            final Date orderDate) throws TargetUnknownIdentifierException {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(PurchaseOptionConfigModel.PURCHASEOPTION, purchaseOption);
        params.put("orderDate", DateUtils.round(orderDate, Calendar.MINUTE)); // round to give some hope of database optimisation
        return getPurchaseOptionConfigForParamsAndQuery(QUERY_PURCHASE_OPTION_CONFIG_FOR_ORDER_DATE, params);
    }

    /**
     * based on a query and a map of parameters return the purchase option config
     * 
     * @param query
     * @param params
     * @return purchase option config
     * @throws TargetUnknownIdentifierException
     */
    protected PurchaseOptionConfigModel getPurchaseOptionConfigForParamsAndQuery(final String query,
            final Map<String, Object> params) throws TargetUnknownIdentifierException {
        final SearchResult<PurchaseOptionConfigModel> searchResult = getFlexibleSearchService().search(
                query, params);
        final List<PurchaseOptionConfigModel> resultList = searchResult.getResult();

        if (CollectionUtils.isEmpty(resultList)) {
            throw new TargetUnknownIdentifierException("Can't find valid PurchaseOptionConfig Model");
        }

        if (resultList.size() > 1) {
            LOG.error("More than 1 PurchaseOptionConfig was found");
        }
        return resultList.get(0);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao#getAllPurchaseOptionConfigs(au.com.target.tgtlayby.model.PurchaseOptionModel)
     */
    @Override
    public List<PurchaseOptionConfigModel> getAllPurchaseOptionConfigs(final PurchaseOptionModel purchaseOption) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(PurchaseOptionConfigModel.PURCHASEOPTION, purchaseOption);
        return find(params);
    }
}
