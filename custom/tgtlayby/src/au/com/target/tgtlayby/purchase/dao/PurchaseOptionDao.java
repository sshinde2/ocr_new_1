/**
 * 
 */
package au.com.target.tgtlayby.purchase.dao;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


public interface PurchaseOptionDao {
    /**
     * get purchase option by code
     * 
     * @param code
     * @return Purchase option model
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    PurchaseOptionModel getPurchaseOptionForCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * get purchase option by name
     * 
     * @param name
     * @return Purchase option model
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    PurchaseOptionModel getPurchaseOptionForName(final String name) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * get all purchase options
     * 
     * @return list
     */
    List<PurchaseOptionModel> getAllPurchaseOptions();

}
