/**
 * 
 */
package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * Dynamic attribute handler for the AbstractOrder.purchaseOption attribute.<br/>
 * Just retrieves the purchaseOption from the related purchaseOptionConfig.<br/>
 * Returns null if the PurchaseOptionConfig is null.
 */
public class OrderPurchaseOption extends
        AbstractDynamicAttributeHandler<PurchaseOptionModel, AbstractOrderModel> {

    @Override
    public PurchaseOptionModel get(final AbstractOrderModel order) {

        Assert.notNull(order, "AbstractOrderModel must not be null");

        final PurchaseOptionConfigModel config = order.getPurchaseOptionConfig();

        if (config == null) {
            return null;
        }

        return config.getPurchaseOption();
    }

}
