/**
 * 
 */
package au.com.target.tgtlayby.cart.data;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.util.PriceValue;


/**
 * Contains information about a particular delivery mode for a cart.
 * 
 */
public class DeliveryModeInformation {

    // An available delivery mode
    private final DeliveryModeModel deliveryMode;

    private final ZoneDeliveryModeValueModel deliveryModeValue;

    // The fee for this delivery mode
    private final PriceValue deliveryFee;

    public DeliveryModeInformation(final DeliveryModeModel deliveryMode,
            final ZoneDeliveryModeValueModel deliveryModeValue, final PriceValue deliveryFee) {
        this.deliveryMode = deliveryMode;
        this.deliveryModeValue = deliveryModeValue;
        this.deliveryFee = deliveryFee;
    }


    /**
     * @return the deliveryMode
     */
    public DeliveryModeModel getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * @return the deliveryFee
     */
    public PriceValue getDeliveryFee() {
        return deliveryFee;
    }

    /**
     * @return the deliveryModeValue
     */
    public ZoneDeliveryModeValueModel getDeliveryModeValue() {
        return deliveryModeValue;
    }

}
