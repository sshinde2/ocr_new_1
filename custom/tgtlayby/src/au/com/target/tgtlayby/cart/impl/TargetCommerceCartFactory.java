/**
 * 
 */
package au.com.target.tgtlayby.cart.impl;

import de.hybris.platform.commerceservices.order.impl.CommerceCartFactory;
import de.hybris.platform.core.model.order.CartModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;


public class TargetCommerceCartFactory extends CommerceCartFactory {

    private PurchaseOptionConfigService purchaseOptionConfigService;


    /*
     * will be called to create a new session cart if one does not exist already
     */
    @Override
    protected CartModel createCartInternal()
    {
        final CartModel cart = super.createCartInternal();
        final PurchaseOptionConfigModel config =
                purchaseOptionConfigService.getCurrentPurchaseOptionConfig();
        cart.setPurchaseOptionConfig(config);
        return cart;
    }

    @Required
    public void setPurchaseOptionConfigService(final PurchaseOptionConfigService purchaseOptionConfigService) {
        this.purchaseOptionConfigService = purchaseOptionConfigService;
    }
}
