package au.com.target.tgtlayby.cart;

//CHECKSTYLE:OFF
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.servlet.http.HttpSession;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


//CHECKSTYLE:ON

/**
 * CartService which supports various cart specific operations based on {@link PurchaseOptionModel}
 */
public interface TargetLaybyCartService extends TargetCartService {

    /**
     * 
     * @param cart
     * @param product
     * @return {@link CartEntryModel}
     */
    CartEntryModel getEntryForProduct(CartModel cart, ProductModel product);

    /**
     * Return the most recent cart for the given customer
     * 
     * @param customer
     * @return {@link CartModel} or null if theres no cart for the customer
     */
    CartModel getMostRecentCartForCustomer(CustomerModel customer);

    /**
     * Get session cart by http session
     * 
     * @param session
     * @return CartModel
     */
    CartModel getCurrentSessionCartBySession(HttpSession session);
}
