package au.com.target.tgtwebcore.servicelayer.services.evaluator.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSalesChannelRestrictionModel;


/**
 * Unit test for {@link CMSSalesChannelRestrictionEvaluator}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSSalesChannelRestrictionEvaluatorTest {

    @InjectMocks
    private final CMSSalesChannelRestrictionEvaluator salesAppEvaluator = new CMSSalesChannelRestrictionEvaluator();

    @Mock
    private CMSSalesChannelRestrictionModel salesChannelModel;

    @Mock
    private RestrictionData restrictionData;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;


    @Test
    public void testEvaluateMobileSalesChannel() {
        final Collection<SalesApplication> salesApplications = new ArrayList<>();

        salesApplications.add(SalesApplication.MOBILEAPP);
        given(salesChannelModel.getSalesApplications()).willReturn(salesApplications);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);
        final boolean result = salesAppEvaluator.evaluate(salesChannelModel, restrictionData);
        Assert.assertTrue(result);
    }

    @Test
    public void testEvaluateOtherSalesChannel() {
        final Collection<SalesApplication> salesApplications = new ArrayList<>();

        salesApplications.add(SalesApplication.WEB);
        given(salesChannelModel.getSalesApplications()).willReturn(salesApplications);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        final boolean result = salesAppEvaluator.evaluate(salesChannelModel, restrictionData);
        Assert.assertTrue(result);
    }

    @Test
    public void testEvaluateEmptySalesChannel() {
        given(salesChannelModel.getSalesApplications()).willReturn(CollectionUtils.EMPTY_COLLECTION);
        final boolean result = salesAppEvaluator.evaluate(salesChannelModel, restrictionData);
        Assert.assertTrue(result);
    }

    @Test
    public void testEvaluateNullSalesChannel() {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(salesChannelModel.getSalesApplications()).willReturn(null);
        final boolean result = salesAppEvaluator.evaluate(salesChannelModel, restrictionData);
        Assert.assertTrue(result);
    }

    @Test
    public void testEvaluatKioskSalesChannel() {
        final Collection<SalesApplication> salesApplications = new ArrayList<>();

        salesApplications.add(SalesApplication.KIOSK);
        given(salesChannelModel.getSalesApplications()).willReturn(salesApplications);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        final boolean result = salesAppEvaluator.evaluate(salesChannelModel, restrictionData);
        Assert.assertFalse(result);
    }
}