/**
 *
 */
package au.com.target.tgtwebcore.media.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class TargetProductMediaWebURLStrategyTest {

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    //CHECKSTYLE:ON
    @Mock
    private MediaStorageConfigService.MediaFolderConfig mockConfig;

    @Mock
    private MediaSource mockMediaSource;


    private final TargetProductMediaWebURLStrategy localMediaStrategy = new TargetProductMediaWebURLStrategy() {
        @Override
        protected final String getTenantId() {
            return "master";
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        localMediaStrategy.setPrettyUrlEnabled(true);
    }

    @Test
    public void testGetDownloadUrlWithNullConfig() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Folder config is required to perform this operation");
        localMediaStrategy.getDownloadUrlForMedia(null, mockMediaSource);

        Mockito.verifyZeroInteractions(mockConfig);
        Mockito.verifyZeroInteractions(mockMediaSource);
    }

    @Test
    public void testGetDownloadUrlWithNullMediaSource() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("MediaSource is required to perform this operation");
        localMediaStrategy.getDownloadUrlForMedia(mockConfig, null);

        Mockito.verifyZeroInteractions(mockConfig);
        Mockito.verifyZeroInteractions(mockMediaSource);
    }

    @Test
    public void testGetDownloadUrlWithNullFolderQualifier() {
        Mockito.when(mockConfig.getFolderQualifier()).thenReturn(null);
        final String url = localMediaStrategy.getDownloadUrlForMedia(mockConfig, mockMediaSource);

        Assertions.assertThat(url).isEmpty();

        Mockito.verify(mockConfig).getFolderQualifier();
        Mockito.verifyNoMoreInteractions(mockConfig);
        Mockito.verifyZeroInteractions(mockMediaSource);
    }

    @Test
    public void testGetDownloadUrlWithNullLocation() {
        Mockito.when(mockConfig.getFolderQualifier()).thenReturn("root");
        Mockito.when(mockMediaSource.getLocation()).thenReturn(null);
        final String url = localMediaStrategy.getDownloadUrlForMedia(mockConfig, mockMediaSource);

        Assertions.assertThat(url).isEmpty();

        Mockito.verify(mockConfig).getFolderQualifier();
        Mockito.verify(mockMediaSource).getLocation();
        Mockito.verifyNoMoreInteractions(mockConfig);
        Mockito.verifyNoMoreInteractions(mockMediaSource);
    }

    @Test
    public void testGetDownloadUrl() {
        Mockito.when(mockConfig.getFolderQualifier()).thenReturn("root");
        Mockito.when(mockMediaSource.getLocation()).thenReturn(StringUtils.EMPTY);
        final String url = localMediaStrategy.getDownloadUrlForMedia(mockConfig, mockMediaSource);

        Assertions.assertThat(url).isEmpty();

        Mockito.verify(mockConfig).getFolderQualifier();
        Mockito.verify(mockMediaSource).getLocation();
        Mockito.verifyNoMoreInteractions(mockConfig);
        Mockito.verifyNoMoreInteractions(mockMediaSource);
    }

    @Test
    public void testGetDownloadUrlWithLocation() {
        Mockito.when(mockConfig.getFolderQualifier()).thenReturn("root");
        Mockito.when(mockMediaSource.getLocation()).thenReturn("/h12/34/1234.jpg");
        final String url = localMediaStrategy.getDownloadUrlForMedia(mockConfig, mockMediaSource);

        Assertions.assertThat(url).isNotEmpty().contains("?attachment=true");

        Mockito.verify(mockConfig).getFolderQualifier();
        Mockito.verify(mockMediaSource, Mockito.times(2)).getLocation();
        Mockito.verify(mockMediaSource).getRealFileName();
        Mockito.verifyNoMoreInteractions(mockConfig);
        Mockito.verifyNoMoreInteractions(mockMediaSource);
    }

}
