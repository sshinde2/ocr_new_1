/**
 *
 */
package au.com.target.tgtwebcore.media.url.impl;

import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.url.impl.LocalMediaWebURLStrategy;
import de.hybris.platform.util.MediaUtil;

import org.apache.commons.validator.GenericValidator;

import com.google.common.base.Preconditions;


/**
 * @author rsamuel3
 *
 */
public class TargetLocalMediaWebURLStrategy extends LocalMediaWebURLStrategy {

    @Override
    public String getUrlForMedia(final MediaStorageConfigService.MediaFolderConfig config, final MediaSource mediaSource)
    {
        Preconditions.checkArgument(config != null, "Folder config is required to perform this operation");
        Preconditions.checkArgument(mediaSource != null, "MediaSource is required to perform this operation");

        return assembleUrl(config.getFolderQualifier(), mediaSource);
    }

    protected String assembleUrl(final String folderQualifier, final MediaSource mediaSource)
    {
        String result = "";
        if ((!GenericValidator.isBlankOrNull(folderQualifier))
                && (!GenericValidator.isBlankOrNull(mediaSource.getLocation()))) {
            result = assembleLegacyURL(mediaSource);
        }
        return result;
    }

    protected String assembleLegacyURL(final MediaSource mediaSource)
    {
        final StringBuilder sb = new StringBuilder(MediaUtil.addTrailingFileSepIfNeeded(getMediaWebRootContext()));
        sb.append("sys_").append(getTenantId()).append("/");
        sb.append(mediaSource.getLocation());
        return sb.toString();
    }

    @Override
    public String getDownloadUrlForMedia(final MediaStorageConfigService.MediaFolderConfig config,
            final MediaSource mediaSource)
    {
        final StringBuilder url = new StringBuilder(getUrlForMedia(config, mediaSource));
        url.append("?").append("attachment").append("=").append("true");
        return url.toString();
    }

}
