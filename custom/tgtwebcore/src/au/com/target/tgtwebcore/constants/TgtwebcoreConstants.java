/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtwebcore.constants;

/**
 * Global class for all Tgtwebcore constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtwebcoreConstants extends GeneratedTgtwebcoreConstants {
    public static final String EXTENSIONNAME = "tgtwebcore";

    // implement here constants used by this extension
    public static final String LOG_IDENTIFIER_REQUEST_USERAGENT = "User-Agent";
    public static final String AFTERPAY_MODAL_ID = "afterpayModalPage";
    public static final String ZIPPAYMENT_MODAL_ID = "zipPaymentModalPage";

    private TgtwebcoreConstants() {
        //empty to avoid instantiating this constant class
    }

    public interface ProductTypes {
        public static final String MHD = "mhd";
    }
}
