package au.com.target.tgtwebcore.servicelayer.data;

import de.hybris.platform.cms2.servicelayer.data.CMSDataFactory;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;


public interface TargetCMSDataFactory extends CMSDataFactory {

    /**
     * Creates Restriction Data for BrandModel
     * 
     * @param brandModel
     * @return TargetCMSRestrictionData
     */
    TargetCMSRestrictionData createRestrictionData(BrandModel brandModel);

    /**
     * Creates Restriction Data for SizeTypeModel
     * 
     * @param sizeTypeModel
     * @return TargetCMSRestrictionData
     */
    TargetCMSRestrictionData createRestrictionData(SizeTypeModel sizeTypeModel);

}
