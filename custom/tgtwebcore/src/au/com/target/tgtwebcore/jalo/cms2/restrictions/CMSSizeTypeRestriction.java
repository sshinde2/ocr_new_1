package au.com.target.tgtwebcore.jalo.cms2.restrictions;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;

import au.com.target.tgtcore.jalo.SizeType;


public class CMSSizeTypeRestriction extends GeneratedCMSSizeTypeRestriction
{
    @SuppressWarnings("deprecation")
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        final Item item = super.createItem(ctx, type, allAttributes);
        return item;
    }

    @SuppressWarnings("deprecation")
    @Override
    public String getDescription(final SessionContext ctx) {
        final Collection<SizeType> sizeTypes = getSizeTypes();
        final StringBuilder result = new StringBuilder();
        if (!sizeTypes.isEmpty())
        {
            final String localizedString = Localization.getLocalizedString("type.CMSBrandRestriction.description.text");
            result.append(localizedString == null ? "Page only applies on brands:" : localizedString);
            for (final SizeType sizeType : sizeTypes)
            {
                result.append(" ").append(sizeType.getName(ctx)).append(" (").append(sizeType.getCode()).append(");");
            }
        }
        return result.toString();
    }

}
