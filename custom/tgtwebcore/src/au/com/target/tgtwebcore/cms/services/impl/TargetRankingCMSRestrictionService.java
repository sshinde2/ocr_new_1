/**
 *
 */
package au.com.target.tgtwebcore.cms.services.impl;

import de.hybris.platform.acceleratorcms.services.impl.RankingCMSRestrictionService;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author Vivek
 */
public class TargetRankingCMSRestrictionService extends RankingCMSRestrictionService {

    private static final Logger LOG = Logger.getLogger(TargetRankingCMSRestrictionService.class);

    @Override
    public Collection<AbstractPageModel> evaluatePages(final Collection<AbstractPageModel> pages,
            final RestrictionData data)
    {
        final NavigableMap<Integer, List<AbstractPageModel>> allowedPages = new TreeMap<Integer, List<AbstractPageModel>>();

        final Collection<AbstractPageModel> defaultPages = getDefaultPages(pages);
        for (final AbstractPageModel page : pages)
        {
            if (defaultPages.contains(page))
            {
                continue;
            }

            final List<AbstractRestrictionModel> restrictions = page.getRestrictions();
            if (restrictions == null || restrictions.isEmpty())
            {
                LOG.debug("Page [" + page.getName()
                        + "] is not default page and contains no restrictions. Skipping this page.");
            }
            else
            {
                LOG.debug("Evaluating restrictions for page [" + page.getName() + "].");
                final boolean onlyOneRestrictionMustApply = page.isOnlyOneRestrictionMustApply();
                final boolean allowed = evaluate(restrictions, data, onlyOneRestrictionMustApply);
                if (allowed)
                {
                    LOG.debug("Adding page [" + page.getName() + "] to allowed pages");
                    final Integer countOfMatchingRestrictions = Integer.valueOf(onlyOneRestrictionMustApply ? 1
                            : restrictions.size());

                    if (allowedPages.containsKey(countOfMatchingRestrictions))
                    {
                        // Add to existing list
                        allowedPages.get(countOfMatchingRestrictions).add(page);
                    }
                    else
                    {
                        // Add a new entry
                        final List<AbstractPageModel> list = new ArrayList<>();
                        list.add(page);
                        allowedPages.put(countOfMatchingRestrictions, list);
                    }
                }
            }
        }

        final List<AbstractPageModel> result = new ArrayList<>();

        if (!allowedPages.isEmpty())
        {
            // Take the highest match count
            result.addAll(allowedPages.lastEntry().getValue());
        }
        else
        {
            if (defaultPages.size() > 1)
            {
                LOG.debug(createMoreThanOneDefaultPageWarning(defaultPages));
            }
            if (CollectionUtils.isNotEmpty(defaultPages))
            {
                LOG.debug("Returning default page");
                result.add(defaultPages.iterator().next());
            }
        }

        return result;
    }


}
