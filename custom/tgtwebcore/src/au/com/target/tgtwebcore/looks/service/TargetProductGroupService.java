/**
 * 
 */
package au.com.target.tgtwebcore.looks.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author Nandini
 *
 */
public interface TargetProductGroupService {

    /**
     * Get all looks for all colour variants of the given product
     * 
     * @param productModel
     * @return List of TargetProductGroupPageModel
     */
    List<TargetProductGroupPageModel> getAllActiveLooksForProduct(ProductModel productModel);
}
