<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<json:object>
	<json:property name="id" value="${component.uid}"/>
	<json:property name="name" value="${component.name}"/>
	<json:property name="title" value="${component.title}"/>
	<json:property name="description" value="${component.description}"/>
	<json:property name="type" value="exit"/>
	<json:property name="exitUrl" value="${component.exitUrl}"/>
	<json:property name="includeUrls" value="${component.includeUrls}"/>
	<json:property name="excludeUrls" value="${component.excludeUrls}"/>
	<json:property name="trafficPercentage" value="${component.trafficPercentage}"/>
	<json:property name="device" value="${component.device}"/>
	<json:property name="nextEligibility" value="${component.nextEligibility}"/>
	<json:property name="feature" value="${component.featureKey}"/>
	<json:property name="minDelay" value="${component.minDelay}"/>
	<json:property name="maxDelay" value="${component.maxDelay}"/>
	<json:property name="minUse" value="${component.minUse}"/>
</json:object>
