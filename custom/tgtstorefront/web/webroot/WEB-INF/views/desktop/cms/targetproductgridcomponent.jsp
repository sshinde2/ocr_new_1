<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>

<div class="lattice-product">
	<c:if test="${not empty component.title}">
		<comp:component-heading
			title="${component.title}"
			compLink="${component.link}"/>
	</c:if>
	<ul class="lattice-list">
		<c:forEach items="${productData}" var="product" varStatus="status">

			<c:set var="variantList" value="${product.targetVariantProductListerData}" />
			<c:url value="${fn:length(variantList) > 0 and not empty variantList[0] ? variantList[0].url : product.url}" var="productUrl" />
			<c:set var="productEcommJson"><template:productEcommJson
				product="${product}"
				list="Grid"
				position="${status.count}"
				assorted="${product.assorted}" /></c:set>
			<li class="lattice-product ga-ec-impression" data-ec-product='${productEcommJson}'>
				<c:set var="primaryImageUrl"><product:productListerImage product="${product}" format="list" index="0" unavailable="true" /></c:set>
				<a href="${productUrl}" title="${quickView} &ndash; ${product.name}" class="${not product.giftCard ? 'quick-add-action ': ''} ga-ec-quick-click" data-product-code="${product.code}" data-rel="${component.uid}" data-product-name="${product.name}">
					<img src="${primaryImageUrl}" alt="${product.name}" title="${product.name}" />
					<product:pricePoint product="${product}" />
				</a>
			</li>
		</c:forEach>
	</ul>
</div>
