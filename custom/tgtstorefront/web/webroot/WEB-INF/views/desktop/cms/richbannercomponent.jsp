<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="marketing" tagdir="/WEB-INF/tags/desktop/marketing" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<c:set var="contentLocation" value="${not empty component.bannerContentLocation ? fn:toLowerCase(component.bannerContentLocation) : ''}" />
<c:set var="bannerClass" value="${not empty contentLocation ? 'banner-'.concat(contentLocation) : ''}" />

<comp:banner banner="${component}" bannerClass="${bannerClass}">

	<c:if test="${not empty linkComponents}">
		<div class="multi-links-container hide-for-tiny">
			<c:set var="position" value="${not empty component.containerPosition ? fn:toLowerCase(component.containerPosition) : 'center' }" />
			<div class="links links-${position}">

				<c:set var="cmsLinkAsButton" value="true" scope="request" />
				<c:forEach items="${linkComponents}" var="link">
					<cms:component component="${link}"/>
				</c:forEach>
				<c:remove var="cmsLinkAsButton" scope="request" />

			</div>
		</div>
	</c:if>

	<c:if test="${not empty component.exclusionsContent or not empty component.exclusionsLink}">
		<marketing:exclusions
			exclusionsContent="${component.exclusionsContent}"
			exclusionsLabel="${component.exclusionsLabel}"
			displayExclusionLabel="true"
			exclusionsLink="${component.exclusionsLink}" />
	</c:if>

	<c:if test="${not empty coordinateLinkComponents}">
		<div class="hide-for-tiny">
			<c:set var="cmsComponentId" value="rb-${component.uid}" scope="request" />
			<c:forEach items="${coordinateLinkComponents}" var="coordLink">
				<cms:component component="${coordLink}"/>
			</c:forEach>
			<c:remove var="cmsComponentId" scope="request" />
		</div>
	</c:if>

	<c:if test="${not empty mobileLink}">
		<div class="only-for-tiny mobile-link">
			<c:set var="cmsLinkAsButton" value="true" scope="request" />
			<cms:component component="${mobileLink}"/>
			<c:remove var="cmsLinkAsButton" scope="request" />
		</div>
	</c:if>

</comp:banner>
