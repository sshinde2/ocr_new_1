<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>

<comp:link link="${component}" buttonLink="${cmsLinkAsButton}" cmsComponentId="${cmsComponentId}" coordinates="${false}" />