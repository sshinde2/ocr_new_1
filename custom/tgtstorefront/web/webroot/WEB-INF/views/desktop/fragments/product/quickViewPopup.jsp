<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<c:set var="pageMode" value="${not availableOnline and availableInStore ? 'store' : 'online'}" />
<c:set var="stockDataAttr"><product:productConsolidatedStockDataAttrs pageMode="${pageMode}" /></c:set>

<c:set var="carouselType" value="cloudzoom" />


<c:set var="ecProductJson"><product:productDetailAnalytics /></c:set>
<feature:enabled name="uiReactPDPDetailPanel">
	<c:set var="reactDetailsPanel" value="${true}" />
</feature:enabled>
<c:choose>
	<c:when test="${not reactDetailsPanel}">
		<c:set var="reactDetailClasses"><%--
			--%>has-detail-panel is-${pageMode}-mode<%--
		--%></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="reactDetailClasses"><%--
			--%>has-react-panel<%--
		--%></c:set>
	</c:otherwise>
</c:choose>

<div class="prod-detail ProductDetails ${reactDetailClasses} is-${carouselType} prod-quick-view ProductDetails is-no-store-search"
	${ecProductJson}
	data-colour-variant-code="${product.colourVariantCode}"
	data-base-prod-code="${product.baseProductCode}"
	data-prod-code="${product.code}"
	data-no-store-search="true"
	${stockDataAttr}>
	<div class="main">
		<c:choose>
			<c:when test="${reactDetailsPanel}">
				<div class="react-root" data-component="quickView">
					<util:reactInitialState
						productData='"${product.baseProductCode}": ${targetProductListerData}'
						applicationData='"page": {"type": "${pageType}","data":{"baseProductCode": "${product.baseProductCode}", "selectedVariant": "${product.code}"}},"environment":${environmentData}'
						featureData="${uiFeaturesEnabled}"
						lookData="${shopTheLookData}" />
				</div>
			</c:when>
			<c:otherwise>
				<product:productDetailsImages product="${product}" galleryImages="${galleryImages}"/>
				<product:productDetailsPanel product="${product}" hideTabs="${true}" pageMode="${pageMode}" />
				<product:productViewLink product="${product}" />
			</c:otherwise>
		</c:choose>
	</div>
	<script>setTimeout(function(){t_msg.publish('/react/reload')}, 500)</script>
</div>
