<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<spring:theme code="instore.stock.level.message.no" var="noStockMsg" />
<spring:theme code="instore.stock.level.message.low" var="lowStockMsg" />
<spring:theme code="instore.stock.level.message.high" var="highStockMsg" />

<feature:enabled name="featureLocationServices">
	<c:set var="applicationData">"page": {"data":{"isFias": ${isFias}, "baseProductCode": "${product.baseProductCode}" } },"environment":${environmentData}</c:set>

	<div class="react-root" data-component="customerLocation" data-root-url="${isFias ? '/find-in-another-store' : '/find-store'}">
		<util:reactInitialState featureData="${uiFeaturesEnabled}" applicationData="${applicationData}" productData='"${product.baseProductCode}": ${productJson}' />
		<util:loading />
	</div>
	<script>setTimeout(function(){t_msg.publish('/react/reload')}, 500);</script>
</feature:enabled>
<feature:disabled name="featureLocationServices">
	<template:pageModal pageTitle="${pageTitle}">
		<div class="main FindNearestStore-lightbox">
			<div class="FindNearestStore ${isFias ? ' fias' : ''}"
					data-position-url="/ws-api/v1/target/stores/nearest"
					data-location-url="/ws-api/v1/target/stores/nearest"
					data-set-store-url="/ws-api/v1/target/customer/preferredStore"
					data-feat-key="sms"
					data-is-fias="${isFias}"
					data-product-code="${product.code}"
					data-no-msg="${noStockMsg}"
					data-low-msg="${lowStockMsg}"
					data-high-msg="${highStockMsg}">
				<h2 class="title u-alignCenter u-cocogoose FindNearestStore-heading"><spring:theme code="instore.stock.search.heading" /></h2>

				<c:if test="${isFias}">
					<div class="FindNearestStore-productDetails">
						<h3 class="FindNearestStore-productName">${product.baseName}</h3>
						<h4 class="FindNearestStore-productColour">
							<c:forEach items="${product.baseOptions}" var="baseOptions">
								<c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
									<c:if test="${baseOptionQualifier.qualifier eq 'swatch' and not empty baseOptionQualifier.value}">
										<c:set var="colour" value="${baseOptionQualifier.value}" />
									</c:if>
									<c:if test="${baseOptionQualifier.qualifier eq 'size' and not empty baseOptionQualifier.value}">
										<c:set var="size" value="${baseOptionQualifier.value}" />
									</c:if>
								</c:forEach>
							</c:forEach>

							<c:if test="${not empty colour}">
								${colour}
								<c:if test="${not empty size}">
									<c:out value=", " />
								</c:if>
							</c:if>
							<c:if test="${not empty size}">
								${size}
							</c:if>
						</h4>
					</div>
				</c:if>

				<label class="FindNearestStore-searchLabel" for="control-search-location-query"><spring:theme code="instore.stock.search.label" /></label>
				<form>
					<div class="FindNearestStore-container">
						<spring:theme code="instore.stock.search.placeholder" var="placeholder" />
						<input class="FindNearestStore-input" type="text" id="control-search-location-query" name="search" placeholder="${placeholder}" maxlength="100" />
						<button class="Button Button--white FindNearestStore-button control-findstore-search-location-button"><spring:theme code="instore.stock.search.go" /></button>
					</div>
					<div class="FindNearestStore-fieldError store-search-field-error"><spring:theme code="instore.stock.search.fieldError" /></div>
					<div class="FindNearestStore-currentLocationContainer">
						<button class="FindNearestStore-currentLocationButton control-findstore-search-position-button" type="button"><spring:theme code="storeFinder.use.my.current.location" /></button>
						<p class="FindNearestStore-currentLocationUnavailable"><spring:theme code="find.nearest.store.location.unavailable" /></p>
					</div>
				</form>

				<div class="FindNearestStore-error">

				</div>
				<div class="FindNearestStore-stores">

				</div>
			</div>
		</div>
	</template:pageModal>
</feature:disabled>
