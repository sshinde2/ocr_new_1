<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>

<json:object>
	<json:property name="success" value="${isVoucherValid}"/>
	<c:if test="${not isVoucherValid}">
		<json:property name="errorMsg" escapeXml="false">
			${errorMsg}
		</json:property>
	</c:if>
	<c:if test="${isVoucherValid}">
		<json:property name="successMsg" escapeXml="false">
			${voucherSuccessMsg}
		</json:property>
	</c:if>
	<json:object name="cartData">
		<json:property name="paymentTotalHtml" escapeXml="false">
			<multi-checkout:payment-total />
		</json:property>
		<json:property name="hasFlybuysDiscount" value="${not empty cartData.flybuysDiscountData}" />
	</json:object>
	<c:if test="${isVoucherValid}">
		<json:property name="cartSummaryHtml" escapeXml="false">
			<cart:cartTotals showEntries="true" cartData="${cartData}"/>
		</json:property>	
	</c:if>
	<json:property name="redeemSummaryHtml" escapeXml="false">
		<multi-checkout:flybuysRedeemSummary redeemOptions="${redeemOptions}" />
	</json:property>
</json:object>