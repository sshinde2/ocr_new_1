<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<template:page pageTitle="${pageTitle}" disableFooterEnews="${true}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages />
		
		<div class="main">

			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:slot var="component" contentSlot="${slots.Main}">
				<cms:component component="${component}"/>
			</cms:slot>
			<c:remove var="contentSlotContext" scope="request" />

			<cms:slot var="component" contentSlot="${slots.Alternative}">
				<c:set var="isAlternative" value="${true}" />
			</cms:slot>
			<c:choose>
				<c:when test="${isAlternative}">
					<c:set var="contentSlotContext" value="full" scope="request" />
					<cms:slot var="component" contentSlot="${slots.Alternative}">
						<cms:component component="${component}"/>
					</cms:slot>
					<c:remove var="contentSlotContext" scope="request" />
				</c:when>
				<c:otherwise>			
					<account:subscribe actionNameKey="enews.Submit">
						<jsp:attribute name="afterFields">
							<user:inlinePrivacy/>
						</jsp:attribute>
					</account:subscribe>
				</c:otherwise>
			</c:choose>

		</div>

		<div class="supplement">
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:slot var="component" contentSlot="${slots.Supplement}">
				<cms:component component="${component}"/>
			</cms:slot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>

</template:page>