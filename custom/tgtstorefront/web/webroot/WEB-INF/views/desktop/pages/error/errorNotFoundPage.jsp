<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<c:if test="${not empty notFoundPageType}">
	<c:set var="pageType" value=".${fn:toLowerCase(notFoundPageType)}" />
</c:if>

<template:page pageTitle="${pageTitle}">
	<script>
		if( window.ga && window.ga.call ) {	ga('send', 'event', 'Error', '404', 'page: ' + document.location.pathname ); }
	</script>
	<common:globalMessages/>
	<div class="main">
		<h1 class="heading"><spring:theme code="system.error${pageType}.page.not.found.heading"/></h1>
		<h2 class="subheading"><spring:theme code="system.error${pageType}.page.not.found.subheading" /></h2>
		<c:url var="homeURL" value="/" />
		<c:url var="sitemapURL" value="/sitemap" />
		<spring:theme var="options" code="system.error${pageType}.page.not.found.options" arguments="${homeURL};${sitemapURL}" argumentSeparator=";" />
		<c:set var="items" value="${fn:split(options, '|')}" />
		<ul>
			<c:forEach var="item" items="${items}">
				<li>${item}</li>
			</c:forEach>
		</ul>
	</div>
</template:page>