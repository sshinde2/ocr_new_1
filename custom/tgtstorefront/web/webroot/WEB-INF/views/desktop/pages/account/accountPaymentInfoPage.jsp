<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.manageCards.heading" /></h1>
	<c:choose>
		<c:when test="${not empty paymentInfoData}">
			<ul class="block-list card-list manage-cards">
				<c:forEach items="${paymentInfoData}" var="paymentMethod" varStatus="status" >
					<customer:existingCards paymentMethod="${paymentMethod}" count="${status.count}" remove="/my-account/remove-payment-method" />
				</c:forEach>
			</ul>
		</c:when>
		<c:otherwise>
			<p><spring:theme code="text.account.manageCards.noCards" /></p>
		</c:otherwise>
	</c:choose>
</account:page>