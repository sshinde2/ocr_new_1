<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>


<template:page pageTitle="${pageTitle}">
	<div class="global-messages">
		<common:globalMessages/>
	</div>
	
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />

	<div class="main">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="mainComponent" contentSlot="${slots.Main}">
			<cms:component component="${mainComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

	<div class="supplement">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="supplementComponent" contentSlot="${slots.Supplement}">
			<cms:component component="${supplementComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

</template:page>