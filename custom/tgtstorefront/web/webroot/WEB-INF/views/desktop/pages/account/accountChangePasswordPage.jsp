<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.updatePassword.heading" /></h1>
	<p><spring:theme code="text.account.updatePassword.note" /></p>
	<p class="required"><spring:theme code="form.mandatory.message"/></p>
	<form:form action="${targetUrlsMapping.myAccountUpdatePassword}" method="post" commandName="updatePasswordForm" autocomplete="off" cssClass="single-action f-narrow">
		<formElement:formPasswordBox idKey="profile.currentPassword" labelKey="profile.currentPassword" path="currentPassword" inputCSS="text password" mandatory="true"/>
		<formElement:formPasswordBox idKey="password" labelKey="profile.newPassword" path="newPassword" inputCSS="text password strength" mandatory="true" strength="${true}"/>
		<formElement:formPasswordBox idKey="profile.checkNewPassword" labelKey="profile.checkNewPassword" path="checkNewPassword" inputCSS="text password" mandatory="true" errorPath="updatePasswordForm"/>
		<div class="f-buttons">
			<button class="button-fwd button-single"><spring:theme code="text.account.button.saveChanges" /></button>
		</div>
		<target:csrfInputToken/>
	</form:form>
</account:page>