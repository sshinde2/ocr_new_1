<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>

<%-- to be replaced to tgt:tag --%>
<cms:slot var="component" contentSlot="${slots.Supplement}">
	<c:set var="hasVisibleComponentsSupplement" value="${true}" />
</cms:slot>

<template:checkoutPage stepName="paymentDetails" hideHome="true" pageTitle="${pageTitle}">
	<common:globalMessages/>
	<cart:sohMessage />
	<div class="co-page">
		<div class="main">
			<multi-checkout:newPayment />
		</div>
		
		<div class="aside">
			<multi-checkout:checkoutOrderDetails cartData="${cartData}" />
		</div>

		<div class="co-controls">
			<multi-checkout:nextActions 
				backCode="checkout.multi.paymentMethod.backToDeliveryOptions"
				backUrl="${targetUrlsMapping.yourAddress}"
				nextCode="checkout.multi.deliveryMethod.continue"
				 />
		</div>

		<c:if test="${hasVisibleComponentsSupplement}">
			<div class="supplement">
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:slot var="supplementComponent" contentSlot="${slots.Supplement}">
				<cms:component component="${supplementComponent}"/>
			</cms:slot>
			<c:remove var="contentSlotContext" scope="request" />
			</div>
		</c:if>
	</div>
</template:checkoutPage>
