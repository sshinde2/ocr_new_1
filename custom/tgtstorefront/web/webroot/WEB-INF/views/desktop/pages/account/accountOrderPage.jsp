<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="order-summary-card" tagdir="/WEB-INF/tags/desktop/order/summaryCard" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.orderDetails" /></h1>
	<%-- Order Status Headings --%>
	<c:if test="${not empty orderData.status}">
		<c:set var="lowerCaseStatus" value="${fn:toLowerCase(orderData.status.code)}" />
		<spring:theme code="text.account.orderStatus.${lowerCaseStatus}.heading" var="statusHeading" text="" />
		<c:set var="cncOrderStatus" value="${orderData.cncCustomOrderStatus}" />
		<c:if test="${not empty statusHeading}">
			<c:choose>
				<c:when test="${not empty cncOrderStatus}">
					<spring:theme code="text.account.order.${cncOrderStatus}.message" />
				</c:when>
				<c:otherwise>
					<h4 class="status-heading">${statusHeading}</h4>
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${cncOrderHelper.isNonCncNonInvoicedOrder(orderData)}" >
			<spring:theme code="text.account.orderStatus.${lowerCaseStatus}.description" var="statusDescription" text="" />
			<c:if test="${not empty statusDescription}">
				<p class="status-copy">${statusDescription}</p>
			</c:if>
		</c:if>
	</c:if>

	<%-- Order number --%>
	<p class="your-order">
		<spring:theme code="text.account.orderDetails.yourOrderNumber" />&nbsp;<strong>${fn:escapeXml(orderData.code)}</strong>
	</p>

	<%-- Cancel order button --%>
	<feature:enabled name="featureUpdateCardEnabled">
		<c:if test="${orderData.status eq 'PARKED'}">
			<spring:theme code="order.summarycard.myaccount.cancel.confirm.heading" var="confirmCancelHeading"/>
			<spring:theme code="order.summarycard.myaccount.cancel.confirm.subText" var="confirmCancelSubText"/>
			<c:set var="cancelUrl" value="${targetUrlsMapping.myAccountOrderDetails}${orderData.code}/cancel-order"/>
			<a class="button-small-text button-norm button-small ConfirmButton summary-panel--topButton" href="#" data-confirm-heading="${confirmCancelHeading}" data-confirm-subtext="${confirmCancelSubText}" data-confirm-url="${cancelUrl}">
				<spring:theme code="order.summarycard.myaccount.cancel.confirm.button"/>
			</a>
		</c:if>
	</feature:enabled>

	<%-- PANEL: Order Status --%>
	<order-summary-card:status
		orderData="${orderData}"
		customerCode="myaccount" />

	<%-- PANEL: e-Gift Card Delivery --%>
	<order-summary-card:giftCard tense="past" />

	<%-- PANEL: Selected Store and PANEL: Contact Details --%>
	<c:if test="${not empty orderData.cncStoreNumber and not empty selectedStore}">
		<order-summary-card:storeDetails selectedStore="${selectedStore}" customerCode="myaccount" />
		<order-summary-card:cncContact addressData="${orderData.deliveryAddress}" customerCode="myaccount" />
	</c:if>

	<%-- PANEL: Delivery Address  --%>
	<c:if test="${not empty orderData.deliveryAddress and empty orderData.cncStoreNumber}">
		<order-summary-card:address
			heading="order.summarycard.myaccount.heading.deliveryAddress"
			addressData="${orderData.deliveryAddress}"
			customerCode="myaccount" />
	</c:if>

	<%-- PANEL: Payment Detail --%>
	<order-summary-card:paymentDetails
		paymentsInfo="${orderData.paymentsInfo}"
		customerCode="myaccount"
		flybuysNumber="${orderData.maskedFlybuysNumber}" />

	<%-- PANEL: Remaining Balance Payment Detail --%>
	<feature:enabled name="featureUpdateCardEnabled">
		<c:if test="${orderData.status eq 'PARKED'}">
			<order-summary-card:remainingBalancePayment
				paymentsInfo="${orderData.outstandingAmountPaymentsInfo}" 
				bottomActionUrl="${orderData.code}/update-card" 
				bottomActionCode="order.summarycard.myaccount.updateCardButton" />
		</c:if>
	</feature:enabled>

	<%-- PANEL: Billing Address --%>
	<c:if test="${empty orderData.cncStoreNumber and not empty orderData.paymentInfo.billingAddress}">
		<order-summary-card:address
			heading="order.summarycard.myaccount.heading.billingAddress"
			addressData="${orderData.paymentInfo.billingAddress}"
			customerCode="myaccount" />
	</c:if>

	<%-- TABLE: Order Summary --%>
	<div class="info-group">
		<h3><spring:theme code="text.account.orderDetails.orderSummary" /></h3>
		<order:orderDetailsItem order="${orderData}"/>
	</div>

	<%-- TABLE: Returns and Exchanges --%>
	<c:if test="${fn:length(orderData.returnAndRefundProducts) > 0}">
		<div class="info-group">
			<h3><spring:theme code="text.account.orderDetails.refundsAndExchanges" /></h3>
			<order:returnsTable returns="${orderData.returnAndRefundProducts}"/>
		</div>
	</c:if>

</account:page>
