<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1><spring:theme code="text.account.${edit ? 'edit' : 'add'}Address.heading" /></h1>
	<div class="item_container">
		<p class="required"><spring:theme code="form.mandatory.message"/></p>

		<form:form action="${edit ? targetUrlsMapping.myAccountAddressEdit : targetUrlsMapping.myAccountAddressAdd}" method="post" commandName="addressForm" cssClass="${singleLineAddressLookup ? '' : 'del-address-form' } single-action f-narrow" data-confirm-url="${edit ? targetUrlsMapping.myAccountAddressEdit : targetUrlsMapping.myAccountAddressAdd}" novalidate="true">
			<spring:hasBindErrors name="addressForm">
				<c:set var="hasErrors" value="${true}" />
			</spring:hasBindErrors>

			<customer:addressFields
				titles="${titleData}"
				selectedTitle="${addressForm.titleCode}"
				states="${states}"
				selectedState="${addressForm.state}"
				countries="${countryData}"
				selectedCountry="${addressForm.countryIso}"
				allowDefault="${not isDefault}"
				singleInputActive="${not hasErrors and not edit}" />

			<div class="f-buttons">
				<button class="button-fwd button-single"><spring:theme code="text.account.addressBook.saveAddress" /></button>
			</div>

			<target:csrfInputToken/>
		</form:form>
	</div>
</account:page>
