<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>

<template:pageModal pageTitle="${pageTitle}">
	<jsp:body>
		<div class="main">
			<customer:confirmAddressForm
				actionUrl="${edit ? targetUrlsMapping.myAccountAddressEditConfirm : targetUrlsMapping.myAccountAddressAddConfirm}" />
		</div>
	</jsp:body>
</template:pageModal>