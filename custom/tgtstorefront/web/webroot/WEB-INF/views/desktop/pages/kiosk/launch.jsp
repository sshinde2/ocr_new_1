<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<head>
<style>
.visuallyhidden {border: 0;clip: rect(0 0 0 0);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}
body { background: #ba0000; color:white; font-family: arial;}
#msg {text-align: center; margin-top: 12em; -webkit-transition: opacity 0.2s ease;}
.flash {opacity: 0;}
</style>
</head>
<body>
<h1 id="msg">Initialising....</h1>

<c:choose>
	<c:when test="${assetModeDevelop}">
		<c:set var="launchUrl">${commonResourcePath}/src/js/kiosk/head/launch.js</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="launchUrl">
			<asset:resource critical="js/kiosk/head/launch.js" basePath="${commonResourcePath}" />
		</c:set>
	</c:otherwise>
</c:choose>

<script src="${launchUrl}"></script>
<script>kioskLaunch(document, location).start();</script>
</body>

