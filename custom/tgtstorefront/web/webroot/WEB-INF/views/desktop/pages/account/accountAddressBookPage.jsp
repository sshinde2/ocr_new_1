<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.manageAddresses.heading" text="Address Book"/></h1>
	<c:choose>
		<c:when test="${not empty addressData}">
			<p><spring:theme code="text.account.manageAddresses.default" /></p>
			<ul class="block-list address-list">
				<c:forEach items="${addressData}" var="deliveryAddress" varStatus="status">
					<customer:existingAddresses deliveryAddress="${deliveryAddress}" isSelected="${deliveryAddress.id eq selectedAddressId}" count="${status.count}" edit="edit-address/${deliveryAddress.id}" defaultAddress="set-default-address/${deliveryAddress.id}" remove="remove-address/${deliveryAddress.id}" />
				</c:forEach>
			</ul>
		</c:when>
		<c:otherwise>
			<p><spring:theme code="text.account.manageAddresses.noAddresses" /></p>
		</c:otherwise>
	</c:choose>

	<a href="add-address" class="button-fwd">
		<spring:theme code="text.account.addressBook.addAddress" text="Add new address"/>
	</a>
</account:page>
