<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<template:checkoutPage stepName="paymentDetails" hideHome="true" pageTitle="${pageTitle}">
	<common:globalMessages/>
	<cart:sohMessage />

	<div class="co-page co-status-init">
		<div class="main">
			<c:set var="modeCode" value="${featuresEnabled.featureIPGEnabled or kioskMode ? '' : '.earlyPay'}" />
			<h2 class="co-heading"><spring:theme code="checkout.multi.paymentDetails.heading${modeCode}" /></h2>

			<multi-checkout:pageLoadingIndicator />
			<div class="co-interactive pay-modes">
				<multi-checkout:existingPayment />
				<multi-checkout:newPayment />
				<c:if test="${!kioskMode}">
					<h3><spring:theme code="checkout.multi.paymentDetails.discounts" /></h3>
				</c:if>
				<form>
					<template:fixSingleInputForm />
					<div class="abstract-placeholder"></div>
				</form>

				<multi-checkout:payment-total />
			</div>
		</div>

		<div class="aside">
			<multi-checkout:checkoutOrderDetails cartData="${cartData}" />
		</div>

		<div class="co-controls">

			<spring:theme code="checkout.multi.paymentDetails.continue${featuresEnabled.featureIPGEnabled or kioskMode ? '.review' : ''}" var="nextCode" />

			<c:if test="${not empty cartData.paymentInfo and cartData.paymentInfo.paypalPaymentInfo}" >
				<spring:theme code="checkout.multi.paymentDetails.continue.paypal" var="nextCode" />
			</c:if>

			<multi-checkout:nextActions
					backCode="checkout.multi.paymentDetails.delivery.backTo"
					backUrl="${targetUrlsMapping.yourAddress}"
					nextCode="${nextCode}"
					 />
		</div>


		<script type="text/x-handlebars-template" id="ajax-error-feedback">
			<feedback:message type="error" size="small" icon="false">
				<p><spring:theme code="xhr.error.occurred" /></p>
			</feedback:message>
		</script>

	</div>
</template:checkoutPage>
