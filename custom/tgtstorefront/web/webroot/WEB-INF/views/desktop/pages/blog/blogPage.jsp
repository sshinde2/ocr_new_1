<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>


<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />
	<div class="inner-content has-alt-aside">
		<div class="main">
			<c:set var="contentSlotContext" value="wide" scope="request" />
			<cms:slot var="pageContentComponent" contentSlot="${slots.Main}">
				<cms:component component="${pageContentComponent}"/>
			</cms:slot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
		<div class="aside">
			<div class="aside-inner">
				<c:set var="contentSlotContext" value="narrow" scope="request" />
				<cms:slot var="leftColumnComponent" contentSlot="${slots.AsideTop}">
					<cms:component component="${leftColumnComponent}"/>
				</cms:slot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>

			<nav:leftNav />

			<div class="aside-inner">
				<c:set var="contentSlotContext" value="narrow" scope="request" />
				<cms:slot var="leftColumnComponent" contentSlot="${slots.Aside}">
					<cms:component component="${leftColumnComponent}"/>
				</cms:slot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>

			<%-- This element is gives the left menu height when no components assigned. --%>
			<span>&nbsp;</span>
		</div>
	</div>
</template:page>
