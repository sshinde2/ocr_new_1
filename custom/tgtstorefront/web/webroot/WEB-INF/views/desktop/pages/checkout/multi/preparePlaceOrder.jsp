<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<json:object>
	<json:property name="success" value="${isPrepareSuccess}"/>
	<c:if test="${not isPrepareSuccess}">
		<json:property name="message" escapeXml="false">
			${errorMsg}
		</json:property>
	</c:if>
	<%-- Order Number is same as Cart Data code --%>
	<json:property name="orderNumber" value="${cartData.code}" />
</json:object>