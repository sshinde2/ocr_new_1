<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Remove Analytics ID's so GA doesn't work --%>
<c:remove var="analyticsAccountId" scope="request" />
<c:remove var="universalAnalyticsId" scope="request" />
<c:remove var="tagManagerContainer" scope="request" />