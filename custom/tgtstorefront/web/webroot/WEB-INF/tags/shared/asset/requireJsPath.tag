<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="module" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<c:set var="isProd" value="${not assetModeDevelop || param.assetmode eq 'prod'}" />

<c:if test="${not empty module}">
	<c:choose>
		<c:when test="${isProd}">
			<c:set var="resourceKey">ui_resource_js/${module}.js</c:set>
			<json:property name="${module}" value="${fn:replace(requestScope[resourceKey], '.js', '')}" />
		</c:when>
		<c:otherwise>
			<json:property name="${module}" value="${module}" />
		</c:otherwise>
	</c:choose>
</c:if>
