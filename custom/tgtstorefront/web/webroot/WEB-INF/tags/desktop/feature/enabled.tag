<%@ tag trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="name" required="false" rtexprvalue="true"%>

<c:if test="${featuresEnabled[name]}">
	<jsp:doBody />
</c:if>
