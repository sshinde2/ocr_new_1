<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ attribute name="redeemOptions" type="au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData" %>

<c:set var="loggedIn" value="${not empty redeemOptions}" />
<c:set var="canEdit" value="${not empty cartData.flybuysDiscountData or (loggedIn and fn:length(redeemOptions.redeemTiers) > 0)}" />

<c:set var="flybuysCollectSuccess" value="${not empty cartData.flybuysNumber}" />
<c:set var="flybuysRedeemSuccess" value="${not empty cartData.flybuysDiscountData}" />

<c:if test="${flybuysCollectSuccess}">
	<c:set var="className">flybuys-collect-success</c:set>
</c:if>

<c:if test="${flybuysRedeemSuccess}">
	<c:set var="className">${className} flybuys-redeem-success</c:set>
</c:if>

<c:if test="${flybuysState.denied and flybuysState.reason ne 'guest'}">
	<c:set var="className">${className} flybuys-redeem-disabled</c:set>
</c:if>

<c:if test="${loggedIn and empty redeemOptions.redeemTiers}">
	<c:set var="className">${className} flybuys-redeem-unavailable</c:set>
</c:if>

<div class="flybuys-options ${className}">
	<dl class="flybuys-collect">
		<dt class="flybuys-collect-title">
			<spring:theme code="icon.radio"/>
			<spring:theme code="icon.tick"/>
			<spring:theme code="icon.cross"/>
			<spring:theme code="payment.flyBuys.collect"/>
		</dt>
		<dd class="flybuys-collect-definition">
			<spring:theme code="payment.flybuys.collect.label"/>
		</dd>
	</dl>
	<c:if test="${flybuysRedeemConfigData.active}">
		<dl class="flybuys-redeem">
			<dt class="flybuys-redeem-title">
				<spring:theme code="icon.radio"/>
				<spring:theme code="icon.tick"/>
				<spring:theme code="icon.cross"/>
				<spring:theme code="payment.flyBuys.redeem"/>
			</dt>
			<dd class="flybuys-redeem-definition">
				<span class="flybuys-redeem-label">
					<c:choose>
						<c:when test="${flybuysState.reason eq 'subtotal'}">
							<spring:theme code="payment.flybuys.redeem.label.subtotal" arguments="${flybuysRedeemConfigData.minCartValue}"/>
						</c:when>
						<c:when test="${flybuysState.reason eq 'voucher'}">
							<spring:theme code="payment.flybuys.redeem.label.voucher"/>
						</c:when>
						<c:when test="${flybuysState.reason eq 'guest'}">
							<spring:theme code="payment.flybuys.redeem.label.guest" arguments="${targetUrlsMapping.checkoutLogin}"/>
						</c:when>
						<c:otherwise>
							<spring:theme code="payment.flybuys.redeem.label"/>
						</c:otherwise>
					</c:choose>
				</span>
				<c:if test="${not flybuysState.denied}">
					<button type="button" class="f-set-button button-norm ">
						<spring:theme code="payment.flyBuys.${canEdit ? 'edit' : 'redeem'}.button"/>
					</button>
				</c:if>
			</dd>
		</dl>
	</c:if>
</div>

<c:if test="${not empty redeemOptions and not flybuysState.denied}">
	<div class="flybuys-transaction">
		<dl class="flybuys-current-points">
			<dt><spring:theme code="payment.flybuys.transaction.currentpoints" /></dt>
			<dd>${redeemOptions.availablePoints}</dd>
		</dl>
		<dl class="flybuys-use-points">
			<dt><spring:theme code="payment.flybuys.transaction.usepoints" /></dt>
			<dd>${not empty cartData.flybuysDiscountData ? cartData.flybuysDiscountData.pointsConsumed : 0}</dd>
		</dl>
		<dl class="flybuys-redeem-val">
			<dt><spring:theme code="payment.flybuys.transaction.redeemval" /></dt>
			<c:choose>
				<c:when test="${not empty cartData.flybuysDiscountData}">
					<dd><format:price priceData="${cartData.flybuysDiscountData.value}" isNegative="true" stripDecimalZeros="true" /></dd>
				</c:when>
				<c:otherwise>
					<dd><spring:theme code="payment.flybuys.transaction.zeroValue" /></dd>
				</c:otherwise>
			</c:choose>
		</dl>
		<c:if test="${empty redeemOptions.redeemTiers}">
			<feedback:message type="error" size="small" icon="false">
				<p><spring:theme code="payment.flyBuys.options.notiers" arguments="${flybuysRedeemConfigData.minPointsBalance}"/></p>
			</feedback:message>
		</c:if>
	</div>
</c:if>