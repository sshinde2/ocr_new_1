<%@ attribute name="cartData" required="false" type="au.com.target.tgtfacades.order.data.TargetCartData" %>
<%@ attribute name="orderData" required="false" type="au.com.target.tgtfacades.order.data.TargetOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:if test="${not empty orderData.barcodePngUrl}">
	<h3 class="co-heading"><spring:theme code="checkout.multi.thankYou.barcode.heading" /></h3>
	<p><spring:theme code="checkout.multi.thankYou.barcode.message" arguments="${orderData.barcodeNumber}" /></p>
	<c:if test="${not empty orderData.barcodeNumber}">
		<p><spring:theme code="checkout.multi.thankYou.barcode.number" arguments="${orderData.barcodeNumber}" /></p>
	</c:if>
	<div id="barcode-container" class="barcode-container">
		<noscript>
			<img src="${orderData.barcodePngUrl}" width="250" />
		</noscript>
		<script>
			(function(d){
				var img = d.createElement('img');
				img.setAttribute('width', '250');
				img.src = Modernizr && Modernizr.svg ? '${orderData.barcodeSvgUrl}' : '${orderData.barcodePngUrl}';
				d.getElementById('barcode-container').appendChild(img);
			}(document));
		</script>
	</div>
</c:if>