<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryAddress" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ attribute name="count" required="false" type="java.lang.Integer" %>
<%@ attribute name="deliveryModeCode" required="true" type="java.lang.String" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<%-- Different URL based on if already selected --%>
<c:if test="${not isSelected && deliveryAddress.validForDeliveryMode}">
	<c:set var="useDeliveryAddressUrl" value="${targetUrlsMapping.yourAddressSelect}/?selectedAddressCode=${deliveryAddress.id}&deliveryModeCode=${deliveryModeCode}" />
	<spring:theme code="checkout.multi.deliveryAddress.useThisAddress" text="Use this address" var="useDeliveryText" />
</c:if>
<c:if test="${isSelected}">
	<c:set var="useDeliveryAddressUrl" value="${targetUrlsMapping.yourAddressContinue}" />
	<spring:theme code="checkout.multi.deliveryAddress.continueWithThisAddress" text="Continue" var="useDeliveryText" />
</c:if>

<customer:existingAddresses
	deliveryAddress="${deliveryAddress}"
	isSelected="${isSelected}"
	count="${count}"
	edit="${targetUrlsMapping.yourAddressEdit}/?editAddressCode=${deliveryAddress.id}&deliveryModeCode=${deliveryModeCode}"
	use="${useDeliveryAddressUrl}"
	useText="${useDeliveryText}"
	invalid="${not deliveryAddress.validForDeliveryMode}"
	deliveryModeCode="${deliveryModeCode}" />