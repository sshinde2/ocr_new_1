<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>


<c:if test="${kioskMode}">
	<sec:authorize access="hasAnyRole('ROLE_CUSTOMERGROUP')"><c:set var="authenticated" value="${true}" /></sec:authorize>
	<c:if test="${authenticated}">
		<div class="global-messages feedback-call-to-action">
			<feedback:message>
				<p><spring:theme code="checkout.multi.thankYou.signout.message"/></p>
				<div class="btns">
					<form:form action="${targetUrlsMapping.logoutKiosk}" method="post">
						<button type="submit" class="button-fwd button-large"><spring:theme code="checkout.multi.thankYou.signout.button"/></button>
						<target:csrfInputToken/>
					</form:form>
				</div>
			</feedback:message>
		</div>
	</c:if>
</c:if>
