<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<c:set var="absoluteOrderData" value="${not empty orderData ? orderData : cartData}" />

<div class="payment-total hide-for-small">
	<common:summaryPanel>
		<p><strong><spring:theme code="basket.page.totals.total"/></strong></p>
		<p class="prominent-price"><format:price priceData="${absoluteOrderData.totalPrice}" /></p>
	</common:summaryPanel>
</div>