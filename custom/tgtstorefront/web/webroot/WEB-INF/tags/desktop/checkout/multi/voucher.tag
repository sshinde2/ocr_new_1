<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="form" type="java.lang.String" %>
<%@ attribute name="path" type="java.lang.String" %>
<%@ attribute name="isolated" required="false"%>
<%@ attribute name="flybuysRedeemApplied" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty path}">
	<spring:theme code="payment.voucher.unavailable" var="exclusiveMessage" />
	<spring:theme code="payment.voucher.check" var="checkText" />
	<c:set var="voucherFieldData">
		data-validate-url="${targetUrlsMapping.paymentValidateVoucher}"
		data-release-url="${targetUrlsMapping.paymentRemoveVoucher}"
		data-exclusion-message="${exclusiveMessage}"
		data-check-text="${checkText}"
	</c:set>

	<spring:bind path="${path}">
		<c:set var="validAndProvided" value="${not empty status.value and empty status.errorMessages and not empty voucherSuccessMsg}" />
		<c:set var="elementCSS">voucher ${flybuysRedeemApplied ? 'f-inactive' : (validAndProvided ? 'f-success' : '')}</c:set>
		<c:set var="checkButton">${form}.voucher.${validAndProvided ? 'uncheck' : 'check'}</c:set>
		<formElement:formInputBox
			idKey="${form}.${path}"
			labelKey="${form}.${path}"
			path="${path}"
			inputCSS="text redeemable-code"
			isolated="${empty isolated or isolated}"
			elementCSS="${elementCSS}"
			checkButton="${checkButton}"
			fieldData="${voucherFieldData}"
			disabled="${flybuysRedeemApplied || validAndProvided}"
			buttonDisabled="${flybuysRedeemApplied}"
			pattern="[A-Z0-9-]*"
			message="${flybuysRedeemApplied ? exclusiveMessage : voucherSuccessMsg}"
			messageType="${flybuysRedeemApplied ? 'disabled' : 'good'}"
			disclaimer="${form}.${path}.disclaimer"
			placeholder="${form}.${path}.placeholder"
			maxlength="64" />
	</spring:bind>

</c:if>


