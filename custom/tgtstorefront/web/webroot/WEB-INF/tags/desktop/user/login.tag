<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>


<sec:authorize access="isRememberMe() or isAuthenticated()">
	<p class="subtle">
		<form:form action="${targetUrlsMapping.logout}" method="post">
			<spring:theme code="login.welcome" arguments="${user.firstName}" htmlEscape="true"/>&nbsp;
			<button type="submit" class="button-anchor blackLink"><spring:theme code="login.notMe"/></button>
			<target:csrfInputToken/>
		</form:form>
	</p>
</sec:authorize>
<form:form action="${action}" method="post" commandName="loginForm" novalidate="true">
	<sec:authorize access="isRememberMe() or isAuthenticated()">
		<input type="hidden" id="j_username" name="j_username" value="${user.uid}"/>
	</sec:authorize>
	<sec:authorize access="!isAuthenticated()">
		<formElement:formInputBox idKey="j_username" labelKey="login.email" labelCSS="login-view-label" placeholder="login.email" path="j_username" inputCSS="text" mandatory="true" type="email" autoCorrect="off"/>
	</sec:authorize>
	<formElement:formPasswordBox idKey="j_password" labelKey="login.password" labelCSS="login-view-label" placeholder="login.password" path="j_password" inputCSS="text password" mandatory="true"/>
	<user:optIn />
	<button id="login" type="submit" class="button-fwd cust-state-trig button-wait"><spring:theme code="login.signIn"/><spring:theme code="icon.right-arrow-small"/></button>
	<c:url value="/login/forgotten-password?forward=${forwardPage}" var="forgottenPwd" />
	<p class="subtle">
		<span class="required">
			<spring:theme code="form.mandatory.message"/>
		</span>
		<a href="${forgottenPwd}" class="forgotten-pwd-link"><spring:theme code="login.link.forgottenPwd"/></a>
		<target:csrfInputToken/>
	</p>
</form:form>


