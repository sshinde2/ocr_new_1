<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="label" required="true" type="java.lang.String" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="value" required="true" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="nested" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>


<template:formElement path="${path}" hideError="${nested}">
	<form:radiobutton path="${path}" value="${value}" cssClass="radio" id="${idKey}" />
	<label class="radio-label" for="${idKey}">${label}</label>
</template:formElement>
