<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelArguments" required="false" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>
<%@ attribute name="inputCSS" required="false" type="java.lang.String" %>
<%@ attribute name="checked" required="false" type="java.lang.String" %>
<%@ attribute name="disabled" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="tabindex" required="false" rtexprvalue="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>


<template:formElement path="${path}">
	<spring:theme code="${idKey}" var="themeIdKey"/>
	<div class="f-set f-opt">
		<form:checkbox cssClass="f-opt-input ${inputCSS}" id="${idKey}" path="${path}" tabindex="${tabindex}" checked="${checked ? 'checked' : ''}" disabled="${disabled}" />
		<label class="f-opt-label ${labelCSS}" for="${idKey}">
			<spring:theme code="${labelKey}" arguments="${labelArguments}" argumentSeparator=";;;" />
			<formElement:label mandatory="${mandatory}" />
		</label>
	</div>
</template:formElement>
