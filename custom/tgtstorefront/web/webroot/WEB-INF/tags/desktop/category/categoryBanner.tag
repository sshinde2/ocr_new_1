<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="banner" required="true" %>
<%@ attribute name="heading" required="true" %>

<c:set var="hasBanner" value="${not empty categoryBanner.url or not empty categoryBanner.description}" />

<c:if test="${hasBanner}">
	<div class="category-banner hide-for-small">

		<c:choose>
			<c:when test="${not empty categoryBanner.url}">
				<img alt="${heading} Category Banner" class="defer-img unveil-img" data-src="${categoryBanner.url}" data-screen-support="large" />
			</c:when>
			<c:when test="${not empty categoryBanner.description}">
				<div>
					${categoryBanner.description}
				</div>
			</c:when>
		</c:choose>

	</div>
</c:if>
