<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ attribute name="slotName" required="true" %>
<%@ attribute name="divider" required="false" %>

<c:set var="contentSlotContext" value="wide" scope="request" />
<cms:slot var="listerComponent" contentSlot="${slots[slotName]}">
	<cms:component component="${listerComponent}"/>
</cms:slot>
<c:remove var="contentSlotContext" scope="request" />