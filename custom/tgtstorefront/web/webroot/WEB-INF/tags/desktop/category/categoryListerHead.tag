<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="extraClass" %>
<%@ attribute name="below" required="false" fragment="true" %>

<div class="category-contextual-info ${extraClass}">
	<h1>${categoryName}</h1>
	<jsp:invoke fragment="below" />
</div>
