<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ attribute name="cartMode" required="false" type="java.lang.String" %>
<%@ attribute name="orderCompleted" required="false" type="java.lang.Boolean" %>

<c:if test="${not empty sohUpdates and fn:length(sohUpdates) gt 0}">
	<div class="global-messages" id="message-soh-change">

		<c:set var="cartMode" value="${not empty cartMode ? cartMode : 'singlecart'}" />
		
		<feedback:message type="${orderCompleted or orderPlaceFailed ? '' : 'error'}" strongHeadingCode="${orderCompleted or orderPlaceFailed ? '' : 'feedback.oops'}">
			
			<c:choose>
				<c:when test="${orderCompleted}">
					<p><spring:theme code="basket.information.soh.order.completed.message" /></p>
				</c:when>
				<c:when test="${orderPlaceFailed}">
					<p><spring:theme code="basket.information.soh.order.place.message" /></p>
				</c:when>
				<c:otherwise>
					<p><spring:theme code="basket.information.soh.message" /></p>
				</c:otherwise>
			</c:choose>

			<%-- Show all adjusted items messages --%>
			<c:forEach items="${sohUpdates}" var="sohUpdate">

				<c:if test="${not empty sohUpdate.adjustedItems and fn:length(sohUpdate.adjustedItems) gt 0}">
					<spring:theme code="basket.information.soh.name.${sohUpdate.purchaseType}" var="purchaseTypeName" />
					<p><spring:theme code="basket.information.soh.${cartMode}.adjustedItems" arguments="${purchaseTypeName}" argumentSeparator=";;" /></p>
					<ul>
						<c:forEach items="${sohUpdate.adjustedItems}" var="adjustedItem">
							
							<c:remove var="adjustedProductSize" />							
							<c:forEach items="${adjustedItem.product.baseOptions}" var="baseOptions">
								<c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
									<c:if test="${baseOptionQualifier.qualifier eq 'size' and not empty baseOptionQualifier.value}">
										<c:set var="adjustedProductSize" value="${baseOptionQualifier.value}" />
									</c:if>
								</c:forEach>
							</c:forEach>
								
							<li>
								<c:if test="${empty adjustedProductSize}">
									<spring:theme code="basket.information.soh.adjustedItem" text="{0}" arguments="${adjustedItem.product.name};;${adjustedItem.quantity}" argumentSeparator=";;" />
								</c:if>
								<c:if test="${not empty adjustedProductSize}">
									<spring:theme code="basket.information.soh.adjustedItem.with.productSize" text="{0}" arguments="${adjustedItem.product.name};;${adjustedProductSize};;${adjustedItem.quantity}" argumentSeparator=";;" />
								</c:if>								
							</li>
						</c:forEach>
					</ul>
				</c:if>

			</c:forEach>

			<%-- Show all out of stock messages --%>
			<c:forEach items="${sohUpdates}" var="sohUpdate">

				<c:if test="${not empty sohUpdate.outOfStockItems and fn:length(sohUpdate.outOfStockItems) gt 0}">
					<spring:theme code="basket.information.soh.name.${sohUpdate.purchaseType}" var="purchaseTypeName" />
					<p><spring:theme code="basket.information.soh.${cartMode}.outOfStockItems" arguments="${purchaseTypeName}" argumentSeparator=";;" /></p>
					<ul>
						<c:forEach items="${sohUpdate.outOfStockItems}" var="outOfStockItem">
	
							<c:remove var="outOfStockProductSize" />
							<c:forEach items="${outOfStockItem.product.baseOptions}" var="baseOptions">
								<c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
									<c:if test="${baseOptionQualifier.qualifier eq 'size' and not empty baseOptionQualifier.value}">
										<c:set var="outOfStockProductSize" value="${baseOptionQualifier.value}" />
									</c:if>
								</c:forEach>
							</c:forEach>
						
							<li>
								<c:if test="${empty outOfStockProductSize}">
									<spring:theme code="basket.information.soh.outOfStockItem" text="{0}" arguments="${outOfStockItem.product.name}" argumentSeparator=";;" />
								</c:if>
								<c:if test="${not empty outOfStockProductSize}">
									<spring:theme code="basket.information.soh.outOfStockItem.with.productSize" text="{0}" arguments="${outOfStockItem.product.name};;${outOfStockProductSize}" argumentSeparator=";;" />
								</c:if>
							</li>
						</c:forEach>
					</ul>
				</c:if>

			</c:forEach>		
			
			<c:if test="${cartMode eq 'singlecart'}">
				<c:choose>
					<c:when test="${orderCompleted}">
						<p><spring:theme code="basket.information.soh.order.completed.back" arguments="${targetUrlsMapping.home}" argumentSeparator=";;" /></p>
					</c:when>
					<c:when test="${orderPlaceFailed}">
						<p><spring:theme code="basket.information.soh.order.place.back" /></p>
					</c:when>
					<c:otherwise>
						<p><spring:theme code="basket.information.soh.back" arguments="${targetUrlsMapping.cart}" argumentSeparator=";;" /></p>
					</c:otherwise>
				</c:choose>				
			</c:if>
				
		</feedback:message>
	</div>
</c:if>