<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<form:form class="updateCartForm"
	id="updateCartForm${entry.entryNumber}"
	action="${targetUrlsMapping.cartUpdate}"
	method="post"
	commandName="updateQuantityForm${entry.product.code}"
	data-initial-quantity="${entry.quantity}"
	data-product-code="${entry.product.code}"
	novalidate="true">
	<jsp:doBody />
	<div class="basket-qty" data-initial-quantity="${entry.quantity}">
		<div class="qty-inner">
			<button type="button" class="qty-handle decrease">
				<span class="visuallyhidden"><spring:theme code="basket.page.decrease.quantity"/></span>
			</button>
			<form:input disabled="${not entry.updateable}" size="1" maxlength="2" id="quantity${entry.entryNumber}" class="text qty" value="${entry.quantity}" path="quantity" type="number" pattern="[0-9]*"/>
			<button type="button" class="qty-handle increase">
				<span class="visuallyhidden"><spring:theme code="basket.page.increase.quantity"/></span>
			</button>
		</div>
		<c:if test="${entry.updateable}" >
			<c:set var="buttonText"><spring:theme code="basket.page.update"/></c:set>
			<button type="submit" name="action" value="update" class="button-norm button-subtle disabled-button updateable cust-state-trig button-wait basket-update" data-wait-text="${buttonText}">${buttonText}</button>
			<c:remove var="buttonText" />
		</c:if>
	</div>
	<target:csrfInputToken/>
</form:form>


