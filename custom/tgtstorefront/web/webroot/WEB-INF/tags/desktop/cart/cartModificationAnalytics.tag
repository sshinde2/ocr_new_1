<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>

<c:if test="${not empty cartModification}">
	<c:set var="json">
		<json:object escapeXml="false">
			<json:property name="action" value="${cartModification.quantityAdded < 0 ? 'Remove' : 'Add'}" />
			<template:productEcommJson
				product="${cartModification.entry.product}"
				assorted="${cartModification.entry.product.assorted}"
				priceData="${cartModification.entry.basePrice}"
				quantity="${cartModification.quantityAdded < 0 ? (cartModification.quantityAdded * -1) : cartModification.quantityAdded}"/>
			<template:gtmEcommJson abstractOrderData="${cartData}" />
		</json:object>
	</c:set>
	<script type="text/x-json" id="basket-modified-json">${json}</script>
</c:if>
