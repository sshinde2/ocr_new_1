<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="stl" tagdir="/WEB-INF/tags/desktop/stl" %>
<%@ attribute name="looks" required="false" type="java.util.List" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ attribute name="unveil" required="false" type="java.lang.Boolean" %>
<%@ attribute name="deferred" required="false" type="java.lang.Boolean" %>
<%@ attribute name="width" required="false" type="java.lang.String" %>
<%@ attribute name="staticLink" required="false" type="java.lang.String" %>
<%@ attribute name="callToAction" required="false" type="java.lang.String" %>

<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>

<c:set var="perPage" value="4" />
<c:set var="carouselClassName" value="Slider--groupPage" />

<c:if test="${width eq 'wide'}">
	<c:set var="perPage" value="3" />
</c:if>
<c:if test="${fn:length(looks) <= perPage}">
	<c:set var="carouselClassName" value="${carouselClassName} Slider-collapse" />
</c:if>
<c:set var="carouselClassName" value="${carouselClassName} Slider--groupBy${perPage}" />

<comp:carousel type="group" itemCount="${fn:length(looks)}" carouselClassName="${carouselClassName}" perPage="${perPage}" hidePause="${true}">
	<jsp:attribute name="heading">
		<comp:component-heading title="${title}" staticLink="${staticLink}" />
	</jsp:attribute>
	<jsp:attribute name="itemList">
		<c:forEach items="${looks}" var="look" varStatus="status" begin="0" end="100">
			<comp:carouselItem
				url="${look.url}"
				parentClasses=""
				listAttributes=""
				linkClass="Slider-groupSubtext">
				<jsp:attribute name="image">
					<stl:lookImage look="${look}" format="grid" deferred="${deferred}" unveil="${unveil}" />
					${callToAction}
				</jsp:attribute>
			</comp:carouselItem>
		</c:forEach>
	</jsp:attribute>
</comp:carousel>
