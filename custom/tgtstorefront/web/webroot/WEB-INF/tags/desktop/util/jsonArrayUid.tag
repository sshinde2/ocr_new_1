<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%--
Rather than rendering the email address in plain readable.
We are converting email string into a array of parts.
So that it is not so easily machine understood (Look ma, no "@" character).
Still very weak obfuscation though.

Conversion
	glenn.baker2@target.com.au ==> {"uidp0":["glenn", "baker2"],"uidp1":["target","com", "au"]}

GTM macro that reconstitutes it is very simple
	function exactTargetEmail() {
	    return ({{uidp0}}).join('.') + "@" + ({{uidp1}}).join('.')
	}

--%>
<c:if test="${fn:contains(user.uid, '@')}">
	<c:set var="emailSplit" value="${fn:split(user.uid, '@')}" />
	<c:if test="${fn:length(emailSplit) eq 2}">
		<json:array name="uidp0" items="${fn:split(emailSplit[0], '.')}" />
		<json:array name="uidp1" items="${fn:split(emailSplit[1], '.')}" />
	</c:if>
</c:if>
