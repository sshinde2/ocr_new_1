<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="listerType" required="false" type="java.lang.String" %>
<%@ attribute name="listerName" required="false" type="java.lang.String" %>
<%@ attribute name="heading" required="true" %>
<%@ attribute name="championSlot" required="false" fragment="true" %>
<%@ attribute name="mainAbove" required="false" fragment="true" %>
<%@ attribute name="mainBelow" required="false" fragment="true" %>
<%@ attribute name="asideAbove" required="false" fragment="true" %>
<%@ attribute name="asideBelow" required="false" fragment="true" %>
<%@ attribute name="supplementAbove" required="false" fragment="true" %>
<%@ attribute name="supplementBelow" required="false" fragment="true" %>
<%@ attribute name="hideDetails" required="false" %>
<%@ attribute name="unveilStartPosition" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="category" tagdir="/WEB-INF/tags/desktop/category" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<feature:enabled name="uiReactListing">
	<c:set var="uiReactListing" value="${true}" />
</feature:enabled>

<c:set var="wasError" value="${not empty searchPageData.errorStatus}" />
<c:set var="hasProducts" value="${not wasError and fn:length(searchPageData.results) > 0}" />

<feature:enabled name="uiNewFacetDisplay">
	<c:set var="multiselectable" value="${true}" />
</feature:enabled>

<feature:enabled name="uiTrackDisplayOnly">
	<c:set var="trackDisplayOnly" value="${true}" />
</feature:enabled>

<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>
	<jsp:invoke fragment="championSlot" />

	<div class="inner-content ${not wasError ? ' has-aside' : ''} ${multiselectable ? ' multiselectable-content' : ''}">
		<c:if test="${not wasError}">
			<div class="aside">
				<jsp:invoke fragment="asideAbove" />
				<feature:enabled name="newSubCategoriesUx">
					<category:categoryListerHead extraClass="only-for-small">
						<jsp:attribute name="below">
							<h3 class="only-for-small">
								<strong class="total-item-count">
									${searchPageData.pagination.totalNumberOfResults}
								</strong> items
							</h3>
						</jsp:attribute>
					</category:categoryListerHead>
				</feature:enabled>
				<nav:categoryNav pageData="${searchPageData}" />
				<div class="promos hide-for-small">
					<c:set var="contentSlotContext" value="narrow" scope="request" />
					<cms:slot var="leftColumnComponent" contentSlot="${slots.Aside}">
						<cms:component component="${leftColumnComponent}"/>
					</cms:slot>
					<c:remove var="contentSlotContext" scope="request" />
				</div>
				<jsp:invoke fragment="asideBelow" />
			</div>
		</c:if>
		<div class="main">
			<jsp:invoke fragment="mainAbove" var="mainAbove" />
			<c:out value="${mainAbove}" escapeXml="false" />

			<kiosk:categoryFacetMenu pageData="${searchPageData}" />

			<nav:productRefineTop
				searchPageData="${searchPageData}"
				searchUrl="${searchPageData.currentQuery.url}"
				wsUrl="${searchPageData.currentQuery.wsUrl}"
				urlElements="${urlElements}"
				contentAboveRefineTop="${not empty mainAbove}"
				hasProducts="${hasProducts}"
				/>
			<c:choose>
				<c:when test="${hasProducts}">
					<c:choose>
						<c:when test="${uiReactListing}">
							<div class="product-listing react-root" data-lister-name="${listerName}" data-component="productListingView">
						</c:when>
						<c:otherwise>
							<div class="product-listing" data-lister-name="${listerName}">
						</c:otherwise>
					</c:choose>
						<feature:enabled name="featureWishlistFavourites">
							<c:if test="${not kioskMode and not iosAppMode}">
								<c:set var="listClass">FavouritableProducts</c:set>
							</c:if>
						</feature:enabled>
						<product:productBaseLayout data="${favouritesData}" listerType="${listerType}" listClass="${listClass}">
							<c:forEach items="${searchPageData.results}" var="product" varStatus="status">
								<product:productListerItem
									listerType="${listerType}"
									listerName="${listerName}"
									product="${product}"
									showBasketButton="true"
									position="${status.index}"
									absoluteIndex="${(searchPageData.pagination.currentPage * searchPageData.pagination.pageSize) + status.count}"
									hideDetails="${hideDetails}"
									unveilStartPosition="${unveilStartPosition}"
									trackDisplayOnly="${trackDisplayOnly}" />
							</c:forEach>
						</product:productBaseLayout>
						<c:if test="${uiReactListing}">
							<util:reactInitialState
								listingData='${productList}'
								applicationData='"page": {"type": "Listing"}' />
						</c:if>
					</div>
				</c:when>
				<c:when test="${wasError}">
					<h3 class="category-message results-problem" data-results="${listerName}" data-problem="Error">
						<span class="tiny-break"><spring:theme code="search.products.error" /></span>
						<%-- RFC 2396 4.2 A URI reference that does not contain a URI is a reference to the current document. --%>
						<span class="tiny-break"><a href=""><spring:theme code="search.products.tryagain" /></a><span>
					</h3>
					<div class="product-listing"></div>
				</c:when>
				<c:when test="${pageType eq 'DealCategory'}">
					<h5 class="category-message results-problem" data-results="${listerName}" data-problem="No results">
						<spring:theme code="deal.category.products.empty" />
					</h5>
					<div class="product-listing"></div>
				</c:when>
				<c:otherwise>
					<h5 class="category-message results-problem" data-results="${listerName}" data-problem="No results"><spring:theme code="${not empty bulkyBoardStoreNumber ? 'kiosk.bulkyboard.' : ''}search.products.empty" /></h5>
					<div class="product-listing"></div>
				</c:otherwise>
			</c:choose>
			<nav:productRefineBottom
				searchPageData="${searchPageData}"
				searchUrl="${searchPageData.currentQuery.url}"
				wsUrl="${searchPageData.currentQuery.wsUrl}"
				hasProducts="${hasProducts}"
				/>
			<jsp:invoke fragment="mainBelow" />
		</div>
		<div class="supplement">
			<jsp:invoke fragment="supplementAbove" />
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:slot var="supplementComponent" contentSlot="${slots.Supplement}">
				<cms:component component="${supplementComponent}"/>
			</cms:slot>
			<c:remove var="contentSlotContext" scope="request" />
			<jsp:invoke fragment="supplementBelow" />
		</div>
	</div>
	<common:handlebarsTemplates template="listerItemTemplate" />
</template:page>
