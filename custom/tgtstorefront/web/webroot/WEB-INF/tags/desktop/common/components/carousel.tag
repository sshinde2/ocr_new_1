<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="itemList" required="false" fragment="true" %>
<%@ attribute name="itemCount" required="false" type="java.lang.Integer" %>
<%@ attribute name="perPage" required="false" type="java.lang.Integer" %>
<%@ attribute name="carouselClassName" required="false" type="java.lang.String" %>
<%@ attribute name="type" required="false" type="java.lang.String" %>
<%@ attribute name="hidePause" required="false" type="java.lang.Boolean" %>
<%@ attribute name="heading" required="false" fragment="true" %>


<c:set var="carouselHeadingClass" value="carousel-heading" />
<c:if test="${not empty type}">
	<c:set var="carouselHeadingClass" value="${type}-${carouselHeadingClass}" />
	<c:set var="sliderType" value="data-slider-type='${type}'" />
</c:if>
<jsp:invoke fragment="heading" var="headingObject" />

<c:if test="${itemCount gt 0}">
	<div class="carousel-container">
		<c:if test="${not empty headingObject}">
			<div class="${carouselHeadingClass}">
				${headingObject}
			</div>
		</c:if>
		<div class="${type}-carousel">
			<div class="Slider ${carouselClassName}" ${sliderType} data-per-page="${perPage}" data-small-per-page="1" data-tiny-per-page="1" >
				<div class="Slider-container">
					<ul class="Slider-slides">
						<jsp:invoke fragment="itemList" />
					</ul>
				</div>
				<c:if test="${itemCount gt perPage}">
					<nav:sliderControls hidePause="${hidePause}" />
				</c:if>
			</div>
		</div>
	</div>
</c:if>
