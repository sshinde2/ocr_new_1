<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<script type="text/x-handlebars-template" id="FindNearestStoreTemplate">
	{{#if stores}}
		{{#unless stockDataIncluded}}
			<h2 class="title u-alignCenter u-cocogoose FindNearestStore-subHeading"><spring:theme code="instore.stock.preferred.heading" /></h2>
			<div class="FindNearestStore-storeDetailsHeading">
				<div class="FindNearestStore-storeDetailsHeadingName"><spring:theme code="storeFinder.storeDetails.name.heading" /></div>
				<div class="FindNearestStore-storeDetailsHeadingDistance"><spring:theme code="storeFinder.storeDetails.distance.heading" /></div>
				<div class="FindNearestStore-empty">&nbsp;</div>
			</div>
		{{/unless}}
		<div class="FindNearestStore-storesList">
			{{#each stores}}
				{{> StoreListItemTemplate stockDataIncluded=../stockDataIncluded}}
			{{/each}}
		</div>
		{{#if stockDataIncluded}}
			<div class="FindNearestStore-moreControls">
				<button class="FindNearestStore-moreStores" type="button"><spring:theme code="instore.stock.store.moreStores"/></button>
				<feedback:message type="warning" size="small" minimal="${true}" cssClass="is-no-bg">
					<p class="Feedback-feedbackMsg"><i class="Icon Icon--warning Icon--size150"></i> <spring:theme code="product.fis.tryAgain" /></p>
				</feedback:message>
			</div>
			<p class="StoreDetails-availabilityTime"><spring:theme code="instore.stock.store.stockavailabilitytime"/>{{availabilityTime}}</p>
		{{/if}}
	{{/if}}
</script>
