<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<a href="#skip-to-content" class="visuallyhidden" data-role="none"><spring:theme code="text.skipToContent" text="Skip to Content"/></a>
<a href="#skiptonavigation" class="visuallyhidden" data-role="none"><spring:theme code="text.skipToNavigation" text="Skip to Navigation"/></a>