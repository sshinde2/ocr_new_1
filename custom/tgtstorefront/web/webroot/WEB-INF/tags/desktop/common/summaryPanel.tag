<%@ tag trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ attribute name="headingCode" required="false" %>
<%@ attribute name="actionCode" required="false" %>
<%@ attribute name="actionUrl" required="false" %>
<%@ attribute name="bottomActionCode" required="false" type="java.lang.String" %>
<%@ attribute name="bottomActionUrl" required="false" type="java.lang.String" %>
<%@ attribute name="actionContent" required="false" %>
<%@ attribute name="fullContent" required="false" type="java.lang.Boolean" %>


<div class="summary-groupe">
	<c:if test="${not empty headingCode}">
		<h3><spring:theme code="${headingCode}" /></h3>
	</c:if>
	<div class="summary-panel ${fullContent ? 'summary-full' : ''}">
		<c:if test="${not empty actionUrl}">
			<div class="PanelActions">
				<a class="button-small-text button-norm button-small summary-panel--changeButton" href="${actionUrl}"><spring:theme code="${actionCode}" /></a>
			</div>
		</c:if>
		<div class="summary-detail ${not empty actionContent or not empty actionUrl ? ' withButton' : ''}">
			<jsp:doBody />
		</div>
		<c:if test="${not empty actionContent}">
			<div class="PanelActions">
				${actionContent}
			</div>
		</c:if>
		<c:if test="${not empty bottomActionUrl}">
			<a class="button-small-text button-fwd-alt button-small summary-panel--bottomButton" href="${bottomActionUrl}"><spring:theme code="${bottomActionCode}" /></a>
		</c:if>
	</div>
</div>