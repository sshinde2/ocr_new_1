<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<script type="text/x-handlebars-template" id="FacetAccordionTemplate">
	<spring:theme code="search.nav.facet.nofollow" var="nofollowStr" />
	{{#if isCategory}}
		<ul class="FacetCategory">
			{{#each facetOptionList as |facet|}}
				<facet:item
					url="{{facet.url}}"
					name="{{{facet.name}}}"
					count="{{facet.count}}"
					type="categoryList"
					selected="{{facet.selected}}"
					category="{{{../name}}}"
					id="{{{facet.name}}}"
					additionalAttributes="{{#unless ../seoFollowEnabled}}${nofollowStr}{{/unless}}" />
			{{/each}}
		</ul>
	{{else if isolated}}
		<ul class="FacetIsolated">
			{{#each facetOptionList as |facet|}}
				<facet:item
					url="{{facet.url}}"
					name="{{{facet.name}}}"
					count="{{facet.count}}"
					itemClass="{{#if facet.selected}}selected{{/if}}"
					category="{{{../name}}}"
					id="{{facet.id}}"
					queryUrl="{{queryUrl}}"
					additionalAttributes="{{#unless ../seoFollowEnabled}}${nofollowStr}{{/unless}}"
				/>
			{{/each}}
		</ul>
	{{else}}
		{{> FacetItemTemplate }}
	{{/if}}
</script>
