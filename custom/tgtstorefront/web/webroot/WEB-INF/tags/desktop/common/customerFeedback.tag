<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<c:set var="customerFeedback">
	<cms:slot var="component" contentSlot="${slots.CustomerFeedback}">
		<cms:component component="${component}" />
	</cms:slot>
</c:set>

<c:if test="${not empty customerFeedback}">
	${customerFeedback}
</c:if>
