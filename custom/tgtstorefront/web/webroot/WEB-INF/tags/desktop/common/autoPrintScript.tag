<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Launch the print dialog if present in URL --%>
<script type="text/javascript">
	(function( win ){
		if ( win.location.href.indexOf('printModal') > -1 ){

			document.documentElement.className += ' modal-print-page';

			setTimeout(function(){
				win.print();
			}, 1000);
		}
	})(window);
</script>