<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ attribute name="selectedStore" required="true" type="java.lang.Object" %>
<%@ attribute name="editUrl" required="false" type="java.lang.String" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>
<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />

<common:summaryPanel 
	headingCode="order.summarycard.${customerCode}.heading.selectedStore"
	actionCode="order.summarycard.${customerCode}.changeStore"
	actionUrl="${editUrl}">

	<div class="store-summary">
		<store:storeAddress store="${selectedStore}"/>
		<p>
			<a href="/checkout/store?ssn=${selectedStore.storeNumber}" class="lb" data-iw="750" >
				<spring:theme code="order.summarycard.${customerCode}.viewStoreDetails" text="View store details"/>
			</a>
		</p>
	</div>
</common:summaryPanel>
