<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="paymentsInfo" required="true" type="java.lang.Object" %>
<%@ attribute name="flybuysNumber" required="false" type="java.lang.String" %>
<%@ attribute name="editUrl" required="false" type="java.lang.String" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>
<%@ attribute name="hidePayment" required="false" type="java.lang.Boolean" %>

<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />

<c:if test="${not hidePayment or not empty flybuysNumber}">

	<common:summaryPanel
		headingCode="order.summarycard.${customerCode}.heading.${not hidePayment ? 'paymentDetails' : 'flybuysDetails'}"
		actionCode="order.summarycard.${customerCode}.editDetails"
		actionUrl="${editUrl}">

		<c:if test="${not hidePayment and not empty paymentsInfo}">
			<c:forEach var="payment" items="${paymentsInfo}">
				<dl>
					<c:choose>
						<c:when test="${payment.paypalPaymentInfo}">
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paymentType" /></dt>
							<dd>${fn:escapeXml(payment.paypalInfoData.paymentProvider)}</dd>
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paypalEmailId" /></dt>
							<dd>${fn:escapeXml(payment.paypalInfoData.paypalEmailId)}</dd>
						</c:when>
						<c:when test="${payment.pinPadPaymentInfo}">
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paymentType" /></dt>
							<dd>${fn:escapeXml(payment.cardTypeData.name)}</dd>
							<dt><spring:theme code="order.summarycard.${customerCode}.label.cardNumber" /></dt>
							<dd>${fn:escapeXml(payment.pinPadPaymentInfoData.maskedCardNumber)}</dd>
						</c:when>
						<c:when test="${payment.payPalHerePaymentInfo}">
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paymentType" /></dt>
							<dd>${fn:escapeXml(payment.payPalHereInfoData.paymentProvider)}</dd>
						</c:when>
						<c:when test="${payment.afterpayPaymentInfo}">
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paymentType" /></dt>
							<dd>${fn:escapeXml(payment.afterpayPaymentInfoData.paymentProvider)}</dd>
						</c:when>
						<c:when test="${payment.zipPaymentInfo}">
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paymentType" /></dt>
							<dd>${fn:escapeXml(payment.zipPaymentInfoData.paymentProvider)}</dd>
						</c:when>
						<c:otherwise>
							<dt><spring:theme code="order.summarycard.${customerCode}.label.paymentType" /></dt>
							<dd>${fn:escapeXml(payment.cardTypeData.name)}</dd>
							<dt><spring:theme code="order.summarycard.${customerCode}.label.cardNumber" /></dt>
							<dd>${fn:escapeXml(payment.cardNumber)}</dd>
							<c:if test="${not empty payment.accountHolderName}">
								<dt><spring:theme code="order.summarycard.${customerCode}.label.nameOnCard" /></dt>
								<dd>${fn:escapeXml(payment.accountHolderName)}</dd>
							</c:if>
							<c:if test="${not empty payment.expiryMonth and not empty payment.expiryYear}">
								<dt><spring:theme code="order.summarycard.${customerCode}.label.expiryDate" /></dt>
								<dd>${fn:escapeXml(payment.expiryMonth)}/${fn:escapeXml(payment.expiryYear)}</dd>
							</c:if>
							<c:if test="${not empty payment.amount}">
								<dt><spring:theme code="order.summarycard.${customerCode}.label.amount" /></dt>
								<dd><format:price priceData="${payment.amount}" displayFreeForZero="true"/></dd>
							</c:if>
						</c:otherwise>
					</c:choose>
					<c:if test="${not empty payment.receiptNumber}">
						<dt><spring:theme code="order.summarycard.${customerCode}.label.receiptNumber" /></dt>
						<dd>${fn:escapeXml(payment.receiptNumber)}</dd>
					</c:if>
				</dl>
			</c:forEach>
		</c:if>

		<c:if test="${not empty flybuysNumber}">
			<dl>
				<dt><spring:theme code="order.summarycard.${customerCode}.label.flybuysDetails" /></dt>
				<dd>${fn:escapeXml(flybuysNumber)}</dd>
			</dl>
		</c:if>
	</common:summaryPanel>

</c:if>
