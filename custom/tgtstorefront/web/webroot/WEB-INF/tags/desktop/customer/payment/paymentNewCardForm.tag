<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<%-- Divider / Heading --%>
<h3><spring:theme code="checkout.multi.paymentDetails.newPaymentDetails.paymentCard"/></h3>
<p><spring:theme code="form.required"/></p>

<%-- Make a payment amount --%>
<form:hidden path="amountCurrentPayment" />

<%-- Gateway Hidden Fields--%>
<input type="hidden" name="gatewayReturnURL" value="${paymentReturnUrl}" />
<input type="hidden" name="gatewayRedirectDisplayBackgroundColor" value="#FFFFFF" />
<input type="hidden" name="gatewayRedirectDisplayContinueButtonText" value="Continue"/>
<input type="hidden" name="paymentSessionId" value="${newCardPaymentDetailsForm.paymentSessionId}"/>

<%-- Card Details --%>
<formElement:formInputBox idKey="payment.nameOnCard" labelKey="payment.nameOnCard" path="nameOnCard" inputCSS="text" mandatory="true" tabindex="1"/>
<formElement:formInputBox clearValue="true" idKey="payment.cardNumber" labelKey="payment.cardNumber" path="gatewayCardNumber" inputCSS="text" mandatory="true" tabindex="2" maxlength="16" pattern="[0-9]*" autoComplete="off" />

<c:forEach items="${fn:split('gatewayCardExpiryDateMonth,gatewayCardExpiryDateYear,gatewayCardSecurityCode', ',')}" var="fieldPath">
	<spring:bind path="${fieldPath}">
		<c:if test="${not empty status.errorMessages}">
			<c:set var="cardDetailsErrors">
				${cardDetailsErrors}<span class="f-err-line"><form:errors path="${fieldPath}" /></span>
			</c:set>
		</c:if>
	</spring:bind>
</c:forEach>

<div class="card-security f-element ${not empty cardDetailsErrors ? ' f-error' : ''}">
	<div class="f-set">
		<div class="card-detail card-month">
			<formElement:formSelectBox idKey="payment.expiryMonth" labelKey="payment.expiryMonth" selectCSSClass="chosen-select" path="gatewayCardExpiryDateMonth" mandatory="true" skipBlank="false" items="${months}" tabindex="3" hideError="true" skipBlankMessageKey="payment.selectMonth" />
		</div>
		<div class="card-detail card-year">
			<formElement:formSelectBox idKey="payment.expiryYear" labelKey="payment.expiryYear" selectCSSClass="chosen-select" path="gatewayCardExpiryDateYear" mandatory="true" skipBlank="false" items="${expiryYears}" tabindex="4" hideError="true" skipBlankMessageKey="payment.selectYear"/>
		</div>
		<div class="card-detail card-cvv">
			<formElement:formInputBox clearValue="true" idKey="payment.issueNumber" labelKey="checkout.security.code" path="gatewayCardSecurityCode" inputCSS="text mini-text" mandatory="true" maxlength="4" tabindex="6" hideError="true" hintLink="payment.security.hintLink" hint="payment.security.hint" pattern="[0-9]*" autoComplete="off" />
		</div>
	</div>
	<c:if test="${not empty cardDetailsErrors}">
		<p class="f-err-msg error-message">${cardDetailsErrors}</p>
	</c:if>
</div>

<customer-payment:ssl-message />

<div class="secured-by">
	<c:if test="${cartData.allowSavePayment and featuresEnabled.featureTnsRememberCard}">
		<formElement:formOptCheckbox idKey="SaveDetails" labelKey="payment.saveInAccount" path="saveInAccount" tabindex="17" checked="${true}" />
	</c:if>
	<c:set var="secureLogo"><asset:resource code="payment.secureLogo" /></c:set>
	<img src="${secureLogo}" class="security-logo" width="112" height="36" />
</div>
