<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="kioskNav" tagdir="/WEB-INF/tags/desktop/kiosk/navigation" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>



<div class="rich-tile kiosk-signout">
	<form:form action="${targetUrlsMapping.logoutKiosk}" method="post">
		<button type="submit" class="kiosk-signout-button cust-state-trig">
			<span class="heading">
				<span class="head-content">
					<span class="rich-icon"></span>
					<span class="text"><spring:theme code="kiosk.text.leftNav.signout" /></span>
				</span>
			</span>
		</button>
		<target:csrfInputToken/>
	</form:form>
</div>
