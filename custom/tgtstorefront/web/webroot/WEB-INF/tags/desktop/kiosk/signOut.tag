<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="kioskNav" tagdir="/WEB-INF/tags/desktop/kiosk/navigation" %>

<div class="kiosk-customer-links">
	<c:if test="${not anonymousCachable}">
		<sec:authorize access="hasAnyRole('ROLE_CUSTOMERGROUP')">
			<kioskNav:signOutTile />
		</sec:authorize>
	</c:if>
</div>

<c:if test="${anonymousCachable}">
	<script>if(window.t_Inject){t_Inject('kioskSignout');}</script>
</c:if>