<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="true" type="java.lang.String"%>
<%@ attribute name="stepName" required="true" type="java.lang.String"%>
<%@ attribute name="stepsFinished" required="false" type="java.lang.Boolean"%>
<%@ attribute name="hideHome" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/desktop/common/footer" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="kioskNav" tagdir="/WEB-INF/tags/desktop/kiosk/navigation" %>

<template:master pageTitle="${pageTitle}" checkoutStepName="${stepName}">

	<jsp:attribute name="optionalMeta">
		<meta name="format-detection" content="telephone=no">
	</jsp:attribute>
	<jsp:attribute name="cmsExternalCss">
		<template:cmsExternalCss />
	</jsp:attribute>
	<jsp:attribute name="cmsInlineCss">
		<template:cmsInlineCss />
	</jsp:attribute>
	<jsp:attribute name="cmsExternalJavaScript">
		<template:cmsExternalJavaScript />
	</jsp:attribute>
	<jsp:attribute name="cmsInlineJavaScript">
		<template:cmsInlineJavaScript />
	</jsp:attribute>
	<jsp:attribute name="outerNavigation">
		<kioskNav:navigation hideBack="${stepsFinished}" hideHome="${hideHome}" />
	</jsp:attribute>
	<jsp:body>
		<header:alertbar />
		<div id="wrapper" data-context-path="${request.contextPath}">
			<div id="page">
				<header:headerCheckout stepName="${stepName}" stepsFinished="${stepsFinished}" />
				<template:flush />
				<div class="content">
					<jsp:doBody/>
				</div>
			</div>
		</div>
		<c:if test="${not mobileAppMode}">
			<footer:footerCheckout />
		</c:if>
	</jsp:body>
	
</template:master>
