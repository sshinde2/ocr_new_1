<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty cmsPage.inlineJavascript}">
	<script type="text/javascript">
		<c:choose>
			<c:when test="${fn:contains( cmsPage.inlineJavascript, 'require.config(' )}">
				${cmsPage.inlineJavascript}
			</c:when>
			<c:otherwise>
				require(['main'], function($) {
					${cmsPage.inlineJavascript}
				});
			</c:otherwise>
		</c:choose>
	</script>
</c:if>