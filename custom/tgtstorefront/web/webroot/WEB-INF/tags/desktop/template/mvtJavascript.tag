<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="breadcrumbStatus">
	<%-- The last breadcrumb object has no categoryCode when on the PDP. --%>
	<c:set var="delimiter" value="${not breadcrumbStatus.first and not empty breadcrumb.categoryCode ? '|' : ''}" />
	<c:set var="categoryPath" value="${categoryPath}${delimiter}${breadcrumb.categoryCode}" />

	<c:if test="${not breadcrumbStatus.last and not empty breadcrumb.name}">
		<c:set var="categoryNamePath" value="${categoryNamePath}${delimiter}${breadcrumb.name}"/>
		<c:set var="lowestCategoryName" value="${breadcrumb.name}"/>
	</c:if>
</c:forEach>
<c:set var="brandCode" value="${pageType == 'Brand' ? categoryName : product.brand}" />

<%-- strip out any double quote chars --%>
<c:set var="categoryPath" value="${fn:replace(categoryPath, '\"', '')}" />
<c:set var="brandCode" value="${fn:replace(brandCode, '\"', '')}" />

<c:if test="${not empty categoryPath or not empty brandCode}">
	<script>
		<c:if test="${not empty categoryPath}">
			var MVT_CATEGORY_PATH = "${categoryPath}";
		</c:if>
		<c:if test="${not empty brandCode}">
			var MVT_BRAND_CODE = "${brandCode}";
		</c:if>
		<c:if test="${not empty product.baseName}">
			var TARGET_META_PRODUCT_NAME = "${product.baseName}";
		</c:if>
		<c:if test="${not empty product.baseProductCode}">
			var TARGET_META_PRODUCT_BASE_CODE = "${product.baseProductCode}";
		</c:if>
		<c:if test="${not empty categoryNamePath}">
			var TARGET_META_CATEGORY_PATH_FRIENDLY = "${categoryNamePath}";
		</c:if>
		<c:if test="${not empty product.topLevelCategory}">
			var TARGET_META_CATEGORY_TOP = "${product.topLevelCategory}";
		</c:if>
		<c:if test="${not empty lowestCategoryName}">
			var TARGET_META_CATEGORY_LOWEST = "${lowestCategoryName}";
		</c:if>
	</script>
</c:if>
