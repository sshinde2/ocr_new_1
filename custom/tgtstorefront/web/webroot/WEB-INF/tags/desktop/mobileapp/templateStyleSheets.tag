<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:if test="${mobileAppMode}">
	<c:set var="mobileAppCss">
		<asset:resource critical="css/mobile-app.css" basePath="${themeResourcePath}" />
	</c:set>
	<link href="${mobileAppCss}" rel="stylesheet" type="text/css" />
</c:if>
