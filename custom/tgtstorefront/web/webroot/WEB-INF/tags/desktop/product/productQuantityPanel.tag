<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="inStock" required="true" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- Quantity --%>
<feature:disabled name="uiPDPaddToCart">
	<div class="prod-qty">
		<c:if test="${inStock}">
			<label for="qty"><spring:theme code="basket.page.quantity" /></label>
			<div class="prod-qty-group">
				<button type="button" class="qty-handle decrease"></button>
				<input type="text" maxlength="2" id="qty" name="qty" class="qty text" value="1" pattern="[0-9]*">
				<button type="button" class="qty-handle increase"></button>
			</div>
		</c:if>
	</div>
</feature:disabled>

