<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ attribute name="tabClass" required="true" type="java.lang.String" %>
<%@ attribute name="tabContentClass" required="true" type="java.lang.String" %>
<%@ attribute name="showDetails" required="false" type="java.lang.Boolean" %>
<%@ attribute name="pageMode" required="false" type="java.lang.String" %>
<%@ attribute name="tabMenuClass" required="false" type="java.lang.String" %>

<c:set var="showDescTab" value="${not empty product.youTubeLink or not empty product.description}" />
<c:set var="showFeatureTab" value="${not empty product.productFeatures or not empty product.materials or not empty product.careInstructions or not empty product.extraInfo}" />
<c:set var="tabMenuClass" value="${not empty tabMenuClass ? tabMenuClass : ''}" />


<div class="${tabClass}">

	<ul class="tab-menu ${tabMenuClass}">
		<c:if test="${showDetails}">
			<li class="tab-handle ui-tabs-active"><a href="#tab-details" ><h2 class="tab-menu-heading"><spring:theme code="product.tab.details" /></h2></a></li>
		</c:if>
		<c:if test="${showDescTab}">
			<li class="tab-handle ${showDetails ? '' : ' ui-tabs-active '} "><a href="#tab-description" ><h2 class="tab-menu-heading"><spring:theme code="product.tab.description" /></h2></a></li>
		</c:if>
		<c:if test="${showFeatureTab}">
			<li class="tab-handle"><a href="#tab-features"><h2 class="tab-menu-heading"><spring:theme code="product.tab.features" /></h2></a></li>
		</c:if>
	</ul>

	<c:if test="${showDetails}">
		<div class="product-group-tab-content" id="tab-details">
			<product:productDetailsPanel
				product="${product}"
				hideDeals="${true}"
				hideDeliveryConcern="${true}"
				hideDetailsAvailable="${true}"
				productGroup="${true}"
				hideTabs="${true}"
				pageMode="${pageMode}"
				hideStoreDetails="${true}" />
		</div>
	</c:if>

	<c:if test="${showDescTab}">
		<div class="${tabContentClass} ${showDetails ? ' tab-inactive ' : ''} product-description" id="tab-description">
			<div class="variant-changeable">
				<h3 class="visuallyhidden"><spring:theme code="product.tab.description" /></h3>
				<product:productDescription product="${product}" />
			</div>
		</div>
	</c:if>
	<c:if test="${showFeatureTab}">
		<div class="${tabContentClass} tab-inactive product-features" id="tab-features">
			<div class="variant-changeable">
				<h3 class="visuallyhidden"><spring:theme code="product.tab.features" /></h3>
				<product:productFeatures product="${product}" />
			</div>

		</div>
	</c:if>

	<%--
	<cms:slot var="tabs" contentSlot="${slots.Tabs}">
		<cms:component component="${tabs}"/>
	</cms:slot>
	--%>

</div>
