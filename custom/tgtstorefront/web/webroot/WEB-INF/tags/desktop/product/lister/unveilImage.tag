<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="src" required="false" type="java.lang.String" %>
<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="className" required="false" type="java.lang.String" %>

<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>
<img src="${emptypixel}" alt="${name}" title="${name}" data-src="${src}" class="${className} unveil-img" /><%--
--%><noscript><%--
	--%><img src="${src}" alt="${name}" title="${name}" class="${className}" /><%--
--%></noscript>
