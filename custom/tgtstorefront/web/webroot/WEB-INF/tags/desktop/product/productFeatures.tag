<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty product.productFeatures}">
	<h6><spring:theme code="product.features.title" text="Product Features" /></h6>
	<ul>
		<c:forEach var="feature" items="${product.productFeatures}">
			<li><c:out value="${feature}"/></li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${not empty product.materials}">
	<h6><spring:theme code="product.materials.title" text="Materials and Composition" /></h6>
	<ul>
		<c:forEach var="material" items="${product.materials}">
			<li><c:out value="${material}"/></li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${not empty product.careInstructions}">
	<h6><spring:theme code="product.careInstructions.title" text="Care Instructions" /></h6>
	<ul>
		<c:forEach var="care" items="${product.careInstructions}">
			<li><c:out value="${care}"/></li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${not empty product.extraInfo}">
	<h6><spring:theme code="product.extraInfo.title" text="Extra Info" /></h6>
	<p><c:out value="${product.extraInfo}" escapeXml="false"/></p>
</c:if>
