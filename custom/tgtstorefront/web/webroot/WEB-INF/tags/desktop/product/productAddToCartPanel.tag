<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="allowAddToCart" required="true" type="java.lang.Boolean" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="inStock" required="true" type="java.lang.Boolean" %>
<%@ attribute name="inStockAtStore" required="true" type="java.lang.Boolean" %>
<%@ attribute name="variantSelected" required="true" type="java.lang.Boolean" %>
<%@ attribute name="productGroup" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disableKioskFeatures" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideStock" required="false" type="java.lang.Boolean" %>
<%@ attribute name="colours" required="true" type="java.lang.Object" %>
<%@ attribute name="sizes" required="true" type="java.lang.Object" %>
<%@ attribute name="hideStoreDetails" required="true" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="assets" tagdir="/WEB-INF/tags/desktop/assets" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<feature:enabled name="uiPDPaddToCart">
	<c:set var="newAdd" value="${true}" />
	<c:set var="feedbackClass" value="slideUp" />
</feature:enabled>

<feature:enabled name="findInStore2PDP">
	<c:set var="newPDP" value="${true}" />
</feature:enabled>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedPDP" value="${true}" />
</feature:enabled>

<%-- TODO: Cleanup "Markup" variables when layout switched on permanently --%>

<c:set var="priceMarkup">
	<div class="variant-changeable ${newPDP ? ' Price' : ' prod-price'}">
		<product:pricePoint
			product="${product}"
			nudge="${variantSelected and not product.showWasPrice}"
			newStyle="${newPDP}"
			smallCurrencySign="${newPDP}" />
	</div>
</c:set>

<c:set var="urgencyMarkup">
	<c:if test="${product.totalInterest gt 1}">
		<c:set var="urgencyMessage"><spring:theme code="product.urgency.message" arguments="${product.totalInterest}" /></c:set>
		<c:set var="urgencyLevel" value="low" />
		<c:if test="${product.totalInterest ge  product.maxThresholdInterest}">
			<c:set var="urgencyMessage"><spring:theme code="product.urgency.message.high"/></c:set>
			<c:set var="urgencyLevel" value="high" />
		</c:if>
		<div class="ProdUrgency ${newPDP ? 'u-alignCenter' : ''}">
			<p class="ProdUrgency-message"><span class="ProdUrgency-Icon ProdUrgency-Icon--${urgencyLevel}">${urgencyMessage}</span></p>
		</div>
	</c:if>
</c:set>

<c:url value="/cart/add" var="cartAddUrl" />
<form:form method="post"
	commandName="addToCartForm"
	id="addToCartForm"
	class="add-to-cart-form"
	action="${cartAddUrl}"
	novalidate="true"
	data-feedback-classes="${feedbackClass}"
	data-product-group="${productGroup} "
	data-new-add="${newAdd}" >

	<input type="hidden" name="productCode" value="${product.code}"/>

	<c:if test="${not product.giftCard and not product.preview}">
		<product:productQuantityPanel product="${product}" inStock="${inStock}" />
	</c:if>

	<div class="prod-price-stock">

		<c:if test="${not newPDP}">
			${priceMarkup}
		</c:if>

		<c:if test="${not product.preview}">
			<div class="stock-info variant-changeable">
				<product:productOfferMicroformat offerAggregate="${not variantSelected}" inStock="${inStock}" inStockAtStore="${inStockAtStore}" >
					<kiosk:productInStoreStock product="${product}" showInStock="true" />

					<c:if test="${not product.giftCard}">
						<c:choose>
							<c:when test="${not consolidatedPDP}">
								<product:productStockLevel
									product="${product}"
									variantSelected="${variantSelected}"
									colours="${colours}"
									sizes="${sizes}"
									messageOnly="${newPDP}"
									/>
							</c:when>
							<c:otherwise>
								<div class="visuallyhidden" ${itempropAvailability}></div>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${newPDP}">
						${priceMarkup}
					</c:if>

					<kiosk:productInStoreStock product="${product}" showInStock="false" />
				</product:productOfferMicroformat>
				<c:if test="${variantSelected}">
					<div class="prod-code">
						<c:set var="productCode"><span itemprop="productID">${product.code}</span></c:set>
						<spring:theme code="product.itemCode.label" text="{0}" arguments="${productCode}" />
					</div>
				</c:if>
			</div>
		</c:if>

	</div>

	<c:if test="${not product.excludeForAfterpay}">
		<product:productAfterpayMessage product="${product}" />
	</c:if>

	<c:if test="${newPDP}">
		${urgencyMarkup}
	</c:if>

	<c:if test="${product.giftCard and product.productTypeCode eq 'digital' and not product.preview}">
		<div class="prod-form">
			<product:productGiftRecipientForm />
		</div>
	</c:if>

	<feature:disabled name="uiPDPaddToCart">
		<product:productAssortmentDisclaimer product="${product}" />
	</feature:disabled>

	<%-- Add to basket --%>
	<c:if test="${not (kioskMode and product.productTypeCode eq 'giftCard')}">
		<div class="prod-button-container">
			<div class="prod-actions">
				<div class="prod-buttons ${newAdd ? 'ProdButtons' : ''}">
					<c:if test="${not disableKioskFeatures and not product.preview}">
						<%-- The Purchase In Store button --%>
						<kiosk:purchaseInStore product="${product}" />
					</c:if>
					<%-- The buy now button --%>
				<feature:disabled name="uiPDPaddToCart">
					<div class="btn-group variant-changeable">
						<c:if test="${product.available.buynow and product.inStock and not product.preview}">
							<button
								class="button-buy-now button-${product.assorted ? 'warning' : 'fwd'}"
								name="purchase"
								value="buynow"
								type="button">
								<span class="icon">&nbsp;</span>
								<spring:theme code="text.addToCart.${product.assorted ? 'assorted' : 'buynow'}.button" />
							</button>
						</c:if>
						<c:if test="${product.preview}">
							<spring:theme code="comingsoon.modal.href" var="comingSoonModalHref"/>
							<a class="lightbox button-fwd button-coming-soon" data-lightbox-type="content" href="${comingSoonModalHref}">
								<span class="icon">&nbsp;</span>
								<spring:theme code="comingsoon.button" />
							</a>
						</c:if>
					</div>
				</feature:disabled>

				<feature:enabled name="uiPDPaddToCart">
					<c:if test="${product.assorted}">
						<c:set var="assortedClass" value="AddCart--warning"/>
					</c:if>
					<div class="variant-changeable">
						<c:if test="${product.available.buynow and not product.preview}">
							<div class="AddCart ${not variantSelected or not product.inStock ? 'AddCart--disabled ' : ''}  ${assortedClass}">
								<c:if test="${((not product.giftCard) or (product.giftCard and product.productTypeCode eq 'giftCard')) and not kioskMode}">
									<a class="AddCart-qtyContainer">
										<c:set var="qtyStart" value="1" />
										<c:set var="qtyEnd" value="99" />

										<select name="qty" class="AddCart-selector" ${not variantSelected ? 'disabled="disabled"' : ''}>
											<c:forEach var="cartQuantity" begin="${qtyStart}" end="${qtyEnd}">
												<option value="${cartQuantity}">${cartQuantity}</option>
											</c:forEach>
										</select>

										<label for="qty" class="AddCart-label">
											<div class="AddCart-labelContainer">
												<span class="AddCart-qtyDisplay">${qtyStart}</span>
												<svg width="11" height="7" viewBox="0 0 11 7" class="AddCart-svg" focusable="false">
													<use xlink:href="#DownArrowIcon" class="Down-Arrow"></use>
												</svg>
												<div class="AddCart-qty">
													<spring:theme code="text.addToCart.qty" />
												</div>
											</div>
										</label>
									</a>
								</c:if><%--
								--%><button
									class="AddCart-button ${product.giftCard or kioskMode ? 'AddCart-button--borderLeftRadius': ''} "
									name="purchase"
									value="buynow"
									data-animation="text-in"
									type="button" ${not variantSelected ? 'disabled="disabled"' : ''}><%--
								--%><c:if test="${product.assorted}"><%--
									--%><span class="AddCart-textWarning"><%--
										--%><spring:theme code="text.addToCart.assorted.warning"/><%--
									--%></span><br/><%--
									--%><span class="assortedWarning"><spring:theme code="text.addToCart.warning.button"/></span>&nbsp<%--
								--%></c:if><%--
								--%><spring:theme code="text.addToCart.add.button"
									arguments="${product.name}||${qtyStart}"  argumentSeparator="||"/><%--
								--%><span class="AddCart-suffix">+</span>
								</button>
								<a href="${targetUrlsMapping.cart}" class="AddCart-basketTag">
									<div class="AddCart-basketIcon"></div>
									<spring:theme code="text.addToCart.basketTag"/>
								</a>
							</div>
						</c:if>
					</div>
					<product:productAssortmentDisclaimer product="${product}" />
				</feature:enabled>
				<c:if test="${not consolidatedPDP}">
					<product:favouritesPDP
						product="${product}" />
				</c:if>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${not newPDP}">
		${urgencyMarkup}
	</c:if>
</form:form>

<script type="text/x-handlebars-template" id="basket-error-feeback">
	<feedback:message type="error" size="small">
		<p><spring:theme code="basket.error.occurred" /></p>
	</feedback:message>
</script>

<script type="text/x-handlebars-template" id="xhr-error-feeback">
	<feedback:message type="error" size="small">
		<p><spring:theme code="xhr.error.occurred" /></p>
	</feedback:message>
</script>

<c:if test="${consolidatedPDP}">
	<common:handlebarsTemplates template="preferredStoreDetailsTemplate" />
</c:if>
