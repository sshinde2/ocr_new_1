<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="megamenu" tagdir="/WEB-INF/tags/desktop/nav/megamenu" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<li class="only-for-small">
	<a href="${fullyQualifiedDomainName}" class="home-link" title="Target Home">
		<spring:theme code="header.link.home" arguments="Rondel--large" />
	</a>
</li>
<c:forEach items="${departments}" var="department" varStatus="departmentStatus">

	<c:set var="noColumns" value="${fn:length(department.columns)}" />
	<c:set var="departmentItemClass">mm-Departments-item mm-Departments-item${departmentStatus.count}</c:set>
	<c:if test="${noColumns gt 0}"><c:set var="departmentItemClass">${departmentItemClass} is-openable</c:set></c:if>
	<c:if test="${departmentStatus.first}"><c:set var="departmentItemClass">${departmentItemClass} is-first</c:set></c:if>
	<c:if test="${departmentStatus.last}"><c:set var="departmentItemClass">${departmentItemClass} is-last</c:set></c:if>

	<li class="${departmentItemClass}">
		<a href="${department.link}" class="mm-Departments-toggle" data-title="${department.title}" title="${department.title}">
			<c:if test="${not empty departmentsSVGUrl}">
				<c:set var="svg">
					<div class="mm-Departments-picture u-scalingSvgContainer">
						<svg class="u-scalingSvg" viewBox="0 0 75 75">
							<use xlink:href="#shape-department-${department.title}"></use>
						</svg>
					</div>
				</c:set>
				<script type="text/x-image-deferred" class="defer-svg" data-screen-support="tiny small" data-definition-url="${departmentsSVGUrl}">${svg}</script>
			</c:if>
			<span class="mm-Departments-title" data-title="${department.title}" title="${department.title}">
				<c:out value="${department.title}" />
			</span>
			<span class="mm-Departments-chevron only-for-small"><spring:theme code="icon.right-arrow-large-nospace" /></span>
		</a>

		<c:if test="${noColumns > 0}">
			<div class="mm-Departments-categories mm-Cat mm-Cat--flyout mm-Cat--columns${target:intToString(noColumns)}">
				<div class="mm-Departments-categoriesInner">
					<div class="mm-Header only-for-small">
						<span class="mm-Header-title mm-Header-back">
							<spring:theme code="icon.left-arrow-large" /><spring:theme code="header.link.menu.back" />
						</span>
						<nav class="mm-Header-selected-title">
							<a href="${department.link}" title="${department.title}" data-title="${department.title}">
								<spring:theme code="text.Browse" />&nbsp;<c:out value="${department.title}" />
								<span class="mm-Departments-chevron only-for-small"><spring:theme code="icon.right-arrow-large" /></span>
							</a>
						</nav>
					</div>

					<c:forEach items="${department.columns}" var="column" varStatus="columnStatus">
						<megamenu:column
							column="${column}"
							columnsStr="${target:gridColumnsPerMegamenuColumn(noColumns)}"
							lastColumn="${columnStatus.last}" />
					</c:forEach>
				</div>
			</div>
		</c:if>
	</li>
</c:forEach>
