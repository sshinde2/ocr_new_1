<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="section" required="true" type="au.com.target.tgtstorefront.navigation.megamenu.MegaMenuSection" %>
<%@ attribute name="parentName" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="globalNavigation" tagdir="/WEB-INF/tags/desktop/nav/globalNavigation" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="missingTitle" value="${empty section.title}" />
<c:set var="isAccordion" value="${not missingTitle and (fn:length(section.entries) > 0 or fn:length(section.sections) > 0)}" />

<c:if test="${isAccordion}">
	<c:set var="accordionClass" value="Menu-section--accordion is-closed" />
</c:if>

<c:if test="${not isAccordion}">
	<c:set var="hideForSmall" value="${missingTitle ? 'hide-for-small' : ''}" />
	<c:set var="headingLinkOnly">Menu-sectionHeading--linkOnly</c:set>
	<c:set var="sectionSuffix"><span class="only-for-small">&nbsp;&#8250;</span></c:set>
</c:if>
<c:set var="sectionName" value="${parentName} -> ${section.title}" />
<c:set var="sectionID" value="${fn:replace(fn:replace(sectionName, ' ', ''), '->', ':')}" />

<div class="Menu-section ${accordionClass} ${hideForSmall}">
	<c:if test="${isAccordion}">
		<input type="checkbox" name="${section.title}" value="${sectionName}" id="${sectionID}" class="visuallyhidden accordion-toggle" />
	</c:if>
	<c:if test="${not empty section.title}">
		<c:if test="${not empty section.link}">
			<c:set var="wrap"><a href="${target:validUrlString(section.link)}" title="${section.title}">||</a></c:set>
			<c:set var="anchor" value="${fn:split(wrap, '||')}" />
		</c:if>
		<c:if test="${isAccordion}">
			<c:set var="wrap"><label for="${sectionID}">||</label></c:set>
			<c:set var="label" value="${fn:split(wrap, '||')}" />
		</c:if>
		<c:choose>
			<c:when test="${section.style eq 'NewLink'}">
				<c:set var="sectionClass" value=" Menu-sectionHeading--new" />
			</c:when>
			<c:when test="${section.style eq 'SaleLink'}">
				<c:set var="sectionClass" value=" Menu-sectionHeading--sale" />
			</c:when>
		</c:choose>
		<span class="Menu-sectionHeading ${headingLinkOnly} ${sectionClass}">
			<c:out value="${anchor[0]}" escapeXml="false" />
				<c:out value="${section.title}" /><c:out value="${sectionSuffix}" escapeXml="false" />
			<c:out value="${anchor[1]}" escapeXml="false" />
			<c:if test="${not empty label}">
				<c:out value="${label[0]}" escapeXml="false" />
					<c:out value="${section.title}" /><c:out value="${sectionSuffix}" escapeXml="false" />
				<c:out value="${label[1]}" escapeXml="false" />
			</c:if>
		</span>
	</c:if>
	<c:if test="${not empty section.sections or not empty section.entries}">
		<nav class="Menu-subSection accordion-content">
			<c:forEach items="${section.entries}" var="entry">
				<globalNavigation:sectionEntry entry="${entry}" />
			</c:forEach>
			<c:forEach items="${section.sections}" var="sectionInner">
				<globalNavigation:section section="${sectionInner}" parentName="${sectionName}" />
			</c:forEach>
			<c:if test="${isAccordion and not empty section.link}">
				<a href="${section.link}" class="only-for-small Menu-allLink" data-title="${section.title}" title="${section.title}">
					<spring:theme code="link.withArrow" arguments="All ${section.title}" />
				</a>
			</c:if>
		</nav>
	</c:if>
</div>
