<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ attribute name="collection" required="true" type="java.lang.Object" %>
<%@ attribute name="depth" required="false" type="java.lang.Integer" %>

<c:if test="${empty depth or depth lt 0}">
	<c:set var="depth" value="${1}" />
</c:if>
<c:set var="nextDepth" value="${depth + 1}" />

<c:forEach items="${collection}" var="child">
	<li>
		<c:set var="className" value="" />
		<c:if test="${child.currentPage}">
			<c:set var="className" value="current" />
		</c:if>
		<c:if test="${child.external}">
			<c:set var="className" value="${className} external" />
		</c:if>
		<c:set var="classAttr">class="${className}"</c:set>
		<c:choose>
			<c:when test="${not empty child.link}">
				<c:url var="url" value="${child.link}" />
				<a href="${url}" title="${child.name}" ${not empty className ? classAttr : ""}>${child.name}</a>
			</c:when>
			<c:otherwise>
				<span ${not empty className ? classAttr : ""}>${child.name}</span>
			</c:otherwise>
		</c:choose>
		<c:if test="${fn:length(child.children) > 0}">
			<ul class="nav-level-${nextDepth}">
				<nav:navTree collection="${child.children}" depth="${nextDepth}" />
			</ul>
		</c:if>
	</li>
</c:forEach>