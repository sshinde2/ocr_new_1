<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<div class="nav-seg left-nav" ${ycommerce:getTestIdAttribute('left-nav', pageContext)}>
	<c:url var="url" value="${leftNavMenuItems.link}" />
	<h3><a href="${url}" title="${leftNavMenuItems.name}">${leftNavMenuItems.name}</a></h3>
	<ul class="nav-level-1">
		<nav:navTree collection="${leftNavMenuItems.children}" />
	</ul>
</div>