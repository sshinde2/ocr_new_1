<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="facets" required="true" type="java.lang.Object" %>
<%@ attribute name="facetName" required="true" type="java.lang.String" %>
<%@ attribute name="facetID" required="false" type="java.lang.String" %>
<%@ attribute name="type" required="false" type="java.lang.String" %>
<%@ attribute name="additionalAttributes" required="false" type="java.lang.String" %>


<c:forEach items="${facets}" var="facetValue" varStatus="facetStatus">
	<facet:item
		url="${facetValue.query.url}"
		queryUrl="${facetValue.query.wsUrl}"
		name="${facetValue.name}"
		count="${facetValue.count}"
		selected="${facetValue.selected}"
		type="${type}"
		category="${facetName}"
		id="${facetValue.code}"
		index="${facetStatus.index}"
		additionalAttributes="${additionalAttributes}"
	/>
</c:forEach>
