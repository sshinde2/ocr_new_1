<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="openingSchedule" required="true" type="au.com.target.tgtfacades.storelocator.data.TargetOpeningScheduleData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld"%>

<c:if test="${not empty openingSchedule and not empty openingSchedule.targetOpeningWeeks}">
	<div class="store-hours">
		<h4 class="hours-heading"><spring:theme code="storeFinder.opening.title" /></h4>
		<c:forEach items="${openingSchedule.targetOpeningWeeks}" var="week" varStatus="status">
			<c:set var="cssClass" value="${status.count % 2 eq 0 ? 'last-col': ''}" />
			<div class="hours-week ${cssClass}">
				<h5 class="title">${week.formattedStartDate} &ndash; ${week.formattedEndDate}</h5>
				<dl class="this-week">
					<c:forEach items="${week.targetOpeningDayList}" var="hours">
						<dt>${hours.formattedDay}</dt>
						<dd>
							<c:choose>
								<c:when test="${hours.closed}">
									<spring:theme code="storeFinder.details.closed" />
								</c:when>
								<c:when test="${hours.allDayTrade}">
									<spring:theme code="storeFinder.details.allDay" />
								</c:when>
								<c:otherwise>
									${hours.formattedOpeningTime} &ndash; ${hours.formattedClosingTime}
								</c:otherwise>
							</c:choose>
						</dd>
					</c:forEach>
				</dl>
			</div>
		</c:forEach>
	</div>
</c:if>