var gulp = require('gulp');
var webserver = require('gulp-webserver');
var minifyCss = require('gulp-minify-css');
var path = require('path');
var fs = require('fs');

var IPG_STYLES = '../../../_ui/desktop/theme-default/build/development/css/ipg.css';
var FONTS = '../../../_ui/desktop/common/fonts/**';
var COLES_CSS = '/Coles_files/css/';
var COLES_FONTS = COLES_CSS + 'fonts/';
var PATH_TO_FOLDER = 'iframe-options';

var folders = getFolders(PATH_TO_FOLDER);

function getFolders(dir) {
	return fs.readdirSync(dir)
		.filter(function(file) {
			return fs.statSync(path.join(dir, file)).isDirectory();
		});
}

function copyCss( styles ){
	if ( folders.length ){
		folders.forEach(function( folderName ){
			var folderName = '/' + folderName,
				destinationDirectory = PATH_TO_FOLDER + folderName +  COLES_CSS;
			console.log( 'Writing CSS to ' + destinationDirectory );
			gulp.src(styles)
				.pipe(gulp.dest( destinationDirectory ))
				.pipe(minifyCss())
				.pipe(gulp.dest( destinationDirectory ));
		});
	} else {
		console.log('No folders found...');
	}
}

function copyFonts(){
	if ( folders.length ){
		folders.forEach(function( folderName ){
			var folderName = '/' + folderName,
				destinationDirectory = PATH_TO_FOLDER + folderName +  COLES_FONTS;
			console.log( 'Copying fonts to ' + destinationDirectory );
			gulp.src([
					FONTS
				])
				.pipe(gulp.dest( destinationDirectory ));
		});
	} else {
		console.log('No folders found...');
	}
}

gulp.task('webserver', ['watch'], function() {
	gulp.src(__dirname)
		.pipe(webserver({
			livereload: true,
			directoryListing: true,
			open: true,
			port: 7778,
			https: true,
			host: '0.0.0.0'
		}));
});

gulp.task('watch', ['copy-assets'], function() {
	gulp.watch('../../../_ui/desktop/theme-default/build/development/css/ipg.css', ['copy-assets']);
});

gulp.task('copy-assets', ['copy-css', 'copy-fonts']);

gulp.task('copy-css', function(){
	copyCss( IPG_STYLES );
});

gulp.task('copy-fonts', function(){
	copyFonts();
});
