module.exports = {
	root: true,
	extends: 'eslint-config-airbnb/legacy',
	env: {
		browser: true,
		node: true,
		jasmine: true,
		amd: true
	},
	rules: {
		indent: [2, 'tab'],
		'comma-dangle': [2, 'never'],
		'func-names': [0],
		'new-cap': [2, { newIsCap: true, capIsNewExceptions: ['Deferred'] }],

		// The follow rules are unique to this project becase it's still es5
		'space-before-function-paren': [2, 'never']


	}
};
