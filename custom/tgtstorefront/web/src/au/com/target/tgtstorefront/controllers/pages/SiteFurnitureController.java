/*
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
public class SiteFurnitureController extends AbstractPageController {

    private static final String CATALOGUE_HEADER = "/catalogue/header";
    private static final String CATALOGUE_FOOTER = "/catalogue/footer";
    private static final String SITE_FURNITURE_HEADER = "/site-furniture/header";
    private static final String SITE_FURNITURE_FOOTER = "/site-furniture/footer";
    private static final String SITE_FURNITURE_CSS = "/site-furniture/css";
    private static final String SITE_FURNITURE_CSS_KEY = "tgtstorefront.sitefurniture.css";

    @Resource(name = "siteConfigService")
    private SiteConfigService siteConfigService;

    @RequestMapping(value = SITE_FURNITURE_HEADER, method = RequestMethod.GET)
    public String header(final Model model) throws CMSItemNotFoundException {
        return handleFurniture(model, ControllerConstants.Views.Pages.SiteFurniture.HEADER_PAGE);
    }

    @RequestMapping(value = SITE_FURNITURE_FOOTER, method = RequestMethod.GET)
    public String footer(final Model model) throws CMSItemNotFoundException {
        return handleFurniture(model, ControllerConstants.Views.Pages.SiteFurniture.FOOTER_PAGE);
    }

    @ResponseBody
    @RequestMapping(value = SITE_FURNITURE_CSS, method = RequestMethod.GET, produces = "text/css; charset=utf-8")
    public String css(final HttpServletResponse response) {
        final String content = siteConfigService.getProperty(SITE_FURNITURE_CSS_KEY);
        response.addHeader("Cache-Control", "max-age=3600");
        return content != null ? content : "/* nothing */";
    }

    @RequestMapping(value = CATALOGUE_HEADER, method = RequestMethod.GET)
    public String headerRedirect() {
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + SITE_FURNITURE_HEADER;
    }

    @RequestMapping(value = CATALOGUE_FOOTER, method = RequestMethod.GET)
    public String footerRedirect() {
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + SITE_FURNITURE_FOOTER;
    }

    private String handleFurniture(final Model model, final String view) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        return view;
    }
}
