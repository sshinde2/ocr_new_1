package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Controller for Instore Wifi landing page
 */
@Controller
public class InStoreWifiPageController extends AbstractCustomerSubscriptionController
{

    private static final String INSTORE_WIFI_SITE_CMS_PAGE_LABEL = "instore-wifi";
    private static final String INSTORE_WIFI_AUTHENTICATED_SITE_CMS_PAGE_LABEL = "instore-wifi-authenticated";

    /**
     * For returning the wifi landing page.
     * 
     * @param model
     * @return view
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.INSTORE_WIFI, method = RequestMethod.GET)
    public String getEnewsWifiPage(final Model model)
            throws CMSItemNotFoundException
    {
        storeCmsPageInModel(model, getCmsPage(INSTORE_WIFI_SITE_CMS_PAGE_LABEL));
        return ControllerConstants.Views.Pages.InStore.INSTORE_WIFI_PAGE;
    }

    /**
     * For returning the wifi authenticated page.
     * 
     * @param model
     * @return view
     * @throws CMSItemNotFoundException
     */

    @RequestMapping(value = ControllerConstants.INSTORE_WIFI_AUTHENTICATED, method = RequestMethod.GET)
    public String getEnewsWifiAuthenticatedPage(final Model model)
            throws CMSItemNotFoundException
    {
        storeCmsPageInModel(model, getCmsPage(INSTORE_WIFI_AUTHENTICATED_SITE_CMS_PAGE_LABEL));
        return ControllerConstants.Views.Pages.InStore.INSTORE_WIFI_AUTHENTICATED_PAGE;
    }

    /**
     * For returning the wifi landing page on the old mapping.
     * 
     * @param model
     * @return view
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.OLD_INSTORE_WIFI, method = RequestMethod.GET)
    public String getOldEnewsWifiPage(final Model model, final HttpServletRequest request)
            throws CMSItemNotFoundException
    {
        storeCmsPageInModel(model, getCmsPage(INSTORE_WIFI_SITE_CMS_PAGE_LABEL));
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.INSTORE_WIFI
                + "?" + request.getQueryString();
    }



}
