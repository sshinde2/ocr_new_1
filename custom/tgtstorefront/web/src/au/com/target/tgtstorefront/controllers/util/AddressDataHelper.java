/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.ui.Model;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.delivery.data.TargetRegionData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.data.AddressSuggestionResult;
import au.com.target.tgtstorefront.forms.AddressForm;
import au.com.target.tgtstorefront.forms.ConfirmAddressForm;
import au.com.target.tgtstorefront.forms.TargetAddressForm;
import au.com.target.tgtstorefront.forms.checkout.ClickAndCollectDetailsForm;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class AddressDataHelper {

    public static final String SINGLE_LINE_ADDRESS_FORMAT = "single.line.address.formatted";

    private static final String LOOKUP_SIZE = "tgtstorefront.address.lookup.size";

    private static final String LOOKUP_MINIMUM_LENGTH = "tgtstorefront.address.lookup.minimum.length";

    private static final String SINGLE_LINE_ADDRESS_LOOKUP = "single.line.address.lookup";

    private static final Logger LOG = Logger.getLogger(AddressDataHelper.class);

    @Resource(name = "targetDeliveryFacade")
    private TargetDeliveryFacade targetDeliveryFacade;

    @Resource(name = "targetAddressVerificationService")
    private TargetAddressVerificationService targetAddressVerificationService;

    @Resource(name = "targetUserFacade")
    private TargetUserFacade targetUserFacade;

    @Resource(name = "checkoutCustomerStrategy")
    private TargetCheckoutCustomerStrategy targetCheckoutCustomerStrategy;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Resource
    private I18NFacade i18NFacade;

    /**
     * Populate a address data object from an address form
     * 
     * @param targetAddressData
     * @param addressForm
     */
    public void populateTargetAddressData(final TargetAddressData targetAddressData,
            final AddressForm addressForm) {
        targetAddressData.setTitleCode(addressForm.getTitleCode());
        targetAddressData.setFirstName(addressForm.getFirstName());
        targetAddressData.setLastName(addressForm.getLastName());
        targetAddressData.setLine1(addressForm.getLine1());
        targetAddressData.setLine2(addressForm.getLine2());
        targetAddressData.setTown(addressForm.getTownCity());
        targetAddressData.setPostalCode(addressForm.getPostcode());
        targetAddressData.setBillingAddress(false);
        targetAddressData.setShippingAddress(true);

        final CountryData countryData = new CountryData();
        countryData.setIsocode(addressForm.getCountryIso());
        targetAddressData.setCountry(countryData);
        targetAddressData.setId(addressForm.getAddressId());
        targetAddressData.setPhone(addressForm.getPhoneNumber());
        targetAddressData.setState(addressForm.getState());
    }


    /**
     * create TargetAddressData object with values from TargetAddressForm
     * 
     * @param addressForm
     * @return targetAddressData
     */
    public TargetAddressData createTargetAustralianShippingAddressData(
            final TargetAddressForm addressForm) {
        final TargetAddressData targetAddressData = createAddress(addressForm);
        if (targetAddressData != null) {
            targetAddressData.setBillingAddress(false);
            targetAddressData.setShippingAddress(true);
            targetAddressData.getCountry().setIsocode(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        }
        return targetAddressData;
    }

    /**
     * create TargetAddressData object with values from TargetAddressForm
     * 
     * @param addressForm
     * @return targetAddressData
     */
    public TargetAddressData createTargetBillingAddressData(final TargetAddressForm addressForm) {
        final TargetAddressData targetAddressData = createAddress(addressForm);
        if (targetAddressData != null) {
            targetAddressData.setBillingAddress(true);
            targetAddressData.setShippingAddress(false);
            targetAddressData.setCountry(i18NFacade.getCountryForIsocode(addressForm.getCountryCode()));
        }
        return targetAddressData;
    }

    /**
     * Create TargetAddressData object with values from TargetAddressForm
     * 
     * @return targetAddressData
     */
    protected TargetAddressData createAddress(final TargetAddressForm addressForm) {
        final TargetAddressData targetAddressData = new TargetAddressData();
        BeanUtils.copyProperties(addressForm, targetAddressData);
        targetAddressData.setTitleCode(!StringUtils.isEmpty(addressForm.getTitle()) ? addressForm.getTitle() : null);
        final CountryData countryData = new CountryData();
        targetAddressData.setCountry(countryData);
        return targetAddressData;
    }

    /**
     * Create a validated shipping address
     * 
     * @param addressForm
     * @return targetAddressData
     */
    public TargetAddressData createVerifiedTargetAustralianShippingAddressData(final TargetAddressForm addressForm) {
        final TargetAddressData targetAddressData = createVerifiedAddress(addressForm);
        if (targetAddressData != null) {
            targetAddressData.setBillingAddress(false);
            targetAddressData.setShippingAddress(true);
            targetAddressData.getCountry().setIsocode(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        }
        return targetAddressData;
    }

    /**
     * Create a validated billing address
     * 
     * @param addressForm
     * @return targetAddressData
     */
    public TargetAddressData createVerifiedTargetAustralianBillingAddressData(
            final TargetAddressForm addressForm) {
        final TargetAddressData targetAddressData = createVerifiedAddress(addressForm);
        if (targetAddressData != null) {
            targetAddressData.setBillingAddress(true);
            targetAddressData.setShippingAddress(false);
            targetAddressData.setCountry(i18NFacade.getCountryForIsocode(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA));
        }
        return targetAddressData;
    }

    /**
     * Create a validated TargetAddressData object from TargetAddressForm
     * 
     * @return targetAddressData
     */
    protected TargetAddressData createVerifiedAddress(final TargetAddressForm addressForm) {
        TargetAddressData targetAddressData = null;
        final FormattedAddressData formattedAddress = formatAddress(
                addressForm.getSingleLineId(), addressForm.getSingleLineLabel());

        if (!addressForm.getSingleLineLabel().equals(formattedAddress.getAddressLine1())) {
            targetAddressData = new TargetAddressData();
            BeanUtils.copyProperties(addressForm, targetAddressData);
            targetAddressData
                    .setTitleCode(!StringUtils.isEmpty(addressForm.getTitle()) ? addressForm.getTitle() : null);

            final CountryData countryData = new CountryData();
            targetAddressData.setCountry(countryData);
            targetAddressData.setAddressValidated(true);
            targetAddressData.setLine1(formattedAddress.getAddressLine1());
            targetAddressData.setLine2(formattedAddress.getAddressLine2());
            targetAddressData.setTown(formattedAddress.getCity());
            targetAddressData.setState(formattedAddress.getState());
            targetAddressData.setPostalCode(formattedAddress.getPostcode());
        }
        return targetAddressData;
    }

    /**
     * Populate an address form from a address data object
     * 
     * @param addressForm
     * @param targetAddressData
     */
    public void populateAddressForm(final AddressForm addressForm,
            final TargetAddressData targetAddressData) {
        addressForm.setAddressId(targetAddressData.getId());
        addressForm.setTitleCode(targetAddressData.getTitleCode());
        addressForm.setFirstName(targetAddressData.getFirstName());
        addressForm.setLastName(targetAddressData.getLastName());
        addressForm.setLine1(targetAddressData.getLine1());
        addressForm.setLine2(targetAddressData.getLine2());
        addressForm.setTownCity(targetAddressData.getTown());
        addressForm.setPostcode(targetAddressData.getPostalCode());
        addressForm.setCountryIso(targetAddressData.getCountry().getIsocode());
        addressForm.setPhoneNumber(targetAddressData.getPhone());
        addressForm.setState(targetAddressData.getState());
        if (targetAddressData.getCountry() != null) {
            addressForm.setCountryIso(targetAddressData.getCountry().getIsocode());
        }
        addressForm.setShippingAddress(Boolean.valueOf(targetAddressData.isShippingAddress()));
        addressForm.setBillingAddress(Boolean.valueOf(targetAddressData.isBillingAddress()));
        addressForm.setDefaultAddress(Boolean.valueOf(targetAddressData.isDefaultAddress()));
    }

    /**
     * check if the single line address lookup feature is enabled.
     * 
     * @return true if the feature is enabled, false if not.
     */
    public boolean isSingleLineLookupFeatureEnabled() {
        return targetFeatureSwitchFacade.isFeatureEnabled(SINGLE_LINE_ADDRESS_LOOKUP) &&
                !salesApplicationFacade.isKioskApplication();
    }

    /**
     * This method is to check whether the single line address lookup feature is enabled or not.
     * 
     * 1) it will set the flag to the model.
     * 
     * 2) it will set the QAS Engine Type to Intuitive if the feature is turned on, and revert it back to the default
     * value if not.
     * 
     * 
     * !!!!Important !!!! 2) can be removed if the feature is on permanently and tgtverifyaddr.engineType is set to
     * Intuitive in tgtverifyaddr/project.properties or local.properties
     * 
     * @param model
     */
    @SuppressWarnings("boxing")
    public void checkIfSingleLineAddressFeatureIsEnabled(final Model model) {
        final boolean featureEnabled = isSingleLineLookupFeatureEnabled();
        model.addAttribute("singleLineAddressLookup",
                featureEnabled);
    }

    /**
     * 
     * @param addressForm
     * @return Formatted String for QAS containing address
     */
    public String formatAddressForQAS(final AddressForm addressForm) {
        return formatAddress(addressForm.getLine1(), addressForm.getLine2(), addressForm.getTownCity(),
                addressForm.getState(), addressForm.getPostcode());
    }

    /**
     * Get address suggestions for given keyword via QAS service.
     * 
     * @param keyword
     * @return suggestionResult
     */
    public AddressSuggestionResult getAddressSuggestions(final String keyword) {
        final AddressSuggestionResult suggestionResult = new AddressSuggestionResult();
        final List<SelectOption> confirmAddressList = new ArrayList<>();
        boolean isVerificationFailed = false;
        if (keyword != null
                && keyword.trim().length() > configurationService.getConfiguration().getInt(
                        LOOKUP_MINIMUM_LENGTH)) {
            try {
                final int maxSize = configurationService.getConfiguration().getInt(
                        LOOKUP_SIZE);
                int currentSize = 0;
                final List<au.com.target.tgtverifyaddr.data.AddressData> addressSuggestions = targetAddressVerificationService
                        .searchAddress(keyword, CountryEnum.AUSTRALIA);

                for (final au.com.target.tgtverifyaddr.data.AddressData addressSuggestion : addressSuggestions) {
                    if (StringUtils.isNotBlank(addressSuggestion.getMoniker())) {
                        confirmAddressList.add(new SelectOption(addressSuggestion.getMoniker(), addressSuggestion
                                .getPartialAddress()));
                        currentSize++;
                        if (currentSize == maxSize) {
                            break;
                        }
                    }
                }
                suggestionResult.setStatus("OK");
            }
            catch (final TooManyMatchesFoundException ex) {
                suggestionResult.setStatus("TooManyMatchesFound");
            }
            catch (final NoMatchFoundException ex) {
                suggestionResult.setStatus("NoMatchFound");
            }
            catch (final ServiceNotAvailableException | RequestTimeOutException ex) {
                isVerificationFailed = true;
            }
        }
        else {
            suggestionResult.setStatus("InputIsTooShort");
        }
        if (isVerificationFailed) {
            suggestionResult.setStatus("VerificationFailed");
        }
        suggestionResult.setSuggestions(confirmAddressList);
        return suggestionResult;
    }

    /**
     * 
     * @param model
     * @param addressForm
     * @param suppliedAddress
     * @param isEdit
     * @return true if it failed
     */
    public boolean processAddressAndVerifyWithQas(final Model model, final AddressForm addressForm,
            final TargetAddressData suppliedAddress, final boolean isEdit) {
        List<au.com.target.tgtverifyaddr.data.AddressData> addressSuggestions = null;
        final List<SelectOption> confirmAddressList = new ArrayList<>();
        boolean isVerificationFailed = false;
        boolean useSuppliedAddress = false;

        try {
            final String formattedAddressForQas = formatAddressForQAS(addressForm);

            addressSuggestions = targetAddressVerificationService
                    .verifyAddress(formattedAddressForQas, CountryEnum.AUSTRALIA);

            for (final au.com.target.tgtverifyaddr.data.AddressData addressSuggestion : addressSuggestions) {
                if (StringUtils.isNotBlank(addressSuggestion.getMoniker())) {
                    confirmAddressList.add(new SelectOption(addressSuggestion.getMoniker(), addressSuggestion
                            .getPartialAddress()));
                }
            }
        }
        catch (final TooManyMatchesFoundException ex) {
            model.addAttribute("tooManyConfirmAddresses", Boolean.TRUE);
            useSuppliedAddress = true;
        }
        catch (final NoMatchFoundException ex) {
            useSuppliedAddress = true;
        }
        catch (final ServiceNotAvailableException ex) {
            isVerificationFailed = true;
        }
        catch (final RequestTimeOutException ex) {
            isVerificationFailed = true;
        }

        if (isVerificationFailed) {
            model.addAttribute("timeOut", Boolean.TRUE);
            useSuppliedAddress = true;
        }
        model.addAttribute("confirmAddressList", confirmAddressList);

        final ConfirmAddressForm confirmAddressForm = new ConfirmAddressForm();
        confirmAddressForm.setSuppliedAddress(addressForm);
        confirmAddressForm.setUseSuppliedAddress(useSuppliedAddress);
        model.addAttribute("confirmAddressForm", confirmAddressForm);

        model.addAttribute("edit", Boolean.valueOf(isEdit));

        return isVerificationFailed;
    }

    /**
     * 
     * @return list of states for Australia
     */
    public List<SelectOption> getStates()
    {
        final List<TargetRegionData> regions = targetDeliveryFacade
                .getRegionsForCountry(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);

        final List<SelectOption> states = new ArrayList<>();

        for (final TargetRegionData region : regions) {
            states.add(new SelectOption(region.getAbbreviation(), region.getName()));
        }

        Collections.sort(states, new Comparator<SelectOption>() {
            @Override
            public int compare(final SelectOption o1, final SelectOption o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        return states;
    }

    /**
     * @param addressCode
     * @return corresponding address data if exist
     */
    public AddressData getAddressDataFromAddressBookById(final String addressCode) {
        for (final AddressData addressData : targetUserFacade.getAddressBook())
        {
            if (addressData.getId() != null && addressData.getId().equals(addressCode))
            {
                return addressData;
            }
        }
        throw new UnknownIdentifierException("Can't find any address for the given address id : " + addressCode);
    }

    /**
     * 
     * populate AddressForm with user model
     * 
     * @param addressForm
     * 
     */
    public void populateAddressFormFromUser(final AddressForm addressForm) {

        if (!targetCheckoutCustomerStrategy.isAnonymousCheckout()) {
            final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)userService.getCurrentUser();

            if (targetCustomerModel.getTitle() != null) {
                addressForm.setTitleCode(targetCustomerModel.getTitle().getCode());
            }

            addressForm.setFirstName(targetCustomerModel.getFirstname());
            addressForm.setLastName(targetCustomerModel.getLastname());
        }

        return;
    }

    /**
     * populate ClickAndCollectDetailsForm with user model
     * 
     * @param clickAndCollectDetailsForm
     */
    public void populateClickAndCollectDetailsFormFromUser(final ClickAndCollectDetailsForm clickAndCollectDetailsForm) {

        if (!targetCheckoutCustomerStrategy.isAnonymousCheckout()) {
            final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)userService.getCurrentUser();

            if (targetCustomerModel.getTitle() != null) {
                clickAndCollectDetailsForm.setTitle(targetCustomerModel.getTitle().getCode());
            }

            clickAndCollectDetailsForm.setFirstName(targetCustomerModel.getFirstname());
            clickAndCollectDetailsForm.setLastName(targetCustomerModel.getLastname());
        }

        return;
    }

    /**
     * @param code
     * @return TargetAddressData
     */
    public FormattedAddressData formatAddress(final String code, final String partialAddress) {
        FormattedAddressData formattedAddressData = null;
        final au.com.target.tgtverifyaddr.data.AddressData qasAddressData = new au.com.target.tgtverifyaddr.data.AddressData(
                code, partialAddress, null);
        try {
            formattedAddressData = targetAddressVerificationService.formatAddress(qasAddressData);
        }
        catch (final ServiceNotAvailableException e) {
            LOG.error(e.getMessage());
        }
        if (formattedAddressData == null) {
            formattedAddressData = new FormattedAddressData();
            formattedAddressData.setAddressLine1(partialAddress);
        }
        return formattedAddressData;
    }

    public boolean isAddressFormVerifiedInSession(final AddressForm addressForm, final HttpSession session) {
        final Object addressData = session.getAttribute(SINGLE_LINE_ADDRESS_FORMAT);
        if (addressData instanceof FormattedAddressData) {
            return isAddressFormVerified(addressForm, (FormattedAddressData)addressData);
        }
        return false;
    }

    public boolean isAddressFormVerified(final AddressForm addressForm, final FormattedAddressData addressInSession) {
        if (addressForm == null || addressInSession == null) {
            return false;
        }
        final String addrToSave = formatAddressForQAS(addressForm);
        final String addrInSession = formatAddress(addressInSession.getAddressLine1(),
                addressInSession.getAddressLine2(), addressInSession.getCity(), addressInSession.getState(),
                addressInSession.getPostcode());
        return addrToSave.equalsIgnoreCase(addrInSession);
    }

    /**
     * 
     * @param addressLine1
     * @param addressLine2
     * @param suburb
     * @param state
     * @param postcode
     * @return formatted string
     */
    public String formatAddress(final String addressLine1, final String addressLine2, final String suburb,
            final String state, final String postcode) {
        final StringBuilder addressToVerify = new StringBuilder();

        addressToVerify.append(addressLine1);
        addressToVerify.append(", ");

        if (StringUtils.isNotBlank(addressLine2)) {
            addressToVerify.append(addressLine2);
            addressToVerify.append(", ");
        }

        addressToVerify.append(suburb);
        addressToVerify.append(' ');
        addressToVerify.append(state);
        addressToVerify.append(' ');
        addressToVerify.append(postcode);

        return addressToVerify.toString();
    }

    public int getMaxSearchSizeForAddressLookup() {
        return configurationService.getConfiguration().getInt(LOOKUP_SIZE);
    }

}
