/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

/**
 * @author gbaker2
 * 
 */
public class VendorPaymentForm extends AbstractPaymentDetailsForm {

    /**
     * Populate form action
     */
    public VendorPaymentForm(final String action) {
        setAction(action);
    }

    /**
     * Populate Super constructor
     */
    public VendorPaymentForm() {
        super();

    }
}
