/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum;
import au.com.target.tgtstorefront.forms.validation.validator.MessageTextValidator;


/**
 * 
 * @author smishra1
 */
@Retention(RUNTIME)
@Constraint(validatedBy = MessageTextValidator.class)
@Documented
@Target({ FIELD })
public @interface MessageText {
    String message() default StringUtils.EMPTY;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean mandatory() default true;

    FieldTypeEnum field() default FieldTypeEnum.messageText;
}
