/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.util;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;


/**
 * Resolves page title according to page, search text, current category or product
 */
public class PageTitleResolver {
    private ProductService productService;
    private CommerceCategoryService commerceCategoryService;
    private CMSSiteService cmsSiteService;
    private TargetDealService targetDealService;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "i18nService")
    private I18NService i18nService;

    /**
     * 
     * @param title
     * @return CMS page title
     */
    public String resolveContentPageTitle(final String title) {
        final StringBuilder builder = new StringBuilder();
        if (!StringUtils.isEmpty(title)) {
            builder.append(title).append(getPageTitleSeparator());
        }
        builder.append(getPageTitleSuffix());
        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * 
     * @param title
     * @return Search page title
     */
    public String resolveSearchPageTitle(final String title) {
        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

        final StringBuilder builder = new StringBuilder();
        if (!StringUtils.isEmpty(title)) {
            builder.append(title).append(getPageTitleSeparator());
        }
        builder.append(currentSite.getName());
        return builder.toString();
    }

    /**
     * 
     * @param title
     * @return Home page title
     */
    public String resolveHomePageTitle(final String title) {
        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
        final StringBuilder builder = new StringBuilder();
        builder.append(currentSite.getName());

        if (!StringUtils.isEmpty(title)) {
            builder.append(getPageTitleSeparator()).append(title);
        }

        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * 
     * @param searchText
     * @param appliedFacets
     * @return Search page title
     */
    public <STATE> String resolveSearchPageTitle(final String searchText,
            final List<BreadcrumbData<STATE>> appliedFacets) {
        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

        final StringBuilder builder = new StringBuilder();
        if (!StringUtils.isEmpty(searchText)) {
            builder.append(searchText).append(getPageTitleSeparator());
        }
        for (final BreadcrumbData pathElement : appliedFacets) {
            builder.append(pathElement.getFacetValueName()).append(getPageTitleSeparator());
        }
        builder.append(currentSite.getName());
        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * 
     * @param category
     * @return title for category page
     */
    public String resolveCategoryPageTitle(final CategoryModel category, final String facetRefinementName) {
        final StringBuilder sb = new StringBuilder();
        appendFacetRefinementName(facetRefinementName, sb);

        if (category instanceof TargetProductCategoryModel) {
            final TargetProductCategoryModel targetProductCategoryModel = (TargetProductCategoryModel)category;
            if (StringUtils.isNotEmpty(targetProductCategoryModel.getMetaPageTitle())) {
                final String metaPageTitle = targetProductCategoryModel.getMetaPageTitle();
                sb.append(metaPageTitle);
                return StringEscapeUtils.escapeHtml(sb.toString());
            }
        }

        final List<CategoryModel> categories = this.getCategoryPath(category);
        for (final CategoryModel c : categories) {
            if (c.getSupercategories().isEmpty()) {
                continue; // If the current category has no supercategories then it's the top level and shouldn't be displayed.
            }

            sb.append(c.getName()).append(getPageTitleSeparator());
        }
        sb.append(getPageTitleSuffix());
        return StringEscapeUtils.escapeHtml(sb.toString());
    }


    /**
     * 
     * @param storeName
     * @return title for store page
     */
    public String resolveStorePageTitle(final String storeName) {
        final StringBuilder builder = new StringBuilder();
        if (StringUtils.isNotEmpty(storeName)) {
            builder.append(storeName).append(getPageTitleSeparator());
        }
        builder.append(getPageTitleSuffix());
        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * 
     * @param category
     * @param appliedFacets
     * @return title for given code and facets
     */
    public <STATE> String resolveCategoryPageTitle(final CategoryModel category,
            final List<BreadcrumbData<STATE>> appliedFacets) {
        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

        final String name = category.getName();
        final StringBuilder builder = new StringBuilder();
        if (CollectionUtils.isEmpty(appliedFacets)) {
            if (!StringUtils.isEmpty(name)) {
                builder.append(name).append(getPageTitleSeparator());
            }
            builder.append(currentSite.getName());
        }
        else {
            for (final BreadcrumbData pathElement : appliedFacets) {
                builder.append(pathElement.getFacetValueName()).append(getPageTitleSeparator());
            }
            builder.append(currentSite.getName());
        }

        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * 
     * @param categoryCode
     * @param appliedFacets
     * @return page title for given code and facets
     */
    public <STATE> String resolveCategoryPageTitle(final String categoryCode,
            final List<BreadcrumbData<STATE>> appliedFacets) {
        final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
        return resolveCategoryPageTitle(category, appliedFacets);
    }

    /**
     * creates page title for given code
     * 
     * @param product
     */
    public String resolveProductPageTitle(final ProductModel product) {
        final String productName = getProductDisplayName(product);
        final String productId = product.getCode();
        final StringBuilder productTitle = StringUtils.isBlank(productName) ? new StringBuilder(productId)
                : new StringBuilder(productName);
        productTitle.append(getPageTitleSeparator());
        productTitle.append(getPageTitleSuffix());
        return StringEscapeUtils.escapeHtml(productTitle.toString());
    }

    /**
     * Resolve the title of the brand page.
     * 
     * @param brand
     * @return title for category page
     */
    public String resolveBrandPageTitle(final BrandModel brand, final AbstractPageModel cmsPage) {
        if (cmsPage != null && StringUtils.isNotEmpty(cmsPage.getTitle())) {
            return cmsPage.getTitle();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(brand.getName()).append(getPageTitleSeparator());
        sb.append(getPageTitleSuffix());
        return StringEscapeUtils.escapeHtml(sb.toString());
    }

    /**
     * @param facetRefinementName
     * @param sb
     */
    private void appendFacetRefinementName(final String facetRefinementName, final StringBuilder sb) {
        if (StringUtils.isNotBlank(facetRefinementName)) {
            sb.append(facetRefinementName).append(getPageTitleSeparator());
        }
    }

    /**
     * resolve the deal category page title
     * 
     * @param dealCategoryModel
     * @return deal category page title with html escaped
     */
    public String resolveDealCategoryPageTitle(final TargetDealCategoryModel dealCategoryModel) {
        if (dealCategoryModel != null) {
            final AbstractSimpleDealModel dealModel = targetDealService.getDealByCategory(dealCategoryModel);
            if (dealModel != null && StringUtils.isNotEmpty(dealModel.getPageTitle())) {
                return StringEscapeUtils.escapeHtml(dealModel.getPageTitle());
            }
        }
        return StringEscapeUtils.escapeHtml(getPageTitleSuffix());
    }

    /**
     * 
     * @param product
     * @return List
     */
    protected List<CategoryModel> getCategoryPath(final ProductModel product) {
        final CategoryModel category = getPrimaryCategoryForProduct(product);
        if (category != null) {
            return getCategoryPath(category);
        }
        return Collections.emptyList();
    }

    /**
     * 
     * @param category
     * @return List
     */
    protected List<CategoryModel> getCategoryPath(final CategoryModel category) {
        final Collection<List<CategoryModel>> paths = getCommerceCategoryService().getPathsForCategory(category);
        // Return first - there will always be at least 1
        final List<CategoryModel> cat2ret = paths.iterator().next();
        Collections.reverse(cat2ret);
        return cat2ret;
    }

    /**
     * 
     * @param product
     * @return CategoryModel
     */
    protected CategoryModel getPrimaryCategoryForProduct(final ProductModel product) {
        // Get the first super-category from the product that isn't a classification category
        for (final CategoryModel category : product.getSupercategories()) {
            if (!(category instanceof ClassificationClassModel)) {
                return category;
            }
        }
        return null;
    }

    /**
     * Returns the product display name.
     * 
     * @param product
     *            the product to get display name for
     * @return the product display name
     */
    protected String getProductDisplayName(final ProductModel product) {
        if (product instanceof AbstractTargetVariantProductModel) {
            return ((AbstractTargetVariantProductModel)product).getDisplayName();
        }
        else {
            return product.getName();
        }
    }

    /**
     * 
     * @return suffix for page title (example -> Target Australia)
     */
    protected String getPageTitleSuffix() {
        return messageSource.getMessage("page.title.suffix", null, i18nService.getCurrentLocale());
    }

    /**
     * Returns the page title separator character wrapped with spaces (for convenience).
     * 
     * @return the page title separator with spaces
     */
    protected String getPageTitleSeparator() {
        final String separator = messageSource.getMessage("page.title.separator", null, i18nService.getCurrentLocale());
        return " " + StringUtils.trimToEmpty(separator) + " ";
    }

    protected CommerceCategoryService getCommerceCategoryService() {
        return commerceCategoryService;
    }

    @Required
    public void setCommerceCategoryService(final CommerceCategoryService commerceCategoryService) {
        this.commerceCategoryService = commerceCategoryService;
    }

    protected ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    protected CMSSiteService getCmsSiteService() {
        return cmsSiteService;
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    @Required
    public void setTargetDealService(final TargetDealService targetDealService) {
        this.targetDealService = targetDealService;
    }


}
