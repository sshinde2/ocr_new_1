/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtfacades.look.TargetLookPageFacade;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.breadcrumb.impl.ShopTheLookBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author mjanarth
 *
 */
@Controller
public class LookViewPageController extends AbstractPageController {

    private static final String LOOK_LABEL_PATTERN = "/look/{lookLabel:.*}";

    private static final String LOOK_PAGE_PATTERN = "/look/**/{lookCode:.*}";

    private static final String DEFAULT_LOOK_CMS_PAGE = "/look/lookDefaultPage";

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "endecaProductQueryBuilder")
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Resource(name = "endecaQueryResultsHelper")
    private EndecaQueryResultsHelper endecaQueryResultsHelper;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;


    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Resource(name = "targetLookPageFacade")
    private TargetLookPageFacade targetLookPageFacade;

    @Resource(name = "targetShopTheLookFacade")
    private TargetShopTheLookFacade targetShopTheLookFacade;

    @Resource(name = "shopTheLookBreadcrumbBuilder")
    private ShopTheLookBreadcrumbBuilder shopTheLookBreadcrumbBuilder;

    private int previousLookCount;

    /**
     * For getting Collection page,redirects to 404 if CMS page not found
     * 
     * @param looklabel
     * @param model
     * @param request
     * @param response
     * @return String
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = LOOK_LABEL_PATTERN, method = RequestMethod.GET)
    public String getLookProductDetails(@PathVariable("lookLabel") final String looklabel,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            final String pageUrl = "/look/" + looklabel;
            request.setAttribute("lookUrl", pageUrl);
            final ContentPageModel pageModel = cmsPageService
                    .getPageForLabelOrId(pageUrl);
            if (pageModel instanceof TargetProductGroupPageModel) {
                final TargetProductGroupPageModel lookPageModel = (TargetProductGroupPageModel)pageModel;
                final List<String> productCodes = createProductCodesFromModels(lookPageModel.getProducts());
                final EndecaSearchStateData endecaSearchData = populateEndecaSearchData(productCodes);
                if (null != endecaSearchData) {
                    populateProductListerData(endecaSearchData, model);
                }
                storeCmsPageInModel(model, lookPageModel);
                model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                        contentPageBreadcrumbBuilder.getBreadcrumbs(lookPageModel));
                final LookDetailsData lookData = targetLookPageFacade.getLookDetails(lookPageModel);
                model.addAttribute("lookPageData", lookData);
                return ControllerConstants.Views.Pages.LookView.LOOK_PAGE;
            }
            else {
                return ControllerConstants.Forward.ERROR_404;
            }
        }
        catch (final CMSItemNotFoundException ex) {
            return ControllerConstants.Forward.ERROR_404;
        }
    }

    /**
     * For getting Look page
     * 
     * @param lookCode
     * @param model
     * @return String
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = LOOK_PAGE_PATTERN, method = RequestMethod.GET)
    public String getLookPage(@PathVariable("lookCode") final String lookCode,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        final TargetLookModel targetLookModel = targetShopTheLookFacade.getLookByCode(lookCode);
        if (targetLookModel == null) {
            return ControllerConstants.Forward.ERROR_404;
        }
        final String redirection = checkRequestUrl(request, response, targetLookModel.getUrl());
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        final ContentPageModel pageModel = cmsPageService.getPageForLabelOrId(DEFAULT_LOOK_CMS_PAGE);
        if (pageModel instanceof TargetProductGroupPageModel) {
            final TargetProductGroupPageModel groupPageModel = (TargetProductGroupPageModel)pageModel;
            storeCmsPageInModel(model, groupPageModel);
        }
        populateProductsDataForLookPage(targetLookModel, model);
        populateLookDetailsData(targetLookModel, model);
        populateBreadcrumbDataForLookPage(targetLookModel, model);

        addAkamaiCacheAttributes(CachedPageType.LOOKPAGE, response, model);
        return ControllerConstants.Views.Pages.LookView.LOOK_PAGE;
    }

    private void populateBreadcrumbDataForLookPage(final TargetLookModel targetLookModel, final Model model) {
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                shopTheLookBreadcrumbBuilder.getBreadcrumbsForLookPage(targetLookModel));
    }

    private void populateLookDetailsData(final TargetLookModel targetLookModel, final Model model) {
        model.addAttribute("lookPageData", targetShopTheLookFacade.populateLookDetailsData(targetLookModel.getId()));
        model.addAttribute("relatedLooks",
                getLooksForCarousel(targetLookModel.getCollection().getId(), targetLookModel.getId()));
        model.addAttribute("relatedLooksUrl", targetLookModel.getCollection().getUrl());
    }

    private List<LookDetailsData> getLooksForCarousel(final String collectionId, final String lookId) {
        final List<TargetLookModel> looks = new ArrayList<>(
                targetShopTheLookFacade.getVisibleLooksForCollection(collectionId));

        int currentLookIndex = -1;
        int i = 0;
        for (final Iterator<TargetLookModel> iterator = looks.iterator(); iterator.hasNext();) {
            final TargetLookModel look = iterator.next();
            if (StringUtils.isNotBlank(lookId) && StringUtils.equalsIgnoreCase(lookId, look.getId())) {
                currentLookIndex = i;
                iterator.remove();
            }
            i++;
        }
        if (currentLookIndex >= getPreviousLookCount()) {
            Collections.rotate(looks, -(currentLookIndex - getPreviousLookCount()));
        }

        return targetShopTheLookFacade.populateLookDetailsDataList(looks);

    }

    private void populateProductListerData(final EndecaSearchStateData endecaSearchData, final Model model) {
        final ENEQueryResults queryResults = endecaProductQueryBuilder.getQueryResults(endecaSearchData, null,
                getMaxProducts());
        if (null != queryResults) {
            final List<TargetProductListerData> productsList = endecaQueryResultsHelper
                    .getTargetProductsListForColorVariant(queryResults);
            model.addAttribute("productsData", productsList);
        }
    }

    private List<String> createProductCodesFromModels(final List<ProductModel> products) {
        final List<String> productCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(products)) {
            for (final ProductModel product : products) {
                productCodes.add(product.getCode());
            }
        }
        return productCodes;
    }

    private EndecaSearchStateData populateEndecaSearchData(final List<String> productCodes) {
        EndecaSearchStateData endecaSearchStateData = null;
        if (CollectionUtils.isNotEmpty(productCodes)) {
            endecaSearchStateData = new EndecaSearchStateData();
            endecaSearchStateData.setMatchMode(EndecaConstants.MATCH_MODE_ANY);
            endecaSearchStateData.setSkipDefaultSort(true);
            endecaSearchStateData.setSkipInStockFilter(true);
            endecaSearchStateData.setSearchField(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
            endecaSearchStateData.setSearchTerm(productCodes);
            endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        }

        return endecaSearchStateData;
    }

    private void populateProductsDataForLookPage(final TargetLookModel targetLookModel, final Model model) {
        final List<String> productCodes = targetShopTheLookFacade.getProductCodeListByLook(targetLookModel.getId());
        if (CollectionUtils.isNotEmpty(productCodes)) {
            final EndecaSearchStateData endecaSearchStateData = populateEndecaSearchData(productCodes);
            if (null != endecaSearchStateData) {
                populateProductListerData(endecaSearchStateData, model);
            }
        }
    }

    /**
     * Get the maximum number of products for endeca search,default to 20
     * 
     * @return prodctSize
     */
    protected int getMaxProducts() {
        return configurationService.getConfiguration().getInt("storefront.lookview.component.size", 50);
    }

    /**
     * Handle exception and forward to 404 page
     * 
     * @param exception
     * @param request
     * @return String
     */
    @ExceptionHandler(Exception.class)
    public String handleException(final Exception exception, final HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        return ControllerConstants.Forward.ERROR_404;
    }

    /**
     * @return the previousLookCount
     */
    public int getPreviousLookCount() {
        return previousLookCount;
    }

    /**
     * @param previousLookCount
     *            the previousLookCount to set
     */
    @Value("#{configurationService.configuration.getInt('tgtstorefront.grouppage.relatedlooks.previous.count', 2)}")
    public void setPreviousLookCount(final int previousLookCount) {
        this.previousLookCount = previousLookCount;
    }

}
