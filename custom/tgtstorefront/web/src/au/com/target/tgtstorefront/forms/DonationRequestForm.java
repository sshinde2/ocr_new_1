/**
 * 
 */
package au.com.target.tgtstorefront.forms;


import au.com.target.tgtstorefront.forms.validation.AbnAcn;
import au.com.target.tgtstorefront.forms.validation.Common;
import au.com.target.tgtstorefront.forms.validation.DonationPreferredContactMethod;
import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.Store;
import au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum;


/**
 * @author knemalik
 * 
 */
public class DonationRequestForm {

    @Common(field = FieldTypeEnum.eventName, isMandatory = false, mustMatch = false, sizeRange = { 0, 128 })
    private String eventName;

    @Common(field = FieldTypeEnum.organisationName, isMandatory = true, mustMatch = false, sizeRange = { 0, 225 })
    private String organisationName;

    @AbnAcn
    private String abnOrAcn;

    @Common(field = FieldTypeEnum.contactName, isMandatory = true, mustMatch = false, sizeRange = { 0, 128 })
    private String contactName;

    @Common(field = FieldTypeEnum.role, isMandatory = false, mustMatch = false, sizeRange = { 0, 128 })
    private String role;

    @Common(field = FieldTypeEnum.organisationStreet, isMandatory = true, mustMatch = false, sizeRange = { 0, 255 })
    private String organisationStreet;

    @Common(field = FieldTypeEnum.organisationCity, isMandatory = true, mustMatch = false, sizeRange = { 0, 24 })
    private String organisationCity;

    @Common(field = FieldTypeEnum.organisationState, isMandatory = true, mustMatch = false, sizeRange = { 0, 24 })
    private String organisationState;

    @Common(field = FieldTypeEnum.organisationPostCode, isMandatory = true, mustMatch = false, sizeRange = { 0, 10 })
    private String organisationPostCode;

    @Email
    private String email;

    @Common(field = FieldTypeEnum.phone, isMandatory = true, mustMatch = false, sizeRange = { 0, 10 })
    private String phoneNumber;

    @Common(field = FieldTypeEnum.reason, isMandatory = true, mustMatch = false, sizeRange = { 0, 255 })
    private String reason;

    @Common(field = FieldTypeEnum.notes, isMandatory = false, mustMatch = false, sizeRange = { 0, 255 })
    private String notes;

    @DonationPreferredContactMethod
    private String preferredMethodofContact;

    @Store
    private String storeNumber;


    /**
     * @return the organisationStreet
     */
    public String getOrganisationStreet() {
        return organisationStreet;
    }

    /**
     * @param organisationStreet
     *            the organisationStreet to set
     */
    public void setOrganisationStreet(final String organisationStreet) {
        this.organisationStreet = organisationStreet;
    }

    /**
     * @return the organisationCity
     */
    public String getOrganisationCity() {
        return organisationCity;
    }

    /**
     * @param organisationCity
     *            the organisationCity to set
     */
    public void setOrganisationCity(final String organisationCity) {
        this.organisationCity = organisationCity;
    }

    /**
     * @return the organisationState
     */
    public String getOrganisationState() {
        return organisationState;
    }

    /**
     * @param organisationState
     *            the organisationState to set
     */
    public void setOrganisationState(final String organisationState) {
        this.organisationState = organisationState;
    }

    /**
     * @return the organisationPostCode
     */
    public String getOrganisationPostCode() {
        return organisationPostCode;
    }

    /**
     * @param organisationPostCode
     *            the organisationPostCode to set
     */
    public void setOrganisationPostCode(final String organisationPostCode) {
        this.organisationPostCode = organisationPostCode;
    }


    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }



    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes
     *            the notes to set
     */
    public void setNotes(final String notes) {
        this.notes = notes;
    }


    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * @return the preferredMethodofContact
     */
    public String getPreferredMethodofContact() {
        return preferredMethodofContact;
    }

    /**
     * @param preferredMethodofContact
     *            the preferredMethodofContact to set
     */
    public void setPreferredMethodofContact(final String preferredMethodofContact) {
        this.preferredMethodofContact = preferredMethodofContact;
    }


    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * @param eventName
     *            the eventName to set
     */
    public void setEventName(final String eventName) {
        this.eventName = eventName;
    }

    /**
     * @return the organisationName
     */
    public String getOrganisationName() {
        return organisationName;
    }

    /**
     * @param organisationName
     *            the organisationName to set
     */
    public void setOrganisationName(final String organisationName) {
        this.organisationName = organisationName;
    }

    /**
     * @return the abnOrAcn
     */
    public String getAbnOrAcn() {
        return abnOrAcn;
    }

    /**
     * @param abnOrAcn
     *            the abnOrAcn to set
     */
    public void setAbnOrAcn(final String abnOrAcn) {
        this.abnOrAcn = abnOrAcn;
    }


    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName
     *            the contactName to set
     */
    public void setContactName(final String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role
     *            the role to set
     */
    public void setRole(final String role) {
        this.role = role;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
