/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.util;


/**
 * Filters given string to prevent cross-site scripting
 */
public final class XSSFilterUtil
{
    private XSSFilterUtil() {
        // prevent construction
    }

    /**
     * 
     * @param value
     *            to be sanitized
     * @return sanitized content
     */
    public static String filter(final String value)
    {
        if (value == null)
        {
            return null;
        }
        String sanitized = value;
        //TODO These will be revisited in the next task
        /* sanitized = sanitized.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
         sanitized = sanitized.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");
         sanitized = sanitized.replaceAll("'", "&#39;");
         sanitized = sanitized.replaceAll("eval\\((.*)\\)", "");
         sanitized = sanitized.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");*/
        sanitized = sanitized.replaceAll("&", "%26");
        return sanitized;
    }
}
