/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.breadcrumb.impl.StoreBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.StoreFinderForm;
import au.com.target.tgtstorefront.forms.StorePositionForm;
import au.com.target.tgtstorefront.util.MetaSanitizerUtil;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;


/**
 * Store details page controller.
 */
@Controller
@RequestMapping(value = ControllerConstants.STORE_CODE_PATH_PATTERN)
public class StorePageController extends AbstractPageController
{
    protected static final Logger LOG = Logger.getLogger(StorePageController.class);

    private static final String STORE_FINDER_CMS_PAGE_LABEL = "storefinder";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "storeBreadcrumbBuilder")
    private StoreBreadcrumbBuilder storeBreadcrumbBuilder;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade storeLocatorFacade;

    /**
     * Enriches model with store details and returns store details view.
     * 
     * @param storeNumber
     *            the unique store number
     * @param model
     *            the model to fill in with details
     * @return the name of the view
     * @throws CMSItemNotFoundException
     *             if page with {@link #STORE_FINDER_CMS_PAGE_LABEL} label is not found
     */
    @RequestMapping(value = ControllerConstants.STORE_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String storeFinderDetails(@PathVariable("storeNumber") final String storeNumber, final Model model,
            final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException, UnsupportedEncodingException
    {
        final TargetPointOfServiceData pointOfServiceData = populatePointOfServiceData(storeNumber);

        if (pointOfServiceData == null) {
            return ControllerConstants.Forward.ERROR_404;
        }
        // Check the url is correct.
        final String redirection = checkRequestUrl(request, response, pointOfServiceData.getUrl());
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        model.addAttribute("storeFinderForm", new StoreFinderForm());
        model.addAttribute("storePositionForm", new StorePositionForm());
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, storeBreadcrumbBuilder.getBreadcrumbs(pointOfServiceData));
        model.addAttribute("store", pointOfServiceData);
        setUpMetaData(model, pointOfServiceData);
        storeCmsPageInModel(model, getContentPageForLabelOrId(STORE_FINDER_CMS_PAGE_LABEL));
        updatePageTitle(pointOfServiceData.getName(), model);

        return ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_DETAILS_PAGE;
    }


    @RequestMapping(value = ControllerConstants.STORE_CODE_HOURS_PATTERN, method = RequestMethod.GET)
    public String getStoreHours(@PathVariable("storeNumber") final String storeNumber, final Model model) {
        final TargetPointOfServiceData pointOfServiceData = populatePointOfServiceData(storeNumber);
        model.addAttribute("store", pointOfServiceData);
        return ControllerConstants.Views.Fragments.STORE.STORE_HOURS;
    }

    /**
     * @param storeNumber
     * @return TargetPointOfServiceData
     */
    private TargetPointOfServiceData populatePointOfServiceData(final String storeNumber) {
        TargetPointOfServiceData pointOfServiceData = null;
        try {
            if (StringUtils.isNotEmpty(storeNumber)) {
                pointOfServiceData = storeLocatorFacade.getPointOfService(Integer
                        .valueOf(storeNumber));
            }

        }
        catch (final NumberFormatException exception) {
            // Suppress
            LOG.info("Got NumberFormatException while fetching POS data, suppressing it.");
        }
        return pointOfServiceData;
    }


    /**
     * Writes metadata constructed from {@code pointOfServiceData} into {@code model}.
     * 
     * @param model
     *            the model to write metadata to
     * @param pointOfServiceData
     *            the POS to create metadata from
     */
    protected void setUpMetaData(final Model model, final TargetPointOfServiceData pointOfServiceData)
    {
        final String metaKeywords = createMetaKeywords(pointOfServiceData);
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(pointOfServiceData.getDescription());
        setUpMetaData(model, metaKeywords, metaDescription);
    }

    protected void updatePageTitle(final String storeName, final Model model)
    {
        storeContentPageTitleInModel(
                model,
                getPageTitleResolver().resolveStorePageTitle(storeName));
    }

    /**
     * Composes a meta keywords for given {@code pointOfServiceData}.
     * 
     * @param pointOfServiceData
     *            the point of service to compose keywords for
     * @return the comma-separated keywords string
     */
    private String createMetaKeywords(final TargetPointOfServiceData pointOfServiceData) {
        final AddressData address = pointOfServiceData.getAddress();
        return Joiner.on(',').skipNulls().join(
                ImmutableList.of(address.getTown(), address.getPostalCode(), address.getCountry().getName()));
    }

}
