/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms;

import de.hybris.platform.validation.annotations.NotBlank;

import au.com.target.tgtstorefront.forms.validation.EqualAttributes;
import au.com.target.tgtstorefront.forms.validation.NotEqualAttributes;
import au.com.target.tgtstorefront.forms.validation.Password;


/**
 * Form object for updating the password.
 */
@EqualAttributes(message = "{validation.checkPwd.equals}", value = { "newPassword", "checkNewPassword" })
@NotEqualAttributes(message = "{validation.newPwdOldPwd.equals}", value = { "currentPassword", "newPassword" })
public class UpdatePasswordForm {
    @NotBlank(message = "{profile.currentPassword.empty}")
    private String currentPassword;

    @Password
    private String newPassword;
    private String checkNewPassword;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(final String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(final String newPassword) {
        this.newPassword = newPassword;
    }

    public String getCheckNewPassword() {
        return checkNewPassword;
    }

    public void setCheckNewPassword(final String checkNewPassword) {
        this.checkNewPassword = checkNewPassword;
    }
}
