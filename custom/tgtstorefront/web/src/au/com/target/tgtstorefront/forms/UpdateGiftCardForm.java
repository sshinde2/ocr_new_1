/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author gbaker2
 *
 */
public class UpdateGiftCardForm {

    @NotNull(message = "{basket.error.action.notNull}")
    private String action;

    @Valid
    private GiftRecipientForm giftRecipientForm;

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * @return the giftRecipientForm
     */
    public GiftRecipientForm getGiftRecipientForm() {
        return giftRecipientForm;
    }

    /**
     * @param giftRecipientForm
     *            the giftRecipientForm to set
     */
    public void setGiftRecipientForm(final GiftRecipientForm giftRecipientForm) {
        this.giftRecipientForm = giftRecipientForm;
    }

}
