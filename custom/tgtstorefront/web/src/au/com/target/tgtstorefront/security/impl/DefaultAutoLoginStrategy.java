/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security.impl;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * Default implementation of {@link AutoLoginStrategy}
 */
public class DefaultAutoLoginStrategy implements AutoLoginStrategy {

    private static final Logger LOG = Logger.getLogger(DefaultAutoLoginStrategy.class);

    private AuthenticationManager authenticationManager;

    private CustomerFacade customerFacade;

    private TargetCookieStrategy guidCookieStrategy;

    private RememberMeServices rememberMeServices;

    private EnhancedCookieGenerator anonymousCartTokenGenerator;

    private CartService cartService;

    private ModelService modelService;

    private TargetOrderFacade targetOrderFacade;

    @Override
    public void login(final String username, final String password, final HttpServletRequest request,
            final HttpServletResponse response) {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        token.setDetails(new WebAuthenticationDetails(request));
        try {
            UserModel previousUser = null;
            if (cartService.hasSessionCart()) {
                previousUser = cartService.getSessionCart().getUser();
            }
            final Authentication authentication = getAuthenticationManager().authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            getCustomerFacade().loginSuccess();
            getAnonymousCartTokenGenerator().removeCookie(response);
            getGuidCookieStrategy().setCookie(request, response);
            rememberMeServices.loginSuccess(request, response, authentication);

            if (previousUser != null && (previousUser instanceof TargetCustomerModel
                    && CustomerType.GUEST.equals(((TargetCustomerModel)previousUser).getType()))) {
                // remove guest user
                modelService.remove(previousUser);
            }
            linkOrderToCustomer(request.getSession());
        }
        catch (final Exception e) {
            SecurityContextHolder.getContext().setAuthentication(null);
            rememberMeServices.loginFail(request, response);
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Failure during autoLogin ",
                    TgtutilityConstants.ErrorCode.ERR_TGTSTORE_AUTO_LOGIN, ""), e);
        }
    }

    /**
     * Link current order to currently signed in user
     * 
     * @param session
     */
    private void linkOrderToCustomer(final HttpSession session) {
        final String orderId = (String)session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
        if (StringUtils.isNotBlank(orderId)) {
            targetOrderFacade.linkOrderToCustomer(orderId);
        }
    }

    protected AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }

    @Required
    public void setAuthenticationManager(final AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    protected CustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    @Required
    public void setCustomerFacade(final CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    protected TargetCookieStrategy getGuidCookieStrategy() {
        return guidCookieStrategy;
    }

    /**
     * @param guidCookieStrategy
     *            the guidCookieStrategy to set
     */
    @Required
    public void setGuidCookieStrategy(final TargetCookieStrategy guidCookieStrategy) {
        this.guidCookieStrategy = guidCookieStrategy;
    }

    /**
     * @return the rememberMeServices
     */
    public RememberMeServices getRememberMeServices() {
        return rememberMeServices;
    }

    /**
     * @param rememberMeServices
     *            the rememberMeServices to set
     */
    @Required
    public void setRememberMeServices(final RememberMeServices rememberMeServices) {
        this.rememberMeServices = rememberMeServices;
    }

    /**
     * @return the anonymousCartTokenGenerator
     */
    protected EnhancedCookieGenerator getAnonymousCartTokenGenerator() {
        return anonymousCartTokenGenerator;
    }

    /**
     * @param anonymousCartTokenGenerator
     *            the anonymousCartTokenGenerator to set
     */
    @Required
    public void setAnonymousCartTokenGenerator(final EnhancedCookieGenerator anonymousCartTokenGenerator) {
        this.anonymousCartTokenGenerator = anonymousCartTokenGenerator;
    }

    /**
     * @param cartService
     *            the cartService to set
     */
    @Required
    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetOrderFacade
     *            the targetOrderFacade to set
     */
    @Required
    public void setTargetOrderFacade(final TargetOrderFacade targetOrderFacade) {
        this.targetOrderFacade = targetOrderFacade;
    }

}
