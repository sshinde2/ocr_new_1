/**
 * 
 */
package au.com.target.tgtstorefront.navigation.megamenu;

import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class MegaMenuSection {
    private String title;

    private String link;

    private String style;

    private List<MegaMenuSection> sections;

    private List<MegaMenuEntry> entries;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link
     *            the link to set
     */
    public void setLink(final String link) {
        this.link = link;
    }

    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style
     *            the style to set
     */
    public void setStyle(final String style) {
        this.style = style;
    }

    /**
     * @return the sections
     */
    public List<MegaMenuSection> getSections() {
        return sections;
    }

    /**
     * @param sections
     *            the sections to set
     */
    public void setSections(final List<MegaMenuSection> sections) {
        this.sections = sections;
    }

    /**
     * @return the entries
     */
    public List<MegaMenuEntry> getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final List<MegaMenuEntry> entries) {
        this.entries = entries;
    }
}
