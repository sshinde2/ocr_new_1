/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;


/**
 * @author Pradeep
 *
 */
public class CookieRetrievalHelper {


    /**
     * get header cookie
     * 
     * @param request
     *            httpRequest
     */
    public String getCookieValue(final HttpServletRequest request, final String cookieName) {
        if (request == null || cookieName == null) {
            return null;
        }
        final Cookie[] cookies = request.getCookies();
        if (ArrayUtils.isNotEmpty(cookies)) {
            for (final Cookie cookie : request.getCookies()) {
                if (cookieName.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }


}
