package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtstorefront.forms.validation.NotEqualAttributes;


/**
 * Validator to validate attributes are not the same.
 */
public class NotEqualAttributesValidator implements ConstraintValidator<NotEqualAttributes, Object> {
    private static final Logger LOG = Logger.getLogger(NotEqualAttributesValidator.class);

    private String firstAttribute;
    private String secondAttribute;

    @Override
    public void initialize(final NotEqualAttributes constraintAnnotation) {
        Assert.notEmpty(constraintAnnotation.value());
        Assert.isTrue(constraintAnnotation.value().length == 2);
        firstAttribute = constraintAnnotation.value()[0];
        secondAttribute = constraintAnnotation.value()[1];
        Assert.hasText(firstAttribute);
        Assert.hasText(secondAttribute);
        Assert.isTrue(!firstAttribute.equals(secondAttribute));
    }

    @Override
    public boolean isValid(final Object object, final ConstraintValidatorContext constraintContext) {
        if (object == null) {
            return true;
        }
        try {
            final Object first = PropertyUtils.getProperty(object, firstAttribute);
            final Object second = PropertyUtils.getProperty(object, secondAttribute);
            return !(first.equals(second));
        }
        catch (final Exception e) {
            LOG.error("Could not validate", e);
            throw new IllegalArgumentException(e);
        }
    }
}
