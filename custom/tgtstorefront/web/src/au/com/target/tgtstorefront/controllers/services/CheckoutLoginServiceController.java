/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtstorefront.checkout.login.request.dto.CheckoutLoginEmailAddressDTO;
import au.com.target.tgtstorefront.checkout.login.request.dto.CheckoutLoginRegistrationDTO;
import au.com.target.tgtstorefront.checkout.login.request.dto.CheckoutLoginResetPasswordDTO;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractCustomerSubscriptionController;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.forms.ENewsSubscriptionForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN)
public class CheckoutLoginServiceController extends AbstractCustomerSubscriptionController {

    protected static final Logger LOG = Logger.getLogger(CheckoutLoginServiceController.class);

    @Autowired
    private TargetCheckoutLoginResponseFacade targetCheckoutLoginResponseFacade;

    @Resource(name = "webServiceExceptionHelper")
    private WebServiceExceptionHelper webServiceExceptionHelper;

    @Autowired
    private TargetCookieStrategy guidCookieStrategy;

    @Autowired
    private AutoLoginStrategy autoLoginStrategy;

    @Autowired
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade;

    @Autowired
    private TargetCustomerFacade<?> targetCustomerFacade;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Response loginRequired() {
        sessionService.setAttribute(SPC_KEY, TURNED_ON);
        return targetCheckoutLoginResponseFacade.createLoginRequiredResponse();
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_SUCCESS, method = RequestMethod.GET)
    @ResponseBody
    public Response loginSuccess() {
        prepopulateForCheckout();
        return targetCheckoutLoginResponseFacade.createLoginSuccessResponse(getCheckoutUrl());
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_FAILURE, method = RequestMethod.GET)
    @ResponseBody
    public Response loginFailure(final HttpSession session) {
        final Object springSecurityLastException = session.getAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION);
        final Class springSecurityLastExceptionClass = springSecurityLastException != null
                ? springSecurityLastException.getClass() : null;

        session.removeAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION);

        return targetCheckoutLoginResponseFacade.createLoginFailedResponse(springSecurityLastExceptionClass);
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_CUSTOMER_REGISTERED, method = RequestMethod.POST)
    @ResponseBody
    public Response registered(@Valid final CheckoutLoginEmailAddressDTO customerUsername) {
        return targetCheckoutLoginResponseFacade.isCustomerRegistered(customerUsername.getEmailAddress());
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_GUEST, method = RequestMethod.POST)
    @ResponseBody
    public Response guest(@Valid final CheckoutLoginEmailAddressDTO customerUsername,
            final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) {
        final Response response = targetCheckoutLoginResponseFacade.processGuestCheckoutRequest(
                customerUsername.getEmailAddress(),
                getCheckoutUrl());
        if (response.isSuccess()) {
            guidCookieStrategy.setCookie(httpServletRequest, httpServletResponse);
        }
        return response;
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_REGISTER, method = RequestMethod.POST)
    @ResponseBody
    public Response register(@Valid final CheckoutLoginRegistrationDTO dto,
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse) {
        final TargetRegisterData registerData = new TargetRegisterData();
        registerData.setLogin(dto.getEmailAddress());
        registerData.setFirstName(dto.getFirstName());
        registerData.setLastName(dto.getLastName());
        registerData.setPassword(dto.getPassword());

        prepopulateForCheckout();
        final Response response = targetCheckoutLoginResponseFacade.processCustomerRegistrationRequest(registerData,
                getCheckoutUrl());

        if (response.isSuccess()) {
            autoLoginStrategy.login(dto.getEmailAddress(), dto.getPassword(), httpServletRequest, httpServletResponse);
            performCustomerSubscription(dto);
        }

        return response;
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_ENEWS_SUBSCRIPTION, method = RequestMethod.POST)
    @ResponseBody
    public Response enews(@Valid final CheckoutLoginEmailAddressDTO dto,
            final HttpServletResponse response) {
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail(dto.getEmailAddress());
        eNewsSubscriptionForm.setSubscriptionSource(ControllerConstants.ACCREDIT_CHECKOUT);
        processSubscribe(eNewsSubscriptionForm, response);
        return new Response(true);
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_RESET_PASSWORD, method = RequestMethod.POST)
    @ResponseBody
    public Response resetPassword(@Valid final CheckoutLoginEmailAddressDTO dto) {
        try {
            final String refererPage = dto.getBlankRedirectPage()
                    ? "" : ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN;
            targetCustomerFacade.forgottenPassword(dto.getEmailAddress(), refererPage);
        }
        catch (final UnknownIdentifierException e) {
            // ignore this for security reasons. the response need not indicate whether an email address is that of a registered user or not.
        }
        return new Response(true);
    }

    @RequestMapping(value = ControllerConstants.WS_API_CHECKOUT_LOGIN_SET_PASSWORD, method = RequestMethod.POST)
    @ResponseBody
    public Response setPassword(@Valid final CheckoutLoginResetPasswordDTO dto,
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse) {
        final Response response = targetCheckoutLoginResponseFacade.processSetPasswordByToken(dto.getPassword(),
                dto.getToken());
        if (response.isSuccess()) {
            final String uid = targetCustomerFacade.getUidForPasswordResetToken(dto.getToken());
            autoLoginStrategy.login(uid, dto.getPassword(), httpServletRequest, httpServletResponse);
            targetCheckoutLoginResponseFacade.processSetPasswordRedirect(response);
            prepopulateForCheckout();
        }
        return response;
    }

    @ExceptionHandler({ Exception.class })
    @ResponseBody
    public Response handleException(final HttpServletRequest request, final Exception ex) {
        return webServiceExceptionHelper.handleException(request, ex);
    }

    private void performCustomerSubscription(final CheckoutLoginRegistrationDTO dto) {
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail(dto.getEmailAddress());
        subscriptionDetails.setFirstName(dto.getFirstName());
        subscriptionDetails.setLastName(dto.getLastName());
        subscriptionDetails.setRegisterForEmail(dto.isOptIntoMarketing());
        subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WEBCHECKOUT);
        try {
            targetCustomerSubscriptionFacade.performCustomerSubscription(subscriptionDetails);
        }
        catch (final Exception e) {
            LOG.warn("Newsletter subscription failed for email=" + dto.getEmailAddress(), e);
        }
    }

    private String getCheckoutUrl() {
        if (shouldGoToSPC()) {
            return ControllerConstants.SINGLE_PAGE_CHECKOUT;
        }

        return ControllerConstants.CART + ControllerConstants.CHECKOUT;
    }

    private void prepopulateForCheckout() {
        if (shouldGoToSPC()) {
            targetPrepopulateCheckoutCartFacade.isCartPrePopulatedForCheckout();
        }
    }
}
