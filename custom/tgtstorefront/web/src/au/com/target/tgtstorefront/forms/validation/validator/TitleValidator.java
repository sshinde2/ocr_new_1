/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.TitleData;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.Title;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TitleValidator extends AbstractTargetValidator implements ConstraintValidator<Title, String> {

    @Resource(name = "targetUserFacade")
    private UserFacade userFacade;

    @Override
    public void initialize(final Title arg0) {
        field = FieldTypeEnum.title;
        isMandatory = arg0.mandatory();
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isAvailable(final String value) {
        final List<TitleData> titles = userFacade.getTitles();
        for (final TitleData titleData : titles) {
            if (titleData.getCode().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}
