/**
 * 
 */
package au.com.target.tgtstorefront.controllers.customdimensions;

/**
 * @author bmcmuffin
 * 
 */
public interface TargetCustomDimensionsMapping {
    /**
     * 
     * @return dimension for assorted
     */
    String getAssorted();

    /**
     * 
     * @return dimension for displayOnly
     */
    String getDisplayOnly();

    /**
     * 
     * @return dimension for endOfLife
     */
    String getEndOfLife();


}
