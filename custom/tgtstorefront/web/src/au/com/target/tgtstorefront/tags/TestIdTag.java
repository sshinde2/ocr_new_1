/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.tags;


import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringUtils;


/**
 * Tag that generates a wrapping div with the specified id. The id is suffixed with an incrementing counter for the page
 * request to ensure that it is unique. The wrapper divs can be turned on and off via a configuration property.
 */
public class TestIdTag extends SimpleTagSupport
{
    private String code;

    /**
     * Render a div tag with a unique test ID.
     * 
     * @see Functions#getTestIdAttribute(String, PageContext)
     */
    @Override
    public void doTag() throws JspException, IOException
    {
        final PageContext pageContext = (PageContext)getJspContext();
        final String formattedTestId = Functions.getTestIdAttribute(getCode(), pageContext);

        if (StringUtils.isNotBlank(formattedTestId))
        {
            final JspWriter jspWriter = pageContext.getOut();

            jspWriter.append("<div ").append(formattedTestId).append(" style=\"display:inline\">");

            // Write the body out
            getJspBody().invoke(jspWriter);

            jspWriter.println("</div>");
        }
        else
        {
            // Just render the contents
            getJspBody().invoke(pageContext.getOut());
        }
    }

    protected String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
