/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.commercefacades.storelocator.data.SpecialOpeningDayData;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.storelocator.data.TargetOpeningWeekData;


/**
 * @author rmcalave
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class TargetShortOpeningScheduleData {

    @JsonIgnore
    abstract List<WeekdayOpeningDayData> getWeekDayOpeningList();

    @JsonIgnore
    abstract List<SpecialOpeningDayData> getSpecialDayOpeningList();

    @JsonIgnore
    abstract List<TargetOpeningWeekData> getTargetOpeningWeeks();
}
