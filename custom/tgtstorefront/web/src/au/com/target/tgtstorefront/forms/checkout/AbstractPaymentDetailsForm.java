/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import au.com.target.tgtstorefront.forms.validation.FlyBuys;
import au.com.target.tgtstorefront.forms.validation.TeamMemberDiscountCard;


/**
 * @author gbaker2
 * 
 */
public abstract class AbstractPaymentDetailsForm {

    @TeamMemberDiscountCard
    private String teamMemberCode;
    @FlyBuys
    private String flyBuys;
    private String voucher;
    private boolean active;

    private String action;
    private String amountCurrentPayment;

    private boolean enabled;

    private String cardType;

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * @return the amountCurrentPayment
     */
    public String getAmountCurrentPayment() {
        return amountCurrentPayment;
    }

    /**
     * @param amountCurrentPayment
     *            the amountCurrentPayment to set
     */
    public void setAmountCurrentPayment(final String amountCurrentPayment) {
        this.amountCurrentPayment = amountCurrentPayment;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }


    /**
     * @return the flyBuys
     */
    public String getFlyBuys() {
        return flyBuys;
    }

    /**
     * @param flyBuys
     *            the flyBuys to set
     */
    public void setFlyBuys(final String flyBuys) {
        this.flyBuys = flyBuys;
    }

    /**
     * @return the teamMemberCode
     */
    public String getTeamMemberCode() {
        return teamMemberCode;
    }

    /**
     * @param teamMemberCode
     *            the teamMemberCode to set
     */
    public void setTeamMemberCode(final String teamMemberCode) {
        this.teamMemberCode = teamMemberCode;
    }

    /**
     * @return the voucher
     */
    public String getVoucher() {
        return voucher;
    }

    /**
     * @param voucher
     *            the voucher to set
     */
    public void setVoucher(final String voucher) {
        this.voucher = voucher;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

}
