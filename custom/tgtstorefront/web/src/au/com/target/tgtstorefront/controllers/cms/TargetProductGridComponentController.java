package au.com.target.tgtstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetProductGridComponentModel;


/**
 * Controller for CMS TargetProductGridComponentController.
 */
@Controller("TargetProductGridComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_PRODUCT_GRID_COMPONENT)
public class TargetProductGridComponentController extends
        ProductCarouselComponentController<TargetProductGridComponentModel>
{
    @Override
    protected void fillModelInternal(final HttpServletRequest request, final Model model,
            final TargetProductGridComponentModel component)
    {
        super.fillModelInternal(request, model, component);
    }

    @Override
    protected int getMaxProducts() {
        return getConfigurationService().getConfiguration().getInt("storefront.product.grid.max.products", 24);
    }
}
