/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import de.hybris.platform.acceleratorfacades.device.DeviceDetectionFacade;
import de.hybris.platform.acceleratorfacades.device.data.DeviceData;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteTheme;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.util.TargetReloadableResourceBundleMessageSource;


/**
 * Interceptor to setup the paths to the UI resource paths in the model before passing it to the view.
 * 
 * Sets up the path to the web accessible UI resources for the following: * The current site * The current theme * The
 * common resources
 * 
 * All of these paths are qualified by the current UiExperienceLevel
 */
public class UiThemeResourceBeforeViewHandler implements BeforeViewHandler
{
    protected static final String COMMON = "common";

    protected static final String UI_RESOURCE_PREFIX = "ui_resource_";

    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;

    @Resource(name = "uiExperienceService")
    private UiExperienceService uiExperienceService;

    @Resource(name = "deviceDetectionFacade")
    private DeviceDetectionFacade deviceDetectionFacade;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "targetReloadableResourceBundleMessageSource")
    private TargetReloadableResourceBundleMessageSource targetReloadableResourceBundleMessageSource;

    private final ConfigurationService configurationService;

    private String defaultThemeName;
    private String cacheBusterTimestamp;

    public UiThemeResourceBeforeViewHandler(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;

        final String buildDate = configurationService.getConfiguration().getString(
                WebConstants.STOREFRONT_BUILDDATE_CONFIG_KEY);

        if (buildDate != null) {
            cacheBusterTimestamp = buildDate.replaceFirst(" ", "");
        }
    }

    protected String getDefaultThemeName()
    {
        return defaultThemeName;
    }

    @Required
    public void setDefaultThemeName(final String defaultThemeName)
    {
        this.defaultThemeName = defaultThemeName;
    }

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView)
    {
        final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

        final String siteName = currentSite.getUid();
        final String themeName = getThemeNameForSite(currentSite);
        final String uiExperienceCode = uiExperienceService.getUiExperienceLevel().getCode();
        final String uiExperienceCodeLower = uiExperienceCode.toLowerCase();
        final String contextPath = request.getContextPath();

        final String siteResourcePath = contextPath + WebConstants.WEB_FOLDER_UI + WebConstants.FORWARD_SLASH
                + uiExperienceCodeLower
                + "/site-" + siteName;
        final String themeResourcePath = contextPath + WebConstants.WEB_FOLDER_UI + WebConstants.FORWARD_SLASH
                + uiExperienceCodeLower
                + "/theme-" + themeName;
        final String commonResourcePath = contextPath + WebConstants.WEB_FOLDER_UI + WebConstants.FORWARD_SLASH
                + uiExperienceCodeLower + WebConstants.FORWARD_SLASH
                + COMMON;

        modelAndView.addObject("siteResourcePath", siteResourcePath);
        modelAndView.addObject("themeResourcePath", themeResourcePath);
        modelAndView.addObject("commonResourcePath", commonResourcePath);

        modelAndView.addObject("uiExperienceLevel", uiExperienceCode);

        final String detectedUiExperienceCode = uiExperienceService.getDetectedUiExperienceLevel().getCode();
        modelAndView.addObject("detectedUiExperienceCode", detectedUiExperienceCode);

        final UiExperienceLevel overrideUiExperienceLevel = uiExperienceService.getOverrideUiExperienceLevel();
        if (overrideUiExperienceLevel == null)
        {
            modelAndView.addObject("uiExperienceOverride", Boolean.FALSE);
        }
        else
        {
            modelAndView.addObject("uiExperienceOverride", Boolean.TRUE);
            modelAndView.addObject("overrideUiExperienceCode", overrideUiExperienceLevel.getCode());
        }

        final DeviceData currentDetectedDevice = deviceDetectionFacade.getCurrentDetectedDevice();
        modelAndView.addObject("detectedDevice", currentDetectedDevice);

        populateCacheBuster(modelAndView);

        final boolean featureSvgBasedMenuEnabled = targetFeatureSwitchFacade
                .isFeatureEnabled("svg.based.menu");
        modelAndView.addObject("featureSvgBasedMenuEnabled", new Boolean(featureSvgBasedMenuEnabled));

        modelAndView.addObject("assetResourcePath", contextPath + WebConstants.WEB_FOLDER_ASSETS);
        final Properties uiManifest = this.targetReloadableResourceBundleMessageSource
                .getUiManifestProperties();
        for (final Entry<Object, Object> e : uiManifest.entrySet()) {
            modelAndView
                    .addObject(UI_RESOURCE_PREFIX + (String)e.getKey(), e.getValue());
        }

    }

    protected String getThemeNameForSite(final CMSSiteModel site)
    {
        final SiteTheme theme = site.getTheme();
        if (theme != null)
        {
            final String themeCode = theme.getCode();
            if (themeCode != null && !themeCode.isEmpty())
            {
                return themeCode;
            }
        }
        return getDefaultThemeName();
    }

    protected void populateCacheBuster(final ModelAndView modelAndView) {
        if (StringUtils.isNotEmpty(cacheBusterTimestamp)) {
            modelAndView.addObject(WebConstants.STOREFRONT_CACHEBUSTER_TIMESTAMP_REQUEST_KEY, cacheBusterTimestamp);

            modelAndView.addObject(WebConstants.STOREFRONT_CACHEBUSTER_REWRITE_ENABLED_REQUEST_KEY,
                    Boolean.valueOf(configurationService.getConfiguration().getBoolean(
                            WebConstants.STOREFRONT_CACHEBUSTER_REWRITE_ENABLED_CONFIG_KEY, false)));
        }
    }
}