/**
 * 
 */
package au.com.target.tgtstorefront.customersegment.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.util.CookieGenerator;

import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;


/**
 * @author rsamuel3
 *
 */
public class CustomerSegmentCookieStrategy implements TargetCookieStrategy {


    /**
     * 
     */
    public static final String SEPARATOR = "|";

    private static final String CUSTOMER_SEGMENT_LOG_PREFIX = "CUSTOMER-SEGMENT-COOKIE : ";

    private static final Logger LOG = Logger.getLogger(CustomerSegmentCookieStrategy.class);

    private CookieGenerator cookieGenerator;

    private String resetPeriod;

    private TargetUserFacade targetUserFacade;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.cookie.TargetCookieStrategy#setCookie(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void setCookie(final HttpServletRequest request, final HttpServletResponse response) {

        boolean addCookie = true;

        final Cookie segmentCookie = getCustomerSegmentCookie(request);

        if (segmentCookie != null) {
            try {
                addCookie = shouldCustomerSegmentReset(getCreatedDate(segmentCookie.getValue()));
            }
            catch (final Exception e) {
                LOG.warn(CUSTOMER_SEGMENT_LOG_PREFIX + "Unable to parse the dates", e);
            }
        }

        if (addCookie) {
            final List<String> customerSegment = targetUserFacade.getAllUserSegmentsForCurrentUser();
            final String customerSegmentCookieValue;
            try {
                customerSegmentCookieValue = buildCookieValue(customerSegment);
                cookieGenerator.addCookie(response, customerSegmentCookieValue);
            }
            catch (final IOException e) {
                LOG.error(CUSTOMER_SEGMENT_LOG_PREFIX + "Unable to set cookie", e);
            }
        }

    }

    /**
     * @param value
     * @return createdDate
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    private String getCreatedDate(final String value) throws JsonParseException, JsonMappingException, IOException {
        String date = value;
        if (value.contains(SEPARATOR)) {
            date = StringUtils.substringBefore(value, SEPARATOR);
        }
        return date;
    }

    /**
     * @param customerSegment
     * @return String representation containing created date
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     */
    private String buildCookieValue(final List<String> customerSegment) throws JsonGenerationException,
            JsonMappingException,
            IOException {
        final StringBuilder cookieValue = new StringBuilder(getCurrentDate());
        cookieValue.append(populateSegments(customerSegment));
        return cookieValue.toString();
    }

    /**
     * @param customerSegment
     * @return String representing the segments
     */
    private String populateSegments(final List<String> customerSegment) {
        final StringBuilder segments = new StringBuilder();
        if (CollectionUtils.isNotEmpty(customerSegment)) {
            for (final String seg : customerSegment) {
                segments.append(SEPARATOR).append(seg);
            }
        }
        return segments.toString();
    }

    /**
     * @return String representation of the date
     */
    private String getCurrentDate() {
        return Long.toString(new Date().getTime());
    }

    /**
     * @param strcreatedDate
     * @return boolean indicating if their segment needs to be reset
     * @throws ParseException
     */
    private boolean shouldCustomerSegmentReset(final String strcreatedDate) throws ParseException {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(strcreatedDate));
        final Date createdDate = calendar.getTime();
        final Date currentDate = new Date();
        if (currentDate.after(createdDate)) {
            final DateTime currentDateTime = new DateTime(currentDate);
            final DateTime createdDateTime = new DateTime(createdDate);
            final int hours = Hours.hoursBetween(createdDateTime, currentDateTime).getHours();
            if (hours > Integer.parseInt(resetPeriod)) {
                return true;
            }
        }
        return false;
    }

    /**
     * get the customer segment cookie
     * 
     * @param request
     * @return customer segment cookie
     */
    protected Cookie getCustomerSegmentCookie(final HttpServletRequest request) {
        if (request.getCookies() != null) {
            final String guidCookieName = cookieGenerator.getCookieName();
            if (StringUtils.isNotBlank(guidCookieName)) {
                for (final Cookie cookie : request.getCookies()) {
                    if (guidCookieName.equals(cookie.getName())) {
                        return cookie;
                    }
                }
            }
        }

        return null;

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.cookie.TargetCookieStrategy#deleteCookie(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void deleteCookie(final HttpServletRequest request, final HttpServletResponse response) {
        cookieGenerator.removeCookie(response);
    }

    /**
     * @param cookieGenerator
     *            the cookieGenerator to set
     */
    @Required
    public void setCookieGenerator(final CookieGenerator cookieGenerator) {
        this.cookieGenerator = cookieGenerator;
    }

    /**
     * @param resetPeriod
     *            the resetPeriod to set
     */
    @Required
    public void setResetPeriod(final String resetPeriod) {
        this.resetPeriod = resetPeriod;
    }

    /**
     * @param targetUserFacade
     *            the targetUserFacade to set
     */
    @Required
    public void setTargetUserFacade(final TargetUserFacade targetUserFacade) {
        this.targetUserFacade = targetUserFacade;
    }

}
