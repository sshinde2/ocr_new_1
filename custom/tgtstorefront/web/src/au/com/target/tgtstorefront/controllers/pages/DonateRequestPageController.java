package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UrlPathHelper;

import au.com.target.tgtfacades.community.TargetCommunityDonationFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.constants.WebConstants.DonationRequest.DonationRequestScope;
import au.com.target.tgtstorefront.constants.WebConstants.DonationRequest.PreferredMethodofContact;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.DonationRequestPageHelper;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.DonationRequestForm;


/**
 * Controller for Donate Request page
 * 
 * @author bmcalis2
 */
@Controller
public class DonateRequestPageController extends AbstractPageController
{

    protected static final Logger LOG = Logger.getLogger(DonateRequestPageController.class);
    private static final String SITE_CMS_PAGE_LABEL = "/communities/donate";

    @Resource(name = "urlPathHelper")
    private UrlPathHelper urlPathHelper;

    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;

    @Resource(name = "targetCommunityDonationFacade")
    private TargetCommunityDonationFacade targetCommunityDonationFacade;

    @Resource(name = "donationRequestPageHelper")
    private DonationRequestPageHelper donationRequestPageHelper;

    /**
     * For returning the donation request page.
     * 
     * @param model
     * @return view
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.DONATE_REQUEST, method = RequestMethod.GET)
    public String getDonateRequestPage(final Model model, final HttpServletRequest request)
            throws CMSItemNotFoundException
    {
        final ContentPageModel pageForRequest = getContentPageForRequest(request);
        model.addAttribute("donationRequestForm", new DonationRequestForm());
        model.addAttribute("storeList", getStoreList());
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                contentPageBreadcrumbBuilder.getBreadcrumbs(pageForRequest));
        storeCmsPageInModel(model, getContentPageForLabelOrId(SITE_CMS_PAGE_LABEL));
        return ControllerConstants.Views.Pages.DonateRequest.DONATE_REQUEST_PAGE;
    }

    @RequestMapping(value = ControllerConstants.DONATE_REQUEST, method = RequestMethod.POST)
    public String sendDonateRequestPage(@Valid final DonationRequestForm donationRequestForm,
            final BindingResult bindingResult, final Model model, final HttpServletRequest request)
            throws CMSItemNotFoundException
    {
        final ContentPageModel pageForRequest = getContentPageForRequest(request);
        model.addAttribute("donationRequestForm", donationRequestForm);
        model.addAttribute("storeList", getStoreList());
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                contentPageBreadcrumbBuilder.getBreadcrumbs(pageForRequest));
        storeCmsPageInModel(model, getContentPageForLabelOrId(SITE_CMS_PAGE_LABEL));

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "donate.request.formentry.invalid");
            return ControllerConstants.Views.Pages.DonateRequest.DONATE_REQUEST_PAGE;
        }

        //reserve for error connecting to web methods 
        if (donationRequestForm == null
                || !(targetCommunityDonationFacade.sendDonationRequest(donationRequestPageHelper
                        .populateDonationRequestDtoFromForm(donationRequestForm)))) {
            GlobalMessages.addErrorMessage(model, "donate.request.failed.message");
            return ControllerConstants.Views.Pages.DonateRequest.DONATE_REQUEST_PAGE;
        }

        GlobalMessages.addConfMessage(model, "donate.request.success.message");
        return ControllerConstants.Views.Pages.DonateRequest.DONATE_REQUEST_PAGE;
    }

    /**
     * 
     * @return the list of store used for select list
     */
    private List<SelectOption> getStoreList() {
        Map<String, List<TargetPointOfServiceData>> storesDto = null;
        storesDto = targetStoreLocatorFacade.getAllStateAndStores();

        final List<SelectOption> stores = new ArrayList<>();

        for (final Entry<String, List<TargetPointOfServiceData>> entry : storesDto.entrySet()) {
            for (final TargetPointOfServiceData targetPointOfServiceData : entry.getValue()) {
                stores.add(new SelectOption(targetPointOfServiceData.getStoreNumber().toString(),
                        targetPointOfServiceData
                                .getTargetAddressData()
                                .getState() + " - " + targetPointOfServiceData.getName()));
            }
        }
        return stores;
    }

    @ModelAttribute("states")
    public List<SelectOption> getStates()
    {
        return addressDataHelper.getStates();
    }

    @ModelAttribute("scope")
    public List<SelectOption> getScope() {
        final List<SelectOption> scopes = new ArrayList<>();
        for (final DonationRequestScope scope : DonationRequestScope.values()) {
            scopes.add(new SelectOption(scope.getValue(), scope.getValue()));
        }
        return scopes;
    }

    @ModelAttribute("preferredMethodofContact")
    public List<SelectOption> getPreferredMethodofContact() {
        final List<SelectOption> preferredMethodofContact = new ArrayList<>();
        for (final PreferredMethodofContact result : PreferredMethodofContact.values()) {
            preferredMethodofContact.add(new SelectOption(result.getValue(), result.getValue()));
        }
        return preferredMethodofContact;
    }

    /**
     * Lookup the CMS Content Page for this request.
     * 
     * @param request
     *            The request
     * @return the CMS content page
     */
    protected ContentPageModel getContentPageForRequest(final HttpServletRequest request)
    {
        // Get the path for this request.
        // Note that the path begins with a '/'
        final String lookupPathForRequest = urlPathHelper.getLookupPathForRequest(request);

        try
        {
            // Lookup the CMS Content Page by label. Note that the label value must begin with a '/'.
            return getCmsPageService().getPageForLabel(lookupPathForRequest);
        }
        catch (final CMSItemNotFoundException ignore)
        {
            // Ignore exception
            return null;
        }
    }

}
