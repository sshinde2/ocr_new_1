/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2lib.cmstags.CMSBodyTag;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author chrisbattle
 * 
 */
public class TargetCMSBodyTag extends CMSBodyTag {

    private static final Logger LOG = Logger.getLogger(TargetCMSBodyTag.class);

    private static final String HEADER_ONLY = "header-only";

    private Boolean headerOnly;

    private String bodyCssClass;

    private String itemType;

    @Override
    public int doStartTag() throws JspException {
        final AbstractPageModel currentPage = (AbstractPageModel)pageContext.getRequest().getAttribute("currentPage");
        String currentPagePk = null;
        if (currentPage != null) {
            currentPagePk = currentPage.getPk().toString();
        }
        final StringBuilder bodyTagBuilder = new StringBuilder();
        bodyTagBuilder.append("body");

        final StringBuilder bodyClass = new StringBuilder();
        // Deliberately not supporting class as string so that it does not get over-used.
        // Example of over-used body class 'home', 'category', 'product', 'checkout', 'account', 'form-page', 'white-background', 'my-first-test-page'
        if (BooleanUtils.isTrue(headerOnly)) {
            bodyClass.append(" " + HEADER_ONLY);
        }
        if (StringUtils.isNotBlank(bodyCssClass)) {
            bodyClass.append(" " + bodyCssClass);
        }

        if (StringUtils.isNotBlank(bodyClass.toString())) {
            bodyTagBuilder.append(" class=\"").append(bodyClass).append("\"");
        }

        if (StringUtils.isNotBlank(itemType)) {
            bodyTagBuilder.append(" itemscope itemtype=\"" + itemType + "\"");
        }

        if (isPreviewDataModelValid()) {
            bodyTagBuilder.append(" data-page-pk=\"" + currentPagePk + "\"");
            bodyTagBuilder.append(" data-current-user-id=\"" + getCurrentUserUid() + "\"");
            bodyTagBuilder.append(" data-current-jalo-session-id=\"" + getCurrentJaloSessionId() + "\"");
        }

        if (isLiveEdit()) {
            bodyTagBuilder.append(" data-live-edit-active=\"true\"");
        }

        try {
            pageContext.getOut().print("<" + bodyTagBuilder.toString() + ">\n");
        }
        catch (final IOException e) {
            LOG.warn("Error processing tag: " + e.getMessage());
        }
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() throws JspException {
        try {
            final StringBuilder bodyTagBuilder = new StringBuilder();
            bodyTagBuilder.append("</body>");
            pageContext.getOut().print(bodyTagBuilder.toString());
        }
        catch (final IOException e) {
            LOG.warn("Error processing tag: " + e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * @return the headerOnly
     */
    public Boolean getHeaderOnly() {
        return headerOnly;
    }

    /**
     * @param headerOnly
     *            the headerOnly to set
     */
    public void setHeaderOnly(final Boolean headerOnly) {
        this.headerOnly = headerOnly;
    }

    /**
     * @return the bodyCssClass
     */
    public String getBodyCssClass() {
        return bodyCssClass;
    }

    /**
     * @param bodyCssClass
     *            the bodyCssClass to set
     */
    public void setBodyCssClass(final String bodyCssClass) {
        this.bodyCssClass = bodyCssClass;
    }

    /**
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * @param itemType
     *            the itemType to set
     */
    public void setItemType(final String itemType) {
        this.itemType = itemType;
    }
}
