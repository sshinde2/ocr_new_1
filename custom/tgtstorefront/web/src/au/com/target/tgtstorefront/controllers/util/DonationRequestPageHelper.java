/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtstorefront.constants.WebConstants.DonationRequest.DonationRequestScope;
import au.com.target.tgtstorefront.forms.DonationRequestForm;
import au.com.target.tgtstorefront.util.XSSFilterUtil;


/**
 * @author bhuang3
 * 
 */
public class DonationRequestPageHelper {

    /**
     * populate the dto from form
     * 
     * @param form
     * @return return TargetCommunityDonationRequestDto, if form is null, return null
     */

    public TargetCommunityDonationRequestDto populateDonationRequestDtoFromForm(final DonationRequestForm form) {

        final TargetCommunityDonationRequestDto dto = new TargetCommunityDonationRequestDto();
        if (null != form) {
            dto.setAbnOrAcn(XSSFilterUtil.filter(form.getAbnOrAcn()));
            dto.setContactName(XSSFilterUtil.filter(form.getContactName()));
            dto.setEventName(XSSFilterUtil.filter(form.getEventName()));
            dto.setOrgCity(XSSFilterUtil.filter(form.getOrganisationCity()));
            dto.setOrgName(XSSFilterUtil.filter(form.getOrganisationName()));
            dto.setOrgState(XSSFilterUtil.filter(form.getOrganisationState()));
            dto.setOrgStreet(XSSFilterUtil.filter(form.getOrganisationStreet()));
            dto.setReason(XSSFilterUtil.filter(form.getReason()));
            dto.setRole(XSSFilterUtil.filter(form.getRole()));
            dto.setRequestNotes(XSSFilterUtil.filter(form.getNotes()));
            dto.setOrgPostcode(form.getOrganisationPostCode());
            dto.setPhone(form.getPhoneNumber());
            dto.setPreferredMethodOfContact(form.getPreferredMethodofContact());
            dto.setStoreNumber(form.getStoreNumber());
            dto.setEmail(form.getEmail());
            dto.setOrgCountry("");
            dto.setOrgFocus("");
            dto.setScope(DonationRequestScope.Local.getValue());
            return dto;
        }
        return null;

    }

}
