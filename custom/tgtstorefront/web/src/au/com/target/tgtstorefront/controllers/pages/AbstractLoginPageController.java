/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.forms.GuestForm;
import au.com.target.tgtstorefront.forms.LoginForm;
import au.com.target.tgtstorefront.forms.RegisterForm;


/**
 * Abstract base class for login page controllers
 */
public abstract class AbstractLoginPageController extends AbstractRegisterPageController {
    protected static final Logger LOG = Logger.getLogger(AbstractLoginPageController.class);

    protected static final String SPRING_SECURITY_LAST_USERNAME = "SPRING_SECURITY_LAST_USERNAME";

    protected static final String CUSTOMER_GROUP = "customergroup";

    @Autowired
    private TargetCookieStrategy guidCookieStrategy;

    protected String getDefaultLoginPage(final HttpSession session, final Model model)
            throws CMSItemNotFoundException {
        final LoginForm loginForm = new LoginForm();
        model.addAttribute(loginForm);
        model.addAttribute(new RegisterForm());
        model.addAttribute(new GuestForm());

        addLoginErrorIfLastLoginAttemptFailed(session, model);

        setLoginPageCommon(model, session, loginForm);


        return getView();
    }

    /**
     * add a global error if last login attempt failed
     * 
     * @param session
     * @param model
     */
    private void addLoginErrorIfLastLoginAttemptFailed(final HttpSession session, final Model model) {
        final Object lastException = session.getAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION);
        if (lastException == null) {
            return;
        }
        else if (lastException instanceof BadCredentialsException) {
            GlobalMessages.addErrorMessage(model, "login.error.account.not.found.title");
        }
        else if (lastException instanceof LockedException) {
            GlobalMessages.addErrorMessage(model, "login.error.account.locked",
                    new Object[] { ControllerConstants.FORGOTTEN_PASSWORD });
        }
        session.removeAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION);
    }

    /**
     * 
     * @param model
     * @throws CMSItemNotFoundException
     */
    protected void setLoginPageCommon(final Model model, final HttpSession session, final LoginForm loginForm)
            throws CMSItemNotFoundException {
        final String username = (String)session.getAttribute(SPRING_SECURITY_LAST_USERNAME);
        session.removeAttribute(SPRING_SECURITY_LAST_USERNAME);

        loginForm.setJ_username(username);

        storeCmsPageInModel(model, getCmsPage());
        setUpMetaDataForContentPage(model, (ContentPageModel)getCmsPage());
        model.addAttribute("metaRobots", "index,no-follow");

        final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.login",
                null,
                getI18nService().getCurrentLocale()), null);
        model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));

    }

    public String handleRegistrationError(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getCmsPage());
        setUpMetaDataForContentPage(model, (ContentPageModel)getCmsPage());
        return getView();
    }

    /**
     * @param guidCookieStrategy
     *            the guidCookieStrategy to set
     */
    public void setGuidCookieStrategy(final TargetCookieStrategy guidCookieStrategy) {
        this.guidCookieStrategy = guidCookieStrategy;
    }

    /**
     * @return the guidCookieStrategy
     */
    protected TargetCookieStrategy getGuidCookieStrategy() {
        return guidCookieStrategy;
    }
}
