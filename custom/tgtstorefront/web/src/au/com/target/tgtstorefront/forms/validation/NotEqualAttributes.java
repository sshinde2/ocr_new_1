
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.validation.Constraint;
import javax.validation.Payload;

import au.com.target.tgtstorefront.forms.validation.validator.NotEqualAttributesValidator;


@Retention(RUNTIME)
@Constraint(validatedBy = NotEqualAttributesValidator.class)
@Documented
public @interface NotEqualAttributes {
    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] value();
}
