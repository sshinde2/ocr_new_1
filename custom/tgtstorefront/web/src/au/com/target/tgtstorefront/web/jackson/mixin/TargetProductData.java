/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.StockData;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;

import au.com.target.tgtfacades.data.CanonicalData;
import au.com.target.tgtfacades.kiosk.product.data.BulkyBoardProductData;
import au.com.target.tgtfacades.product.data.enums.TargetManchesterSizeEnum;


/**
 * @author htan3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class TargetProductData {
    @JsonIgnore
    abstract String getColourName();

    @JsonProperty("colour")
    abstract String getSwatch();

    @JsonIgnore
    abstract boolean isSizeVariant();

    @JsonIgnore
    abstract CanonicalData getCanonical();

    @JsonIgnore
    abstract String getProductType();

    @JsonIgnore
    abstract String getSellableVariantDisplayCode();

    @JsonIgnore
    abstract TargetManchesterSizeEnum getManchesterSize();

    @JsonIgnore
    abstract boolean isColourVariant();

    @JsonIgnore
    abstract CanonicalData getBaseProductCanonical();

    @JsonIgnore
    abstract boolean isHasSizeChart();

    @JsonIgnore
    abstract String getColourVariantCode();

    @JsonIgnore
    abstract Date getNewLowerPriceStartDate();

    @JsonIgnore
    abstract DateTime getLastModified();

    @JsonIgnore
    abstract boolean isClickAndCollectExcludeTargetCountry();

    @JsonIgnore
    abstract String getProductPromotionalDeliverySticker();

    @JsonIgnore
    abstract PriceRangeData getPriceRange();

    @JsonIgnore
    abstract PriceData getWasPrice();

    @JsonIgnore
    abstract boolean isGiftCard();

    @JsonIgnore
    abstract List getPromotionStatuses();

    @JsonIgnore
    abstract PriceRangeData getWasPriceRange();

    @JsonIgnore
    abstract boolean isShowWasPrice();

    @JsonIgnore
    abstract boolean isShowWhenOutOfStock();

    @JsonIgnore
    abstract boolean isOnlineExclusive();

    @JsonIgnore
    abstract boolean isBulkyBoardProduct();

    @JsonIgnore
    abstract String getDealDescription();

    @JsonIgnore
    abstract List getProductFeatures();

    @JsonIgnore
    abstract List getCareInstructions();

    @JsonIgnore
    abstract String getYouTubeLink();

    @JsonIgnore
    abstract String getSizeChartURL();

    @JsonIgnore
    abstract boolean isAllowDeliverToStore();

    @JsonIgnore
    abstract boolean isProductDeliveryToStoreOnly();

    @JsonIgnore
    abstract boolean isSizeChartDisplay();

    @JsonIgnore
    abstract PriceData getBulkyHomeDeliveryFee();

    @JsonIgnore
    abstract PriceData getBulkyCncFreeThreshold();

    @JsonIgnore
    abstract String getProductTypeCode();

    @JsonIgnore
    abstract BulkyBoardProductData getBulkyBoard();

    @JsonIgnore
    abstract List getMaterials();

    @JsonIgnore
    abstract String getExtraInfo();

    @JsonIgnore
    abstract String getSizeType();

    @JsonIgnore
    abstract Map getAvailable();

    @JsonIgnore
    abstract Map getInStock();

    @JsonIgnore
    abstract boolean isBigAndBulky();

    @JsonIgnore
    abstract boolean isNewArrived();

    @JsonIgnore
    abstract String getItemType();

    @JsonIgnore
    abstract boolean isPreview();

    @JsonIgnore
    abstract String getApn();

    @JsonIgnore
    abstract List getDeliveryModes();

    @JsonIgnore
    abstract String getProductPromotionalDeliveryText();

    @JsonIgnore
    abstract String getSummary();

    @JsonIgnore
    abstract StockData getStock();

    @JsonIgnore
    abstract Collection getReviews();

    @JsonIgnore
    abstract List getProductReferences();

    @JsonIgnore
    abstract Collection getClassifications();

    @JsonIgnore
    abstract Double getAverageRating();

    @JsonIgnore
    abstract Boolean getPurchasable();

    @JsonIgnore
    abstract List getVolumePrices();

    @JsonIgnore
    abstract String getVariantType();

    @JsonIgnore
    abstract String getDescription();

    @JsonIgnore
    abstract List getVariantMatrix();

    @JsonIgnore
    abstract List getBaseOptions();

    @JsonIgnore
    abstract String getBaseProduct();

    @JsonIgnore
    abstract Boolean getAvailableForPickup();

    @JsonIgnore
    abstract List getVariantOptions();

    @JsonIgnore
    abstract Integer getNumberOfReviews();

    @JsonIgnore
    abstract String getUrl();

    @JsonIgnore
    abstract PriceData getPrice();

    @JsonIgnore
    abstract Collection getImages();

    @JsonIgnore
    abstract Collection getCategories();

    @JsonIgnore
    abstract String getManufacturer();

    @JsonIgnore
    abstract Boolean getVolumePricesFlag();

    @JsonIgnore
    abstract List getFutureStocks();

    @JsonIgnore
    abstract Collection getPotentialPromotions();

}
