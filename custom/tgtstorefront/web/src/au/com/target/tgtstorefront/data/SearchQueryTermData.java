/**
 * 
 */
package au.com.target.tgtstorefront.data;

import java.io.Serializable;


/**
 * @author rsamuel3
 * 
 */
public class SearchQueryTermData implements Serializable {
    private String key;
    private String value;

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key
     *            the key to set
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }
}
