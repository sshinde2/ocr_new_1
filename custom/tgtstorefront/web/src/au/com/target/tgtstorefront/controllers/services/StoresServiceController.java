/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import de.hybris.platform.commercefacades.storefinder.StoreFinderFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.text.MessageFormat;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.CookieGenerator;

import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.google.location.finder.TargetGoogleLocationFinderFacade;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/stores")
public class StoresServiceController {

    private static final Logger LOG = Logger.getLogger(StoresServiceController.class);

    private static final String ERR_STOCK_VIS_COOKIE = "Unable to store cookie for the location={0}";

    private static final String DEFAULT_DELIVERY_LOCATION_CODE = "default.location";

    private static final String INTERNATIONAL_LOCATION = "INT";

    @Resource
    private TargetStoreFinderStockFacade targetStoreFinderStockFacade;

    @Resource(name = "inStoreStockSearchQuerySanitiser")
    private SearchQuerySanitiser inStoreStockSearchQuerySanitiser;

    @Resource(name = "inStoreStockSearchProductCodeSanitiser")
    private SearchQuerySanitiser inStoreStockSearchProductCodeSanitiser;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "webServiceExceptionHelper")
    private WebServiceExceptionHelper webServiceExceptionHelper;

    @Resource(name = "storeStockPostCodeCookieGenerator")
    private CookieGenerator storeStockPostCodeCookieGenerator;

    @Resource(name = "storeFinderFacade")
    private StoreFinderFacade storeFinderFacade;

    @Resource(name = "targetSharedConfigFacade")
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Resource(name = "targetGoogleLocationFinderFacade")
    private TargetGoogleLocationFinderFacade targetGoogleLocationFinderFacade;

    @ResponseBody
    @RequestMapping(value = "/nearest", method = RequestMethod.GET)
    public Response getNearestStores(
            @RequestParam(required = false) final Double longitude,
            @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final String pc,
            @RequestParam(required = false) final Integer sn,
            @RequestParam(required = false, defaultValue = "0") final int currentPage,
            final HttpServletResponse httpServletResponse) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getNearestStores: pc=" + pc + " | location=" + location + " | latitude="
                    + latitude
                    + " | longitude=" + longitude
                    + " | sn=" + sn);
        }
        String sanitisedLocation = null;
        if (StringUtils.isNotEmpty(location)) {
            sanitisedLocation = inStoreStockSearchQuerySanitiser.sanitiseSearchText(location);
        }
        String sanitisedProductCode = null;
        if (StringUtils.isNotEmpty(pc)) {
            sanitisedProductCode = inStoreStockSearchProductCodeSanitiser.sanitiseSearchText(pc);
        }
        final PageableData pageableData = createPageableData();
        pageableData.setPageSize(configurationService.getConfiguration().getInt("storefront.storelocator.pageSize", 5));
        pageableData.setCurrentPage(currentPage);
        final Response response = targetStoreFinderStockFacade.doSearchProductStock(sanitisedProductCode,
                sanitisedLocation, latitude,
                longitude,
                sn, pageableData, false);
        if (response.isSuccess()) {
            if (StringUtils.isNotEmpty(sanitisedLocation)) {
                final ProductStockSearchResultResponseData responseData = (ProductStockSearchResultResponseData)response
                        .getData();
                addToCookieForSelectedLocation(httpServletResponse, sanitisedLocation, inStoreStockSearchQuerySanitiser
                        .sanitiseSearchText(responseData.getSelectedLocation()));
            }
            //Following step will add location details to t_pc browser cookie derived from latLng input parameters by making geoCoding API request.
            if (null != latitude && null != longitude) {
                final String userCurrentLocation = targetGoogleLocationFinderFacade.searchUserCurrentAddressUsingLatLng(
                        latitude, longitude);
                if (StringUtils.isNotEmpty(userCurrentLocation)) {
                    addToCookieForSelectedLocation(httpServletResponse, userCurrentLocation,
                            inStoreStockSearchQuerySanitiser
                                    .sanitiseSearchText(userCurrentLocation));
                }
            }
        }
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "leadtimes/{deliveryType}", method = RequestMethod.POST)
    public Response getNearestCncHdDeliveryLocation(@PathVariable final String deliveryType,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final Integer storeNumber,
            @RequestParam(required = false) final Integer akGeo) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format(
                    "getNearestCncHdDeliveryLocation with location={0}, storeNumber={1} and akGeo={2}", location,
                    storeNumber, akGeo));
        }
        final String sanitisedLocation = getSanitisedLocation(location, storeNumber);
        final PageableData pageableData = createPageableData();
        pageableData.setPageSize(1);
        final Response response = targetStoreFinderStockFacade.nearestStoreLocation(deliveryType,
                    sanitisedLocation,
                    storeNumber, pageableData);
        if (response != null && response.isSuccess() && Integer.valueOf(1).equals(akGeo)) {
            LOG.info("akamaiGeoLocation=" + sanitisedLocation);
        }
        return response;
    }

    /**
     * @param location
     * @param storeNumber
     * @return String
     */
    private String getSanitisedLocation(final String location, final Integer storeNumber) {
        String sanitisedLocation = null;
        if (StringUtils.isNotEmpty(location)) {
            sanitisedLocation = inStoreStockSearchQuerySanitiser.sanitiseSearchText(location);
        }
        //If no data(location or storeNumber) provided then default value for the location will be set.
        if (null == storeNumber) {
            sanitisedLocation = StringUtils.isEmpty(sanitisedLocation)
                    || INTERNATIONAL_LOCATION.equalsIgnoreCase(sanitisedLocation)
                            ? targetSharedConfigFacade.getConfigByCode(DEFAULT_DELIVERY_LOCATION_CODE)
                            : sanitisedLocation;
        }
        return sanitisedLocation;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response handleException(final HttpServletRequest request,
            final Exception exception) {
        return webServiceExceptionHelper.handleException(request, exception);
    }

    protected PageableData createPageableData() {
        return new PageableData();
    }

    /**
     * This method will add cookie for the selected location.
     *
     * @param httpServletResponse
     * @param location
     * @param sanitisedSelectedLocation
     */
    private void addToCookieForSelectedLocation(final HttpServletResponse httpServletResponse,
            final String location, final String sanitisedSelectedLocation) {
        try {
            storeStockPostCodeCookieGenerator.addCookie(httpServletResponse,
                    URIUtil.encodePath(sanitisedSelectedLocation, "utf-8"));
        }
        catch (final URIException uriException) {
            LOG.error(MessageFormat.format(ERR_STOCK_VIS_COOKIE, location));
        }
    }

}
