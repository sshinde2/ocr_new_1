/**
 * 
 */
package au.com.target.tgtstorefront.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.csrf.CsrfException;


/**
 * Handles CSRF exceptions for ws-api requests by redirecting to login required request.
 * 
 * @author mgazal
 *
 */
public class CsrfExceptionHandler extends AccessDeniedHandlerImpl {

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private String redirectUrl;

    @Override
    public void handle(final HttpServletRequest request, final HttpServletResponse response,
            final AccessDeniedException accessDeniedException) throws IOException, ServletException {
        if (accessDeniedException instanceof CsrfException) {
            redirectStrategy.sendRedirect(request, response, redirectUrl);
            return;
        }
        super.handle(request, response, accessDeniedException);
    }

    /**
     * @param redirectUrl
     *            the redirectUrl to set
     */
    @Required
    public void setRedirectUrl(final String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}
