/**
 * Controller class for generating navigation departments
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.NavigationNodeHelper;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment;
import au.com.target.tgtwebcore.model.cms2.components.TargetDepartmentsNavigationComponentModel;


@Controller("TargetDepartmentsNavigationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_DEPARTMENT_NAVIGATION_COMPONENT)
public class TargetDepartmentsNavigationComponentController extends
        AbstractCMSComponentController<TargetDepartmentsNavigationComponentModel> {

    @Resource(name = "navigationNodeHelper")
    private NavigationNodeHelper navigationNodeHelper;

    /**
     * Builds list of {@link MegaMenuDepartment} and add to model
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetDepartmentsNavigationComponentModel component) {

        final CMSNavigationNodeModel departmentNavNode = component.getNavigationNode();

        if (departmentNavNode == null || CollectionUtils.isEmpty(departmentNavNode.getChildren())) {
            return;
        }

        final List<MegaMenuDepartment> departments = getDepartments(departmentNavNode);
        model.addAttribute("departments", departments);
        model.addAttribute("departmentsSVGUrl", getSVGSpriteUrl(departmentNavNode));
        model.addAttribute("layoutType", component.getLayoutType());
    }

    /**
     * Returns list of {@link MegaMenuDepartment}
     * 
     * @param departmentNavNode
     *            navigation node
     * 
     * @return list of department navigation nodes
     */
    protected List<MegaMenuDepartment> getDepartments(final CMSNavigationNodeModel departmentNavNode) {
        return navigationNodeHelper.buildDepartmentWithoutChildren(departmentNavNode);
    }

    /**
     * Get SVG URL for node
     * 
     * @param navigationNode
     *            Navigation node
     * @return SVG URL
     */
    protected String getSVGSpriteUrl(final CMSNavigationNodeModel navigationNode) {
        return navigationNodeHelper.getSVGSpriteUrl(navigationNode);
    }
}