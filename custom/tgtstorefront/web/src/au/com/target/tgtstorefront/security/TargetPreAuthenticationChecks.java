/**
 * 
 */
package au.com.target.tgtstorefront.security;

import de.hybris.platform.core.Constants;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;


/**
 * @author rmcalave
 * 
 */
public class TargetPreAuthenticationChecks implements UserDetailsChecker {

    private static final String ROLE_ADMIN_GROUP = "ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase();

    private TargetAuthenticationFacade targetAuthenticationFacade;

    private UserService userService;

    private GrantedAuthority adminAuthority = new SimpleGrantedAuthority(ROLE_ADMIN_GROUP);

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetailsChecker#check(org.springframework.security.core.userdetails.UserDetails)
     */
    @Override
    public void check(final UserDetails details) {
        // Check if user is anonymous
        if (Constants.USER.ANONYMOUS_CUSTOMER.equalsIgnoreCase(details.getUsername()))
        {
            throw new BadCredentialsException("Login attempt as " + Constants.USER.ANONYMOUS_CUSTOMER + " is rejected");
        }

        // Check if user is admin
        if (Constants.USER.ADMIN_EMPLOYEE.equalsIgnoreCase(details.getUsername()))
        {
            throw new BadCredentialsException("Login attempt as " + Constants.USER.ADMIN_EMPLOYEE + " is rejected");
        }

        // Check if the user is in role admingroup
        if (getAdminAuthority() != null && details.getAuthorities().contains(getAdminAuthority()))
        {
            throw new BadCredentialsException("Login attempt as " + Constants.USER.ADMIN_USERGROUP + " is rejected");
        }

        try {
            userService.getUserForUID(details.getUsername(), TargetCustomerModel.class);
        }
        catch (final UnknownIdentifierException ex) {
            throw new BadCredentialsException("User doesn't exist", ex);
        }
        catch (final ClassMismatchException ex) {
            throw new BadCredentialsException("Not a authorised user to loging in web store", ex);
        }

        // Check if the account is locked (due to too many login attempts)
        if (getTargetAuthenticationFacade().isAccountLocked(details.getUsername())) {
            throw new LockedException("Too many login attempts for user");
        }
    }

    /**
     * @return the targetAuthenticationFacade
     */
    protected TargetAuthenticationFacade getTargetAuthenticationFacade() {
        return targetAuthenticationFacade;
    }

    /**
     * @param targetAuthenticationFacade
     *            the targetAuthenticationFacade to set
     */
    @Required
    public void setTargetAuthenticationFacade(final TargetAuthenticationFacade targetAuthenticationFacade) {
        this.targetAuthenticationFacade = targetAuthenticationFacade;
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param adminGroup
     *            the adminGroup to set
     */
    public void setAdminGroup(final String adminGroup)
    {
        if (StringUtils.isBlank(adminGroup))
        {
            adminAuthority = null;
        }
        else
        {
            adminAuthority = new SimpleGrantedAuthority(adminGroup);
        }
    }

    protected GrantedAuthority getAdminAuthority()
    {
        return adminAuthority;
    }
}
