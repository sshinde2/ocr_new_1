/**
 * 
 */
package au.com.target.tgtstorefront.navigation.megamenu;

import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class MegaMenuColumn {
    private List<MegaMenuSection> sections;

    private String style;
    private String title;

    /**
     * @return the sections
     */
    public List<MegaMenuSection> getSections() {
        return sections;
    }

    /**
     * @param sections
     *            the sections to set
     */
    public void setSections(final List<MegaMenuSection> sections) {
        this.sections = sections;
    }

    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style
     *            the style to set
     */
    public void setStyle(final String style) {
        this.style = style;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the style to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }
}
