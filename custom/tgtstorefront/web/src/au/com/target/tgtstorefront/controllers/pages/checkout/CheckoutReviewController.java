/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.PlaceOrderForm;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.REVIEW)
@RequireHardLogIn
public class CheckoutReviewController extends AbstractCheckoutController implements MessageSourceAware {

    protected static final Logger LOG = Logger.getLogger(CheckoutReviewController.class);
    private static final String EXPEDITED = "expedited";

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    private MessageSource messageSource;

    @Override
    protected String getRedirectionAndCheckSOH(final Model model, final RedirectAttributes redirectAttributes,
            final String url, final TargetCartData cartData) {
        final String redirect = super.getRedirectionAndCheckSOH(model, redirectAttributes, url, cartData);
        if (StringUtils.isNotBlank(redirect)) {
            return redirect;
        }

        if (getCheckoutFacade().hasIncompleteDeliveryDetails()) {
            return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
        }
        if (hasNoPaymentInfo(cartData)) {
            return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
        }

        return getRedirectionIfFlybuysReassessed(redirectAttributes);
    }

    /**
     * 
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET)
    public String orderSummary(final Model model, final RedirectAttributes redirectAttributes,
            final HttpServletRequest request,
            @RequestParam(value = EXPEDITED, required = false) final String expedited)
                    throws CMSItemNotFoundException {
        getCheckoutFacade().validateDeliveryMode();

        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (isIpgPayment(cartData.getPaymentInfo())) {
            // reverse existing gift card payments (if any) before retrieving a new token
            reverseExistingGiftCardPayments(cartData);

            if (!generateIpgToken(request, model, getDomainUrl(request), cartData)) {
                String paymentType = null;
                if (isIpgCreditCardPayment(cartData.getPaymentInfo())) {
                    paymentType = messageSource.getMessage("checkout.paymentMethod.creditcard", null,
                            commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage()));
                    redirectAttributes.addFlashAttribute("ipgCreditCardUnavailable", Boolean.TRUE);
                }
                else if (isIpgGiftCardPayment(cartData.getPaymentInfo())) {
                    paymentType = messageSource.getMessage("checkout.paymentMethod.giftcard", null,
                            commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage()));
                    redirectAttributes.addFlashAttribute("ipgGiftCardUnavailable", Boolean.TRUE);
                }

                if (paymentType != null) {
                    GlobalMessages.addFlashErrorMessage(redirectAttributes,
                            "checkout.error.paymentmethod.ipg.unavailable", new String[] { paymentType });
                }

                return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
            }
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }

        if (getCheckoutFacade().isPaymentModeGiftCard() && !getCheckoutFacade().isGiftCardPaymentAllowedForCart()) {
            getCheckoutFacade().removePaymentInfo();
            GlobalMessages.addFlashErrorMessage(redirectAttributes,
                    "checkout.error.paymentmethod.ipg.giftcard.not.allowed");
            return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
        }

        addCommonElementsToModel(model, cartData, request.getSession());
        addExpediteCheckoutMessage(model, expedited);
        return ControllerConstants.Views.Pages.MultiStepCheckout.REVIEW_YOUR_ORDER_PAGE;
    }

    /**
     * Add global message to inform customer how he has arrived directly to order review page
     * 
     * @param model
     */
    protected void addExpediteCheckoutMessage(final Model model, final String expedited) {
        if (wasCustomerExpeditedToReviewPage(expedited)) {
            GlobalMessages.addInfoMessage(model, "expedite.checkout.redirect.message");
            model.addAttribute(EXPEDITED, Boolean.TRUE);

        }
    }

    /**
     * Checks weather expedite checkout is Turned on or not
     * 
     * @param expedite
     * @return boolean
     */
    protected boolean wasCustomerExpeditedToReviewPage(final String expedite) {
        return StringUtils.equalsIgnoreCase(expedite, EXPEDITED_YES);
    }

    protected void addCommonElementsToModel(final Model model, final TargetCartData cartData, final HttpSession session)
            throws CMSItemNotFoundException {
        model.addAttribute("cartData", cartData);

        if (((TargetZoneDeliveryModeData)cartData.getDeliveryMode()).isDeliveryToStore()) {
            final Integer storeNumber = cartData.getCncStoreNumber();

            if (storeNumber != null) {
                //TODO use a common method to add store data
                final TargetPointOfServiceData selectedStore = getTargetStoreLocatorFacade().getPointOfService(
                        storeNumber);

                model.addAttribute("selectedStore", selectedStore);
            }
        }

        model.addAttribute(new PlaceOrderForm());

        final String tokenName = TargetSessionTokenUtil.getTokenKey();
        model.addAttribute("tokenName", tokenName);
        model.addAttribute("tokenValue", session.getAttribute(tokenName));

        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_REVIEW_YOUR_ORDER_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_REVIEW_YOUR_ORDER_CMS_PAGE_LABEL));
        model.addAttribute("metaRobots", "no-index,no-follow");
    }

    /**
     * @param sessionId
     * @param token
     */
    private String createIFrameUrl(final String sessionId, final String token) {
        final StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(getCheckoutFacade().getIpgBaseUrl());
        urlBuilder.append("?sessionId=");
        urlBuilder.append(sessionId);
        urlBuilder.append("&SST=");
        urlBuilder.append(token);
        return urlBuilder.toString();
    }

    protected boolean generateIpgToken(final HttpServletRequest request, final Model model,
            final String domainUrl, final TargetCartData cartData) {

        try {
            final String token = getCheckoutFacade()
                    .getIpgSessionToken(domainUrl + ControllerConstants.PLACE_ORDER
                            + ControllerConstants.PLACE_ORDER_IPG_IFRAME_RETURN)
                    .getRequestToken();
            if (StringUtils.isNotEmpty(token)) {
                if (getTargetUserFacade().hasSavedValidIpgCreditCards()) {
                    model.addAttribute("existIpgSavedCreditCards", Boolean.TRUE);
                }
                if (isIpgGiftCardPayment(cartData.getPaymentInfo())) {
                    model.addAttribute("giftCardPaymentMode", Boolean.TRUE);
                }
                request.getSession().setAttribute(token, token);
                getCheckoutFacade().updateIpgPaymentInfoWithToken(token);
                model.addAttribute("IPGIframeUrl",
                        createIFrameUrl(getCheckoutFacade().getCart().getUniqueKeyForPayment(), token));
                return true;
            }

        }
        catch (final Exception e) {
            LOG.error("fail to get ipg token", e);
            return false;
        }

        return false;
    }

    /* (non-Javadoc)
     * @see org.springframework.context.MessageSourceAware#setMessageSource(org.springframework.context.MessageSource)
     */
    @Override
    public void setMessageSource(final MessageSource paramMessageSource) {
        this.messageSource = paramMessageSource;
    }
}
