/**
 * This package contains abstract classes with Jackson MixIn Annotations for real data object.
 * The mixin jackson annotation is used in TargetMappingJacksonHttpMessageConverter to convert the data object to the required json format.
 */
package au.com.target.tgtstorefront.web.jackson.mixin;