/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.MobilePhone;
import au.com.target.tgtstorefront.forms.validation.Store;


/**
 * @author rmcalave
 * 
 */
public class ClickAndCollectDetailsForm {
    private String deliveryMode;

    @Store(checkClickAndCollectAvailable = true)
    private String selectedStoreNumber;

    private String title;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @MobilePhone(mandatory = false)
    private String phoneNumber;

    /**
     * @return the selectedStoreNumber
     */
    public String getSelectedStoreNumber() {
        return selectedStoreNumber;
    }

    /**
     * @param selectedStoreNumber
     *            the selectedStoreNumber to set
     */
    public void setSelectedStoreNumber(final String selectedStoreNumber) {
        this.selectedStoreNumber = selectedStoreNumber;
    }


    /**
     * Abbreviated getter to reduce payload size
     * 
     * @return the selectedStoreNumber
     */
    public String getSSN() {
        return selectedStoreNumber;
    }

    /**
     * Abbreviated setter to reduce payload size
     * 
     * @param ssn
     *            the selectedStoreNumber to set
     */
    public void setSSN(final String ssn) {
        this.selectedStoreNumber = ssn;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the deliveryMode
     */
    public String getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * @param deliveryMode
     *            the deliveryMode to set
     */
    public void setDeliveryMode(final String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

}