package au.com.target.tgtstorefront.data.sitemap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.joda.time.DateTime;


/**
 * Encapsulates information about an site map entry in the index document.
 * 
 * @see SiteMapIndexData
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteMapIndexElementData {

    @XmlElement(name = "loc", required = true)
    private String location;
    @XmlElement(name = "lastmod")
    private DateTime lastModified;


    /**
     * Constructs new empty index element.
     */
    public SiteMapIndexElementData() {
        // intentional empty constructor
    }

    /**
     * Constructs new index element with given {@code location} and {@code lastModified} parameters.
     * 
     * @param location
     *            the location of the site map
     * @param lastModified
     *            the time that the corresponding site map file was modified
     */
    public SiteMapIndexElementData(final String location, final DateTime lastModified) {
        this.location = location;
        this.lastModified = lastModified;
    }

    /**
     * Returns the location of the site map.
     * 
     * @return the URL of the site map
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location of the site map.
     * 
     * @param location
     *            the URL to set
     */
    public void setLocation(final String location) {
        this.location = location;
    }

    /**
     * Returns the time that the corresponding site map file was modified.
     * 
     * @return the last modification timestamp
     */
    public DateTime getLastModified() {
        return lastModified;
    }

    /**
     * Sets the time that the corresponding site map file was modified
     * 
     * @param lastModified
     *            the last modification timestamp to set
     */
    public void setLastModified(final DateTime lastModified) {
        this.lastModified = lastModified;
    }
}
