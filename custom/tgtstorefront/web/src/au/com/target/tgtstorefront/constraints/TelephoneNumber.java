/**
 * 
 */
package au.com.target.tgtstorefront.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import au.com.target.tgtstorefront.constraints.impl.TelephoneNumberImpl;


/**
 * @author rmcalave
 * 
 */
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TelephoneNumberImpl.class)
@Documented
public @interface TelephoneNumber {
    String message() default "{phoneNumber.invalid}";

    int minDigits() default 0;

    int maxDigits() default Integer.MAX_VALUE;

    Class<? extends Payload>[] payload() default {};

    Class<?>[] groups() default {};
}
