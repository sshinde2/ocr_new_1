/**
 *
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.payment.AdapterException;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemTierData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.exceptions.BadFundingMethodException;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.controllers.util.TargetPaymentHelper;
import au.com.target.tgtstorefront.controllers.util.ThreePartDateHelper;
import au.com.target.tgtstorefront.forms.BillingAddressForm;
import au.com.target.tgtstorefront.forms.checkout.AbstractPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.BillingAddressOnlyForm;
import au.com.target.tgtstorefront.forms.checkout.ExistingCardPaymentForm;
import au.com.target.tgtstorefront.forms.checkout.FlybuysLoginForm;
import au.com.target.tgtstorefront.forms.checkout.FlybuysRedeemForm;
import au.com.target.tgtstorefront.forms.checkout.NewCardPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.VendorPaymentForm;
import au.com.target.tgtstorefront.forms.validation.validator.BillingAddressStateValidator;
import au.com.target.tgtstorefront.forms.validation.validator.PaymentDetailsValidator;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.PAYMENT)
@RequireHardLogIn
public class CheckoutPaymentController extends AbstractCheckoutController {

    protected static final Logger LOG = Logger.getLogger(CheckoutPaymentController.class);

    private static final String FLYBUYS_REMOVE_DISCOUNT_CODE = "remove";

    private static final String MULTIPLE_PAYMENT_MODE = "multiplePaymentMode";

    @Autowired
    private PaymentDetailsValidator paymentDetailsValidator;
    @Autowired
    private BillingAddressStateValidator billingAddressStateValidator;
    @Autowired
    private TargetPaymentHelper targetPaymentHelper;

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;


    @ModelAttribute("flybuysBirthYears")
    public List<SelectOption> getFlybuysBirthYears() {
        return WebConstants.Payment.FLYBUYS_BIRTH_YEARS;
    }

    @ModelAttribute("flybuysRedeemConfigData")
    public FlybuysRedeemConfigData getFlybuysRedeemConfigData() {
        return getFlybuysDiscountFacade().getFlybuysRedeemConfigData();
    }

    /**
     * 
     * @return List<SelectOption>
     */
    @ModelAttribute("statesWithNotApplicable")
    public List<SelectOption> getStatesWithNotApplicable() {
        final List<SelectOption> statesWithNotApplicable = getStates();
        if (statesWithNotApplicable != null) {
            statesWithNotApplicable.add(0, new SelectOption(WebConstants.STATE_CODE_NOT_APPLICABLE,
                    WebConstants.STATE_NAME_NOT_APPLICABLE));

        }
        return statesWithNotApplicable;
    }

    @Override
    protected String getRedirectionAndCheckSOH(final Model model, final RedirectAttributes redirectAttributes,
            final String url, final TargetCartData cartData) {
        final String redirect = super.getRedirectionAndCheckSOH(model, redirectAttributes, url, cartData);
        if (StringUtils.isNotBlank(redirect)) {
            return redirect;
        }

        if (getCheckoutFacade().hasIncompleteDeliveryDetails()) {
            return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
        }
        return getRedirectionIfFlybuysReassessed(redirectAttributes);
    }




    /**
     * 
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET)
    public String doChoosePaymentMethod(final Model model, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        final boolean isIPGEnabled = targetPaymentHelper.checkIfIPGFeatureIsEnabled();
        final boolean isGiftCardEnabled = targetPaymentHelper.isGiftCardEnable();
        final SalesApplication currentSalesApplication = getSalesApplicationFacade().getCurrentSalesApplication();
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        getCheckoutFacade().validateDeliveryMode();
        getCheckoutFacade().applyUsersFlybuysNumberToCart();
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        NewCardPaymentDetailsForm newCardPaymentDetailsForm = null;
        ExistingCardPaymentForm existingCardPaymentForm = null;
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }
        if (SalesApplication.KIOSK.equals(currentSalesApplication)) {
            model.addAttribute(MULTIPLE_PAYMENT_MODE, Boolean.FALSE);
            populateBillingAddressOnlyFormForPinPad(model, cartData);
        }
        else {
            model.addAttribute(MULTIPLE_PAYMENT_MODE, Boolean.TRUE);
            if (isIPGEnabled) {
                populateBillingAddressOnlyForm(model, cartData, true, null);
                if (isGiftCardEnabled) {
                    model.addAttribute("isGiftCardPaymentAllowedForCart",
                            Boolean.valueOf(getCheckoutFacade().isGiftCardPaymentAllowedForCart()));
                    populateGiftCardBillingAddressOnlyForm(model, cartData, true, null);
                }
            }
            else {
                final String tnsSessionToken = targetPaymentHelper.getTNSSessionToken(model);
                newCardPaymentDetailsForm = new NewCardPaymentDetailsForm(
                        targetPaymentHelper.getTnsHostedPaymentFormBaseUrl() + tnsSessionToken);
                newCardPaymentDetailsForm.setPaymentSessionId(tnsSessionToken);
                targetPaymentHelper.populatePaymentDefault(model, ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_TNS_RETURN);
                existingCardPaymentForm = new ExistingCardPaymentForm(
                        ControllerConstants.PAYMENT
                                + ControllerConstants.PAYMENT_EXISTING);
            }
            targetPaymentHelper.populatePaymentDefaultForms(model, existingCardPaymentForm, new VendorPaymentForm(
                    ControllerConstants.PAYMENT
                            + ControllerConstants.PAYMENT_PAYPAL),
                    newCardPaymentDetailsForm, cartData, null);
            populateInfoMessage(cartData, model);
        }

        populateVoucherMessagesIfValid(model);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        targetPaymentHelper.populatePaymentCart(model, cartData);
        populatePaymentCMSPage(model);

        // reverse existing gift card payments (if any) before selecting a payment method
        if (isIpgPayment(cartData.getPaymentInfo())) {
            reverseExistingGiftCardPayments(cartData);
        }
        return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
    }

    /**
     * @param model
     * @param cartData
     */
    private void populateBillingAddressOnlyFormForPinPad(final Model model, final TargetCartData cartData) {
        BillingAddressOnlyForm billingAddressOnlyForm = null;
        billingAddressOnlyForm = new BillingAddressOnlyForm(ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_BILLING, null);
        billingAddressOnlyForm.setActive(true);
        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);
        model.addAttribute("billingAddressOnlyForm", billingAddressOnlyForm);
    }

    /**
     * @param model
     * @param cartData
     * @param isActive
     *            try to get the active status of the form from cartModel.
     */
    private void populateBillingAddressOnlyForm(final Model model, final TargetCartData cartData,
            final boolean isActive, final AbstractPaymentDetailsForm sourceForm) {
        BillingAddressOnlyForm billingAddressOnlyForm = null;
        if (isActive) {
            billingAddressOnlyForm = new BillingAddressOnlyForm(ControllerConstants.PAYMENT
                    + ControllerConstants.PAYMENT_BILLING, TgtFacadesConstants.CREDIT_CARD);
        }
        else {
            billingAddressOnlyForm = new BillingAddressOnlyForm(ControllerConstants.PAYMENT
                    + ControllerConstants.PAYMENT_BILLING, null);
        }
        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, sourceForm);
        model.addAttribute("billingAddressOnlyForm", billingAddressOnlyForm);
    }

    /**
     * @param model
     * @param cartData
     * @param isActive
     *            try to get the active status of the form from cartModel.
     */
    private void populateGiftCardBillingAddressOnlyForm(final Model model, final TargetCartData cartData,
            final boolean isActive, final AbstractPaymentDetailsForm sourceForm) {
        BillingAddressOnlyForm billingAddressOnlyForm = null;
        if (isActive) {
            billingAddressOnlyForm = new BillingAddressOnlyForm(
                    ControllerConstants.PAYMENT
                            + ControllerConstants.PAYMENT_GIFT_CARD_BILLING,
                    TgtFacadesConstants.GIFT_CARD);
        }
        else {
            billingAddressOnlyForm = new BillingAddressOnlyForm(
                    ControllerConstants.PAYMENT
                            + ControllerConstants.PAYMENT_GIFT_CARD_BILLING,
                    null);
        }
        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, sourceForm);
        model.addAttribute("giftCardBillingAddressOnlyForm", billingAddressOnlyForm);
    }


    /**
     * 
     * @param request
     * @param model
     * @param form
     * @param result
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_TNS_RETURN, method = RequestMethod.POST)
    public String tnsPost(final HttpServletRequest request, final Model model,
            final RedirectAttributes redirectAttributes,
            @Valid final NewCardPaymentDetailsForm form, final BindingResult result)
                    throws CMSItemNotFoundException {
        paymentDetailsValidator.validate(form, result);
        billingAddressStateValidator.validate(form.getBillingAddress(), result);
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        if (result.hasErrors()) {

            return tnsPaymentErrorPopulator(model, form, cartData);
        }
        final String redirection = applyTMDIfItNotApplied(redirectAttributes, cartData, form.getTeamMemberCode());
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        final CCPaymentInfoData paymentInfoData = targetPaymentHelper.createCCPaymentInfoData(form);
        TargetAddressData addressData = null;

        final BillingAddressForm addressForm = form.getBillingAddress();
        addressData = targetPaymentHelper.createTargetAddressData(addressForm);

        paymentInfoData.setBillingAddress(addressData);
        getCheckoutFacade().setFlybuysNumber(form.getFlyBuys());
        final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(paymentInfoData);
        if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getSubscriptionId())) {
            getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
        }
        else {
            return tnsPaymentErrorPopulator(model, form, (TargetCartData)getCheckoutFacade().getCheckoutCart());
        }
        getCheckoutFacade().setPaymentMode(TgtFacadesConstants.CREDIT_CARD);
        model.addAttribute("paymentId", newPaymentSubscription.getId());
        return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
    }

    @RequestMapping(value = { ControllerConstants.PAYMENT_BILLING,
            ControllerConstants.PAYMENT_TNS_RETURN, ControllerConstants.PAYMENT_PAYPAL,
            ControllerConstants.PAYMENT_EXISTING }, method = RequestMethod.GET)
    public String get() {
        return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
    }

    @SuppressWarnings("boxing")
    @RequestMapping(value = ControllerConstants.PAYMENT_BILLING, method = RequestMethod.POST)
    public String billingPost(final Model model, final RedirectAttributes redirectAttributes,
            @Valid final BillingAddressOnlyForm form, final BindingResult result)
                    throws CMSItemNotFoundException {
        final boolean isIPGEnabled = targetPaymentHelper.checkIfIPGFeatureIsEnabled();
        final SalesApplication currentSalesApplication = getSalesApplicationFacade().getCurrentSalesApplication();
        final boolean isKiosk = SalesApplication.KIOSK.equals(currentSalesApplication);
        billingAddressStateValidator.validate(form.getBillingAddress(), result);
        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        if (result.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "checkout.error.paymentmethod.formentry.invalid");
            return handleBillingPostError(model, form, isIPGEnabled, isKiosk, cartData);
        }
        final String redirection = applyTMDIfItNotApplied(redirectAttributes, cartData, form.getTeamMemberCode());
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        getCheckoutFacade().setFlybuysNumber(form.getFlyBuys());

        final TargetAddressData targetBillingAddressData = targetPaymentHelper.createTargetAddressData(form
                .getBillingAddress());
        if (isKiosk) {
            getCheckoutFacade().createPinPadPaymentInfo(targetBillingAddressData);
            getCheckoutFacade().setPaymentMode(TgtFacadesConstants.PINPAD);
        }
        else {
            if (TgtFacadesConstants.GIFT_CARD.equalsIgnoreCase(form.getCardType())) {
                getCheckoutFacade().createIpgPaymentInfo(targetBillingAddressData,
                        IpgPaymentTemplateType.GIFTCARDMULTIPLE);
                getCheckoutFacade().setPaymentMode(TgtFacadesConstants.GIFT_CARD);
            }
            else if (TgtFacadesConstants.CREDIT_CARD.equalsIgnoreCase(form.getCardType())) {
                getCheckoutFacade().createIpgPaymentInfo(targetBillingAddressData,
                        IpgPaymentTemplateType.CREDITCARDSINGLE);
                getCheckoutFacade().setPaymentMode(TgtFacadesConstants.IPG);
            }
        }

        return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
    }

    @RequestMapping(value = ControllerConstants.PAYMENT_GIFT_CARD_BILLING, method = RequestMethod.POST)
    public String giftCardBillingPost(
            final Model model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("giftCardBillingAddressOnlyForm") final BillingAddressOnlyForm form,
            final BindingResult result)
                    throws CMSItemNotFoundException {
        return billingPost(model, redirectAttributes, form, result);
    }

    @RequestMapping(value = ControllerConstants.PAYMENT_PAYPAL, method = RequestMethod.POST)
    public String vendorPost(final HttpServletRequest request, final Model model,
            @Valid final VendorPaymentForm vendorPaymentForm, final BindingResult result,
            final RedirectAttributes redirectAttributes)
                    throws CMSItemNotFoundException {

        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);

        if (result.hasErrors()) {

            GlobalMessages.addErrorMessage(model, "checkout.error.payment.paypal.errors");

            vendorPaymentForm.setActive(true);
            vendorPaymentForm.setEnabled(true);
            final String tnsSessionToken = targetPaymentHelper.getTNSSessionToken(model);

            targetPaymentHelper.populatePaymentDefault(model, ControllerConstants.PAYMENT
                    + ControllerConstants.PAYMENT_TNS_RETURN);
            targetPaymentHelper.populatePaymentCart(model, cartData);
            populatePaymentCMSPage(model);
            final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm(
                    targetPaymentHelper.getTnsHostedPaymentFormBaseUrl() + tnsSessionToken);
            populateVoucherMessagesIfValid(model);
            populateFlybuysRedeemTiers(model);
            populateFlybuysDenialData(model);
            targetPaymentHelper.populatePaymentDefaultForms(model, new ExistingCardPaymentForm(
                    ControllerConstants.PAYMENT
                            + ControllerConstants.PAYMENT_EXISTING),
                    null,
                    newCardPaymentDetailsForm, cartData, vendorPaymentForm);

            if (targetPaymentHelper.checkIfIPGFeatureIsEnabled()) {
                populateBillingAddressOnlyForm(model, cartData, false, vendorPaymentForm);
                if (targetPaymentHelper.isGiftCardEnable()) {
                    populateGiftCardBillingAddressOnlyForm(model, cartData, false, vendorPaymentForm);
                }
            }
            model.addAttribute(MULTIPLE_PAYMENT_MODE, Boolean.TRUE);
            return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
        }

        // SOH check for paypal specifically
        String redirection = super.getRedirectionAndCheckSOH(model, redirectAttributes,
                ControllerConstants.Redirection.CHECKOUT_PAYMENT, cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        redirection = applyTMDIfItNotApplied(redirectAttributes, cartData, vendorPaymentForm.getTeamMemberCode());
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }

        getCheckoutFacade().setFlybuysNumber(vendorPaymentForm.getFlyBuys());

        final String payPalReturnUrl = targetPaymentHelper.getBaseSecureUrl().concat(ControllerConstants.PAYMENT)
                .concat(
                        ControllerConstants.PAYMENT_PAYPAL_RETURN);
        final String payPalCancelUrl = targetPaymentHelper.getBaseSecureUrl().concat(ControllerConstants.PAYMENT);

        String payPalSessionToken = StringUtils.EMPTY;
        try {
            payPalSessionToken = getCheckoutFacade().getPayPalSessionToken(payPalReturnUrl, payPalCancelUrl)
                    .getRequestToken();
            //set the token in session to validate on return
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PAYPAL_EC_TOKEN, payPalSessionToken);
        }
        catch (final AdapterException adapterException) {
            LOG.error("Not able to connect with PayPal" + new Date());
        }
        getCheckoutFacade().setPaymentMode(TgtFacadesConstants.PAYPAL);
        getCheckoutFacade().removePaymentInfo();
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX.concat(targetPaymentHelper.getPayPalHostedPaymentFormBaseUrl())
                .concat(
                        payPalSessionToken);
    }

    private void orderDeliveryAddressLog(final CartData order) {
        if (order != null) {
            final AddressData address = order.getDeliveryAddress();
            if (address != null) {
                LOG.info(MessageFormat.format("=====Delivery Address for Order {0} is {1} {2},{3},{4}=====",
                        order.getCode(), address.getLine1(), address.getTown(), address.getPostalCode(),
                        address.getCountry().getName()));
            }
        }
    }

    @RequestMapping(value = ControllerConstants.PAYMENT_PAYPAL_RETURN, method = RequestMethod.GET)
    public String vendorReturn(final Model model,
            @RequestParam(ControllerConstants.REQUEST_PARAM_PAYPAL_TOKEN) final String token,
            @RequestParam(ControllerConstants.REQUEST_PARAM_PAYPAL_PAYER_ID) final String payerId,
            final HttpServletRequest request, final RedirectAttributes redirectModel)
                    throws CMSItemNotFoundException {
        try {
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                    Boolean.TRUE);

            TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);

            if (!token.equals(request.getSession().getAttribute(ControllerConstants.SESSION_PARAM_PAYPAL_EC_TOKEN))) {
                // Token mismatch treat as payment failure - TODO check this
                GlobalMessages.addErrorMessage(model, "payment.error.paypal");
                setupFormData(model);
                return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
            }


            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PAYPAL_EC_TOKEN);

            getCheckoutFacade().validateDeliveryMode();
            String redirection = getRedirectionAndCheckSOH(model, redirectModel, DEFAULT_SOH_REDIRECT_URL,
                    cartData);
            if (StringUtils.isNotEmpty(redirection)) {
                return redirection;
            }
            if (cartData.isDataStale()) {
                cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            }
            orderDeliveryAddressLog(getCheckoutFacade().getCheckoutCart());
            getCheckoutFacade().createPayPalPaymentInfo(token, payerId);

            // Set the IP address and browser cookies in checkout cart for use by fraud check when the order is created
            getTargetPlaceOrderFacade().setClientDetailsForFraudDetection(
                    getRequestHeadersHelper().getRemoteIPAddress(request),
                    getCookieStringBuilder().buildCookieString(request.getCookies()));

            TargetPlaceOrderResult placeOrderResult = null;
            try {
                placeOrderResult = getTargetPlaceOrderFacade().placeOrder();
            }
            catch (final BadFundingMethodException bfme) {
                //set the token in session to validate on return
                request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PAYPAL_EC_TOKEN, token);
                return UrlBasedViewResolver.REDIRECT_URL_PREFIX.concat(
                        targetPaymentHelper.getPayPalHostedPaymentFormBaseUrl())
                        .concat(token);
            }

            if (placeOrderResult != null) {

                if (TargetPlaceOrderResultEnum.SUCCESS.equals(placeOrderResult.getPlaceOrderResult())) {
                    final String orderCode = placeOrderResult.getAbstractOrderModel().getCode();
                    request.getSession().setAttribute(
                            ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, orderCode);
                    return ControllerConstants.Redirection.CHECKOUT_THANK_YOU.concat(orderCode);
                }

                // Cart validation failure or payment failure
                if (TargetPlaceOrderResultEnum.PAYMENT_FAILURE.equals(placeOrderResult.getPlaceOrderResult())
                        || TargetPlaceOrderResultEnum.USER_MISMATCH.equals(placeOrderResult.getPlaceOrderResult())
                        || TargetPlaceOrderResultEnum.INVALID_CART.equals(placeOrderResult.getPlaceOrderResult())) {
                    setupFormData(model);
                    return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
                }

                // Case of SOH check failed 
                if (TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK.equals(placeOrderResult.getPlaceOrderResult())) {

                    final TargetCartData cart = setupFormData(model);
                    redirection = super.getRedirectionAndCheckSOH(model, redirectModel,
                            ControllerConstants.Redirection.CHECKOUT_PAYMENT, cart);
                    if (StringUtils.isNotEmpty(redirection)) {
                        return redirection;
                    }
                }

            }

            // Handle all other cases including UNKNOWN as order creation failed
            final TargetPlaceOrderResultEnum placeOrderResultEnum = null == placeOrderResult ? null
                    : placeOrderResult.getPlaceOrderResult();
            return handleOrderCreationFailed(placeOrderResultEnum, cartData.getCode());
        }
        finally {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        }
    }


    /**
     * Set up data for forms if payment failed or SOH check failed
     * 
     * @param model
     * @return TargetCartData
     * @throws CMSItemNotFoundException
     */
    private TargetCartData setupFormData(final Model model) throws CMSItemNotFoundException {

        String tnsSessionToken = targetPaymentHelper.getTNSSessionToken(model);
        try {
            tnsSessionToken = getCheckoutFacade().getTNSSessionToken().getRequestToken();
        }
        catch (final AdapterException adapterException) {
            targetPaymentHelper.populateTnsErrorMessage(model);
        }
        final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm(
                targetPaymentHelper.getTnsHostedPaymentFormBaseUrl() + tnsSessionToken);
        newCardPaymentDetailsForm.setPaymentSessionId(tnsSessionToken);
        final VendorPaymentForm vendorPaymentForm = new VendorPaymentForm(ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_PAYPAL);
        vendorPaymentForm.setActive(true);
        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        targetPaymentHelper.populatePaymentDefault(model, ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_TNS_RETURN);
        targetPaymentHelper.populatePaymentCart(model, cartData);
        populatePaymentCMSPage(model);
        populateVoucherMessagesIfValid(model);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        targetPaymentHelper.populatePaymentDefaultForms(model, new ExistingCardPaymentForm(ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_EXISTING), vendorPaymentForm,
                newCardPaymentDetailsForm, cartData, vendorPaymentForm);
        model.addAttribute(MULTIPLE_PAYMENT_MODE, Boolean.TRUE);
        return cartData;
    }


    @RequestMapping(value = ControllerConstants.PAYMENT_EXISTING, method = RequestMethod.GET)
    public String existingCardGet() {
        return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
    }

    @RequestMapping(value = ControllerConstants.PAYMENT_EXISTING, method = RequestMethod.POST)
    public String existingCardPost(final Model model, final RedirectAttributes redirectAttributes,
            @Valid final ExistingCardPaymentForm existingCardPaymentForm, final BindingResult result)
                    throws CMSItemNotFoundException {

        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        if (result.hasErrors()) {
            existingCardPaymentForm.setActive(true);
            final String tnsSessionToken = targetPaymentHelper.getTNSSessionToken(model);
            GlobalMessages.addErrorMessage(model, "checkout.error.paymentmethod.formentry.invalid");
            targetPaymentHelper.populatePaymentDefault(model, ControllerConstants.PAYMENT
                    + ControllerConstants.PAYMENT_TNS_RETURN);
            targetPaymentHelper.populatePaymentCart(model, cartData);
            populatePaymentCMSPage(model);
            final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm(
                    targetPaymentHelper.getTnsHostedPaymentFormBaseUrl() + tnsSessionToken);
            newCardPaymentDetailsForm.setPaymentSessionId(tnsSessionToken);
            populateVoucherMessagesIfValid(model);
            populateFlybuysRedeemTiers(model);
            populateFlybuysDenialData(model);
            targetPaymentHelper.populatePaymentDefaultForms(model, null, new VendorPaymentForm(
                    ControllerConstants.PAYMENT
                            + ControllerConstants.PAYMENT_PAYPAL),
                    newCardPaymentDetailsForm, cartData, existingCardPaymentForm);
            return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
        }

        final String redirection = applyTMDIfItNotApplied(redirectAttributes, cartData,
                existingCardPaymentForm.getTeamMemberCode());
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        getCheckoutFacade().setFlybuysNumber(existingCardPaymentForm.getFlyBuys());
        getCheckoutFacade().setPaymentDetails(existingCardPaymentForm.getCardId());
        getCheckoutFacade().setPaymentMode(TgtFacadesConstants.CREDIT_CARD);
        return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
    }

    @Override
    @RequestMapping(value = ControllerConstants.PAYMENT_VALIDATE_TMD, method = RequestMethod.GET)
    public String validateTMD(final Model model,
            @RequestParam(value = "num", required = false) final String tmdCardNumber) throws CMSItemNotFoundException {
        final String returnValue = super.validateTMD(model, tmdCardNumber);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        return returnValue;
    }

    /**
     * @param model
     * @param voucher
     * @return viewName
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_VALIDATE_VOUCHER, method = RequestMethod.GET)
    public String validateVoucher(final Model model,
            @RequestParam(value = "code", required = false) final String voucher) throws CMSItemNotFoundException {

        boolean isVoucherValid = false;
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            isVoucherValid = (voucher != null
                    && TargetValidationCommon.Voucher.PATTERN.matcher(voucher).matches()
                    && redeemVoucherIfItNotRedeemed(voucher));
            final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            model.addAttribute("cartData", cartData);
        }

        model.addAttribute("isVoucherValid", Boolean.valueOf(isVoucherValid));
        if (isVoucherValid) {
            populateValidVoucherMessages(model, voucher);
        }
        else {
            model.addAttribute("errorMsg", getMessageSource()
                    .getMessage("payment.voucherCode.invalid", null, getI18nService().getCurrentLocale()));
        }

        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        return ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_VOUCHER_PAGE;
    }

    /**
     * @param model
     * @return viewName
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_REMOVE_VOUCHER, method = RequestMethod.GET)
    public String removeVoucher(final Model model) throws CMSItemNotFoundException {
        final String voucher = getCheckoutFacade().getFirstAppliedVoucher();
        if (voucher != null) {
            getCheckoutFacade().removeVoucher(voucher);
        }
        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        model.addAttribute("cartData", cartData);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        return ControllerConstants.Views.Pages.MultiStepCheckout.REMOVE_VOUCHER_PAGE;
    }

    private boolean redeemVoucherIfItNotRedeemed(final String voucher) {
        if (StringUtils.isNotBlank(voucher)) {
            final String appliedVoucher = getCheckoutFacade().getFirstAppliedVoucher();
            if (voucher.equals(appliedVoucher)) {
                return true;
            }
            else if (appliedVoucher == null) {
                return getCheckoutFacade().applyVoucher(voucher);
            }
        }
        return false;
    }

    /*
     * Used when returning to this page when preivously had a voucher
     */
    private void populateVoucherMessagesIfValid(final Model model) {
        final String appliedVoucher = getCheckoutFacade().getFirstAppliedVoucher();
        if (appliedVoucher != null) {
            populateValidVoucherMessages(model, appliedVoucher);
        }
    }

    private void populateValidVoucherMessages(final Model model, final String voucher) {
        model.addAttribute("appliedVoucher", voucher);
        String successMsg = getMessageSource()
                .getMessage("payment.voucherCode.applied", new Object[] { voucher },
                        getI18nService().getCurrentLocale());
        final PriceData voucherPriceData = getCheckoutFacade().getAppliedVoucherPrice(voucher);
        if (voucherPriceData != null) {
            successMsg += " " + getMessageSource().getMessage("payment.voucherCode.savings",
                    new Object[] { voucherPriceData.getFormattedValue() },
                    getI18nService().getCurrentLocale());
        }
        model.addAttribute("voucherSuccessMsg", successMsg);
    }

    /*
     * Loads the flybuys login form
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_FLYBUYS_VALIDATE, method = RequestMethod.GET)
    public String validateFlybuysNumber(final Model model,
            @RequestParam(value = "cardNumber", required = false) final String cardNumber) {

        if (getCheckoutFacade().isFlybuysDiscountAlreadyApplied(cardNumber)) {
            model.addAttribute("flybuysalreadyapplied", Boolean.TRUE);
        }
        else {
            final boolean validCardNumber = StringUtils.isNotBlank(cardNumber)
                    && getCheckoutFacade().setFlybuysNumber(cardNumber);
            model.addAttribute("success", Boolean.valueOf(validCardNumber));
        }
        return ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_VALIDATE_RESULT;
    }

    /*
     * Loads the flybuys login form
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_FLYBUYS_LOGIN, method = RequestMethod.GET)
    public String authenticateFlybuysForm(final Model model) {
        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        final FlybuysLoginForm flybuysLoginForm = new FlybuysLoginForm();
        flybuysLoginForm.setCardNumber(cartData.getFlybuysNumber());
        model.addAttribute("flybuysLoginForm", flybuysLoginForm);
        return ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_LOGIN_FORM;
    }

    /*
     * Flybuys Form submission
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_FLYBUYS_LOGIN, method = RequestMethod.POST)
    public String authenticateFlybuysSubmit(final Model model, @Valid final FlybuysLoginForm flybuysLoginForm,
            final BindingResult result) {

        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        flybuysLoginForm.setCardNumber(cartData.getFlybuysNumber());
        model.addAttribute("cartData", cartData);
        model.addAttribute("flybuysLoginForm", flybuysLoginForm);

        boolean success = true;

        if (result.hasErrors()) {

            success = false;
        }
        else {

            final Date dateOfBirth = ThreePartDateHelper.convertToDate(flybuysLoginForm.getBirthDay(),
                    flybuysLoginForm.getBirthMonth(),
                    flybuysLoginForm.getBirthYear());

            final FlybuysRequestStatusData statusData = getFlybuysDiscountFacade().getFlybuysRedemptionData(

            cartData.getFlybuysNumber(), dateOfBirth, flybuysLoginForm.getPostCode());
            final FlybuysResponseType flybuysRequestStatus = statusData.getResponse();


            if (flybuysRequestStatus != FlybuysResponseType.SUCCESS) {
                statusData.setErrorMessage(getMessageSource().getMessage("checkout.error.flybuys."
                        + flybuysRequestStatus.getMessageKey(), null,
                        getI18nService().getCurrentLocale()));
                success = false;
            }

            model.addAttribute("flybuysRequestStatusData", statusData);
        }

        model.addAttribute("success", Boolean.valueOf(success));
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);

        return ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_LOGIN_FORM_RESULT;
    }

    /*
     * Loading the redeem lists from session.. Already authenticated.
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_FLYBUYS_SELECT_OPTION, method = RequestMethod.GET)
    public String selectFlybuysReedemTierOptionList(final Model model) {
        final FlybuysRedeemTierData appliedFlybuysRedeemTier = getFlybuysDiscountFacade()
                .getAppliedFlybuysRedeemTierForCheckoutCart();

        final FlybuysRedeemForm flybuysRedeemForm = new FlybuysRedeemForm();

        if (appliedFlybuysRedeemTier != null) {
            flybuysRedeemForm.setRedeemCode(appliedFlybuysRedeemTier.getRedeemCode());
        }

        model.addAttribute("flybuysRedeemForm", flybuysRedeemForm);

        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        model.addAttribute("cartData", cartData);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        return ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION;
    }

    /*
     * Adds the selected redeem option tier to the cart
     */
    @RequestMapping(value = ControllerConstants.PAYMENT_FLYBUYS_SELECT_OPTION, method = RequestMethod.POST)
    public String applyFlybuysReedemTierOption(final Model model, @Valid final FlybuysRedeemForm flybuysRedeemForm,
            final BindingResult result) {
        model.addAttribute("flybuysRedeemForm", flybuysRedeemForm);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);

        if (result.hasErrors()) {
            model.addAttribute("success", Boolean.FALSE);
            return ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT;
        }

        boolean success = false;

        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            if (FLYBUYS_REMOVE_DISCOUNT_CODE.equals(flybuysRedeemForm.getRedeemCode())) {
                success = getFlybuysDiscountFacade().removeFlybuysDiscountFromCheckoutCart();
            }
            else {
                success = getFlybuysDiscountFacade().applyFlybuysDiscountToCheckoutCart(
                        flybuysRedeemForm.getRedeemCode());
            }
            final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            model.addAttribute("cartData", cartData);
        }

        model.addAttribute("success", Boolean.valueOf(success));
        if (!success) {
            // Adding an error against redeemCode as that's the only field on the form
            result.rejectValue("redeemCode", "payment.flybuys.redeem.label.voucher");
        }

        return ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT;
    }

    private void populateFlybuysRedeemTiers(final Model model) {
        final FlybuysRedemptionData redeemOptions = getFlybuysDiscountFacade()
                .getFlybuysRedemptionDataForCheckoutCart();
        model.addAttribute("redeemOptions", redeemOptions);
    }

    /**
     * Populate flybuys denial data.
     * 
     * @param model
     *            the model
     */
    private void populateFlybuysDenialData(final Model model) {
        final FlybuysDiscountDenialDto denialData = getFlybuysDiscountFacade().getFlybuysDiscountAllowed();
        if (denialData != null) {
            model.addAttribute("flybuysState", denialData);
        }
    }

    private String tnsPaymentErrorPopulator(final Model model, final NewCardPaymentDetailsForm form,
            final TargetCartData cartData) throws CMSItemNotFoundException {
        form.setActive(true);
        GlobalMessages.addErrorMessage(model, "checkout.error.paymentmethod.formentry.invalid");
        GlobalMessages.addInfoMessage(model, "checkout.information.paymentmethod.resupply.credit.card.detail");
        final String tnsSessionToken = targetPaymentHelper.getTNSSessionToken(model);
        form.setPaymentSessionId(tnsSessionToken);
        form.setGatewayCardSecurityCode(StringUtils.EMPTY);
        form.setGatewayCardNumber(StringUtils.EMPTY);
        form.setAction(targetPaymentHelper.getTnsHostedPaymentFormBaseUrl() + tnsSessionToken);
        targetPaymentHelper.populatePaymentDefault(model, ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_TNS_RETURN);
        targetPaymentHelper.populatePaymentCart(model, cartData);
        populatePaymentCMSPage(model);
        populateVoucherMessagesIfValid(model);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        targetPaymentHelper.populatePaymentDefaultForms(model, new ExistingCardPaymentForm(ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_EXISTING), new VendorPaymentForm(ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_PAYPAL),
                null, cartData, form);
        model.addAttribute(MULTIPLE_PAYMENT_MODE, Boolean.TRUE);
        return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
    }

    private void populatePaymentCMSPage(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_NEW_PAYMENT_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_NEW_PAYMENT_CMS_PAGE_LABEL));
    }

    /**
     * 
     * @param cartData
     * @param model
     */
    private void populateInfoMessage(final TargetCartData cartData, final Model model) {
        final TargetCCPaymentInfoData targetCCPaymentInfoData = (TargetCCPaymentInfoData)cartData.getPaymentInfo();
        if (null != targetCCPaymentInfoData && !targetCCPaymentInfoData.isPaymentSucceeded()
                && !isIpgPayment(targetCCPaymentInfoData)) {
            GlobalMessages.addInfoMessage(model, "checkout.information.paymentmethod.resupply.credit.card.detail");
        }
    }

    /**
     * Handle error for kiosk/ipg post payment and return to payment page
     * 
     * @param model
     * @param form
     * @param isIPGEnabled
     * @param isKiosk
     * @param cartData
     * @return forward view name
     * @throws CMSItemNotFoundException
     */
    @SuppressWarnings("boxing")
    private String handleBillingPostError(final Model model, final BillingAddressOnlyForm form,
            final boolean isIPGEnabled, final boolean isKiosk, final TargetCartData cartData)
                    throws CMSItemNotFoundException {
        if (TgtFacadesConstants.GIFT_CARD.equalsIgnoreCase(form.getCardType())) {
            populateBillingAddressOnlyForm(model, cartData, false, form);
        }
        else if (TgtFacadesConstants.CREDIT_CARD.equalsIgnoreCase(form.getCardType())) {
            populateGiftCardBillingAddressOnlyForm(model, cartData, false, form);
        }
        form.setActive(true);
        targetPaymentHelper.populatePaymentCart(model, cartData);
        populatePaymentCMSPage(model);
        populateVoucherMessagesIfValid(model);
        populateFlybuysRedeemTiers(model);
        populateFlybuysDenialData(model);
        if (!isKiosk && isIPGEnabled) {
            targetPaymentHelper.populatePaymentDefaultForms(model, new ExistingCardPaymentForm(
                    ControllerConstants.PAYMENT
                            + ControllerConstants.PAYMENT_EXISTING),
                    new VendorPaymentForm(
                            ControllerConstants.PAYMENT
                                    + ControllerConstants.PAYMENT_PAYPAL),
                    null, cartData, form);
            populateInfoMessage(cartData, model);
        }
        model.addAttribute(MULTIPLE_PAYMENT_MODE, !isKiosk);
        return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE;
    }


}
