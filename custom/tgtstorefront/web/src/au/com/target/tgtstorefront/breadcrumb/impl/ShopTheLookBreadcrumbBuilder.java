/**
 * 
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;


/**
 * @author bhuang3
 *
 */
public class ShopTheLookBreadcrumbBuilder {

    private static final String LAST_LINK_CLASS = "active";

    private UrlResolver<TargetShopTheLookModel> targetShopTheLookUrlResolver;

    private UrlResolver<CategoryModel> targetCategoryModelUrlResolver;

    public List<Breadcrumb> getBreadcrumbs(final TargetShopTheLookModel targetShopTheLookModel)
    {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb last = getShopTheLookBreadcrumb(targetShopTheLookModel);
        last.setLinkClass(LAST_LINK_CLASS);
        breadcrumbs.add(last);
        breadcrumbs.add(getCategoryBreadcrumb(targetShopTheLookModel.getCategory()));
        Collections.reverse(breadcrumbs);
        return breadcrumbs;
    }

    public List<Breadcrumb> getBreadcrumbsForLookPage(final TargetLookModel targetLookModel)
    {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb last = getLookBreadcrumb(targetLookModel);
        last.setLinkClass(LAST_LINK_CLASS);
        breadcrumbs.add(last);
        final TargetShopTheLookModel shopTheLook = getShopTheLookModel(targetLookModel);
        if (shopTheLook != null) {
            breadcrumbs.add(getShopTheLookBreadcrumb(shopTheLook));
            breadcrumbs.add(getCategoryBreadcrumb(shopTheLook.getCategory()));
        }
        Collections.reverse(breadcrumbs);
        return breadcrumbs;
    }

    private TargetShopTheLookModel getShopTheLookModel(final TargetLookModel targetLookModel) {
        TargetShopTheLookModel shopTheLook = null;
        if (targetLookModel.getCollection() != null) {
            shopTheLook = targetLookModel.getCollection().getShopTheLook();
        }
        return shopTheLook;
    }

    protected Breadcrumb getLookBreadcrumb(final TargetLookModel targetLookModel)
    {
        final String lookUrl = targetLookModel.getUrl();
        return new Breadcrumb(lookUrl, targetLookModel.getName(), null);
    }

    protected Breadcrumb getShopTheLookBreadcrumb(final TargetShopTheLookModel targetShopTheLookModel)
    {
        final String shopTheLookUrl = getTargetShopTheLookUrlResolver().resolve(targetShopTheLookModel);
        return new Breadcrumb(shopTheLookUrl, targetShopTheLookModel.getName(), null);
    }

    protected Breadcrumb getCategoryBreadcrumb(final CategoryModel category)
    {
        final String categoryUrl = getTargetCategoryModelUrlResolver().resolve(category);
        return new Breadcrumb(categoryUrl, category.getName(), null, category.getCode());
    }

    /**
     * @return the targetShopTheLookUrlResolver
     */
    protected UrlResolver<TargetShopTheLookModel> getTargetShopTheLookUrlResolver() {
        return targetShopTheLookUrlResolver;
    }

    /**
     * @param targetShopTheLookUrlResolver
     *            the targetShopTheLookUrlResolver to set
     */
    @Required
    public void setTargetShopTheLookUrlResolver(final UrlResolver<TargetShopTheLookModel> targetShopTheLookUrlResolver) {
        this.targetShopTheLookUrlResolver = targetShopTheLookUrlResolver;
    }

    /**
     * @return the targetCategoryModelUrlResolver
     */
    protected UrlResolver<CategoryModel> getTargetCategoryModelUrlResolver() {
        return targetCategoryModelUrlResolver;
    }

    /**
     * @param targetCategoryModelUrlResolver
     *            the targetCategoryModelUrlResolver to set
     */
    @Required
    public void setTargetCategoryModelUrlResolver(final UrlResolver<CategoryModel> targetCategoryModelUrlResolver) {
        this.targetCategoryModelUrlResolver = targetCategoryModelUrlResolver;
    }

}
