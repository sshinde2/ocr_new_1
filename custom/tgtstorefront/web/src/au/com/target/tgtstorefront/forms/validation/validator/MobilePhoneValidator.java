/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import java.util.regex.Matcher;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.MobilePhone;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class MobilePhoneValidator extends AbstractTargetValidator implements ConstraintValidator<MobilePhone, String> {

    @Override
    public void initialize(final MobilePhone mobilePhone) {
        field = FieldTypeEnum.mobilePhone;
        sizeRange = new int[] { TargetValidationCommon.MobilePhone.MIN_SIZE,
                TargetValidationCommon.MobilePhone.MAX_SIZE };
        isMandatory = mobilePhone.mandatory();
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.MobilePhone.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isSizeOutOfRange(final String value) {
        final Matcher replace = TargetValidationCommon.MobilePhone.CLEAN_REGEX.matcher(value);
        final String cleanValue = replace.replaceAll(StringUtils.EMPTY);
        return super.isSizeOutOfRange(cleanValue);
    }

    @Override
    protected String getInvalidPatternMessage() {
        final String messageCode = StringUtils.removeEnd(INVALID_PREFIX, ".");

        return messageSource.getMessage(messageCode, new String[] { "Mobile Phone Number" },
                i18nService.getCurrentLocale());
    }

    @Override
    protected String getInvalidSizeRangeSpecCaraSlash(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeSpecCaraPhone.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max),
                        getFieldString(fieldType).toLowerCase() },
                i18nService.getCurrentLocale());
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtstorefront.forms.validation.validator.AbstractTargetValidator#getInvalidSizeRange(au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum, int, int)
     */
    @Override
    protected String getInvalidSizeRange(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeDigits.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max) },
                i18nService.getCurrentLocale());
    }
}
