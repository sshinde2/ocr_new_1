package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2lib.model.components.BannerComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductData;


/**
 * Data model representing a slot in Product Engagement Component
 * 
 */
public class EngagementContainerSlotData {

    private BannerComponentModel banner;
    private ProductData productData;

    /**
     * @return the banner
     */
    public BannerComponentModel getBanner() {
        return banner;
    }

    /**
     * @param banner
     *            the banner to set
     */
    public void setBanner(final BannerComponentModel banner) {
        this.banner = banner;
    }

    /**
     * @return the productData
     */
    public ProductData getProductData() {
        return productData;
    }

    /**
     * @param productData
     *            the productData to set
     */
    public void setProductData(final ProductData productData) {
        this.productData = productData;
    }

}
