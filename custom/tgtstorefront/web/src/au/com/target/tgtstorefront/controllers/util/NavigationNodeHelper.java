package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuColumn;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuEntry;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuSection;
import au.com.target.tgtstorefront.util.NavigationNodeUtils;
import au.com.target.tgtwebcore.enums.NavigationNodeStyleEnum;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;


/**
 * Helper to construct navigation nodes
 */
public class NavigationNodeHelper {

    private static final Logger LOG = Logger.getLogger(NavigationNodeHelper.class);

    private static final String IMAGE_MIME_TYPE_PREFIX = "image";

    @Resource(name = "categoryConverter")
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Resource(name = "categoryDataUrlResolver")
    private UrlResolver<CategoryData> categoryDataUrlResolver;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    /**
     * Construct a list of navigation departments without children
     * 
     * @param navigationNode
     *            navigation node
     * 
     * @return departments
     */
    public List<MegaMenuDepartment> buildDepartmentWithoutChildren(final CMSNavigationNodeModel navigationNode) {
        return buildDepartments(navigationNode, false);
    }

    /**
     * Builds a list of {@link MegaMenuDepartment} for given navigation node <br/>
     * 
     * @param navigationNode
     *            - navigation node
     * 
     * @param isChildrenIncluded
     *            - true or false to indicate whether children need to be added to {@link MegaMenuDepartment}
     * 
     * @return departments - list of {@link MegaMenuDepartment}
     */
    protected List<MegaMenuDepartment> buildDepartments(final CMSNavigationNodeModel navigationNode,
            final boolean isChildrenIncluded) {

        final List<MegaMenuDepartment> departments = new ArrayList<>();

        if (navigationNode != null && navigationNode.isVisible()) {
            for (final CMSNavigationNodeModel child : navigationNode.getChildren()) {
                if (child.isVisible()) {
                    departments.add(buildDepartment(child, isChildrenIncluded));
                }
            }
        }
        return departments;
    }

    /**
     * Builds a {@link MegaMenuDepartment} for given navigation node <br/>
     * 
     * @param navigationNode
     *            - navigation node
     * 
     * @param isChildrenIncluded
     *            - true or false to indicate whether children need to be added to {@link MegaMenuDepartment}
     * 
     * @return department - an instance of {@link MegaMenuDepartment}
     */
    public MegaMenuDepartment buildDepartment(final CMSNavigationNodeModel navigationNode,
            final boolean isChildrenIncluded) {

        final MegaMenuDepartment department = new MegaMenuDepartment();

        department.setTitle(getNavigationNodeTitle(navigationNode, true));
        department.setLink(getNavigationNodeLink(navigationNode));

        if (isChildrenIncluded) {
            int maxColumnSize = 5;
            try {
                maxColumnSize = configurationService.getConfiguration().getInt("megamenu.max.column", 5);
            }
            catch (final NumberFormatException ex) {
                LOG.error("Configuration value 'megamenu.max.column' is not a valid integer");
            }

            final List<MegaMenuColumn> columns = new ArrayList<>();
            for (final CMSNavigationNodeModel child : navigationNode.getChildren()) {
                if (child.isVisible() && NavigationNodeTypeEnum.COLUMN.equals(child.getType())) {
                    final MegaMenuColumn megaMenuColumn = buildColumn(child);
                    if (CollectionUtils.isNotEmpty(megaMenuColumn.getSections())) {
                        columns.add(megaMenuColumn);
                    }
                    if (columns.size() == maxColumnSize) {
                        break;
                    }
                }
            }

            department.setColumns(columns);

        }
        return department;
    }

    /**
     * Gets the title for a NavigationNode.<br />
     * 
     * @param navigationNode
     *            The navigation node to get the link from.
     * 
     * @param isFallbackToName
     *            true to fall back to using the name value.
     * 
     * @return title
     */
    public String getNavigationNodeTitle(final CMSNavigationNodeModel navigationNode, final boolean isFallbackToName) {
        String title = NavigationNodeUtils.getNavigationNodeTitle(navigationNode, isFallbackToName);

        if (StringUtils.isBlank(title)) {
            final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();
            for (final CMSNavigationEntryModel entry : entries) {

                final ItemModel entryItem = entry.getItem();
                if (entryItem instanceof CategoryModel) {
                    final CategoryData categoryData = categoryConverter.convert((CategoryModel)entryItem);

                    title = categoryData.getName();
                }
                else if (entryItem instanceof ContentPageModel) {
                    final ContentPageModel contentPage = (ContentPageModel)entryItem;

                    title = contentPage.getTitle();

                    if (StringUtils.isBlank(title)) {
                        title = contentPage.getName();
                    }
                }
                else if (entryItem instanceof CMSLinkComponentModel) {
                    final CMSLinkComponentModel cmsLinkComponent = (CMSLinkComponentModel)entryItem;

                    // If the visibility of the link component is set to false then don't use it.
                    if (Boolean.FALSE.equals(cmsLinkComponent.getVisible())) {
                        continue;
                    }

                    title = cmsLinkComponent.getName();
                }

                if (StringUtils.isNotBlank(title)) {
                    break;
                }
            }
        }

        return title;
    }

    /**
     * Gets the link for a NavigationNode.<br />
     * Where this comes from depends on the type of entry in the NavigationNode.<br />
     * <br />
     * For a Category, the link is a link to that category, created using the {@link #categoryDataUrlResolver}.<br />
     * For a Content Page, the link is a link to that page, using the 'Label' value from the ContentPageModel.<br />
     * For a CMSLinkComponent, the link will be the value set in the 'URL' value, but only the the CMSLinkComponent is
     * 'Visible'.
     * 
     * @param navigationNode
     *            The navigation node to get the link from.
     * @return The link
     */
    public String getNavigationNodeLink(final CMSNavigationNodeModel navigationNode) {
        final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();
        for (final CMSNavigationEntryModel entry : entries) {

            final ItemModel entryItem = entry.getItem();
            if (entryItem instanceof CategoryModel) {
                final CategoryData categoryData = categoryConverter.convert((CategoryModel)entryItem);

                return categoryDataUrlResolver.resolve(categoryData);
            }
            else if (entryItem instanceof ContentPageModel) {
                final ContentPageModel contentPage = (ContentPageModel)entryItem;

                return contentPage.getLabel();
            }
            else if (entryItem instanceof CMSLinkComponentModel) {
                final CMSLinkComponentModel cmsLinkComponent = (CMSLinkComponentModel)entryItem;

                if (Boolean.FALSE.equals(cmsLinkComponent.getVisible())) {
                    continue;
                }

                return cmsLinkComponent.getUrl();
            }
        }

        return null;
    }

    /**
     * builds column list
     * 
     * @param columnNavigationNode
     * @return MegaMenuColumn
     */
    public MegaMenuColumn buildColumn(final CMSNavigationNodeModel columnNavigationNode) {
        final MegaMenuColumn column = new MegaMenuColumn();

        final List<MegaMenuSection> sections = new ArrayList<>();
        for (final CMSNavigationNodeModel child : columnNavigationNode.getChildren()) {
            if (child.isVisible() && NavigationNodeTypeEnum.SECTION.equals(child.getType())) {
                final MegaMenuSection megaMenuSection = buildSection(child);
                if (null != megaMenuSection.getEntries()) {
                    sections.add(megaMenuSection);
                }
            }
        }

        column.setSections(sections);
        column.setStyle(getNavigationNodeStyle(columnNavigationNode));
        column.setTitle(columnNavigationNode.getTitle());
        return column;
    }

    /**
     * builds the section and create entries
     * 
     * @param sectionNavigationNode
     * @return MegaMenuSection
     */
    protected MegaMenuSection buildSection(final CMSNavigationNodeModel sectionNavigationNode) {
        final MegaMenuSection section = new MegaMenuSection();
        section.setTitle(NavigationNodeUtils.getNavigationNodeTitle(sectionNavigationNode, false));
        section.setLink(getNavigationNodeLink(sectionNavigationNode));
        section.setStyle(getNavigationNodeStyle(sectionNavigationNode));

        final List<MegaMenuSection> sections = new ArrayList<>();
        final List<MegaMenuEntry> megaMenuEntries = new ArrayList<>();
        for (final CMSNavigationNodeModel child : sectionNavigationNode.getChildren()) {
            if (child.isVisible() && NavigationNodeTypeEnum.SECTION.equals(child.getType())) {
                final MegaMenuSection megaMenuSection = buildSection(child);
                if (null != megaMenuSection.getEntries()) {
                    sections.add(megaMenuSection);
                }
            }
            else if (child.isVisible()) {
                megaMenuEntries.add(createMegaMenuEntry(child));
            }
        }

        section.setSections(sections);
        section.setEntries(megaMenuEntries);

        return section;
    }


    /**
     * Create and populate a MegaMenuEntry from entries in the NavigationNode.<br />
     * <br />
     * Will use the first of the supported types to populate the MegaMenuEntry.<br />
     * Supported content types are:<br />
     * <ul>
     * <li>CategoryModel</li>
     * <li>ContentPageModel</li>
     * <li>CMSLinkComponentModel</li>
     * </ul>
     * Supported media types are:<br />
     * <ul>
     * <li>MediaModel</li>
     * </ul>
     * <br />
     * All other types are ignored.<br />
     * If a 'content' type of entry is present, then the 'text' and 'linkUrl' properties will be set.<br />
     * If a 'media' type (that has a Mime Type starting with 'image') is present then the 'imageUrl' and 'imageAltText'
     * properties will be set.<br />
     * <br />
     * If a NavigationNode contains only a 'content' type of entry this would represent text only, being a link if
     * 'linkUrl' is set.<br />
     * If a NavigationNode contains only an 'image' type of entry this would represent an image only.<br />
     * If a NavigationNode contains both types of entries, and 'linkUrl' is set, this would represent a clickable image.
     * 
     * @param navigationNode
     *            The navigation node to base the MegaMenuEntry on.
     * @return The populated MegaMenuEntry
     */
    protected MegaMenuEntry createMegaMenuEntry(final CMSNavigationNodeModel navigationNode) {
        final MegaMenuEntry megaMenuEntry = new MegaMenuEntry();

        megaMenuEntry.setText(getNavigationNodeTitle(navigationNode, true));

        // Indicates whether 'content' data has been found and populated into the MegaMenuEntry
        boolean isNonImageDataPopulated = false;

        // Indicates whether 'image' data has been found and populated into the MegaMenuEntry
        boolean isImageDataPopulated = false;

        final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();
        for (final CMSNavigationEntryModel entry : entries) {

            final ItemModel entryItem = entry.getItem();

            // If 'content' data has not been found and populated yet...
            if (!isNonImageDataPopulated) {
                // ...then check if the entryItem is a type that we're interested in.

                if (entryItem instanceof CategoryModel) {
                    final CategoryData categoryData = categoryConverter.convert((CategoryModel)entryItem);
                    megaMenuEntry.setLinkUrl(categoryDataUrlResolver.resolve(categoryData));

                    isNonImageDataPopulated = true;
                }
                else if (entryItem instanceof ContentPageModel) {
                    final ContentPageModel contentPage = (ContentPageModel)entryItem;
                    megaMenuEntry.setLinkUrl(contentPage.getLabel());

                    isNonImageDataPopulated = true;
                }
                else if (entryItem instanceof CMSLinkComponentModel) {
                    final CMSLinkComponentModel cmsLinkComponent = (CMSLinkComponentModel)entryItem;

                    // If the visibility of the link component is set to false then don't use it.
                    if (Boolean.FALSE.equals(cmsLinkComponent.getVisible())) {
                        continue;
                    }

                    megaMenuEntry.setLinkUrl(cmsLinkComponent.getUrl());

                    isNonImageDataPopulated = true;
                }
            }

            // If 'image' data has not been found yet...
            if (!isImageDataPopulated) {
                // ...then check if the entryItem is a type that we're interested in.
                if (entryItem instanceof MediaModel) {
                    final MediaModel media = (MediaModel)entryItem;

                    // Check if the media is an image type.
                    if (StringUtils.startsWith(media.getMime(), IMAGE_MIME_TYPE_PREFIX)) {
                        megaMenuEntry.setImageUrl(media.getDownloadURL());
                        megaMenuEntry.setImageAltText(media.getAltText());
                    }

                    isImageDataPopulated = true;
                }
            }

            // If both 'content' and 'image' data has been populated into the MegaMenuEntry
            // then we can stop the loop.
            if (isNonImageDataPopulated && isImageDataPopulated) {
                break;
            }
        }
        megaMenuEntry.setStyle(getNavigationNodeStyle(navigationNode));
        return megaMenuEntry;
    }


    /**
     * @param sectionNavigationNode
     * @return code of NavigationNodeStyleEnum
     */
    private String getNavigationNodeStyle(final CMSNavigationNodeModel sectionNavigationNode) {
        String styleCode = null;
        final NavigationNodeStyleEnum nodeStyle = sectionNavigationNode.getStyle();
        if (nodeStyle != null) {
            styleCode = nodeStyle.getCode();
        }
        return styleCode;
    }

    /**
     * Returns the SVG sprite URL for given component
     * 
     * @param navigationNode
     *            Navigation node
     * @return SVG URL
     */
    public String getSVGSpriteUrl(final CMSNavigationNodeModel navigationNode) {

        final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();
        for (final CMSNavigationEntryModel entry : entries) {
            final ItemModel entryItem = entry.getItem();
            if (entryItem instanceof MediaModel) {
                final MediaModel media = (MediaModel)entryItem;
                if (StringUtils.startsWith(media.getMime(), "image/svg")) {
                    return media.getURL();
                }
            }
        }
        return null;
    }
}