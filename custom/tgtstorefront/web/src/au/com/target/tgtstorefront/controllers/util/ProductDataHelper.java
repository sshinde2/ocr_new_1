/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Arrays;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtstorefront.controllers.pages.data.SchemaData;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * Helper that contains product data related utility methods
 */
public final class ProductDataHelper {
    public static final String CURRENT_PRODUCT = "currentProductCode";

    //CMS component ID to load CMS Component Object
    public static final String DELIVERY_HOME_DELIVERY_ID = "deliveryHomeDelivery";
    public static final String TAB_DELIVERY_HOME_DELIVERY_ID = "tabDeliveryHomeDelivery";
    public static final String DELIVERY_CNC_ID = "deliveryCNC";
    public static final String TAB_DELIVERY_CNC_ID = "tabDeliveryCNC";
    public static final String DELIVERY_IN_STORE_PURCHASE_ID = "deliveryInStorePurchase";
    public static final String TAB_DELIVERY_IN_STORE_PURCHASE_ID = "tabDeliveryInStorePurchase";
    public static final String DELIVERY_TERMS_ID = "deliveryTermsComponent";
    public static final String CNC_ERROR_MESSAGE_ID = "cNCErrorMessageComponent";
    //CMS Component fields name
    public static final String CMS_FIELD_NAME = "Name";
    public static final String CMS_FIELD_CONTENT = "Content";

    public static final String SIZES_LIST_NAME = "sizes";
    public static final String COLORS_LIST_NAME = "colours";
    public static final String ACTIVE_COLOR = "activeColour";


    private static final Logger LOG = Logger.getLogger(ProductDataHelper.class);

    private ProductDataHelper() {
        // prevent construction
    }

    public static String getCurrentProduct(final HttpServletRequest request) {
        return (String)request.getAttribute(CURRENT_PRODUCT);
    }

    public static void setCurrentProduct(final HttpServletRequest request, final String currentProductCode) {
        request.setAttribute(CURRENT_PRODUCT, currentProductCode);
    }

    /**
     * Add to the model, Content and Name of a CMS component
     * 
     * @param cMSComponentId
     * @param locale
     * @param model
     * @param cmsComponentService
     */
    public static void addContentNameFromCMSComponent(final String cMSComponentId, final Locale locale,
            final Model model, final CMSComponentService cmsComponentService) {
        //We check if the component id exist
        if (getAttrFromAbstractCMSComponentID(cMSComponentId, ProductDataHelper.CMS_FIELD_CONTENT, locale,
                cmsComponentService) == null) {
            return;
        }
        model.addAttribute(
                cMSComponentId,
                ProductDataHelper.getAttrFromAbstractCMSComponentID(
                        cMSComponentId, ProductDataHelper.CMS_FIELD_CONTENT, locale, cmsComponentService));
        model.addAttribute(cMSComponentId.concat(ProductDataHelper.CMS_FIELD_NAME),
                ProductDataHelper.getAttrFromAbstractCMSComponentID(cMSComponentId, ProductDataHelper.CMS_FIELD_NAME,
                        locale, cmsComponentService));
    }

    /**
     * 
     * @param abstractCMSComponentID
     * @param attrName
     * @param locale
     * @param cmsComponentService
     * @return Attribute loaded from a CMS Component
     */
    public static String getAttrFromAbstractCMSComponentID(
            final String abstractCMSComponentID,
            final String attrName,
            final Locale locale,
            final CMSComponentService cmsComponentService) {
        final AbstractCMSComponentModel cMSComponent;
        try {
            cMSComponent = cmsComponentService
                    .getAbstractCMSComponent(abstractCMSComponentID);

            final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");
            final Object localizedAttr = modelService.getAttributeValue(cMSComponent, attrName);

            if (null != localizedAttr) {
                return localizedAttr.toString();
            }
        }
        catch (final CMSItemNotFoundException e) {
            LOG.error("The CMS item id doesn't exit : " + abstractCMSComponentID, e);
        }

        return null;
    }

    public static String getProductCodeToRedirect(final ProductFacade productFacade,
            final ProductModel productModel) {
        for (final VariantProductModel variantProductModel : productModel.getVariants()) {
            final TargetProductData productData = (TargetProductData)productFacade.getProductForCodeAndOptions(
                    variantProductModel.getCode(),
                    Arrays.asList(ProductOption.STOCK, ProductOption.VARIANT_FULL));
            //If this product have variants option we determine if they are not all out of stock before redirecting
            if (CollectionUtils.isNotEmpty(productData.getVariantOptions())) {
                for (final VariantOptionData variantOptionData : productData.getVariantOptions()) {
                    //If their is a variant option in stock, redirect to this product data
                    if (TargetProductUtils.checkIfStockDataHasStock(variantOptionData.getStock())) {
                        return productData.getCode();
                    }
                }
            }
            //If we have no variants, check if this product data is in stock before redirecting
            else if (TargetProductUtils.checkIfStockDataHasStock(productData.getStock())) {
                return productData.getCode();
            }
        }
        //Every variance are out of stock we redirect to the first one in the list
        if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
            return productModel.getVariants().iterator().next().getCode();
        }
        else {
            LOG.warn("Cannot redirect to any variance, for code product : " + productModel.getCode());
            return productModel.getCode();
        }
    }


    /**
     * Populate the schemaData
     * 
     * @param productCode
     * @param productListerData
     * @return SchemaData
     */
    public static SchemaData getSchemaData(final TargetProductListerData productListerData, final String productCode) {
        final SchemaData data = new SchemaData();
        if (null != productListerData) {
            data.setCategoryName(productListerData.getTopLevelCategory());
            data.setMaxAvailQtyInStore(productListerData.getMaxAvailStoreQty());
            data.setMaxAvailQtyOnline(productListerData.getMaxAvailOnlineQty());
            data.setPriceRange(productListerData.getPriceRange());
            data.setPrice(productListerData.getPrice());
            data.setName(productListerData.getName());
            data.setProductCode(productCode);
            if (CollectionUtils.isNotEmpty(productListerData.getTargetVariantProductListerData())) {
                final TargetVariantProductListerData variantData = productListerData
                        .getTargetVariantProductListerData().get(0);
                data.setProductDisplayType(variantData.getProductDisplayType());
                data.setNormalSaleDate(variantData.getNormalSaleStartDate());
                if (CollectionUtils.isNotEmpty(variantData.getLargeImageUrls())) {
                    //Just need the first image url
                    data.setPrimaryImage(variantData.getLargeImageUrls().get(0));
                }
            }
        }
        return data;
    }

}
