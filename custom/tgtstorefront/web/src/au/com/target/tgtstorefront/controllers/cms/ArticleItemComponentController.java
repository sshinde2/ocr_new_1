/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.converters.ArticleItemConverter;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;


@Controller("ArticleItemComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.ARTICLE_ITEM_COMPONENT)
public class ArticleItemComponentController extends AbstractCMSComponentController<ArticleItemComponentModel> {
    @Autowired
    private ArticleItemConverter articleItemConverter;




    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final ArticleItemComponentModel component) {
        if (component.getYouTubeLink() != null) {
            model.addAttribute(WebConstants.ARTICLE_ITEM_YOUTUBE_LINK, component.getYouTubeLink());
        }
        model.addAttribute("article", articleItemConverter.convert(component));
    }


}
