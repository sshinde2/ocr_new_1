/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import org.springframework.ui.Model;


/**
 * @author rmcalave
 * 
 */
public class MiniCartDataHelper {

    private static final String MODEL_ATTR_SUBTOTAL = "subTotal";
    private static final String MODEL_ATTR_TOTALITEMS = "totalItems";
    private static final String MODEL_ATTR_TOTALDISPLAY = "totalDisplay";
    private static final String TOTAL_DISPLAY_SUBTOTAL = "SUBTOTAL";

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    /**
     * Populate the model with the information required to display the mini cart summary.
     * 
     * @param model
     *            The model to populate.
     */
    public void populateMiniCartData(final Model model) {
        final CartData cartData = cartFacade.getMiniCart();
        model.addAttribute(MODEL_ATTR_SUBTOTAL, cartData.getSubTotal());
        model.addAttribute(MODEL_ATTR_TOTALITEMS, cartData.getTotalUnitCount());
        model.addAttribute(MODEL_ATTR_TOTALDISPLAY, TOTAL_DISPLAY_SUBTOTAL);
    }
}
