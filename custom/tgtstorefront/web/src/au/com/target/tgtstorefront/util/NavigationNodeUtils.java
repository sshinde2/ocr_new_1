/**
 * 
 */
package au.com.target.tgtstorefront.util;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import org.apache.commons.lang.StringUtils;


/**
 * @author rmcalave
 * 
 */
public final class NavigationNodeUtils {
    private static final String DEFAULT_NAV_NODE_NAME = "CMSNavigationNode";

    private NavigationNodeUtils() {
    }

    /**
     * Gets the title of the NavigationNode. If 'title' is not set and 'fallbackToName' is true then use the 'name' of
     * the navigation node.<br />
     * If the 'name' value is not set the default value of "CMSNavigationNode" will be filtered out and a blank String
     * will be returned.
     * 
     * @param navigationNode
     * @param fallbackToName
     *            true to fall back to using the name value.
     * @return The title of the navigation node.
     */
    public static String getNavigationNodeTitle(final CMSNavigationNodeModel navigationNode,
            final boolean fallbackToName) {
        String title = navigationNode.getTitle();

        if (fallbackToName && StringUtils.isBlank(title)) {
            title = navigationNode.getName();

            if (DEFAULT_NAV_NODE_NAME.equals(title)) {
                title = "";
            }
        }

        return title;
    }
}
