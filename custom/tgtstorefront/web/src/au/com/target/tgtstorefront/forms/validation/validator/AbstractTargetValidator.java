/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import java.lang.reflect.Field;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;



/**
 * 
 * @author Benoit Vanalderweireldt
 * 
 */
public abstract class AbstractTargetValidator {

    protected static final String FIELD_PREFIX = "field.";

    protected static final String INVALID_PREFIX = FIELD_PREFIX + "invalid.";

    protected static final String DEFAULT_PATTERN = "PATTERN";

    protected static final Logger LOG = Logger.getLogger(AbstractTargetValidator.class);

    protected FieldTypeEnum field;
    protected boolean isMandatory;
    protected boolean isSizeRange;
    protected boolean isSizeFixed;
    protected int[] sizeRange;
    protected boolean mustMatch;
    protected Pattern pattern;

    @Resource(name = "messageSource")
    protected MessageSource messageSource;

    @Resource(name = "i18nService")
    protected I18NService i18nService;


    /**
     * 
     * @param value
     * @param context
     * @return boolean
     */
    public boolean isValidCommon(final String value, final ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        final boolean isBlank = StringUtils.isBlank(value);
        if (isMandatory && isBlank) {
            updatedContext(context, getInvalidEmpty(field));
            return false;
        }
        else if (!isMandatory && isBlank) {
            return true;
        }
        else if (mustMatch && !pattern.matcher(value).matches()) {
            updatedContext(context, getInvalidPatternMessage());
            return false;
        }
        else if (isSizeOutOfRange(value)) {
            if (sizeRange[0] == sizeRange[1]) {
                updatedContext(context, getInvalidSize(field, sizeRange[0]));
            }
            else {
                updatedContext(context, getInvalidSizeRange(field, sizeRange[0], sizeRange[1]));
            }
            return false;
        }
        else if (isSizeFixed && value.length() != sizeRange[0]) {
            updatedContext(context, getInvalidSizeMessage(field, sizeRange[0]));
            return false;
        }
        else if (!isType(value)) {
            updatedContext(context, getInvalidNotSelect(field));
            return false;
        }
        else if (!isAvailable(value)) {
            updatedContext(context, getInvalidNotAvailable(field));
            return false;
        }

        return true;
    }

    /**
     * @return true if size is strictly under minimum or strictly over maximum
     */
    protected boolean isSizeOutOfRange(final String value) {
        if (!isSizeRange) {
            return false;
        }
        if (value.length() < sizeRange[0] || value.length() > sizeRange[1]) {
            return true;
        }
        return false;
    }

    /**
     * @param value
     * @return true is it can be cast to the expected type
     */
    protected boolean isType(final String value) {
        // by default always return true it must be override
        return true;
    }

    /**
     * @param value
     * @return true if it's a valid choice
     */
    protected boolean isAvailable(final String value) {
        // by default always return true it must be override
        return true;
    }


    /**
     * 
     * Load a static Pattern reference
     * 
     * @param target
     */
    protected void loadPattern(final Class target) {
        try {
            final Field patternField = target.getField(DEFAULT_PATTERN);
            final Object instance = Class.forName("au.com.target.tgtutility.util.TargetValidationCommon$"
                    .concat(WordUtils.capitalize(field
                            .toString())));
            pattern = (Pattern)patternField.get(instance);
        }
        catch (final Exception e) {
            LOG.error(
                    "Wrong annotation configuration, cannot retrieve pattern of the field, have you updated TargetValidationCommon ?",
                    e);
            Validate.isTrue(false);
        }
    }



    /**
     * 
     * @param context
     * @param message
     */
    protected void updatedContext(final ConstraintValidatorContext context, final String message) {
        context.buildConstraintViolationWithTemplate(message)
                .addConstraintViolation();
    }

    /**
     * 
     * @param fieldType
     * @return The string representation of a field in properties
     */
    protected String getFieldString(final FieldTypeEnum fieldType) {
        return messageSource
                .getMessage(FIELD_PREFIX.concat(fieldType.toString()), null, i18nService.getCurrentLocale());
    }

    /**
     * Default message when format is not valid
     * 
     * @return String
     */
    protected String getInvalidPatternMessage() {
        return getInvalidFormat(field);
    }

    /**
     * 
     * @param fieldType
     * @return String
     */
    protected String getInvalidEmpty(final FieldTypeEnum fieldType) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.empty.toString()),
                new Object[] { fieldType.isConsonant(), getFieldString(fieldType) },
                i18nService.getCurrentLocale());
    }


    /**
     * 
     * @param fieldType
     * @param min
     * @param max
     * @return String
     */
    protected String getInvalidSizeRangeSpecCara(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeSpecCara.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @param min
     * @param max
     * @return String
     */
    protected String getInvalidSizeRangeSpecCaraSlash(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeSpecCaraSlash.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @param min
     * @param max
     * @return String
     */
    protected String getInvalidsizeRangeSpaceLetters(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeSpaceLetters.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @param min
     * @param max
     * @return String
     */
    protected String getInvalidSizeRange(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRange.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @param min
     * @return String
     */
    protected String getInvalidSize(final FieldTypeEnum fieldType, final int min) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRange.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @return String
     */
    protected String getInvalidFormat(final FieldTypeEnum fieldType) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.format.toString()),
                new Object[] { getFieldString(fieldType) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @param min
     * @return String
     */
    protected String getInvalidSizeMore(final FieldTypeEnum fieldType, final int min) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeMore.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @param invalidCharacters
     * @return String
     */
    protected String getInvalidCharacters(final FieldTypeEnum fieldType, final String invalidCharacters) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.characters.toString()),
                new Object[] { getFieldString(fieldType), invalidCharacters },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @return String
     */
    protected String getInvalidNotAvailable(final FieldTypeEnum fieldType) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.notAvailable.toString()),
                new Object[] { getFieldString(fieldType) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @return String
     */
    protected String getInvalidNotSelect(final FieldTypeEnum fieldType) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.select.toString()),
                new Object[] { fieldType.isConsonant(), getFieldString(fieldType) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @return String
     */
    protected String getInvalidSizeMessage(final FieldTypeEnum fieldType, final int size) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.size.toString()),
                new Object[] { getFieldString(fieldType), Integer.valueOf(size) },
                i18nService.getCurrentLocale());
    }

    /**
     * 
     * @param fieldType
     * @return String
     */
    protected String getInvalidInsecure(final FieldTypeEnum fieldType) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.insecure.toString()),
                new Object[] { getFieldString(fieldType) },
                i18nService.getCurrentLocale());
    }

}
