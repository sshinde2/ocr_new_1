/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.navigation.AbstractMenuBuilder;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.util.NavigationNodeUtils;
import au.com.target.tgtwebcore.model.cms2.components.NavigationComponentModel;


/**
 * @author rmcalave
 * 
 */
public class NavigationComponentMenuBuilder extends AbstractMenuBuilder {
    public NavigationMenuItem createNavigationComponentMenu(final NavigationComponentModel component) {
        final CMSNavigationNodeModel containerNavNode = component.getNavigationNode();

        if (containerNavNode == null || !containerNavNode.isVisible()) {
            return null;
        }

        final List<CMSNavigationEntryModel> containerNavNodeEntries = containerNavNode.getEntries();

        if (CollectionUtils.isEmpty(containerNavNodeEntries)) {
            return null;
        }

        final List<CMSNavigationNodeModel> containerNavNodeChildren = containerNavNode.getChildren();
        final Set<NavigationMenuItem> childNavigationMenuItems = createChildNavigationMenuItems(containerNavNodeChildren);

        return createNavigationMenuItem(containerNavNodeEntries.get(0), null, childNavigationMenuItems,
                NavigationNodeUtils.getNavigationNodeTitle(containerNavNode, true));
    }

    private Set<NavigationMenuItem> createChildNavigationMenuItems(
            final List<CMSNavigationNodeModel> containerNavNodeChildren) {
        if (CollectionUtils.isEmpty(containerNavNodeChildren)) {
            return null;
        }

        final Set<NavigationMenuItem> navigationMenuItems = new LinkedHashSet<>();

        for (final CMSNavigationNodeModel navNode : containerNavNodeChildren) {
            final String navigationNodeTitle = NavigationNodeUtils.getNavigationNodeTitle(navNode, true);

            final List<CMSNavigationEntryModel> navNodeEntries = navNode.getEntries();

            if (!navNode.isVisible() || CollectionUtils.isEmpty(navNodeEntries)) {
                continue;
            }

            navigationMenuItems.add(createNavigationMenuItem(navNodeEntries.get(0), null, null, navigationNodeTitle));
        }

        return navigationMenuItems;
    }
}
