/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.security.IdentifyExitingCheckoutStrategy;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;


/**
 * @author bhuang3
 * 
 */
public class AnonymousCheckoutHandlerInterceptor implements BeforeControllerHandler {

    private static final Logger LOG = Logger.getLogger(AnonymousCheckoutHandlerInterceptor.class);
    private TargetCheckoutCustomerStrategy checkoutCustomerStrategy;
    private SessionService sessionService;
    private IdentifyExitingCheckoutStrategy identifyExitingCheckoutStrategy;
    private UserService userService;


    protected TargetCheckoutCustomerStrategy getCheckoutCustomerStrategy() {
        return checkoutCustomerStrategy;
    }

    @Override
    public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
            final HandlerMethod handlerMethod) {
        if (identifyExitingCheckoutStrategy.isUserMovingOutOfCheckout(request)
                && Boolean.TRUE.equals(getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT))
                && getCheckoutCustomerStrategy().isAnonymousCheckout()
                && !TargetSessionTokenUtil.isPlaceOrderInProgress(request)) {
            final CustomerModel customerModel = checkoutCustomerStrategy.getCurrentUserForCheckout();
            if (null != customerModel && customerModel instanceof TargetCustomerModel) {
                final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)customerModel;
                UserModel newCartCustomer = userService.getAnonymousUser();
                if (Boolean.TRUE
                        .equals(getSessionService().getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST))) {
                    LOG.info("ExitingCheckout: soft logged in customer as guest, pk=" + targetCustomerModel.getPk());
                    final UserModel currentUser = userService.getCurrentUser();
                    newCartCustomer = currentUser != null ? currentUser : newCartCustomer;
                    request.getSession().removeAttribute(WebConstants.SECURE_GUID_SESSION_KEY);
                }

                if (CustomerType.GUEST.equals(targetCustomerModel.getType())) {
                    LOG.info("ExitingCheckout: guest customer, pk=" + targetCustomerModel.getPk());
                    checkoutCustomerStrategy.removeGuestCheckoutCustomer(targetCustomerModel, newCartCustomer);
                }
            }
            getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
            getSessionService().removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
        }

        return true;
    }

    @Required
    public void setCheckoutCustomerStrategy(final TargetCheckoutCustomerStrategy checkoutCustomerStrategy) {
        this.checkoutCustomerStrategy = checkoutCustomerStrategy;
    }

    protected SessionService getSessionService() {
        return sessionService;
    }

    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param identifyExitingCheckoutStrategy
     *            the identifyExitingCheckoutStrategy to set
     */
    @Required
    public void setIdentifyExitingCheckoutStrategy(
            final IdentifyExitingCheckoutStrategy identifyExitingCheckoutStrategy) {
        this.identifyExitingCheckoutStrategy = identifyExitingCheckoutStrategy;
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

}
