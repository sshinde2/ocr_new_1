package au.com.target.endeca.infront.cartridge;

import java.util.HashMap;
import java.util.Map;

import au.com.target.endeca.infront.constants.EndecaConstants;

import com.endeca.infront.cartridge.ResultsListRequestParamMarshaller;


/**
 * The Class TgtRecommendationRequestParamMarshaller.
 * 
 * @author ragarwa3
 * 
 */
public class TgtRecommendationRequestParamMarshaller extends ResultsListRequestParamMarshaller {

    /**
     * Instantiates a new tgt recommendation request param marshaller.
     */
    public TgtRecommendationRequestParamMarshaller() {
        final Map map = new HashMap();
        map.put(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES,
                EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES);
        map.put(EndecaConstants.EndecaParamMarshaller.ENDECA_TOP_LEVEL_CATEGORY_CODE,
                EndecaConstants.EndecaParamMarshaller.ENDECA_TOP_LEVEL_CATEGORY_CODE);
        setRequestMap(map);
    }

}
