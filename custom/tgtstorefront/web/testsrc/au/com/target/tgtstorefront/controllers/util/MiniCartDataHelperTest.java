package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;


@RunWith(MockitoJUnitRunner.class)
public class MiniCartDataHelperTest {

    @Mock
    private CartFacade mockCartFacade;

    @InjectMocks
    private final MiniCartDataHelper miniCartDataHelper = new MiniCartDataHelper();

    @Test
    public void testPopulateMiniCartData() throws Exception {
        final PriceData subTotal = new PriceData();
        final Integer totalUnitCount = new Integer(3);

        final CartData cartData = new CartData();
        cartData.setSubTotal(subTotal);
        cartData.setTotalUnitCount(totalUnitCount);

        BDDMockito.given(mockCartFacade.getMiniCart()).willReturn(cartData);

        final Model mockModel = Mockito.mock(Model.class);

        miniCartDataHelper.populateMiniCartData(mockModel);

        Mockito.verify(mockModel).addAttribute("subTotal", subTotal);
        Mockito.verify(mockModel).addAttribute("totalItems", totalUnitCount);
        Mockito.verify(mockModel).addAttribute("totalDisplay", "SUBTOTAL");
    }

}
