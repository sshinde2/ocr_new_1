/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import au.com.target.tgtfacades.cart.TargetOrderErrorHandlerFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.AdjustedEntryQuantityData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CookieStringBuilder;
import au.com.target.tgtstorefront.controllers.util.RequestHeadersHelper;
import au.com.target.tgtstorefront.data.MessageKeyArguments;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutPlaceOrderControllerTest {

    @InjectMocks
    private CheckoutPlaceOrderController controller = new CheckoutPlaceOrderController();
    @Mock
    private Model model;
    @Mock
    private HttpServletRequest request;
    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;
    @Mock
    private HttpSession session;
    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private TargetOrderErrorHandlerFacade targetOrderErrorHandlerFacade;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private TargetCheckoutFacade checkoutFacade;
    @Mock
    private SalesApplicationFacade salesApplicationFacade;
    @Mock
    private TargetCartData cartData;
    @Mock
    private CartModel cartModel;
    @Mock
    private RequestHeadersHelper requestHeadersHelper;
    @Mock
    private CookieStringBuilder cookieStringBuilder;
    @Mock
    private TargetPlaceOrderFacade targetPlaceOrderFacade;
    @Mock
    private TargetPlaceOrderResult placeOrderResult;
    @Mock
    private TargetCCPaymentInfoData paymentInfo;
    @Mock
    private UserService userService;

    private final RedirectAttributesModelMap redirectAttributes = new RedirectAttributesModelMap();
    private Exception exception;


    @Before
    public void setUp() {
        controller = spy(controller);
        doReturn(null).when(controller).getRedirectionAndCheckSOH(model, redirectAttributes, StringUtils.EMPTY,
                cartData);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
        when(request.getSession()).thenReturn(session);
        when(targetPlaceOrderFacade.placeOrder()).thenReturn(placeOrderResult);
        when(cartData.getPaymentInfo()).thenReturn(paymentInfo);
        when(cartModel.getCode()).thenReturn("cart1");
        when(request.getParameter(TargetSessionTokenUtil.getTokenKey())).thenReturn("token");
        when(session.getAttribute(TargetSessionTokenUtil.getTokenKey())).thenReturn("token");
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldReturnToReviewPageIfIpgPaymentFailed() throws CMSItemNotFoundException, InvalidCartException {
        when(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).thenReturn("token");
        when(session.getAttribute("token")).thenReturn("token");
        when(placeOrderResult.getPlaceOrderResult()).thenReturn(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
        when(paymentInfo.isPaymentSucceeded()).thenReturn(false);
        when(checkoutFacade.getCart()).thenReturn(cartModel);
        final String returnPage = controller.placeIpgOrder(model, redirectAttributes, request);
        final Map<String, ?> map = redirectAttributes.getFlashAttributes();

        final List<MessageKeyArguments> errMsgList = (List<MessageKeyArguments>)map.get("accErrorMsgs");

        assertEquals(1, errMsgList.size());
        assertEquals("payment.error.ipg", errMsgList.get(0).getKey());
        assertEquals(ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY, returnPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldReturnToHoldingPageIfTokenMismatch() throws CMSItemNotFoundException, InvalidCartException {
        when(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).thenReturn("token");
        when(session.getAttribute(ControllerConstants.IPG_PARAM_TOKEN)).thenReturn(null);

        final String returnPage = controller.placeIpgOrder(model, redirectAttributes, request);

        assertEquals(ControllerConstants.Redirection.CHECKOUT_ORDER_PLACE_HOLDING, returnPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldRefreshInReviewPageIfPaymentCancelled() throws CMSItemNotFoundException, InvalidCartException {
        final TargetQueryTransactionDetailsResult queryResults = new TargetQueryTransactionDetailsResult();
        queryResults.setCancel(true);

        when(targetPlaceOrderFacade.getQueryTransactionDetailResults()).thenReturn(queryResults);

        final String redirectPage = controller.getRedirectUrl("123456789", "", "sessionId");
        assertEquals(ControllerConstants.REVIEW, redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldProceedIfPaymentNotCancelled() throws CMSItemNotFoundException, InvalidCartException {
        final TargetQueryTransactionDetailsResult queryResults = new TargetQueryTransactionDetailsResult();
        queryResults.setCancel(false);

        when(targetPlaceOrderFacade.getQueryTransactionDetailResults()).thenReturn(queryResults);

        final String redirectPage = controller.getRedirectUrl("123456789", "", "sessionId");
        assertEquals(ControllerConstants.PLACE_ORDER
                + ControllerConstants.PLACE_IPG_ORDER_HOLDING + "?" + ControllerConstants.IPG_PARAM_TOKEN
                + "=123456789&SessionId=sessionId", redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldProceedForNullTransactionResutls() throws CMSItemNotFoundException, InvalidCartException {
        when(targetPlaceOrderFacade.getQueryTransactionDetailResults()).thenReturn(null);

        final String redirectPage = controller.getRedirectUrl("123456789", "", "sessionId");
        assertEquals(ControllerConstants.PLACE_ORDER
                + ControllerConstants.PLACE_IPG_ORDER_HOLDING + "?" + ControllerConstants.IPG_PARAM_TOKEN
                + "=123456789&SessionId=sessionId", redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldRedirectToPaymentPageIfIpgAborted() throws CMSItemNotFoundException, InvalidCartException {
        final String redirectPage = controller.getRedirectUrl(null, "ipg_abort_session", "sessionId");
        assertEquals(ControllerConstants.PAYMENT, redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldNotRedirectToPaymentPageOtherStringProvided()
            throws CMSItemNotFoundException, InvalidCartException {
        final String redirectPage = controller.getRedirectUrl("1234", "delete_the_website", "sessionId");
        assertEquals(ControllerConstants.PLACE_ORDER
                + ControllerConstants.PLACE_IPG_ORDER_HOLDING + "?" + ControllerConstants.IPG_PARAM_TOKEN
                + "=1234&SessionId=sessionId", redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldRedirectToHoldingPageIfNotAborted() throws CMSItemNotFoundException, InvalidCartException {
        final String redirectPage = controller.getRedirectUrl("1234", null, "sessionId");
        assertEquals(ControllerConstants.PLACE_ORDER
                + ControllerConstants.PLACE_IPG_ORDER_HOLDING + "?" + ControllerConstants.IPG_PARAM_TOKEN
                + "=1234&SessionId=sessionId", redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldReturnNullIfNothingProvided() throws CMSItemNotFoundException, InvalidCartException {
        final String redirectPage = controller.getRedirectUrl(null, null, null);
        assertEquals(null, redirectPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testHasSOHUpdatedWhenBothAdjustedAndOutOfStockItemsAvailable() {
        final Set<AdjustedCartEntriesData> sohUpdates = new HashSet<>();
        final AdjustedCartEntriesData data = new AdjustedCartEntriesData();

        final AdjustedEntryQuantityData adjustedData = new AdjustedEntryQuantityData();

        final List<AdjustedEntryQuantityData> adjustedItems = new ArrayList<>();
        adjustedItems.add(adjustedData);
        final List<AdjustedEntryQuantityData> outOfStockItems = new ArrayList<>();
        outOfStockItems.add(adjustedData);

        data.setAdjustedItems(adjustedItems);
        data.setOutOfStockItems(outOfStockItems);

        sohUpdates.add(data);

        final Map<String, Object> map = new HashMap<>();
        map.put(ControllerConstants.SOH_UPDATES, sohUpdates);

        model.addAttribute(ControllerConstants.SOH_UPDATES, Collections.singleton(sohUpdates));

        when(model.containsAttribute(ControllerConstants.SOH_UPDATES)).thenReturn(true);
        when(model.asMap()).thenReturn(map);

        final boolean hasSOHUpdatd = controller.hasSOHUpdated(model);
        assertTrue(hasSOHUpdatd);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testHasSOHUpdatedWhenOnlyAdjustedItemsAvailable() {
        final Set<AdjustedCartEntriesData> sohUpdates = new HashSet<>();
        final AdjustedCartEntriesData data = new AdjustedCartEntriesData();

        final AdjustedEntryQuantityData adjustedData = new AdjustedEntryQuantityData();

        final List<AdjustedEntryQuantityData> adjustedItems = new ArrayList<>();
        adjustedItems.add(adjustedData);
        data.setAdjustedItems(adjustedItems);

        sohUpdates.add(data);

        final Map<String, Object> map = new HashMap<>();
        map.put(ControllerConstants.SOH_UPDATES, sohUpdates);

        model.addAttribute(ControllerConstants.SOH_UPDATES, Collections.singleton(sohUpdates));

        when(model.containsAttribute(ControllerConstants.SOH_UPDATES)).thenReturn(true);
        when(model.asMap()).thenReturn(map);

        final boolean hasSOHUpdatd = controller.hasSOHUpdated(model);
        assertTrue(hasSOHUpdatd);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testHasSOHUpdatedWhenOnlyOutOfStockItemsAvailable() {
        final Set<AdjustedCartEntriesData> sohUpdates = new HashSet<>();
        final AdjustedCartEntriesData data = new AdjustedCartEntriesData();

        final AdjustedEntryQuantityData adjustedData = new AdjustedEntryQuantityData();

        final List<AdjustedEntryQuantityData> outOfStockItems = new ArrayList<>();
        outOfStockItems.add(adjustedData);
        data.setAdjustedItems(outOfStockItems);

        sohUpdates.add(data);

        final Map<String, Object> map = new HashMap<>();
        map.put(ControllerConstants.SOH_UPDATES, sohUpdates);

        model.addAttribute(ControllerConstants.SOH_UPDATES, Collections.singleton(sohUpdates));

        when(model.containsAttribute(ControllerConstants.SOH_UPDATES)).thenReturn(true);
        when(model.asMap()).thenReturn(map);

        final boolean hasSOHUpdatd = controller.hasSOHUpdated(model);
        assertTrue(hasSOHUpdatd);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testHasSOHUpdatedWhenNoAdjustedItemsAvailable() {
        final Set<AdjustedCartEntriesData> sohUpdates = new HashSet<>();
        final Map<String, Object> map = new HashMap<>();
        map.put(ControllerConstants.SOH_UPDATES, sohUpdates);

        model.addAttribute(ControllerConstants.SOH_UPDATES, Collections.singleton(sohUpdates));

        when(model.containsAttribute(ControllerConstants.SOH_UPDATES)).thenReturn(true);
        when(model.asMap()).thenReturn(map);

        final boolean hasSOHUpdatd = controller.hasSOHUpdated(model);
        assertFalse(hasSOHUpdatd);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testHasSOHUpdatedWhenModelDoesNotHaveSOHUpdateData() {
        when(model.containsAttribute(ControllerConstants.SOH_UPDATES)).thenReturn(false);
        final boolean hasSOHUpdatd = controller.hasSOHUpdated(model);
        assertFalse(hasSOHUpdatd);
    }

    @Test
    public void testPlaceOrderInteralGiftCardReversalWhenTotalOutOfStockHappens() throws CMSItemNotFoundException {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        giftCards.add(giftCard);
        when(targetPlaceOrderFacade.getGiftCardPayments(Mockito.any(CartModel.class))).thenReturn(giftCards);

        final String redirection = "redirection:/review";
        doReturn(redirection).when(controller).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY,
                cartData);

        final String redirectionReturned = controller.placeOrderInternal(model, redirectAttributes, request, true);

        verify(targetPlaceOrderFacade, Mockito.times(1)).reverseGiftCardPayment();
        verify(controller, Mockito.times(0)).handlePlaceOrder();
        assertEquals(redirection, redirectionReturned);
    }

    @Test
    public void testPlaceOrderInteralGiftCardReversalWhenPartialStockAdjustement() throws CMSItemNotFoundException {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        giftCards.add(giftCard);
        when(targetPlaceOrderFacade.getGiftCardPayments(Mockito.any(CartModel.class))).thenReturn(giftCards);

        doReturn(null).when(controller).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY,
                cartData);
        doReturn(Boolean.TRUE).when(controller).hasSOHUpdated(model);

        final String redirectionReturned = controller.placeOrderInternal(model, redirectAttributes, request, true);

        verify(targetPlaceOrderFacade, Mockito.times(1)).reverseGiftCardPayment();
        verify(controller, Mockito.times(0)).handlePlaceOrder();
        assertEquals(ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY, redirectionReturned);
    }

    @Test
    public void testPlaceOrderInteralGiftCardReversalWhenTotalOutOfStockHappensWithoutGiftCardsInPayments()
            throws CMSItemNotFoundException {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        when(targetPlaceOrderFacade.getGiftCardPayments(Mockito.any(CartModel.class))).thenReturn(giftCards);

        final String redirection = "redirection:/review";
        doReturn(redirection).when(controller).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY,
                cartData);

        final String redirectionReturned = controller.placeOrderInternal(model, redirectAttributes, request, true);

        verify(targetPlaceOrderFacade, Mockito.times(0)).reverseGiftCardPayment();
        verify(controller, Mockito.times(0)).handlePlaceOrder();
        assertEquals(redirection, redirectionReturned);
    }

    @Test
    public void testPlaceOrderInteralGiftCardReversalWhenPartialStockAdjustementWithoutGiftCardsI()
            throws CMSItemNotFoundException {
        final List<TargetCardResult> giftCards = new ArrayList<>();
        when(targetPlaceOrderFacade.getGiftCardPayments(Mockito.any(CartModel.class))).thenReturn(giftCards);

        doReturn(null).when(controller).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY,
                cartData);
        doReturn(Boolean.TRUE).when(controller).hasSOHUpdated(model);

        final String redirectionReturned = controller.placeOrderInternal(model, redirectAttributes, request, true);

        verify(targetPlaceOrderFacade, Mockito.times(0)).reverseGiftCardPayment();
        verify(controller, Mockito.times(1)).handlePlaceOrder();
        assertTrue(redirectionReturned.contains(ControllerConstants.Redirection.CHECKOUT_THANK_YOU_CHECK));
    }

    @Test
    public void testHandlePlaceOrderResultReturnToReviewPageWhenOrderTotalsDontMatch() throws CMSItemNotFoundException {
        final TargetPlaceOrderResultEnum result = TargetPlaceOrderResultEnum.
                PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS;
        final String redirectUrl = controller.handlePlaceOrderResult(result, "orderId",
                mock(RedirectAttributes.class), true);

        assertEquals(ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY, redirectUrl);
    }

    @Test
    public void testIsGiftCardPaymentAllowedWhenCartHasNoGiftCards() {
        assertTrue(controller.isGiftCardPaymentAllowed());
    }

    @Test
    public void testIsGiftCardPaymentAllowedWhenCartHasGiftCards() {
        mockInvalidGiftCardPayment();
        assertFalse(controller.isGiftCardPaymentAllowed());
        verify(targetPlaceOrderFacade).reverseGiftCardPayment();
    }

    @Test
    public void testPlaceOrderInteralGiftCardReversalWhenGiftCardsPaymentIsNotAllowedAndPaymentModeIsGiftCard()
            throws CMSItemNotFoundException {
        mockInvalidGiftCardPayment();
        final String redirection = "redirect:/checkout/payment-details";
        final String redirectionReturned = controller.placeOrderInternal(model, redirectAttributes, request, true);
        verify(targetPlaceOrderFacade, Mockito.times(1)).reverseGiftCardPayment();
        assertEquals(redirection, redirectionReturned);
    }

    @Test
    public void testPlaceOrderInteralGiftCardReversalWhenGiftCardsPaymentIsAllowedAndPaymentModeIsGiftCard()
            throws CMSItemNotFoundException {
        final String redirectionReturned = controller.placeOrderInternal(model, redirectAttributes, request, true);
        verify(targetPlaceOrderFacade, Mockito.times(0)).reverseGiftCardPayment();
        verify(controller, Mockito.times(1)).handlePlaceOrder();
        assertTrue(redirectionReturned.contains(ControllerConstants.Redirection.CHECKOUT_THANK_YOU));
    }

    @Test
    public void testHandleExceptionWhenPlaceOrderWithIpgForAnonymousCart() {
        when(request.getParameter(ControllerConstants.IPG_PARAM_SESSIONID)).thenReturn("sessionId");
        when(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).thenReturn("token");
        when(Boolean.valueOf(userService.isAnonymousUser(Mockito.any(UserModel.class)))).thenReturn(Boolean.TRUE);
        final String returnView = controller.handleExceptionDuringCheckout(exception, request);
        assertEquals(returnView, ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE);
        verify(targetPlaceOrderFacade).reverseGiftCardPayment("token", "sessionId");
        verify(request).setAttribute("ipgRedirectUrl",
                ControllerConstants.THANK_YOU + ControllerConstants.THANK_YOU_CHECK + "?error=checkout");
    }

    @Test
    public void testHandleExceptionWhenPlaceOrderWithIpg() {
        when(request.getParameter(ControllerConstants.IPG_PARAM_SESSIONID)).thenReturn("sessionId");
        when(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).thenReturn("token");
        when(Boolean.valueOf(userService.isAnonymousUser(Mockito.any(UserModel.class)))).thenReturn(Boolean.FALSE);
        final String returnView = controller.handleExceptionDuringCheckout(exception, request);
        assertEquals(returnView, ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE);
        verify(targetPlaceOrderFacade).reverseGiftCardPayment("token", "sessionId");
        verify(request).setAttribute("ipgRedirectUrl",
                ControllerConstants.THANK_YOU + ControllerConstants.THANK_YOU_CHECK);
    }

    @Test
    public void testHandleExceptionWhenPlaceOrderWithoutIpgToken() {
        when(request.getParameter(ControllerConstants.IPG_PARAM_SESSIONID)).thenReturn(null);
        when(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).thenReturn(null);
        final String returnView = controller.handleExceptionDuringCheckout(exception, request);
        assertEquals(returnView, ControllerConstants.Redirection.CHECKOUT_THANK_YOU_CHECK);
        verifyZeroInteractions(targetPlaceOrderFacade);
    }

    /**
     * Method to mock gift card payment for invalid cart
     */
    private void mockInvalidGiftCardPayment() {
        when(Boolean.valueOf(checkoutFacade.isPaymentModeGiftCard())).thenReturn(Boolean.TRUE);
        when(Boolean.valueOf(checkoutFacade.isGiftCardPaymentAllowedForCart())).thenReturn(Boolean.FALSE);
        when(checkoutFacade.getCart()).thenReturn(cartModel);
        final List<TargetCardResult> results = new ArrayList<>();
        results.add(Mockito.mock(TargetCardResult.class));
        when(targetPlaceOrderFacade.getGiftCardPayments(cartModel)).thenReturn(results);
    }
}