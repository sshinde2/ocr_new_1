/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartHttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaDimensionQueryParamBuilder;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.BrandData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.SearchBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.kiosk.util.BulkyBoardHelper;
import au.com.target.tgtstorefront.controllers.pages.CategoryPageController.CategorySearchEvaluator;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.util.TargetEndecaSearchHelper;
import au.com.target.tgtutility.util.JsonConversionUtil;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


//import au.com.target.tgtsearch.constants.TgtsearchConstants;


/**
 * @author rmcalave
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryPageControllerTest {

    @Spy
    @InjectMocks
    private final CategoryPageController categoryPageController = new CategoryPageController();

    @Mock
    private TargetCMSPageService targetCMSPageService;

    @Mock
    private CommerceCategoryService commerceCategoryService;

    @Mock
    private UrlResolver<CategoryModel> categoryModelUrlResolver;

    @Mock
    private UrlResolver<BrandModel> brandModelUrlResolver;

    @Mock
    private BrandService brandService;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private EndecaDimensionQueryParamBuilder endecaDimensionQueryParamBuilder;

    @Mock
    private Converter<BrandModel, BrandData> targetBrandConverter;

    @Mock
    private TargetEndecaSearchHelper targetEndecaSearchHelper;

    @Mock
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Mock
    private BulkyBoardHelper bulkyBoardHelper;

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private Model mockModel;

    private String categoryCode;
    private String pageNoString;
    private String sortCode;
    private String viewAs;
    private String pageSize;
    private String navigationState;
    private Model model = null;
    private HttpServletRequest request = null;
    private HttpServletResponse response = null;
    private String brandCode;
    private String storeNumber;

    @Before
    public void setUp() {
        categoryCode = "testCategoryCode";
        pageNoString = "testPageNoString";
        sortCode = "testSortCode";
        viewAs = "testViewAs";
        pageSize = "testPageSize";
        navigationState = "testNavigationState";
        model = new RedirectAttributesModelMap();
        request = new MockMultipartHttpServletRequest();
        response = new MockHttpServletResponse();
        brandCode = "testBrandCode";
        storeNumber = "123456";
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCategoryWithRedirection() throws UnsupportedEncodingException {
        final CategoryModel category = new CategoryModel();
        given(commerceCategoryService.getCategoryForCode(categoryCode)).willReturn(category);
        given(categoryModelUrlResolver.resolve(category)).willReturn("resolvedcategoryUrl");
        final String result = categoryPageController.category(categoryCode, pageNoString, sortCode, viewAs, pageSize,
                navigationState,
                model, request, response);
        assertThat(result).isEqualTo("redirect:resolvedcategoryUrl");
    }

    @Test
    public void testCategoryDoCategory() throws UnsupportedEncodingException {
        final CategoryModel category = new CategoryModel();
        ((MockHttpServletRequest)request).setRequestURI("resolvedcategoryUrl");
        given(commerceCategoryService.getCategoryForCode(categoryCode)).willReturn(category);
        given(categoryModelUrlResolver.resolve(category)).willReturn("resolvedcategoryUrl");
        willReturn("call doCategory").given(categoryPageController)
                .doCategory(category, pageNoString, sortCode, viewAs, pageSize, navigationState,
                        model, null,
                        request, response);
        final String result = categoryPageController.category(categoryCode, pageNoString, sortCode, viewAs, pageSize,
                navigationState,
                model, request, response);
        assertThat(result).isEqualTo("call doCategory");
    }

    @Test
    public void testBrandWithRedirection() throws UnsupportedEncodingException {
        final CategoryModel category = new CategoryModel();
        final BrandModel brandModel = new BrandModel();
        brandModel.setName(brandCode);
        given(commerceCategoryService.getCategoryForCode(categoryCode)).willReturn(category);
        given(brandService.getBrandForFuzzyName(brandCode, false)).willReturn(brandModel);

        given(brandModelUrlResolver.resolve(brandModel)).willReturn("resolvedBrandUrl");
        final String result = categoryPageController.brand(brandCode, pageNoString, sortCode, viewAs, pageSize,
                navigationState,
                model, request, response);
        assertThat(result).isEqualTo("redirect:resolvedBrandUrl");
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testBrandWithExpection() throws UnsupportedEncodingException {
        final BrandModel brandModel = null;
        given(brandService.getBrandForFuzzyName(brandCode, false)).willReturn(brandModel);
        categoryPageController.brand(brandCode, pageNoString, sortCode, viewAs, pageSize,
                navigationState,
                model, request, response);
    }

    @Test
    public void testBrandDoBrand() throws UnsupportedEncodingException {
        final CategoryModel category = new CategoryModel();
        final BrandModel brandModel = new BrandModel();
        ((MockHttpServletRequest)request).setRequestURI("resolvedBrandUrl");
        brandModel.setName(brandCode);
        given(commerceCategoryService.getCategoryForCode(TgtCoreConstants.Category.ALL_PRODUCTS))
                .willReturn(category);
        given(brandService.getBrandForFuzzyName(brandCode, false)).willReturn(brandModel);

        given(brandModelUrlResolver.resolve(brandModel)).willReturn("resolvedBrandUrl");

        willReturn("call doBrand")
                .given(categoryPageController)
                .doBrand(category, brandModel, pageNoString, sortCode, viewAs, pageSize, navigationState, model,
                        request, response);
        final String result = categoryPageController.brand(brandCode, pageNoString, sortCode, viewAs, pageSize,
                navigationState,
                model, request, response);
        assertThat(result).isEqualTo("call doBrand");
    }

    @Test
    public void testDoCategoryWithCategoryModel() throws UnsupportedEncodingException, CMSItemNotFoundException {
        final CategoryModel category = new CategoryModel();

        category.setCode(categoryCode);
        storeNumber = null;

        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);

        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);

        final CategoryPageModel categoryPage = new CategoryPageModel();
        given(targetCMSPageService.getPageForCategory(category))
                .willReturn(categoryPage);

        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController)
                .doCategorySearch(categoryCode, null, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, categoryPage, storeNumber, request);

        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);

        doNothing().when(categoryPageController).populateCategoryData(model, category, categorySearch);
        doNothing().when(categoryPageController).storeContinueUrl(request);

        doNothing().when(categoryPageController)
                .populateBreadcrumbData(category, null, searchResults, searchPageData, model);

        given(categorySearch.getCmsPage()).willReturn(categoryPage);

        willReturn(Boolean.FALSE).given(categoryPageController)
                .isPageCacheable(any(PageTemplateModel.class));
        //categoryModel
        doNothing().when(categoryPageController).updatePageTitle(category, null, categoryPage, model,
                null);

        doNothing()
                .when(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));

        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));

        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, model);
        final String result = categoryPageController.doCategory(category, pageNoString, sortCode, viewAs, pageSize,
                navigationState, model,
                storeNumber, request, response);

        assertThat(result).isEqualTo("testViewPage");
        verify(targetEndecaSearchHelper)
                .handlePreferencesInitialData(request, response, categoryCode, navigationState, sortCode,
                        viewAs,
                        pageSize, StringUtils.EMPTY);
        verify(targetCMSPageService).getPageForCategory(category);
        verify(categoryPageController)
                .doCategorySearch(categoryCode, null, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, categoryPage, storeNumber, request);
        verify(categorySearch).getSearchPageData();
        verify(categoryPageController).populateCategoryData(model, category, categorySearch);
        verify(categoryPageController)
                .populateBreadcrumbData(category, null, searchResults, searchPageData, model);
        verify(categoryPageController).isPageCacheable(any(PageTemplateModel.class));
        verify(categoryPageController).updatePageTitle(category, null, categoryPage, model, null);
        verify(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));
        verify(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));
        verify(categoryPageController).populateActualViewAsType(viewPage, model);
    }

    @Test
    public void testDoCategoryWithTargetDealCategoryModel() throws UnsupportedEncodingException,
            CMSItemNotFoundException {
        final CategoryModel category = new TargetDealCategoryModel();

        category.setCode(categoryCode);
        storeNumber = null;

        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);

        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);

        final CategoryPageModel categoryPage = new CategoryPageModel();
        given(targetCMSPageService.getPageForCategory(category))
                .willReturn(categoryPage);

        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController)
                .doCategorySearch(categoryCode, null, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, categoryPage, storeNumber, request);

        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData =
                mock(TargetProductCategorySearchPageData.class);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);

        doNothing().when(categoryPageController).populateCategoryData(model, category, categorySearch);

        doNothing().when(categoryPageController).storeContinueUrl(request);

        doNothing().when(categoryPageController)
                .populateBreadcrumbData(category, null, searchResults, searchPageData, model);

        given(categorySearch.getCmsPage()).willReturn(categoryPage);

        willReturn(Boolean.FALSE).given(categoryPageController)
                .isPageCacheable(any(PageTemplateModel.class));
        //TargetDealcategoryModel
        doNothing().when(categoryPageController)
                .updateDealCategoryPageTitle((TargetDealCategoryModel)category, model);
        doNothing().when(categoryPageController).storeMetaRobotsNoIndexNoFollow(model);

        doNothing()
                .when(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));

        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));

        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, model);
        final String result = categoryPageController.doCategory(category, pageNoString, sortCode, viewAs, pageSize,
                navigationState, model,
                storeNumber, request, response);

        assertThat(result).isEqualTo("testViewPage");
        verify(targetEndecaSearchHelper)
                .handlePreferencesInitialData(request, response, categoryCode, navigationState, sortCode,
                        viewAs,
                        pageSize, StringUtils.EMPTY);
        verify(targetCMSPageService).getPageForCategory(category);
        verify(categoryPageController)
                .doCategorySearch(categoryCode, null, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, categoryPage, storeNumber, request);
        verify(categorySearch).getSearchPageData();
        verify(categoryPageController).populateCategoryData(model, category, categorySearch);
        verify(categoryPageController)
                .populateBreadcrumbData(category, null, searchResults, searchPageData, model);
        verify(categoryPageController).isPageCacheable(any(PageTemplateModel.class));
        verify(categoryPageController).updateDealCategoryPageTitle((TargetDealCategoryModel)category, model);
        verify(categoryPageController).storeMetaRobotsNoIndexNoFollow(model);
        verify(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));
        verify(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));
        verify(categoryPageController).populateActualViewAsType(viewPage, model);
    }

    @Test
    public void testPopulateReactDataForDoCategoryReactListingFeatureSwitchOff() throws UnsupportedEncodingException,
            CMSItemNotFoundException {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(TargetProductCategorySearchPageData.class);
        final CategoryModel category = new CategoryModel();
        category.setCode(categoryCode);
        storeNumber = null;
        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);
        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);
        final CategoryPageModel categoryPage = new CategoryPageModel();
        given(targetCMSPageService.getPageForCategory(category)).willReturn(categoryPage);
        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController).doCategorySearch(categoryCode, null, pageNoString,
                seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                seachEvaluatorData.getViewAsFn(), navigationState, categoryPage, storeNumber, request);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);
        doNothing().when(categoryPageController).populateCategoryData(mockModel, category, categorySearch);
        doNothing().when(categoryPageController).storeContinueUrl(request);
        doNothing().when(categoryPageController).populateBreadcrumbData(category, null, searchResults, searchPageData,
                mockModel);
        given(categorySearch.getCmsPage()).willReturn(categoryPage);
        willReturn(Boolean.FALSE).given(categoryPageController).isPageCacheable(any(PageTemplateModel.class));
        doNothing().when(categoryPageController).updatePageTitle(category, null, categoryPage, mockModel, null);
        doNothing().when(categoryPageController).setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                any(Model.class));
        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController).getViewPage(anyString(), any(AbstractPageModel.class));
        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, mockModel);

        final List<ProductData> products = null;
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isReactListingEnabled();
        given(searchPageData.getResults()).willReturn(products);

        categoryPageController.doCategory(category, pageNoString, sortCode, viewAs, pageSize,
                navigationState, mockModel, storeNumber, request, response);

        verify(mockModel, never()).addAttribute(eq("productList"), anyString());
    }

    @Test
    public void testPopulateReactDataForDoCategoryReactListingFeatureSwitchOn() throws UnsupportedEncodingException,
            CMSItemNotFoundException {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(TargetProductCategorySearchPageData.class);
        final CategoryModel category = new CategoryModel();
        category.setCode(categoryCode);
        storeNumber = null;
        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);
        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);
        final CategoryPageModel categoryPage = new CategoryPageModel();
        given(targetCMSPageService.getPageForCategory(category)).willReturn(categoryPage);
        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController).doCategorySearch(categoryCode, null, pageNoString,
                seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                seachEvaluatorData.getViewAsFn(), navigationState, categoryPage, storeNumber, request);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);
        doNothing().when(categoryPageController).populateCategoryData(mockModel, category, categorySearch);
        doNothing().when(categoryPageController).storeContinueUrl(request);
        doNothing().when(categoryPageController).populateBreadcrumbData(category, null, searchResults, searchPageData,
                mockModel);
        given(categorySearch.getCmsPage()).willReturn(categoryPage);
        willReturn(Boolean.FALSE).given(categoryPageController).isPageCacheable(any(PageTemplateModel.class));
        doNothing().when(categoryPageController).updatePageTitle(category, null, categoryPage, mockModel, null);
        doNothing().when(categoryPageController).setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                any(Model.class));
        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController).getViewPage(anyString(), any(AbstractPageModel.class));
        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, mockModel);

        final ProductData productData1 = new ProductData();
        productData1.setCode("PRODUCT_CODE1");
        final ProductData productData2 = new ProductData();
        productData2.setCode("PRODUCT_CODE2");
        final ProductData productData3 = new ProductData();
        productData3.setCode("PRODUCT_CODE3");
        final ProductData productData4 = new ProductData();
        productData4.setCode("PRODUCT_CODE4");
        final ProductData productData5 = new ProductData();
        productData5.setCode("PRODUCT_CODE5");
        final List<ProductData> products = new ArrayList<>();
        products.add(productData1);
        products.add(productData2);
        products.add(productData3);
        products.add(productData4);
        products.add(productData5);
        final Map<String, ProductData> productsDataMapExpected = new LinkedHashMap<>();
        for (final ProductData product : products) {
            productsDataMapExpected.put(product.getCode(), product);
        }
        final String productsDataJsonExpected = JsonConversionUtil.convertToJsonString(productsDataMapExpected);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isReactListingEnabled();
        given(searchPageData.getResults()).willReturn(products);

        categoryPageController.doCategory(category, pageNoString, sortCode, viewAs, pageSize,
                navigationState, mockModel, storeNumber, request, response);

        final ArgumentCaptor<String> productsDataJsonActual = ArgumentCaptor.forClass(String.class);
        verify(mockModel).addAttribute(eq("productList"), productsDataJsonActual.capture());
        assertThat(productsDataJsonActual.getValue()).isEqualTo(productsDataJsonExpected);
    }

    @Test
    public void testPopulateReactDataForDoBrandReactListingFeatureSwitchOn() throws UnsupportedEncodingException, CMSItemNotFoundException {
        final CategoryModel category = new CategoryModel();
        final BrandModel brandModel = new BrandModel();
        category.setCode(categoryCode);
        storeNumber = null;

        final BrandData brandData = new BrandData();
        given(targetBrandConverter.convert(brandModel)).willReturn(brandData);

        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);

        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);

        final AbstractPageModel cmsPage = new BrandPageModel();
        willReturn(cmsPage).given(categoryPageController).getBrandPage(brandModel);

        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController)
                .doCategorySearch(categoryCode, brandData, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, cmsPage, null, request);

        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData =
                mock(TargetProductCategorySearchPageData.class);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);

        doNothing().when(categoryPageController).populateBrandData(mockModel, brandModel, categorySearch);
        doNothing().when(categoryPageController).storeContinueUrl(request);

        doNothing().when(categoryPageController)
                .populateBreadcrumbData(category, brandModel, searchResults, searchPageData, mockModel);

        given(categorySearch.getCmsPage()).willReturn(cmsPage);

        willReturn(Boolean.FALSE).given(categoryPageController)
                .isPageCacheable(any(PageTemplateModel.class));
        //categoryModel
        doNothing().when(categoryPageController).updatePageTitle(category, brandModel, cmsPage, mockModel,
                null);

        doNothing()
                .when(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));

        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));

        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, mockModel);

        final ProductData productData1 = new ProductData();
        productData1.setCode("PRODUCT_CODE1");
        final ProductData productData2 = new ProductData();
        productData2.setCode("PRODUCT_CODE2");
        final ProductData productData3 = new ProductData();
        productData3.setCode("PRODUCT_CODE3");
        final ProductData productData4 = new ProductData();
        productData4.setCode("PRODUCT_CODE4");
        final ProductData productData5 = new ProductData();
        productData5.setCode("PRODUCT_CODE5");
        final List<ProductData> products = new ArrayList<>();
        products.add(productData1);
        products.add(productData2);
        products.add(productData3);
        products.add(productData4);
        products.add(productData5);
        final Map<String, ProductData> productsDataMapExpected = new LinkedHashMap<>();
        for (final ProductData product : products) {
            productsDataMapExpected.put(product.getCode(), product);
        }
        final String productsDataJsonExpected = JsonConversionUtil.convertToJsonString(productsDataMapExpected);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isReactListingEnabled();
        given(searchPageData.getResults()).willReturn(products);

        categoryPageController.doBrand(category, brandModel, pageNoString, sortCode, viewAs,
                pageSize,
                navigationState, mockModel, request, response);

        final ArgumentCaptor<String> productsDataJsonActual = ArgumentCaptor.forClass(String.class);
        verify(mockModel).addAttribute(eq("productList"), productsDataJsonActual.capture());
        assertThat(productsDataJsonActual.getValue()).isEqualTo(productsDataJsonExpected);
    }

    @Test
    public void testPopulateReactDataForDoBrandReactListingFeatureSwitchOff() throws UnsupportedEncodingException, CMSItemNotFoundException {
        final CategoryModel category = new CategoryModel();
        final BrandModel brandModel = new BrandModel();
        category.setCode(categoryCode);
        storeNumber = null;

        final BrandData brandData = new BrandData();
        given(targetBrandConverter.convert(brandModel)).willReturn(brandData);

        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);

        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);

        final AbstractPageModel cmsPage = new BrandPageModel();
        willReturn(cmsPage).given(categoryPageController).getBrandPage(brandModel);

        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController)
                .doCategorySearch(categoryCode, brandData, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, cmsPage, null, request);

        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData =
                mock(TargetProductCategorySearchPageData.class);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);

        doNothing().when(categoryPageController).populateBrandData(mockModel, brandModel, categorySearch);
        doNothing().when(categoryPageController).storeContinueUrl(request);

        doNothing().when(categoryPageController)
                .populateBreadcrumbData(category, brandModel, searchResults, searchPageData, mockModel);

        given(categorySearch.getCmsPage()).willReturn(cmsPage);

        willReturn(Boolean.FALSE).given(categoryPageController)
                .isPageCacheable(any(PageTemplateModel.class));
        //categoryModel
        doNothing().when(categoryPageController).updatePageTitle(category, brandModel, cmsPage, mockModel,
                null);

        doNothing()
                .when(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));

        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));

        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, mockModel);

        final List<ProductData> products = null;
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isReactListingEnabled();
        given(searchPageData.getResults()).willReturn(products);

        categoryPageController.doBrand(category, brandModel, pageNoString, sortCode, viewAs,
                pageSize,
                navigationState, mockModel, request, response);

        verify(mockModel, never()).addAttribute(eq("productList"), anyString());
    }

    @Test
    public void testDoBrand() throws UnsupportedEncodingException, CMSItemNotFoundException {
        final CategoryModel category = new CategoryModel();
        final BrandModel brandModel = new BrandModel();
        category.setCode(categoryCode);
        storeNumber = null;

        final BrandData brandData = new BrandData();
        given(targetBrandConverter.convert(brandModel)).willReturn(brandData);

        final SeachEvaluatorData seachEvaluatorData = new SeachEvaluatorData();
        seachEvaluatorData.setItemPerPageFn(10);
        seachEvaluatorData.setSortCodeFn(sortCode);
        seachEvaluatorData.setViewAsFn(viewAs);

        given(
                targetEndecaSearchHelper.handlePreferencesInitialData(request, response, categoryCode, navigationState,
                        sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);

        final AbstractPageModel cmsPage = new BrandPageModel();
        willReturn(cmsPage).given(categoryPageController).getBrandPage(brandModel);

        final CategorySearchEvaluator categorySearch = mock(CategorySearchEvaluator.class);
        willReturn(categorySearch).given(categoryPageController)
                .doCategorySearch(categoryCode, brandData, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, cmsPage, null, request);

        final EndecaSearch searchResults = mock(EndecaSearch.class);
        given(categorySearch.getSearchResults()).willReturn(searchResults);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData =
                mock(TargetProductCategorySearchPageData.class);
        given(categorySearch.getSearchPageData()).willReturn(searchPageData);

        doNothing().when(categoryPageController).populateBrandData(model, brandModel, categorySearch);
        doNothing().when(categoryPageController).storeContinueUrl(request);

        doNothing().when(categoryPageController)
                .populateBreadcrumbData(category, brandModel, searchResults, searchPageData, model);

        given(categorySearch.getCmsPage()).willReturn(cmsPage);

        willReturn(Boolean.FALSE).given(categoryPageController)
                .isPageCacheable(any(PageTemplateModel.class));
        //categoryModel
        doNothing().when(categoryPageController).updatePageTitle(category, brandModel, cmsPage, model,
                null);

        doNothing()
                .when(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));

        final String viewPage = "testViewPage";
        willReturn(viewPage).given(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));

        doNothing().when(categoryPageController).populateActualViewAsType(viewPage, model);
        final String result = categoryPageController.doBrand(category, brandModel, pageNoString, sortCode, viewAs,
                pageSize,
                navigationState, model, request, response);

        assertThat(result).isEqualTo("testViewPage");
        verify(targetEndecaSearchHelper)
                .handlePreferencesInitialData(request, response, categoryCode, navigationState, sortCode,
                        viewAs,
                        pageSize, StringUtils.EMPTY);
        verify(categoryPageController).getBrandPage(brandModel);
        verify(categoryPageController)
                .doCategorySearch(categoryCode, brandData, pageNoString,
                        seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                        seachEvaluatorData.getViewAsFn(), navigationState, cmsPage, null, request);
        verify(categorySearch).getSearchPageData();
        verify(categoryPageController).populateBrandData(model, brandModel, categorySearch);
        verify(categoryPageController)
                .populateBreadcrumbData(category, brandModel, searchResults, searchPageData, model);
        verify(categoryPageController).isPageCacheable(any(PageTemplateModel.class));
        verify(categoryPageController).updatePageTitle(category, brandModel, cmsPage, model, null);
        verify(categoryPageController)
                .setUpMetaData(any(AbstractPageModel.class), any(CategoryModel.class),
                        any(Model.class));
        verify(categoryPageController)
                .getViewPage(anyString(), any(AbstractPageModel.class));
        verify(categoryPageController).populateActualViewAsType(viewPage, model);
    }

    @Test
    public void testPopulateBreadcrumbData() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        final BrandModel brand = mock(BrandModel.class);
        final CategoryModel category = mock(CategoryModel.class);
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<>();
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        breadcrumbs.add(mock(Breadcrumb.class));
        doNothing().when(categoryPageController).populateModel(model, searchPageData);
        doNothing().when(categoryPageController).populateCanonicalData(model, searchPageData);
        given(searchPageData.getFreeTextSearch()).willReturn("freeTextSearch");
        given(searchPageData.getBreadcrumbs()).willReturn(refinements);
        given(searchBreadcrumbBuilder.getBreadcrumbs(category, brand,
                refinements, "freeTextSearch", searchResults)).willReturn(breadcrumbs);
        categoryPageController.populateBreadcrumbData(category,
                brand, searchResults, searchPageData, model);
        verify(targetEndecaSearchHelper).populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
    }

    @Test
    public void testFacetAttributeForPageTitleWhenMultipleFacetSelected() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();
        final EndecaDimension facetSize = new EndecaDimension();

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        colorBlack.setSelected(true);
        final EndecaDimension colorBlue = new EndecaDimension();
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        final List<EndecaDimension> sizeRefinment = new ArrayList<>();
        final EndecaDimension sizeMedium = new EndecaDimension();
        sizeMedium.setSelected(true);
        final EndecaDimension sizeLarge = new EndecaDimension();
        sizeRefinment.add(sizeLarge);
        sizeRefinment.add(sizeMedium);
        facetSize.setRefinement(sizeRefinment);

        facets.add(facetColor);
        facets.add(facetSize);

        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNull(result);
    }

    @Test
    public void testFacetAttributeForPageTitleWhenOneFacetWithMultipleAttribute() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();
        facetColor.setActualFollowup(true);

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        colorBlack.setSelected(true);
        final EndecaDimension colorBlue = new EndecaDimension();
        colorBlue.setSelected(true);
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        facets.add(facetColor);

        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNull(result);
    }

    @Test
    public void testFacetAttributeForPageTitleWhenOneFacetWithOneAttributeButNoFollowUp() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        colorBlack.setSelected(true);
        final EndecaDimension colorBlue = new EndecaDimension();
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        facets.add(facetColor);

        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNull(result);
    }

    @Test
    public void testFacetAttributeForPageTitleWhenOneFacetWithOneAttributeWithFollowUp() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();
        facetColor.setActualFollowup(true);

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        colorBlack.setSelected(true);
        colorBlack.setName("Black");
        final EndecaDimension colorBlue = new EndecaDimension();
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        facets.add(facetColor);

        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNotNull(result);
        assertThat(result).isEqualTo("Black");
    }

    @Test
    public void testFacetAttributeForPageTitleWhenNoFacetSelected() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();
        final EndecaDimension facetSize = new EndecaDimension();

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        final EndecaDimension colorBlue = new EndecaDimension();
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        final List<EndecaDimension> sizeRefinment = new ArrayList<>();
        final EndecaDimension sizeMedium = new EndecaDimension();
        final EndecaDimension sizeLarge = new EndecaDimension();
        sizeRefinment.add(sizeLarge);
        sizeRefinment.add(sizeMedium);
        facetSize.setRefinement(sizeRefinment);
        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNull(result);
    }

    @Test
    public void testFacetAttributeForPageTitleWhenFacetHaveSubFacets() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();
        facetColor.setActualFollowup(true);

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        colorBlack.setSelected(true);
        colorBlack.setName("0001**Womens Petite**00004**10P");
        final EndecaDimension colorBlue = new EndecaDimension();
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        facets.add(facetColor);

        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNotNull(result);
        assertThat(result).isEqualTo("10P");

    }

    @Test
    public void testFacetAttributeForPageTitleForMenSizeFacet() {
        final List<EndecaDimension> facets = new ArrayList<>();
        final EndecaDimension facetColor = new EndecaDimension();
        facetColor.setActualFollowup(true);

        final List<EndecaDimension> colorRefinement = new ArrayList<>();
        final EndecaDimension colorBlack = new EndecaDimension();
        colorBlack.setSelected(true);
        colorBlack.setName("0005**Mens Apparel**00302**77cm - 30in");
        final EndecaDimension colorBlue = new EndecaDimension();
        colorRefinement.add(colorBlue);
        colorRefinement.add(colorBlack);
        facetColor.setRefinement(colorRefinement);

        facets.add(facetColor);

        final String result = categoryPageController.facetAttributeForPageTitle(facets);
        assertNotNull(result);
        assertThat(result).isEqualTo("77cm - 30in");
    }
}
