/**
 * 
 */
package au.com.target.tgtstorefront.util;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;


/**
 * @author rmcalave
 * 
 */
public class NavigationNodeUtilsTest {
    @Test
    public void testGetNavigationNodeTitleWithTitle() {
        final String title = "Test Title";

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(title);

        final String returnedTitle = NavigationNodeUtils.getNavigationNodeTitle(mockNavigationNode, true);

        Assert.assertEquals(title, returnedTitle);
    }

    @Test
    public void testGetNavigationNodeTitleWithoutTitleWithNameWithFallback() {
        final String name = "Test Name";

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getName()).willReturn(name);

        final String returnedTitle = NavigationNodeUtils.getNavigationNodeTitle(mockNavigationNode, true);

        Assert.assertEquals(name, returnedTitle);
    }

    @Test
    public void testGetNavigationNodeTitleWithoutTitleWithNameWithNoFallback() {
        final String name = "Test Name";

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getName()).willReturn(name);

        final String returnedTitle = NavigationNodeUtils.getNavigationNodeTitle(mockNavigationNode, false);

        Assert.assertNull(returnedTitle);
    }

    @Test
    public void testGetNavigationNodeTitleWithoutTitleWithoutNameWithFallback() {
        final String name = "CMSNavigationNode"; // Default name when name isn't set, should be filtered out.

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getName()).willReturn(name);

        final String returnedTitle = NavigationNodeUtils.getNavigationNodeTitle(mockNavigationNode, true);

        Assert.assertEquals("", returnedTitle);
    }
}
