/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.navigation.SiteMapMenuBuilder;
import au.com.target.tgtwebcore.model.cms2.pages.SizePageModel;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;
import junit.framework.Assert;



/**
 * @author umesh
 * 
 */
@UnitTest
public class SizePageControllerTest {
    @InjectMocks
    private final SizePageController sizePageController = new SizePageController();

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private CMSNavigationService cmsNavigationService;

    @Mock
    private SiteMapMenuBuilder siteMapMenuBuilder;

    @Mock
    private TargetSizeTypeService targetSizeTypeService;

    @Mock
    private TargetCMSPageService targetCMSPageService;

    private Model model;

    @Mock
    private SizeTypeModel sizeType;

    @Mock
    private SizePageModel sizePageModel;

    @Mock
    private CMSNavigationNodeModel navModel;

    @Mock
    private NavigationMenuItem structure;

    @Mock
    private CMSItemNotFoundException cmsItemNotFoundException;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        model = new BindingAwareModelMap();
        sizePageController.setCmsPageService(cmsPageService);
        sizePageController.setSiteMapMenuBuilder(siteMapMenuBuilder);
        sizePageController.setTargetCMSPageService(targetCMSPageService);
        sizePageController.setTargetSizeTypeService(targetSizeTypeService);
    }

    @Test
    public void testSizePageWithInvalidSizeTypeCode() throws CMSItemNotFoundException {
        BDDMockito.given(targetSizeTypeService.findSizeTypeForCode(Mockito.anyString())).willReturn(null);
        BDDMockito.given(targetSizeTypeService.getDefaultSizeType()).willReturn(null);
        sizePageController.getViewForPage(Mockito.anyString(), model);
        Assert.assertNull(model.asMap().get("pageTitle"));
    }

    @Test
    public void testSizePagewithValidSizeTypeCode() throws CMSItemNotFoundException {
        BDDMockito.given(targetSizeTypeService.findSizeTypeForCode(Mockito.anyString())).willReturn(sizeType);
        BDDMockito.given(sizeType.getCmsNavigationNode()).willReturn(navModel);
        BDDMockito.given(siteMapMenuBuilder.createMenuItem(navModel)).willReturn(structure);
        sizePageController.getViewForPage(Mockito.anyString(), model);
        Assert.assertEquals(structure, model.asMap().get("navigation"));
    }

    @Test
    public void testDefaultSizePageWithInvalidSizeTypeCode() throws CMSItemNotFoundException {
        BDDMockito.given(targetSizeTypeService.findSizeTypeForCode(Mockito.anyString())).willReturn(null);
        BDDMockito.given(targetSizeTypeService.getDefaultSizeType()).willReturn(sizeType);
        BDDMockito.given(sizeType.getCmsNavigationNode()).willReturn(navModel);
        BDDMockito.given(siteMapMenuBuilder.createMenuItem(navModel)).willReturn(structure);
        BDDMockito.given(targetCMSPageService.getPageForSize(sizeType)).willReturn(sizePageModel);
        sizePageController.getViewForPage(Mockito.anyString(), model);
        Assert.assertEquals(structure, model.asMap().get("navigation"));
        Assert.assertNotNull(model.asMap().get("cmsPage"));
    }

    @Test
    public void testSizePageWithEmptyNavigationNode() throws CMSItemNotFoundException {
        BDDMockito.given(targetSizeTypeService.findSizeTypeForCode(Mockito.anyString())).willReturn(sizeType);
        BDDMockito.given(sizeType.getCmsNavigationNode()).willReturn(null);
        sizePageController.getViewForPage(Mockito.anyString(), model);
        Assert.assertNull(model.asMap().get("navigation"));
    }

    @Test
    public void testSizePageWithEmptyNavigationMenuItem() throws CMSItemNotFoundException {
        BDDMockito.given(targetSizeTypeService.findSizeTypeForCode(Mockito.anyString())).willReturn(sizeType);
        BDDMockito.given(sizeType.getCmsNavigationNode()).willReturn(navModel);
        BDDMockito.given(siteMapMenuBuilder.createMenuItem(navModel)).willReturn(null);
        sizePageController.getViewForPage(Mockito.anyString(), model);
        Assert.assertNull(model.asMap().get("navigation"));
    }
}
