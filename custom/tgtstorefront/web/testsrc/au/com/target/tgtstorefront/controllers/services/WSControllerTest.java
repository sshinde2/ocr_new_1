/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.web.csrf.CsrfToken;

import au.com.target.tgtfacades.config.CheckoutConfigurationFacadeImpl;
import au.com.target.tgtfacades.order.data.SessionData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.SessionResponseData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Unit tests for WSController
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WSControllerTest {

    @Mock
    private CheckoutConfigurationFacadeImpl checkoutConfigurationFacade;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @InjectMocks
    private final WSController controller = new WSController();

    @Before
    public void setup() {
        given(request.getSession()).willReturn(session);
    }

    @Test
    public void verifyGetEnv() {
        controller.getEnv();
        verify(checkoutConfigurationFacade).getCheckoutEnvConfigs();
    }

    @Test
    public void verifyGetSessionInformation() {
        controller.getSessionInformation(request);
        verify(checkoutConfigurationFacade).getSessionInformation(null);
    }

    @Test
    public void getSessionInformationResponseDataNotNull() {
        given(checkoutConfigurationFacade.getSessionInformation(null)).willReturn(new SessionData());
        final Response response = controller.getSessionInformation(request);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
    }

    @Test
    public void getSessionInformationResponseDataNull() {
        final Response response = controller.getSessionInformation(request);
        assertThat(response.isSuccess()).isTrue();
        assertThat(((SessionResponseData)response.getData()).getSession()).isNull();
    }

    @Test
    public void getSessionInformationHasCSRFToken() {
        final CsrfToken csrfToken = mock(CsrfToken.class);
        given(csrfToken.getToken()).willReturn("token");
        given(checkoutConfigurationFacade.getSessionInformation(null)).willReturn(new SessionData());
        given(request.getAttribute(CsrfToken.class.getName())).willReturn(csrfToken);
        final Response response = controller.getSessionInformation(request);
        assertThat(response.isSuccess()).isTrue();
        assertThat(((SessionResponseData)response.getData()).getSession()).isNotNull();
        assertThat(((SessionResponseData)response.getData()).getSession().getCsrfToken()).isNotEmpty();
    }

    @Test
    public void getSessionInformationAfterPlaceOrder() {
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("1234");
        final SessionData sessionData = mock(SessionData.class);
        given(checkoutConfigurationFacade.getSessionInformation("1234")).willReturn(sessionData);
        final Response response = controller.getSessionInformation(request);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        assertThat(((SessionResponseData)response.getData()).getSession()).isEqualTo(sessionData);
    }
}
