/**
 * 
 */
package au.com.target.tgtstorefront.controllers.stock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.product.stock.StockAvailabilitiesData;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StockLookUpAsServiceControllerTest {

    @Mock
    private TargetStockLookUpFacade targetStockLookupFacade;

    @InjectMocks
    private final StockLookUpAsServiceController stockLookUpAsServiceController = new StockLookUpAsServiceController();

    @Test
    public void testStockForRequest() {
        final StockAvailabilitiesData stockAvailabilitiesData = Mockito.mock(StockAvailabilitiesData.class);
        final StockAvailabilitiesData stockAvailabilitiesDataResult = Mockito.mock(StockAvailabilitiesData.class);
        BDDMockito.given(targetStockLookupFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .willReturn(stockAvailabilitiesDataResult);
        Assertions.assertThat(stockLookUpAsServiceController.getStockForRequest(stockAvailabilitiesData, "ebay"))
                .isEqualTo(stockAvailabilitiesDataResult);

    }

}
