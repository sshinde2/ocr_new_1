/**
 * 
 */
package au.com.target.tgtstorefront.web.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;


/**
 * @author htan3
 *
 */
@UnitTest
public class TargetMappingJacksonHttpMessageConverterTest {

    private final TargetMappingJacksonHttpMessageConverter converter = new TargetMappingJacksonHttpMessageConverter();


    @Test
    public void testSetMixinsWithNull() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("java.lang.Object", "java.lang.String");
        converter.setMixins(map);
        converter.setMixins(null);
        assertNull(converter.getObjectMapper().getSerializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithWrongClass() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("target", "mixin");
        converter.setMixins(map);
        assertNull(converter.getObjectMapper().getSerializationConfig().findMixInClassFor(Object.class));
    }

    @Test
    public void testSetMixinsWithGoodClass() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("java.lang.Object", "java.lang.String");
        converter.setMixins(map);
        assertEquals(String.class, converter.getObjectMapper().getSerializationConfig().findMixInClassFor(Object.class));
    }
}
