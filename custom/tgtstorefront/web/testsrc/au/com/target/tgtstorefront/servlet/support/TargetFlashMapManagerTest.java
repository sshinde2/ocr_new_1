package au.com.target.tgtstorefront.servlet.support;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.util.UrlPathHelper;


@RunWith(MockitoJUnitRunner.class)
public class TargetFlashMapManagerTest {

    @InjectMocks
    private final TargetFlashMapManager targetFlashMapManager = new TargetFlashMapManager();

    @Mock
    private UrlPathHelper mockUrlPathHelper;

    @Mock
    private HttpServletRequest mockRequest;

    @Test
    public void testIsFlashMapForRequestNoPathNoParams() throws Exception {
        final FlashMap flashMap = new FlashMap();

        Assert.assertTrue(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestNoPathNoMatchingParams() throws Exception {
        final String targetRequestParamKey = "leftKey";
        final String targetRequestParamValue = "leftKey";

        final String requestParamKey = "rightKey";
        final String requestParamValue = "rightValue";

        BDDMockito.given(mockRequest.getParameterValues(requestParamKey))
                .willReturn(new String[] { requestParamValue });

        final FlashMap flashMap = new FlashMap();
        flashMap.addTargetRequestParam(targetRequestParamKey, targetRequestParamValue);

        Assert.assertFalse(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestNoPathMatchingParams() throws Exception {
        final String requestParamKey = "key";
        final String requestParamValue = "value";

        BDDMockito.given(mockRequest.getParameterValues(requestParamKey))
                .willReturn(new String[] { requestParamValue });

        final FlashMap flashMap = new FlashMap();
        flashMap.addTargetRequestParam(requestParamKey, requestParamValue);

        Assert.assertTrue(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestNullPathParameterMappingsNoPathMatch() throws Exception {
        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/my-account");

        Assert.assertFalse(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestNoPathParameterMappingsNoPathMatch() throws Exception {
        final Map<String, String> pathParameterMappings = new HashMap<String, String>();
        targetFlashMapManager.setPathParameterMappings(pathParameterMappings);

        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/my-account");

        Assert.assertFalse(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestWithPathParameterMappingsNoPathMatch() throws Exception {
        final Map<String, String> pathParameterMappings = new HashMap<String, String>();
        pathParameterMappings.put("/customer/getinfo", "p");
        targetFlashMapManager.setPathParameterMappings(pathParameterMappings);

        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/my-account");

        Assert.assertFalse(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestWithPathParameterMappingsWithQsPathMatchNoQsParams() throws Exception {
        final Map<String, String> pathParameterMappings = new HashMap<String, String>();
        pathParameterMappings.put("/customer/getinfo", "p");
        targetFlashMapManager.setPathParameterMappings(pathParameterMappings);

        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/customer/getinfo");

        Assert.assertFalse(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestWithPathParameterMappingsWithQsPathMatchNoMatchingQsParams() throws Exception {
        final Map<String, String> pathParameterMappings = new HashMap<String, String>();
        pathParameterMappings.put("/customer/getinfo", "p");
        targetFlashMapManager.setPathParameterMappings(pathParameterMappings);

        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/customer/getinfo");
        BDDMockito.given(mockUrlPathHelper.getOriginatingQueryString(mockRequest)).willReturn("p=%2Fmy-account");
        BDDMockito.given(mockUrlPathHelper.decodeRequestString(mockRequest, "p=%2Fmy-account")).willReturn(
                "p=/my-account");

        Assert.assertFalse(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestWithPathParameterMappingsWithQsPathMatchMatchingQsParams() throws Exception {
        final Map<String, String> pathParameterMappings = new HashMap<String, String>();
        pathParameterMappings.put("/customer/getinfo", "p");
        targetFlashMapManager.setPathParameterMappings(pathParameterMappings);

        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/customer/getinfo");
        BDDMockito.given(mockUrlPathHelper.getOriginatingQueryString(mockRequest)).willReturn("p=%2F");
        BDDMockito.given(mockUrlPathHelper.decodeRequestString(mockRequest, "p=%2F")).willReturn("p=/");

        Assert.assertTrue(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestPathMatch() throws Exception {
        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/my-account");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/my-account");

        Assert.assertTrue(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }

    @Test
    public void testIsFlashMapForRequestAlternatePathMatch() throws Exception {
        final FlashMap flashMap = new FlashMap();
        flashMap.setTargetRequestPath("/my-account");

        BDDMockito.given(mockUrlPathHelper.getOriginatingRequestUri(mockRequest)).willReturn("/my-account/");

        Assert.assertTrue(targetFlashMapManager.isFlashMapForRequest(flashMap, mockRequest));
    }
}
