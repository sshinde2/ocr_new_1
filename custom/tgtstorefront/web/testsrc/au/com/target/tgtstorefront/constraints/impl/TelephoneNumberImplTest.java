/**
 * 
 */
package au.com.target.tgtstorefront.constraints.impl;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtstorefront.constraints.TelephoneNumber;


/**
 * @author rmcalave
 * 
 */
public class TelephoneNumberImplTest {
    private final TelephoneNumberImpl telephoneNumberConstraint = new TelephoneNumberImpl();

    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        final TelephoneNumber mockTelephoneNumber = Mockito.mock(TelephoneNumber.class);
        BDDMockito.given(mockTelephoneNumber.minDigits()).willReturn(Integer.valueOf(7));
        BDDMockito.given(mockTelephoneNumber.maxDigits()).willReturn(Integer.valueOf(16));

        telephoneNumberConstraint.initialize(mockTelephoneNumber);
    }

    @Test
    public void testIsValidWithNullValue() {
        final boolean result = telephoneNumberConstraint.isValid(null, null);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsValidWithValidValue() {
        final boolean result = telephoneNumberConstraint.isValid("+61(3)91234567", null);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsValidWithTooManyDigits() {
        final boolean result = telephoneNumberConstraint.isValid("+61(3)91234567890123", null);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsValidWithNotEnoughtDigits() {
        final boolean result = telephoneNumberConstraint.isValid("+61(3)912", null);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsValidWithInvalidCharacters() {
        final boolean result = telephoneNumberConstraint.isValid("+61(3)91%&#*@2", null);

        Assert.assertFalse(result);
    }
}
