/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import au.com.target.endeca.infront.dto.CustomerSubscriptionInfo;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.ENewsSubscriptionForm;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;



/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ENewsSubscriptionPageControllerTest {

    @InjectMocks
    private final ENewsSubscriptionPageController eNewsController = new ENewsSubscriptionPageController();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private MessageSource messageSource;

    @Mock
    private I18NService i18nService;

    @Mock
    private Model model;

    @Mock
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Mock
    private EnhancedCookieGenerator enewsCookieGenerator;

    @Mock
    private TargetCustomerSubscriptionResponseDto result;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private RedirectAttributes redirectAttributes;

    private final CustomerSubscriptionInfo customerSubscriptionInfo = new CustomerSubscriptionInfo();

    @Test
    public void testENewsQuickWithInvalidEmail() throws Exception {
        customerSubscriptionInfo.setEmail("invalid.email");
        customerSubscriptionInfo.setSubscriptionSource("newsletter");
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "error");
    }


    @Test
    public void testENewsQuickWithEmptyEmail() throws Exception {
        customerSubscriptionInfo.setEmail("");
        customerSubscriptionInfo.setSubscriptionSource("newsletter");
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "error");
    }

    @Test
    public void testENewsQuickWithValidEmailAddress() throws Exception {
        customerSubscriptionInfo.setEmail("test@test.com");
        customerSubscriptionInfo.setSubscriptionSource("newsletter");
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "success");
    }

    @Test
    public void testENewsQuickWithExistingSubscription() throws Exception {
        customerSubscriptionInfo.setEmail("test@test.com");
        customerSubscriptionInfo.setSubscriptionSource("newsletter");
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "success");
    }

    @Test
    public void testENewsQuickWithNullSubscritionSource() throws Exception {
        customerSubscriptionInfo.setEmail("test@test.com");
        customerSubscriptionInfo.setSubscriptionSource(null);
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "success");
    }

    @Test
    public void testENewsQuickWithEmptySubscritionSource() throws Exception {
        customerSubscriptionInfo.setEmail("test@test.com");
        customerSubscriptionInfo.setSubscriptionSource("");
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "success");
    }

    @Test
    public void testENewsQuickWithValidSubscritionSource() throws Exception {
        customerSubscriptionInfo.setEmail("test@test.com");
        customerSubscriptionInfo.setSubscriptionSource("newsletter");
        eNewsController.enewsQuick(model, customerSubscriptionInfo, response);
        verify(model).addAttribute("subscriptionType", "Newsletter");
        verify(model).addAttribute("messageType", "success");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testENewsFormWithValidEmailAddress() throws Exception {
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail("test@gmail.com");
        when(bindingResult.hasErrors()).thenReturn(Boolean.FALSE);
        final String redirect = eNewsController.doSubscribe(eNewsSubscriptionForm, bindingResult, model,
                response,
                redirectAttributes);

        assertEquals(ControllerConstants.Redirection.ENEWS_SUBSCRIPTION, redirect);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testENewsFormWithEmptySubscriptionSource() throws Exception {
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail("test@gmail.com");
        eNewsSubscriptionForm.setSubscriptionSource("");
        when(bindingResult.hasErrors()).thenReturn(Boolean.FALSE);
        final String redirect = eNewsController.doSubscribe(eNewsSubscriptionForm, bindingResult, model,
                response,
                redirectAttributes);

        assertEquals(ControllerConstants.Redirection.ENEWS_SUBSCRIPTION, redirect);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testENewsFormWithNullSubscriptionSource() throws Exception {
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail("test@gmail.com");
        eNewsSubscriptionForm.setSubscriptionSource(null);
        when(bindingResult.hasErrors()).thenReturn(Boolean.FALSE);
        final String redirect = eNewsController.doSubscribe(eNewsSubscriptionForm, bindingResult, model,
                response,
                redirectAttributes);

        assertEquals(ControllerConstants.Redirection.ENEWS_SUBSCRIPTION, redirect);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testENewsFormWithValidSubscriptionSource() throws Exception {
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail("test@gmail.com");
        eNewsSubscriptionForm.setSubscriptionSource("checkout");
        when(bindingResult.hasErrors()).thenReturn(Boolean.FALSE);
        final String redirect = eNewsController.doSubscribe(eNewsSubscriptionForm, bindingResult, model,
                response,
                redirectAttributes);

        assertEquals(ControllerConstants.Redirection.ENEWS_SUBSCRIPTION, redirect);
    }

    @Test
    public void testGetEnewsRedirectWithValidAccredit() throws Exception {

        final RedirectView redirect = eNewsController.getRedirectEnews(model, "value");
        assertEquals(ControllerConstants.ENEWS_SUBSCRIPTION + ControllerConstants.ACCREDIT_URL + "value",
                redirect.getUrl());
    }

    @Test
    public void testGetEnewsRedirectWithNullAccredit() throws Exception {

        final RedirectView redirect = eNewsController.getRedirectEnews(model, "");
        assertEquals(ControllerConstants.ENEWS_SUBSCRIPTION, redirect.getUrl());
    }

    @Test
    public void testGetEnewsRedirectWithEmptyAccredit() throws Exception {
        final RedirectView redirect = eNewsController.getRedirectEnews(model, null);
        assertEquals(ControllerConstants.ENEWS_SUBSCRIPTION, redirect.getUrl());
    }


}
