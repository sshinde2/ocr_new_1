/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.lang.annotation.Annotation;

import javax.validation.Payload;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtstorefront.forms.validation.ThreePartDate;


/**
 * Unit test for {@link ThreePartDateValidator}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ThreePartDateValidatorTest {
    private static final String MONTH = "3";
    private static final String YEAR = "2000";
    private static final String DAY = "31";

    @InjectMocks
    private final ThreePartDateValidator threePartDateValidator = new ThreePartDateValidator();

    @Test
    public void testNullDay() {
        assertThat(threePartDateValidator.isDateValid(null, MONTH, YEAR)).isFalse();
    }

    @Test
    public void testNullMonth() {
        assertThat(threePartDateValidator.isDateValid(DAY, null, YEAR)).isFalse();
    }

    @Test
    public void testNullYear() {
        assertThat(threePartDateValidator.isDateValid(DAY, MONTH, null)).isFalse();
    }

    @Test
    public void testNonStringDay() {
        assertThat(threePartDateValidator.isDateValid(new Integer(2), MONTH, YEAR)).isFalse();
    }

    @Test
    public void testNonStringMonth() {
        assertThat(threePartDateValidator.isDateValid(DAY, new Integer(2), YEAR)).isFalse();
    }

    @Test
    public void testNonStringYear() {
        assertThat(threePartDateValidator.isDateValid(DAY, MONTH, new Integer(2))).isFalse();
    }

    @Test
    public void testNonIntDay() {
        assertThat(threePartDateValidator.isDateValid("x", MONTH, YEAR)).isFalse();
    }

    @Test
    public void testNonIntMonth() {
        assertThat(threePartDateValidator.isDateValid(DAY, "x", YEAR)).isFalse();
    }

    @Test
    public void testNonIntYear() {
        assertThat(threePartDateValidator.isDateValid(DAY, MONTH, "x")).isFalse();
    }

    @Test
    public void testValidDate() {
        assertThat(threePartDateValidator.isDateValid(DAY, MONTH, YEAR)).isTrue();
    }

    @Test
    public void testInvalidDate() {
        assertThat(threePartDateValidator.isDateValid("31", "02", "2000")).isFalse();
    }

    @Test
    public void testnullDateWithMandatoryFalse() {
        final ThreePartDate constraintAnnotation = new ThreePartDate() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return null;
            }

            @Override
            public String[] value() {
                final String[] array = { "02", "07", "2008" };
                return array;
            }

            @Override
            public Class<? extends Payload>[] payload() {
                return null;
            }

            @Override
            public String message() {
                return null;
            }

            @Override
            public boolean mandatory() {
                return false;
            }

            @Override
            public Class<?>[] groups() {
                return null;
            }
        };
        threePartDateValidator.initialize(constraintAnnotation);
        assertThat(threePartDateValidator.isDateValid(null, null, null)).isTrue();
    }

}
