/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSComponentService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.type.ComposedTypeModel;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.suggestion.SimpleSuggestionFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtwebcore.model.cms2.components.PurchasedProductReferencesComponentModel;


/**
 * Unit test for {@link PurchasedProductReferenceComponentController}
 */
@UnitTest
public class PurchasedProductReferenceComponentControllerTest
{
    private static final String COMPONENT_UID = "componentUid";
    private static final String TEST_COMPONENT_UID = "componentUID";
    private static final String TEST_TYPE_CODE = PurchasedProductReferencesComponentModel._TYPECODE;
    private static final String TEST_TYPE_VIEW = ControllerConstants.Views.Cms.COMPONENT_PREFIX
            + StringUtils.lowerCase(TEST_TYPE_CODE);
    private static final String TITLE = "title";
    private static final String TITLE_VALUE = "Accessories";
    private static final String PRODUCT_REFERENCES = "productReferences";
    private static final String COMPONENT = "component";
    private static final String CATEGORY_CODE = "CategoryCode";

    private PurchasedProductReferenceComponentController purchasedProductReferenceComponentController;

    @Mock
    private PurchasedProductReferencesComponentModel purchasedProductReferencesComponentModel;
    @Mock
    private Model model;
    @Mock
    private DefaultCMSComponentService cmsComponentService;
    @Mock
    private SimpleSuggestionFacade simpleSuggestionFacade;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ComposedTypeModel composedTypeModel;
    @Mock
    private CategoryModel categoryModel;

    private final ProductData productData = Mockito.mock(ProductData.class);
    private final List<ProductData> productDataList = Collections.singletonList(productData);

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        purchasedProductReferenceComponentController = new PurchasedProductReferenceComponentController();
        purchasedProductReferenceComponentController.setCmsComponentService(cmsComponentService);
        ReflectionTestUtils
                .setField(purchasedProductReferenceComponentController, "simpleSuggestionFacade",
                        simpleSuggestionFacade);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRenderComponent() throws Exception
    {
        given(purchasedProductReferencesComponentModel.getMaximumNumberProducts()).willReturn(Integer.valueOf(1));
        given(purchasedProductReferencesComponentModel.getTitle()).willReturn(TITLE_VALUE);
        given(purchasedProductReferencesComponentModel.getProductReferenceTypes()).willReturn(
                Collections.singletonList(ProductReferenceTypeEnum.ACCESSORIES));
        given(purchasedProductReferencesComponentModel.getCategory()).willReturn(categoryModel);
        given(categoryModel.getCode()).willReturn(CATEGORY_CODE);
        given(purchasedProductReferencesComponentModel.isFilterPurchased()).willReturn(Boolean.TRUE);

        given(
                simpleSuggestionFacade.getReferencesForPurchasedInCategory(Mockito.anyString(),
                        Mockito.any(ProductReferenceTypeEnum.class), Mockito.anyBoolean(), Mockito.<Integer> any()))
                .willReturn(
                        productDataList);

        final String viewName = purchasedProductReferenceComponentController.handleComponent(request, model,
                purchasedProductReferencesComponentModel);
        verify(model, Mockito.times(1)).addAttribute(TITLE, TITLE_VALUE);
        verify(model, Mockito.times(1)).addAttribute(PRODUCT_REFERENCES, productDataList);
        Assert.assertEquals(TEST_TYPE_VIEW, viewName);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRenderComponentUid() throws Exception
    {
        given(request.getAttribute(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(
                purchasedProductReferencesComponentModel);
        given(purchasedProductReferencesComponentModel.getMaximumNumberProducts()).willReturn(Integer.valueOf(1));
        given(purchasedProductReferencesComponentModel.getTitle()).willReturn(TITLE_VALUE);
        given(purchasedProductReferencesComponentModel.getProductReferenceTypes()).willReturn(
                Collections.singletonList(ProductReferenceTypeEnum.ACCESSORIES));
        given(purchasedProductReferencesComponentModel.getCategory()).willReturn(categoryModel);
        given(categoryModel.getCode()).willReturn(CATEGORY_CODE);
        given(purchasedProductReferencesComponentModel.isFilterPurchased()).willReturn(Boolean.TRUE);


        given(
                simpleSuggestionFacade.getReferencesForPurchasedInCategory(Mockito.anyString(),
                        Mockito.any(ProductReferenceTypeEnum.class), Mockito.anyBoolean(), Mockito.<Integer> any()))
                .willReturn(
                        productDataList);

        final String viewName = purchasedProductReferenceComponentController.handleGet(request, model);
        verify(model, Mockito.times(1)).addAttribute(COMPONENT, purchasedProductReferencesComponentModel);
        verify(model, Mockito.times(1)).addAttribute(TITLE, TITLE_VALUE);
        verify(model, Mockito.times(1)).addAttribute(PRODUCT_REFERENCES, productDataList);
        Assert.assertEquals(TEST_TYPE_VIEW, viewName);
    }


    @Test(expected = AbstractPageController.HttpNotFoundException.class)
    public void testRenderComponentNotFound() throws Exception
    {
        given(request.getAttribute(COMPONENT_UID)).willReturn(null);
        given(request.getParameter(COMPONENT_UID)).willReturn(null);
        purchasedProductReferenceComponentController.handleGet(request, model);
    }

    @Test(expected = AbstractPageController.HttpNotFoundException.class)
    public void testRenderComponentNotFound2() throws Exception
    {
        given(request.getAttribute(COMPONENT_UID)).willReturn(null);
        given(request.getParameter(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(null);
        purchasedProductReferenceComponentController.handleGet(request, model);
    }

    @Test(expected = AbstractPageController.HttpNotFoundException.class)
    public void testRenderComponentNotFound3() throws Exception
    {
        given(request.getAttribute(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(null);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID))
                .willThrow(new CMSItemNotFoundException(""));
        purchasedProductReferenceComponentController.handleGet(request, model);
    }

}
