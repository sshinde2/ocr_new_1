/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.customer.impl.DefaultTargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.PasswordResetPageController;
import au.com.target.tgtstorefront.forms.ForgottenPwdForm;
import au.com.target.tgtstorefront.forms.UpdatePwdForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;
import au.com.target.tgtstorefront.util.PageTitleResolver;


/**
 * Unit tests for PasswordResetPageController.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PasswordResetPageControllerTest {

    private static final String TOKEN = "token";

    @Mock
    private DefaultTargetCustomerFacade customerFacade;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private AutoLoginStrategy autoLoginStrategy;

    @Mock
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private RedirectAttributes redirectModel;

    @Mock
    private UpdatePwdForm updatePwdForm;

    @Mock
    private ForgottenPwdForm forgottenPwdForm;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @InjectMocks
    private final PasswordResetPageController passwordResetPageController = new PasswordResetPageController();

    @Before
    public void setupTest() {
        given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.FALSE);
        given(updatePwdForm.getToken()).willReturn(TOKEN);
        given(customerFacade.getUidForPasswordResetToken(TOKEN)).willReturn("test");
    }

    @Test
    public void verifyEmailPassedToResetPasswordPage() throws CMSItemNotFoundException {
        final ContentPageModel pageModel = mock(ContentPageModel.class);
        given(cmsPageService.getPageForLabelOrId("forgottenPassword")).willReturn(pageModel);
        final Model localModel = mock(Model.class);
        final ArgumentCaptor<ForgottenPwdForm> formCaptor = ArgumentCaptor.forClass(ForgottenPwdForm.class);
        given(localModel.addAttribute(formCaptor.capture())).willReturn(localModel);

        passwordResetPageController.getPasswordRequest("user@company.com", localModel);
        assertThat(formCaptor.getValue()).isNotNull();
        assertThat(formCaptor.getValue().getEmail()).isEqualTo("user@company.com");
    }

    @Test
    public void verifyFwdParamPassedToCustomerFacadeDuringPasswordReset() throws CMSItemNotFoundException {
        given(forgottenPwdForm.getEmail()).willReturn("test@target.com");
        passwordResetPageController.passwordRequest(forgottenPwdForm, bindingResult, "basket", model, redirectModel);
        verify(customerFacade, times(1)).forgottenPassword("test@target.com", "basket");
    }

    @Test
    public void testChangePasswordWhenRedirectPageIsPresent() throws CMSItemNotFoundException {
        given(customerFacade.getRedirectPageForPasswordResetToken(TOKEN)).willReturn("basket");
        final String redirectLink = passwordResetPageController.changePassword(updatePwdForm, bindingResult, model,
                redirectModel, request,
                response);
        assertThat(redirectLink).isNotNull();
        assertThat(redirectLink).isEqualTo(ControllerConstants.Redirection.CHECKOUT);
    }

    @Test
    public void testChangePasswordWhenRedirectPageIsNull() throws CMSItemNotFoundException {
        given(customerFacade.getRedirectPageForPasswordResetToken(TOKEN)).willReturn(null);
        final String redirectLink = passwordResetPageController.changePassword(updatePwdForm, bindingResult, model,
                redirectModel, request,
                response);
        assertThat(redirectLink).isNotNull();
        assertThat(redirectLink).isEqualTo(ControllerConstants.Redirection.MY_ACCOUNT);
    }

    @Test
    public void testChangePasswordWhenRedirectPageIsEmpty() throws CMSItemNotFoundException {
        given(customerFacade.getRedirectPageForPasswordResetToken(TOKEN)).willReturn("");
        final String redirectLink = passwordResetPageController.changePassword(updatePwdForm, bindingResult, model,
                redirectModel, request,
                response);
        assertThat(redirectLink).isNotNull();
        assertThat(redirectLink).isEqualTo(ControllerConstants.Redirection.MY_ACCOUNT);
    }

    @Test
    public void testGetChangePasswordWillRedirectToHomeWithNoToken()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        final String redirectLink = passwordResetPageController.getChangePassword(null, model);
        assertThat(redirectLink).isNotNull();
        assertThat(redirectLink).isEqualTo(ControllerConstants.Redirection.HOME);
    }

    @Test
    public void testGetChangePasswordWillRedirectToSpcWithFeatureSwitch()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled("spa.reset.password.single.page");
        final String redirectLink = passwordResetPageController.getChangePassword(TOKEN, model);
        assertThat(redirectLink).isNotNull();
        assertThat(redirectLink)
                .isEqualTo(ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_PASSWORD_RESET + "?token=" + TOKEN);
    }

    @Test
    public void testGetChangePasswordWillEscapeTokenForSpc()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled("spa.reset.password.single.page");
        final String redirectLink = passwordResetPageController.getChangePassword("aa+bb cc==", model);
        assertThat(redirectLink).isNotNull();
        assertThat(redirectLink)
                .isEqualTo(ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_PASSWORD_RESET + "?token="
                        + "aa%2Bbb+cc%3D%3D");
    }
}
