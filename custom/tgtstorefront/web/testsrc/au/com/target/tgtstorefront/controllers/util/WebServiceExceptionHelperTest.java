package au.com.target.tgtstorefront.controllers.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.powermock.api.mockito.PowerMockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.TypeMismatchException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WebServiceExceptionHelperTest {

    private final WebServiceExceptionHelper webServiceExceptionHelper = new WebServiceExceptionHelper();

    @Mock
    private HttpServletRequest request;

    @Before
    public void setUp() {
        given(request.getRequestURI()).willReturn("URI");
    }

    @Test
    public void testHandleBindingExceptionWithNoFieldError() throws Exception {
        final BindingResult mockBindingResult = mock(BindingResult.class);

        final Response response = webServiceExceptionHelper.handleException(request,
                new BindException(mockBindingResult));
        final Error error = response.getData().getError();

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(error.getRequestUri()).isEqualTo("URI");
        assertThat(error.getCode()).isEqualTo("ERROR_UNEXPECTED_EXCEPTION");
    }

    @Test
    public void testHandleBindingException() throws Exception {
        final FieldError mockFieldError = mock(FieldError.class);

        final List<FieldError> fieldErrors = new ArrayList<>();
        fieldErrors.add(mockFieldError);

        final BindingResult mockBindingResult = mock(BindingResult.class);
        given(mockBindingResult.getFieldErrors()).willReturn(fieldErrors);

        final Response response = webServiceExceptionHelper.handleException(request,
                new BindException(mockBindingResult));

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();

        final BaseResponseData data = response.getData();
        assertThat(data).isNotNull();

        final Error error = data.getError();
        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(error.getMessage()).isEqualTo("Invalid post data");
        assertThat(error.getRequestUri()).isEqualTo("URI");
    }

    @Test
    public void testHandleMethodArgumentNotValidExceptionWithNoFieldErrors() throws Exception {
        final BindingResult mockBindingResult = mock(BindingResult.class);
        final MethodArgumentNotValidException mockMethodArgumentNotValidException = mock(
                MethodArgumentNotValidException.class);
        given(mockMethodArgumentNotValidException.getBindingResult()).willReturn(mockBindingResult);

        final Response response = webServiceExceptionHelper
                .handleException(request, mockMethodArgumentNotValidException);
        final Error error = response.getData().getError();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(error.getRequestUri()).isEqualTo("URI");
        assertThat(error.getCode()).isEqualTo("ERROR_UNEXPECTED_EXCEPTION");
    }

    @Test
    public void testHandleMethodArgumentNotValidException() throws Exception {
        final FieldError mockFieldError = mock(FieldError.class);
        final List<FieldError> fieldErrors = new ArrayList<>();
        fieldErrors.add(mockFieldError);
        final BindingResult mockBindingResult = mock(BindingResult.class);
        given(mockBindingResult.getFieldErrors()).willReturn(fieldErrors);
        final MethodArgumentNotValidException mockMethodArgumentNotValidException = mock(
                MethodArgumentNotValidException.class);
        given(mockMethodArgumentNotValidException.getBindingResult()).willReturn(mockBindingResult);

        final Response response = webServiceExceptionHelper
                .handleException(request, mockMethodArgumentNotValidException);

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        final BaseResponseData data = response.getData();
        assertThat(data).isNotNull();
        final Error error = data.getError();
        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(error.getMessage()).isEqualTo("Invalid post data");
        assertThat(error.getRequestUri()).isEqualTo("URI");
    }

    @Test
    public void testHandleTypeMismatchException() {
        final TypeMismatchException exception = mock(TypeMismatchException.class);
        final Response response = webServiceExceptionHelper.handleException(request, exception);
        final BaseResponseData data = response.getData();
        assertThat(data).isNotNull();
        final Error error = data.getError();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo("ERR_INVALID_TYPE");
        assertThat(error.getMessage()).isEqualTo("Invalid post data");
        assertThat(error.getRequestUri()).isEqualTo("URI");
    }

    @Test
    public void testHandleGeneralException() {
        final Exception exception = mock(Exception.class);
        given(exception.getMessage()).willReturn("error msg");
        final Response response = webServiceExceptionHelper.handleException(request, exception);
        final BaseResponseData data = response.getData();
        assertThat(data).isNotNull();
        final Error error = data.getError();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo("ERROR_UNEXPECTED_EXCEPTION");
        assertThat(error.getMessage()).isEqualTo("error msg");
        assertThat(error.getRequestUri()).isEqualTo("URI");
    }

}
