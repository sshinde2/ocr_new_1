/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.media.MediaModel;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtfacades.util.TargetPriceDataHelper;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtwebcore.cache.service.AkamaiCacheConfigurationService;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductCollectionPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;
import junit.framework.Assert;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LooksCollectionPageControllerTest {

    @InjectMocks
    private final LooksCollectionPageController controller = new LooksCollectionPageController();
    @Mock
    private CMSPageService mockCmsPageService;

    @Mock
    private Model mockModel;

    @Mock
    private TargetProductCollectionPageModel pageModel;

    @Mock
    private TargetProductGroupPageModel grpPageModel;

    @Mock
    private ContentPageModel contentPageModel;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private MediaModel mockMedia;

    @Mock
    private CMSNavigationNodeModel navNodeModel;

    @Mock
    private CMSNavigationNodeModel childNavNodeModel;

    @Mock
    private CMSNavigationEntryModel navEntryModel;

    @Mock
    private TargetPriceDataHelper targetPriceDataHelper;

    @Mock
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Mock
    private AkamaiCacheConfigurationService akamaiCacheConfigurationService;

    @Mock
    private HttpServletResponse response;

    @SuppressWarnings("boxing")
    @Before
    public void init() {
        BDDMockito.given(pageTitleResolver.resolveContentPageTitle(Mockito.anyString())).willReturn("test Page");
        BDDMockito
                .given(targetPriceDataHelper.populatePriceData(Mockito.anyDouble(), Mockito.any(PriceDataType.class)))
                .willReturn(new PriceData());
    }

    @Test
    public void testGetCollectionsPageWithPageNotfound() throws UnsupportedEncodingException, CMSItemNotFoundException {
        final CMSItemNotFoundException expectedException = new CMSItemNotFoundException("Page not found");
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/collection/Test")).willThrow(expectedException);
        final String redirect = controller.geCollectionPage("Test", mockModel, response);
        Assert.assertEquals(redirect, ControllerConstants.Forward.ERROR_404);
    }

    @Test
    public void testGetCollectionsPage() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/collection/Test")).willReturn(pageModel);
        BDDMockito.given(pageModel.getTitle()).willReturn("Title");
        final String redirect = controller.geCollectionPage("Test", mockModel, response);
        Assert.assertEquals(redirect, ControllerConstants.Views.Pages.LooksCollection.COLLECTION_PAGE);
    }

    @Test
    public void testGetCollectionsPageContentPage() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/collection/Test")).willReturn(contentPageModel);
        BDDMockito.given(pageModel.getTitle()).willReturn("Title");
        final String redirect = controller.geCollectionPage("Test", mockModel, response);
        Assert.assertEquals(redirect, ControllerConstants.Forward.ERROR_404);
    }

    @Test
    public void testPopulateCollectionDataWithNullPageModel() {
        final TargetProductCollectionPageModel pagemodel = null;
        final LooksCollectionData data = controller.populateCollectionData(pagemodel);
        Assert.assertNotNull(data);
    }

    @Test
    public void testPopulateCollectionDataWithNoLooks() {
        populateCollectionData();
        BDDMockito.given(pageModel.getNavigationNodeList()).willReturn(null);
        final LooksCollectionData data = controller.populateCollectionData(pageModel);
        Assert.assertNotNull(data);
        Assert.assertEquals(data.getDescription(), "Test Title");
        Assert.assertEquals(data.getImageUrl(), "/test/imgurl");
        Assert.assertEquals(data.getImgAltTxt(), "Img Text");
        Assert.assertEquals(data.getName(), "name");
        Assert.assertNotNull(data.getLookDetails());
        Assert.assertEquals(data.getLookDetails().size(), 0);
    }

    @Test
    public void testPopulateCollectionDataWithEmptyChildren() {
        populateCollectionData();
        final List<CMSNavigationNodeModel> nodeList = new ArrayList<>();
        BDDMockito.given(pageModel.getNavigationNodeList()).willReturn(nodeList);
        final LooksCollectionData data = controller.populateCollectionData(pageModel);
        Assert.assertNotNull(data);
        Assert.assertEquals(data.getDescription(), "Test Title");
        Assert.assertEquals(data.getImageUrl(), "/test/imgurl");
        Assert.assertEquals(data.getImgAltTxt(), "Img Text");
        Assert.assertEquals(data.getName(), "name");
        Assert.assertNotNull(data.getLookDetails());
        Assert.assertEquals(data.getLookDetails().size(), 0);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateCollectionDataWithChildrenNoEntries() {
        populateCollectionData();
        final List<CMSNavigationNodeModel> nodeList = Collections.singletonList(navNodeModel);
        final List<CMSNavigationNodeModel> childNodeList = Collections.singletonList(childNavNodeModel);
        BDDMockito.given(pageModel.getNavigationNodeList()).willReturn(nodeList);
        BDDMockito.given(pageModel.getNavigationNodeList().get(0).getChildren()).willReturn(childNodeList);
        BDDMockito.given(childNavNodeModel.isVisible()).willReturn(true);
        final LooksCollectionData data = controller.populateCollectionData(pageModel);
        Assert.assertNotNull(data);
        Assert.assertEquals(data.getDescription(), "Test Title");
        Assert.assertEquals(data.getImageUrl(), "/test/imgurl");
        Assert.assertEquals(data.getImgAltTxt(), "Img Text");
        Assert.assertEquals(data.getName(), "name");
        Assert.assertNotNull(data.getLookDetails());
        Assert.assertEquals(data.getLookDetails().size(), 1);
        Assert.assertEquals(data.getLookDetails().get(0).isVisible(), true);
        Assert.assertEquals(data.getLookDetails().get(0).getGroupImgUrl(), null);
        Assert.assertEquals(data.getLookDetails().get(0).getLabel(), null);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateCollectionDataWithChildrenWithEntries() {
        populateCollectionData();
        final List<CMSNavigationNodeModel> nodeList = Collections.singletonList(navNodeModel);
        final List<CMSNavigationNodeModel> childNodeList = Collections.singletonList(childNavNodeModel);
        final List<CMSNavigationEntryModel> navEntryList = Collections.singletonList(navEntryModel);
        BDDMockito.given(pageModel.getNavigationNodeList()).willReturn(nodeList);
        BDDMockito.given(pageModel.getNavigationNodeList().get(0).getChildren()).willReturn(childNodeList);
        BDDMockito.given(childNavNodeModel.isVisible()).willReturn(true);
        BDDMockito.given(childNavNodeModel.getEntries()).willReturn(navEntryList);
        BDDMockito.given(navEntryModel.getItem()).willReturn(grpPageModel);
        BDDMockito.given(grpPageModel.getGroupImage()).willReturn(mockMedia);
        BDDMockito.given(grpPageModel.getName()).willReturn("Test name");
        BDDMockito.given(grpPageModel.getLabel()).willReturn("Test label");
        BDDMockito.given(grpPageModel.getFromPrice()).willReturn(Double.valueOf(100.0));
        final LooksCollectionData data = controller.populateCollectionData(pageModel);
        Assert.assertNotNull(data);
        Assert.assertEquals(data.getDescription(), "Test Title");
        Assert.assertEquals(data.getImageUrl(), "/test/imgurl");
        Assert.assertEquals(data.getImgAltTxt(), "Img Text");
        Assert.assertEquals(data.getName(), "name");
        Assert.assertNotNull(data.getLookDetails());
        Assert.assertEquals(data.getLookDetails().size(), 1);
        Assert.assertEquals(data.getLookDetails().get(0).isVisible(), true);
        Assert.assertEquals(data.getLookDetails().get(0).getGroupImgUrl(), "/test/imgurl");
        Assert.assertEquals(data.getLookDetails().get(0).getLabel(), "Test label");
        Assert.assertEquals(data.getLookDetails().get(0).getName(), "Test name");
        Assert.assertNotNull(data.getLookDetails().get(0).getPriceData());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateCollectionDataWithChildrenNullPrice() {
        populateCollectionData();
        final List<CMSNavigationNodeModel> nodeList = Collections.singletonList(navNodeModel);
        final List<CMSNavigationNodeModel> childNodeList = Collections.singletonList(childNavNodeModel);
        final List<CMSNavigationEntryModel> navEntryList = Collections.singletonList(navEntryModel);
        BDDMockito.given(pageModel.getNavigationNodeList()).willReturn(nodeList);
        BDDMockito.given(pageModel.getNavigationNodeList().get(0).getChildren()).willReturn(childNodeList);
        BDDMockito.given(childNavNodeModel.isVisible()).willReturn(true);
        BDDMockito.given(childNavNodeModel.getEntries()).willReturn(navEntryList);
        BDDMockito.given(navEntryModel.getItem()).willReturn(grpPageModel);
        BDDMockito.given(grpPageModel.getGroupImage()).willReturn(mockMedia);
        BDDMockito.given(grpPageModel.getName()).willReturn("Test name");
        BDDMockito.given(grpPageModel.getLabel()).willReturn("Test label");
        BDDMockito.given(grpPageModel.getFromPrice()).willReturn(null);
        final LooksCollectionData data = controller.populateCollectionData(pageModel);
        Assert.assertNotNull(data);
        Assert.assertEquals(data.getDescription(), "Test Title");
        Assert.assertEquals(data.getImageUrl(), "/test/imgurl");
        Assert.assertEquals(data.getImgAltTxt(), "Img Text");
        Assert.assertEquals(data.getName(), "name");
        Assert.assertNotNull(data.getLookDetails());
        Assert.assertEquals(data.getLookDetails().size(), 1);
        Assert.assertEquals(data.getLookDetails().get(0).isVisible(), true);
        Assert.assertEquals(data.getLookDetails().get(0).getGroupImgUrl(), "/test/imgurl");
        Assert.assertEquals(data.getLookDetails().get(0).getLabel(), "Test label");
        Assert.assertEquals(data.getLookDetails().get(0).getName(), "Test name");
        Assert.assertNull(data.getLookDetails().get(0).getPriceData());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateCollectionDataWithChildrenWithDiffPageModel() {
        populateCollectionData();
        final List<CMSNavigationNodeModel> nodeList = Collections.singletonList(navNodeModel);
        final List<CMSNavigationNodeModel> childNodeList = Collections.singletonList(childNavNodeModel);
        final List<CMSNavigationEntryModel> navEntryList = Collections.singletonList(navEntryModel);
        BDDMockito.given(pageModel.getNavigationNodeList()).willReturn(nodeList);
        BDDMockito.given(pageModel.getNavigationNodeList().get(0).getChildren()).willReturn(childNodeList);
        BDDMockito.given(childNavNodeModel.isVisible()).willReturn(true);
        BDDMockito.given(childNavNodeModel.getEntries()).willReturn(navEntryList);
        BDDMockito.given(navEntryModel.getItem()).willReturn(contentPageModel);
        final LooksCollectionData data = controller.populateCollectionData(pageModel);
        Assert.assertNotNull(data);
        Assert.assertEquals(data.getDescription(), "Test Title");
        Assert.assertEquals(data.getImageUrl(), "/test/imgurl");
        Assert.assertEquals(data.getImgAltTxt(), "Img Text");
        Assert.assertEquals(data.getName(), "name");
        Assert.assertNotNull(data.getLookDetails());
        Assert.assertEquals(data.getLookDetails().size(), 1);
        Assert.assertEquals(data.getLookDetails().get(0).isVisible(), true);
        Assert.assertEquals(data.getLookDetails().get(0).getGroupImgUrl(), null);
        Assert.assertEquals(data.getLookDetails().get(0).getLabel(), null);
        Assert.assertEquals(data.getLookDetails().get(0).getName(), null);

    }

    private void populateCollectionData() {
        BDDMockito.given(pageModel.getCollectionDescription()).willReturn("Test Title");
        BDDMockito.given(pageModel.getCollectionImage()).willReturn(mockMedia);
        BDDMockito.given(pageModel.getCollectionImage().getURL()).willReturn("/test/imgurl");
        BDDMockito.given(pageModel.getCollectionImage().getAltText()).willReturn("Img Text");
        BDDMockito.given(pageModel.getName()).willReturn("name");
    }
}
