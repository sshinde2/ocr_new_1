package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.CookieGenerator;

import au.com.target.tgtcore.google.location.finder.service.TargetLocationFinderService;
import au.com.target.tgtfacades.google.location.finder.TargetGoogleLocationFinderFacade;
import au.com.target.tgtfacades.google.location.finder.data.GoogleResponseData;
import au.com.target.tgtfacades.google.location.finder.data.Result;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LocationFinderServiceControllerTest {

    private static final String LOCATION = "Carnegie$$%%&&";

    private static final String SANITISED_LOCATION = "Carnegie";

    private static final String LOCATION_POSTCODE = "3145$$%%&&";

    private static final String CARNEGIE_POST_CODE = "3145";

    private static final String STATE_VIC = "vic";

    private static final int VIC_HD_SHORT_LEAD_TIME = 3;

    private static final int VIC_ED_SHORT_LEAD_TIME = 4;

    private static final int VIC_BULKY1_SHORT_LEAD_TIME = 5;

    private static final int VIC_BULKY2_SHORT_LEAD_TIME = 9;

    private static final String SUBURB = "Suburb";

    private static final String POST_CODE = "Post Code";

    private static final String STATE = "State";

    private static final String HD_SHORT_LEAD_TIME = "HD Short Lead Time";

    private static final String ED_SHORT_LEAD_TIME = "ED Short Lead Time";

    private static final String BULKY1_SHORT_LEAD_TIME = "Bulky1 Short Lead Time";

    private static final String BULKY2_SHORT_LEAD_TIME = "Bulky2 Short Lead Time";

    @Mock
    private SearchQuerySanitiser searchQuerySanitiserMock;

    @Mock
    private TargetGoogleLocationFinderFacade targetGoogleLocationFinderFacadeMock;

    @Mock
    private TargetLocationFinderService targetLocationFinderServiceMock;

    private final HttpServletResponse httpServletResponseMock = new MockHttpServletResponse();

    @Mock
    private CookieGenerator cookieGenerator;

    @InjectMocks
    private final LocationFinderServiceController locationFinderServiceController = new LocationFinderServiceController();

    /**
     * Method will verify geocoded delivery location of the user based on location parameter.
     * 
     */
    @Test
    public void testGetDeliveryLocationDetailsWithLocationParameter() throws IOException {
        given(searchQuerySanitiserMock.sanitiseSearchText(LOCATION)).willReturn(SANITISED_LOCATION);
        final GoogleResponseData googleResponseDto = createGoogleResponseDto();
        final Response response = new Response(true);
        response.setData(googleResponseDto);
        given(targetGoogleLocationFinderFacadeMock.searchUserDeliveryLocation(SANITISED_LOCATION, null,
                null)).willReturn(response);
        final Response actualResponse = locationFinderServiceController.getDeliveryLocationDetails(LOCATION,
                null, null, httpServletResponseMock);
        final GoogleResponseData googleResponseDtoActual = (GoogleResponseData)actualResponse.getData();
        final Result result = googleResponseDtoActual.getResults().get(0);
        assertThat(response).isEqualTo(actualResponse);
        assertThat(result.getSuburb()).as(SUBURB).isEqualTo(SANITISED_LOCATION);
        assertThat(result.getPostalCode()).as(POST_CODE).isEqualTo(CARNEGIE_POST_CODE);
        assertThat(result.getState()).as(STATE).isEqualTo(STATE_VIC);
        assertThat(result.getHdShortLeadTime()).as(HD_SHORT_LEAD_TIME).isEqualTo(VIC_HD_SHORT_LEAD_TIME);
        assertThat(result.getEdShortLeadTime()).as(ED_SHORT_LEAD_TIME).isEqualTo(VIC_ED_SHORT_LEAD_TIME);
        assertThat(result.getBulky1ShortLeadTime()).as(BULKY1_SHORT_LEAD_TIME).isEqualTo(VIC_BULKY1_SHORT_LEAD_TIME);
        assertThat(result.getBulky2ShortLeadTime()).as(BULKY2_SHORT_LEAD_TIME).isEqualTo(VIC_BULKY2_SHORT_LEAD_TIME);
        verify(cookieGenerator).addCookie(httpServletResponseMock,
                "Carnegie%203145");
    }

    /**
     * Method will verify geocoded delivery location of the user based on postcode parameter.
     * 
     */
    @Test
    public void testgetDeliveryLocationDetailsWithPostcodeParameter() {
        given(searchQuerySanitiserMock.sanitiseSearchText(LOCATION_POSTCODE)).willReturn(CARNEGIE_POST_CODE);
        final GoogleResponseData googleResponseDto = createGoogleResponseDto();
        final Response response = new Response(true);
        response.setData(googleResponseDto);
        given(targetGoogleLocationFinderFacadeMock.searchUserDeliveryLocation(CARNEGIE_POST_CODE, null, null))
                .willReturn(response);
        final Response actualResponse = locationFinderServiceController.getDeliveryLocationDetails(LOCATION_POSTCODE,
                null, null, httpServletResponseMock);
        verifyGeoLocationDetails(response, actualResponse);
    }

    /**
     * Method will verify geocoded delivery location of the user based on LatLng parameter.
     * 
     */
    @Test
    public void testgetDeliveryLocationDetailsWithLatLngParameter() {
        given(searchQuerySanitiserMock.sanitiseSearchText(LOCATION_POSTCODE)).willReturn(CARNEGIE_POST_CODE);
        final GoogleResponseData googleResponseDto = createGoogleResponseDto();
        final Response response = new Response(true);
        response.setData(googleResponseDto);
        final Double latitude = Double.valueOf(-37.88867);
        final Double longitude = Double.valueOf(145.05713);
        given(targetGoogleLocationFinderFacadeMock.searchUserDeliveryLocation(null, latitude, longitude))
                .willReturn(response);
        final Response actualResponse = locationFinderServiceController.getDeliveryLocationDetails(null,
                latitude, longitude, httpServletResponseMock);
        verifyGeoLocationDetails(response, actualResponse);
        verify(cookieGenerator).addCookie(httpServletResponseMock,
                "Carnegie%203145");
    }

    /**
     * This method will verify when no response received with location parameter.
     */
    @Test
    public void testgetDeliveryLocationDetailsWithLocationParameterWithNoResponse() {
        given(searchQuerySanitiserMock.sanitiseSearchText(LOCATION)).willReturn(SANITISED_LOCATION);
        final Response response = locationFinderServiceController.getDeliveryLocationDetails(LOCATION, null, null,
                httpServletResponseMock);
        assertThat(response).isNull();
    }

    /**
     * This method will verify when no response received with postcode parameter.
     */
    @Test
    public void testgetDeliveryLocationDetailsWithPostcodeWithNoResponse() {
        given(searchQuerySanitiserMock.sanitiseSearchText(LOCATION_POSTCODE)).willReturn(CARNEGIE_POST_CODE);
        final Response response = locationFinderServiceController.getDeliveryLocationDetails(LOCATION_POSTCODE, null,
                null, httpServletResponseMock);
        assertThat(response).isNull();
    }

    /**
     * This method will verify when no response received with LatLng parameter.
     */
    @Test
    public void testgetDeliveryLocationDetailsWithLatLngWithNoResponse() {
        final Double latitude = Double.valueOf(-37.88867);
        final Double longitude = Double.valueOf(145.05713);
        given(searchQuerySanitiserMock.sanitiseSearchText(LOCATION_POSTCODE)).willReturn(CARNEGIE_POST_CODE);
        final Response response = locationFinderServiceController.getDeliveryLocationDetails(null, latitude,
                longitude, httpServletResponseMock);
        assertThat(response).isNull();
    }

    /**
     * Verifying Geolocation details.
     * 
     * @param response
     * @param actualResponse
     */
    private void verifyGeoLocationDetails(final Response response, final Response actualResponse) {
        final GoogleResponseData googleResponseDtoActual = (GoogleResponseData)actualResponse.getData();
        final Result result = googleResponseDtoActual.getResults().get(0);
        assertThat(response).isEqualTo(actualResponse);
        assertThat(result.getSuburb()).as(SUBURB).isEqualTo(SANITISED_LOCATION);
        assertThat(result.getPostalCode()).as(POST_CODE).isEqualTo(CARNEGIE_POST_CODE);
        assertThat(result.getState()).as(STATE).isEqualTo(STATE_VIC);
        assertThat(result.getHdShortLeadTime()).as(HD_SHORT_LEAD_TIME).isEqualTo(VIC_HD_SHORT_LEAD_TIME);
        assertThat(result.getEdShortLeadTime()).as(ED_SHORT_LEAD_TIME).isEqualTo(VIC_ED_SHORT_LEAD_TIME);
        assertThat(result.getBulky1ShortLeadTime()).as(BULKY1_SHORT_LEAD_TIME).isEqualTo(VIC_BULKY1_SHORT_LEAD_TIME);
        assertThat(result.getBulky2ShortLeadTime()).as(BULKY2_SHORT_LEAD_TIME).isEqualTo(VIC_BULKY2_SHORT_LEAD_TIME);
    }

    /**
     * Method will create mock response data based on search type.
     * 
     * @return GoogleResponseDto
     */
    private GoogleResponseData createGoogleResponseDto() {
        final GoogleResponseData googleResponseDto = new GoogleResponseData();
        final List<Result> results = new ArrayList<>();
        final Result result = new Result();
        result.setState(STATE_VIC);
        result.setSuburb(SANITISED_LOCATION);
        result.setPostalCode(CARNEGIE_POST_CODE);
        result.setHdShortLeadTime(VIC_HD_SHORT_LEAD_TIME);
        result.setEdShortLeadTime(VIC_ED_SHORT_LEAD_TIME);
        result.setBulky1ShortLeadTime(VIC_BULKY1_SHORT_LEAD_TIME);
        result.setBulky2ShortLeadTime(VIC_BULKY2_SHORT_LEAD_TIME);
        results.add(result);
        googleResponseDto.setResults(results);
        return googleResponseDto;
    }

}
