/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Unit tests for AbstractBeforeControllerHandler
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractBeforeControllerHandlerTest {

    private static final String LOGIN_URL = "/login";
    private static final String LEGACY_LOGIN_AND_CHECKOUT_URL = "/checkout/login";
    private static final String SPC_LOGIN_AND_CHECKOUT_URL = "/spc/order";

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private SalesApplicationFacade mockSalesApplicationFacade;

    @InjectMocks
    private final AbstractBeforeControllerHandler handler = new AbstractBeforeControllerHandler() {

        @Override
        public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
                final HandlerMethod handlerMethod) {
            return false;
        }

    };

    @Before
    public void setup() {
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(
                "checkout.error.paymentmethod.ipg.transactiondetails.queryresult.unavailable");
        willReturn(Boolean.FALSE).given(mockSalesApplicationFacade).isKioskApplication();
    }

    @Test
    public void redirectURLWhenRequestIsNull() {
        final String redirectURL = handler.getRedirectUrl(null);
        assertThat(redirectURL).isEqualTo(LOGIN_URL);
    }

    @Test
    public void redirectURLWhenServletPathHasNoCheckout() {
        given(mockRequest.getServletPath()).willReturn("");
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(LOGIN_URL);
    }

    @Test
    public void redirectURLWhenRequestHasNoErrorMessageKey() {
        given(mockRequest.getServletPath()).willReturn("checkout");
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(
                null);
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(LEGACY_LOGIN_AND_CHECKOUT_URL);
    }

    @Test
    public void redirectURLWhenFeatureSwithToSpcLoginAndCheckout() {
        given(mockRequest.getServletPath()).willReturn("checkout");
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(
                null);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isSpcLoginAndCheckout();
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(SPC_LOGIN_AND_CHECKOUT_URL);
    }

    @Test
    public void redirectURLWhenRequestHasEmptyErrorMessageKey() {
        given(mockRequest.getServletPath()).willReturn("checkout");
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(
                "");
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(LEGACY_LOGIN_AND_CHECKOUT_URL);
    }

    @Test
    public void redirectURLWhenRequestIsWSAPICheckout() {
        given(mockRequest.getServletPath()).willReturn("/ws-api/v1/target/checkout/cart/detail");
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo("/ws-api/v1/target/checkout/login");
    }

    @Test
    public void redirectURLWhenRequestHasErrorMessageKey() {
        given(mockRequest.getServletPath()).willReturn("checkout");
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(
                LEGACY_LOGIN_AND_CHECKOUT_URL + "?" + ControllerConstants.ERROR + "="
                        + "checkout.error.paymentmethod.ipg.transactiondetails.queryresult.unavailable");
    }

    @Test
    public void getRedirectUrlWithNoErrorMessageKeyAndSalesChannelKiosk() {
        given(mockRequest.getServletPath()).willReturn("checkout");
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(
                null);

        willReturn(Boolean.TRUE).given(mockSalesApplicationFacade).isKioskApplication();

        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(LEGACY_LOGIN_AND_CHECKOUT_URL);
    }

    @Test
    public void getRedirectUrlWithEmptyErrorMessageKeyAndSalesChannelKiosk() {
        given(mockRequest.getServletPath()).willReturn("checkout");
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(
                "");

        willReturn(Boolean.TRUE).given(mockSalesApplicationFacade).isKioskApplication();

        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(LEGACY_LOGIN_AND_CHECKOUT_URL);
    }

    @Test
    public void getRedirectUrlWithErrorMessageKeyAndSalesChannelKiosk() {
        given(mockRequest.getServletPath()).willReturn("checkout");

        willReturn(Boolean.TRUE).given(mockSalesApplicationFacade).isKioskApplication();

        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo(
                LEGACY_LOGIN_AND_CHECKOUT_URL + "?" + ControllerConstants.ERROR + "="
                        + "checkout.error.paymentmethod.ipg.transactiondetails.queryresult.unavailable");
    }

    @Test
    public void getRedirectUrlWhenReturningFromAfterPay() {
        given(mockRequest.getParameter(ControllerConstants.ERROR)).willReturn(null);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isSpcLoginAndCheckout();
        given(mockRequest.getServletPath())
                .willReturn(ControllerConstants.PAYMENT + ControllerConstants.PAYMENT_AFTERPAY_RETURN
                        + ControllerConstants.SINGLE_PAGE_CHECKOUT);
        final String redirectURL = handler.getRedirectUrl(mockRequest);
        assertThat(redirectURL).isEqualTo("/spc/order/place-order/error?e=QUZURVJQQVlfU0VTU0lPTl9USU1FT1VU");
    }
}
