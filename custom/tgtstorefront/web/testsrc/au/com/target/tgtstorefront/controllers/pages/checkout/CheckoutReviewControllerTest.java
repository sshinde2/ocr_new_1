/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import junit.framework.Assert;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutReviewControllerTest {
    @Mock
    private SessionService sessionService;

    private final Model model = new ExtendedModelMap();

    @InjectMocks
    private CheckoutReviewController checkoutReviewController = new CheckoutReviewController();

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private TargetCheckoutFacade checkoutFacade;

    @Mock
    private TargetPlaceOrderFacade placeOrderFacade;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private CommonI18NService mockCommonI18NService;

    @Mock
    private MessageSource mockMessageSource;

    private Locale enAuLocale;

    @Before
    public void setUp() {
        when(request.getSession()).thenReturn(session);

        final LanguageModel mockLanguage = mock(LanguageModel.class);
        given(mockCommonI18NService.getCurrentLanguage()).willReturn(mockLanguage);

        enAuLocale = Locale.forLanguageTag("en-AU");
        given(mockCommonI18NService.getLocaleForLanguage(mockLanguage)).willReturn(enAuLocale);
    }

    @Ignore
    @Test
    public void testaddExpediteCheckoutMessage() {
        Mockito.when(sessionService.getAttribute("expedite")).thenReturn("on");
        checkoutReviewController.addExpediteCheckoutMessage(model, "on");
    }

    @Ignore
    @Test
    public void testaddExpediteCheckoutMessageWhenExpediteIsNotSet() {
        checkoutReviewController.addExpediteCheckoutMessage(model, null);
        BDDMockito.verify(sessionService).getAttribute(Mockito.anyString());
    }

    @Ignore
    @Test
    public void testaddExpediteCheckoutMessageWhenExpediteIsSetToOff() {
        checkoutReviewController.addExpediteCheckoutMessage(model, "off");
    }

    @Test
    public void testIsExpediteCheckoutTurnedOn() {

        Assert.assertTrue(checkoutReviewController.wasCustomerExpeditedToReviewPage("yes"));
    }

    @Test
    public void testIsExpediteCheckoutTurnedOff() {
        Assert.assertFalse(checkoutReviewController.wasCustomerExpeditedToReviewPage("off"));
    }

    @Test
    public void testIsExpediteCheckoutTurnedOnNull() {
        Assert.assertFalse(checkoutReviewController.wasCustomerExpeditedToReviewPage(null));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRedirectToPaymentPageIfIpgGiftCardsUnavailable() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetCCPaymentInfoData paymentInfo = mock(TargetCCPaymentInfoData.class);
        final IpgPaymentInfoData mockIpgPaymentInfoData = mock(IpgPaymentInfoData.class);
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        final ContentPageModel page = mock(ContentPageModel.class);
        final AdapterException exception = mock(AdapterException.class);
        checkoutReviewController = spy(checkoutReviewController);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);

        given(mockIpgPaymentInfoData.getIpgPaymentTemplateType()).willReturn(IpgPaymentTemplateType.GIFTCARDMULTIPLE);

        when(paymentInfo.isIpgPaymentInfo()).thenReturn(true);
        given(paymentInfo.getIpgPaymentInfoData()).willReturn(mockIpgPaymentInfoData);

        doReturn(null).when(checkoutReviewController)
                .getRedirectionAndCheckSOH(model, redirectAttributes, "", cartData);
        when(cartData.getPaymentInfo()).thenReturn(paymentInfo);
        when(cmsPageService.getPageForLabelOrId(anyString())).thenReturn(page);
        when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        when(checkoutFacade.getIpgSessionToken("returnUrl")).thenThrow(exception);
        final String doaminUrl = "https://www.target.com.au";
        final String uri = "/checkout/review-order/tsfdfs";
        when(request.getRequestURL()).thenReturn(new StringBuffer(doaminUrl + uri));
        when(request.getContextPath()).thenReturn("");
        when(request.getRequestURI()).thenReturn(uri);

        given(mockMessageSource.getMessage("checkout.paymentMethod.giftcard", null, enAuLocale))
                .willReturn("gift card");

        final String redirect = checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");

        assertEquals(ControllerConstants.Redirection.CHECKOUT_PAYMENT, redirect);
        verify(redirectAttributes).addFlashAttribute("ipgGiftCardUnavailable", Boolean.TRUE);
        verify(redirectAttributes, never()).addFlashAttribute(eq("ipgCreditCardUnavailable"), any(Boolean.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRedirectToPaymentPageIfIpgCreditCardUnavailable() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetCCPaymentInfoData paymentInfo = mock(TargetCCPaymentInfoData.class);
        final IpgPaymentInfoData mockIpgPaymentInfoData = mock(IpgPaymentInfoData.class);
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        final ContentPageModel page = mock(ContentPageModel.class);
        final AdapterException exception = mock(AdapterException.class);
        checkoutReviewController = spy(checkoutReviewController);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);

        given(mockIpgPaymentInfoData.getIpgPaymentTemplateType()).willReturn(IpgPaymentTemplateType.CREDITCARDSINGLE);

        when(paymentInfo.isIpgPaymentInfo()).thenReturn(true);
        given(paymentInfo.getIpgPaymentInfoData()).willReturn(mockIpgPaymentInfoData);

        doReturn(null).when(checkoutReviewController)
                .getRedirectionAndCheckSOH(model, redirectAttributes, "", cartData);
        when(cartData.getPaymentInfo()).thenReturn(paymentInfo);
        when(cmsPageService.getPageForLabelOrId(anyString())).thenReturn(page);
        when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        when(checkoutFacade.getIpgSessionToken("returnUrl")).thenThrow(exception);
        final String doaminUrl = "https://www.target.com.au";
        final String uri = "/checkout/review-order/tsfdfs";
        when(request.getRequestURL()).thenReturn(new StringBuffer(doaminUrl + uri));
        when(request.getContextPath()).thenReturn("");
        when(request.getRequestURI()).thenReturn(uri);

        given(mockMessageSource.getMessage("checkout.paymentMethod.creditcard", null, enAuLocale))
                .willReturn("credit card");

        final String redirect = checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");

        assertEquals(ControllerConstants.Redirection.CHECKOUT_PAYMENT, redirect);
        verify(redirectAttributes).addFlashAttribute("ipgCreditCardUnavailable", Boolean.TRUE);
        verify(redirectAttributes, never()).addFlashAttribute(eq("ipgGiftCardUnavailable"), any(Boolean.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testNotRedirectToPaymentPageIfGoWithTNS() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetCCPaymentInfoData paymemtInfo = mock(TargetCCPaymentInfoData.class);
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        final ContentPageModel page = mock(ContentPageModel.class);
        checkoutReviewController = spy(checkoutReviewController);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
        when(paymemtInfo.isIpgPaymentInfo()).thenReturn(false);
        doReturn(null).when(checkoutReviewController)
                .getRedirectionAndCheckSOH(model, redirectAttributes, "", cartData);
        when(cartData.getPaymentInfo()).thenReturn(paymemtInfo);
        when(cmsPageService.getPageForLabelOrId(anyString())).thenReturn(page);
        when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);

        final String result = checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.REVIEW_YOUR_ORDER_PAGE, result);
    }

    @Test
    public void testGetDomain() {
        final String doaminUrl = "https://www.target.com.au";
        final String uri = "/checkout/review-order/tsfdfs";
        when(request.getRequestURL()).thenReturn(new StringBuffer(doaminUrl + uri));
        when(request.getContextPath()).thenReturn("");
        when(request.getRequestURI()).thenReturn(uri);
        assertEquals(doaminUrl, checkoutReviewController.getDomainUrl(request));
    }

    @Test
    public void testOrderSummaryWhenGiftCardPaymentNotAllowed() throws CMSItemNotFoundException {
        when(Boolean.valueOf(checkoutFacade.isPaymentModeGiftCard())).thenReturn(Boolean.TRUE);
        when(Boolean.valueOf(checkoutFacade.isGiftCardPaymentAllowedForCart())).thenReturn(Boolean.FALSE);
        mockPaymentAndSohInfo();
        final String result = checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");
        assertEquals(ControllerConstants.Redirection.CHECKOUT_PAYMENT, result);
    }

    @Test
    public void testOrderSummaryWhenGiftCardPaymentAllowed() throws CMSItemNotFoundException {
        when(Boolean.valueOf(checkoutFacade.isPaymentModeGiftCard())).thenReturn(Boolean.TRUE);
        when(Boolean.valueOf(checkoutFacade.isGiftCardPaymentAllowedForCart())).thenReturn(Boolean.TRUE);
        mockPaymentAndSohInfo();
        final String result = checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");
        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.REVIEW_YOUR_ORDER_PAGE, result);
    }

    /**
     * Method to mock payment info and soh details.
     * 
     * @throws CMSItemNotFoundException
     */
    private void mockPaymentAndSohInfo() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetCCPaymentInfoData paymemtInfo = mock(TargetCCPaymentInfoData.class);
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        final ContentPageModel page = mock(ContentPageModel.class);
        checkoutReviewController = spy(checkoutReviewController);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
        when(Boolean.valueOf(paymemtInfo.isIpgPaymentInfo())).thenReturn(Boolean.FALSE);
        doReturn(null).when(checkoutReviewController)
                .getRedirectionAndCheckSOH(model, redirectAttributes, "", cartData);
        when(cartData.getPaymentInfo()).thenReturn(paymemtInfo);
        when(cmsPageService.getPageForLabelOrId(anyString())).thenReturn(page);
        when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
    }

    @Test
    public void testIsIpgPaymentWhenPaymentInfoIsNotTargetCCPaymentInfoData() {
        final CCPaymentInfoData paymentInfoData = mock(CCPaymentInfoData.class);
        final boolean isIpg = checkoutReviewController.isIpgPayment(paymentInfoData);
        assertFalse(isIpg);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsIpgPaymentWhenPaymentInfoIsTargetCCPaymentInfoDataNotIpg() {
        final TargetCCPaymentInfoData paymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(paymentInfoData.isIpgPaymentInfo()).willReturn(false);
        final boolean isIpg = checkoutReviewController.isIpgPayment(paymentInfoData);
        assertFalse(isIpg);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsIpgPaymentWhenPaymentInfoIsTargetCCPaymentInfoDataIpg() {
        final TargetCCPaymentInfoData paymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(paymentInfoData.isIpgPaymentInfo()).willReturn(true);
        final boolean isIpg = checkoutReviewController.isIpgPayment(paymentInfoData);
        assertTrue(isIpg);
    }

    @Test
    public void testReverseExistingGiftCardPaymentsWhenIpgSessionIsNull() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(checkoutFacade.getIpgSessionTokenFromCart(cartData)).willReturn(null);
        checkoutReviewController.reverseExistingGiftCardPayments(cartData);
        verify(placeOrderFacade, times(0)).reverseGiftCardPayment();
    }

    @Test
    public void testReverseExistingGiftCardPaymentsWhenIpgSessionIsEmpty() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(checkoutFacade.getIpgSessionTokenFromCart(cartData)).willReturn("");
        checkoutReviewController.reverseExistingGiftCardPayments(cartData);
        verify(placeOrderFacade, times(0)).reverseGiftCardPayment();
    }

    @Test
    public void testReverseExistingGiftCardPaymentsWhenValidIpgSession() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(checkoutFacade.getIpgSessionTokenFromCart(cartData)).willReturn("123456");
        checkoutReviewController.reverseExistingGiftCardPayments(cartData);
        verify(placeOrderFacade, times(1)).reverseGiftCardPayment();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testNonIpgPaymentMethodDoesNotTriggerReverseGiftCardPayments() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        //        final TargetCCPaymentInfoData paymentInfo = mock(TargetCCPaymentInfoData.class);

        checkoutReviewController = spy(checkoutReviewController);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);

        doReturn(null).when(checkoutReviewController)
                .getRedirectionAndCheckSOH(model, redirectAttributes, "", cartData);
        doReturn(Boolean.FALSE).when(checkoutReviewController)
                .isIpgPayment(Mockito.any(TargetCCPaymentInfoData.class));
        doNothing().when(checkoutReviewController)
                .addCommonElementsToModel(model, cartData, session);
        doNothing().when(checkoutReviewController).addExpediteCheckoutMessage(model, "expedited");

        checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");
        verify(checkoutReviewController, times(0)).reverseExistingGiftCardPayments(cartData);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIpgPaymentMethodTriggersReverseGiftCardPayments() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);

        checkoutReviewController = spy(checkoutReviewController);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);

        doReturn(null).when(checkoutReviewController)
                .getRedirectionAndCheckSOH(model, redirectAttributes, "", cartData);
        doReturn(Boolean.TRUE).when(checkoutReviewController)
                .isIpgPayment(any(TargetCCPaymentInfoData.class));
        doNothing().when(checkoutReviewController)
                .addCommonElementsToModel(model, cartData, session);
        doNothing().when(checkoutReviewController).addExpediteCheckoutMessage(model, "expedited");
        doReturn(Boolean.FALSE).when(checkoutReviewController).generateIpgToken(request, model,
                "expedited", cartData);
        doReturn("").when(checkoutReviewController).getDomainUrl(request);

        checkoutReviewController.orderSummary(model, redirectAttributes, request, "expedited");
        verify(checkoutReviewController, times(1)).reverseExistingGiftCardPayments(cartData);
    }
}