/**
 *
 */
package au.com.target.tgtstorefront.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Test suite for {@link PageTitleResolver}.
 * 
 * @author asingh78
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PageTitleResolverTest {

    private static final String SITE_NAME = "Target";
    private static final String PRODUCT_NAME = "Pants";
    private static final String COLOUR_VARIANT_DISPLAY_NAME = "Pants Black";
    private static final String TITLE_SUFFIX = "Target Australia";
    private static final String TITLE_SEPARATOR = " | ";

    @Spy
    @InjectMocks
    private final PageTitleResolver pageTitleResolver = new PageTitleResolver();

    @Mock
    private CMSSiteService cmsSiteService;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private TargetProductModel product;

    @Mock
    private TargetDealService targetDealService;

    @Mock
    private CommerceCategoryService commerceCategoryService;

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        given(product.getName()).willReturn(PRODUCT_NAME);
        given(colourVariant.getName()).willReturn(PRODUCT_NAME);
        given(colourVariant.getDisplayName()).willReturn(COLOUR_VARIANT_DISPLAY_NAME);

        willReturn(TITLE_SUFFIX).given(pageTitleResolver).getPageTitleSuffix();
        willReturn(TITLE_SEPARATOR).given(pageTitleResolver).getPageTitleSeparator();
    }

    /**
     * Verifies that search page title includes current page title + site name.
     */
    @Test
    public void resolveSearchPageTitle() {
        final CMSSiteModel currentSite = Mockito.mock(CMSSiteModel.class);
        given(currentSite.getName()).willReturn(SITE_NAME);
        given(cmsSiteService.getCurrentSite()).willReturn(currentSite);
        assertThat("<test> | Target").isEqualTo(pageTitleResolver.resolveSearchPageTitle("<test>"));
    }

    /**
     * Verifies that site name is returned as a page title, when page title is not available.
     */
    @Test
    public void resolveSearchPageTitleAsNull() {
        final CMSSiteModel currentSite = Mockito.mock(CMSSiteModel.class);
        given(currentSite.getName()).willReturn(SITE_NAME);
        given(cmsSiteService.getCurrentSite()).willReturn(currentSite);
        assertThat(SITE_NAME).isEqualTo(pageTitleResolver.resolveSearchPageTitle(null));
    }

    /**
     * Verifies that base product name might be returned as page title.
     */
    @Test
    public void testResolveProductTitle() {
        assertThat(PRODUCT_NAME + " | " + TITLE_SUFFIX).isEqualTo(
                pageTitleResolver.resolveProductPageTitle(product));
    }

    /**
     * Verifies that product variant display name might be returned as a page title.
     */
    @Test
    public void testResolveProductTitleForColourVariant() {
        assertThat(COLOUR_VARIANT_DISPLAY_NAME + " | " + TITLE_SUFFIX)
                .isEqualTo(pageTitleResolver.resolveProductPageTitle(colourVariant));
    }

    /**
     * Verifies that store name might be returned as a page title.
     */
    @Test
    public void testResolveStorePageTitleForStore() {
        assertThat("Waurn Ponds" + " | " + TITLE_SUFFIX).isEqualTo(
                pageTitleResolver.resolveStorePageTitle("Waurn Ponds"));
    }

    /**
     * Verifies that only suffix is returned as a page title, when page title is not available.
     */
    @Test
    public void resolveStorePageTitleAsNull() {
        assertThat(TITLE_SUFFIX).isEqualTo(pageTitleResolver.resolveStorePageTitle(null));
    }

    @Test
    public void testResolveDealCategoryPageTitle() {
        final TargetDealCategoryModel dealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        dealModel.setPageTitle("test title");
        given(targetDealService.getDealByCategory(dealCategoryModel)).willReturn(dealModel);
        assertThat("test title").isEqualTo(pageTitleResolver.resolveDealCategoryPageTitle(dealCategoryModel));
    }

    @Test
    public void testResolveDealCategoryPageTitleWithNullCategoryModel() {
        final TargetDealCategoryModel dealCategoryModel = null;
        assertThat(TITLE_SUFFIX).isEqualTo(pageTitleResolver.resolveDealCategoryPageTitle(dealCategoryModel));
    }

    @Test
    public void testResolveDealCategoryPageTitleWithNullDealModel() {
        final TargetDealCategoryModel dealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = null;
        given(targetDealService.getDealByCategory(dealCategoryModel)).willReturn(dealModel);
        assertThat(TITLE_SUFFIX).isEqualTo(pageTitleResolver.resolveDealCategoryPageTitle(dealCategoryModel));
    }

    @Test
    public void testResolveDealCategoryPageTitleWithNullPageTitle() {
        final TargetDealCategoryModel dealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        dealModel.setPageTitle(StringUtils.EMPTY);
        given(targetDealService.getDealByCategory(dealCategoryModel)).willReturn(dealModel);
        assertThat(TITLE_SUFFIX).isEqualTo(pageTitleResolver.resolveDealCategoryPageTitle(dealCategoryModel));
    }

    @Test
    public void testResolveCategoryPageTitleWheMetaTitleAvailable() {
        final TargetProductCategoryModel categoryModel = new TargetProductCategoryModel();
        categoryModel.setMetaPageTitle("Meta title");

        final String pageTitle = pageTitleResolver.resolveCategoryPageTitle(categoryModel, "black");
        assertThat("black | Meta title").isEqualTo(pageTitle);
    }

    @Test
    public void testResolveCategoryPageTitleWheNoMetaTitleAvailable() {
        final TargetProductCategoryModel categoryModel = new TargetProductCategoryModel();

        final CategoryModel categoryModelOne = mock(CategoryModel.class);
        final CategoryModel categoryModelTwo = mock(CategoryModel.class);
        final List<CategoryModel> listOfCategory = mock(List.class);

        willReturn(Boolean.FALSE).given(listOfCategory).isEmpty();
        given(categoryModelOne.getName()).willReturn("cat1");
        given(categoryModelOne.getSupercategories()).willReturn(listOfCategory);
        given(categoryModelTwo.getName()).willReturn("cat2");
        given(categoryModelTwo.getSupercategories()).willReturn(listOfCategory);

        final List<CategoryModel> path = new ArrayList<>();
        path.add(categoryModelTwo);
        path.add(categoryModelOne);

        given(commerceCategoryService.getPathsForCategory(categoryModel)).willReturn(ImmutableList.of(path));

        final String pageTitle = pageTitleResolver.resolveCategoryPageTitle(categoryModel, "black");
        assertThat("black | cat1 | cat2 | Target Australia").isEqualTo(pageTitle);
    }

}
