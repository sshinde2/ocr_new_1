/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Pradeep
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CookieRetrievalHelperTest {

    private static final String TARGET_ANONYMOUS_TOKEN = "targetAnonymousToken";
    private static final String GUID = "guid";
    private final CookieRetrievalHelper cookieRetrievalHelper = new CookieRetrievalHelper();

    @Mock
    private HttpServletRequest request;


    @Test
    public void testGetCookieValueNullRequest() {
        Assert.assertNull(cookieRetrievalHelper.getCookieValue(null, GUID));

    }

    @Test
    public void testGetCookieValueNullCookieName() {
        Assert.assertNull(cookieRetrievalHelper.getCookieValue(request, null));

    }

    @Test
    public void testGetCookieValueNullCookies() {
        Mockito.doReturn(null).when(request).getCookies();
        Assert.assertNull(cookieRetrievalHelper.getCookieValue(request, TARGET_ANONYMOUS_TOKEN));

    }


    @Test
    public void test() {
        final Cookie cookie = new Cookie(TARGET_ANONYMOUS_TOKEN, GUID);
        final Cookie[] cookies = { cookie };
        Mockito.doReturn(cookies).when(request).getCookies();
        Assert.assertTrue(GUID.equals(cookieRetrievalHelper.getCookieValue(request, TARGET_ANONYMOUS_TOKEN)));

    }
}
