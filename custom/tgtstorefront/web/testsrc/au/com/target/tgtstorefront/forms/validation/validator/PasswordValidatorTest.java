/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtstorefront.forms.validation.Password;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class PasswordValidatorTest {
    @Mock
    protected MessageSource messageSource;

    @Mock
    protected I18NService i18nService;

    @Mock
    protected Password password;

    @Mock
    protected ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @Mock
    private TargetCustomerFacade mockTargetCustomerFacade;

    @InjectMocks
    private final PasswordValidator passwordValidator = new PasswordValidator();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .thenReturn(constraintViolationBuilder);

        doReturn(Boolean.TRUE).when(password).checkBlacklist();
        passwordValidator.initialize(password);
    }

    @Test
    public void testIsValidWithBlacklistedPassword() {
        doReturn(Boolean.TRUE).when(mockTargetCustomerFacade).isPasswordBlacklisted("password");
        assertThat(passwordValidator.isValid("password", constraintValidatorContext)).isFalse();
    }

    @Test
    public void testIsValidWithNotBlacklistedPassword() {
        doReturn(Boolean.FALSE).when(mockTargetCustomerFacade).isPasswordBlacklisted("password");
        assertThat(passwordValidator.isValid("password", constraintValidatorContext)).isTrue();
    }

}
