package au.com.target.tgtstorefront.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.AuthenticationException;

import au.com.target.tgtfacades.login.TargetAuthenticationFacade;


@RunWith(MockitoJUnitRunner.class)
public class LoginAuthenticationFailureHandlerTest {
    @Mock
    private TargetAuthenticationFacade mockTargetAuthenticationFacade;

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    @Mock
    private AuthenticationException mockAuthenticationException;

    @InjectMocks
    private final LoginAuthenticationFailureHandler loginAuthenticationFailureHandler = new LoginAuthenticationFailureHandler();

    @Test
    public void testOnAuthenticationFailure() throws Exception {
        final String username = "user@domain.com";

        final HttpSession mockSession = Mockito.mock(HttpSession.class);
        BDDMockito.given(mockRequest.getSession()).willReturn(mockSession);
        BDDMockito.given(mockRequest.getParameter("j_username")).willReturn(username);

        loginAuthenticationFailureHandler.onAuthenticationFailure(mockRequest, mockResponse,
                mockAuthenticationException);

        Mockito.verify(mockSession).setAttribute("SPRING_SECURITY_LAST_USERNAME", username);
        Mockito.verify(mockTargetAuthenticationFacade).incrementFailedLoginAttemptCounter(username);
    }

    @Test
    public void testOnAuthenticationFailureWithBlankUsername() throws Exception {
        final String username = "";

        final HttpSession mockSession = Mockito.mock(HttpSession.class);
        BDDMockito.given(mockRequest.getSession()).willReturn(mockSession);
        BDDMockito.given(mockRequest.getParameter("j_username")).willReturn(username);

        loginAuthenticationFailureHandler.onAuthenticationFailure(mockRequest, mockResponse,
                mockAuthenticationException);

        Mockito.verify(mockSession).setAttribute("SPRING_SECURITY_LAST_USERNAME", username);
        Mockito.verifyZeroInteractions(mockTargetAuthenticationFacade);
    }

    @Test
    public void testOnAuthenticationFailureWithNullUsername() throws Exception {
        final String username = null;

        final HttpSession mockSession = Mockito.mock(HttpSession.class);
        BDDMockito.given(mockRequest.getSession()).willReturn(mockSession);
        BDDMockito.given(mockRequest.getParameter("j_username")).willReturn(username);

        loginAuthenticationFailureHandler.onAuthenticationFailure(mockRequest, mockResponse,
                mockAuthenticationException);

        Mockito.verify(mockSession).setAttribute("SPRING_SECURITY_LAST_USERNAME", username);
        Mockito.verifyZeroInteractions(mockTargetAuthenticationFacade);
    }
}
