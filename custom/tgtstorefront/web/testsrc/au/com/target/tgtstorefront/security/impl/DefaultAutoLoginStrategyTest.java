/**
 * 
 */
package au.com.target.tgtstorefront.security.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.RememberMeServices;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultAutoLoginStrategyTest {

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private TargetCookieStrategy guidCookieStrategy;

    @Mock
    private RememberMeServices rememberMeServices;

    @Mock
    private EnhancedCookieGenerator anonymousCartTokenGenerator;

    @Mock
    private CartService cartService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetOrderFacade targetOrderFacade;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @InjectMocks
    private final AutoLoginStrategy autoLoginStrategy = new DefaultAutoLoginStrategy();

    @Before
    public void setup() {
        given(request.getSession()).willReturn(session);
    }

    @Test
    public void testLoginGuestUserCleanup() {
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        final CartModel cartModel = mock(CartModel.class);
        final TargetCustomerModel guestUser = mock(TargetCustomerModel.class);
        given(guestUser.getType()).willReturn(CustomerType.GUEST);
        given(cartModel.getUser()).willReturn(guestUser);
        given(cartService.getSessionCart()).willReturn(cartModel);

        autoLoginStrategy.login("user", "pass", request, response);
        verify(modelService).remove(guestUser);
        verify(targetOrderFacade, never()).linkOrderToCustomer(Mockito.anyString());
    }

    @Test
    public void testLoginNoGuesUserCleanupWhenNull() {
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        final CartModel cartModel = mock(CartModel.class);
        given(cartService.getSessionCart()).willReturn(cartModel);

        autoLoginStrategy.login("user", "pass", request, response);
        verify(modelService, never()).remove(any());
        verify(targetOrderFacade, never()).linkOrderToCustomer(Mockito.anyString());
    }

    @Test
    public void testLoginDoesNotCleanupNonGuestUser() {
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        final CartModel cartModel = mock(CartModel.class);
        final TargetCustomerModel guestUser = mock(TargetCustomerModel.class);
        given(cartModel.getUser()).willReturn(guestUser);
        given(cartService.getSessionCart()).willReturn(cartModel);

        autoLoginStrategy.login("user", "pass", request, response);
        verify(modelService, never()).remove(any());
        verify(targetOrderFacade, never()).linkOrderToCustomer(Mockito.anyString());
    }

    @Test
    public void testLinkOrderToCustomer() {
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("1234");
        autoLoginStrategy.login("user", "password", request, response);
        verify(targetOrderFacade).linkOrderToCustomer("1234");
    }

}
