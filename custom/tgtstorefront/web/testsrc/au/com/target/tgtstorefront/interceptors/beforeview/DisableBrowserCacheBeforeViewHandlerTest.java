/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author bhuang3
 *
 */
public class DisableBrowserCacheBeforeViewHandlerTest {

    private final DisableBrowserCacheBeforeViewHandler beforeViewHandler = new DisableBrowserCacheBeforeViewHandler();

    @Test
    public void testBeforeViewWithStartWithCheckoutUrl() {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final ModelAndView modelAndView = mock(ModelAndView.class);
        when(request.getServletPath()).thenReturn("/checkout/your-address");
        beforeViewHandler.beforeView(request, response, modelAndView);
        verify(response).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        verify(response).setDateHeader("Expires", -1);
    }

    @Test
    public void testBeforeViewWithOutCheckoutInUrl() {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final ModelAndView modelAndView = mock(ModelAndView.class);
        when(request.getServletPath()).thenReturn("/basket");
        beforeViewHandler.beforeView(request, response, modelAndView);
        verify(response, times(0)).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        verify(response, times(0)).setDateHeader("Expires", -1);
    }

    @Test
    public void testBeforeViewWithCheckoutInUrl() {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final ModelAndView modelAndView = mock(ModelAndView.class);
        when(request.getServletPath()).thenReturn("/help/checkout/stuff");
        beforeViewHandler.beforeView(request, response, modelAndView);
        verify(response, times(0)).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        verify(response, times(0)).setDateHeader("Expires", -1);
    }

    @Test
    public void testBeforeViewWithNullRequest() {
        final HttpServletRequest request = null;
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final ModelAndView modelAndView = mock(ModelAndView.class);
        beforeViewHandler.beforeView(request, response, modelAndView);
        verify(response, times(0)).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        verify(response, times(0)).setDateHeader("Expires", -1);
    }
}
