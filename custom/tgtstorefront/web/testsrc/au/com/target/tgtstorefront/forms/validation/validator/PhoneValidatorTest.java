/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Locale;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.Phone;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class PhoneValidatorTest {

    @Mock
    private Phone phone;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @Mock
    private MessageSource mockMessageSource;

    @Mock
    private I18NService mockI18nService;

    @InjectMocks
    private final PhoneValidator phoneValidator = new PhoneValidator();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
        phoneValidator.initialize(phone);

        BDDMockito.given(mockI18nService.getCurrentLocale()).willReturn(Locale.ENGLISH);
        BDDMockito.given(mockMessageSource.getMessage("field.phone", null, Locale.ENGLISH)).willReturn(
                "Phone Number");
    }

    @Test
    public void correctPhoneNumber() {
        Assert.assertTrue(phoneValidator.isValidCommon("+(61)(02) 12345678", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("(02) 12345678", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("+(61) 02-12345678", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("+612 12345678", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("0212345678", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("212345678", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("+123456789012345", constraintValidatorContext));
        Assert.assertTrue(phoneValidator.isValidCommon("1234567890123456 ", constraintValidatorContext));
    }

    @Test
    public void inCorrectPhoneNumber() {
        Assert.assertFalse(phoneValidator.isValidCommon("", constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon("(+610212)345678", constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon(null, constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon("(+61) 02- 12345678", constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon("61 +02 12345678", constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon("(+61)(02) 1234567", constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon("+1234567890123456", constraintValidatorContext));
        Assert.assertFalse(phoneValidator.isValidCommon("1234567890123456+", constraintValidatorContext));
    }

    @Test
    public void testPhoneNumberRange() {
        Assert.assertTrue(phoneValidator.isSizeOutOfRange("+123 (456) 789 (0123456)"));
        Assert.assertTrue(phoneValidator.isSizeOutOfRange("(123) (456) 789 (01234567)"));
        Assert.assertTrue(phoneValidator.isSizeOutOfRange("123 (456) 789 (0123456) 7"));

        Assert.assertFalse(phoneValidator.isSizeOutOfRange("+123 (456) 789 (012345)"));
        Assert.assertFalse(phoneValidator.isSizeOutOfRange("(123) (456) 789 (0123456)"));
        Assert.assertFalse(phoneValidator.isSizeOutOfRange("123 (456) 789 (012345) 6"));
    }
}
