/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;


import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.MAggrERec;
import com.endeca.navigation.MAggrERecList;
import com.endeca.navigation.Navigation;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaRecords;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.endeca.infront.querybuilder.EndecaQueryBuilder;
import au.com.target.endeca.infront.util.EndecaQueryConnecton;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.GenericSortModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.search.TargetProductHideStrategy;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;



/**
 * Unit test for {@link ProductCarouselComponentController}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductCarouselComponentControllerTest {
    private static final String CODE_PRODUCT = "codeProdProduct";
    private static final String COMPONENT_TITLE = "componentTitle";
    private static final String CATEGORY_CODE = "cat123";
    private static final String BRAND_NAME = "Target";
    private static final String DIMENSION = "12345678";
    private static final String BRAND_DIMENSION = "4563";

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    @Mock
    private Configuration mockConfiguration;

    @Mock
    private HttpServletRequest request;

    @Mock
    private ProductCarouselComponentModel component;

    @Mock
    private TargetProductData productDataFromCategory;

    @Mock
    private EndecaPageAssemble endecaPageAssemble;

    @Mock
    private EndecaSearchStateData endecaSearchStateDataForProductSearch;

    @Mock
    private TargetProductHideStrategy conditionalOutOfStockHideStrategy;

    @Mock
    private EndecaQueryBuilder builder;

    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @Mock
    private EndecaQueryConnecton endecaQueryConnection;

    @Mock
    private HttpENEConnection queryConnection;

    @Mock
    private ENEQueryResults eneQueryResults;

    @Mock
    private Navigation navigation;

    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;
    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @Mock
    private BrandModel brandModel;

    @InjectMocks
    private final ProductCarouselComponentController productCarouselComponentController = new ProductCarouselComponentController();

    @SuppressWarnings("boxing")
    @Before
    public void setUp() throws TargetEndecaException, ENEQueryException {
        given(configurationService.getConfiguration()).willReturn(mockConfiguration);
        given(mockConfiguration.getInt(Mockito.eq("storefront.carousel.search.pagesize"), Mockito.anyInt()))
                .willReturn(100);
        given(component.getTitle()).willReturn(COMPONENT_TITLE);
        given(endecaQueryConnection.getConection()).willReturn(queryConnection);
        given(endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                CATEGORY_CODE)).willReturn(DIMENSION);
        given(endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND,
                BRAND_NAME)).willReturn(BRAND_DIMENSION);
    }

    @Test
    public void testFillModelGivenLinkedProductInStock() throws JAXBException, TransformerException, IOException,
            ENEQueryException {
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final ProductModel productModel = mock(ProductModel.class);
        final ProductData productDataProducts = mock(ProductData.class);
        final StockData stockData = mock(StockData.class);
        final AggrERecList aggrERecs = new MAggrERecList();
        final AggrERec aggrERec = new MAggrERec();
        final Model model = new ExtendedModelMap();
        aggrERecs.add(aggrERec);

        given(colourVariantProductModel.getBaseProduct()).willReturn(productModel);
        given(productModel.getCode()).willReturn(CODE_PRODUCT);
        given(productDataProducts.getStock()).willReturn(stockData);
        given(stockData.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        given(queryConnection.query(Mockito.any(ENEQuery.class))).willReturn(eneQueryResults);
        given(eneQueryResults.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(aggrERecs);
        given(aggrERecToProductConverter.convert(Mockito.any(AggrERec.class))).willReturn(
                createProductListerDataWithVariants("1234_blue", "2345_blac;"));

        productCarouselComponentController.fillModelInternal(request, model, component);

        Assert.assertTrue(model.containsAttribute("productData"));
        Assert.assertNotNull(model.asMap().get("productData"));

        final List<TargetProductListerData> products = (ArrayList<TargetProductListerData>)model.asMap().get(
                "productData");
        Assert.assertTrue(products.isEmpty());
    }

    @Test
    public void testCollectCategoryAndBrandProductsGivenLinkedCategoryWithProduct() throws JAXBException,
            TransformerException,
            IOException, ENEQueryException {
        setupCategory();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add(CATEGORY_CODE);

        final AggrERecList aggrERecs = new MAggrERecList();
        final AggrERec aggrERec = new MAggrERec();
        aggrERecs.add(aggrERec);

        final StockData stockData = new StockData();
        stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);

        given(productDataFromCategory.getStock()).willReturn(stockData);

        given(eneQueryResults.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(aggrERecs);
        given(aggrERecToProductConverter.convert(Mockito.any(AggrERec.class))).willReturn(
                createProductListerDataWithVariants("1234_blue", "2345_blac"));

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setCategoryCodes(categoryCodes);
        given(component.getBrand()).willReturn(brandModel);
        given(brandModel.getName()).willReturn(BRAND_NAME);
        given(endecaProductQueryBuilder.getQueryResults(endecaSearchStateData, null, 100)).willReturn(
                eneQueryResults);
        final EndecaSearchStateData searchStateData = productCarouselComponentController
                .collectCategoryAndBrandProducts(component,
                        productCarouselComponentController.getCarouselSearchPageSize(), endecaSearchStateData);
        final List<TargetProductListerData> results = productCarouselComponentController
                .getTargetProductsList(searchStateData, null, false, false);
        Assert.assertNotNull(searchStateData.getBrandCode());
        Assert.assertEquals(BRAND_DIMENSION, searchStateData.getBrandCode());
        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());
    }

    @Test
    public void testCollectCategoryAndBrandProductsBrandWithUnsupportedDimension() throws JAXBException,
            TransformerException,
            IOException, ENEQueryException {
        setupCategory();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add(CATEGORY_CODE);

        final AggrERecList aggrERecs = new MAggrERecList();
        final AggrERec aggrERec = new MAggrERec();
        aggrERecs.add(aggrERec);

        final StockData stockData = new StockData();
        stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);

        given(productDataFromCategory.getStock()).willReturn(stockData);

        given(eneQueryResults.getNavigation()).willReturn(navigation);
        given(navigation.getAggrERecs()).willReturn(aggrERecs);
        given(aggrERecToProductConverter.convert(Mockito.any(AggrERec.class))).willReturn(
                createProductListerDataWithVariants("1234_blue", "2345_blac"));

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setCategoryCodes(categoryCodes);
        given(component.getBrand()).willReturn(brandModel);
        given(brandModel.getName()).willReturn("Apple");
        given(endecaProductQueryBuilder.getQueryResults(endecaSearchStateData, null, 100)).willReturn(
                eneQueryResults);
        final EndecaSearchStateData searchStateData = productCarouselComponentController
                .collectCategoryAndBrandProducts(component,
                        productCarouselComponentController.getCarouselSearchPageSize(), endecaSearchStateData);
        final List<TargetProductListerData> results = productCarouselComponentController
                .getTargetProductsList(searchStateData, null, false, false);
        Assert.assertNull(searchStateData.getBrandCode());
        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());
    }

    @Test
    public void testCollectCategoryAndBrandProductsWithSortOrder() throws JAXBException, TransformerException,
            IOException {
        final String sortCode = "price-desc";

        setupCategory();

        final GenericSortModel mockSolrSort = mock(GenericSortModel.class);
        given(mockSolrSort.getCode()).willReturn(sortCode);
        given(component.getSortBy()).willReturn(mockSolrSort);

        setupDataForCategorySearch();

        final EndecaSearchStateData endecaSearchStateData = productCarouselComponentController
                .collectCategoryAndBrandProducts(
                        component,
                        productCarouselComponentController.getCarouselSearchPageSize(), null);
        final Map<String, Integer> sortStateMap = endecaSearchStateData.getSortStateMap();
        Assert.assertNotNull(sortStateMap);
        Assert.assertTrue(sortStateMap.containsKey("price"));
        Assert.assertEquals(Integer.valueOf(1), sortStateMap.get("price"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAddProductDataIfItCanBeShown() {
        final List<TargetProductListerData> filteredProductDataList = new ArrayList<>();
        final List<TargetProductListerData> unfilteredProductDataList = new ArrayList<>();
        final TargetProductListerData productA = new TargetProductListerData();
        final StockData stockDataA = new StockData();
        stockDataA.setStockLevelStatus(StockLevelStatus.INSTOCK);
        productA.setStock(stockDataA);
        given(conditionalOutOfStockHideStrategy.hideThisProduct(productA)).willReturn(false);
        final TargetProductListerData productB = new TargetProductListerData();
        final StockData stockDataB = new StockData();
        stockDataB.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        productB.setStock(stockDataB);
        given(conditionalOutOfStockHideStrategy.hideThisProduct(productB)).willReturn(true);
        final TargetProductListerData productC = new TargetProductListerData();
        productC.setStock(stockDataA);
        given(conditionalOutOfStockHideStrategy.hideThisProduct(productC)).willReturn(false);

        unfilteredProductDataList.add(productA);
        unfilteredProductDataList.add(productB);
        unfilteredProductDataList.add(productC);

        productCarouselComponentController.addProductDataIfItCanBeShown(unfilteredProductDataList,
                filteredProductDataList);
        Assert.assertEquals(2, filteredProductDataList.size());
    }

    @Test
    public void testSortProductsBasedOnSelected() {
        final TargetProductListerData productListerData = createProductListerDataWithVariants("1234_blue",
                "2345_black", "3456_red",
                "4567_brown");
        final List<ProductModel> productModels = createProductModels("8765_green", "1234_blue", "9876_grey");

        given(
                Boolean.valueOf(conditionalOutOfStockHideStrategy.hideThisProduct(Mockito
                        .any(TargetVariantProductListerData.class)))).willReturn(Boolean.FALSE);

        productCarouselComponentController.sortProductVariants(productListerData, productModels, false);
        Assertions.assertThat(productListerData).isNotNull();
        final List<TargetVariantProductListerData> targetVariantProductListerData = productListerData
                .getTargetVariantProductListerData();
        Assertions.assertThat(targetVariantProductListerData).isNotNull().isNotEmpty()
                .hasSize(4);
        Assertions.assertThat(targetVariantProductListerData.get(0).getColourVariantCode()).isNotNull().isNotEmpty()
                .isEqualTo("1234_blue");

    }

    @Test
    public void testSortProductsBasedOnSelectedVariantOutOfStock() {
        final TargetProductListerData productListerData = createProductListerDataWithVariants(
                "2345_black", "3456_red",
                "4567_brown");
        final TargetVariantProductListerData listerVariant = new TargetVariantProductListerData();
        listerVariant.setColourVariantCode("1234_blue");
        listerVariant.setInStock(false);
        listerVariant.setShowWhenOutOfStock(false);
        final List<TargetVariantProductListerData> targetVariantProductListerData1 = productListerData
                .getTargetVariantProductListerData();
        targetVariantProductListerData1.add(3, listerVariant);
        Assertions.assertThat(targetVariantProductListerData1).hasSize(4);
        Assertions.assertThat(targetVariantProductListerData1.get(3).getColourVariantCode()).isEqualTo("1234_blue");
        final List<ProductModel> productModels = createProductModels("8765_green", "1234_blue", "9876_grey");

        given(
                Boolean.valueOf(conditionalOutOfStockHideStrategy.hideThisProduct(Mockito
                        .any(TargetVariantProductListerData.class)))).willReturn(Boolean.TRUE);

        productCarouselComponentController.sortProductVariants(productListerData, productModels, false);
        Assertions.assertThat(productListerData).isNotNull();
        final List<TargetVariantProductListerData> targetVariantProductListerData = targetVariantProductListerData1;
        Assertions.assertThat(targetVariantProductListerData).isNotNull().isNotEmpty()
                .hasSize(4);
        Assertions.assertThat(targetVariantProductListerData.get(3).getColourVariantCode()).isNotNull().isNotEmpty()
                .isEqualTo("1234_blue");

    }


    /**
     * @param productCodes
     * @return List of Products
     */
    private List<ProductModel> createProductModels(final String... productCodes) {
        final List<ProductModel> productModels = new ArrayList<>();
        for (final String productCode : productCodes) {
            final VariantProductModel variant = new VariantProductModel();
            variant.setCode(productCode);
            productModels.add(variant);
        }
        return productModels;
    }

    /**
     * @param variants
     *            to be created
     * @return List of ProductListerData
     */
    private TargetProductListerData createProductListerDataWithVariants(final String... variants) {
        final TargetProductListerData listerData = new TargetProductListerData();
        final List<TargetVariantProductListerData> listerVariants = new ArrayList<>();
        for (final String variant : variants) {
            final TargetVariantProductListerData listerVariant = new TargetVariantProductListerData();
            listerVariant.setColourVariantCode(variant);
            listerVariant.setInStock(true);
            listerVariant.setShowWhenOutOfStock(true);
            listerVariants.add(listerVariant);
        }
        listerData.setTargetVariantProductListerData(listerVariants);
        return listerData;
    }

    private void setupCategory() {
        // Setup a category related to the component
        final CategoryModel categoryModel = mock(CategoryModel.class);
        given(component.getCategories()).willReturn(Collections.singletonList(categoryModel));
        given(categoryModel.getCode()).willReturn(CATEGORY_CODE);
    }

    private EndecaSearch setupDataForCategorySearch() throws JAXBException, TransformerException,
            IOException {

        // Search returns some data

        final EndecaSearch searchData = Mockito.mock(EndecaSearch.class);
        final EndecaRecords endecaRecords = Mockito.mock(EndecaRecords.class);
        given(searchData.getRecords()).willReturn(endecaRecords);
        final EndecaProduct endecaProduct = Mockito.mock(EndecaProduct.class);
        final List<EndecaProduct> productList = Collections.singletonList(endecaProduct);
        given(endecaRecords.getProduct()).willReturn(productList);
        return searchData;

    }
}
