/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.exception;

/**
 * @author vmuthura
 * 
 */
public class PayPalServiceException extends Exception
{
    /**
     * 
     */
    public PayPalServiceException()
    {
        super();
    }

    /**
     * @param message
     *            - message explaining the cause
     */
    public PayPalServiceException(final String message)
    {
        super(message);
    }

    /**
     * @param message
     *            - message explaining the cause
     * @param cause
     *            - the root cause
     */
    public PayPalServiceException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param cause
     *            - the root cause
     */
    public PayPalServiceException(final Throwable cause)
    {
        super(cause);
    }
}
