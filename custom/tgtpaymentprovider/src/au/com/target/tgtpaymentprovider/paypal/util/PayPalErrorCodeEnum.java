/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.util;

/**
 * A PayPal errorcode mapping(range) enumeration. Error codes are documented here:
 * https://www.x.com/developers/paypal/documentation-tools/api/errorcodes
 * 
 * @author vmuthura
 * 
 */
public enum PayPalErrorCodeEnum
{
    //General API Errors
    AUTH_FAILED(10002),
    VERSION_ERROR(10006),
    SECURITY_ERROR(10008),
    API_CALL_RATE_LIMITED(10014),
    API_TEMPORARILY_NOT_AVAILABLE(10101),

    //Validation Errors
    MISSING_PARAMETER_UNKNOWN(81000),
    INVALID_PARAMETER_UNKNOWN(81001),
    UNSPECIFIED_METHOD(81002, 81004),
    MISSING_PARAMETER(81100, 81200),
    INVALID_PARAMETER(81201, 81250),
    INTERNAL_ERROR_VALIDATION(81251),
    CURRENCY_NOT_SUPPORTED(99998),

    //SetExpressCheckout/GetExpressCheckout/GetTransactionDetails API Errors
    INTERNAL_ERROR(10001),
    MISSING_ARGUMENT(10003),
    INVALID_ARGUMENT(10004),
    PERMISSION_DENIED(10007),
    EC_TOKEN_MISSING(10408),
    UNAUTHORIZED_ACCESS(10409),
    EC_TOKEN_INVLAID(10410),
    EC_TOKEN_EXPIRED(10411),
    BUYER_HAS_NO_FUNDS(13112),
    TXN_IN_PROGRESS_FOR_TOKEN(13116),

    //DoExpressCheckoutPayment API Errors
    PAYERID_INVALID(10406),
    DUPLICATE_INVOICE(10412),
    BAD_FUNDING_METHOD(10486), // This error code notifies merchants when a buyer's payment failed due to a bad funding method (typically an invalid or maxed out credit card).

    //RefundTransaction API Errors
    UNSUPPORTED_OPTION(10005),
    TXN_REFUSED(10009),
    INVALID_TXN_ID(10011);


    private int start;
    private int finish;

    /**
     * @param code
     *            -
     */
    private PayPalErrorCodeEnum(final int code)
    {
        this.start = code;
        this.finish = code;
    }

    /**
     * @param start
     *            - range start
     * @param finish
     *            - range finish
     */
    private PayPalErrorCodeEnum(final int start, final int finish)
    {
        this.start = start;
        this.finish = finish;
    }

    /**
     * @param errorCode
     *            -
     * @return - {@link PayPalErrorCodeEnum}
     */
    public static PayPalErrorCodeEnum fromCode(final String errorCode)
    {
        final int code = Integer.parseInt(errorCode);
        for (final PayPalErrorCodeEnum payPalErrorCode : values())
        {
            if (code >= payPalErrorCode.start && code <= payPalErrorCode.finish)
            {
                return payPalErrorCode;
            }
        }

        return null;
    }

}
