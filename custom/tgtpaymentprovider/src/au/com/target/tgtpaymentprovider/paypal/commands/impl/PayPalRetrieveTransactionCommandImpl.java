/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import de.hybris.platform.payment.AdapterException;

import org.apache.log4j.Logger;

import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.apis.eBLBaseComponents.PayerInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentTransactionType;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.util.PayPal2HybrisResponseMap;


/**
 * @author vmuthura
 * 
 */
public class PayPalRetrieveTransactionCommandImpl extends AbstractPayPalCommand implements
        TargetRetrieveTransactionCommand {
    private static final Logger LOG = Logger.getLogger(PayPalRetrieveTransactionCommandImpl.class);

    @Override
    public TargetRetrieveTransactionResult perform(
            final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest)
    {
        if (targetRetrieveTransactionRequest == null)
        {
            throw new IllegalArgumentException("TargetRetrieveTransactionRequest is null");
        }

        final GetTransactionDetailsReq req = new GetTransactionDetailsReq();

        final GetTransactionDetailsRequestType request = new GetTransactionDetailsRequestType();
        request.setTransactionID(targetRetrieveTransactionRequest.getTransactionId());

        req.setGetTransactionDetailsRequest(request);

        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();
        try
        {
            final GetTransactionDetailsResponseType response = getPayPalService().retrieve(req);
            result.setReconciliationId(response.getCorrelationID());
            result.setTransactionStatus(PayPal2HybrisResponseMap.getHybrisTransactionStatus(response.getAck()));
            final PaymentTransactionType paymentTransDetails = response.getPaymentTransactionDetails();
            if (paymentTransDetails != null) {
                final PayerInfoType payerInfo = paymentTransDetails.getPayerInfo();
                if (payerInfo != null) {
                    result.setPayerId(payerInfo.getPayerID());
                }
                final PaymentInfoType paymentInfo = paymentTransDetails.getPaymentInfo();
                if (paymentInfo != null)
                {
                    result.setRequestId(paymentInfo.getTransactionID());
                    result.setTransactionStatusDetails(
                            PayPal2HybrisResponseMap.getHybrisTransactionStatusDetails(paymentInfo.getPaymentStatus()));
                    try {
                        result.setCapturedAmount(Double.valueOf(paymentInfo.getGrossAmount().getValue()));
                    }
                    catch (final NumberFormatException e) {
                        LOG.error("Failed to get captured amount", e);
                    }
                }
            }

            return result;
        }
        catch (final PayPalServiceException e)
        {
            throw new AdapterException(e);
        }
    }

}
