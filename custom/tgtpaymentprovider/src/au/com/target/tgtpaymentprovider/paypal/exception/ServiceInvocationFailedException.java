/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.exception;

/**
 * @author vmuthura
 * 
 */
public class ServiceInvocationFailedException extends Exception
{
    /**
     * 
     */
    public ServiceInvocationFailedException()
    {
        super();
    }

    /**
     * @param message
     *            - message explaining the cause
     */
    public ServiceInvocationFailedException(final String message)
    {
        super(message);
    }

    /**
     * @param throwable
     *            - the root cause
     */
    public ServiceInvocationFailedException(final Throwable throwable)
    {
        super(throwable);
    }

    /**
     * @param message
     *            - message explaining the cause
     * @param throwable
     *            - the root cause
     */
    public ServiceInvocationFailedException(final String message, final Throwable throwable)
    {
        super(message, throwable);
    }
}
