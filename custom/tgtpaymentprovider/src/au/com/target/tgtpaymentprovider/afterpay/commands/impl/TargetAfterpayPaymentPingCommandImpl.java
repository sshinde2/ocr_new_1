/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import au.com.target.tgtpayment.commands.TargetPaymentPingCommand;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;


/**
 * @author gsing236
 *
 */
public class TargetAfterpayPaymentPingCommandImpl extends TargetAbstractAfterpayCommand
        implements TargetPaymentPingCommand {

    @Override
    public TargetPaymentPingResult perform(final TargetPaymentPingRequest request) {
        final boolean afterpayConnectionAvailable = getTargetAfterpayClient().isAfterpayConnectionAvailable();

        final TargetPaymentPingResult result = new TargetPaymentPingResult();
        result.setSuccess(afterpayConnectionAvailable);
        return result;
    }
}
