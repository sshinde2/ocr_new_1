/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CapturePaymentRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;


/**
 * @author bpottass
 *
 */
public class AfterpayCapturePaymentCommandImpl extends TargetAbstractAfterpayCommand
        implements TargetCaptureCommand {


    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest paramRequest) {
        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetCaptureRequest is null");
        }
        try {
            final CapturePaymentResponse capturePaymentResponse = getTargetAfterpayClient()
                    .capturePayment(transalateRequest(paramRequest));

            return transalateResult(capturePaymentResponse);

        }
        catch (final TargetAfterpayClientException e) {
            throw new AdapterException(e);
        }

    }

    /**
     * 
     * @return TargetCaptureResult
     */
    private TargetCaptureResult transalateResult(final CapturePaymentResponse capturePaymentResponse) {
        final TargetCaptureResult targetCaptureResult = new TargetCaptureResult();
        if (capturePaymentResponse != null) {
            if (capturePaymentResponse.getHttpStatus().value() < 400) {
                mapPaymentResponse(capturePaymentResponse, targetCaptureResult);

            }
            else {
                targetCaptureResult.setTransactionStatus(TransactionStatus.REJECTED);
                targetCaptureResult.setTransactionStatusDetails(TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
                targetCaptureResult.setDeclineCode(capturePaymentResponse.getHttpStatus().toString());
                targetCaptureResult.setDeclineMessage(capturePaymentResponse.getHttpStatus().getReasonPhrase());
            }

        }
        return targetCaptureResult;
    }

    /**
     * @param paramRequest
     */
    private CapturePaymentRequest transalateRequest(final TargetCaptureRequest paramRequest) {
        final CapturePaymentRequest capturePaymentRequest = new CapturePaymentRequest();
        capturePaymentRequest.setToken(paramRequest.getToken());
        capturePaymentRequest.setMerchantReference(
                paramRequest.getOrder() != null ? paramRequest.getOrder().getOrderId() : null);
        return capturePaymentRequest;
    }


}
