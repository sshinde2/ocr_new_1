/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import au.com.target.tgtpayment.commands.result.TargetAbstractPaymentResult;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.data.response.AbstractPaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.util.AfterpayToHybrisResponseMap;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


/**
 * @author bhuang3
 *
 */
public class TargetAbstractAfterpayCommand {

    private TargetAfterpayClient targetAfterpayClient;

    private TargetAfterpayHelper targetAfterpayHelper;

    /**
     * @return the targetAfterpayClient
     */
    protected TargetAfterpayClient getTargetAfterpayClient() {
        return targetAfterpayClient;
    }

    /**
     * @param targetAfterpayClient
     *            the targetAfterpayClient to set
     */
    public void setTargetAfterpayClient(final TargetAfterpayClient targetAfterpayClient) {
        this.targetAfterpayClient = targetAfterpayClient;
    }

    /**
     * @return the targetAfterpayHelper
     */
    public TargetAfterpayHelper getTargetAfterpayHelper() {
        return targetAfterpayHelper;
    }

    /**
     * @param targetAfterpayHelper
     *            the targetAfterpayHelper to set
     */
    public void setTargetAfterpayHelper(final TargetAfterpayHelper targetAfterpayHelper) {
        this.targetAfterpayHelper = targetAfterpayHelper;
    }

    /**
     * common method (capture payment and get payment) to map payment response to result
     * 
     * @param capturePaymentResponse
     *            - {@link AbstractPaymentResponse}
     * @param targetCaptureResult
     *            - {@link TargetAbstractPaymentResult}
     */
    protected void mapPaymentResponse(final AbstractPaymentResponse capturePaymentResponse,
            final TargetAbstractPaymentResult targetCaptureResult) {
        targetCaptureResult.setRequestId(capturePaymentResponse.getId());
        targetCaptureResult.setRequestToken(capturePaymentResponse.getToken());
        targetCaptureResult.setTransactionStatus(
                AfterpayToHybrisResponseMap.getTransactionStatus(capturePaymentResponse.getStatus()));
        targetCaptureResult.setTransactionStatusDetails(
                AfterpayToHybrisResponseMap.getTransactionStatusDetails(capturePaymentResponse.getStatus()));
        targetCaptureResult
                .setRequestTime(
                        getTargetAfterpayHelper().convertStringToDate(capturePaymentResponse.getCreated()));
        targetCaptureResult.setTotalAmount(
                getTargetAfterpayHelper().convertMoneyToBigDecimal(capturePaymentResponse.getTotalAmount()));
        targetCaptureResult
                .setCurrency(
                        getTargetAfterpayHelper()
                                .getCurrencyInstanceFromMoney(capturePaymentResponse.getTotalAmount()));
        targetCaptureResult.setMerchantTransactionCode(capturePaymentResponse.getMerchantReference());
        targetCaptureResult.setReconciliationId(capturePaymentResponse.getId());
    }



}
