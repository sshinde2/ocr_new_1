package au.com.target.tgtpaymentprovider.zippay.response.types;

import org.codehaus.jackson.annotate.JsonProperty;


public abstract class CreateChargeResponse {

    @JsonProperty("receipt_number")
    abstract String getReceiptNumber();


}
