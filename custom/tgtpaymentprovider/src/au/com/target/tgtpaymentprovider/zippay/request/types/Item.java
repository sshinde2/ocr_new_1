/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.request.types;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * @author salexa10
 *
 */
public abstract class Item {

    @JsonProperty("image_uri")
    abstract String getImageUri();

}
