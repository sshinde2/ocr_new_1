/**
 *
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateChargeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionError;
import au.com.target.tgtpaymentprovider.zippay.data.request.Authority;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateChargeRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;


/**
 * @author ramsatish
 *
 */
public class TargetZippayCreateChargeCommandImpl extends TargetAbstractZippayCommand
    implements TargetCaptureCommand {


    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest paramRequest) {
        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetCreateChargeRequest is null");
        }
        try {
            final CreateChargeResponse createChargeResponse = getTargetZippayClient()
                .createCharge(translateRequest(paramRequest));
            return translateResponse(createChargeResponse);
        }

        // Check for error codes other than HTTP 200
        catch (final TargetZippayTransactionError targetZippayTransactionError) {
            return translateResponse(null);
        }
        // Otherwise
        catch (final TargetZippayClientException e) {
            throw new AdapterException(e);
        }
    }


    /**
     * @param paramRequest
     * @return {@link TargetCreateChargeRequest}
     */
    private CreateChargeRequest translateRequest(final TargetCaptureRequest paramRequest) {
        final CreateChargeRequest createChargeRequest = new CreateChargeRequest();
        final Order orderDto = paramRequest.getOrder();
        if (orderDto != null) {
            createChargeRequest.setCurrency(orderDto.getOrderTotalAmount().getCurrency().getCurrencyCode());
            createChargeRequest.setAmount(orderDto.getOrderTotalAmount().getAmount().doubleValue());
            }
        final Authority authority = new Authority();
        authority.setType(paramRequest.getAuthorityType());
        authority.setValue(paramRequest.getAuthorityValue());
        createChargeRequest.setAuthority(authority);
        createChargeRequest.setCapture(paramRequest.isCapture());
        return createChargeRequest;
    }



    /**
     * @param createChargeResponse
     * @return {@link TargetCaptureResult}
     */
    private TargetCaptureResult translateResponse(final CreateChargeResponse createChargeResponse) {
        final TargetCaptureResult targetCaptureResult = new TargetCaptureResult();
        if (createChargeResponse != null) {
            targetCaptureResult.setReconciliationId(createChargeResponse.getReceiptNumber());
            targetCaptureResult.setRequestId(createChargeResponse.getId());
            if (StringUtils.equals(TgtpaymentConstants.ZIP_CAPTURED_SUCCESS, createChargeResponse.getState())) {
                targetCaptureResult.setTransactionStatus(TransactionStatus.ACCEPTED);
                targetCaptureResult.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
                return targetCaptureResult;
            }
        }
        targetCaptureResult.setTransactionStatus(TransactionStatus.ERROR);
        targetCaptureResult.setTransactionStatusDetails(TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        targetCaptureResult.setDeclineCode(createChargeResponse != null ? createChargeResponse.getState() : null);
        return targetCaptureResult;
    }

}
