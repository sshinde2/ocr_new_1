/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.request;

import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;


/**
 * @author mjanarth
 *
 */
public class SubmitSingleRefundRequest {

    private RefundInfo refundInfo;

    /**
     * @return the refundInfo
     */
    public RefundInfo getRefundInfo() {
        return refundInfo;
    }

    /**
     * @param refundInfo
     *            the refundInfo to set
     */
    public void setRefundInfo(final RefundInfo refundInfo) {
        this.refundInfo = refundInfo;
    }

}
