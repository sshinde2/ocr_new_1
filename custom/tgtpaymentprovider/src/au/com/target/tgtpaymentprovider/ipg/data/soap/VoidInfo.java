package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Void")
@XmlAccessorType(XmlAccessType.FIELD)
public class VoidInfo {

    @XmlElement(name = "Receipt")
    private String receiptNumber;

    @XmlElement(name = "Security")
    private Security security;

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the security
     */
    public Security getSecurity() {
        return security;
    }

    /**
     * @param security
     *            the security to set
     */
    public void setSecurity(final Security security) {
        this.security = security;
    }
}