package au.com.target.tgtpaymentprovider.ipg.data.request;

import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;

public class SingleVoidRequest {

    private VoidInfo voidInfo;

    /**
     * @return the voidInfo
     */
    public VoidInfo getVoidInfo() {
        return voidInfo;
    }

    /**
     * @param voidInfo
     *            the voidInfo to set
     */
    public void setVoidInfo(final VoidInfo voidInfo) {
        this.voidInfo = voidInfo;
    }
}