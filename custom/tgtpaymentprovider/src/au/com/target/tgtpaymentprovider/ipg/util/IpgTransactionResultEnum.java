package au.com.target.tgtpaymentprovider.ipg.util;

import java.util.HashMap;
import java.util.Map;


/**
 * IPG transaction result status represented as enum
 */
public enum IpgTransactionResultEnum {
    DECLINED("0"),

    SUCCESSFUL("1"),

    SESSION_STILL_IN_PROGRESS("2"),

    SESSION_TIMED_OUT("3"),

    CANCELLED("4");

    protected static final Map<String, IpgTransactionResultEnum> LOOKUP = new HashMap<>();

    static {
        for (final IpgTransactionResultEnum transactionStatus : IpgTransactionResultEnum.values()) {
            LOOKUP.put(transactionStatus.value, transactionStatus);
        }
    }

    @SuppressWarnings("unused")
    private final String value;

    private IpgTransactionResultEnum(final String value) {
        this.value = value;
    }

    /**
     * Returns {@link IpgTransactionResultEnum} for given code
     * 
     * @param code
     * @return IPG transaction result enum
     */
    public static IpgTransactionResultEnum get(final String code) {
        return LOOKUP.get(code);
    }
}
