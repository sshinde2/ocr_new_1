/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionDetails {

    @XmlElement(name = "Amount")
    private String amount;
    @XmlElement(name = "TransType")
    private String transType;
    @XmlElement(name = "Receipt")
    private String receiptNumber;
    @XmlElement(name = "RespCode")
    private String responseCode;
    @XmlElement(name = "RespText")
    private String responseText;
    @XmlElement(name = "TrnDateTime")
    private String transDateTime;
    @XmlElement(name = "SettlementDate")
    private String settlementDate;

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the transType
     */
    public String getTransType() {
        return transType;
    }

    /**
     * @param transType
     *            the transType to set
     */
    public void setTransType(final String transType) {
        this.transType = transType;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText
     *            the responseText to set
     */
    public void setResponseText(final String responseText) {
        this.responseText = responseText;
    }

    /**
     * @return the transDateTime
     */
    public String getTransDateTime() {
        return transDateTime;
    }

    /**
     * @param transDateTime
     *            the transDateTime to set
     */
    public void setTransDateTime(final String transDateTime) {
        this.transDateTime = transDateTime;
    }

    /**
     * @return the settlementDate
     */
    public String getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate
     *            the settlementDate to set
     */
    public void setSettlementDate(final String settlementDate) {
        this.settlementDate = settlementDate;
    }


}
