/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ThreeDsAuthenticationData {

    @XmlElement(name = "Three3DSOrderNum")
    private String threeDsOrderNumber;
    @XmlElement(name = "ThreeDSSessToken")
    private String threeDsSessionToken;
    @XmlElement(name = "ECI")
    private String eci;
    @XmlElement(name = "VER")
    private String version;
    @XmlElement(name = "PAR")
    private String par;
    @XmlElement(name = "SigVerif")
    private String signVerification;
    @XmlElement(name = "CAVV")
    private String cavv;
    @XmlElement(name = "XID")
    private String xid;
    @XmlElement(name = "CAR")
    private String car;

    /**
     * @return the threeDsOrderNumber
     */
    public String getThreeDsOrderNumber() {
        return threeDsOrderNumber;
    }

    /**
     * @param threeDsOrderNumber
     *            the threeDsOrderNumber to set
     */
    public void setThreeDsOrderNumber(final String threeDsOrderNumber) {
        this.threeDsOrderNumber = threeDsOrderNumber;
    }

    /**
     * @return the threeDsSessionToken
     */
    public String getThreeDsSessionToken() {
        return threeDsSessionToken;
    }

    /**
     * @param threeDsSessionToken
     *            the threeDsSessionToken to set
     */
    public void setThreeDsSessionToken(final String threeDsSessionToken) {
        this.threeDsSessionToken = threeDsSessionToken;
    }

    /**
     * @return the eci
     */
    public String getEci() {
        return eci;
    }

    /**
     * @param eci
     *            the eci to set
     */
    public void setEci(final String eci) {
        this.eci = eci;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @return the par
     */
    public String getPar() {
        return par;
    }

    /**
     * @param par
     *            the par to set
     */
    public void setPar(final String par) {
        this.par = par;
    }

    /**
     * @return the signVerification
     */
    public String getSignVerification() {
        return signVerification;
    }

    /**
     * @param signVerification
     *            the signVerification to set
     */
    public void setSignVerification(final String signVerification) {
        this.signVerification = signVerification;
    }

    /**
     * @return the cavv
     */
    public String getCavv() {
        return cavv;
    }

    /**
     * @param cavv
     *            the cavv to set
     */
    public void setCavv(final String cavv) {
        this.cavv = cavv;
    }

    /**
     * @return the xid
     */
    public String getXid() {
        return xid;
    }

    /**
     * @param xid
     *            the xid to set
     */
    public void setXid(final String xid) {
        this.xid = xid;
    }

    /**
     * @return the car
     */
    public String getCar() {
        return car;
    }

    /**
     * @param car
     *            the car to set
     */
    public void setCar(final String car) {
        this.car = car;
    }

}
