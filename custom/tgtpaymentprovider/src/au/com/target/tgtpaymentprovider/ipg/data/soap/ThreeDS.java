/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author htan3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ThreeDS {
    @XmlElement(name = "ThreeDSOrderNumber")
    private String threeDsOrderNumber;
    @XmlElement(name = "TDSSessToken")
    private String threeDsSessionToken;

    /**
     * @return the threeDsOrderNumber
     */
    public String getThreeDsOrderNumber() {
        return threeDsOrderNumber;
    }

    /**
     * @param threeDsOrderNumber
     *            the threeDsOrderNumber to set
     */
    public void setThreeDsOrderNumber(final String threeDsOrderNumber) {
        this.threeDsOrderNumber = threeDsOrderNumber;
    }

    /**
     * @return the threeDsSessionToken
     */
    public String getThreeDsSessionToken() {
        return threeDsSessionToken;
    }

    /**
     * @param threeDsSessionToken
     *            the threeDsSessionToken to set
     */
    public void setThreeDsSessionToken(final String threeDsSessionToken) {
        this.threeDsSessionToken = threeDsSessionToken;
    }

}
