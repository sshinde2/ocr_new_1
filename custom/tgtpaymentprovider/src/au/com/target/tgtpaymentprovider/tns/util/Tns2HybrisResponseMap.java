/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.HashMap;
import java.util.Map;

import au.com.target.tgtpaymentprovider.tns.data.GatewayResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;


/**
 * @author bjames4
 * 
 */

public final class Tns2HybrisResponseMap
{

    //CHECKSTYLE:OFF
    private static Map<GatewayResponseEnum, TransactionStatusDetails> responseMap = new HashMap<GatewayResponseEnum, TransactionStatusDetails>();
    //CHECKSTYLE:ON
    private static Map<ResultEnum, TransactionStatus> resultMap = new HashMap<ResultEnum, TransactionStatus>();

    /**
     * 
     */
    private Tns2HybrisResponseMap()
    {
        //Avoid instantiation
    }

    static
    {
        // Init response map
        responseMap.put(GatewayResponseEnum.APPROVED, TransactionStatusDetails.SUCCESFULL);
        responseMap.put(GatewayResponseEnum.UNSPECIFIED_FAILURE, TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        responseMap.put(GatewayResponseEnum.DECLINED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(GatewayResponseEnum.TIMED_OUT, TransactionStatusDetails.TIMEOUT);
        responseMap.put(GatewayResponseEnum.EXPIRED_CARD, TransactionStatusDetails.INVALID_CARD_EXPIRATION_DATE);
        responseMap.put(GatewayResponseEnum.INSUFFICIENT_FUNDS, TransactionStatusDetails.INSUFFICIENT_FUNDS);
        responseMap.put(GatewayResponseEnum.ACQUIRER_SYSTEM_ERROR, TransactionStatusDetails.COMMUNICATION_PROBLEM);
        responseMap.put(GatewayResponseEnum.SYSTEM_ERROR, TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        responseMap.put(GatewayResponseEnum.NOT_SUPPORTED, TransactionStatusDetails.INACTIVE_OR_INVALID_CARD);
        responseMap.put(GatewayResponseEnum.DECLINED_DO_NOT_CONTACT, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(GatewayResponseEnum.ABORTED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(GatewayResponseEnum.BLOCKED, TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(GatewayResponseEnum.CANCELLED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(GatewayResponseEnum.DEFERRED_TRANSACTION_RECEIVED, TransactionStatusDetails.REVIEW_NEEDED);
        responseMap.put(GatewayResponseEnum.REFERRED, TransactionStatusDetails.REVIEW_NEEDED);
        responseMap.put(GatewayResponseEnum.AUTHENTICATION_FAILED,
                TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(GatewayResponseEnum.INVALID_CSC, TransactionStatusDetails.INVALID_CVN);
        responseMap.put(GatewayResponseEnum.LOCK_FAILURE, TransactionStatusDetails.PROCESSOR_DECLINE);
        responseMap.put(GatewayResponseEnum.SUBMITTED, TransactionStatusDetails.REVIEW_NEEDED);
        responseMap.put(GatewayResponseEnum.NOT_ENROLLED_3D_SECURE,
                TransactionStatusDetails.THREE_D_SECURE_NOT_SUPPORTED);
        responseMap.put(GatewayResponseEnum.PENDING, TransactionStatusDetails.REVIEW_NEEDED);
        responseMap.put(GatewayResponseEnum.EXCEEDED_RETRY_LIMIT, TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        responseMap.put(GatewayResponseEnum.DUPLICATE_BATCH, TransactionStatusDetails.DUPLICATE_TRANSACTION);
        responseMap.put(GatewayResponseEnum.DECLINED_AVS, TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(GatewayResponseEnum.DECLINED_CSC, TransactionStatusDetails.INVALID_CVN);
        responseMap.put(GatewayResponseEnum.DECLINED_AVS_CSC, TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(GatewayResponseEnum.DECLINED_PAYMENT_PLAN,
                TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(GatewayResponseEnum.UNKNOWN, TransactionStatusDetails.UNKNOWN_CODE);

        // Init resultMap
        resultMap.put(ResultEnum.SUCCESS, TransactionStatus.ACCEPTED);
        resultMap.put(ResultEnum.ERROR, TransactionStatus.ERROR);
        resultMap.put(ResultEnum.FAILURE, TransactionStatus.REJECTED);
        resultMap.put(ResultEnum.PENDING, TransactionStatus.REVIEW);
        resultMap.put(ResultEnum.UNKNOWN, TransactionStatus.ERROR);
    }

    /**
     * @param gatewayResponseEnum
     *            - {@link GatewayResponseEnum}
     * @return hybris {@link TransactionStatusDetails}
     */
    public static TransactionStatusDetails
            getHybrisTransactionStatusDetails(final GatewayResponseEnum gatewayResponseEnum)
    {
        return responseMap.get(gatewayResponseEnum);
    }

    /**
     * @param tnsResult
     *            - {@link ResultEnum}
     * @return hybris {@link TransactionStatus}
     */
    public static TransactionStatus getHybrisTransactionStatus(final ResultEnum tnsResult)
    {
        return resultMap.get(tnsResult);
    }
}
