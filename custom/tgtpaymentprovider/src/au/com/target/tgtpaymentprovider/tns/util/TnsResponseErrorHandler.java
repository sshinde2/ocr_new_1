/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;


/**
 * @author vmuthura
 * 
 */
public class TnsResponseErrorHandler extends DefaultResponseErrorHandler
{

    @Override
    public void handleError(final ClientHttpResponse response) throws IOException {
        final HttpStatus statusCode = response.getStatusCode();
        switch (statusCode.series().value())
        {
            case 4: // '\004'
                //throw new HttpClientErrorException(statusCode, response.getStatusText(), body, charset);
                break;

            case 5: // '\005'
                //throw new HttpServerErrorException(statusCode, response.getStatusText(), body, charset);
                break;
            default:
                throw new RestClientException(("Unknown status code [" + statusCode + "]"));
        }

    }

}
