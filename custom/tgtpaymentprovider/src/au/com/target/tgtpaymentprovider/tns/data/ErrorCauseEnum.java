/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author vmuthura
 * 
 */
public enum ErrorCauseEnum
{
    REQUEST_REJECTED, //The request was rejected due to security reasons such as firewall rules, expired certificate, etc.
    INVALID_REQUEST, //The request was rejected because it did not conform to the API protocol.
    SERVER_FAILED, //There was an internal system failure.
    SERVER_BUSY //The server did not have enough resources to process the request at the moment.
}