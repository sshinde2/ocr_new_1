/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author bjames4
 * 
 */
public class ErrorDetails {
    private ErrorCauseEnum cause;
    private String explanation;
    private String field;
    private String supportCode;
    private ValidationTypeEnum validationType;

    /**
     * @return the cause
     */
    public ErrorCauseEnum getCause() {
        return cause;
    }

    /**
     * @param cause
     *            the cause to set
     */
    public void setCause(final ErrorCauseEnum cause) {
        this.cause = cause;
    }

    /**
     * @return the explanation
     */
    public String getExplanation() {
        return explanation;
    }

    /**
     * @param explanation
     *            the explanation to set
     */
    public void setExplanation(final String explanation) {
        this.explanation = explanation;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field
     *            the field to set
     */
    public void setField(final String field) {
        this.field = field;
    }

    /**
     * @return the supportCode
     */
    public String getSupportCode() {
        return supportCode;
    }

    /**
     * @param supportCode
     *            the supportCode to set
     */
    public void setSupportCode(final String supportCode) {
        this.supportCode = supportCode;
    }

    /**
     * @return the validationType
     */
    public ValidationTypeEnum getValidationType() {
        return validationType;
    }

    /**
     * @param validationType
     *            the validationType to set
     */
    public void setValidationType(final ValidationTypeEnum validationType) {
        this.validationType = validationType;
    }
}
