/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;


/**
 * @author vmuthura
 * 
 */
public class DateTypeAdapter extends XmlAdapter<String, Date>
{

    private final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

    @Override
    public String marshal(final Date date) throws Exception
    {
        if (date == null)
        {
            return null;
        }
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        return format.format(date);
    }

    @Override
    public Date unmarshal(final String dateString) throws Exception
    {
        if (StringUtils.isBlank(dateString))
        {
            return null;
        }

        return DatatypeConverter.parseDateTime(dateString).getTime();
    }

}
