/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.impl;

import java.math.BigDecimal;
import java.util.Currency;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.PaymentType;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.data.request.PayRequest;
import au.com.target.tgtpaymentprovider.tns.data.request.RefundRequest;
import au.com.target.tgtpaymentprovider.tns.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.tns.data.request.VpcRefundRequest;
import au.com.target.tgtpaymentprovider.tns.data.response.CreateSessionResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.PayResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.RefundResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.RetrieveTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.TokenizeResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.VpcRefundResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author vmuthura
 * 
 */
public class TnsServiceImpl implements TnsService
{

    private static final String PAY = "PAY";
    private static final String REFUND = "REFUND";

    private static final String CMD_REFUND = "refund";
    private TnsWebOperations tnsClient;
    private TnsWebOperations vpcClient;
    private String tnsBaseUrl;
    private String vpcBaseUrl;


    /**
     * @param tnsClient
     *            the tnsClient to set
     */
    @Required
    public void setTnsClient(final TnsWebOperations tnsClient)
    {
        this.tnsClient = tnsClient;
    }

    /**
     * @param vpcClient
     *            the vpcClient to set
     */
    @Required
    public void setVpcClient(final TnsWebOperations vpcClient)
    {
        this.vpcClient = vpcClient;
    }

    /**
     * @param tnsBaseUrl
     *            the tnsBaseUrl to set
     */
    @Required
    public void setTnsBaseUrl(final String tnsBaseUrl)
    {
        this.tnsBaseUrl = tnsBaseUrl;
    }

    /**
     * @param vpcBaseUrl
     *            the vpcBaseUrl to set
     */
    @Required
    public void setVpcBaseUrl(final String vpcBaseUrl)
    {
        this.vpcBaseUrl = vpcBaseUrl;
    }

    @Override
    public PayResponse pay(final String orderId, final String transactionId, final String token,
            final BigDecimal amount, final Currency currency)
            throws TnsServiceException {
        final PayRequest payRequest = new PayRequest();
        payRequest.setApiOperation(PAY);

        final Transaction transaction = new Transaction();
        transaction.setAmount(amount.toString());
        transaction.setCurrency(currency.toString());

        final SourceOfFunds sourceOfFunds = new SourceOfFunds();
        sourceOfFunds.setToken(token);
        sourceOfFunds.setType(PaymentType.CARD);

        payRequest.setSourceOfFunds(sourceOfFunds);
        payRequest.setTransaction(transaction);

        final String url = tnsBaseUrl + "/order/{orderId}/transaction/{transactionId}";

        final PayResponse payResponse = tnsClient.put(url, payRequest, PayResponse.class,
                orderId, transactionId);

        return payResponse;
    }

    @Override
    public CreateSessionResponse createSession() throws TnsServiceException
    {
        final CreateSessionResponse response = tnsClient.post(tnsBaseUrl + "/session", null,
                CreateSessionResponse.class);

        return response;
    }

    @Override
    public RetrieveTransactionResponse retrieve(final String orderNumber, final String transactionId)
            throws TnsServiceException
    {
        final String url = tnsBaseUrl + "/order/{orderId}/transaction/{transactionId}";

        final RetrieveTransactionResponse response = tnsClient.get(url, RetrieveTransactionResponse.class, orderNumber,
                transactionId);

        return response;
    }

    @Override
    public TokenizeResponse tokenize(final String session) throws TnsServiceException
    {
        final String url = tnsBaseUrl + "/token";
        final TokenizeRequest request = new TokenizeRequest();
        final SourceOfFunds sourceOfFunds = new SourceOfFunds();
        sourceOfFunds.setType(PaymentType.CARD);
        sourceOfFunds.setSession(session);
        request.setSourceOfFunds(sourceOfFunds);

        final TokenizeResponse response = tnsClient.post(url, request, TokenizeResponse.class);

        return response;
    }

    @Override
    public RefundResponse refund(final String orderId, final String transactionId, final String captureTransactionId,
            final String token,
            final BigDecimal amount, final Currency currency)
            throws TnsServiceException {

        final String url = tnsBaseUrl + "/order/" + orderId + "/transaction/" + transactionId;
        final RefundRequest refundRequest = new RefundRequest();
        refundRequest.setApiOperation(REFUND);
        final SourceOfFunds sourceOfFunds = new SourceOfFunds();
        final Transaction transaction = new Transaction();

        transaction.setAmount(amount.toString());
        transaction.setCurrency(currency.toString());
        transaction.setTargetTransactionId(captureTransactionId);
        sourceOfFunds.setToken(token);
        sourceOfFunds.setType(PaymentType.CARD);
        refundRequest.setSourceOfFunds(sourceOfFunds);
        refundRequest.setTransaction(transaction);

        final RefundResponse refundResponse = tnsClient.put(url, refundRequest,
                RefundResponse.class);

        return refundResponse;
    }

    @Override
    public VpcRefundResponse excessRefund(final String transactionId, final String originalTransactionId,
            final BigDecimal amount, final Currency currency) throws TnsServiceException {

        final VpcRefundRequest vpcRefundRequest = new VpcRefundRequest();
        vpcRefundRequest.setCommand(CMD_REFUND);
        vpcRefundRequest.setTransactionNumber(originalTransactionId);
        vpcRefundRequest.setAmount(String.valueOf(amount.intValue()));
        vpcRefundRequest.setCurrency(currency.getCurrencyCode());
        vpcRefundRequest.setMerchTxnRef(transactionId);

        vpcRefundRequest.setReturnAuthResponseData("Y");

        final VpcRefundResponse response = vpcClient.post(vpcBaseUrl, vpcRefundRequest, VpcRefundResponse.class);

        return response;

    }
}
