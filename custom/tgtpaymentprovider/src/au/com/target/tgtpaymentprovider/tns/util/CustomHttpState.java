/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.auth.AuthScope;


/**
 * @author vmuthura
 * 
 */
public class CustomHttpState extends HttpState {

    /**
     * Wrapper method around {@link HttpState#setCredentials(AuthScope, Credentials)} to make it usable via spring DI
     * 
     * @param credentials
     *            -
     */
    public void setAuthCredentials(final Credentials credentials) {
        super.setCredentials(AuthScope.ANY, credentials);
    }

}
