/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author bjames4
 * 
 */
public enum GatewayResponseEnum {

    BASIC_VERIFICATION_SUCCESSFUL, //The card number format was successfully verified and the card exists in a known range.
    NO_VERIFICATION_PERFORMED, //The card details were not verified.
    EXTERNAL_VERIFICATION_SUCCESSFUL, //The card details were successfully verified.
    EXTERNAL_VERIFICATION_DECLINED, //The card details were sent for verification, but were was declined.
    EXTERNAL_VERIFICATION_DECLINED_EXPIRED_CARD, //The card details were sent for verification, but were declined as the card has expired.
    EXTERNAL_VERIFICATION_DECLINED_INVALID_CSC, //The card details were sent for verification, but were declined as the Card Security Code (CSC) was invalid.
    EXTERNAL_VERIFICATION_PROCESSING_ERROR, //There was an error processing the verification.
    EXTERNAL_VERIFICATION_BLOCKED, //The external verification was blocked due to risk rules.
    APPROVED, //Transaction Approved
    UNSPECIFIED_FAILURE, //Transaction could not be processed
    DECLINED, //Transaction declined by issuer
    TIMED_OUT, //Response timed out
    EXPIRED_CARD, //Transaction declined due to expired card
    INSUFFICIENT_FUNDS, //Transaction declined due to insufficient funds
    ACQUIRER_SYSTEM_ERROR, //Acquirer system error occured processing the transaction
    SYSTEM_ERROR, //Internal system error occured processing the transaction
    NOT_SUPPORTED, //Transaction type not supported
    DECLINED_DO_NOT_CONTACT, //Transaction declined - do not contact issuer
    ABORTED, //Transaction aborted by payer
    BLOCKED, //Transaction blocked due to Risk or 3D Secure blocking rules
    CANCELLED, //Transaction cancelled by payer
    DEFERRED_TRANSACTION_RECEIVED, //Deferred transaction received and awaiting processing
    REFERRED, //Transaction declined - refer to issuer
    AUTHENTICATION_FAILED, //3D Secure authentication failed
    INVALID_CSC, //Invalid card security code
    LOCK_FAILURE, //Order locked - another transaction is in progress for this order
    SUBMITTED, //Transaction submitted - response has not yet been received
    NOT_ENROLLED_3D_SECURE, //Card holder is not enrolled in 3D Secure
    PENDING, //Transaction is pending
    EXCEEDED_RETRY_LIMIT, //Transaction retry limit exceeded
    DUPLICATE_BATCH, //Transaction declined due to duplicate batch
    DECLINED_AVS, //Transaction declined due to address verification
    DECLINED_CSC, //Transaction declined due to card security code
    DECLINED_AVS_CSC, //Transaction declined due to address verification and card security code
    DECLINED_PAYMENT_PLAN, //Transaction declined due to payment plan
    APPROVED_PENDING_SETTLEMENT, //Transaction Approved - pending batch settlement
    UNKNOWN //Response unknown
}
