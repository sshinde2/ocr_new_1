/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.HashMap;
import java.util.Map;

import au.com.target.tgtpaymentprovider.tns.data.VpcTransactionResponseEnum;


/**
 * @author bjames4
 * 
 */

public final class Vpc2HybrisResponseMap
{

    //CHECKSTYLE:OFF
    private static Map<VpcTransactionResponseEnum, TransactionStatusDetails> responseMap = new HashMap<VpcTransactionResponseEnum, TransactionStatusDetails>();

    //CHECKSTYLE:ON

    /**
     * 
     */
    private Vpc2HybrisResponseMap()
    {
        //Avoid instantiation
    }

    static
    {
        // Init response map
        responseMap.put(VpcTransactionResponseEnum.SUCCESS, TransactionStatusDetails.SUCCESFULL);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_BANK_ERROR,
                TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        responseMap.put(VpcTransactionResponseEnum.BANK_DECLINED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_CARD_EXPIRED,
                TransactionStatusDetails.INVALID_CARD_EXPIRATION_DATE);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_INSUFFICIENT_FUNDS,
                TransactionStatusDetails.INSUFFICIENT_FUNDS);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_TXN_NOT_SUPPORTED,
                TransactionStatusDetails.INACTIVE_OR_INVALID_CARD);
        responseMap.put(VpcTransactionResponseEnum.TXN_ABORTED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(VpcTransactionResponseEnum.TXN_BLOCKED, TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(VpcTransactionResponseEnum.TXN_CANCELLED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(VpcTransactionResponseEnum.SECURE_3DS_FAILED,
                TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(VpcTransactionResponseEnum.ADDR_VERIFICATION_CSC_FAILED, TransactionStatusDetails.INVALID_CVN);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_CONTACT_BANK,
                TransactionStatusDetails.PROCESSOR_DECLINE);
        responseMap.put(VpcTransactionResponseEnum.TXN_SUBMITTED, TransactionStatusDetails.REVIEW_NEEDED);
        responseMap.put(VpcTransactionResponseEnum.NOT_ENROLLED_3DS,
                TransactionStatusDetails.THREE_D_SECURE_NOT_SUPPORTED);
        responseMap.put(VpcTransactionResponseEnum.TXN_PENDING, TransactionStatusDetails.REVIEW_NEEDED);
        responseMap.put(VpcTransactionResponseEnum.RETRY_LIMIT_EXCEEDED, TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_DUPLICATE_BATCH,
                TransactionStatusDetails.DUPLICATE_TRANSACTION);
        responseMap.put(VpcTransactionResponseEnum.ADDR_VERIFICATION_FAILED,
                TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(VpcTransactionResponseEnum.CSC_FAILED, TransactionStatusDetails.INVALID_CVN);
        responseMap.put(VpcTransactionResponseEnum.CARD_SECURITY_CODE_FAILED,
                TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(VpcTransactionResponseEnum.TXN_DECLINED_PAYMENT_PLAN,
                TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP);
        responseMap.put(VpcTransactionResponseEnum.UNKNOWN, TransactionStatusDetails.UNKNOWN_CODE);
    }

    /**
     * @param vpcTransactionResponseEnum
     *            -
     * @return -
     */
    public static TransactionStatusDetails
            getHybrisTransactionStatusDetails(final VpcTransactionResponseEnum vpcTransactionResponseEnum)
    {
        return responseMap.get(vpcTransactionResponseEnum);
    }
}
