/**
 * 
 */
package au.com.target.tgtpaymentprovider.paynow.commands.impl;


import org.apache.commons.lang.NotImplementedException;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;


/**
 * @author ragarwa3
 * 
 */
public class PayNowRefundFollowOnCommandImpl extends AbstractPayNowCommand implements TargetFollowOnRefundCommand {

    static final String COMMAND_REFUND = "refund";

    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest arg0) {
        throw new NotImplementedException(getCommandNotImplementedMessage(COMMAND_REFUND));
    }

}
