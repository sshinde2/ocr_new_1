/**
 * 
 */
package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import java.text.MessageFormat;


/**
 * @author ragarwa3
 * 
 */
public class AbstractPayNowCommand {

    private static final String ERR_COMMAND_NOT_IMPLEMENTED = "ERROR: Paynow command - {0} not implemented yet.";

    String getCommandNotImplementedMessage(final String commandName) {
        return MessageFormat.format(ERR_COMMAND_NOT_IMPLEMENTED, commandName);
    }

}
