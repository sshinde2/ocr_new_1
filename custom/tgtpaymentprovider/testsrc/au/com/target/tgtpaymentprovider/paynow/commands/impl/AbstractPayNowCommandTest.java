package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;

import junit.framework.Assert;


@UnitTest
public class AbstractPayNowCommandTest {

    private static final String TEST_COMMAND = "testCommand";

    private final AbstractPayNowCommand command = new AbstractPayNowCommand();

    @Test
    public void testGetCommandNotImplementedMessage() {
        Assert.assertEquals("ERROR: Paynow command - testCommand not implemented yet.",
                command.getCommandNotImplementedMessage(TEST_COMMAND));
    }

}
