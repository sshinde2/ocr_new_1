package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import org.apache.commons.lang.NotImplementedException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayNowGetSubscriptionDataCommandImplTest {

    @InjectMocks
    private final PayNowGetSubscriptionDataCommandImpl command = new PayNowGetSubscriptionDataCommandImpl();

    @Mock
    private SubscriptionDataRequest request;

    @Test
    public void testPerformNotImplemented() {
        try {
            command.perform(request);
            Assert.fail();
        }
        catch (final NotImplementedException e) {
            Assert.assertEquals(command
                    .getCommandNotImplementedMessage(PayNowGetSubscriptionDataCommandImpl.COMMAND_GET_SUBSCRIPTION),
                    e.getMessage());
        }
    }

}
