/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.GatewayResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.ResponseDetails;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.data.response.RetrieveTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author bjames4
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TnsRetrieveTransactionCommandImplTest {

    @Mock
    private TnsService tnsService;

    @Mock
    private TnsWebOperations tnsClient;

    @InjectMocks
    private final TnsRetrieveTransactionCommandImpl retrieveCommand = new TnsRetrieveTransactionCommandImpl();

    private final TargetRetrieveTransactionRequest retrieveRequest = new TargetRetrieveTransactionRequest();

    private String transactionId;
    private String reconcillationId;

    /**
     * @throws TnsServiceException
     *             -
     */
    @Before
    public void init() throws TnsServiceException {
        transactionId = "123456";
        reconcillationId = "123";
        final RetrieveTransactionResponse retrieveResponse = new RetrieveTransactionResponse();
        retrieveResponse.setResult(ResultEnum.SUCCESS);
        final ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setGatewayCode(GatewayResponseEnum.APPROVED);
        final Transaction transaction = new Transaction();
        transaction.setId(transactionId);
        transaction.setReceipt(reconcillationId);
        transaction.setAmount("10.00");
        retrieveResponse.setTransaction(transaction);
        retrieveResponse.setResponse(responseDetails);
        retrieveResponse.setSession("abc123");

        when(tnsService.retrieve(Mockito.anyString(), Mockito.anyString())).thenReturn(retrieveResponse);
    }

    /**
     * 
     */
    @Test
    public void testPerform() {
        retrieveRequest.setOrderId("1");
        retrieveRequest.setTransactionId("12");
        final TargetRetrieveTransactionResult response = retrieveCommand.perform(retrieveRequest);
        assertEquals("Expecting response", TransactionStatus.ACCEPTED, response.getTransactionStatus());
        assertEquals("Expecting response", TransactionStatusDetails.SUCCESFULL,
                response.getTransactionStatusDetails());
        assertEquals("Expecting MerchantTransactionCode", "12", response.getMerchantTransactionCode());
        assertEquals("Expecting ReconciliationId", "123", response.getReconciliationId());
        assertEquals("Expecting RequestId", "123456", response.getRequestId());
        assertEquals("Expecting RequestToken", "abc123", response.getRequestToken());
        assertEquals("Expecting TotalAmount", "10.00", response.getTotalAmount().toString());
    }
}
