/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.junit.Test;


/**
 * @author vmuthura
 * 
 */
@UnitTest
public class CustomHttpStateTest {

    private final CustomHttpState httpState = new CustomHttpState();

    /**
     * 
     */
    @Test
    public void testSetCredentials() {
        final Credentials credentials = new UsernamePasswordCredentials("test", "password");
        httpState.setAuthCredentials(credentials);
        assertEquals("", ((UsernamePasswordCredentials)httpState.getCredentials(AuthScope.ANY)).getUserName(), "test");
        assertEquals("", ((UsernamePasswordCredentials)httpState.getCredentials(AuthScope.ANY)).getPassword(),
                "password");
    }

}
