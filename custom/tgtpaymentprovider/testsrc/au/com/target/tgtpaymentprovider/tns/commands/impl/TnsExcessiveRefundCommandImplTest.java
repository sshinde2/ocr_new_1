/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.data.VpcTransactionResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.VpcRefundResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author bjames4
 * 
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(value = "classpath:tgtpaymentprovider-spring-test.xml")
public class TnsExcessiveRefundCommandImplTest {

    private String transactionId;
    private String correlationId;

    private final TargetExcessiveRefundRequest refundRequest = new TargetExcessiveRefundRequest();

    @Mock
    private TnsService tnsService;

    @InjectMocks
    private final TnsExcessiveRefundCommandImpl refundImpl = new TnsExcessiveRefundCommandImpl();


    /**
     * @throws PayPalServiceException
     *             -
     * @throws TnsServiceException
     *             -
     */
    @Before
    public void init() throws PayPalServiceException, TnsServiceException
    {
        transactionId = "123456";
        correlationId = "123";

        final VpcRefundResponse response = new VpcRefundResponse();
        response.setVpcTransactionNumber(transactionId);
        response.setTransactionResponseCode(VpcTransactionResponseEnum.SUCCESS);
        response.setReceiptNumber(correlationId);
        response.setAmount("1000");
        response.setMerchTxnRef("abc123");

        when(tnsService.excessRefund(Mockito.anyString(), Mockito.anyString(), (BigDecimal)Mockito.anyObject(),
                (Currency)Mockito.anyObject())).thenReturn(response);
    }

    /**
     * 
     */
    @Test
    public void testPerform() {
        refundRequest.setTransactionId(transactionId);
        refundRequest.setTotalAmount(BigDecimal.valueOf(10.00));
        refundRequest.setCurrency(Currency.getInstance("AUD"));

        final TargetExcessiveRefundResult response = refundImpl.perform(refundRequest);
        assertEquals("Expecting response", TransactionStatus.ACCEPTED, response.getTransactionStatus());
        assertEquals("Expecting response", TransactionStatusDetails.SUCCESFULL,
                response.getTransactionStatusDetails());
        assertEquals("Expecting MerchantTransactionCode", "123456", response.getMerchantTransactionCode());
        assertEquals("Expecting ReconciliationId", "123", response.getReconciliationId());
        assertEquals("Expecting RequestId", "abc123", response.getRequestId());
        assertEquals("Expecting RequestToken", "123456", response.getRequestToken());
        assertEquals("Expecting TotalAmount", "10", response.getTotalAmount().toString());
    }
}
