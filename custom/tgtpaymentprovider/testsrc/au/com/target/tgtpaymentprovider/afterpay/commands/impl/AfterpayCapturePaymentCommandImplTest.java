package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CapturePaymentRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayCapturePaymentCommandImplTest {

    private final TargetAfterpayHelper targetAfterpayHelper = new TargetAfterpayHelper();

    @Mock
    private TargetAfterpayClient targetAfterpayClient;

    @InjectMocks
    private final AfterpayCapturePaymentCommandImpl command = new AfterpayCapturePaymentCommandImpl();

    @Mock
    private TargetCaptureRequest paramRequest;

    @Before
    public void setup() {
        command.setTargetAfterpayHelper(targetAfterpayHelper);
    }

    @Test
    public void testPerformSucess() throws TargetAfterpayClientException {

        given(paramRequest.getToken()).willReturn("token");
        final Order order = mock(Order.class);
        given(paramRequest.getOrder()).willReturn(order);
        given(order.getOrderId()).willReturn("1234");

        final CapturePaymentResponse response = mock(CapturePaymentResponse.class);
        given(response.getHttpStatus()).willReturn(HttpStatus.CREATED);
        given(response.getToken()).willReturn("token");

        given(targetAfterpayClient.capturePayment(Mockito.any(CapturePaymentRequest.class)))
                .willReturn(response);

        given(response.getId()).willReturn("12345678");
        given(response.getToken()).willReturn("token");
        given(response.getStatus()).willReturn("APPROVED");
        given(response.getCreated()).willReturn("2015-07-14T10:08:14.123Z");

        final Money money = mock(Money.class);
        given(response.getTotalAmount()).willReturn(money);
        given(money.getAmount()).willReturn("100.00");
        given(money.getCurrency()).willReturn("AUD");
        given(response.getMerchantReference()).willReturn("1234");

        final TargetCaptureResult targetCaptureResult = command.perform(paramRequest);


        final ArgumentCaptor<CapturePaymentRequest> capturePaymentRequestArgsCapture = ArgumentCaptor
                .forClass(CapturePaymentRequest.class);
        verify(targetAfterpayClient).capturePayment(capturePaymentRequestArgsCapture.capture());
        final CapturePaymentRequest capturePaymentRequest = capturePaymentRequestArgsCapture.getValue();
        assertThat(capturePaymentRequest.getToken()).isEqualTo("token");
        assertThat(capturePaymentRequest.getMerchantReference()).isEqualTo("1234");


        assertThat(targetCaptureResult).isNotNull();
        assertThat(targetCaptureResult.getRequestToken()).isEqualTo("token");
        assertThat(targetCaptureResult.getTransactionStatus()).isEqualTo(TransactionStatus.ACCEPTED);
        assertThat(targetCaptureResult.getTransactionStatusDetails()).isEqualTo(TransactionStatusDetails.SUCCESFULL);

    }

    @Test
    public void testPerformRejectedStatus() throws TargetAfterpayClientException {
        final CapturePaymentResponse response = mock(CapturePaymentResponse.class);
        given(response.getHttpStatus()).willReturn(HttpStatus.PAYMENT_REQUIRED);

        given(targetAfterpayClient.capturePayment(Mockito.any(CapturePaymentRequest.class)))
                .willReturn(response);

        final TargetCaptureResult targetCaptureResult = command.perform(paramRequest);
        assertThat(targetCaptureResult).isNotNull();
        assertThat(targetCaptureResult.getTransactionStatus()).isEqualTo(TransactionStatus.REJECTED);
        assertThat(targetCaptureResult.getDeclineMessage())
                .isEqualTo(HttpStatus.PAYMENT_REQUIRED.getReasonPhrase());

    }

    @Test(expected = AdapterException.class)
    public void testPerformWithClientExpection() throws TargetAfterpayClientException {
        given(targetAfterpayClient.capturePayment(Mockito.any(CapturePaymentRequest.class)))
                .willThrow(new TargetAfterpayClientException());
        command.perform(paramRequest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() throws TargetAfterpayClientException {
        command.perform(null);
    }

}
