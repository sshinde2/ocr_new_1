/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayFollowOnRefundCommandImplTest {

    private final TargetAfterpayHelper targetAfterpayHelper = new TargetAfterpayHelper();

    @Mock
    private TargetAfterpayClient targetAfterpayClient;

    @InjectMocks
    private final AfterpayFollowOnRefundCommandImpl command = new AfterpayFollowOnRefundCommandImpl();

    @Mock
    private TargetFollowOnRefundRequest paramRequest;

    @Before
    public void setup() {
        command.setTargetAfterpayHelper(targetAfterpayHelper);
    }

    @Test
    public void testPerform() throws TargetAfterpayClientException {
        given(paramRequest.getCaptureTransactionId()).willReturn("12345");
        given(paramRequest.getTotalAmount()).willReturn(BigDecimal.valueOf(10));
        given(paramRequest.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(paramRequest.getOrderId()).willReturn("orderId");
        final CreateRefundResponse response = mock(CreateRefundResponse.class);
        given(response.getRefundId()).willReturn("87654");
        given(response.getRequestId()).willReturn("requestGUID");
        final Money money = mock(Money.class);
        given(money.getAmount()).willReturn("10.00");
        given(money.getCurrency()).willReturn("AUD");
        given(response.getAmount()).willReturn(money);
        final ArgumentCaptor<CreateRefundRequest> createRefundRequestCapture = ArgumentCaptor
                .forClass(CreateRefundRequest.class);
        final ArgumentCaptor<String> paymentIdCaptor = ArgumentCaptor.forClass(String.class);
        given(targetAfterpayClient.createRefund(paymentIdCaptor.capture(), createRefundRequestCapture.capture()))
                .willReturn(response);

        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult).isNotNull();
        assertThat(followOnRefundResult.getReconciliationId()).isEqualTo("87654");
        assertThat(followOnRefundResult.getRequestId()).isEqualTo("requestGUID");
        assertThat(followOnRefundResult.getTransactionStatus()).isEqualTo(TransactionStatus.ACCEPTED);
        assertThat(followOnRefundResult.getTotalAmount()).isEqualTo(new BigDecimal("10.00"));
        assertThat(paymentIdCaptor.getValue()).isEqualTo("12345");
        final CreateRefundRequest createRefundRequest = createRefundRequestCapture.getValue();
        assertThat(createRefundRequest).isNotNull();
        assertThat(createRefundRequest.getAmount().getAmount()).isEqualTo("10.00");
        assertThat(createRefundRequest.getAmount().getCurrency()).isEqualTo("AUD");
        assertThat(createRefundRequest.getRequestId()).isNotNull();
    }

    @Test
    public void testPerformWithClientConflict() throws TargetAfterpayClientException {
        given(paramRequest.getCaptureTransactionId()).willReturn("12345");
        given(paramRequest.getTotalAmount()).willReturn(BigDecimal.valueOf(10));
        given(paramRequest.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(paramRequest.getOrderId()).willReturn("orderId");
        final CreateRefundResponse refundResponse = mock(CreateRefundResponse.class);
        given(refundResponse.getErrorStatus()).willReturn(HttpStatus.CONFLICT);
        given(targetAfterpayClient.createRefund(Mockito.anyString(), Mockito.any(CreateRefundRequest.class)))
                .willReturn(refundResponse);
        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult.getTransactionStatus()).isEqualTo(TransactionStatus.ERROR);
    }

    @Test
    public void testPerformWithClientPreconditionFailed() throws TargetAfterpayClientException {
        given(paramRequest.getCaptureTransactionId()).willReturn("12345");
        given(paramRequest.getTotalAmount()).willReturn(BigDecimal.valueOf(10));
        given(paramRequest.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(paramRequest.getOrderId()).willReturn("orderId");
        final CreateRefundResponse refundResponse = mock(CreateRefundResponse.class);
        given(refundResponse.getErrorStatus()).willReturn(HttpStatus.PRECONDITION_FAILED);
        given(targetAfterpayClient.createRefund(Mockito.anyString(), Mockito.any(CreateRefundRequest.class)))
                .willReturn(refundResponse);
        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult.getTransactionStatus()).isEqualTo(TransactionStatus.REJECTED);
    }

    @Test
    public void testPerformWithClientInvalidAmount() throws TargetAfterpayClientException {
        given(paramRequest.getCaptureTransactionId()).willReturn("12345");
        given(paramRequest.getTotalAmount()).willReturn(BigDecimal.valueOf(10));
        given(paramRequest.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(paramRequest.getOrderId()).willReturn("orderId");
        final CreateRefundResponse refundResponse = mock(CreateRefundResponse.class);
        given(refundResponse.getErrorStatus()).willReturn(HttpStatus.UNPROCESSABLE_ENTITY);
        given(targetAfterpayClient.createRefund(Mockito.anyString(), Mockito.any(CreateRefundRequest.class)))
                .willReturn(refundResponse);
        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult.getTransactionStatus()).isEqualTo(TransactionStatus.REJECTED);
    }

    @Test(expected = AdapterException.class)
    public void testPerformWithClientExpection() throws TargetAfterpayClientException {
        given(paramRequest.getCaptureTransactionId()).willReturn("12345");
        given(paramRequest.getTotalAmount()).willReturn(BigDecimal.valueOf(10));
        given(paramRequest.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(paramRequest.getOrderId()).willReturn("orderId");
        given(targetAfterpayClient.createRefund(Mockito.anyString(), Mockito.any(CreateRefundRequest.class)))
                .willThrow(new TargetAfterpayClientException());
        command.perform(paramRequest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() throws TargetAfterpayClientException {
        command.perform(null);
    }

}
