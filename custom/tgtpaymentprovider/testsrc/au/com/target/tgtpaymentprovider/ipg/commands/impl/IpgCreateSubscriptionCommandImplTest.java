package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgCreateSubscriptionCommandImplTest {

    @Mock
    private IpgService ipgService;

    @InjectMocks
    private final TargetCreateSubscriptionCommand createSubscriptionCommand = new IpgCreateSubcriptionCommandImpl();

    private TargetCreateSubscriptionRequest request;

    @Before
    public void setup() {
        request = new TargetCreateSubscriptionRequest();
    }

    @Test(expected = AdapterException.class)
    public void testPerformWhenIpgServiceException() throws IpgServiceException {
        BDDMockito.when(ipgService.createSession((TargetCreateSubscriptionRequest)Mockito.anyObject())).thenThrow(
                new IpgServiceException());
        createSubscriptionCommand.perform(request);
    }

    @Test
    public void testPerformSuccess() throws IpgServiceException {
        final SessionResponse response = new SessionResponse();
        response.setSecureSessionToken("123465789");

        BDDMockito.when(ipgService.createSession((TargetCreateSubscriptionRequest)Mockito.anyObject())).thenReturn(
                response);
        final TargetCreateSubscriptionResult result = createSubscriptionCommand.perform(request);

        Assert.assertNotNull(result);
        Assert.assertEquals("123465789", result.getRequestToken());
    }
}
