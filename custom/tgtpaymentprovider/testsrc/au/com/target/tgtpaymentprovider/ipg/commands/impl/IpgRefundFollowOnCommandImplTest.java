/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;


import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgRefundFollowOnCommandImplTest {
    @InjectMocks
    private final IpgRefundFollowOnCommandImpl refundCommand = new IpgRefundFollowOnCommandImpl();

    @Mock
    private IpgService ipgService;

    @Mock
    private TargetCardPaymentResult transactionResult;

    @Mock
    private TargetFollowOnRefundRequest targetFollowonRequest;


    @Before
    public void setUp() throws IpgServiceException {
        Mockito.when(ipgService.submitSingleRefund(targetFollowonRequest)).thenReturn(transactionResult);
    }

    @Test
    public void testPerformWithNullRequest() throws IpgServiceException {
        final TargetFollowOnRefundRequest request = null;
        final TargetFollowOnRefundResult result = refundCommand.perform(request);
        assertNull(result);
    }

    @Test
    public void testPerformWithSuccess() throws IpgServiceException {
        Mockito.when(transactionResult.getResponseCode()).thenReturn("0");
        Mockito.when(transactionResult.getReceiptNumber()).thenReturn("123456");
        final TargetFollowOnRefundResult result = refundCommand.perform(targetFollowonRequest);
        Mockito.verify(ipgService, Mockito.times(1)).submitSingleRefund(targetFollowonRequest);
        Assert.assertEquals("123456", result.getReconciliationId());
        Assert.assertEquals(result.getTransactionStatus(), TransactionStatus.ACCEPTED);
    }

    @Test
    public void testPerformWithError() throws IpgServiceException {
        Mockito.when(transactionResult.getResponseCode()).thenReturn("1");
        Mockito.when(transactionResult.getReceiptNumber()).thenReturn("123456");
        final TargetFollowOnRefundResult result = refundCommand.perform(targetFollowonRequest);
        Mockito.verify(ipgService, Mockito.times(1)).submitSingleRefund(targetFollowonRequest);
        Assert.assertEquals("123456", result.getReconciliationId());
        Assert.assertEquals(result.getTransactionStatus(), TransactionStatus.ERROR);
        Assert.assertEquals(transactionResult, result.getRefundResult());
    }

    @Test
    public void testPerformWithErrorDeclineCode41() throws IpgServiceException {
        Mockito.when(transactionResult.getResponseCode()).thenReturn("1");
        Mockito.when(transactionResult.getDeclinedCode()).thenReturn("41");

        Mockito.when(targetFollowonRequest.getReceiptNumber()).thenReturn("123456");
        final TargetFollowOnRefundResult result = refundCommand.perform(targetFollowonRequest);
        Mockito.verify(ipgService, Mockito.times(1)).submitSingleRefund(targetFollowonRequest);
        Assert.assertEquals("123456", result.getReconciliationId());
        Assert.assertEquals(result.getTransactionStatus(), TransactionStatus.ERROR);
        Assert.assertEquals(result.getTransactionStatusDetails(), TransactionStatusDetails.BANK_DECLINE);
        Assert.assertEquals(transactionResult, result.getRefundResult());
    }

    @Test
    public void testPerformWithErrorDeclineCode17() throws IpgServiceException {
        Mockito.when(transactionResult.getResponseCode()).thenReturn("1");
        Mockito.when(transactionResult.getDeclinedCode()).thenReturn("17");

        Mockito.when(targetFollowonRequest.getReceiptNumber()).thenReturn("123456");
        final TargetFollowOnRefundResult result = refundCommand.perform(targetFollowonRequest);
        Mockito.verify(ipgService, Mockito.times(1)).submitSingleRefund(targetFollowonRequest);
        Assert.assertEquals("123456", result.getReconciliationId());
        Assert.assertEquals(result.getTransactionStatus(), TransactionStatus.ERROR);
        Assert.assertEquals(result.getTransactionStatusDetails(), TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        Assert.assertEquals(transactionResult, result.getRefundResult());
    }
}
