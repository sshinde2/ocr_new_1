/**
 * 
 */
package au.com.target.tgtsale.stock.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * @author rmcalave
 * 
 */
public class BulkyBoardStockUpdateJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(BulkyBoardStockUpdateJob.class);

    private TargetProductService targetProductService;

    private TargetStoreStockService targetStoreStockService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        final List<AbstractTargetVariantProductModel> bulkyBoardVariants = targetProductService.getBulkyBoardProducts();

        try {
            targetStoreStockService.updateStoreStockLevels(bulkyBoardVariants);
        }
        catch (final TargetIntegrationException ex) {
            LOG.error("Failed to update Bulky Board stock levels", ex);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param targetStoreStockService
     *            the targetStoreStockService to set
     */
    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }
}
