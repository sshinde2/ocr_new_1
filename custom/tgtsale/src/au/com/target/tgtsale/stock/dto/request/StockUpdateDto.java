/**
 * 
 */
package au.com.target.tgtsale.stock.dto.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "integration-stockUpdate")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @XmlElementWrapper(name = "stores")
    @XmlElement(name = "store")
    private List<StockUpdateStoreDto> stockUpdateStoreDtos;

    public StockUpdateDto() {
        this.stockUpdateStoreDtos = new ArrayList<>();
    }

    /**
     * @return the stockUpdateStoreDtos
     */
    public List<StockUpdateStoreDto> getStockUpdateStoreDtos() {
        return stockUpdateStoreDtos;
    }

    /**
     * @param stockUpdateStoreDtos
     *            the stockUpdateStoreDtos to set
     */
    public void setStockUpdateStoreDtos(final List<StockUpdateStoreDto> stockUpdateStoreDtos) {
        this.stockUpdateStoreDtos = stockUpdateStoreDtos;
    }

    public void addStockUpdateStoreDto(final StockUpdateStoreDto stockUpdateStoreDto) {
        stockUpdateStoreDtos.add(stockUpdateStoreDto);
    }
}
