/**
 * 
 */
package au.com.target.tgtsale.stock.dto.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "integration-stockUpdateStore")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdateStoreDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @XmlElement(name = "storeNumber")
    private String storeNumber;

    @XmlElement(name = "storeState")
    private String storeState;

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private List<StockUpdateProductDto> stockUpdateProductDtos;

    public StockUpdateStoreDto() {
        stockUpdateProductDtos = new ArrayList<>();
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeState
     */
    public String getStoreState() {
        return storeState;
    }

    /**
     * @param storeState
     *            the storeState to set
     */
    public void setStoreState(final String storeState) {
        this.storeState = storeState;
    }

    /**
     * @return the stockUpdateProductDtos
     */
    public List<StockUpdateProductDto> getStockUpdateProductDtos() {
        return stockUpdateProductDtos;
    }

    /**
     * @param stockUpdateProductDtos
     *            the stockUpdateProductDtos to set
     */
    public void setStockUpdateProductDtos(final List<StockUpdateProductDto> stockUpdateProductDtos) {
        this.stockUpdateProductDtos = stockUpdateProductDtos;
    }

    public void addStockUpdateProductDto(final StockUpdateProductDto product) {
        stockUpdateProductDtos.add(product);
    }
}
