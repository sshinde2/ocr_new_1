/**
 * 
 */
package au.com.target.tgtsale.integration.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;


/**
 * @author fkratoch
 * 
 */
public class IntegrationPosProductItemPriceDto implements Serializable {

    @XmlElement
    private Date start;

    @XmlElement
    private Date end;

    @XmlElement
    private double sellPrice;

    @XmlElement
    private double permPrice;

    @XmlElement
    private boolean promoEvent;

    /**
     * @return the start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @param start
     *            the start to set
     */
    public void setStart(final Date start) {
        this.start = start;
    }

    /**
     * @return the end
     */
    public Date getEnd() {
        return end;
    }

    /**
     * @param end
     *            the end to set
     */
    public void setEnd(final Date end) {
        this.end = end;
    }

    /**
     * @return the sellPrice
     */
    public double getSellPrice() {
        return sellPrice;
    }

    /**
     * @param sellPrice
     *            the sellPrice to set
     */
    public void setSellPrice(final double sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * @return the permPrice
     */
    public double getPermPrice() {
        return permPrice;
    }

    /**
     * @param permPrice
     *            the permPrice to set
     */
    public void setPermPrice(final double permPrice) {
        this.permPrice = permPrice;
    }

    /**
     * @return the promoEvent
     */
    public boolean isPromoEvent() {
        return promoEvent;
    }

    /**
     * @param promoEvent
     *            the promoEvent to set
     */
    public void setPromoEvent(final boolean promoEvent) {
        this.promoEvent = promoEvent;
    }




}
