/**
 * 
 */
package au.com.target.tgtsale.integration.exception;

/**
 * Exception that is thrown when the call to the esb was not successful. The response from the esb comes with a message
 * and that message is set as the message of this exception Also saves up the content of message received
 * 
 * @author rsamuel3
 * 
 */
public class UnsuccessfulESBOperationException extends Exception {

    private final String content;

    private final String errorCode;

    public UnsuccessfulESBOperationException(final String message) {
        super(message);
        content = null;
        errorCode = null;
    }


    public UnsuccessfulESBOperationException(final String message, final Throwable t) {
        super(message, t);
        content = null;
        errorCode = null;
    }

    public UnsuccessfulESBOperationException(final String message, final String content, final Throwable t) {
        super(message, t);
        this.content = content;
        errorCode = null;
    }

    public UnsuccessfulESBOperationException(final String message, final String content) {
        super(message);
        this.content = content;
        errorCode = null;
    }

    public UnsuccessfulESBOperationException(final String message, final String errorCode, final String content) {
        super(message);
        this.errorCode = errorCode;
        this.content = content;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
}
