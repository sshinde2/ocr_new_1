/**
 * 
 */
package au.com.target.tgtsale.pos.service;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author mmaki
 * 
 */
public interface TargetCustomerLockService
{

    /**
     * @param order
     *            - order whose customer needs to be locked
     * @return - success true/false
     */
    boolean posLockDirect(OrderModel order);

    /**
     * @param order
     *            - order whose customer needs to be unlocked
     * @return - success true/false
     */
    boolean posUnLockDirect(OrderModel order);

    /**
     * @param order
     *            - order whose customer needs an indirect lock
     * @return - success true/false
     */
    boolean convertLockIndirect(OrderModel order);

}
