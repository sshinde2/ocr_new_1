/**
 * 
 */
package au.com.target.tgtsale.pos.service;

import java.util.List;


/**
 * @author fkratoch
 * 
 */
public interface TargetDataRequestJmsService
{

    /**
     * request product price data from POS
     */
    void requestProductPriceData();

    /**
     * get the hot cards from POS
     */
    void requestHotcardData();

    /**
     * get layby changes from POS
     */
    void requestLaybyChangesData();

    void triggerCatalogSyncFeedback();

    /**
     * request null price changes from pos
     * 
     * @param codes
     */
    void requestNullPriceProducts(List<String> codes);

}
