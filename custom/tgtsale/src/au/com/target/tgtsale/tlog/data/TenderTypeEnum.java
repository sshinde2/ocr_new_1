/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum TenderTypeEnum {

    @XmlEnumValue("cash")
    CASH("cash"),

    @XmlEnumValue("eft")
    EFT("eft"),

    @XmlEnumValue("paypal")
    PAYPAL("paypal"),

    @XmlEnumValue("afterpay")
    AFTERPAY("afterpay"),

    @XmlEnumValue("zip")
    ZIP("zip");

    private String type;

    /**
     * @param type
     *            - Tender type
     */
    private TenderTypeEnum(final String type) {
        this.type = type;
    }

    /**
     * @return - Tender type
     */
    public String getType() {
        return type;
    }
}
