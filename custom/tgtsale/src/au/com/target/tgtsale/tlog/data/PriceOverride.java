/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PriceOverride
{
    @XmlAttribute(name = "reason")
    private ReasonTypeEnum reason;

    @XmlAttribute(name = "newPrice")
    private BigDecimal newPrice;

    /**
     * @return the reason
     */
    public ReasonTypeEnum getReason()
    {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final ReasonTypeEnum reason)
    {
        this.reason = reason;
    }

    /**
     * @return the newPrice
     */
    public BigDecimal getNewPrice()
    {
        return newPrice;
    }

    /**
     * @param newPrice
     *            the newPrice to set
     */
    public void setNewPrice(final BigDecimal newPrice)
    {
        this.newPrice = newPrice;
    }

}
