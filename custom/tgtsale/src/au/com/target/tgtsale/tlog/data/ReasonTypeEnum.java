/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.RefundReason;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum ReasonTypeEnum
{

	//For price override
	@XmlEnumValue("01")
	SOILED_DAMAGED("01","Soiled/Damaged"),

	@XmlEnumValue("05")
	RAINCHECK_ADVERTISED("05","Raincheck/Advertised"),

	@XmlEnumValue("06")
	QUIT_CYCLE("06","Quit Cycle"),

	@XmlEnumValue("11")
	INCORRECT_SPL("11","Incorrect SPL"),

	@XmlEnumValue("20")
    PRICE_ISSUE("20",RefundReason.PRICEMATCH.getCode().toLowerCase(), RefundReason.MISSEDLINKDEAL.getCode().toLowerCase(),RefundReason.SITEERROR.getCode().toLowerCase(),
				CancelReason.FRAUDCHECKREJECTED.getCode().toLowerCase()),

	@XmlEnumValue("90")
	FAULTY("90",RefundReason.MANUFACTURINGFAULT.getCode().toLowerCase(), RefundReason.REPAIR.getCode().toLowerCase(),RefundReason.RECALLPRODUCT.getCode().toLowerCase(),RefundReason.DAMAGEDINTRANSIT.getCode().toLowerCase(),
			CancelReason.RECALL.getCode().toLowerCase()),

	@XmlEnumValue("40")
	NOT_FAULTY("40",RefundReason.CHANGEOFMINDRETURN.getCode().toLowerCase(), RefundReason.CUSTOMERMISUSE.getCode().toLowerCase(),RefundReason.GOODWILL.getCode().toLowerCase(),
				RefundReason.MISPICKWRONGITEMDELIVERED.getCode().toLowerCase(),RefundReason.LATEDELIVERY.getCode().toLowerCase(),RefundReason.MISPICKITEMMISSING.getCode().toLowerCase(),
				RefundReason.WRONGDESCRIPTION.getCode().toLowerCase(),RefundReason.MISSINGPARTS.getCode().toLowerCase(),RefundReason.LOSTINTRANSIT.getCode().toLowerCase(),
				CancelReason.CUSTOMERREQUEST.getCode().toLowerCase(),CancelReason.OTHER.getCode().toLowerCase(),CancelReason.LATEDELIVERY.getCode().toLowerCase(),CancelReason.WAREHOUSE.getCode().toLowerCase(),CancelReason.OUTOFSTOCK.getCode().toLowerCase(),CancelReason.NA.getCode().toLowerCase());

	private final String[] reasons;


	public String getCode() {
		return code;
	}

	private final String code;


	ReasonTypeEnum(String code, String... reasons) {
		this.code = code;
		this.reasons = reasons;
	}

	/**
	 * @return {@link ReasonTypeEnum} reasons
	 */
	public String[] getReasons() {
		return reasons;
	}

	/**
	 * @param reasonCode
	 *            -
	 * @return {@link ReasonTypeEnum} ReasonTypeEnum
	 */
	public static ReasonTypeEnum find(String reasonCode) {
		if(StringUtils.isNotEmpty(reasonCode)) {
			for (ReasonTypeEnum reason : ReasonTypeEnum.values()) {
				if (ArrayUtils.contains(reason.getReasons(),StringUtils.lowerCase(reasonCode))) {
					return reason;
				}
			}
		}
		return null;
	}
}
