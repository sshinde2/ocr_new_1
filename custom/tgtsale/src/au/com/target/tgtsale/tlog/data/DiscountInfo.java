/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtsale.tlog.data.adapter.DecimalFormatAdapter;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DiscountInfo
{
    @XmlAttribute(name = "type")
    private DiscountTypeEnum type;

    @XmlAttribute(name = "code")
    private String code;

    @XmlAttribute(name = "pct")
    @XmlJavaTypeAdapter(DecimalFormatAdapter.class)
    private Double percentage;

    @XmlAttribute(name = "amount")
    private BigDecimal amount;

    @XmlAttribute(name = "style")
    private DiscountStyleEnum style;

    @XmlAttribute(name = "incremental")
    private String incremental;

    @XmlAttribute(name = "points")
    private Integer points;

    @XmlAttribute(name = "redeemCode")
    private String redeemCode;

    @XmlAttribute(name = "confirmationCode")
    private String confirmationCode;


    /**
     * @return the type
     */
    public DiscountTypeEnum getType()
    {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final DiscountTypeEnum type)
    {
        this.type = type;
    }

    /**
     * @return the percentage
     */
    public Double getPercentage()
    {
        return percentage;
    }

    /**
     * @param percentage
     *            the percentage to set
     */
    public void setPercentage(final Double percentage)
    {
        this.percentage = percentage;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }

    /**
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code)
    {
        this.code = code;
    }

    public DiscountStyleEnum getStyle() {
        return style;
    }

    public void setStyle(final DiscountStyleEnum style) {
        this.style = style;
    }

    public String getIncremental() {
        return incremental;
    }

    public void setIncremental(final String incremental) {
        this.incremental = incremental;
    }

    /**
     * @return the points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(final Integer points) {
        this.points = points;
    }

    /**
     * @return the redeemCode
     */
    public String getRedeemCode() {
        return redeemCode;
    }

    /**
     * @param redeemCode
     *            the redeemCode to set
     */
    public void setRedeemCode(final String redeemCode) {
        this.redeemCode = redeemCode;
    }

    /**
     * @return the confirmationCode
     */
    public String getConfirmationCode() {
        return confirmationCode;
    }

    /**
     * @param confirmationCode
     *            the confirmationCode to set
     */
    public void setConfirmationCode(final String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

}
