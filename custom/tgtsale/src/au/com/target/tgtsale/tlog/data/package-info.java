@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(value = au.com.target.tgtsale.tlog.data.adapter.BigDecimalAdapter.class, type = java.math.BigDecimal.class)
})
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;


