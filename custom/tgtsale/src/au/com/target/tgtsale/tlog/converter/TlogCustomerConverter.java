/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.core.model.user.AddressModel;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtsale.tlog.data.AddressInfo;
import au.com.target.tgtsale.tlog.data.AddressTypeEnum;
import au.com.target.tgtsale.tlog.data.CustomerInfo;


/**
 * @author mjanarth
 *
 */
public interface TlogCustomerConverter {

    /**
     * @param address
     *            -
     * @param type
     *            -
     * @param targetPOS
     * @return -
     */
    public AddressInfo getAddressInfoFromAddressModel(final AddressModel address,
            final AddressTypeEnum type, final TargetPointOfServiceModel targetPOS);



    /**
     * @param customerModel
     *            -
     * @param billingAddress
     *            -
     * @param deliveryAddress
     *            -
     * @param pos
     * @return -
     */
    public CustomerInfo getCustomerInfoFromTargetCustomerModel(final TargetCustomerModel customerModel,
            final AddressModel billingAddress, final AddressModel deliveryAddress, final TargetPointOfServiceModel pos);

}
