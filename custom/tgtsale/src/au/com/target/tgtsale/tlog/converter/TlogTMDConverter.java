/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.ProductPromotionModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.ItemInfo;


/**
 * @author mjanarth
 *
 */
public interface TlogTMDConverter {


    /**
     * Gets the new tmd promotion result for current entry.
     * 
     * @param orderEntryModel
     * 
     * @param allResults
     *            the all results
     * @return the new tmd promotion result for current entry
     */
    public PromotionResult getTMDPromotionResultForCurrentEntry(
            final AbstractOrderEntryModel orderEntryModel, final List<PromotionResult> allResults);

    /**
     * 
     * @param orderModel
     * @return TMDiscountPromotionModel
     */
    public TMDiscountPromotionModel getTMDFromPromotionResults(final OrderModel orderModel);

    /**
     * Adds the variationAmtTmd to the first itemDiscount
     * 
     * @param itemList
     * @param variationAmtTmd
     */
    public void addVariationAmtToTMDDiscount(final List<ItemInfo> itemList, final double variationAmtTmd);


    /**
     * Gets the tmd percentage.
     * 
     * @param tMDiscountModel
     *            the t m discount model
     * @return the tmd percentage
     */
    public Double getTmdPercentage(final ProductPromotionModel tMDiscountModel);


    /**
     * Get DiscountInfo for TMD promo result. <br/>
     * Amount is worked out as proportion of the TMD discount weighted by the given order entry value after deal.
     * 
     * @param promotionResult
     * @param totalOrderEntryValue
     * @param partOrderEntryValue
     * @return DiscountInfo
     */
    public DiscountInfo getDiscountInfoFromTMDProportionByValue(final PromotionResult promotionResult,
            final double totalOrderEntryValue, final double partOrderEntryValue);


    /**
     * @param tMDiscountModel
     *            -
     * @param code
     *            -
     * @param amount
     *            -
     * @return -
     */
    public DiscountInfo getDiscountInfoFromPromotion(final ProductPromotionModel tMDiscountModel,
            final String code, final BigDecimal amount);

    /**
     * Checks if is variation in tmd discount.
     * 
     * @param itemList
     *            the item list
     * @param totalTmdiscount
     * @return true, if is variation in tm discount
     */
    public double variationInTmDiscount(final List<ItemInfo> itemList, final double totalTmdiscount);


    /**
     * Gets the new tmd from promotion results.
     * 
     * @param promotionResult
     *            the order model
     * @return the new tmd from promotion results
     */
    public TMDiscountProductPromotion getNewTMDFromPromotionResults(final PromotionResult promotionResult);

}
