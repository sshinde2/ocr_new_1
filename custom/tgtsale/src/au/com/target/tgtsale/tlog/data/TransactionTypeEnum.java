/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum TransactionTypeEnum
{
    @XmlEnumValue("sale")
    SALE("sale"),

    @XmlEnumValue("refund")
    REFUND("refund"),

    @XmlEnumValue("laybyCreate")
    LAYBY_CREATE("laybyCreate"),

    @XmlEnumValue("laybyPayment")
    LAYBY_PAYMENT("laybyPayment"),

    @XmlEnumValue("laybyCancel")
    LAYBY_CANCEL("laybyCancel"),

    @XmlEnumValue("laybyCustUpdate")
    LAYBY_CUSTOMERINFO_UPDATE("laybyCustUpdate");

    private String type;

    /**
     * @param type
     *            - Transaction type
     */
    private TransactionTypeEnum(final String type)
    {
        this.type = type;
    }

    /**
     * @return type - Transaction type
     */
    public String getType()
    {
        return type;
    }
}
