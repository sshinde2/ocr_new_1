/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotions.jalo.AbstractPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtsale.tlog.converter.TlogTMDConverter;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.DiscountTypeEnum;
import au.com.target.tgtsale.tlog.data.ItemInfo;


/**
 * @author mjanarth
 *
 */
public class TlogTMDConverterImpl implements TlogTMDConverter {



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#getTMDPromotionResultForCurrentEntry(de.hybris.platform.core.model.order.AbstractOrderEntryModel, java.util.List)
     */
    @Override
    public PromotionResult getTMDPromotionResultForCurrentEntry(
            final AbstractOrderEntryModel orderEntryModel, final List<PromotionResult> allResults) {
        for (final PromotionResult result : allResults) {
            if (result.isApplied() && (result.getPromotion() instanceof TMDiscountProductPromotion)
                    && null != result.getCustom()
                    && orderEntryModel.getEntryNumber().equals(Integer.valueOf(result.getCustom()))) {
                return result;
            }
        }
        return null;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#getTMDFromPromotionResults(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public TMDiscountPromotionModel getTMDFromPromotionResults(final OrderModel orderModel) {
        final Collection<PromotionResultModel> promotionResults = orderModel.getAllPromotionResults();

        if (promotionResults == null) {
            return null;
        }

        for (final PromotionResultModel promotionResult : promotionResults) {
            final AbstractPromotionModel promotion = promotionResult.getPromotion();
            if (promotion != null && promotion instanceof TMDiscountPromotionModel) {
                return (TMDiscountPromotionModel)promotion;
            }
        }

        return null;
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#addVariationAmtToTMDDiscount(java.util.List, double)
     */
    @Override
    public void addVariationAmtToTMDDiscount(final List<ItemInfo> itemList, final double variationAmtTmd) {
        if (CollectionUtils.isNotEmpty(itemList)
                && Double.valueOf(variationAmtTmd).compareTo(Double.valueOf(0)) != 0) {
            // for any deal with 100% markdown ,the first item in the list may have null itemdiscount,so loop through to find out first discount
            for (final ItemInfo itemInfo : itemList) {
                BigDecimal sumOfDiscountAndVariation = BigDecimal.valueOf(0);
                if (null != itemInfo.getItemDiscount()) {
                    final double firstItemDiscount = itemInfo.getItemDiscount().getAmount().doubleValue();
                    sumOfDiscountAndVariation = BigDecimal.valueOf(firstItemDiscount + variationAmtTmd);
                    //set only if the sum is not a negative value
                    if (sumOfDiscountAndVariation.compareTo(BigDecimal.ZERO) > 0) {
                        itemInfo.getItemDiscount().setAmount(sumOfDiscountAndVariation);
                        break;
                    }
                }
            }
        }
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#getTmdPercentage(de.hybris.platform.promotions.model.ProductPromotionModel)
     */
    @Override
    public Double getTmdPercentage(final ProductPromotionModel tMDiscountModel) {
        Double percentage = null;

        if (tMDiscountModel instanceof TMDiscountPromotionModel) {
            percentage = ((TMDiscountPromotionModel)tMDiscountModel).getPercentageDiscount();
        }
        else if (tMDiscountModel instanceof TMDiscountProductPromotionModel) {
            percentage = ((TMDiscountProductPromotionModel)tMDiscountModel).getPercentageDiscount();
        }
        return percentage;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#getDiscountInfoFromTMDProportionByValue(de.hybris.platform.promotions.jalo.PromotionResult, double, double)
     */
    @Override
    public DiscountInfo getDiscountInfoFromTMDProportionByValue(final PromotionResult promotionResult,
            final double totalOrderEntryValue, final double partOrderEntryValue) {

        final double totalOrderEntryValueAfterDeals = totalOrderEntryValue + promotionResult.getTotalDiscount();

        // If the order entry is zero, then avoid div by zero and omit tmd info - 
        // shouldn't actually get here since the caller checks partOrderEntryValue is nonzero before calling this method
        if (totalOrderEntryValueAfterDeals <= 0) {
            return null;
        }

        final DiscountInfo discountInfo = setupTMDDiscountInfo(promotionResult);

        final double amount = promotionResult.getTotalDiscount() * partOrderEntryValue / totalOrderEntryValueAfterDeals;
        discountInfo.setAmount(BigDecimal.valueOf(amount).setScale(2, RoundingMode.HALF_EVEN));

        return discountInfo;

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#getDiscountInfoFromPromotion(de.hybris.platform.promotions.model.ProductPromotionModel, java.lang.String, java.math.BigDecimal)
     */
    @Override
    public DiscountInfo getDiscountInfoFromPromotion(final ProductPromotionModel tMDiscountModel,
            final String code, final BigDecimal amount) {
        if (tMDiscountModel == null) {
            return null;
        }
        final Double percentage = getTmdPercentage(tMDiscountModel);
        if (percentage == null || percentage.compareTo(Double.valueOf(0)) == 0) {
            return null;
        }

        final DiscountInfo discountInfo = new DiscountInfo();
        discountInfo.setType(DiscountTypeEnum.TEAM_MEMBER);
        discountInfo.setCode(code);

        discountInfo.setPercentage(percentage);
        discountInfo.setAmount(amount);

        return discountInfo;
    }

    /* (non-Javadoc)
      * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#variationInTmDiscount(java.util.List, double)
      */
    @Override
    public double variationInTmDiscount(final List<ItemInfo> itemList, final double totalTmdiscount) {
        double variationTotal = 0.0;
        for (final ItemInfo item : itemList) {
            if (null != item.getItemDiscount() && null != item.getItemDiscount().getAmount()) {
                variationTotal += item.getItemDiscount().getAmount().doubleValue();
            }
        }

        return totalTmdiscount - variationTotal;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogTMDConverter#getNewTMDFromPromotionResults(de.hybris.platform.promotions.jalo.PromotionResult)
     */
    @Override
    public TMDiscountProductPromotion getNewTMDFromPromotionResults(final PromotionResult promotionResult) {
        if (promotionResult == null) {
            return null;
        }

        final AbstractPromotion promotion = promotionResult.getPromotion();
        if (promotion != null && promotion instanceof TMDiscountProductPromotion) {
            return (TMDiscountProductPromotion)promotion;
        }

        return null;
    }

    /**
     * Set up a DiscountInfo based on the given promotionResult, with fields EXCEPT value populated.
     * 
     * @param promotionResult
     * @return DiscountInfo
     */
    private static DiscountInfo setupTMDDiscountInfo(final PromotionResult promotionResult) {

        final DiscountInfo discountInfo = new DiscountInfo();
        discountInfo.setType(DiscountTypeEnum.TEAM_MEMBER);
        discountInfo.setCode(promotionResult.getPromotion().getCode());

        discountInfo
                .setPercentage(((TMDiscountProductPromotion)promotionResult.getPromotion()).getPercentageDiscount());

        return discountInfo;
    }

}
