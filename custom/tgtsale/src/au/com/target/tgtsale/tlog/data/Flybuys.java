/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Flybuys
{
    @XmlAttribute(name = "number")
    private String number;

    public Flybuys()
    {
        //
    }

    public Flybuys(final String number)
    {
        this.number = number;
    }

    /**
     * @return the number
     */
    public String getNumber()
    {
        return number;
    }

    /**
     * @param number
     *            the number to set
     */
    public void setNumber(final String number)
    {
        this.number = number;
    }

}
