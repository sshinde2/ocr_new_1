/**
 * 
 */
package au.com.target.tgtsale.order.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtsale.exception.IllegalPosModificationException;
import au.com.target.tgtsale.integration.dto.TargetLaybyEntries;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrderEntry;
import au.com.target.tgtsale.order.PosLaybyUpdater;


public class PosLaybyUpdaterImpl implements PosLaybyUpdater {


    private static final String POS_CANCEL_NOTE = "Cancelled at POS";
    private static final CancelReason POS_CANCEL_REASON = CancelReason.NA;

    private OrderCancelRecordsHandler orderCancelRecordsHandler;
    private ModelService modelService;
    private TargetCancelService targetCancelService;


    /**
     * check the pos layby order and the hybris layby order to make sure they match
     * 
     * @param posLaybyOrder
     *            order as received from pos
     * @param order
     *            order as exist in hybris database
     * @return true if both orders are the same
     */
    @Override
    public boolean checkLaybyOrdersMatch(final TargetLaybyOrder posLaybyOrder, final OrderModel order)
            throws IllegalPosModificationException {
        Assert.notNull(order, "order cannot be null");
        Assert.notNull(posLaybyOrder, "pos layby order cannot be null");

        if (OrderStatus.CANCELLED.equals(order.getStatus())) {
            if (posLaybyOrder.isCancelled()) {
                // no modification should happen on called laybys
                return true;
            }
            else {
                throw new IllegalPosModificationException("POS has uncancelled order [" + posLaybyOrder.getOrderCode()
                        + "]");
            }
        }
        else if (posLaybyOrder.isCancelled()) {
            return false;
        }

        // In the outer order just check for delivery cost change
        if (Double.valueOf(posLaybyOrder.getShippingPrice()).compareTo(order.getDeliveryCost()) != 0) {
            return false;
        }

        // remove the dead hybrisEntries, dead hybris entries will have zero quantity
        final List<AbstractOrderEntryModel> hybrisEntries = removeZeroQuantityEntries(order.getEntries());

        // Case of no entries
        final TargetLaybyEntries posEntriesObj = posLaybyOrder.getEntries();
        if (posEntriesObj == null) {
            return CollectionUtils.isEmpty(hybrisEntries);
        }

        final List<TargetLaybyOrderEntry> posEntries = posEntriesObj.getEntry();
        if (posEntries == null) {
            return CollectionUtils.isEmpty(hybrisEntries);
        }

        if (posEntries.size() < hybrisEntries.size()) {
            return false;
        }

        NEXT_SKU: for (final TargetLaybyOrderEntry posEntry : posEntries) {
            for (final AbstractOrderEntryModel entry : hybrisEntries) {
                if (posEntry.getCode().equals(entry.getProduct().getCode())) {
                    if (posEntry.getQuantity() != entry.getQuantity().longValue()
                            || Double.valueOf(posEntry.getBasePrice()).compareTo(entry.getBasePrice()) != 0) {
                        return false;
                    }
                    // move to next sku
                    continue NEXT_SKU; // cannot break as it will then get the throw that is outside the first loop
                }
            }
            // this sku was not in hybris db for this order
            throw new IllegalPosModificationException("POS has added sku [" + posEntry.getCode() + "]");
        }

        // got here and could not find any problems, must be good
        return true;
    }


    /**
     * adjust the hybris order to match the pos order
     * 
     * @param posLaybyOrder
     *            order as received from pos
     * @param order
     *            order as exist in hybris database
     * @throws IllegalPosModificationException
     *             pos has done something it should not
     */
    @Override
    public void modifyLaybyOrder(final TargetLaybyOrder posLaybyOrder, final OrderModel order)
            throws IllegalPosModificationException {

        if (OrderStatus.CANCELLED.equals(order.getStatus())) {
            if (posLaybyOrder.isCancelled()) {
                // no modification should happen on called laybys
                return;
            }
            else {
                throw new IllegalPosModificationException("POS has uncancelled order ["
                        + posLaybyOrder.getOrderCode() + "]");
            }
        }

        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();

        // For full cancel, create cancel record with all order entries and recalculate.<br/>
        // No need to update anything else from the POS layby.
        if (posLaybyOrder.isCancelled()) {
            final List<OrderCancelEntry> orderCancelEntries = new ArrayList<OrderCancelEntry>(orderEntries.size());
            for (final AbstractOrderEntryModel entry : orderEntries) {
                orderCancelEntries.add(new OrderCancelEntry(entry, entry.getQuantity().longValue(),
                        POS_CANCEL_NOTE, POS_CANCEL_REASON));
            }

            cancelTheseEntries(order, orderCancelEntries);
            targetCancelService.recalculateOrder(order);
            return;
        }


        // Process cancel for any modified or removed entries
        final TargetLaybyEntries posEntriesObj = posLaybyOrder.getEntries();
        if (posEntriesObj != null) {

            // find the modified entries
            final List<OrderCancelEntry> orderCancelEntries = new ArrayList<OrderCancelEntry>(orderEntries.size());
            final List<TargetLaybyOrderEntry> posEntries = posEntriesObj.getEntry();
            NEXT_SKU: for (final TargetLaybyOrderEntry posEntry : posEntries) {
                for (final AbstractOrderEntryModel hybrisEntry : orderEntries) {
                    if (posEntry.getCode().equals(hybrisEntry.getProduct().getCode())) {
                        final long posQty = posEntry.getQuantity();
                        final long hybQty = hybrisEntry.getQuantity().longValue();
                        // if the quantity or price doesn't match then override the hybris order to use values from the pos order
                        if (Double.valueOf(posEntry.getBasePrice()).compareTo(hybrisEntry.getBasePrice()) != 0) {
                            final double basePrice = posEntry.getBasePrice();
                            hybrisEntry.setBasePrice(Double.valueOf(basePrice));
                            modelService.save(hybrisEntry);
                        }

                        if (posQty < hybQty) {
                            orderCancelEntries.add(new OrderCancelEntry(hybrisEntry,
                                    hybQty - posQty, POS_CANCEL_NOTE, POS_CANCEL_REASON));
                        }
                        continue NEXT_SKU; // cannot break as it will then get the throw that is outside the first loop
                    }
                }
                throw new IllegalPosModificationException("POS has added sku [" + posEntry.getCode() + "]");
            }

            // find the removed entries
            final List<AbstractOrderEntryModel> hybrisEntries = removeZeroQuantityEntries(order.getEntries());
            if (hybrisEntries.size() > posEntries.size()) {
                nextSku: for (final AbstractOrderEntryModel hybrisEntry : hybrisEntries) {
                    for (final TargetLaybyOrderEntry posEntry : posEntries) {
                        if (posEntry.getCode().equals(hybrisEntry.getProduct().getCode())) {
                            continue nextSku;
                        }
                    }
                    orderCancelEntries.add(new OrderCancelEntry(hybrisEntry,
                            hybrisEntry.getQuantity().longValue(), POS_CANCEL_NOTE, POS_CANCEL_REASON));
                }
            }

            cancelTheseEntries(order, orderCancelEntries);
        }

        // Update the delivery fee and recalculate
        order.setDeliveryCost(Double.valueOf(posLaybyOrder.getShippingPrice()));
        /*
        order.setLayByFee(Double.valueOf(posLaybyOrder.getLayByFee()));
        order.setTotalDiscounts(Double.valueOf(posLaybyOrder.getTotalDiscounts()));
        order.setTotalTax(Double.valueOf(posLaybyOrder.getTotalTax()));
        order.setTotalPrice(Double.valueOf(posLaybyOrder.getTotalPrice()));
        order.setSubtotal(Double.valueOf(posLaybyOrder.getSubtotalPrice()));
        */
        modelService.save(order);

        targetCancelService.recalculateOrder(order);
    }


    /**
     * Create an order cancel request if there is a full or partial cancel of the order,<br/>
     * ie if changed quantities.
     * 
     * @param order
     * @param orderCancelEntries
     * @throws IllegalPosModificationException
     */
    protected void cancelTheseEntries(final OrderModel order, final List<OrderCancelEntry> orderCancelEntries)
            throws IllegalPosModificationException {

        if (CollectionUtils.isNotEmpty(orderCancelEntries)) {
            final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(order,
                    orderCancelEntries, POS_CANCEL_NOTE);
            orderCancelRequest.setCancelReason(POS_CANCEL_REASON);

            // Add full delivery fee to modification record for a fully cancelled order
            if (!orderCancelRequest.isPartialCancel() && order.getInitialDeliveryCost() != null) {

                orderCancelRequest.setShippingAmountToRefund(Double.valueOf(order.getInitialDeliveryCost()
                        .doubleValue()));
            }

            try {
                // This will create the order history record
                final OrderCancelRecordEntryModel orderCancelRequestEntry = orderCancelRecordsHandler
                        .createRecordEntry(orderCancelRequest);


                processPosLaybyCancelRequest(orderCancelRequest, orderCancelRequestEntry);
            }
            catch (final Exception ex) {
                throw new IllegalPosModificationException("Could not generate cancel record", ex);
            }
        }
    }


    /**
     * Process the OrderCancelRequest record
     * 
     * @param orderCancelRequest
     * @param cancelRequestRecordEntry
     * @throws OrderCancelException
     */
    protected void processPosLaybyCancelRequest(final OrderCancelRequest orderCancelRequest,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry)
            throws OrderCancelException {

        final OrderModel order = orderCancelRequest.getOrder();

        // Remember whether we should run a cancel extract to warehouse later.
        // This is up front since it uses the existing consignment statuses before they get modified below.
        targetCancelService.releaseStock(orderCancelRequest);

        //modify order and consignment entries
        targetCancelService.modifyOrderAccordingToRequest(orderCancelRequest);
        modelService.refresh(order);

        targetCancelService.updateOrderStatus(cancelRequestRecordEntry, order);

        //update record entries
        targetCancelService.updateRecordEntry(orderCancelRequest);

    }


    /**
     * Return list of order entries with the dead ones removed
     * 
     * @param hybrisEntries
     * @return List<AbstractOrderEntryModel>
     */
    private List<AbstractOrderEntryModel> removeZeroQuantityEntries(final List<AbstractOrderEntryModel> hybrisEntries) {

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();

        for (final AbstractOrderEntryModel orderEntry : hybrisEntries) {
            if (!OrderEntryStatus.DEAD.equals(orderEntry.getQuantityStatus())
                    && orderEntry.getQuantity() != null
                    && orderEntry.getQuantity().longValue() > 0) {
                entries.add(orderEntry);
            }
        }

        return entries;
    }


    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param orderCancelRecordsHandler
     *            the orderCancelRecordsHandler to set
     */
    @Required
    public void setOrderCancelRecordsHandler(final OrderCancelRecordsHandler orderCancelRecordsHandler) {
        this.orderCancelRecordsHandler = orderCancelRecordsHandler;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }


}
