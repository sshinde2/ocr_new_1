/**
 * 
 */
package au.com.target.tgtsale.product.dto.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * represent request when getting product info from POS
 * 
 */
@XmlRootElement(name = "integration-product")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductRequestDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "ean")
    private String ean;

    @XmlElement(name = "storeNumber")
    private String storeNumber;

    @XmlElement(name = "storeState")
    private String storeState;

    public String getEan() {
        return ean;
    }

    public void setEan(final String ean) {
        this.ean = ean;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getStoreState() {
        return storeState;
    }

    public void setStoreState(final String storeState) {
        this.storeState = storeState;
    }



}
