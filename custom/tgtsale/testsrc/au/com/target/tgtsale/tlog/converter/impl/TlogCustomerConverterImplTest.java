/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.c2l.CountryModel;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtsale.tlog.data.AddressInfo;
import au.com.target.tgtsale.tlog.data.AddressTypeEnum;
import au.com.target.tgtsale.tlog.data.CustomerInfo;


/**
 * @author htan3
 *
 */
@UnitTest
public class TlogCustomerConverterImplTest {

    private static final String PROMOTION_CODE = "12345";

    private final TlogCustomerConverterImpl converter = new TlogCustomerConverterImpl();

    private TargetAddressModel addressModel;

    private TargetCustomerModel customerModel;

    private CountryModel country;

    private TargetPointOfServiceModel targetPos;


    @Before
    public void setUp() {
        addressModel = mock(TargetAddressModel.class);
        country = mock(CountryModel.class);
        customerModel = mock(TargetCustomerModel.class);
        targetPos = mock(TargetPointOfServiceModel.class);

        when(targetPos.getType()).thenReturn(PointOfServiceTypeEnum.POS);
        when(targetPos.getName()).thenReturn("pos");

        when(addressModel.getLine1()).thenReturn("12 Thompson rd");
        when(addressModel.getTown()).thenReturn("Geelong East");
        when(country.getName()).thenReturn("Australia");
        when(addressModel.getCountry()).thenReturn(country);
        when(addressModel.getPostalcode()).thenReturn("3219");
        when(addressModel.getPhone1()).thenReturn("0352943333");
        when(addressModel.getCellphone()).thenReturn("0426888999");

        when(customerModel.getCustomerID()).thenReturn(PROMOTION_CODE);
        when(customerModel.getFirstname()).thenReturn("Ted");
        when(customerModel.getLastname()).thenReturn("Baker");
        when(customerModel.getTitle()).thenReturn(null);
        when(customerModel.getContactEmail()).thenReturn("ted@bed.com");
    }

    @Test
    public void testSetPhoneInfoWhenGetCustomerInfoFromBillingAddress()
    {
        final TargetAddressModel address = mock(TargetAddressModel.class);
        when(address.getCountry()).thenReturn(country);
        when(address.getPhone1()).thenReturn("phone1");
        final CustomerInfo customerInfo = converter.getCustomerInfoFromTargetCustomerModel(customerModel, address,
                null, null);

        assertEquals("Id ", PROMOTION_CODE, customerInfo.getId());
        assertEquals("First name ", "Ted", customerInfo.getFirstName());
        assertEquals("Last name ", "Baker", customerInfo.getLastName());
        assertEquals("Id ", "ted@bed.com", customerInfo.getEmail());
        assertNull("Title ", customerInfo.getTitle());
        assertEquals("Address size ", 1, customerInfo.getAddress().size());
        assertNotNull(customerInfo.getAddress().get(0).getPhone());
        assertEquals("phone1", customerInfo.getAddress().get(0).getPhone());

    }

    @Test
    public void testSetMobileInfoGetCustomerInfoFromDeliveryAddress()
    {
        final TargetAddressModel address = mock(TargetAddressModel.class);
        when(address.getCountry()).thenReturn(country);
        when(address.getPhone1()).thenReturn("phone1");
        final CustomerInfo customerInfo = converter.getCustomerInfoFromTargetCustomerModel(customerModel, null,
                address, targetPos);

        assertEquals("Id ", PROMOTION_CODE, customerInfo.getId());
        assertEquals("First name ", "Ted", customerInfo.getFirstName());
        assertEquals("Last name ", "Baker", customerInfo.getLastName());
        assertEquals("Id ", "ted@bed.com", customerInfo.getEmail());
        assertNull("Title ", customerInfo.getTitle());
        assertEquals("Address size ", 1, customerInfo.getAddress().size());
        assertEquals("phone1", customerInfo.getAddress().get(0).getMobile());

    }

    @Test
    public void testGetCustomerInfoFromNull() {
        assertNull("CustomerInfo ", converter.getCustomerInfoFromTargetCustomerModel(null, null, null, null));
    }

    @Test
    public void testCustomerInfoHas2AddressesIfBothBillingAndDeliveryArePresent() {
        final TargetAddressModel billing = mock(TargetAddressModel.class);
        final TargetAddressModel delivery = mock(TargetAddressModel.class);
        when(billing.getCountry()).thenReturn(country);
        when(delivery.getCountry()).thenReturn(country);

        final CustomerInfo customerInfo = converter.getCustomerInfoFromTargetCustomerModel(customerModel, billing,
                delivery, targetPos);
        assertNotNull("CustomerInfo ", customerInfo);
        assertEquals("Address size ", 2, customerInfo.getAddress().size());

    }

    @Test
    public void testGetAddressInfoFromNull() {
        assertNull(converter.getAddressInfoFromAddressModel(null, null, null));
    }

    @Test
    public void testGetAddressInfoFromTargetPos() {
        final AddressInfo delivery = converter.getAddressInfoFromAddressModel(addressModel, null, targetPos);
        verify(addressModel, times(0)).getLine2();
        assertEquals("POS-pos", delivery.getAddressLine1());
    }

    @Test
    public void testSetMobileFromCellPhoneForDelivery() {
        when(addressModel.getCellphone()).thenReturn("cellphone");
        when(addressModel.getPhone1()).thenReturn("phone1");
        final AddressInfo delivery = converter.getAddressInfoFromAddressModel(addressModel,
                AddressTypeEnum.DELIVERY, null);

        assertEquals("cellphone", delivery.getMobile());
        assertNull(delivery.getPhone());

    }

    @Test
    public void testGetAddressInfoFromDelivery() {
        final AddressInfo delivery = converter.getAddressInfoFromAddressModel(addressModel,
                AddressTypeEnum.DELIVERY, null);

        assertEquals("Type ", AddressTypeEnum.DELIVERY, delivery.getType());
        assertEquals("Line 1 ", "12 Thompson rd", delivery.getAddressLine1());
        assertNull("Line 2 ", delivery.getAddressLine2());
        assertEquals("Town ", "Geelong East", delivery.getCity());
        assertEquals("Country ", "Australia", delivery.getCountry());
        assertEquals("PostalCode ", "3219", delivery.getPostCode());
        assertNull("Phone ", delivery.getPhone());
        assertEquals("Mobile ", "0426888999", delivery.getMobile());
    }

    @Test
    public void testGetAddressInfoFromBilling() {
        final AddressInfo billing = converter.getAddressInfoFromAddressModel(addressModel,
                AddressTypeEnum.BILLING, null);
        assertEquals("Type ", AddressTypeEnum.BILLING, billing.getType());
        assertEquals("Line 1 ", "12 Thompson rd", billing.getAddressLine1());
        assertNull("Line 2 ", billing.getAddressLine2());
        assertEquals("Town ", "Geelong East", billing.getCity());
        assertEquals("Country ", "Australia", billing.getCountry());
        assertEquals("PostalCode ", "3219", billing.getPostCode());
        assertEquals("Phone ", "0352943333", billing.getPhone());
        assertEquals("Mobile ", "0426888999", billing.getMobile());
    }
}
