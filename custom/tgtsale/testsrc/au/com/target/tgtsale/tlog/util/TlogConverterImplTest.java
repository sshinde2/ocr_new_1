/**
 * 
 */
package au.com.target.tgtsale.tlog.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.enums.ReplacementReason;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReplacementEntryModel;
import de.hybris.platform.returns.model.ReturnOrderEntryModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.currency.conversion.service.impl.CurrencyConversionFactorServiceImpl;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtsale.constants.TgtsaleConstants;
import au.com.target.tgtsale.tlog.converter.TlogPaymentEntryConverter;
import au.com.target.tgtsale.tlog.converter.impl.TlogCancelConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogCustomerConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogDealConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogPaymentEntryConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogSaleConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogTMDConverterImpl;
import au.com.target.tgtsale.tlog.data.AddressInfo;
import au.com.target.tgtsale.tlog.data.AddressTypeEnum;
import au.com.target.tgtsale.tlog.data.CustomerInfo;
import au.com.target.tgtsale.tlog.data.DealDetails;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.DiscountTypeEnum;
import au.com.target.tgtsale.tlog.data.ItemDeal;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.ReasonTypeEnum;
import au.com.target.tgtsale.tlog.data.ShippingInfo;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;
import au.com.target.tgtsale.tlog.data.Transaction;
import au.com.target.tgtsale.tlog.data.TransactionTypeEnum;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TlogConverterImplTest {


    /**
     * 
     */
    private static final String PRODUCT_P12346 = "P12346";
    /**
     * 
     */
    private static final String PRODUCT_P12345 = "P12345";
    /**
     * 
     */
    private static final String DEAL_REWARD = "R";
    /**
     * 
     */
    private static final String PROMOTION_CODE = "12345";
    private static final String PRODUCT_P12347 = "P12347";
    private static final String PRODUCT_P12348 = "P12348";
    private static final String CUSTOMER_FIRST_NAME = "Hybris";
    private static final String CUSTOMER_LAST_NAME = "Test";
    private static final String ADDRESS_LINE_1 = "12 Thompson road";
    private static final String CITY = "Geelong";
    private static final String COUNTRY = "Australia";
    private static final String POSTAL_CODE = "3215";
    private static final String GEELONG_STORE_NAME = "Geelong CBD Store";
    private static final Date DUE_DATE = new Date(System.currentTimeMillis());
    private static final String RECEIPT_NUMBER = "123456";
    private static final String CARD_NUMBER = "424242******4242";
    private static final String ORDER_NUMBER = "order123456";







    private PromotionOrderResults promotionOrderResults;

    private final TlogCancelConverterImpl tlogCancelConverter = new TlogCancelConverterImpl();
    private final TlogCustomerConverterImpl tlogCustomerConverter = new TlogCustomerConverterImpl();
    private final TlogSaleConverterImpl tlogSaleConverter = new TlogSaleConverterImpl();
    private final TlogTMDConverterImpl tlogTMDConverter = new TlogTMDConverterImpl();
    private final TlogDealConverterImpl tlogDealConverter = new TlogDealConverterImpl();
    private final TlogPaymentEntryConverterImpl tlogPaymentEntryConverter = new TlogPaymentEntryConverterImpl();
    private final CurrencyConversionFactorServiceImpl currencyConversionFactorService = new CurrencyConversionFactorServiceImpl();

    private final TlogConverterImpl tlogConverter = new TlogConverterImpl();

    @Mock
    private CountryModel country;

    @Mock
    private AddressModel address;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfig;

    @Mock
    private PromotionResult promotionResult;

    @Mock
    private TargetLaybyPaymentDueService targetLaybyPaymentDueService;

    @Mock
    private TMDiscountProductPromotion tmDiscountProductPromotion;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetPointOfServiceModel pos;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SessionContext sessioncontext = mock(SessionContext.class);
        final AbstractOrder abstractOrder = mock(AbstractOrder.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        promotionOrderResults = new PromotionOrderResults(sessioncontext, abstractOrder,
                Collections.singletonList(promotionResult), 2.1);
        given(promotionResult.getSession()).willReturn(jaloSession);
        given(Boolean.valueOf(promotionResult.getFired())).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(promotionResult.isApplied())).willReturn(Boolean.TRUE);
        given(promotionResult.getPromotion()).willReturn(tmDiscountProductPromotion);
        given(promotionResult.getCustom()).willReturn("0");
        tlogCancelConverter.setTlogPaymentEntryConverter(tlogPaymentEntryConverter);
        tlogSaleConverter.setTlogDealConverter(tlogDealConverter);
        tlogSaleConverter.setTlogTMDConverter(tlogTMDConverter);
        tlogSaleConverter.setCurrencyConversionFactorService(currencyConversionFactorService);
        tlogConverter.setTlogCustomerConverter(tlogCustomerConverter);
        tlogConverter.setTlogCancelConverter(tlogCancelConverter);
        tlogConverter.setTlogTMDConverter(tlogTMDConverter);
        tlogConverter.setTlogSaleConverter(tlogSaleConverter);
        tlogConverter.setTlogPaymentEntryConverter(tlogPaymentEntryConverter);
        tlogConverter.setTargetLaybyPaymentDueService(targetLaybyPaymentDueService);
        tlogConverter.setTargetPointOfServiceService(targetPointOfServiceService);

    }

    @Test
    public void testGetOrderInfoFromOrderModel() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);

        assertNull("OrderInfo ", tlogConverter.getOrderInfoFromOrderModel(null, null));

        given(orderModel.getCode()).willReturn("123456");

        assertThat(tlogConverter.getOrderInfoFromOrderModel(orderModel, null).getId()).isEqualTo("123456");
    }

    @Test
    public void testGetTransactionForModificationRecordEntry() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderModificationRecordModel orderModificationRecordModel = Mockito
                .mock(OrderModificationRecordModel.class);

        final OrderCancelRecordEntryModel orderCancelRecordEntry = Mockito.mock(OrderCancelRecordEntryModel.class);
        final TMDiscountPromotionModel tmd = Mockito.mock(TMDiscountPromotionModel.class);

        final PaymentTransactionModel ptm = Mockito.mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel capture = Mockito.mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel refund = Mockito.mock(PaymentTransactionEntryModel.class);
        final CreditCardPaymentInfoModel ccard = Mockito.mock(CreditCardPaymentInfoModel.class);

        given(ccard.getNumber()).willReturn("");

        given(capture.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(capture.getTime()).willReturn(new Date());
        given(capture.getCreationtime()).willReturn(new Date());
        given(capture.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(capture.getReceiptNo()).willReturn("");
        given(capture.getAmount()).willReturn(BigDecimal.valueOf(1.00d));
        given(capture.getPaymentTransaction()).willReturn(ptm);

        given(refund.getType()).willReturn(PaymentTransactionType.REFUND_STANDALONE);
        given(refund.getTime()).willReturn(new Date());
        given(refund.getCreationtime()).willReturn(new Date());
        given(refund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(refund.getReceiptNo()).willReturn("");
        given(refund.getAmount()).willReturn(BigDecimal.valueOf(21.00d));
        given(refund.getPaymentTransaction()).willReturn(ptm);

        given(ptm.getEntries()).willReturn(ImmutableList.of(capture, refund));
        given(ptm.getPaymentProvider()).willReturn("tns");
        given(ptm.getInfo()).willReturn(ccard);

        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(ptm));

        given(orderCancelRecordEntry.getModificationRecord()).willReturn(orderModificationRecordModel);
        given(orderModificationRecordModel.getOrder()).willReturn(orderModel);
        given(orderCancelRecordEntry.getRefundedShippingAmount()).willReturn(Double.valueOf(10.0));
        given(orderCancelRecordEntry.getRefundedAmount()).willReturn(Double.valueOf(21.00d));
        given(orderCancelRecordEntry.getOrderEntriesModificationEntries()).willReturn(Collections.EMPTY_LIST);

        given(tmd.getPercentageDiscount()).willReturn(Double.valueOf(5.00d));

        final Transaction result = tlogConverter.getTransactionForModificationRecordEntry(orderCancelRecordEntry,
                null, ImmutableList.of(refund), null);

        assertThat(result.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(-10.0));
        assertThat(result.getTender().size()).isEqualTo(2);
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithFlybuysRefund() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderCancelRecordEntryModel orderCancelRecordEntry = Mockito.mock(OrderCancelRecordEntryModel.class);
        final OrderModificationRecordModel orderModificationRecordModel = Mockito
                .mock(OrderModificationRecordModel.class);
        final PaymentTransactionModel ptm = Mockito.mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel capture = Mockito.mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel refund = Mockito.mock(PaymentTransactionEntryModel.class);
        final CreditCardPaymentInfoModel ccard = Mockito.mock(CreditCardPaymentInfoModel.class);

        final List<DiscountModel> discountModels = new ArrayList<>();
        final FlybuysDiscountModel flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);
        discountModels.add(flybuysDiscountModel);

        given(ccard.getNumber()).willReturn("");

        given(capture.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(capture.getTime()).willReturn(new Date());
        given(capture.getCreationtime()).willReturn(new Date());
        given(capture.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(capture.getReceiptNo()).willReturn("");
        given(capture.getAmount()).willReturn(BigDecimal.valueOf(1.00d));
        given(capture.getPaymentTransaction()).willReturn(ptm);

        given(refund.getType()).willReturn(PaymentTransactionType.REFUND_STANDALONE);
        given(refund.getTime()).willReturn(new Date());
        given(refund.getCreationtime()).willReturn(new Date());
        given(refund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(refund.getReceiptNo()).willReturn("");
        given(refund.getAmount()).willReturn(BigDecimal.valueOf(21.00d));
        given(refund.getPaymentTransaction()).willReturn(ptm);

        given(ptm.getEntries()).willReturn(ImmutableList.of(capture, refund));
        given(ptm.getPaymentProvider()).willReturn("tns");
        given(ptm.getInfo()).willReturn(ccard);

        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(ptm));
        given(orderModel.getFlyBuysCode()).willReturn("testFlybuys");
        given(orderModel.getDiscounts()).willReturn(discountModels);
        given(flybuysDiscountModel.getRedeemCode()).willReturn("testRedeem");

        given(orderCancelRecordEntry.getModificationRecord()).willReturn(orderModificationRecordModel);
        given(orderModificationRecordModel.getOrder()).willReturn(orderModel);
        given(orderCancelRecordEntry.getRefundedShippingAmount()).willReturn(Double.valueOf(10.0));
        given(orderCancelRecordEntry.getRefundedAmount()).willReturn(Double.valueOf(10d));
        given(orderCancelRecordEntry.getRefundedFlybuysAmount()).willReturn(Double.valueOf(5d));
        given(orderCancelRecordEntry.getRefundedFlybuysPoints()).willReturn(Integer.valueOf(1000));
        given(orderCancelRecordEntry.getFlybuysPointsRefundConfirmationCode()).willReturn("testConfirm");
        given(orderCancelRecordEntry.getOrderEntriesModificationEntries()).willReturn(Collections.EMPTY_LIST);

        final Transaction result = tlogConverter.getTransactionForModificationRecordEntry(orderCancelRecordEntry,
                null, ImmutableList.of(refund), null);

        assertThat(result.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(-10.0));
        assertThat(result.getTender().size()).isEqualTo(2);
        assertNotNull(result.getDiscounts());

        final DiscountInfo fbDiscountInfo = result.getDiscounts().iterator().next();
        assertThat(fbDiscountInfo.getType()).isEqualTo(DiscountTypeEnum.FLYBUYS);
        assertThat(fbDiscountInfo.getCode()).isEqualTo("testFlybuys");
        assertThat(fbDiscountInfo.getRedeemCode()).isEqualTo("testRedeem");
        assertThat(fbDiscountInfo.getConfirmationCode()).isEqualTo("testConfirm");
        assertThat(fbDiscountInfo.getPoints()).isEqualTo(Integer.valueOf(1000));
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithFlybuysRefundZero() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderCancelRecordEntryModel orderCancelRecordEntry = Mockito.mock(OrderCancelRecordEntryModel.class);
        final OrderModificationRecordModel orderModificationRecordModel = Mockito
                .mock(OrderModificationRecordModel.class);
        final PaymentTransactionModel ptm = Mockito.mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel capture = Mockito.mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel refund = Mockito.mock(PaymentTransactionEntryModel.class);
        final CreditCardPaymentInfoModel ccard = Mockito.mock(CreditCardPaymentInfoModel.class);

        final List<DiscountModel> discountModels = new ArrayList<>();
        final FlybuysDiscountModel flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);
        discountModels.add(flybuysDiscountModel);

        given(ccard.getNumber()).willReturn("");

        given(capture.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(capture.getTime()).willReturn(new Date());
        given(capture.getCreationtime()).willReturn(new Date());
        given(capture.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(capture.getReceiptNo()).willReturn("");
        given(capture.getAmount()).willReturn(BigDecimal.valueOf(1.00d));
        given(capture.getPaymentTransaction()).willReturn(ptm);

        given(refund.getType()).willReturn(PaymentTransactionType.REFUND_STANDALONE);
        given(refund.getTime()).willReturn(new Date());
        given(refund.getCreationtime()).willReturn(new Date());
        given(refund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(refund.getReceiptNo()).willReturn("");
        given(refund.getAmount()).willReturn(BigDecimal.valueOf(21.00d));
        given(refund.getPaymentTransaction()).willReturn(ptm);

        given(ptm.getEntries()).willReturn(ImmutableList.of(capture, refund));
        given(ptm.getPaymentProvider()).willReturn("tns");
        given(ptm.getInfo()).willReturn(ccard);

        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(ptm));
        given(orderModel.getFlyBuysCode()).willReturn("testFlybuys");
        given(orderModel.getDiscounts()).willReturn(discountModels);
        given(flybuysDiscountModel.getRedeemCode()).willReturn("testRedeem");

        given(orderCancelRecordEntry.getModificationRecord()).willReturn(orderModificationRecordModel);
        given(orderModificationRecordModel.getOrder()).willReturn(orderModel);
        given(orderCancelRecordEntry.getRefundedShippingAmount()).willReturn(Double.valueOf(10.0));
        given(orderCancelRecordEntry.getRefundedAmount()).willReturn(Double.valueOf(10d));
        given(orderCancelRecordEntry.getRefundedFlybuysAmount()).willReturn(Double.valueOf(5d));
        given(orderCancelRecordEntry.getRefundedFlybuysPoints()).willReturn(Integer.valueOf(0));
        given(orderCancelRecordEntry.getFlybuysPointsRefundConfirmationCode()).willReturn("testConfirm");
        given(orderCancelRecordEntry.getOrderEntriesModificationEntries()).willReturn(Collections.EMPTY_LIST);

        final Transaction result = tlogConverter.getTransactionForModificationRecordEntry(orderCancelRecordEntry,
                null, ImmutableList.of(refund), null);

        assertThat(result.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(-10.0));
        assertThat(result.getTender().size()).isEqualTo(2);
        assertThat(result.getDiscounts()).isNull();
    }

    @Test
    public void testGetCustomerInfoFromTargetCustomerModel() {
        final TargetCustomerModel customerModel = Mockito.mock(TargetCustomerModel.class);
        final TargetAddressModel billing = Mockito.mock(TargetAddressModel.class);
        final CountryModel country = Mockito.mock(CountryModel.class);

        given(country.getName()).willReturn("Australia");
        given(billing.getCountry()).willReturn(country);

        assertNull("CustomerInfo ", tlogConverter.getCustomerInfoFromTargetCustomerModel(null, null, null, null));

        given(customerModel.getCustomerID()).willReturn(PROMOTION_CODE);
        given(customerModel.getFirstname()).willReturn("Ted");
        given(customerModel.getLastname()).willReturn("Baker");
        given(customerModel.getTitle()).willReturn(null);
        given(customerModel.getContactEmail()).willReturn("ted@bed.com");

        final CustomerInfo customerInfo = tlogConverter.getCustomerInfoFromTargetCustomerModel(customerModel,
                billing,
                null, null);

        assertThat(customerInfo.getId()).isEqualTo(PROMOTION_CODE);
        assertThat(customerInfo.getFirstName()).isEqualTo("Ted");
        assertThat(customerInfo.getLastName()).isEqualTo("Baker");
        assertThat(customerInfo.getEmail()).isEqualTo("ted@bed.com");
        assertThat(customerInfo.getTitle()).isNull();
        assertThat(customerInfo.getAddress().size()).isEqualTo(1);

    }


    @Test
    public void testGetItemListFromReturnEntries() {
        final List<AbstractOrderEntryModel> orderEntries = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            final ReturnOrderEntryModel returnEntryModel = Mockito.mock(ReturnOrderEntryModel.class);

            final TargetProductModel targetProduct = new TargetProductModel();
            targetProduct.setCode(PROMOTION_CODE);
            given(returnEntryModel.getProduct()).willReturn(targetProduct);
            orderEntries.add(returnEntryModel);
        }

        //        assertThat("Item list ", 5, TlogHelper.getItemListFromOrderEntries(orderEntries, null).size());
    }


    @Test
    public void testGetTransactionForModificationRecordEntryEmptyShippingCost() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 10.0d, 2);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 10.0d, 1);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2);
        orderRecordModel.setRefundedAmount(Double.valueOf(1.0d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 1.0d);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(1.0d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);


        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(0.33d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(0.34d).negate());
            }
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCost() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 10.0d, 2);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 10.0d, 1);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(1.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(10.0d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 10.0d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(10.0d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(3d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(3d).negate());
            }
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostLTTender() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 10.0d, 2);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 10.0d, 1);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(6.0d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 6.0d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(6.0d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(2d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(2d).negate());
            }
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostLTTenderNTenderIs0() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 10.0d, 2);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 10.0d, 1);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(0d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 0d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);


        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", null, null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.ZERO);
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.ZERO);
            }
        }

        assertThat(transaction.getTender()).isNull();
    }


    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCost1() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 10.0d, 3);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 10.0d, 1);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2, 3L,
                1L);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(34.0d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 34.0d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(34.0d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(3);

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(6.25d).negate());
                assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12345);
            }
            else {
                if (item.getId().equals(PRODUCT_P12346)) {
                    assertThat(item.getPrice()).isNotNull()
                            .isEqualByComparingTo(BigDecimal.valueOf(6.25d).negate());
                }
                else {
                    assertThat(item.getPrice()).isNotNull()
                            .isEqualByComparingTo(BigDecimal.valueOf(6.25d).negate());
                }
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            }
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
            assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(10.0d));
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCost9Entries() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 5.0d, 1);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 5.0d, 1);
        final OrderEntryModel entry3 = buildOrderEntryModel(PRODUCT_P12347, 5.0d, 1);
        final OrderEntryModel entry4 = buildOrderEntryModel(PRODUCT_P12348, 2.0d, 1);
        final OrderEntryModel entry5 = buildOrderEntryModel("P12349", 2.0d, 1);
        final OrderEntryModel entry6 = buildOrderEntryModel("P12350", 2.0d, 1);
        final OrderEntryModel entry7 = buildOrderEntryModel("P12351", 3.0d, 1);
        final OrderEntryModel entry8 = buildOrderEntryModel("P12352", 3.0d, 1);
        final OrderEntryModel entry9 = buildOrderEntryModel("P12353", 3.0d, 1);


        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2,
                entry3, entry4, entry5, entry6, entry7, entry8, entry9);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(5.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(28.55d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 28.55d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(28.55d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(9);

        for (final ItemInfo item : items) {
            if (item.getFilePrice().intValue() == 5) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(3.92d).negate());
            }
            else if (item.getFilePrice().intValue() == 2.0) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(1.57d).negate());
            }
            else {
                assertThat(item.getPrice().equals(BigDecimal.valueOf(-2.35d))
                        || item.getPrice().equals(BigDecimal.valueOf(-2.38d))).isTrue();
            }
            assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostWithOneEntry() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12346, 15.0d, 3);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, null, 3L,
                0L);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(48.0d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, null, 48.0d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(48.0d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(13d).negate());
                assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12346);
            }
            else {
                if (item.getId().equals(PRODUCT_P12346)) {
                    assertThat(item.getPrice()).isNotNull()
                            .isEqualByComparingTo(BigDecimal.valueOf(13d).negate());
                }
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            }
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
            assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(15.0d));
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostWithOneEntryDecimalTender() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12346, 15.00d, 5);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, null, 3L,
                0L);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(69.99d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, null, 69.99d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(69.99d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(20.33d).negate());
                assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12346);
            }
            else {
                if (item.getId().equals(PRODUCT_P12346)) {
                    assertThat(item.getPrice()).isNotNull()
                            .isEqualByComparingTo(BigDecimal.valueOf(20.33d).negate());
                }
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            }
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
            assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(15.0d));
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostWithOneEntryWithDecimal() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12346, 13.99d, 5);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, null, 3L,
                0L);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(71.5025d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, null, 71.5025d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(71.5025d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(20.83d).negate());
                assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12346);
            }
            else {
                if (item.getId().equals(PRODUCT_P12346)) {
                    assertThat(item.getPrice()).isNotNull()
                            .isEqualByComparingTo(BigDecimal.valueOf(20.84d).negate());
                }
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            }
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
            assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(13.99d));
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostWithOneEntry2() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12346, 15.00d, 15);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, null, 15L,
                0L);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(207.00d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, null, 207.00d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(207.00d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12346);
            if (item.getQuantity().longValue() == 14L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualTo(BigDecimal.valueOf(13.20d).setScale(2, RoundingMode.HALF_UP).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualTo(BigDecimal.valueOf(13.20).setScale(2, RoundingMode.HALF_UP).negate());
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            }
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
            assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(15.0d));
        }
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithShippingCostTwoEntries() {

        final OrderEntryModel entry1 = buildOrderEntryModel(PRODUCT_P12345, 15.0d, 9);
        final OrderEntryModel entry2 = buildOrderEntryModel(PRODUCT_P12346, 26.0d, 3);

        final OrderModificationRecordEntryModel orderRecordModel = buildOrderModificationEntryModel(entry1, entry2, 9L,
                3L);
        orderRecordModel.setRefundedShippingAmount(Double.valueOf(9.0d));
        orderRecordModel.setRefundedAmount(Double.valueOf(19.6d));
        final OrderModificationRecordModel orderModificationRecord = new OrderModificationRecordModel();

        final OrderModel ordermodel = buildOrderForRefunds(entry1, entry2, 19.6d);

        orderModificationRecord.setOrder(ordermodel);
        orderRecordModel.setModificationRecord(orderModificationRecord);

        final PaymentTransactionModel refundTransaction = setupPaymentTransactionModel(19.6d);

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(orderRecordModel,
                "testUser", refundTransaction.getEntries(), null);

        assertThat(transaction).isNotNull();

        final List<ItemInfo> items = transaction.getItems();

        assertThat(items).isNotNull().isNotEmpty().hasSize(3);

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 8L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(0.75d).negate());
                assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12345);
                assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(15.0d));
            }
            else if (item.getQuantity().longValue() == 3L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(1.29d).negate());
                assertThat(item.getId()).isNotNull().isEqualTo(PRODUCT_P12346);
                assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(26.0d));
            }
            else {
                if (item.getId().equals(PRODUCT_P12345)) {
                    assertThat(item.getPrice()).isNotNull()
                            .isEqualTo(BigDecimal.valueOf(0.73d).negate());
                }
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
                assertThat(item.getFilePrice()).isEqualTo(BigDecimal.valueOf(15.0d));
            }
            assertThat(item.getReason()).isEqualTo(ReasonTypeEnum.FAULTY);
        }
    }

    /**
     * @return OrderEntryModel
     */
    private OrderEntryModel buildOrderEntryModel(final String productCode, final double basePrice, final int quantity) {
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(productCode);
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(basePrice));
        entry1.setQuantity(Long.valueOf(quantity));
        entry1.setProduct(targetProduct);
        return entry1;
    }

    /**
     * @param entry1
     * @param entry2
     * @param e
     * @param d
     * @return OrderModificationRecordEntryModel
     */
    private OrderModificationRecordEntryModel buildOrderModificationEntryModel(final OrderEntryModel entry1,
            final OrderEntryModel entry2, final long d, final long e) {
        final OrderModificationRecordEntryModel orderRecordModel = new OrderReturnRecordEntryModel();
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();

        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(d));

        modRecordEntry1.setOrderEntry(entry1);
        if (entry1 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry2.setReturnedQuantity(Long.valueOf(e));

            modRecordEntry2.setOrderEntry(entry2);
            orderEntries.add(modRecordEntry2);
        }
        orderEntries.add(modRecordEntry1);
        orderRecordModel.setOrderEntriesModificationEntries(orderEntries);
        return orderRecordModel;
    }


    /**
     * @param entry1
     * @param entry2
     * @param entry3
     * @param entry4
     * @param entry5
     * @param entry6
     * @param entry7
     * @param entry8
     * @param entry9
     * @return OrderModificationRecordEntryModel
     */
    private OrderModificationRecordEntryModel buildOrderModificationEntryModel(final OrderEntryModel entry1,
            final OrderEntryModel entry2, final OrderEntryModel entry3, final OrderEntryModel entry4,
            final OrderEntryModel entry5, final OrderEntryModel entry6, final OrderEntryModel entry7,
            final OrderEntryModel entry8, final OrderEntryModel entry9) {
        final OrderModificationRecordEntryModel orderRecordModel = new OrderReturnRecordEntryModel();
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();

        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(1));

        modRecordEntry1.setOrderEntry(entry1);
        orderEntries.add(modRecordEntry1);
        if (entry2 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry2.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry2.setOrderEntry(entry2);
            orderEntries.add(modRecordEntry2);
        }
        if (entry3 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry3 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry3.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry3.setOrderEntry(entry3);
            orderEntries.add(modRecordEntry3);
        }
        if (entry4 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry4 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry4.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry4.setOrderEntry(entry4);
            orderEntries.add(modRecordEntry4);
        }
        if (entry5 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry5 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry5.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry5.setOrderEntry(entry5);
            orderEntries.add(modRecordEntry5);
        }
        if (entry6 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry6 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry6.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry6.setOrderEntry(entry6);
            orderEntries.add(modRecordEntry6);
        }
        if (entry7 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry7 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry7.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry7.setOrderEntry(entry7);
            orderEntries.add(modRecordEntry7);
        }
        if (entry8 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry8 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry8.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry8.setOrderEntry(entry8);
            orderEntries.add(modRecordEntry8);
        }
        if (entry9 != null) {
            final OrderEntryReturnRecordEntryModel modRecordEntry9 = new OrderEntryReturnRecordEntryModel();
            modRecordEntry9.setReturnedQuantity(Long.valueOf(1));

            modRecordEntry9.setOrderEntry(entry9);
            orderEntries.add(modRecordEntry9);
        }

        orderRecordModel.setOrderEntriesModificationEntries(orderEntries);
        return orderRecordModel;
    }

    /**
     * @param entry1
     * @param entry2
     * @return OrderModificationRecordEntryModel
     */
    private OrderModificationRecordEntryModel buildOrderModificationEntryModel(final OrderEntryModel entry1,
            final OrderEntryModel entry2) {
        final OrderModificationRecordEntryModel orderRecordModel = new OrderReturnRecordEntryModel();
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();

        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2));

        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1));

        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);
        orderRecordModel.setOrderEntriesModificationEntries(orderEntries);
        return orderRecordModel;
    }

    /**
     * @param entry1
     * @param entry2
     * @return OrderModel
     */
    public OrderModel buildOrderForRefunds(final OrderEntryModel entry1, final OrderEntryModel entry2,
            final double tenderAmount) {
        final OrderModel ordermodel = new OrderModel();
        final List<AbstractOrderEntryModel> orderModelEntries = new ArrayList<>();
        orderModelEntries.add(entry1);
        if (entry2 != null) {
            orderModelEntries.add(entry2);
        }
        ordermodel.setEntries(orderModelEntries);
        //paymentTransaction
        ordermodel.setPaymentTransactions(Collections.singletonList(setupPaymentTransactionModel(tenderAmount)));
        return ordermodel;
    }

    /**
     * @param tenderAmount
     * @return PaymentTransactionModel
     */
    private PaymentTransactionModel setupPaymentTransactionModel(final double tenderAmount) {
        final PaymentTransactionModel paymentTransactionModel = new PaymentTransactionModel();
        paymentTransactionModel.setPaymentProvider("paypal");
        final PaypalPaymentInfoModel paypalPaymentInfo = new PaypalPaymentInfoModel();
        paypalPaymentInfo.setPayerId("testPayer");
        paymentTransactionModel.setInfo(paypalPaymentInfo);
        final PaymentTransactionEntryModel paymentEntryModel = new PaymentTransactionEntryModel();
        paymentEntryModel.setType(PaymentTransactionType.REFUND_FOLLOW_ON);
        paymentEntryModel.setAmount(BigDecimal.valueOf(tenderAmount));
        paymentEntryModel.setTransactionStatus("ACCEPTED");
        paymentEntryModel.setPaymentTransaction(paymentTransactionModel);
        paymentEntryModel.setReceiptNo("PP12345b");
        paymentTransactionModel.setEntries(Collections.singletonList(paymentEntryModel));
        return paymentTransactionModel;
    }

    @Test
    public void testGetItemListForModifiedEntriesDecimalTenderAndBasePrices1() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PRODUCT_P12345);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.0d));
        entry1.setQuantity(Long.valueOf(2));
        final TargetProductModel targetProduct1 = new TargetProductModel();
        targetProduct1.setCode(PRODUCT_P12346);
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(10.0d));
        entry2.setQuantity(Long.valueOf(1));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(1.0d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity().longValue() == 2L) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(0.33d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(0.34d).negate());
            }
        }
    }

    @Ignore
    public void testGetReasonForEntry() {
        final ReplacementEntryModel replacementEntryModel = Mockito.mock(ReplacementEntryModel.class);
        final RefundEntryModel refundEntryModel = Mockito.mock(RefundEntryModel.class);

        given(replacementEntryModel.getReason()).willReturn(ReplacementReason.DAMAGEDINTRANSIT);
        given(refundEntryModel.getReason()).willReturn(RefundReason.GOODWILL);

        //assertThat("Replacement reason ", ReasonTypeEnum.FAULTY, TlogHelper.getReasonForEntry(replacementEntryModel));
        //assertThat("Refund reason ", ReasonTypeEnum.NOT_FAULTY, TlogHelper.getReasonForEntry(refundEntryModel));
    }


    @Test
    public void testGetShippingInfo() {
        final ShippingInfo shippingInfo = tlogConverter.getShippingInfo(BigDecimal.valueOf(09.35d), null);

        assertThat(shippingInfo.getAmount()).isEqualTo(BigDecimal.valueOf(09.35d));
        assertThat(shippingInfo.getOriginalAmount()).isNull();
    }


    @Test
    public void testAllEntriesConsumedByDeal() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 2L);
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        order.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order, null,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(1);
        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(1500);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(2);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal1 = item1.getItemDeal();
        assertThat(itemDeal1).isNotNull();
        assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item1.getItemDiscount()).isNull();

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(2L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal2 = item2.getItemDeal();
        assertThat(itemDeal2).isNotNull();
        assertThat(itemDeal2.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal2.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item2.getItemDiscount()).isNull();
    }

    @Test
    public void testAllEntriesConsumedByDealMarkdownZero() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = createOrderModel(1L, 0L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L, 10d);
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        order.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order,
                promotionOrderResults,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(1);
        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(0);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(1);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
    }

    @Test
    public void testAllEntriesConsumedByDealNoTMD() {
        final OrderModel order = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 2L);
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        order.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order, null,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(1);
        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(1500);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(2);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal1 = item1.getItemDeal();
        assertThat(itemDeal1).isNotNull();
        assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item1.getItemDiscount()).isNull();

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(2L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal2 = item2.getItemDeal();
        assertThat(itemDeal2).isNotNull();
        assertThat(itemDeal2.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal2.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item2.getItemDiscount()).isNull();
    }

    @Test
    public void testAllEntriesExcept1ConsumedByDeal() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 1L);
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        order.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order, null,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(1);
        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(1000);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(3);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal1 = item1.getItemDeal();
        assertThat(itemDeal1).isNotNull();
        assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item1.getItemDiscount()).isNull();

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal2 = item2.getItemDeal();
        assertThat(itemDeal2).isNotNull();
        assertThat(itemDeal2.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal2.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item2.getItemDiscount()).isNull();

        final ItemInfo item3 = items.get(2);
        assertThat(item3.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item3.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item3.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        assertThat(item3.getItemDeal()).isNull();
        assertThat(item3.getItemDiscount()).isNull();
    }

    @Test
    public void testNoEntriesConsumedByDeal() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = createOrderModel(1L, 2L, 0L, 0L);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order,
                promotionOrderResults,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNull();
        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(2);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        assertThat(item1.getItemDeal()).isNull();

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(2L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        assertThat(item2.getItemDeal()).isNull();
    }

    @Test
    public void testExcept2ConsumedByDeal1ConsumedByTMD() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = createOrderModel(1L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 1L);

        final PromotionResultModel promoResult2 = new PromotionResultModel();
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1, 1L);
        promoResult2.setPromotion(tmdDiscountPromotionModel);
        consumedEntries1.add(consumedEntry3);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResult2.setCertainty(Float.valueOf(1.0f));

        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        order.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order, null,
                deals, 0.5d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(1);
        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(1000);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(3);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal1 = item1.getItemDeal();
        assertThat(itemDeal1).isNotNull();
        assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item1.getItemDiscount()).isNull();

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal2 = item2.getItemDeal();
        DiscountInfo itemDiscount = item2.getItemDiscount();
        if (itemDiscount != null) {
            assertThat(itemDiscount.getAmount()).isNotNull().isEqualByComparingTo(new BigDecimal(0.50));
        }
        else {
            assertThat(itemDeal2).isNotNull();
            assertThat(itemDeal2).isNotNull();
            assertThat(itemDeal2.getType()).isNotEmpty().contains(DEAL_REWARD);
            assertThat(itemDeal2.getMarkdown()).isNotNull().isEqualTo("500");
            assertThat(item2.getItemDiscount()).isNull();
        }

        final ItemInfo item3 = items.get(2);
        assertThat(item3.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item3.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item3.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal3 = item3.getItemDeal();
        itemDiscount = item3.getItemDiscount();
        if (itemDiscount != null) {
            assertThat(itemDiscount.getAmount()).isNotNull().isEqualByComparingTo(new BigDecimal(0.50));
        }
        else {
            assertThat(itemDeal3).isNotNull();
            assertThat(itemDeal3).isNotNull();
            assertThat(itemDeal3.getType()).isNotEmpty().contains(DEAL_REWARD);
            assertThat(itemDeal3.getMarkdown()).isNotNull().isEqualTo("500");
            assertThat(item3.getItemDiscount()).isNull();
        }
    }

    @Test
    public void testMultipleInstancesSameDeal() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = populateDataForSameDeal();

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order, null,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(2);
        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(1000);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final DealDetails details2 = dealDetails.get("2:" + PROMOTION_CODE);
        assertThat(details2.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details2.getMarkdown()).isNotNull().isEqualTo(1000);
        assertThat(details2.getType()).isNotEmpty().contains("6");
        assertThat(details2.getInstance()).isNotEmpty().contains("2");

        int countInstance1 = 0;
        int countInstance2 = 0;

        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(4);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal1 = item1.getItemDeal();
        assertThat(itemDeal1).isNotNull();
        assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item1.getItemDiscount()).isNull();
        assertThat(itemDeal1.getInstance()).isNotNull();
        if (itemDeal1.getInstance().equals("1")) {
            countInstance1++;
        }
        else if (itemDeal1.getInstance().equals("2")) {
            countInstance2++;
        }

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal2 = item2.getItemDeal();
        assertThat(itemDeal2).isNotNull();
        assertThat(itemDeal2.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal2.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item2.getItemDiscount()).isNull();
        assertThat(itemDeal2.getInstance()).isNotNull();
        if (itemDeal2.getInstance().equals("1")) {
            countInstance1++;
        }
        else if (itemDeal2.getInstance().equals("2")) {
            countInstance2++;
        }


        final ItemInfo item3 = items.get(2);
        assertThat(item3.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item3.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item3.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal3 = item3.getItemDeal();
        assertThat(itemDeal3).isNotNull();
        assertThat(itemDeal3.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal3.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item3.getItemDiscount()).isNull();
        assertThat(itemDeal3.getInstance()).isNotNull();
        if (itemDeal3.getInstance().equals("1")) {
            countInstance1++;
        }
        else if (itemDeal3.getInstance().equals("2")) {
            countInstance2++;
        }

        final ItemInfo item4 = items.get(3);
        assertThat(item4.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item4.getQuantity()).isNotNull().isEqualTo(1L);
        assertThat(item4.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal4 = item4.getItemDeal();
        assertThat(itemDeal4).isNotNull();
        assertThat(itemDeal4.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal4.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item4.getItemDiscount()).isNull();
        assertThat(itemDeal4.getInstance()).isNotNull();
        if (itemDeal4.getInstance().equals("1")) {
            countInstance1++;
        }
        else if (itemDeal4.getInstance().equals("2")) {
            countInstance2++;
        }

        assertThat(countInstance1).isEqualTo(2);
        assertThat(countInstance2).isEqualTo(2);
    }

    /**
     * @return created OrderModel
     */
    private OrderModel populateDataForSameDeal() {
        final OrderModel order = createOrderModel(2L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 1L);

        final PromotionResultModel promoResult2 = new PromotionResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(2, 0, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry4 = populatePromotionalResults(2, 1, 1L);
        promoResult2.setConsumedEntries(consumedEntries1);

        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel);
        promoResult2.setPromotion(promotionModel);
        promoResult1.setCertainty(Float.valueOf(1.0f));
        promoResult2.setCertainty(Float.valueOf(1.0f));
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult2);
        consumedEntry4.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries.add(consumedEntry2);
        consumedEntries1.add(consumedEntry3);
        consumedEntries1.add(consumedEntry4);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        order.setAllPromotionResults(promoResults);
        return order;
    }

    @Test
    public void testMultipleDeals() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel order = createOrderModel(2L, 2L, 0L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 2L);

        final PromotionResultModel promoResult2 = new PromotionResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1, 2L);
        promoResult2.setConsumedEntries(consumedEntries1);

        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE);
        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE + 1);
        promoResult1.setPromotion(promotionModel);
        promoResult2.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry3.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry3);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        order.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(order, null,
                deals, 0d);
        assertThat(results).isNotNull();
        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(2);
        final DealDetails details = dealDetails.get("1" + ":" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(1000);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");
        final DealDetails details2 = dealDetails.get("1" + ":" + PROMOTION_CODE + 1);
        assertThat(details2.getCode()).isNotEmpty().contains(PROMOTION_CODE + 1);
        assertThat(details2.getMarkdown()).isNotNull().isEqualTo(1000);
        assertThat(details2.getType()).isNotEmpty().contains("6");
        assertThat(details2.getInstance()).isNotEmpty().contains("1");


        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(2);
        final ItemInfo item1 = items.get(0);
        assertThat(item1.getId()).isNotEmpty().contains(PRODUCT_P12345);
        assertThat(item1.getQuantity()).isNotNull().isEqualTo(2L);
        assertThat(item1.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal1 = item1.getItemDeal();
        assertThat(itemDeal1).isNotNull();
        assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item1.getItemDiscount()).isNull();
        assertThat(itemDeal1.getInstance()).isNotNull().contains("1");

        final ItemInfo item2 = items.get(1);
        assertThat(item2.getId()).isNotEmpty().contains(PRODUCT_P12346);
        assertThat(item2.getQuantity()).isNotNull().isEqualTo(2L);
        assertThat(item2.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
        final ItemDeal itemDeal2 = item2.getItemDeal();
        assertThat(itemDeal2).isNotNull();
        assertThat(itemDeal2.getType()).isNotEmpty().contains(DEAL_REWARD);
        assertThat(itemDeal2.getMarkdown()).isNotNull().isEqualTo("500");
        assertThat(item2.getItemDiscount()).isNull();
        assertThat(itemDeal2.getInstance()).isNotNull().contains("1");
    }

    /**
     * Of the 3 products 2 are consumed by the deal and 1 is not
     */
    @Test
    public void testGet3Products2ConsumedByDeal1NotConsumed() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);

        final PromotionResultModel promoResult2 = new PromotionResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 1L);
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE + 1);
        promoResult2.setPromotion(promotionModel);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult2);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        orderModel.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(orderModel,
                null, deals, 0d);
        assertThat(results).isNotNull();

        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(2);

        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(500);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");

        final DealDetails details2 = dealDetails.get("1:" + PROMOTION_CODE + 1);
        assertThat(details2.getCode()).isNotEmpty().contains(PROMOTION_CODE + 1);
        assertThat(details2.getMarkdown()).isNotNull().isEqualTo(500);
        assertThat(details2.getType()).isNotEmpty().contains("6");
        assertThat(details2.getInstance()).isNotEmpty().contains("1");


        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(3);

        for (final ItemInfo item : items) {
            assertThat(item.getId()).isNotEmpty();
            assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
            if (item.getId().equals(PRODUCT_P12347)) {
                final ItemDeal itemDeal = item.getItemDeal();
                assertThat(itemDeal).isNull();
                assertThat(item.getItemDiscount()).isNull();
            }
            else {
                assertThat(item.getId()).isNotEmpty().contains("P1234");
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
                assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
                final ItemDeal itemDeal1 = item.getItemDeal();
                assertThat(itemDeal1).isNotNull();
                assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
                assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
                assertThat(item.getItemDiscount()).isNull();
            }
        }



    }

    /**
     * Of the 3 products 2 are consumed by the deal and 1 is not
     */
    @Test
    public void testGet3Products2ConsumedByDeal1WithCertaintyLT1() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 0L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);

        final PromotionResultModel promoResult2 = new PromotionResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 1L);
        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE + 1);
        promoResult2.setPromotion(promotionModel);

        final PromotionResultModel promoResult3 = new PromotionResultModel();
        promoResult3.setCertainty(Float.valueOf(0.333f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries3 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 1, 1L);
        promoResult3.setPromotion(promotionModel);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult2);
        consumedEntry3.setPromotionResult(promoResult3);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry2);
        consumedEntries3.add(consumedEntry3);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResult3.setConsumedEntries(consumedEntries3);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        promoResults.add(promoResult3);
        orderModel.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(orderModel,
                null, deals, 0d);
        assertThat(results).isNotNull();

        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();
        assertThat(dealDetails).isNotEmpty().hasSize(2);

        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(500);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");

        final DealDetails details2 = dealDetails.get("1:" + PROMOTION_CODE + 1);
        assertThat(details2.getCode()).isNotEmpty().contains(PROMOTION_CODE + 1);
        assertThat(details2.getMarkdown()).isNotNull().isEqualTo(500);
        assertThat(details2.getType()).isNotEmpty().contains("6");
        assertThat(details2.getInstance()).isNotEmpty().contains("1");


        final List<ItemInfo> items = results.getItems();
        assertThat(items).isNotNull().isNotEmpty().hasSize(3);

        for (final ItemInfo item : items) {
            assertThat(item.getId()).isNotEmpty();
            assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
            assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
            if (item.getId().equals(PRODUCT_P12347)) {
                final ItemDeal itemDeal = item.getItemDeal();
                assertThat(itemDeal).isNull();
                assertThat(item.getItemDiscount()).isNull();
            }
            else {
                assertThat(item.getId()).isNotEmpty().contains("P1234");
                assertThat(item.getQuantity()).isNotNull().isEqualTo(1L);
                assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(new BigDecimal(10.0));
                final ItemDeal itemDeal1 = item.getItemDeal();
                assertThat(itemDeal1).isNotNull();
                assertThat(itemDeal1.getType()).isNotEmpty().contains(DEAL_REWARD);
                assertThat(itemDeal1.getMarkdown()).isNotNull().isEqualTo("500");
                assertThat(item.getItemDiscount()).isNull();
            }
        }



    }

    /**
     * of the 4 products 2 are consumed by seperate deals and 2 by TMD
     */
    @Test
    public void testGet4Products2ConsumedByDealN2ByTMD() {
        final TMDiscountPromotionModel tmdDiscountPromotionModel = Mockito.mock(TMDiscountPromotionModel.class);
        given(tmdDiscountPromotionModel.getPercentageDiscount()).willReturn(Double.valueOf(5d));
        final OrderModel orderModel = createOrderModel(1L, 1L, 1L, 1L);
        final Set<PromotionResultModel> promoResults = new HashSet<>();
        final PromotionResultModel promoResult1 = new PromotionResultModel();
        promoResult1.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry1 = populatePromotionalResults(1, 0, 1L);

        final PromotionResultModel promoResult2 = new PromotionResultModel();
        promoResult2.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries1 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry2 = populatePromotionalResults(1, 1, 1L);

        final PromotionResultModel promoResult3 = new PromotionResultModel();
        promoResult3.setCertainty(Float.valueOf(1.0f));
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries2 = new ArrayList<>();
        final TargetPromotionOrderEntryConsumedModel consumedEntry3 = populatePromotionalResults(1, 2, 1L);
        final TargetPromotionOrderEntryConsumedModel consumedEntry4 = populatePromotionalResults(1, 3, 1L);
        consumedEntries2.add(consumedEntry3);
        consumedEntries2.add(consumedEntry4);
        promoResult3.setConsumedEntries(consumedEntries2);
        promoResult3.setPromotion(tmdDiscountPromotionModel);


        final ValueBundleDealModel promotionModel = new ValueBundleDealModel();
        promotionModel.setCode(PROMOTION_CODE + 1);
        promoResult2.setPromotion(promotionModel);

        final ValueBundleDealModel promotionModel1 = new ValueBundleDealModel();
        promotionModel1.setCode(PROMOTION_CODE);
        promoResult1.setPromotion(promotionModel1);
        consumedEntry1.setPromotionResult(promoResult1);
        consumedEntry2.setPromotionResult(promoResult2);
        consumedEntry3.setPromotionResult(promoResult3);
        consumedEntry4.setPromotionResult(promoResult3);
        consumedEntries.add(consumedEntry1);
        consumedEntries1.add(consumedEntry2);
        promoResult1.setConsumedEntries(consumedEntries);
        promoResult2.setConsumedEntries(consumedEntries1);
        promoResults.add(promoResult1);
        promoResults.add(promoResult2);
        promoResults.add(promoResult3);
        orderModel.setAllPromotionResults(promoResults);

        final Map<String, String> deals = new HashMap<String, String>();
        deals.put("ValueBundleDealModel", "6");

        final DealResults results = tlogConverter.getDetailsFromOrderEntriesNPromotions(orderModel,
                promotionOrderResults, deals, 0.5d);
        assertThat(results).isNotNull();

        final Map<String, DealDetails> dealDetails = results.getDealDetailsForInstance();

        assertThat(dealDetails).isNotEmpty().hasSize(2);

        final DealDetails details = dealDetails.get("1:" + PROMOTION_CODE);
        assertThat(details.getCode()).isNotEmpty().contains(PROMOTION_CODE);
        assertThat(details.getMarkdown()).isNotNull().isEqualTo(500);
        assertThat(details.getType()).isNotEmpty().contains("6");
        assertThat(details.getInstance()).isNotEmpty().contains("1");

        final DealDetails details2 = dealDetails.get("1:" + PROMOTION_CODE + 1);
        assertThat(details2.getCode()).isNotEmpty().contains(PROMOTION_CODE + 1);
        assertThat(details2.getMarkdown()).isNotNull().isEqualTo(500);
        assertThat(details2.getType()).isNotEmpty().contains("6");
        assertThat(details2.getInstance()).isNotEmpty().contains("1");

    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForPinPad() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = Mockito
                .mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = Mockito.mock(PaymentTransactionModel.class);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("pinpad");
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(10.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        final Date now = new Date();
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        final PinPadPaymentInfoModel pinPadInfo = new PinPadPaymentInfoModel();
        pinPadInfo.setMaskedCardNumber("512345xxxxxxx346");
        pinPadInfo.setRrn("123456");
        given(paymentTransactionModel.getInfo()).willReturn(pinPadInfo);
        final TenderInfo tenderInfo = tlogConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);
        Assert.assertNotNull(tenderInfo);
        assertThat(tenderInfo.getType()).isEqualTo(TenderTypeEnum.EFT);
        assertThat(tenderInfo.getAmount()).isEqualTo(BigDecimal.valueOf(10.99d));
        assertThat(tenderInfo.getCardNumber()).isEqualTo("5123450000000346");
        assertThat(tenderInfo.getCreditCardRRN()).isEqualTo("123456");
        assertThat(tenderInfo.getProcessedTime()).isEqualTo(now);
        assertThat(tenderInfo.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTransactionForModificationRecordEntryWithRefundAmountRemaining() {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderModificationRecordModel orderModificationRecordModel = mock(OrderModificationRecordModel.class);

        final OrderCancelRecordEntryModel orderCancelRecordEntry = mock(OrderCancelRecordEntryModel.class);

        final PaymentTransactionModel ptm = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel refund = mock(PaymentTransactionEntryModel.class);
        final CreditCardPaymentInfoModel ccard = mock(CreditCardPaymentInfoModel.class);

        final PaymentInfoModel mockPaymentInfo = mock(PaymentInfoModel.class);

        given(ccard.getNumber()).willReturn("");

        given(refund.getType()).willReturn(PaymentTransactionType.REFUND_STANDALONE);
        given(refund.getTime()).willReturn(new Date());
        given(refund.getCreationtime()).willReturn(new Date());
        given(refund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(refund.getReceiptNo()).willReturn("");
        given(refund.getAmount()).willReturn(BigDecimal.valueOf(21.00d));
        given(refund.getPaymentTransaction()).willReturn(ptm);
        given(refund.getIpgPaymentInfo()).willReturn(mockPaymentInfo);

        given(ptm.getEntries()).willReturn(ImmutableList.of(refund));
        given(ptm.getPaymentProvider()).willReturn("ipg");
        given(ptm.getInfo()).willReturn(ccard);

        given(orderModel.getPaymentTransactions()).willReturn(ImmutableList.of(ptm));

        given(orderCancelRecordEntry.getModificationRecord()).willReturn(orderModificationRecordModel);
        given(orderModificationRecordModel.getOrder()).willReturn(orderModel);
        given(orderCancelRecordEntry.getRefundedShippingAmount()).willReturn(Double.valueOf(10.0));
        given(orderCancelRecordEntry.getRefundedAmount()).willReturn(Double.valueOf(36.0d));
        given(orderCancelRecordEntry.getOrderEntriesModificationEntries()).willReturn(Collections.EMPTY_LIST);

        final Transaction result = tlogConverter.getTransactionForModificationRecordEntry(orderCancelRecordEntry,
                null, ImmutableList.of(refund), BigDecimal.valueOf(15.0d));

        assertThat(result.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(-10.0));

        assertThat(result.getTender()).hasSize(2);
        assertThat(result.getTender()).onProperty("type").containsExactly(TenderTypeEnum.EFT, TenderTypeEnum.CASH);
        assertThat(result.getTender()).onProperty("amount").containsExactly(BigDecimal.valueOf(-21.0d),
                BigDecimal.valueOf(-15.0d));
    }

    /**
     * 
     */
    @Test
    public void testAddTransactionTypeForPreOrder() {
        final Transaction transaction = new Transaction();
        tlogConverter.addTransactionType(purchaseOptionConfig, transaction, Boolean.TRUE);
        assertThat(transaction.getLaybyType()).isEqualTo(TgtsaleConstants.TLOG.PRE_ORDER_LAYBY_TYPE);
        assertThat(transaction.getType()).isEqualTo(TransactionTypeEnum.LAYBY_CREATE);
        assertThat(transaction.getLaybyVersion()).isEqualTo(TgtsaleConstants.TLOG.PRE_ORDER_LAYBY_VERSION);
    }


    /**
     * 
     */
    @Test
    public void testAddTransactionTypeForLayby() {
        given(purchaseOptionConfig.getAllowPaymentDues()).willReturn(Boolean.TRUE);
        given(purchaseOptionConfig.getPosCode()).willReturn("6");
        final Transaction transaction = new Transaction();
        tlogConverter.addTransactionType(purchaseOptionConfig, transaction, Boolean.FALSE);
        assertThat(transaction.getLaybyType()).isEqualTo("6");
        assertThat(transaction.getType()).isEqualTo(TransactionTypeEnum.LAYBY_CREATE);
        assertThat(transaction.getLaybyVersion()).isNull();
    }

    /**
     * 
     */
    @Test
    public void testAddTransactionTypeForNormalPurchase() {
        given(purchaseOptionConfig.getAllowPaymentDues()).willReturn(Boolean.FALSE);
        given(purchaseOptionConfig.getPosCode()).willReturn("6");
        final Transaction transaction = new Transaction();
        tlogConverter.addTransactionType(purchaseOptionConfig, transaction, Boolean.FALSE);
        assertThat(transaction.getLaybyType()).isEqualTo("6");
        assertThat(transaction.getType()).isEqualTo(TransactionTypeEnum.SALE);
        assertThat(transaction.getLaybyVersion()).isNull();
    }


    @Test
    public void testAddPaymentAndCustomerInfoForPreOrder() {
        final OrderModel mockOrder = mockOrderModel();
        final Transaction transaction = new Transaction();
        tlogConverter.addPaymentAndCustomerInfoForPreOrder(mockOrder, transaction, BigDecimal.TEN);
        assertThat(transaction.getPayment()).isNotNull();
        assertThat(transaction.getPayment().getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(transaction.getLaybyFee()).isNotNull();
        assertThat(transaction.getLaybyFee()).isNotNull();
        assertThat(transaction.getLaybyFee().getAmount()).isEqualTo(BigDecimal.ZERO);
        verifyCustomerInfo(transaction.getCustomerInfo());
    }


    @Test
    public void testAddPaymentAndCustomerInfoForPreOrderWithoutBillingAddress() {
        final OrderModel mockOrder = mockOrderModel();
        given(mockOrder.getPaymentAddress()).willReturn(null);
        final AddressModel deliveryAddress = populateAddress();
        given(mockOrder.getDeliveryAddress()).willReturn(deliveryAddress);
        final Transaction transaction = new Transaction();
        tlogConverter.addPaymentAndCustomerInfoForPreOrder(mockOrder, transaction, BigDecimal.TEN);
        assertThat(transaction.getPayment()).isNotNull();
        assertThat(transaction.getPayment().getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(transaction.getLaybyFee()).isNotNull();
        assertThat(transaction.getLaybyFee()).isNotNull();
        assertThat(transaction.getLaybyFee().getAmount()).isEqualTo(BigDecimal.ZERO);
        verifyCustomerInfo(transaction.getCustomerInfo());
        verifyAddress(transaction.getCustomerInfo().getAddress().get(0), AddressTypeEnum.BILLING, false);
        verifyAddress(transaction.getCustomerInfo().getAddress().get(1), AddressTypeEnum.DELIVERY, false);
    }

    @Test
    public void testAddLaybyInformationWithoutCnc() {
        final OrderModel mockOrder = mockOrderModel();
        final Transaction transaction = new Transaction();
        final PaymentDueData paymentDue = new PaymentDueData();
        paymentDue.setAmount(100);
        paymentDue.setDueDate(DUE_DATE);
        given(mockOrder.getLayByFee()).willReturn(Double.valueOf(100));
        given(mockOrder.getCncStoreNumber()).willReturn(null);
        given(targetLaybyPaymentDueService.getFinalPaymentDue(mockOrder)).willReturn(paymentDue);
        tlogConverter.addLaybyInformationDetails(mockOrder, transaction, BigDecimal.TEN);
        assertThat(transaction.getLaybyFee().getAmount()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(transaction.getPayment()).isNotNull();
        assertThat(transaction.getPayment().getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(transaction.getDueDate()).isEqualTo(DUE_DATE);
        verifyCustomerInfo(transaction.getCustomerInfo());
    }

    @Test
    public void testAddLaybyInformationWithCnc() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final OrderModel mockOrder = mockOrderModel();
        final Transaction transaction = new Transaction();
        final PaymentDueData paymentDue = new PaymentDueData();
        final AddressModel deliveryAddress = populateAddress();
        paymentDue.setAmount(100);
        paymentDue.setDueDate(DUE_DATE);
        given(mockOrder.getLayByFee()).willReturn(Double.valueOf(100));
        given(mockOrder.getDeliveryAddress()).willReturn(deliveryAddress);
        given(mockOrder.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        given(targetLaybyPaymentDueService.getFinalPaymentDue(mockOrder)).willReturn(paymentDue);
        populatePOS();
        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(5001))).willReturn(pos);
        tlogConverter.addLaybyInformationDetails(mockOrder, transaction, BigDecimal.TEN);
        assertThat(transaction.getLaybyFee().getAmount()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(transaction.getPayment()).isNotNull();
        assertThat(transaction.getPayment().getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(transaction.getDueDate()).isEqualTo(DUE_DATE);
        verifyCustomerInfo(transaction.getCustomerInfo());
        verifyAddress(transaction.getCustomerInfo().getAddress().get(0), AddressTypeEnum.BILLING, false);
        verifyAddress(transaction.getCustomerInfo().getAddress().get(1), AddressTypeEnum.DELIVERY, true);
    }

    @Test
    public void testGetTransactionForPreOrderCancelRefundFollowOn() {
        final OrderModel mockOrder = mockOrderModel();
        final List<PaymentTransactionEntryModel> paymentEntries = createRefundPaymentEntry(false);
        final Transaction transaction = tlogConverter.getTransactionForPreOrderCancel(mockOrder, paymentEntries);
        assertThat(transaction).as("Transaction").isNotNull();
        assertThat(transaction.getType()).as("Transaction Type").isEqualTo(TransactionTypeEnum.LAYBY_CANCEL);
        assertThat(transaction.getOrder()).as("Order Info").isNotNull();
        assertThat(transaction.getOrder().getId()).as("Order Number").isEqualTo(ORDER_NUMBER);
        assertThat(transaction.getTender()).as("Tender Info").isNotNull();
        assertThat(transaction.getTender().size()).isEqualTo(1);
        assertThat(transaction.getTender().get(0).getApproved()).as("Tender Approved").isEqualTo("Y");
        assertThat(transaction.getTender().get(0).getType()).as("Tender Type").isEqualTo(TenderTypeEnum.EFT);
        assertThat(transaction.getTender().get(0).getAmount()).as("TenderAmount").isEqualTo(
                BigDecimal.valueOf(21.0).negate());
        assertThat(transaction.getTender().get(0).getCreditCardRRN()).as("Tender Receipt number").isEqualTo(
                RECEIPT_NUMBER);
        assertThat(transaction.getTender().get(0).getCardNumber()).as("Tender Card Number").isEqualTo(
                CARD_NUMBER.replace("x", "0")
                        .replace("*", "0"));
    }

    @Test
    public void testGetTransactionForPreOrderCancelRefundStandAlone() {
        final OrderModel mockOrder = mockOrderModel();
        final List<PaymentTransactionEntryModel> paymentEntries = createRefundPaymentEntry(true);
        final Transaction transaction = tlogConverter.getTransactionForPreOrderCancel(mockOrder, paymentEntries);
        assertThat(transaction).as("Transaction").isNotNull();
        assertThat(transaction.getType()).as("Transaction Type").isEqualTo(TransactionTypeEnum.LAYBY_CANCEL);
        assertThat(transaction.getOrder()).as("Order Info").isNotNull();
        assertThat(transaction.getOrder().getId()).as("Order Number").isEqualTo(ORDER_NUMBER);
        assertThat(transaction.getTender()).as("Tender Info").isNotNull();
        assertThat(transaction.getTender().size()).isEqualTo(1);
        assertThat(transaction.getTender().get(0).getApproved()).as("Tender Approved").isEqualTo("Y");
        assertThat(transaction.getTender().get(0).getType()).as("Tender Type").isEqualTo(TenderTypeEnum.CASH);
        assertThat(transaction.getTender().get(0).getCreditCardRRN()).as("Tender Receipt number").isNull();
        assertThat(transaction.getTender().get(0).getCardNumber()).as("Tender Card Number").isNull();
        assertThat(transaction.getTender().get(0).getAmount()).as("TenderAmount").isEqualTo(
                BigDecimal.valueOf(21.0).negate());

    }

    @Test
    public void testGetTransactionForPreOrderFulfilment() {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionModel transactionModel1 = mock(PaymentTransactionModel.class);
        final PaymentTransactionModel transactionModel2 = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry1 = mock(PaymentTransactionEntryModel.class);

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(transactionModel1);
        paymentTransactions.add(transactionModel2);

        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        entries.add(entry1);

        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);
        willReturn(Boolean.TRUE).given(transactionModel1).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel1).getIsCaptureSuccessful();
        willReturn(Boolean.FALSE).given(transactionModel2).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel2).getIsCaptureSuccessful();
        given(transactionModel2.getEntries()).willReturn(entries);
        given(entry1.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(entry1.getType()).willReturn(PaymentTransactionType.CAPTURE);

        final Date creationDate = mock(Date.class);
        final TlogPaymentEntryConverter tlogPaymentEntryConverter1 = mock(TlogPaymentEntryConverter.class);
        final TenderInfo tenderInfo = mock(TenderInfo.class);

        given(orderModel.getCode()).willReturn("O1243");
        given(process.getCreationtime()).willReturn(creationDate);
        given(tlogPaymentEntryConverter1.getTenderInfoFromPaymentEntry(entry1, false)).willReturn(tenderInfo);
        willReturn(BigDecimal.TEN).given(entry1).getAmount();

        tlogConverter.setTlogPaymentEntryConverter(tlogPaymentEntryConverter1);
        final Transaction transaction = tlogConverter.getTransactionForPreOrderFulfilment(process, orderModel);


        assertThat(transaction).isNotNull();
        assertThat(transaction.getOrder()).isNotNull();
        assertThat(transaction.getOrder().getId()).isEqualTo("O1243");
        assertThat(transaction.getTimestamp()).isEqualTo(creationDate);
        assertThat(transaction.getType()).isEqualTo(TransactionTypeEnum.LAYBY_PAYMENT);
        assertThat(transaction.getTender().contains(tenderInfo)).isTrue();
        assertThat(transaction.getPayment().getAmount()).isEqualTo(BigDecimal.TEN);

    }

    @Test
    public void testGetTransactionForPreOrderFulfilmentWithNullPaymentEntry() {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final OrderModel orderModel = mock(OrderModel.class);

        final Date creationDate = mock(Date.class);
        final TlogPaymentEntryConverter tlogPaymentEntryConverter1 = mock(TlogPaymentEntryConverter.class);

        given(orderModel.getCode()).willReturn("O1243");
        given(process.getCreationtime()).willReturn(creationDate);

        tlogConverter.setTlogPaymentEntryConverter(tlogPaymentEntryConverter1);
        final Transaction transaction = tlogConverter.getTransactionForPreOrderFulfilment(process, orderModel);


        assertThat(transaction).isNotNull();
        assertThat(transaction.getOrder()).isNotNull();
        assertThat(transaction.getOrder().getId()).isEqualTo("O1243");
        assertThat(transaction.getTimestamp()).isEqualTo(creationDate);
        assertThat(transaction.getType()).isEqualTo(TransactionTypeEnum.LAYBY_PAYMENT);
        assertThat(transaction.getTender().contains(null)).isTrue();
        assertThat(transaction.getPayment().getAmount()).isEqualTo(BigDecimal.ZERO);

    }

    private void verifyCustomerInfo(final CustomerInfo customerInfo) {
        assertThat(customerInfo).isNotNull();
        assertThat(customerInfo.getFirstName()).isEqualTo(CUSTOMER_FIRST_NAME);
        assertThat(customerInfo.getLastName()).isEqualTo(CUSTOMER_LAST_NAME);

    }

    private void verifyAddress(final AddressInfo address, final AddressTypeEnum type, final boolean pos) {
        assertThat(address).isNotNull();
        if (pos) {
            assertThat(address.getAddressLine1()).isEqualTo("STORE" + "-" + GEELONG_STORE_NAME);
        }
        else {
            assertThat(address.getAddressLine1()).isEqualTo(ADDRESS_LINE_1);
        }
        assertThat(address.getCity()).isEqualTo(CITY);
        assertThat(address.getCountry()).isEqualTo(COUNTRY);
        assertThat(address.getPostCode()).isEqualTo(POSTAL_CODE);
        assertThat(address.getType()).isEqualTo(type);
    }

    private AddressModel populateAddress() {
        given(address.getLine1()).willReturn("12 Thompson road");
        given(address.getLine1()).willReturn(ADDRESS_LINE_1);
        given(address.getTown()).willReturn(CITY);
        given(country.getName()).willReturn(COUNTRY);
        given(address.getCountry()).willReturn(country);
        given(address.getPostalcode()).willReturn(POSTAL_CODE);
        return address;
    }

    private TargetPointOfServiceModel populatePOS() {
        given(pos.getStoreNumber()).willReturn(Integer.valueOf(5001));
        given(pos.getType()).willReturn(PointOfServiceTypeEnum.STORE);
        given(pos.getName()).willReturn(GEELONG_STORE_NAME);
        return pos;
    }

    private OrderModel mockOrderModel() {
        final OrderModel orderModel = mock(OrderModel.class);
        final AddressModel address = populateAddress();
        final TargetCustomerModel customerModel = mock(TargetCustomerModel.class);
        given(orderModel.getCode()).willReturn(ORDER_NUMBER);
        given(customerModel.getFirstname()).willReturn(CUSTOMER_FIRST_NAME);
        given(orderModel.getPaymentAddress()).willReturn(address);
        given(customerModel.getLastname()).willReturn(CUSTOMER_LAST_NAME);
        given(orderModel.getUser()).willReturn(customerModel);

        return orderModel;
    }

    private OrderModel createOrderModel(final long quantity1, final long quantity2, final long quantity3,
            final long quantity4) {
        final OrderModel order = new OrderModel();
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final OrderEntryModel entry1 = new OrderEntryModel();
        final ProductModel productModel = new ProductModel();
        productModel.setCode(PRODUCT_P12345);
        entry1.setEntryNumber(Integer.valueOf(0));
        entry1.setQuantity(Long.valueOf(quantity1));
        entry1.setBasePrice(Double.valueOf(10d));
        entry1.setProduct(productModel);
        entry1.setOrder(order);
        entry1.setTotalPrice(Double.valueOf(quantity1 * 10));
        entries.add(entry1);
        if (quantity2 > 0) {
            final OrderEntryModel entry2 = new OrderEntryModel();
            final ProductModel productModel1 = new ProductModel();
            productModel1.setCode(PRODUCT_P12346);
            entry2.setEntryNumber(Integer.valueOf(1));
            entry2.setQuantity(Long.valueOf(quantity2));
            entry2.setBasePrice(Double.valueOf(10d));
            entry2.setProduct(productModel1);
            entry2.setOrder(order);
            entry2.setTotalPrice(Double.valueOf(quantity2 * 10));
            entries.add(entry2);
        }
        if (quantity3 > 0) {
            final OrderEntryModel entry3 = new OrderEntryModel();
            final ProductModel productModel2 = new ProductModel();
            productModel2.setCode(PRODUCT_P12347);
            entry3.setEntryNumber(Integer.valueOf(2));
            entry3.setQuantity(Long.valueOf(quantity2));
            entry3.setBasePrice(Double.valueOf(10d));
            entry3.setProduct(productModel2);
            entry3.setOrder(order);
            entry3.setTotalPrice(Double.valueOf(quantity2 * 10));
            entries.add(entry3);
        }

        if (quantity4 > 0) {
            if (quantity3 > 0) {
                final OrderEntryModel entry4 = new OrderEntryModel();
                final ProductModel productModel3 = new ProductModel();
                productModel3.setCode(PRODUCT_P12348);
                entry4.setEntryNumber(Integer.valueOf(3));
                entry4.setQuantity(Long.valueOf(quantity2));
                entry4.setBasePrice(Double.valueOf(10d));
                entry4.setProduct(productModel3);
                entry4.setOrder(order);
                entry4.setTotalPrice(Double.valueOf(quantity2 * 10));
                entries.add(entry4);
            }
        }

        order.setEntries(entries);

        return order;

    }

    private TargetPromotionOrderEntryConsumedModel populatePromotionalResults(final int instance,
            final int entryNum, final long quantity) {
        final TargetPromotionOrderEntryConsumedModel entryConsumed = new TargetPromotionOrderEntryConsumedModel();
        entryConsumed.setAdjustedUnitPrice(Double.valueOf(5.0d));
        entryConsumed.setDealItemType(DealItemTypeEnum.REWARD);
        entryConsumed.setInstance(Integer.valueOf(instance));
        entryConsumed.setQuantity(Long.valueOf(quantity));
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entry.setEntryNumber(Integer.valueOf(entryNum));
        entryConsumed.setOrderEntry(entry);
        return entryConsumed;
    }

    private TargetPromotionOrderEntryConsumedModel populatePromotionalResults(final int instance,
            final int entryNum, final long quantity, final double adjustedPrice) {
        final TargetPromotionOrderEntryConsumedModel entryConsumed = new TargetPromotionOrderEntryConsumedModel();
        entryConsumed.setAdjustedUnitPrice(Double.valueOf(adjustedPrice));
        entryConsumed.setDealItemType(DealItemTypeEnum.REWARD);
        entryConsumed.setInstance(Integer.valueOf(instance));
        entryConsumed.setQuantity(Long.valueOf(quantity));
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entry.setEntryNumber(Integer.valueOf(entryNum));
        entryConsumed.setOrderEntry(entry);
        return entryConsumed;
    }

    private List<PaymentTransactionEntryModel> createRefundPaymentEntry(final boolean isRefundStandAlone) {

        final PaymentTransactionModel ptm = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel refund = mock(PaymentTransactionEntryModel.class);
        final IpgCreditCardPaymentInfoModel ccard = mock(IpgCreditCardPaymentInfoModel.class);
        final List<PaymentTransactionEntryModel> transactionEntries = new ArrayList<PaymentTransactionEntryModel>();

        if (isRefundStandAlone) {
            given(refund.getType()).willReturn(PaymentTransactionType.REFUND_STANDALONE);

        }
        else {
            given(refund.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
            given(refund.getIpgPaymentInfo()).willReturn(ccard);

        }
        given(refund.getTime()).willReturn(new Date());
        given(refund.getCreationtime()).willReturn(new Date());
        given(refund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(refund.getReceiptNo()).willReturn(RECEIPT_NUMBER);
        given(refund.getAmount()).willReturn(BigDecimal.valueOf(21.00d));
        given(refund.getPaymentTransaction()).willReturn(ptm);
        given(ccard.getNumber()).willReturn(CARD_NUMBER);
        given(ptm.getEntries()).willReturn(ImmutableList.of(refund));
        given(ptm.getPaymentProvider()).willReturn("ipg");
        given(ptm.getInfo()).willReturn(ccard);
        transactionEntries.add(refund);
        return transactionEntries;

    }
}
