/**
 *
 */
package au.com.target.tgtsale.stock.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.stock.TargetStockLevelStatusStrategy;
import au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtsale.stock.client.TargetStockUpdateClient;
import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * @author smudumba
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreStockServiceImplTest {

    private static final String PRODUCT_ONE = "1000";

    private static final String PRODUCT_TWO = "1001";

    private static final Integer STORE_NUMBER = Integer.valueOf(1234);

    private static final String ORDER_CODE = "11001100";

    @Mock
    private StockService mockStockService;

    @Mock
    private TargetWarehouseService mockTargetWarehouseService;

    @Mock
    private TargetPointOfServiceService mockTargetPointOfServiceService;

    @Mock
    private TargetProductService mockTargetProductService;

    @Mock
    private CatalogVersionService mockCatalogVersionService;

    @Mock
    private TargetStockUpdateClient mockTargetStockUpdateClient;

    @Mock
    private AbstractTargetVariantProductModel variant;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private StockVisibilityClient stockVisibilityClient;

    @Mock
    private ProductModel productModel;

    @Mock
    private TargetStockLevelStatusStrategy storeStockLevelStatusStrategy;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final TargetStoreStockServiceImpl targetStoreStockServiceImpl = new TargetStoreStockServiceImpl();


    /**
     * @throws java.lang.Exception
     */

    @Before
    public void setUp() throws Exception {

        //setup store stock responses
        final List<StockUpdateStoreResponseDto> stockUpdateStoreResponseDtos = new ArrayList<>();
        final StockUpdateStoreResponseDto stockUpdateStoreResponseDto = new StockUpdateStoreResponseDto();
        stockUpdateStoreResponseDto.setStoreNumber(STORE_NUMBER.toString());
        final List<StockUpdateProductResponseDto> stockUpdateProductResponseDtos = new ArrayList<>();
        StockUpdateProductResponseDto stockUpdateProductResponseDto = new StockUpdateProductResponseDto();
        stockUpdateProductResponseDto.setItemcode(PRODUCT_ONE);
        stockUpdateProductResponseDto.setSoh("30");
        stockUpdateProductResponseDtos.add(stockUpdateProductResponseDto);
        stockUpdateProductResponseDto = new StockUpdateProductResponseDto();
        stockUpdateProductResponseDto.setItemcode(PRODUCT_TWO);
        stockUpdateProductResponseDto.setSoh("20");
        stockUpdateProductResponseDtos.add(stockUpdateProductResponseDto);
        stockUpdateStoreResponseDto.setStockUpdateProductResponseDtos(stockUpdateProductResponseDtos);
        stockUpdateStoreResponseDtos.add(stockUpdateStoreResponseDto);

        final StockUpdateResponseDto stockUpdateResponse = new StockUpdateResponseDto();
        stockUpdateResponse.setStockUpdateStoreResponseDtos(stockUpdateStoreResponseDtos);

        final TargetPointOfServiceModel storeModel = new TargetPointOfServiceModel();
        final AddressModel address = new AddressModel();
        address.setDistrict("Melbourne");
        storeModel.setAddress(address);
        storeModel.setStoreNumber(STORE_NUMBER);
        given(mockTargetStockUpdateClient.getStockLevelsFromPos(Mockito.any(StockUpdateDto.class)))
                .willReturn(stockUpdateResponse);
        given(mockTargetPointOfServiceService.getPOSByStoreNumber(Mockito.any(Integer.class))).willReturn(
                storeModel);
        given(targetProductService.getProductForCode(any(String.class))).willReturn(productModel);

        willReturn(Boolean.FALSE)
                .given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT);
    }

    @Test
    public void testIsOrderEntriesInStockAtStoreWithProductInStock() {
        //Order for store which has stock

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);
        oem = new AbstractOrderEntryModel();
        pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_TWO);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm);
        oems.add(oem);
        final boolean result = targetStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, STORE_NUMBER, ORDER_CODE);
        Assert.assertFalse(result);
    }

    @Test
    public void testGetStockForProducts() {
        final List<AbstractTargetVariantProductModel> products = new ArrayList<>();
        products.add(variant);
        final StockUpdateResponseDto response = targetStoreStockServiceImpl.getStockForProducts(products, STORE_NUMBER);
        Assert.assertNotNull(response);
        final List<StockUpdateProductResponseDto> productStock = response.getStockUpdateStoreResponseDtos().get(0)
                .getStockUpdateProductResponseDtos();
        Assert.assertNotNull(productStock);
        assertThat(productStock.get(0).getItemcode()).isEqualTo(PRODUCT_ONE);
        assertThat(productStock.get(1).getItemcode()).isEqualTo(PRODUCT_TWO);
    }


    @Test
    public void testGetStockForProductsThrowingException() throws TargetIntegrationException {
        final List<AbstractTargetVariantProductModel> products = new ArrayList<>();
        products.add(variant);
        BDDMockito.given(mockTargetStockUpdateClient.getStockLevelsFromPos(Mockito.any(StockUpdateDto.class)))
                .willThrow(new TargetIntegrationException());
        final StockUpdateResponseDto response = targetStoreStockServiceImpl.getStockForProducts(products, STORE_NUMBER);
        Assert.assertNull(response);
    }

    @Test
    public void testIsOrderEntriesInStockAtStoreWithAllProductOutStock() {
        //Order for store which has stock

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(70L));
        oem.setProduct(pm);
        oems.add(oem);
        oem = new AbstractOrderEntryModel();
        pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_TWO);
        oem.setQuantity(Long.valueOf(100L));
        oem.setProduct(pm);
        oems.add(oem);
        final boolean result = targetStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, STORE_NUMBER, ORDER_CODE);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsOrderEntriesInStockAtStoreWithNullOrder() throws TargetIntegrationException {
        //Order for store which has stock

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        final AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(70L));
        oem.setProduct(pm);
        oems.add(oem);
        final boolean result = targetStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, STORE_NUMBER, ORDER_CODE);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsOrderEntriesInStockAtStoreWithNullStore() {
        //Order for store which has stock

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        final AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode(PRODUCT_ONE);
        oem.setQuantity(Long.valueOf(70L));
        oem.setProduct(pm);
        oems.add(oem);
        final boolean result = targetStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, null, ORDER_CODE);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsOrderEntriesInStockAtStoreWithNoProductsInStore() {

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        final AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel pm = new AbstractTargetVariantProductModel();
        pm.setCode("4000");
        oem.setQuantity(Long.valueOf(70L));
        oem.setProduct(pm);
        oems.add(oem);
        final boolean result = targetStoreStockServiceImpl.isOrderEntriesInStockAtStore(oems, STORE_NUMBER, ORDER_CODE);
        Assert.assertFalse(result);
    }

    @Test
    public void testLookupStockForItemsInStoresWithStockVisibilityClient() throws TargetIntegrationException {
        final ImmutableList<String> stores = ImmutableList.of("store1", "store2");
        final ImmutableList<String> itemcodes = ImmutableList.of("productCode");
        targetStoreStockServiceImpl.lookupStockForItemsInStores(
                stores, itemcodes);
        verify(stockVisibilityClient).lookupStockForItemsInStores(stores, itemcodes);
    }

    @Test
    public void testGetStoreStockLevelStatusMapForProductsWithInvalidStoreNumber() {
        doReturn(Boolean.FALSE).when(mockTargetPointOfServiceService).validateStoreNumber(Integer.valueOf(0));
        final Map<String, StockLevelStatus> resultMap = targetStoreStockServiceImpl
                .getStoreStockLevelStatusMapForProducts(Integer.valueOf(0), Arrays.asList("P1000_Red_S"));
        assertThat(resultMap).isEmpty();
    }

    @Test
    public void testGetStoreStockLevelStatusMapForProducts() {
        final Integer storeNumber = Integer.valueOf(20);
        doReturn(Boolean.TRUE).when(mockTargetPointOfServiceService).validateStoreNumber(storeNumber);
        final StockVisibilityItemLookupResponseDto toBeReturned = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto item = new StockVisibilityItemResponseDto();
        item.setCode("P1000_Red_S");
        item.setSoh("10");
        toBeReturned.setItems(Arrays.asList(item));
        final List<String> itemCodes = Arrays.asList("P1000_Red_S");
        given(stockVisibilityClient.lookupStockForItemsInStores(Collections.singletonList("20"),
                itemCodes)).willReturn(toBeReturned);
        final Map<String, StockLevelStatus> resultMap = targetStoreStockServiceImpl
                .getStoreStockLevelStatusMapForProducts(storeNumber, itemCodes);
        assertThat(resultMap).hasSize(1);
    }

}
