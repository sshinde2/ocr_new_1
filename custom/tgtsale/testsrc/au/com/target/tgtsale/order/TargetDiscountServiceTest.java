package au.com.target.tgtsale.order;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtsale.model.TargetTMDPrefixModel;
import au.com.target.tgtsale.order.impl.TargetSaleDiscountServiceImpl;
import au.com.target.tgtsale.storeconfig.TargetStoreConfigDao;
import au.com.target.tgtsale.tmd.TargetTMDHotCardService;


/**
 * 
 */
@UnitTest
public class TargetDiscountServiceTest {

    @Mock
    private TargetStoreConfigDao targetStoreConfigDao;
    @Mock
    private TargetTMDHotCardService targetTMDHotCardService;

    private MyTargetSaleDiscountServiceImpl targetDiscountService;

    class MyTargetSaleDiscountServiceImpl extends TargetSaleDiscountServiceImpl {
        // expose the protected methods so we can test them

        @Override
        public boolean isValidCardType(final String card,
                final TMDiscountProductPromotionModel tmDiscountProductPromotionModel) {
            return super.isValidCardType(card, tmDiscountProductPromotionModel);
        }
    }

    @SuppressWarnings("boxing")
    @Before
    public void prepare() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        MockitoAnnotations.initMocks(this);
        targetDiscountService = new MyTargetSaleDiscountServiceImpl();
        targetDiscountService.setTargetStoreConfigDao(targetStoreConfigDao);
        targetDiscountService.setTargetTMDHotCardService(targetTMDHotCardService);
    }

    private void setIsCardCurrentToReturnTrue() {
        targetDiscountService = new MyTargetSaleDiscountServiceImpl() {

            @Override
            public boolean isValidCardType(final String card,
                    final TMDiscountProductPromotionModel tmDiscountProductPromotionModel) {
                return true;
            }
        };
        targetDiscountService.setTargetStoreConfigDao(targetStoreConfigDao);
        targetDiscountService.setTargetTMDHotCardService(targetTMDHotCardService);
    }

    @Test
    public void testIsValidTMDCardValidCard() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException
    {
        setIsCardCurrentToReturnTrue();
        final String cardnumber = "2771334345257";
        final boolean validTMDCard = targetDiscountService.isValidTMDCard(cardnumber);
        Assert.assertFalse(validTMDCard);
    }

    @Test
    public void testIsValidTMDCardInHotList() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException
    {
        setIsCardCurrentToReturnTrue();
        setCardAsHot(2771334345257L);
        final boolean validTMDCard = targetDiscountService.isValidTMDCard("2771334345257");
        Assert.assertFalse(validTMDCard);
    }

    // Isolate the the suppress boxing warnings to the one method
    @SuppressWarnings("boxing")
    private void setCardAsHot(final long cardnumber) {
        BDDMockito.given(targetTMDHotCardService.exists(cardnumber)).willReturn(Boolean.TRUE);
    }

    @Test
    public void testIsValidTMDCardShortNumber()
    {
        setIsCardCurrentToReturnTrue();
        final String cardnumber = "277133434525";
        Assert.assertFalse(targetDiscountService.isValidTMDCard(cardnumber));
    }

    @Test
    public void testIsValidTMDCardFailChecksum()
    {
        setIsCardCurrentToReturnTrue();
        final String cardnumber = "2771334345258";
        Assert.assertFalse(targetDiscountService.isValidTMDCard(cardnumber));
    }

    @Test
    public void testIsValidTMDCardNonNumeric()
    {
        setIsCardCurrentToReturnTrue();
        final String cardnumber = "277133434S257";
        Assert.assertFalse(targetDiscountService.isValidTMDCard(cardnumber));
    }

    @Test
    public void testIsValidTMDCardUnknownPrefix()
    {
        targetDiscountService = new MyTargetSaleDiscountServiceImpl() {

            @Override
            public boolean isValidCardType(final String card,
                    final TMDiscountProductPromotionModel tmDiscountProductPromotionModel) {
                return false;
            }
        };
        targetDiscountService.setTargetStoreConfigDao(targetStoreConfigDao);
        targetDiscountService.setTargetTMDHotCardService(targetTMDHotCardService);

        final String cardnumber = "2771334345257";
        Assert.assertFalse(targetDiscountService.isValidTMDCard(cardnumber));
    }

    @Test
    public void testisValidCardType() throws Exception {
        final TargetTMDPrefixModel nonTgtModel = new TargetTMDPrefixModel();
        nonTgtModel.setCode("nonTargetPrefix");
        nonTgtModel.setPrefixes("11,22,33");
        final TMDiscountProductPromotionModel nonTgtPromoModel = Mockito.mock(TMDiscountProductPromotionModel.class);
        BDDMockito.given(nonTgtPromoModel.getTgtTmdPrefix()).willReturn(nonTgtModel);

        final TargetTMDPrefixModel tgtModel = new TargetTMDPrefixModel();
        tgtModel.setCode("targetPrefix");
        tgtModel.setPrefixes("44,55");
        final TMDiscountProductPromotionModel tgtPromoModel = Mockito.mock(TMDiscountProductPromotionModel.class);
        BDDMockito.given(tgtPromoModel.getTgtTmdPrefix()).willReturn(tgtModel);

        Assert.assertTrue(targetDiscountService.isValidCardType("11111111111", nonTgtPromoModel));
        Assert.assertTrue(targetDiscountService.isValidCardType("22222222222", nonTgtPromoModel));
        Assert.assertTrue(targetDiscountService.isValidCardType("33333333333", nonTgtPromoModel));
        Assert.assertTrue(targetDiscountService.isValidCardType("44444444444", tgtPromoModel));
        Assert.assertTrue(targetDiscountService.isValidCardType("555555555555", tgtPromoModel));
        Assert.assertFalse(targetDiscountService.isValidCardType("666666666666", null));
        Assert.assertFalse(targetDiscountService.isValidCardType("777777777777", tgtPromoModel));
        Assert.assertFalse(targetDiscountService.isValidCardType("888888888888", null));
        Assert.assertFalse(targetDiscountService.isValidCardType("999999999999", tgtPromoModel));
        Assert.assertFalse(targetDiscountService.isValidCardType("000000000000", nonTgtPromoModel));
    }
}
