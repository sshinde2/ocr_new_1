/**
 * 
 */
package au.com.target.tgtsale.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtsale.exception.IllegalPosModificationException;
import au.com.target.tgtsale.integration.dto.TargetLaybyEntries;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrderEntry;


/**
 * Unit test for {@link PosLaybyUpdaterImpl} checkLaybyOrdersMatch method
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("boxing")
public class TestWhenPosLaybyUpdaterCheckLaybyOrdersMatch {

    @Mock
    private TargetBusinessProcessService businessProcessService;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetCancelService targetCancelService;


    @InjectMocks
    private final PosLaybyUpdaterImpl posLaybyUpdater = new PosLaybyUpdaterImpl() {
        @Override
        public TargetBusinessProcessService getTargetBusinessProcessService() {
            return businessProcessService;
        }
    };


    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetLaybyOrder posLaybyOrder;

    @Mock
    private TargetLaybyOrderEntry targetLaybyOrderEntry;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private AbstractOrderEntryModel anotherHybrisEntry;

    @Mock
    private TargetLaybyOrderEntry anotherPosEntry;

    @Mock
    private ProductModel anotherProduct;


    private final List<AbstractOrderEntryModel> hybrisEntries = new ArrayList<>();
    private final List<TargetLaybyOrderEntry> posEntries = new ArrayList<>();
    private final TargetLaybyEntries posEntriesObj = new TargetLaybyEntries();

    @Before
    public void setup() {

        // By default the businessProcessService will return process
        BDDMockito.given(businessProcessService.createProcess(Mockito.anyString(), Mockito.anyString())).willReturn(
                process);


        BDDMockito.given(order.getCode()).willReturn("order1");

        posEntries.add(targetLaybyOrderEntry);
        posEntriesObj.setEntry(posEntries);
        BDDMockito.given(posLaybyOrder.getEntries()).willReturn(posEntriesObj);

        hybrisEntries.add(abstractOrderEntryModel);
        BDDMockito.given(order.getEntries()).willReturn(hybrisEntries);
    }

    @Test
    public void testGivenBothCancelled() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.TRUE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.CANCELLED);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertTrue(result);
    }

    private void setupMatchingOrders() {

        BDDMockito.given(targetLaybyOrderEntry.getCode()).willReturn("Going to match");
        BDDMockito.given(abstractOrderEntryModel.getProduct()).willReturn(productModel);
        BDDMockito.given(productModel.getCode()).willReturn("Going to match");
        BDDMockito.given(targetLaybyOrderEntry.getQuantity()).willReturn(10L);
        BDDMockito.given(abstractOrderEntryModel.getQuantity()).willReturn(10L);
        BDDMockito.given(targetLaybyOrderEntry.getBasePrice()).willReturn(12.00D);
        BDDMockito.given(abstractOrderEntryModel.getBasePrice()).willReturn(12.00D);

        // Default to order in progress being partially cancelled
        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.FALSE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);

        // Default matching delivery fees
        BDDMockito.given(order.getDeliveryCost()).willReturn(10.0);
        BDDMockito.given(posLaybyOrder.getShippingPrice()).willReturn(10.0);
    }

    private void setupMatchingOrdersMultipleEntries() {

        setupMatchingOrders();

        posEntries.add(anotherPosEntry);
        hybrisEntries.add(anotherHybrisEntry);

        BDDMockito.given(anotherPosEntry.getCode()).willReturn("another product code");
        BDDMockito.given(anotherHybrisEntry.getProduct()).willReturn(anotherProduct);
        BDDMockito.given(anotherProduct.getCode()).willReturn("another product code");
        BDDMockito.given(anotherHybrisEntry.getQuantity()).willReturn(8L);
        BDDMockito.given(anotherPosEntry.getQuantity()).willReturn(8L);
        BDDMockito.given(anotherHybrisEntry.getBasePrice()).willReturn(12.00D);
        BDDMockito.given(anotherPosEntry.getBasePrice()).willReturn(12.00D);
    }

    @Test
    public void testGivenMatchingOrders() throws Exception {

        setupMatchingOrders();
        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertTrue(result);
    }

    @Test
    public void testGivenOrderCancelledByPos() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.TRUE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertFalse(result);
    }

    @Test(expected = IllegalPosModificationException.class)
    public void testGivenOrderUnCancelled() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.FALSE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.CANCELLED);

        posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
    }


    @Test
    public void testGivenDifferentDeliveryFees() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(order.getDeliveryCost()).willReturn(55.0);
        BDDMockito.given(posLaybyOrder.getShippingPrice()).willReturn(10.0);
        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertFalse(result);
    }

    @Test(expected = IllegalPosModificationException.class)
    public void testGivenPosHasNewSku() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(targetLaybyOrderEntry.getCode()).willReturn("Not going to match");

        posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
    }

    @Test
    public void testGivenDeadHybrisEntry() throws Exception {

        setupMatchingOrders();

        hybrisEntries.add(anotherHybrisEntry);

        BDDMockito.given(anotherHybrisEntry.getProduct()).willReturn(anotherProduct);
        BDDMockito.given(anotherHybrisEntry.getQuantity()).willReturn(1L);
        BDDMockito.given(anotherHybrisEntry.getQuantityStatus()).willReturn(OrderEntryStatus.DEAD);
        BDDMockito.given(anotherHybrisEntry.getBasePrice()).willReturn(12.00D);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertTrue(result);
    }

    @Test
    public void testGivenZeroHybrisEntry() throws Exception {

        setupMatchingOrders();

        hybrisEntries.add(anotherHybrisEntry);

        BDDMockito.given(anotherHybrisEntry.getProduct()).willReturn(anotherProduct);
        BDDMockito.given(anotherHybrisEntry.getQuantity()).willReturn(0L);
        BDDMockito.given(anotherHybrisEntry.getBasePrice()).willReturn(12.00D);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertTrue(result);
    }

    @Test
    public void testGivenMultipleMatchingEntries() throws Exception {

        setupMatchingOrdersMultipleEntries();

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertTrue(result);
    }

    @Test
    public void testGivenPosSecondItemQtyChanged() throws Exception {

        setupMatchingOrdersMultipleEntries();

        BDDMockito.given(anotherHybrisEntry.getQuantity()).willReturn(8L);
        BDDMockito.given(anotherPosEntry.getQuantity()).willReturn(7L);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertFalse(result);
    }

    @Test
    public void testGivenPosSecondItemPriceChanged() throws Exception {

        setupMatchingOrdersMultipleEntries();

        BDDMockito.given(anotherHybrisEntry.getBasePrice()).willReturn(12.00D);
        BDDMockito.given(anotherPosEntry.getBasePrice()).willReturn(11.00D);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertFalse(result);
    }

    @Test
    public void testGivenPosSecondItemRemoved() throws Exception {

        setupMatchingOrdersMultipleEntries();

        posEntries.remove(anotherPosEntry);

        final boolean result = posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
        Assert.assertFalse(result);
    }

    @Test(expected = IllegalPosModificationException.class)
    public void testGivenPosSecondItemNotInHybris() throws Exception {

        setupMatchingOrdersMultipleEntries();

        BDDMockito.given(anotherPosEntry.getCode()).willReturn("not the same code");
        BDDMockito.given(anotherProduct.getCode()).willReturn("another product code");

        posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
    }

    @Test(expected = IllegalPosModificationException.class)
    public void testGivenPosThirdItemAdded() throws Exception {

        setupMatchingOrdersMultipleEntries();

        final TargetLaybyOrderEntry yetAnotherPosEntry = Mockito.mock(TargetLaybyOrderEntry.class);
        posEntries.add(yetAnotherPosEntry);

        BDDMockito.given(yetAnotherPosEntry.getCode()).willReturn("yet another product code");
        BDDMockito.given(yetAnotherPosEntry.getQuantity()).willReturn(8L);

        posLaybyUpdater.checkLaybyOrdersMatch(posLaybyOrder, order);
    }


}
