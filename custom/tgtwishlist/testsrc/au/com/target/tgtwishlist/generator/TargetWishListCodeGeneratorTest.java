/**
 *
 */
package au.com.target.tgtwishlist.generator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class TargetWishListCodeGeneratorTest {


    @Test
    public void testCodeGenerateSucessfulWhenNoCodePresent() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        final UserModel userModel = Mockito.mock(TargetCustomerModel.class);
        targetWishListModel.setUser(userModel);
        final PK userPk = PK.fromLong(1111);
        BDDMockito.given(userModel.getPk()).willReturn(userPk);
        targetWishListModel.setType(WishListTypeEnum.FAVOURITE);
        targetWishListModel.setName("listName");
        final String code = TargetWishListCodeGenerator.generateKey(targetWishListModel);
        Assertions.assertThat(code).isEqualTo(userPk + "-listName-FAVOURITE");
    }

    @Test
    public void testOnPrepareSucessfulWhenNoCodePresentWithVeryLongName() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        final UserModel userModel = Mockito.mock(TargetCustomerModel.class);
        targetWishListModel.setUser(userModel);
        final PK userPk = PK.fromLong(1111);
        BDDMockito.given(userModel.getPk()).willReturn(userPk);
        targetWishListModel.setType(WishListTypeEnum.FAVOURITE);
        targetWishListModel.setName("this name is very long that it cannot fit the required limit");
        final String code = TargetWishListCodeGenerator.generateKey(targetWishListModel);
        Assertions.assertThat(code).isEqualTo(userPk + "-this-name-is-very-lo-FAVOURITE");
    }

    @Test
    public void testOnPrepareSucessfulWhenNoCodePresentWithNoName() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        final UserModel userModel = Mockito.mock(TargetCustomerModel.class);
        targetWishListModel.setUser(userModel);
        final PK userPk = PK.fromLong(1111);
        BDDMockito.given(userModel.getPk()).willReturn(userPk);
        targetWishListModel.setType(WishListTypeEnum.FAVOURITE);
        final String code = TargetWishListCodeGenerator.generateKey(targetWishListModel);
        Assertions.assertThat(code).isEqualTo(userPk + "-FAVOURITE");
    }

    @Test
    public void testOnPrepareSucessfulWhenNoCodePresentWithNoUser() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        targetWishListModel.setType(WishListTypeEnum.FAVOURITE);
        targetWishListModel.setName("listName");
        final String code = TargetWishListCodeGenerator.generateKey(targetWishListModel);
        Assertions.assertThat(code).isEqualTo("listName" + "-FAVOURITE");
    }

    @Test
    public void testOnPrepareSucessfulWhenNoCodePresentWithNoType() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        final UserModel userModel = Mockito.mock(TargetCustomerModel.class);
        targetWishListModel.setUser(userModel);
        final PK userPk = PK.fromLong(1111);
        BDDMockito.given(userModel.getPk()).willReturn(userPk);
        targetWishListModel.setName("listName");
        final String code = TargetWishListCodeGenerator.generateKey(targetWishListModel);
        Assertions.assertThat(code).isEqualTo(userPk + "-listName");
    }

}
