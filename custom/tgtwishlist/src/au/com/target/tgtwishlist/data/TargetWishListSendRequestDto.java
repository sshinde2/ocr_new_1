/**
 * 
 */
package au.com.target.tgtwishlist.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author rsamuel3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TargetWishListSendRequestDto {
    private String emailType = "Favourites";
    private TargetCustomerWishListDto customer;

    /**
     * @return the customer
     */
    public TargetCustomerWishListDto getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final TargetCustomerWishListDto customer) {
        this.customer = customer;
    }

    /**
     * @return the emailType
     */
    public String getEmailType() {
        return emailType;
    }

    /**
     * @param emailType
     *            the emailType to set
     */
    public void setEmailType(final String emailType) {
        this.emailType = emailType;
    }

    @Override
    public String toString() {
        return customer.toString();
    }
}
