/**
 *
 */
package au.com.target.tgtwishlist.dao;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
public interface TargetWishListDao {
    /**
     * This method will retrieve the wishlist based on the User and Type
     *
     * @param user
     * @param type
     * @return list of targetWishListModel
     */
    List<TargetWishListModel> getWishListByUserAndType(UserModel user, WishListTypeEnum type);

    /**
     * This method will retrieve the wishlist based on the Name, User and Type
     *
     * @param name
     * @param user
     * @param type
     * @return targetWishListModel
     */
    TargetWishListModel getWishListByNameUserAndType(String name, UserModel user, WishListTypeEnum type);

    /**
     * This will retrieve the n number of wish list entries based sorted by the oldest
     *
     * @param wishlist
     * @return List<TargetWishListEntry>
     */
    public List<TargetWishListEntryModel> findOldestWishlistEntryForAWishList(TargetWishListModel wishlist,
            int noOfEntries);

    /**
     * This will retrieve the wish lists which contains the list of variants.
     * 
     * @param variants
     * @return List<TargetWishListEntryModel>
     */
    public List<TargetWishListEntryModel> findWishListEntriesWithVariant(final List<String> variants);
}
