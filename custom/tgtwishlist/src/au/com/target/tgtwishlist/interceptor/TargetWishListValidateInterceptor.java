/**
 *
 */
package au.com.target.tgtwishlist.interceptor;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.enums.WishListVisibilityEnum;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
public class TargetWishListValidateInterceptor implements ValidateInterceptor<TargetWishListModel> {

    /*
     * (non-Javadoc)
     *
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object,
     * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final TargetWishListModel wishList, final InterceptorContext arg1)
            throws InterceptorException {

        if (null == wishList.getUser()) {
            throw new InterceptorException(ErrorMessages.WISHLIST_VALIDATION_ERROR + ErrorMessages.USER_IS_REQUIRED);
        }
        if (!(wishList.getUser() instanceof TargetCustomerModel)) {
            throw new InterceptorException(ErrorMessages.WISHLIST_VALIDATION_ERROR
                    + ErrorMessages.USER_SHOULD_BE_TARGETCUSTOMER);
        }

        final WishListTypeEnum wishListType = wishList.getType();
        //Valdiating the type of wishlist is present.
        if (null == wishListType) {
            throw new InterceptorException(ErrorMessages.WISHLIST_VALIDATION_ERROR + ErrorMessages.TYPE_IS_REQUIRED);
        }

        if (WishListTypeEnum.FAVOURITE.equals(wishListType)) {
            validateFavouritesList(wishList);
        }
    }

    private void validateFavouritesList(final TargetWishListModel wishList) throws InterceptorException {

        //validating for a user only one favourites list exist.
        final UserModel user = wishList.getUser();
        final List<Wishlist2Model> usersWishLists = user.getWishlist();

        for (final Wishlist2Model usersWishList : usersWishLists) {
            if (!(usersWishList instanceof TargetWishListModel)) {
                continue;
            }
            final TargetWishListModel usersTargetWishList = (TargetWishListModel)usersWishList;
            if (usersTargetWishList.getType().equals(WishListTypeEnum.FAVOURITE)
                    && wishList.getPk() != usersTargetWishList.getPk()) {
                throw new InterceptorException(
                        ErrorMessages.WISHLIST_VALIDATION_ERROR + ErrorMessages.FAVOURITES_LIST_ALREADY_EXIST_FOR_USER);
            }
        }

        //validating for a user the favourites list can only be private.
        if (!WishListVisibilityEnum.PRIVATE.equals(wishList.getVisibility())) {
            throw new InterceptorException(
                    ErrorMessages.WISHLIST_VALIDATION_ERROR
                            + ErrorMessages.FAVOURITES_LIST_NON_PRIVATE_VALIDATION_ERROR);
        }

    }


    protected interface ErrorMessages {
        public static final String WISHLIST_VALIDATION_ERROR = "Error creating/updating wishlist - ";
        public static final String USER_IS_REQUIRED = "Wish List should belong to a user";
        public static final String TYPE_IS_REQUIRED = "Wish List Should have a type";
        public static final String FAVOURITES_LIST_ALREADY_EXIST_FOR_USER = "User already has another favourites wishlist";
        public static final String FAVOURITES_LIST_NON_PRIVATE_VALIDATION_ERROR = "Favourites Wish List should be only private";
        public static final String USER_SHOULD_BE_TARGETCUSTOMER = "Only Target Customer can have target wish list";
    }


}
