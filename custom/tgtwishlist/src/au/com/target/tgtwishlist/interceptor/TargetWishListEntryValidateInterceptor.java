/**
 *
 */
package au.com.target.tgtwishlist.interceptor;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;


/**
 * @author pthoma20
 *
 */
public class TargetWishListEntryValidateInterceptor implements ValidateInterceptor<TargetWishListEntryModel> {


    private CatalogVersionService catalogVersionService;

    /*
     * (non-Javadoc)
     *
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object,
     * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final TargetWishListEntryModel wishListEntry, final InterceptorContext arg1)
            throws InterceptorException {

        final ProductModel productModel = wishListEntry.getProduct();
        if (null == productModel) {
            throw new InterceptorException(ErrorMessages.WISHLIST_ENTRY_VALIDATION_ERROR + ErrorMessages.PRODUCT_NULL);
        }

        final CatalogVersionModel onlineCatalogVersion = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION);

        if (productModel.getCatalogVersion() == null
                || !onlineCatalogVersion.getPk().equals(productModel.getCatalogVersion().getPk())) {
            throw new InterceptorException(
                    ErrorMessages.WISHLIST_ENTRY_VALIDATION_ERROR + ErrorMessages.PRODUCT_SHOULD_BE_ONLINE_VERSION);
        }
    }



    protected interface ErrorMessages {
        public static final String WISHLIST_ENTRY_VALIDATION_ERROR = "Error creating/updating wishlist entry - ";
        public static final String PRODUCT_NULL = "Product cannot be null for a wish list Entry";
        public static final String PRODUCT_SHOULD_BE_ONLINE_VERSION = "Product should an Online version";
    }



    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }




}
