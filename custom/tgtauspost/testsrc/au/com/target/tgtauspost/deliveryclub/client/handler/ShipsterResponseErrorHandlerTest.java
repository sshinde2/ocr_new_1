package au.com.target.tgtauspost.deliveryclub.client.handler;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipsterResponseErrorHandlerTest {

    private Set<HttpStatus> shipsterKnownErrorCodes;

    @Mock
    private ResponseErrorHandler mockFallbackResponseErrorHandler;

    @Mock
    private ClientHttpResponse mockClientHttpResponse;

    @InjectMocks
    private final ShipsterResponseErrorHandler shipsterResponseErrorHandler = new ShipsterResponseErrorHandler();

    @Before
    public void setup() {
        shipsterKnownErrorCodes = new HashSet<>();
        shipsterResponseErrorHandler.setShipsterKnownErrorCodes(shipsterKnownErrorCodes);
    }

    @Test
    public void testHasErrorWithNoKnownCodes() throws IOException {
        willReturn(Boolean.TRUE).given(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);

        final boolean result = shipsterResponseErrorHandler.hasError(mockClientHttpResponse);

        assertThat(result).isTrue();
        verify(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);
    }

    @Test
    public void testHasErrorWithNoMatchingCode() throws IOException {
        willReturn(Boolean.TRUE).given(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);

        shipsterKnownErrorCodes.add(HttpStatus.PAYMENT_REQUIRED);
        given(mockClientHttpResponse.getStatusCode()).willReturn(HttpStatus.I_AM_A_TEAPOT);

        final boolean result = shipsterResponseErrorHandler.hasError(mockClientHttpResponse);

        assertThat(result).isTrue();
        verify(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);
    }

    @Test
    public void testHasErrorWithUnknownStatusCode() throws IOException {
        willReturn(Boolean.TRUE).given(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);

        shipsterKnownErrorCodes.add(HttpStatus.PAYMENT_REQUIRED);
        given(mockClientHttpResponse.getStatusCode()).willThrow(new IOException("Exception!"));

        final boolean result = shipsterResponseErrorHandler.hasError(mockClientHttpResponse);

        assertThat(result).isTrue();
        verify(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);
    }

    @Test
    public void testHasErrorWithMatchingCode() throws IOException {
        willReturn(Boolean.TRUE).given(mockFallbackResponseErrorHandler).hasError(mockClientHttpResponse);

        shipsterKnownErrorCodes.add(HttpStatus.I_AM_A_TEAPOT);
        given(mockClientHttpResponse.getStatusCode()).willReturn(HttpStatus.I_AM_A_TEAPOT);

        final boolean result = shipsterResponseErrorHandler.hasError(mockClientHttpResponse);

        assertThat(result).isFalse();
        verifyZeroInteractions(mockFallbackResponseErrorHandler);
    }

    @Test
    public void testHandleError() throws IOException {
        shipsterResponseErrorHandler.handleError(mockClientHttpResponse);

        verify(mockFallbackResponseErrorHandler).handleError(mockClientHttpResponse);
    }
}
