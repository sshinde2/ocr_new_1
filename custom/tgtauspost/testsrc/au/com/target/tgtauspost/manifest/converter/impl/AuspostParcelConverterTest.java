/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.ParcelDetailsDTO;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuspostParcelConverterTest {

    private final AuspostParcelConverter converter = new AuspostParcelConverter();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testWithParcelModelNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Parcels cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithParcelDetailsValid() {
        final String weight = "14.34";
        final String height = "10.34";
        final String length = "11.34";
        final String width = "12.34";
        final ParcelDetailsDTO parcelsDto = converter.convert(AuspostConverterTestUtil
                .getConsignmentParcelModel(weight, height,
                        length, width));
        verifyParcelDetails(parcelsDto, weight, "10", "11", "12");
    }

    @Test
    public void testWithParcelDetailsValidWithWeightsRoundingUp() {
        final String weight2 = "16.87423434";
        final String height2 = "100";
        final String length2 = "200";
        final String width2 = "300";
        final ParcelDetailsDTO parcelsDto = converter.convert(AuspostConverterTestUtil
                .getConsignmentParcelModel(weight2, height2,
                        length2, width2));
        verifyParcelDetails(parcelsDto, "16.87", height2, length2, width2);
    }

    @Test
    public void testWithParcelDetailsValidWithWeightsRoundingDown() {
        final String weight2 = "16.87623434";
        final String height2 = "100";
        final String length2 = "200";
        final String width2 = "300";
        final ParcelDetailsDTO parcelsDto = converter.convert(AuspostConverterTestUtil
                .getConsignmentParcelModel(weight2, height2,
                        length2, width2));
        verifyParcelDetails(parcelsDto, "16.88", height2, length2, width2);
    }

    /**
     * @param parcel
     */
    protected void verifyParcelDetails(final ParcelDetailsDTO parcel, final String weight, final String height,
            final String length, final String width) {
        final ConsignmentParcelDTO parcelDetails = parcel.getParcelDetails();
        Assertions.assertThat(parcelDetails).isNotNull();
        Assertions.assertThat(parcelDetails.getWeight()).isNotNull().isEqualTo(weight);
        Assertions.assertThat(parcelDetails.getHeight()).isNotNull().isEqualTo(height);
        Assertions.assertThat(parcelDetails.getLength()).isNotNull().isEqualTo(length);
        Assertions.assertThat(parcelDetails.getWidth()).isNotNull().isEqualTo(width);
    }

}
