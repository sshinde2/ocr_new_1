/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ManifestStoreDetailsConverterTest {

    @Mock
    private TargetPointOfServiceModel pos;

    @Mock
    private StoreFulfilmentCapabilitiesModel fulfilmentCapalities;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    private final ManifestStoreDetailsConverter converter = new ManifestStoreDetailsConverter();

    @Test
    public void testWithNullPOS() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Target Point of Service cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithNullStoreDTO() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Store DTO cannot be null");
        converter.convert(pos, null);
    }

    @Test
    public void testWithNullStoreFulfilmentCapabilities() {
        BDDMockito.when(pos.getFulfilmentCapability()).thenReturn(null);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("StoreFulfilment Capabilities cannot be null");
        converter.convert(pos, new StoreDTO());
    }

    @Test
    public void testWithAllValidData() {
        BDDMockito.when(pos.getFulfilmentCapability()).thenReturn(fulfilmentCapalities);
        BDDMockito.when(fulfilmentCapalities.getUsername()).thenReturn("eParcelDemoSoap");
    }
}
