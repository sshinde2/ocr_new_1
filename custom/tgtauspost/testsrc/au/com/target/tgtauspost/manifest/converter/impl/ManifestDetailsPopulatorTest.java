/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.AddressDTO;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtauspost.integration.dto.TargetManifestDTO;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ManifestDetailsPopulatorTest {
    @Mock
    private TargetPointOfServiceModel pos;

    @Mock
    private TargetManifestModel manifest;

    @Mock
    private ManifestStoreDetailsConverter manifestStoreDetailsConverter;

    @Mock
    private AuspostAddressConverter auspostAddressConverter;

    @Mock
    private ManifestConverter manifestConverter;

    @InjectMocks
    private final ManifestDetailsPopulator manifestDetailsPopulator = new ManifestDetailsPopulator();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testWithNullPos() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Point of Service cannot be null");
        manifestDetailsPopulator.populateManifestDetails(null, manifest);
    }

    @Test
    public void testWithNullManifest() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Manifest cannot be null");
        manifestDetailsPopulator.populateManifestDetails(pos, null);
    }

    @Test
    public void testWithAllValidData() {
        BDDMockito.when(auspostAddressConverter.convert(Mockito.any(AddressModel.class))).thenReturn(new AddressDTO());
        BDDMockito.when(manifestStoreDetailsConverter.convert(pos)).thenReturn(new StoreDTO());
        BDDMockito.when(manifestConverter.convert(manifest)).thenReturn(new TargetManifestDTO());
        final AuspostRequestDTO request = manifestDetailsPopulator.populateManifestDetails(pos, manifest);
        Assertions.assertThat(request).isNotNull();
        Assertions.assertThat(request.getStore()).isNotNull();
        Assertions.assertThat(request.getStore().getAddress()).isNotNull();
        Assertions.assertThat(request.getStore().getLabel()).isNull();
        Assertions.assertThat(request.getStore().getManifest()).isNotNull();
        Assertions.assertThat(request.getStore().getConsignment()).isNull();
    }
}
