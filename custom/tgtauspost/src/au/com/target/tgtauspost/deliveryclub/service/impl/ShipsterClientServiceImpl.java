/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtauspost.deliveryclub.client.AusPostShipsterClient;
import au.com.target.tgtauspost.deliveryclub.client.impl.AusPostShipsterClientImpl;
import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.deliveryclub.service.ShipsterClientService;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterDeliveryAddressDTO;
import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.shipster.ShipsterConfigService;



/**
 * @author gsing236
 *
 */
public class ShipsterClientServiceImpl implements ShipsterClientService {

    private static final Logger LOG = Logger.getLogger(AusPostShipsterClientImpl.class);
    private static final String SHA1_ALGORITHM = "SHA1";
    private static final String ERROR_SHIPSTER_SERIVCE = "Shipster: Service error";
    private AusPostShipsterClient ausPostShipsterClient;
    private ShipsterConfigService shipsterConfigService;

    @Override
    public boolean verifyEmail(final String email) throws AuspostShipsterClientException {

        final ShipsterVerifyEmailResponseDTO verifyEmailResponse = ausPostShipsterClient
                .verify(getEncryptedEmail(email));
        return verifyEmailResponse.isVerified();

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtauspost.deliveryclub.service.AusPostShipsterService#confirmOrder(au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO)
     */
    @Override
    public ShipsterConfirmOrderResponseDTO confirmOrder(final OrderModel orderModel)
            throws AuspostShipsterClientException {

        ShipsterConfirmOrderResponseDTO shipsterConfirmOrderResponseDTO = null;
        if (isOrderValidForShipsterOrderConfirmation(orderModel)) {
            final ShipsterConfirmOrderRequestDTO request = createShipsterConfirmationOrderRequestDTO(orderModel);
            shipsterConfirmOrderResponseDTO = ausPostShipsterClient.confirmOrder(request);
        }
        return shipsterConfirmOrderResponseDTO;
    }


    /**
     * 
     * @param orderModel
     * @return true if the user is delivery club member and false if
     */
    private boolean isOrderValidForShipsterOrderConfirmation(final OrderModel orderModel) {

        if (BooleanUtils.isFalse(orderModel.getAusPostClubMember())) {
            return false;
        }

        final DeliveryModeModel deliveryMode = getDeliveryMode(orderModel);
        if (!(deliveryMode instanceof TargetZoneDeliveryModeModel)) {
            return false;
        }

        final ShipsterConfigModel shipsterConfigModel = getShipsterConfigModelForDeliveryMode(
                (TargetZoneDeliveryModeModel)deliveryMode);
        if (shipsterConfigModel == null) {
            return false;
        }
        if (BooleanUtils.isFalse(shipsterConfigModel.getActive())) {
            return false;
        }
        return true;
    }

    /**
     * @param orderModel
     * @return ShipsterConfirmOrderRequestDTO
     */
    protected ShipsterConfirmOrderRequestDTO createShipsterConfirmationOrderRequestDTO(final OrderModel orderModel) {
        final ShipsterConfirmOrderRequestDTO request = new ShipsterConfirmOrderRequestDTO();

        final UserModel user = orderModel.getUser();

        if (user != null && user instanceof TargetCustomerModel) {
            final TargetCustomerModel targetCustomer = (TargetCustomerModel)user;
            request.setEmail(getEncryptedEmail(targetCustomer.getContactEmail()));
        }
        request.setOrderReference(orderModel.getCode());
        request.setOrderTotal(orderModel.getTotalPrice());
        request.setShippingFee(orderModel.getOriginalDeliveryCost());
        request.setShippingFeePaid(orderModel.getDeliveryCost());

        final ShipsterConfigModel shipsterConfigModel = getShipsterConfigModelForDeliveryMode(
                (TargetZoneDeliveryModeModel)getDeliveryMode(orderModel));
        request.setShippingMethod(getShippingMethodForShipster(shipsterConfigModel, orderModel));

        final ShipsterDeliveryAddressDTO deliveryAddressDTO = createShipsterDeliveryAddress(orderModel);
        request.setDeliveryAddress(deliveryAddressDTO);

        return request;
    }

    /**
     * @return Shipster specific delivery method if present otherwise target delivery mode
     */
    private String getShippingMethodForShipster(final ShipsterConfigModel shipsterConfigModel,
            final OrderModel orderModel) {
        return shipsterConfigModel.getShipsterShippingMethod() != null ? shipsterConfigModel.getShipsterShippingMethod()
                : getDeliveryMode(orderModel).getDescription();
    }

    /**
     * 
     * @param targetZoneDeliveryModeModel
     * @return ShipsterConfigModel containing if the delivery mode is active and Shipster specific shipping method
     */
    private ShipsterConfigModel getShipsterConfigModelForDeliveryMode(
            final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel) {
        return shipsterConfigService.getShipsterModelForDeliveryModes(targetZoneDeliveryModeModel);
    }

    /**
     * 
     * @param orderModel
     * @return DeliveryModeModel
     */
    private DeliveryModeModel getDeliveryMode(final OrderModel orderModel) {
        return orderModel.getDeliveryMode();
    }

    /**
     * 
     * @param orderModel
     * @return ShipsterDeliveryAddressDTO
     */
    private ShipsterDeliveryAddressDTO createShipsterDeliveryAddress(final OrderModel orderModel) {
        final ShipsterDeliveryAddressDTO deliveryAddressDTO = new ShipsterDeliveryAddressDTO();
        final AddressModel addressModel = orderModel.getDeliveryAddress();
        if (addressModel != null) {
            deliveryAddressDTO.setFirstName(addressModel.getFirstname());
            deliveryAddressDTO.setLastName(addressModel.getLastname());
            deliveryAddressDTO.setLine1(addressModel.getLine1());
            deliveryAddressDTO.setLine2(addressModel.getLine2());
            deliveryAddressDTO.setSuburb(addressModel.getTown());
            deliveryAddressDTO.setState(addressModel.getDistrict());
            deliveryAddressDTO.setPostcode(addressModel.getPostalcode());
            deliveryAddressDTO
                    .setCountry(addressModel.getCountry() != null ? addressModel.getCountry().getIsocode() : null);
        }
        return deliveryAddressDTO;
    }


    /**
     * @param email
     * @return SHA email
     * @throws NoSuchAlgorithmException
     */
    private String hashEmail(final String email) throws NoSuchAlgorithmException {
        final String trimmedLowerCaseEmail = StringUtils.trim(email).toLowerCase();
        final MessageDigest messageDigest = MessageDigest.getInstance(SHA1_ALGORITHM);
        final byte[] result = messageDigest.digest(trimmedLowerCaseEmail.getBytes());
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        final String hashedEmail = sb.toString();
        return hashedEmail;
    }

    /**
     * 
     * @param email
     * @return hashed email
     * @throws AuspostShipsterClientException
     */
    private String getEncryptedEmail(final String email) throws AuspostShipsterClientException {
        try {

            return hashEmail(email);
        }
        catch (final NoSuchAlgorithmException e) {
            LOG.error(ERROR_SHIPSTER_SERIVCE + " Error occured while hasing email " + e);
            throw new AuspostShipsterClientException(e);
        }
    }


    /**
     * @param ausPostShipsterClient
     *            the ausPostShipsterClient to set
     */
    @Required
    public void setAusPostShipsterClient(final AusPostShipsterClient ausPostShipsterClient) {
        this.ausPostShipsterClient = ausPostShipsterClient;
    }

    /**
     * @param shipsterConfigService
     *            the shipsterConfigService to set
     */
    @Required
    public void setShipsterConfigService(final ShipsterConfigService shipsterConfigService) {
        this.shipsterConfigService = shipsterConfigService;
    }

}
