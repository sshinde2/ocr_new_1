/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.dto;

import java.io.Serializable;


/**
 * @author rmcalave
 *
 */
public class ShipsterErrorResponseDTO implements Serializable {
    private String id;

    private String code;

    private String detail;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * @param detail
     *            the detail to set
     */
    public void setDetail(final String detail) {
        this.detail = detail;
    }
}
