/**
 * 
 */
package au.com.target.tgtauspost.exception;

/**
 * Custom exception to throw when data for AusPost is incomplete
 * 
 * @author jjayawa1
 *
 */
public class AuspostIncompleteDataException extends RuntimeException {

    /**
     * @param message
     *            Custom message
     */
    public AuspostIncompleteDataException(final String message) {
        super(message);
    }

    /**
     * @param message
     *            Custom message
     * @param e
     *            Exception
     */
    public AuspostIncompleteDataException(final String message, final Throwable e) {
        super(message, e);
    }

}
