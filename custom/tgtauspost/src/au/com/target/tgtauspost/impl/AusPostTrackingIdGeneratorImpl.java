/**
 * 
 */
package au.com.target.tgtauspost.impl;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.StringUtils;

import au.com.target.tgtfulfilment.exceptions.TrackingIdGenerationException;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentTrackingIdGenerator;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author smishra1
 *
 */
public class AusPostTrackingIdGeneratorImpl implements ConsignmentTrackingIdGenerator {
    private KeyGenerator keyGenerator;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.impl.ConsignmentTrackingIdGenerator#generateTrackingIdForConsignment(au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel)
     */
    @Override
    public String generateTrackingIdForConsignment(final StoreFulfilmentCapabilitiesModel storeFulfilmentModel)
            throws TrackingIdGenerationException {
        final String merchantLocationId = storeFulfilmentModel.getMerchantLocationId();
        final Object generatedCode = keyGenerator.generate();

        if (null == generatedCode) {
            throw new TrackingIdGenerationException(
                    "Error while generating tracking Id : Null code returned by key generator");
        }

        if (StringUtils.isEmpty(merchantLocationId)) {
            throw new TrackingIdGenerationException("Error while generating tracking Id : No MerchantLocationId found");
        }
        return merchantLocationId.concat(generatedCode.toString());
    }

    /**
     * 
     * 
     * @param keyGenerator
     *            KeyGenerator
     */
    @Required
    public void setKeyGenerator(final KeyGenerator keyGenerator) {
        this.keyGenerator = keyGenerator;
    }
}
