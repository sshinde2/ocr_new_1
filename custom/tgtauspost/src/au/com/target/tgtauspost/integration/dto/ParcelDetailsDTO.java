/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;


/**
 * DTO to hold parcel details.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ParcelDetailsDTO {
    private ConsignmentParcelDTO parcelDetails;

    /**
     * @return the parcelDetails
     */
    public ConsignmentParcelDTO getParcelDetails() {
        return parcelDetails;
    }

    /**
     * @param parcelDetails
     *            the parcelDetails to set
     */
    public void setParcelDetails(final ConsignmentParcelDTO parcelDetails) {
        this.parcelDetails = parcelDetails;
    }

}
