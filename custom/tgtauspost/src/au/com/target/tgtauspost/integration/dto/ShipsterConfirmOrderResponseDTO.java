/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import au.com.target.tgtauspost.deliveryclub.dto.AbstractShipsterResponseDTO;


/**
 * @author gsing236
 *
 */
public class ShipsterConfirmOrderResponseDTO extends AbstractShipsterResponseDTO {

    private boolean submitted;
    private String transactionId;

    /**
     * @return the submitted
     */
    public boolean isSubmitted() {
        return submitted;
    }

    /**
     * @param submitted
     *            the submitted to set
     */
    public void setSubmitted(final boolean submitted) {
        this.submitted = submitted;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId
     *            the transactionId to set
     */
    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

}
