/**
 * 
 */
package au.com.target.tgtauspost.order.strategies.impl;

import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.InvalidDeliveryAddressDenialStrategy;


/**
 * @author rsamuel3
 *
 */
public class AuspostInvalidDeliveryAddressDenialStrategy extends InvalidDeliveryAddressDenialStrategy {

    /**
     * validate that a chargezone exists for the postcode
     * 
     * @param postcode
     * @param postcodeModel
     */
    @Override
    protected DenialResponse validatePostcode(final String postcode,
            final PostCodeModel postcodeModel) {
        final DenialResponse response = super.validatePostcode(postcode, postcodeModel);
        if (response.isDenied()) {
            return response;
        }
        if (postcodeModel != null && postcodeModel.getAuspostChargeZone() == null) {
            return DenialResponse.createDenied("EmptyChargeZoneForPostcode postcode=" + postcode);
        }
        return DenialResponse.createNotDenied();
    }

}
