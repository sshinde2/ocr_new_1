/**
 * 
 */
package au.com.target.tgtmail.dto;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;

/**
 * @author mjanarth
 * 
 */
public class TargetCustomerSubscriptionResponseDto {

    private Integer responseCode;
    private String responseMessage;
    private TargetCustomerSubscriptionResponseType responseType;

    /**
     * @return the responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseType
     */
    public TargetCustomerSubscriptionResponseType getResponseType() {
        return responseType;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final TargetCustomerSubscriptionResponseType responseType) {
        this.responseType = responseType;
    }
}
