/**
 * 
 */
package au.com.target.tgtmail.actions;

import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtbusproc.aop.BusinessProcessLoggingAspect;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.service.TargetCustomerSubscriptionService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author bhuang3
 * 
 */
public class SendCustomerSubscriptionAction extends RetryActionWithSpringConfig<BusinessProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendCustomerSubscriptionAction.class);

    private TargetCustomerSubscriptionService targetCustomerSubscriptionService;

    public enum Transition
    {
        OK, RETRYEXCEEDED, NOK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final BusinessProcessModel process) throws RetryLaterException, Exception {
        final Object subscriptionData = getOrderProcessParameterHelper().getCustomerSubscriptionData(process);
        if (!(subscriptionData instanceof TargetCustomerSubscriptionRequestDto)) {
            LOG.warn(SplunkLogFormatter.formatMessage(
                    "RequestDto is not the instance of TargetCustomerSubscriptionRequestDto",
                    TgtutilityConstants.ErrorCode.WARN_CUSTOMER_SUBSCRIPTION_SEND_FAIL));
            return Transition.NOK.toString();
        }
        final TargetCustomerSubscriptionRequestDto subscriptionRequestDto = (TargetCustomerSubscriptionRequestDto)subscriptionData;
        final TargetCustomerSubscriptionResponseDto responseDto = targetCustomerSubscriptionService
                .createCustomerSubscription(subscriptionRequestDto);
        if (responseDto == null) {
            LOG.warn(SplunkLogFormatter.formatMessage(
                    "ResponseDto cannot be null",
                    TgtutilityConstants.ErrorCode.WARN_CUSTOMER_SUBSCRIPTION_SEND_FAIL));
            return Transition.NOK.toString();
        }

        final TargetCustomerSubscriptionResponseType responseType = responseDto.getResponseType();
        if (responseType == null) {
            LOG.warn(SplunkLogFormatter.formatMessage("No responseType returned for customer subscription request",
                    TgtutilityConstants.ErrorCode.WARN_CUSTOMER_SUBSCRIPTION_SEND_FAIL));
            return Transition.NOK.toString();
        }
        if (TargetCustomerSubscriptionResponseType.SUCCESS.equals(responseType)) {
            return Transition.OK.toString();
        }
        else if (TargetCustomerSubscriptionResponseType.OTHERS.equals(responseType)) {
            LOG.warn(SplunkLogFormatter.formatMessage(responseDto.getResponseMessage(),
                    String.valueOf(responseDto.getResponseCode())));
            return Transition.NOK.toString();
        }
        else if (TargetCustomerSubscriptionResponseType.ALREADY_EXISTS.equals(responseType)) {
            LOG.warn(SplunkLogFormatter.formatMessage(responseDto.getResponseMessage(),
                    String.valueOf(responseDto.getResponseCode())));
            return Transition.OK.toString();
        }
        else if (TargetCustomerSubscriptionResponseType.INVALID.equals(responseType)) {
            LOG.warn(SplunkLogFormatter.formatMessage(responseDto.getResponseMessage(),
                    String.valueOf(responseDto.getResponseCode())));
            return Transition.NOK.toString();
        }
        else if (TargetCustomerSubscriptionResponseType.UNAVAILABLE.equals(responseType)) {
            LOG.warn(String.format(BusinessProcessLoggingAspect.ERROR_MESSAGE_FORMAT, responseDto.getResponseMessage(),
                    TgtutilityConstants.ErrorCode.ERR_CUSTOMER_SUBSCRIPTION_SEND_FAIL,
                    SendCustomerSubscriptionAction.class.getSimpleName(),
                    process.getCode(), "N/A"));
            throw new RetryLaterException("Customer subscription service is unavailable, subscription details:"
                    + subscriptionRequestDto.toString());
        }
        return Transition.NOK.toString();
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param targetCustomerSubscriptionService
     *            the targetCustomerSubscriptionService to set
     */
    @Required
    public void setTargetCustomerSubscriptionService(
            final TargetCustomerSubscriptionService targetCustomerSubscriptionService) {
        this.targetCustomerSubscriptionService = targetCustomerSubscriptionService;
    }






}
