Feature: Homepage

Scenario: Homepage has logo, header, footer and enews signup.
  Given go to homepage
  Then target logo should be preset
  And global header should be present
  And global footer should be present
  And enews signup should be present
