@nonweb
Feature: Endeca should update product stock. Baseline update will always update stock while partical update only update stock if stock status is changed.

  Background: 
    Given available stock of saleable product 'P1000_black_S' is 10
    And endeca script 'BaselineUpdate' completes successfully

  Scenario: Partial update should happen if the stock status is changed.
    Given available stock of saleable product 'P1000_black_S' is 0
    When endeca script 'PartialUpdate' completes successfully
    Then available stock of saleable product 'P1000_black_S' is 0 in endeca
    And stockLevelStatus of saleable product 'P1000_black_S' is 'outOfStock' in endeca

  Scenario: Partial update do not update stock if stock status is unchanged.
    Given available stock of saleable product 'P1000_black_S' is 5
    When endeca script 'PartialUpdate' completes successfully
    Then available stock of saleable product 'P1000_black_S' is 10 in endeca
    And stockLevelStatus of saleable product 'P1000_black_S' is 'inStock' in endeca

  Scenario: Baseline update should always update product stock
    Given available stock of saleable product 'P1000_black_S' is 5
    When endeca script 'BaselineUpdate' completes successfully
    Then available stock of saleable product 'P1000_black_S' is 5 in endeca
    And stockLevelStatus of saleable product 'P1000_black_S' is 'inStock' in endeca
