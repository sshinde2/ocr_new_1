@setmocks
Feature: Retrieving product information from POS store

  # Store 7001 is set up in tgtinitialdata
  # Assumes we use MockTargetPOSProductClient, see InjectMocksResource in tgttest.
  # TODO: maybe pass through the mock data from this scenario, using rest call. 
  # Or abstract further to remove specific barcodes etc.
  
  Scenario: Product exists in store, but not in hybris
    When get pos product with barcode 5030949110350
    Then pos product information is:
      | code     | name                 | price | wasPrice |
      | 55555555 | Test pos description | $5    | $8       |

  Scenario: Product exists in store, and in hybris
    When get pos product with barcode 9345973983075
    Then pos product information is:
      | code          | name       | price | wasPrice | imageAlt           |
      | P1000_black_S | Baby shoes | $5    |          | Baby Shoes product |

  Scenario: Product not found in store
    When get pos product with barcode 55
    Then error is 'Itemcode not found'

  Scenario: POS error code for product
    When get pos product with barcode 746775078065
    Then error is 'POS error'
