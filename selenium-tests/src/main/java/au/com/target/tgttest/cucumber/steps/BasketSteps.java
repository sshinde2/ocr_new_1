/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.common.ShoppingBasketItem;
import au.com.target.tgttest.selenium.process.ShoppingProcess;
import au.com.target.tgttest.selenium.process.TestSession;
import au.com.target.tgttest.selenium.verifier.ShoppingBasketVerifier;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps involving shopping basket.<br/>
 * Assumes TestSession product list is loaded.
 * 
 */
public class BasketSteps extends BaseSteps {


    private ShoppingBasketPage basketPage;


    @When("^add product number (\\d+) to basket$")
    public void addProductToBasket(final int num) {

        assertThat(num).as("product number").isGreaterThan(0);

        TestSession.getInstance().setProduct(TestSession.getInstance().getProductList().get(num - 1));
        ShoppingProcess.getInstance().addProductToBasket(TestSession.getInstance().getProduct());
    }

    @When("^add all products to basket$")
    public void addAllProductsToBasket() {

        ShoppingProcess.getInstance().addProductsToBasket(TestSession.getInstance().getProductList());
    }

    @Then("^basket num items is (\\d+)$")
    public void verifyBasketNumItems(final int num) {

        assertThat(getBasketPage().getItemCount()).isEqualTo(num);
    }

    @Then("^basket total is '(.*)'$")
    public void verifyBasketTotal(final String total) {

        assertThat(getBasketPage().getTotal()).isEqualTo(total);
    }

    @Then("^basket total is the total product price$")
    public void verifyBasketTotalIsTotalProductPrice() {

        final String expectedPrice = TestSession.getInstance().getTotalProductPriceFormatted();
        verifyBasketTotal(expectedPrice);
    }

    @Then("^basket has all the products$")
    public void verifyBasketHasAllProducts() {

        final List<ShoppingBasketItem> entries = getBasketPage().getItems();

        assertThat(entries.size()).isEqualTo(TestSession.getInstance().getProductList().size());

        for (final Product product : TestSession.getInstance().getProductList()) {
            ShoppingBasketVerifier.verifyShoppingBasketItem(getBasketPage(), product, 1);
        }

    }


    private ShoppingBasketPage getBasketPage() {

        if (basketPage == null) {
            basketPage = new ShoppingBasketPage(getBrowserDriver());
        }
        return basketPage;
    }

}
