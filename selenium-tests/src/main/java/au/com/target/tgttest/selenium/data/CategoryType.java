/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * @author SBryan5
 * 
 */
public enum CategoryType
{
    KIDS_SHOES(DepartmentType.KIDS, "Shoes", "/c/kids/shoes/W160452", "W160452"),
    KIDS_AFL(DepartmentType.KIDS, "AFL", "/c/kids/afl/W132218", "W132218"),
    KIDS_CLEARANCE(DepartmentType.KIDS, "Clearance", "/c/kids/clearance/W258127", "W258127"),
    KIDS_GIRLS_1_TO_7(DepartmentType.KIDS, "Girls 1 - 7", "/c/kids/girls-1-7/W103459", "W103459"),
    KIDS_GIRLS_7_TO_16(DepartmentType.KIDS, "Girls 7 - 16", "/c/kids/girls-7-16/W103460", "W103460"),
    BABY_FURNITURE(DepartmentType.BABY, "Furniture", "/c/baby/furniture/W109775", "W109775"),
    SCHOOL_UNIFORMS(DepartmentType.SCHOOL, "School Uniforms", "/c/school/school-uniforms/W97369", "W97369"),
    SCHOOL_CLEARANCE(DepartmentType.SCHOOL, "Clearance", "/c/school/clearance/W258130", "W258130"),
    BATH_BODY(DepartmentType.BODY, "Bath + Bodycare", "/c/body-beauty/bath-bodycare/W298361", "W298361"),
    BABY_BABYWEAR(DepartmentType.BABY, "Babywear", "/c/baby/babywear/W93745", "W93745"),
    BABY_TRAVEL_AND_TRANSPORT(DepartmentType.BABY, "Travel + Transport", "c/baby/travel-transport/W150900", "W150900"),
    SCHOOL_STATIONERY(DepartmentType.SCHOOL, "Stationery", "/c/school/stationery/W292240", "W292240"),
    TOYS_BIKES(DepartmentType.TOYS, "Bikes, Scooters + Ride On\\'s",
            "/c/toys/bikes-scooters-ride-on-s/W146718", "W146718");

    private DepartmentType dept;
    private String name;
    private String relativeURL;
    private String code;

    private CategoryType(final DepartmentType dept, final String name, final String relativeURL, final String code)
    {
        this.dept = dept;
        this.name = name;
        this.relativeURL = relativeURL;
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the relativeURL
     */
    public String getRelativeURL()
    {
        return relativeURL;
    }

    /**
     * @return the dept
     */
    public DepartmentType getDept() {
        return dept;
    }

    public String getCode() {
        return code;
    }

}
