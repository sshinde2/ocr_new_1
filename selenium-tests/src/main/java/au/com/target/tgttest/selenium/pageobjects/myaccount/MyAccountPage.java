/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.myaccount;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;


/**
 * My Account Page
 * 
 */
public class MyAccountPage extends TemplatePage {

    /**
     * @param driver
     */
    private final By LogedinMyAccountVerifier = By.cssSelector("div.account-personal");

    public MyAccountPage(final BrowserDriver driver) {
        super(driver);
    }


    @Override
    public boolean isCurrentPage() {
        if (getText(fwd().div(LogedinMyAccountVerifier).link()).contains("My Personal Details")) {
            return true;
        }
        else {
            return false;
        }


    }

    @Override
    public String getPageRelativeURL() {

        return null;
    }

}
