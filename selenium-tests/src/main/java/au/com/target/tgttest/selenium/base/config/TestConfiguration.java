package au.com.target.tgttest.selenium.base.config;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import au.com.target.tgttest.selenium.base.BrowserType;
import au.com.target.tgttest.selenium.sauce.SauceTestConfiguration;


public abstract class TestConfiguration
{
    private static final Logger LOG = LogManager.getLogger(TestConfiguration.class);

    private static final String DEFAULT_PROTOCOL = "http://";

    private static TestConfiguration theConfig;

    protected String currentTestName;
    protected String runMode;
    protected String host;
    protected String port;

    protected Settings settings;


    /**
     * constructor
     *
     * @param aSettings
     *            a Settings implementation.
     */
    protected TestConfiguration(final Settings aSettings)
    {
        settings = aSettings;
    }

    /**
     * get the test configuration instance.
     *
     * @return TestConiguration instance as test config.
     */
    public static TestConfiguration getInstance()
    {
        if (theConfig == null) {

            // Creates an instance of TestConfiguration
            final Settings settings = new LocalSettings();

            final String runMode = checkRunMode(settings);

            if ("local".equalsIgnoreCase(runMode))
            {
                theConfig = new LocalTestConfiguration(settings);
                LOG.info("Loading local test config");
            }
            else if ("sauce".equalsIgnoreCase(runMode))
            {
                theConfig = new SauceTestConfiguration(settings);
                LOG.info("Loading sauce test config");
            }
            else
            {
                throw new RuntimeException("Run Mode not recognised");
            }

            theConfig.setRunMode(runMode);
        }

        return theConfig;
    }


    protected String getHost() {
        String host = System.getenv("tgttest.selenium.host");
        if (StringUtils.isEmpty(host)) {
            // if empty, let's try to get it over the system properties...
            host = System.getProperty("tgttest.selenium.host");
        }
        if (StringUtils.isEmpty(host))
        {
            // and as a last option, just read the selenium.properties default file...
            host = getSettings().getDefaultHost();
        }

        return host;
    }

    protected String getPort() {
        String port = System.getenv("tgttest.selenium.port");
        if (StringUtils.isEmpty(port)) {
            // if empty, let's try to get it over the system properties...
            port = System.getProperty("tgttest.selenium.port");
        }
        if (StringUtils.isEmpty(port))
        {
            // and as a last option, just read the selenium.properties default file...
            port = getSettings().getDefaultPort();
        }

        return port;
    }

    public String getDefaultBrowserName() {

        String name = System.getProperty("tgttest.seleniumtest.browser");

        if (StringUtils.isEmpty(name)) {
            name = getSettings().getDefaultBrowser();
        }

        return name;
    }


    /**
     * returns the wait time out stored in either the system properties or the overridden test configuration class.
     *
     * @return int the time in seconds for the selenium wait timeout.
     */
    public int getWaitTimeout()
    {
        final String timeout = System.getenv("tgttest.selenium.timeout");

        int iTimeout;

        if (StringUtils.isEmpty(timeout))
        {
            iTimeout = getSettings().getTimeout();
        }
        else
        {
            iTimeout = Integer.parseInt(timeout);
        }

        LOG.info("Using timeout=" + iTimeout);

        return iTimeout;
    }



    /**
     * returns whether the test runs as a saucelabs test or not.
     *
     * @return boolean true or false whether the test runs in a saucelabs envronment or not.
     */
    public boolean isSauceTest()
    {
        return ("sauce".equalsIgnoreCase(runMode));
    }

    /**
     * Return true if the run mode is sauce
     *
     * @param settings
     *            the settings instance to check for the value.
     *
     * @return boolean true or false wheter the settings is set on saucelabs or not.
     */
    public static boolean checkSauceTest(final Settings settings)
    {
        return "sauce".equals(checkRunMode(settings));
    }


    /**
     * Calculate run mode supplied by environment
     *
     * @param settings
     *            a Settings instance to check for the runmode value.
     *
     * @return String the run mode.
     */
    private static String checkRunMode(final Settings settings)
    {
        // Check system properties first for runmode variable...
        String browserRunMode = System.getProperty("tgttest.selenium.runmode");

        // ...secondary check the environment variables...
        if (StringUtils.isEmpty(browserRunMode)) {
            browserRunMode = System.getenv("tgttest.selenium.runmode");
        }

        // ...and fall back on value from settings file, if runmode was still not set!
        if (StringUtils.isEmpty(browserRunMode))
        {
            browserRunMode = settings.getRunMode();
        }

        return browserRunMode;
    }


    /**
     * returns the member var of settings.
     *
     * @return Settings the locally stored settings.
     */
    public Settings getSettings() {
        return settings;
    }


    public String getCurrentTestName() {
        return currentTestName;
    }

    public void setCurrentTestName(final String currentTestName) {
        this.currentTestName = currentTestName;
    }


    public String getRunMode() {
        return runMode;
    }

    public void setRunMode(final String runMode) {
        this.runMode = runMode;
    }


    /**
     * Construct the site url given the relative part
     *
     * @param rel
     *            the relative part of the url
     */
    public String constructUrl(final String rel) {
        final StringBuffer url = new StringBuffer();
        url.append(getProtocol());
        url.append(getHost());

        if (!StringUtils.isEmpty(getPort())) {
            url.append(':');
            url.append(getPort());
        }
        url.append('/');

        if (!StringUtils.isEmpty(rel)) {
            url.append(rel);
        }

        return url.toString();
    }


    public String getProtocol() {
        return DEFAULT_PROTOCOL;
    }


    public void notifySessionResult(final String jobId, final String name, final boolean result)
    {
        final String jobDescription = "Job " + name + " (" + jobId + ")";
        if (result)
        {
            LOG.info(jobDescription + " succeeded");
        }
        else
        {
            LOG.info(jobDescription + " failed");
        }
    }



    /**
     * Get/define the driver for the given browser type
     *
     * @param browser
     *            - defines the browser
     * @param name
     *            - name of the test session
     * @return WebDriver
     */
    public abstract WebDriver getDriver(BrowserType browser, String name);



    public String getAdminHost() {
        return getSettings().getAdminHost();
    }

    public String getAdminPort() {
        return getSettings().getAdminPort();
    }

    public String getAdminUsername() {
        return getSettings().getAdminUsername();
    }

    public String getAdminPassword() {
        return getSettings().getAdminPassword();
    }

    public String getWSUsername() {
        return getSettings().getWSUsername();
    }

    public String getWSPassword() {
        return getSettings().getWSPassword();
    }

    public String getESBUsername() {
        return getSettings().getESBUsername();
    }

    public String getESBPassword() {
        return getSettings().getESBPassword();
    }

    public String getEndecaEnePort() {
        return getSettings().getEndecaENEPort();
    }

    public String getCSCockpitUrl() {
        return getAdminHost() + ":" + getAdminPort() + "/" + getSettings().getCSCockpitUrl();
    }

    public String getCSCockpitLogin() {
        return getSettings().getCSCockpitLogin();
    }

    public String getCSCockpitPassword() {
        return getSettings().getCSCockpitPassword();
    }

}
