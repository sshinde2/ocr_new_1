/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;


/**
 * A Page interpretation for the Checkout step "user login"...
 * 
 * @author maesi
 * 
 */
public class UserLoginCheckoutPage extends TemplatePage {

    private final By loginFormWrapper = By.cssSelector("div.log-option.log-login");
    private final By loginUserNameEntry = By.id("j_username");
    private final By loginPasswordEntry = By.id("j_password");
    private final By btnProceedAsSignedInUser = By.cssSelector("button.button-fwd");

    private final By guestFormWrapper = By.cssSelector("div.log-option.log-anonymous");
    private final By guestEmailAccountEntry = By.id("j_username");
    private final By btnProceedAsGuest = By.cssSelector("button.button-fwd");

    private final By btnCreateAccount = By.cssSelector("button.button-fwd.panel-trigger");

    /**
     * @param driver
     */
    public UserLoginCheckoutPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(fwd().div(loginFormWrapper));
    }

    @Override
    public String getPageRelativeURL() {
        return "/checkout/login";
    }

    private FluentWebElement getProceedAsSignedInUserButton() {
        return fwd().div(loginFormWrapper).button(btnProceedAsSignedInUser);
    }

    private FluentWebElement getLoginUserNameEntry() {
        return fwd().div(loginFormWrapper).input(loginUserNameEntry);
    }

    private FluentWebElement getLoginPasswordEntry() {
        return fwd().div(loginFormWrapper).input(loginPasswordEntry);
    }

    private FluentWebElement getProceedAsGuestButton() {
        return fwd().div(guestFormWrapper).button(btnProceedAsGuest);
    }

    private FluentWebElement getGuestEmail() {
        return fwd().div(guestFormWrapper).input(guestEmailAccountEntry);
    }

    /**
     * Sign in.
     * 
     * @param userEmailAddress
     *            the existing user email address as a String.
     * @param password
     *            the existing user password as a String.
     * 
     * @return the DeliveryMethodPage
     */
    public DeliveryMethodPage proceedAsSignedInUser(final String userEmailAddress, final String password) {
        getLoginUserNameEntry().clearField().sendKeys(userEmailAddress);
        getLoginPasswordEntry().clearField().sendKeys(password);
        getProceedAsSignedInUserButton().click();
        return new DeliveryMethodPage(getBrowserDriver());
    }


    public DeliveryMethodPage proceedAsGuest(final String emailAddress) {
        getGuestEmail().clearField().sendKeys(emailAddress);
        getProceedAsGuestButton().click();
        return new DeliveryMethodPage(getBrowserDriver());
    }



}
