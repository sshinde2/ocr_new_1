package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.HomePage;


/**
 * A Selenium page base class for the "Thank you for shopping" page. This page is the last page during a checkout
 * procedure.
 * 
 * @author maesi
 * 
 */
public class ThankyouPage extends BaseCheckoutStepPage {

    private static final String ID_SELECTOR_BARCODE_CONTAINER = "barcode-container";

    private final By barcodeImageContainer = By.id(ID_SELECTOR_BARCODE_CONTAINER);
    private final By buttonContinueShopping = By.id("co-end");
    private final By buttonRegister = By.id("registerForm");


    /**
     * constructor
     * 
     * @param driver
     *            current WebDriver.
     */
    public ThankyouPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {
        return getMainH2Text().contains("Thank You");
    }

    @Override
    public String getPageRelativeURL() {
        return "/checkout/thank-you";
    }

    /**
     * triggers the continue shopping button if found and returns a HomePage.
     * 
     * @return HomePage.
     */
    public HomePage clickContinueShoppingButton() {
        fwd().link(buttonContinueShopping).click();
        return new HomePage(getBrowserDriver());
    }


    /**
     * returns true if there is a barcode (for pick up as result of click and collect transaction) shown on the page.
     * 
     * @return true/false whether barcode container is shown or not.
     */
    public boolean isBarCodeForPickupAvailable() {
        return isDisplayed(fwd().div(barcodeImageContainer));
    }


}