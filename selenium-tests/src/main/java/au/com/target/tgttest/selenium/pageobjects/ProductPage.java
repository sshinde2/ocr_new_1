/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import static org.fest.assertions.Assertions.assertThat;
import static org.seleniumhq.selenium.fluent.Period.secs;

import java.math.BigDecimal;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.common.MinicartPopup;
import au.com.target.tgttest.selenium.util.PriceUtil;


/**
 * Product page object
 * 
 */
public class ProductPage extends TemplatePage {

    private final By buyNowButton = By.cssSelector("button.button-buy-now");
    private final By productDetailTitle = By.cssSelector("h1.title");
    private final By productInStock = By.cssSelector("div.in-stock");
    private final By productOutOfStock = By.cssSelector("div.out-of-stock");
    private final By productPrice = By.cssSelector("div.prod-price span.price");
    private final By productWasPrice = By.cssSelector("div.prod-price span.was-price");
    private final By productDetailDiv = By.cssSelector("div.prod-detail");

    /**
     * @param driver
     */
    public ProductPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(fwd().div(productDetailDiv));
    }

    @Override
    public String getPageRelativeURL() {
        // YTODO Auto-generated method stub
        return null;
    }

    public boolean hasBuyNowButton() {
        return isDisplayed(fwd().button(buyNowButton).ifInvisibleWaitUpTo(secs(5)));
    }

    public MinicartPopup clickBuyNowButton() {
        fwd().button(buyNowButton).click();

        // Wait for minicart popup
        final MinicartPopup minicart = new MinicartPopup(getBrowserDriver());
        assertThat(minicart.isDisplayed()).as("minicart popup did not display").isTrue();
        return minicart;
    }

    public String getProductDetailTitleText()
    {
        return getText(fwd().h1(productDetailTitle));
    }

    public String getProductInStockColour() {
        return fwd().div(productInStock).getCssValue("color").toString();
    }

    public String getProductOutOfStockColour() {
        return fwd().div(productOutOfStock).getCssValue("color").toString();
    }

    public String getProductOutOfStockText() {
        return getText(fwd().div(productOutOfStock));
    }

    public String getProductPriceText()
    {
        return getText(fwd().span(productPrice));
    }

    public BigDecimal getProductPriceValue()
    {
        return PriceUtil.getPriceValue(getProductPriceText());
    }

    public String getProductWasPriceText()
    {
        return getText(fwd().span(productWasPrice));
    }

    public BigDecimal getProductWasPriceValue()
    {
        return PriceUtil.getWasPriceValue(getProductWasPriceText());
    }


}
