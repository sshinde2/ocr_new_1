/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;


/**
 * Watches JUnit tests and notifies results (eg to Sauce)
 * 
 */
public class CustomTestWatcher extends TestWatcher
{
    private static final Logger LOG = LogManager.getLogger(CustomTestWatcher.class);
    private String jobId;
    private TestConfiguration config;
    private String name;

    @Override
    protected void failed(final Throwable e, final Description description)
    {
        LOG.error("CustomTestWatcher.failed: ", e);

        // Also log the stack trace so the cause does not get lost
        LOG.error("Stack trace:" + ExceptionUtils.getStackTrace(e));

        config.notifySessionResult(jobId, name, false);
    }

    @Override
    protected void succeeded(final Description description)
    {
        config.notifySessionResult(jobId, name, true);
    }

    @Override
    protected void starting(final Description description)
    {
        // name = description.getTestClass().getSimpleName() + "." + description.getMethodName();
        name = description.getDisplayName();
    }

    public void setJobId(final String jobId) {
        this.jobId = jobId;
    }

    public void setConfig(final TestConfiguration config) {
        this.config = config;
    }


}
