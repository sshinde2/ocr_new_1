/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Search results filter.
 */
public enum SearchFilterType
{
    price_asc("price-asc", "Price: Low - High"),
    price_desc("price-desc", "Price: High - Low"),
    name_asc("name-asc", "Products: A - Z"),
    name_desc("name-desc", "Products: Z - A"),
    latest("latest", "Latest"),
    relevance("relevance", "Relevance"),
    brand("brand", "Brand");

    private String value;
    private String text;

    private SearchFilterType(final String value, final String text) {
        this.value = value;
        this.text = text;
    }

    public static SearchFilterType findByTitle(final String type) {
        for (final SearchFilterType condidate : SearchFilterType.values()) {
            if (condidate.getText().equals(type)) {
                return condidate;
            }
        }

        return null;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }
}
