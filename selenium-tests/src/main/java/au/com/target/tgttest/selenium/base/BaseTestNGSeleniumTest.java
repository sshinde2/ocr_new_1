/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.xml.XmlTest;

import au.com.target.tgttest.selenium.base.config.BrowserOptions;


/**
 * Base test running with TestNG
 * 
 */
public abstract class BaseTestNGSeleniumTest {

    protected static final Logger LOG = LogManager.getLogger(BaseTestNGSeleniumTest.class);


    @BeforeSuite
    public void suiteStart()
    {
        setupSuiteData();
    }

    @AfterSuite
    public void suiteEnd()
    {
        try {
            removeSuiteData();
        }
        catch (final Exception e) {
            LOG.error("Error while trying to run data remove method", e);
        }

    }

    /**
     * Note we set up the Driver per Class rather than per Test or per Method. So a new browser session is run for each
     * test Class.
     * 
     */
    @Parameters({ "browser", "platform", "version", "deviceOrientation", "deviceType" })
    @BeforeClass(alwaysRun = true)
    public void startSeleniumSession(@Optional("") final String browser,
            @Optional("") final String platform,
            @Optional("") final String version,
            @Optional("") final String deviceOrientation,
            @Optional("") final String deviceType,
            @Optional final XmlTest test)
    {
        final String testName = (test != null) ? test.getName() : "";

        final String seleniumSessionName = getTestSessionName(testName);

        final BrowserOptions options = new BrowserOptions(browser);
        options.setPlatform(platform);
        options.setVersion(version);
        options.setDeviceOrientation(deviceOrientation);
        options.setDeviceType(deviceType);

        SeleniumSession.initialise(options, seleniumSessionName);
    }


    /**
     * method which gets triggered at the end of a test class. this also triggers the removeData method, which is useful
     * to set data back to an initial state.
     * 
     */
    @AfterClass
    public void endSession()
    {
        SeleniumSession.getInstance().shutdownDriverSession();
    }

    /**
     * Clears session data before each test.
     */
    @BeforeMethod
    public void clearSessionDataBeforeTest()
    {
        SeleniumSession.getInstance().clearSessionData();
    }


    @BeforeMethod
    public void setupMethodData() {
        setupTestData();
    }

    /**
     * setting up the test case specific data.
     * 
     */
    protected void setupTestData() {
        // By default do nothing, override in subclass where needed.
    }


    @AfterMethod
    public void removeMethodData() {
        removeTestData();
    }

    /**
     * removing test case specific data. if data needs to be removed or set to a default after a certain test,
     * use/override this method.
     * 
     */
    protected void removeTestData() {
        // By default do nothing, override in subclass where needed.
    }

    /**
     * Since the driver lasts for the class (ie session), we need to calculate the success of the session based on
     * session fails if any method fails.
     */
    @AfterMethod(alwaysRun = true)
    public void recordResult(final ITestResult result)
    {
        if (!result.isSuccess())
        {
            SeleniumSession.getInstance().failSession();
        }
    }


    /**
     * Set up data required for entire TestNG suite
     */
    private void setupSuiteData() {
        // empty
    }

    private void removeSuiteData() {
        // empty
    }

    protected String getTestSessionName(final String testName) {

        String name = getClass().getSimpleName();
        if (testName != null)
        {
            name = testName + ":" + name;
        }
        return name;
    }

}
