/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;



/**
 * Shopping Basket element
 * 
 */
public class ShoppingBasketItem {

    private static final Logger LOG = LogManager.getLogger(TestConfiguration.class);

    private static final Pattern ITEM_CODE_PATTERN = Pattern.compile("^Item code: (.*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);

    private static final Pattern SIZE_PATTERN = Pattern.compile("^Size: (.*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);

    private static final Pattern COLOUR_PATTERN = Pattern.compile("^Colour: (.*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);

    private static final Pattern PRICE_PATTERN = Pattern.compile("^\\$(\\d+\\.\\d{2})$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);

    private static final By IMAGE_LOCATOR = By.cssSelector("td.thumb a img");

    private static final By NAME_LOCATOR = By.cssSelector("td.description p[class*='name']");

    private static final By ITEM_CODE_LOCATOR = By.cssSelector("td.description p[class*='spec-code']");

    private static final By SIZE_LOCATOR = By.cssSelector("td.description p[class*='spec-size']");

    private static final By COLOUR_LOCATOR = By.cssSelector("td.description p[class*='spec-colour']");

    private static final By QUANTITY_LOCATOR = By.cssSelector("td.quantity input[name='quantity']");

    private static final By PRICE_PER_ITEM_LOCATOR = By.cssSelector("td.price");

    private static final By TOTAL_PRICE_LOCATOR = By.cssSelector("td.total");

    private static final By BUTTON_REMOVE_ITEM = By.cssSelector("td.quantity button[value='remove']");

    private static final By BUTTON_UPDATE_ITEM_NUMBERS = By.cssSelector("td.quantity button[value='update']");

    private final String name;
    private final String itemCode;
    private final String size;
    private final String colour;
    private final int quantity;
    private final BigDecimal pricePerItem;
    private final BigDecimal totalPrice;
    private final String imageSource;
    private final String imageAltText;

    private final FluentWebElement element;

    public ShoppingBasketItem(final FluentWebElement element) {

        this.element = element;

        final String text = element.getText().toString();
        this.name = element.p(NAME_LOCATOR).getText().toString();
        this.itemCode = extract(ITEM_CODE_PATTERN, element.p(ITEM_CODE_LOCATOR));
        if (SIZE_PATTERN.matcher(text).find())
        {
            this.size = extract(SIZE_PATTERN, element.p(SIZE_LOCATOR));
        }
        else
        {
            this.size = null;
        }
        if (COLOUR_PATTERN.matcher(text).find())
        {
            this.colour = extract(COLOUR_PATTERN, element.p(COLOUR_LOCATOR));
        }
        else
        {
            this.colour = null;
        }
        final String quantityString = getQuantityElement().getAttribute("value").toString();
        int qty = -1;
        if (!StringUtils.isEmpty(quantityString))
        {
            qty = Integer.parseInt(quantityString);
        }
        this.quantity = qty;
        final String pricePerItemString = extract(PRICE_PATTERN, element.td(PRICE_PER_ITEM_LOCATOR));
        if (pricePerItemString == null)
        {
            throw new IllegalStateException(MessageFormat.format(
                    "Per-item price could not be determined for item {0}", this.name));
        }
        else
        {
            this.pricePerItem = new BigDecimal(pricePerItemString);
        }
        final String totalPriceString = extract(PRICE_PATTERN, element.td(TOTAL_PRICE_LOCATOR));
        if (totalPriceString == null)
        {
            throw new IllegalStateException(MessageFormat.format(
                    "Total item proice could not be determined for item {0}", this.name));
        }
        else
        {
            this.totalPrice = new BigDecimal(totalPriceString);
        }
        final FluentWebElement imageElement = element.img(IMAGE_LOCATOR);
        imageSource = imageElement.getAttribute("src").toString();
        imageAltText = imageElement.getAttribute("alt").toString();


        // updateButton = element.findElement(BUTTON_UPDATE_ITEM_NUMBERS);
    }

    private static String extract(final Pattern pattern, final FluentWebElement element)
    {
        final Matcher matcher = pattern.matcher(element.getText().toString());
        String results = null;
        if (matcher.find())
        {
            results = matcher.group(1);
        }
        return results;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @return the pricePerItem
     */
    public BigDecimal getPricePerItem() {
        return pricePerItem;
    }

    /**
     * @return the totalPrice
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * @return the imageSource
     */
    public String getImageSource() {
        return imageSource;
    }

    /**
     * @return the imageAltText
     */
    public String getImageAltText() {
        return imageAltText;
    }

    private FluentWebElement getQuantityElement() {

        return element.input(QUANTITY_LOCATOR);
    }


    /**
     * sets a new quantity for the quantity entry.
     * 
     * @param number
     * @param clickUpdateButton
     */
    @SuppressWarnings("boxing")
    public void setQuantity(final int number, final boolean clickUpdateButton) {

        getQuantityElement().isDisplayed().shouldBe(true);
        getQuantityElement().clearField();
        getQuantityElement().sendKeys(String.valueOf(number));

        if (clickUpdateButton) {
            clickUpdateButton();
        }
    }


    /**
     * this method triggers the "remove button", so items can be removed from the shopping basket.
     */
    @SuppressWarnings("boxing")
    public void clickRemoveButton() {

        final FluentWebElement removeButton = element.button(BUTTON_REMOVE_ITEM);
        removeButton.click();

        LOG.info("Removed item " + name + " from the basket");
    }

    /**
     * this method triggers the "update button", so changes in quantity, totals, etc. will be updated...
     * 
     */
    @SuppressWarnings("boxing")
    public void clickUpdateButton() {

        final FluentWebElement updateButton = element.button(BUTTON_UPDATE_ITEM_NUMBERS);
        updateButton.click();
    }

}
