/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * some recuring test data which is needed during checkout stories...
 * 
 * @author maesi
 * 
 */
public interface CheckoutTestData {

    String MASTERCARD_TEST_NR = "5123456789012346";
    String MASTERCARD_TEST_NAME = "Mastercard Tester";
    String MASTERCARD_TEST_EXPIRY_MONTH = "05 - May";
    String MASTERCARD_TEST_EXPIRY_YEAR = "2017";
    String MASTERCARD_TEST_SECURITY_CODE = "123";

    String PAYPAL_TEST_GUEST_CARD_NUMBER = "4098644613586486";
    String PAYPAL_TEST_GUEST_FIRSTNAME = "VisaCard";
    String PAYPAL_TEST_GUEST_LASTNAME = "PaypalTester";
    String PAYPAL_TEST_GUEST_EXPIRY_MONTH = "02";
    String PAYPAL_TEST_GUEST_EXPIRY_YEAR = "18";
    String PAYPAL_TEST_GUEST_SECURITY_CODE = "123";
    String PAYPAL_TEST_GUEST_PHONE = "0485987456";

    String HOME_DELIVERY_TITLE = "Mr";
    String HOME_DELIVERY_FIRST_NAME = "Test";
    String HOME_DELIVERY_SURNAME = "Tester";
    String HOME_DELIVERY_ADDRESS_LINE_1 = "1 Test Lane";
    String HOME_DELIVERY_ADDRESS_LINE_2 = "";
    String HOME_DELIVERY_STATE = "Victoria";
    String HOME_DELIVERY_SUBURB = "Fitzroy";
    String HOME_DELIVERY_POSTCODE = "3065";
    String HOME_DELIVERY_PHONE_NR = "0404888888";

    String TEST_BILLING_DETAILS_TITLE = "Ms";
    String TEST_BILLING_DETAILS_FIRST_NAME = "Test";
    String TEST_BILLING_DETAILS_SURNAME = "Tester";
    String TEST_BILLING_DETAILS_ADDRESS_LINE_1 = "1 Test Lane";
    String TEST_BILLING_DETAILS_ADDRESS_LINE_2 = "";
    String TEST_BILLING_DETAILS_STATE = "Tasmania";
    String TEST_BILLING_DETAILS_SUBURB = "Hobart";
    String TEST_BILLING_DETAILS_POSTCODE = "6000";
    String TEST_BILLING_DETAILS_COUNTRY = "Australia";

    String TEST_PICKUP_DETAILS_TITLE = "Mrs";
    String TEST_PICKUP_DETAILS_FIRST_NAME = "Test";
    String TEST_PICKUP_DETAILS_SURNAME = "TestAndCollect";
    String TEST_PICKUP_DETAILS_PHONE_NR = "0404988188";

    String CLICK_AND_COLLECT_TEST_STATE = "Western Australia";
}
