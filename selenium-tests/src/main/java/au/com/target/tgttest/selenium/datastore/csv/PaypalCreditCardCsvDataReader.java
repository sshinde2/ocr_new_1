/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.csv;

import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;


/**
 * PaypalCreditCard csv reader
 * 
 */
public class PaypalCreditCardCsvDataReader extends AbstractCsvDataReader<PaypalCreditCard> {

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.datastore.csv.AbstractCsvDataReader#createInstanceFromCsv(java.lang.String[])
     */
    @Override
    protected PaypalCreditCard createInstanceFromCsv(final String[] csvData) {
        final PaypalCreditCard card = new PaypalCreditCard();

        card.setNumber(getValueForHeader("NUMBER", csvData));
        card.setFirstname(getValueForHeader("FIRSTNAME", csvData));
        card.setLastname(getValueForHeader("LASTNAME", csvData));
        card.setExpiryMonth(getValueForHeader("EXPIRY_MONTH", csvData));
        card.setExpiryYear(getValueForHeader("EXPIRY_YEAR", csvData));
        card.setSecurityCode(getValueForHeader("SECURITY_CODE", csvData));

        return card;
    }

}
