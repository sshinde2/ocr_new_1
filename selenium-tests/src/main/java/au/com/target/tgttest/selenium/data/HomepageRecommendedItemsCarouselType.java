/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Data enum for the recommended products carousel on the home page.
 */
public enum HomepageRecommendedItemsCarouselType implements ProductCarouselItem {
    BABY_SHOES("Baby Shoes product", "8796154560542.jpg",
            "/p/baby-shoes-product/P1000_black", "$10.00", "$999.00", true),
    BABY_GIRLS("Baby Girls product", "8796154855454.jpg",
            "/p/baby-girls-product/P1001_blue", "$10.00", "$999.00", true),
    BABY_NEWBORN("Baby Newborn product", "8796156821534.jpg",
            "/p/Baby-Newborn-product/P1002_black", "$10.00", "$999.00", true),
    BABY_BOYS("Baby Boys product", "8796157181982.jpg",
            "/p/baby-boys-product/P1003_red", "$10.00", "$999.00", true),
    BABY_BAG_BROWN("Baby Bag product brown", "8796160950302.jpg",
            "/p/baby-bag-product/P1061_brown", "$30.00", "$999.00", true),
    BABY_AFL("Baby AFL product - Black", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-afl-product/P1008_black", "$10.00", "$999.00", true),
    BABY_CHRISTENING_WEAR("Baby Christening Wear product - Black",
            "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-christening-wear-product/P1009_black", "$10.00", "$999.00", true),
    BABY_PLAY_EQUIPMENT("Baby Play Equipment product - Black",
            "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-play-equipment-product/P1010_black", "$10.00", "$999.00", true),
    BABY_SWINGS("Baby Swings product - Black", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-swings-product/P1011_black", "$10.00", "$999.00", true),
    BABY_TOYS("Baby Toys product - Black", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-toys-product/P1012_black", "$10.00", "$999.00", true),
    BABY_RATTLES("Baby Rattles product - Red", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-rattles-product/P1013_red", "$10.00", "$999.00", true),
    BABY_TEETHERS("Baby Teethers product - Red", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-teethers-product/P1014_red", "$10.00", "$999.00", true),
    BABY_COTS_AND_MATTRESSES("Baby Cots + Mattresses product - Red",
            "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-cots-mattresses-product/P1015_red", "$10.00", "$999.00", true),
    BABY_HIGH_CHAIRS("Baby High Chairs product - Red", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-high-chairs-product/P1016_red", "$10.00", "$999.00", true),
    BABY_BASSINETS("Baby Bassinets product - Red", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/baby-bassinets-product/P1017_red", "$10.00", "$999.00", true),
    NINJA("Ninja product", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/ninja-product/P0375_black",
            "$10.00", "$999.00", false),
    BABY_JACKET_PRODUCT_RED("Baby Jacket product red", "8796158656542.jpg", "/p/baby-jacket-product/P1060_red",
            "$10.00", "$999.00",
            true);

    private final String titleText;
    private final String imageUrl;
    private final String linkHref;
    private final String currentPrice;
    private final String wasPrice;
    private final boolean approved;

    /**
     * Creates an ItemCarouselType.
     * 
     * @param titleText
     *            the title text (not truncated)
     * @param imageUrl
     *            the url of the thumbnail image
     * @param linkHref
     *            the href of the link
     * @param currentPrice
     *            the current price (with currency symbol)
     * @param wasPrice
     *            the old price (with currency symbol)
     * @param approved
     *            the product's approval state
     */
    private HomepageRecommendedItemsCarouselType(final String titleText, final String imageUrl, final String linkHref,
            final String currentPrice, final String wasPrice, final boolean approved)
    {
        this.titleText = titleText;
        this.imageUrl = imageUrl;
        this.linkHref = linkHref;
        this.currentPrice = currentPrice;
        this.wasPrice = wasPrice;
        this.approved = approved;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getTitleText()
     */
    @Override
    public String getTitleText() {
        return titleText;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getImageUrl()
     */
    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getLinkHref()
     */
    @Override
    public String getLinkHref() {
        return linkHref;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getCurrentPrice()
     */
    @Override
    public String getCurrentPrice() {
        return currentPrice;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getWasPrice()
     */
    @Override
    public String getWasPrice() {
        return wasPrice;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#isApproved()
     */
    @Override
    public boolean isApproved() {
        return approved;
    }
}
