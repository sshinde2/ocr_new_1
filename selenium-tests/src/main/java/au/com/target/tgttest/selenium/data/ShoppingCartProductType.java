/**
 * 
 */
package au.com.target.tgttest.selenium.data;

import java.math.BigDecimal;


/**
 * Data enum for testing against the shopping cart functionality.
 */
public enum ShoppingCartProductType {
    MUSICAL_EQUIPMENT_PRODUCT("p/musical-equipment-product/CP7017", "Musical Equipment product", "CP7017",
            null, "10.00", null, "missing-product-96x96.png", "Musical Equipment product"),
    IPODS_PRODUCT("p/ipods-mp3-players-accessories-product/CP7012",
            "iPods, MP3 Players + Accessories product", "CP7012", null,
            "10.00", null, "missing-product-96x96.png", "iPods, MP3 Players + Accessories product"),
    BATTERIES("p/batteries-product/CP7016", "Batteries product", "CP7016", null, "10.00", null,
            "missing-product-96x96.png", "Batteries product"),
    BABY_GIRLS_BLUE_SMALL("p/baby-girls-product/P1001_blue_S", "Baby Girls Product - Blue", "P1001_BLUE_S", "S",
            "10.00", "BLUE", "", "Baby Girls product");
    private final String productUrl;
    private final String name;
    private final String itemCode;
    private final String size;
    private final BigDecimal price;
    private final String colour;
    private final String imageSource;
    private final String imageAltText;

    private ShoppingCartProductType(final String productUrl, final String name, final String itemCode,
            final String size,
            final String price,
            final String colour,
            final String imageSource, final String imageAltText)
    {
        this.productUrl = productUrl;
        this.name = name;
        this.itemCode = itemCode;
        this.size = size;
        this.price = new BigDecimal(price);
        this.colour = colour;
        this.imageSource = imageSource;
        this.imageAltText = imageAltText;
    }


    /**
     * @return the productUrl
     */
    public String getProductUrl() {
        return productUrl;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @return the imageSource
     */
    public String getImageSource() {
        return imageSource;
    }

    /**
     * @return the imageAltText
     */
    public String getImageAltText() {
        return imageAltText;
    }
}
