/**
 * 
 */
package au.com.target.tgttest.selenium.cscockpit.pageobjects;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;
import org.seleniumhq.selenium.fluent.FluentWebElements;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * Represents a cscockpit order page
 * 
 */
public class CSOrderPage extends BasePage {

    public enum OrderManagementFunction {

        CancelOrder("Cancel Order"),
        PartialCancelOrder("Partial Cancel Order"),
        RefundOrder("Refund Order");

        private String label;

        private OrderManagementFunction(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        /**
         * Get the enum for the given label
         * 
         * @param a_label
         * @return function enum
         */
        public static OrderManagementFunction valueForLabel(final String a_label) {
            for (final OrderManagementFunction function : OrderManagementFunction.values()) {
                if (function.getLabel().equalsIgnoreCase(a_label)) {
                    return function;
                }
            }
            return null;
        }
    }

    // Order Management group
    private final By orderManagementLabelSpan = By.cssSelector("div.csOrderActionsWidget span.z-label");
    private final By orderManagementActionsDiv = By.cssSelector("div.orderManagementActionsWidget");


    /**
     * Constructor
     * 
     * @param driver
     */
    public CSOrderPage(final BrowserDriver driver) {
        super(driver);
    }


    /**
     * Verify this is the current page - wait for a while for page to render
     * 
     * @return true if it is
     */
    public boolean isCurrentPage() {

        // Check for Menu span containing Order Management label
        final FluentWebElement elt = fwd().within(secs(20)).span(orderManagementLabelSpan);
        if (getText(elt).contains("Order Management")) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * Whether the given function is available
     * 
     * @param function
     * @return true if available
     */
    public boolean isOrderManagementFunctionAvailable(final OrderManagementFunction function) {

        final FluentWebElement elt = getOrderManagementButtonForLabel(function.getLabel());

        // Disabled buttons have attribute disabled="disabled"
        if (StringUtils.isEmpty(elt.getAttribute("disabled").toString())) {
            return true;
        }

        return false;
    }


    /**
     * Cancel this order
     */
    public void cancelOrder() {

        getOrderManagementButtonLabel("Cancel Order").click();
    }


    private FluentWebElement getOrderManagementButtonLabel(final String name) {

        // each button has a table containing a td.z-button-cm with the button label
        final FluentWebElements tdButtons = fwd().within(secs(5)).div(orderManagementActionsDiv)
                .tds(By.className("z-button-cm"));

        for (final FluentWebElement elt : tdButtons) {

            if (getText(elt).equalsIgnoreCase(name)) {
                return elt;
            }
        }

        throw new RuntimeException("Could not find order management button label: " + name);
    }

    private FluentWebElement getOrderManagementButtonForLabel(final String name) {

        // Each order management button has a corresponding span element.
        // Each span has a descendant td for the label and an associated descendant button.
        final FluentWebElements spanButtons = fwd().within(secs(5)).div(orderManagementActionsDiv)
                .spans(By.className("z-button"));

        for (final FluentWebElement spanElt : spanButtons) {

            final FluentWebElement buttonElt = spanElt.button();

            final FluentWebElement tdLabelElt = spanElt.td(By.className("z-button-cm"));
            if (getText(tdLabelElt).equalsIgnoreCase(name)) {
                return buttonElt;
            }
        }

        throw new RuntimeException("Could not find order management button with label: " + name);
    }

}
