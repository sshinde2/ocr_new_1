/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * A util class which helps to deal with UI components, eg dropdowns
 * 
 * @author maesi
 * 
 */
public final class SelectUtil extends BasePage {

    private static final Logger LOG = LogManager.getLogger(SelectUtil.class);

    public SelectUtil(final BrowserDriver driver) {
        super(driver);
    }

    /**
     * selects an option for a native selenium select.
     * 
     * @param nativeSelectElement
     *            the native Select as a WebElement.
     * @param optionToSelect
     *            the display option to select as a String.
     * 
     * @return boolean true or false, depending on whether the option could be selected or not.
     */
    public boolean selectOptionForNativeSelect(final FluentWebElement nativeSelectElement,
            final String optionToSelect) {

        nativeSelectElement.isDisplayed().shouldBe(Boolean.TRUE);

        try {
            getSelect(nativeSelectElement.getWebElement()).selectByValue(optionToSelect);
            return true;
        }
        catch (final NoSuchElementException e) {
            LOG.error("error while trying to select an option in select '" + nativeSelectElement.getTagName()
                    + "': " + e.getMessage(), e);
        }

        return false;
    }


    /**
     * 
     * @param idSelectorForSelectDiv
     *            an id name as a String from the div-tagged select DOM-element.
     * @param displayedValueToSelect
     *            String the displayable option value, which should be set into the select.
     * 
     * @return boolean returns true or false whether the value could be selected or not.
     */
    public boolean selectOptionByValue(final String idSelectorForSelectDiv, final String displayedValueToSelect) {

        final FluentWebElement selectDiv = fwd().div(By.id(idSelectorForSelectDiv));

        final FluentWebElement elementSelect = selectDiv.link(By.cssSelector("a.chosen-single"));


        // the select first needs to be activated as the options are lazy-loaded.
        LOG.info("Opening select box \"" + idSelectorForSelectDiv + "\" to ensure it is populated.");
        elementSelect.click();


        return selectOptionByValue(selectDiv, displayedValueToSelect, false);
    }

    /**
     * a help method to select a certain displayable value/option from a select.
     * 
     * @param selectDiv
     *            WebElement which represents a select.
     * @param displayedValueToSelect
     *            String the displayable option value, which should be set into the select.
     * @param doActivateSelectBefore
     *            true/false whether the select/combobox needs to be activated before the option will be selected.
     * @return boolean returns true or false whether the value could be selected or not.
     */
    public boolean selectOptionByValue(final FluentWebElement selectDiv,
            final String displayedValueToSelect,
            final boolean doActivateSelectBefore) {

        final FluentWebElement link = selectDiv.link(By.className("chosen-single"));
        final FluentWebElement listDiv = selectDiv.div(By.className("chosen-drop"));


        if (doActivateSelectBefore) {
            LOG.info("Opening dropdown");
            link.click();
        }

        // then click the list item for the display value which needs to be selected...
        final FluentWebElement selectedOption = findSelectOptionByDisplayText(listDiv, displayedValueToSelect);

        if (selectedOption != null) {
            LOG.info("Selecting option: " + displayedValueToSelect);
            selectedOption.click();
            return true;
        }
        else {
            LOG.warn("Unable to select option \"" + displayedValueToSelect + "\" because it could not be found");
        }
        return false;
    }

    /**
     * method to find a certain option by value. used for "external result link list values" of a select/dropdown
     * component.
     * 
     * @param optionsDiv
     *            div containing options
     * @param displayText
     *            String the option display value.
     * @return WebElement returns a WebElement or null in case the text could not be found in the option values.
     */
    private FluentWebElement findSelectOptionByDisplayText(final FluentWebElement optionsDiv,
            final String displayText) {
        for (final FluentWebElement option : optionsDiv.lis(By.className("active-result"))) {
            if (displayText.equals(option.getText().toString())) {
                return option;
            }
        }
        return null;
    }

    /**
     * returns a Selenium Select component by WebElement.
     * 
     * Attention: This method is not very helpful in case of Target, as there is used quite a bit of javascript to
     * operate a select. In case a standard select component is used wrapped within a single dom-element, this is the
     * way to use!
     * 
     * @param element
     *            a WebElement which should represent a Select
     * @return a Selenium Select.
     */
    private Select getSelect(final WebElement element) {
        try {
            return new Select(element);
        }
        catch (final Exception e) {
            final String error = "Not able to instantiate a Select component for WebElement.";
            LOG.error(error, e);
            return null;
        }
    }


    /**
     * help method to focus a certain webelement and set it to displayed
     * 
     * @param element
     *            a WebElement the cursor should scroll to.
     * @param driver
     *            the WebDriver instance.
     */
    public static void moveToWebElement(final WebElement element, final WebDriver driver) {
        LOG.info("Moving to element: " + element);
        try {
            if (element.getTagName().equals("input")) {
                element.sendKeys("");
            }
            else {
                new Actions(driver).moveToElement(element).build().perform();
            }
        }
        catch (final Exception e) {
            final String error = "Not able to focus component for web element '" + element.getText() + "'.";
            LOG.error(error, e);
        }
    }




}