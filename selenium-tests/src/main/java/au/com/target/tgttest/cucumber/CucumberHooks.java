/**
 * 
 */
package au.com.target.tgttest.cucumber;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.target.tgttest.hybris.admin.ImpexImporter;
import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.base.config.BrowserOptions;
import au.com.target.tgttest.selenium.base.config.TestConfiguration;
import au.com.target.tgttest.webservice.TestWebServiceFacade;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


/**
 * Cucumber hooks
 * 
 */
public class CucumberHooks {

    private static final String TEST_USERS_IMPEX = "test-users.impex";

    private static final Logger LOG = LogManager.getLogger(CucumberHooks.class);

    // Whether the scenario is a web one (launches browser driver)
    private boolean web = true;

    /**
     * Handle nonweb tag
     */
    @Before(value = "@nonweb", order = 10)
    public void beforeScenarioSetWebFalse() {
        web = false;
    }


    /**
     * Handle web tag (the default)
     */
    @Before(value = "@web", order = 10)
    public void beforeScenarioSetWebTrue() {
        web = true;
    }

    /**
     * Handle setmocks tag
     */
    @Before(value = "@setmocks", order = 20)
    public void beforeScenarioSetMocks() {
        TestWebServiceFacade.setPosProductClientMock();
    }

    @Before(order = 100)
    public void beforeScenario() {

        if (web) {

            // This is the cucumber way to do BeforeSuite/AfterSuite
            // see http://zsoltfabok.com/blog/2012/09/cucumber-jvm-hooks/
            if (!SeleniumSession.isInitialised()) {

                // For now let the default browser options apply
                final BrowserOptions options = new BrowserOptions(null);
                SeleniumSession.initialise(options, "cucumber");

                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        SeleniumSession.getInstance().shutdownDriverSession();
                    }
                });


                // TODO - refactor to method running setup before suite
                ImpexImporter.getInstance().importImpexFromImpexFolder(TEST_USERS_IMPEX);

            }


            // Every scenario
            SeleniumSession.getInstance().clearSessionData();

        }

    }


    @After
    public void afterScenarioScreenshotOnError(final Scenario scenario) {

        if (web && scenario.isFailed()
                && TestConfiguration.getInstance().getSettings().getTakeScreenshotOnErrors()) {

            SeleniumSession.getInstance().getBrowserDriver().takeScreenshot(scenario.getName());
        }
    }
}
