/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.kiosk;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * Pos price check page
 * 
 */
public class PosPriceCheckPage extends BasePage {

    private final By itemCode = By.cssSelector("div.pricechk-code");
    private final By price = By.cssSelector("span.price");
    private final By wasPrice = By.cssSelector("span.was-price");
    private final By title = By.cssSelector("div.pricechk-basic h1.title");
    private final By heroImage = By.cssSelector("img#hero");
    private final By errorMessage = By.cssSelector("div.pricechk-missing p");


    /**
     * @param store
     * @param barcode
     * @return the get url
     */
    public static String getUrl(final String store, final String barcode) {

        return "k/pricecheck?storeNumber=" + store + "&barcode=" + barcode;
    }

    /**
     * @param driver
     */
    public PosPriceCheckPage(final BrowserDriver driver) {
        super(driver);
    }

    /**
     * Return item code
     * 
     * @return code
     */
    public String getItemCode() {
        return getText(fwd().div(itemCode));
    }


    /**
     * Get item price
     * 
     * @return price
     */
    public String getPrice() {
        return getText(fwd().span(price));
    }

    /**
     * Get item was price
     * 
     * @return was price
     */
    public String getWasPrice() {
        return getText(fwd().span(wasPrice));
    }

    /**
     * Get item price
     * 
     * @return price
     */
    public String getTitle() {
        return getText(fwd().h1(title));
    }

    /**
     * Get hero image alt text
     * 
     * @return alt
     */
    public String getImageAltText() {

        return fwd().img(heroImage).getAttribute("alt").toString();
    }

    /**
     * Return the specific error message
     * 
     * @return message
     */
    public String getErrorMessage() {

        return getText(fwd().p(errorMessage));
    }

}
