/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * @author cwijesu1
 * 
 */
public class SignUpPage extends TemplatePage {
    private final By firstnameTag = By.id("enews.firstName");
    private final By emailTag = By.id("enews.email");
    private static final String ID_SELECTOR_SIGNUPPAGE_TITLE_COMBOBOX = "enews_title_chosen";
    private final By submitButton = By.cssSelector("form#ENewsSubscriptionForm button");
    private final By loginErrorDiv = By.id("email.errors");



    public SignUpPage(final BrowserDriver driver) {
        super(driver);
    }

    // YTODO Auto-generated method stub
    @Override
    public String getPageRelativeURL() {

        return "enews";
    }

    @Override
    public boolean isCurrentPage() {

        if (getMainH1Text().contains("Sign up for Target eNews")) {
            return true;
        }
        return false;

    }

    public void populateSignupForm(final String preTitle, final String firstName,
            final String email) {

        if (StringUtils.isNotEmpty(preTitle)
                && !getComponentUtil().selectOptionByValue(ID_SELECTOR_SIGNUPPAGE_TITLE_COMBOBOX, preTitle))
        {
            throw new Error("The Title could not be selected with value '" + preTitle + "'.");
        }
        if (StringUtils.isNotEmpty(firstName))
        {
            fwd().input(firstnameTag).clearField().sendKeys(firstName);
        }
        if (StringUtils.isNotEmpty(email))
        {
            fwd().input(emailTag).clearField().sendKeys(email);
        }


    }

    public void SignupForEnew()
    {
        fwd().button(submitButton).click();
    }

    public String getSignupErrorText() {
        final FluentWebElement errorDiv = fwd().span(loginErrorDiv).within(secs(3));
        final String errorText = getText(errorDiv);
        return errorText;

    }

}
