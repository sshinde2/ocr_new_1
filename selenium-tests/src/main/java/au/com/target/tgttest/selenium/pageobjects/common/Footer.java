/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.data.FooterContentType;


/**
 * global footer
 * 
 */
public class Footer extends BasePage {


    private final By productNav = By.cssSelector("div.footer div.product-nav");
    private final By contactInfo = By.cssSelector("div.footer div.contact-info");
    private final By legalInfo = By.cssSelector("div.footer-legal");

    /**
     * @param driver
     */
    public Footer(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return isDisplayed(fwd().div(contactInfo));
    }

    public String getContactInfoText()
    {
        return getText(fwd().div(contactInfo));
    }

    public String getLegalInfoText()
    {
        return getText(fwd().div(legalInfo));
    }


    /**
     * Return true if the given department link exists in the product nav
     */
    public boolean existsDepartmentLink(final String deptName)
    {
        return isDisplayed(fwd().div(productNav).link(By.linkText(deptName)));
    }


    public boolean existsContentLink(final FooterContentType footerContentType)
    {
        return isDisplayed(fwd().link(getLinkFor(footerContentType)));
    }

    private By getLinkFor(final FooterContentType footerContentType) {
        final StringBuilder sb = new StringBuilder(".footer");
        sb.append(" a[title='").append(footerContentType.getTitleText().replaceAll("\\'", "\\\\'")).append("']");
        return By.cssSelector(sb.toString());
    }


}
