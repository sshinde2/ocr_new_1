/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.csv;

import au.com.target.tgttest.selenium.datastore.bean.Product;



public class ProductCsvDataReader extends AbstractCsvDataReader<Product> {

    @Override
    protected Product createInstanceFromCsv(final String[] csvData) {
        final Product result = new Product();
        result.setCode(getValueForHeader("CV_CODE", csvData));
        result.setName(getValueForHeader("CV_NAME", csvData));
        result.setBaseName(getValueForHeader("BASE_NAME", csvData));
        result.setDescription(getValueForHeader("BASE_DESCRIPTION", csvData));
        result.setBrand(getValueForHeader("BRAND_NAME", csvData));
        result.setColour(getValueForHeader("COLOUR_NAME", csvData));
        result.setSwatch(getValueForHeader("SWATCH_NAME", csvData));
        result.setColourCode(getValueForHeader("COLOUR_CODE", csvData));
        result.setSwatchCode(getValueForHeader("SWATCH_CODE", csvData));
        result.setSize(getNullableValueForHeader("SIZE", csvData));
        result.setPrice(getPriceValueForHeader("PRICE", csvData));
        result.setWasPrice(getPriceValueForHeader("WASPRICE", csvData));

        return result;
    }
}
