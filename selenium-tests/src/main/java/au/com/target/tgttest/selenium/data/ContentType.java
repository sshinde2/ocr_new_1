/**
 * 
 */
package au.com.target.tgttest.selenium.data;





/**
 * Represents a department type.
 */
public enum ContentType {
    /**  */
    COMPANY(0, "Company", "/company", "Company"),

    /**  */
    ABOUT_US(1, "About Us", "/company/about-us", "About Us"),
    /**  */
    CAREERS(1, "Careers", "/company/careers", "Careers"),

    /**  */
    OUR_CAREER_PATH(2, "Our Career Paths", "/company/careers/our-career-paths", "Our Career Paths"),
    /**  */
    CAREERS_MORE_INFO(2, "Find Out More", "/company/careers/more-info", "Find Out More"),
    /**  */
    OUR_TEAM(2, "Our Team", "/company/careers/our-team", "Our Team"),
    /**  */
    CAREERS_ABOUT_US(2, "About Us", "/company/about-us", "About Us"),
    /**  */
    OUR_BENEFITS(2, "Our Benefits", "/company/careers/our-benefits", "Our Benefits"),

    /**  */
    HELP(0, "Help", "/help", "Help"),

    /**  */
    PAYMENT_DELIVERY(1, "Payment + Delivery", "/help/payment-delivery", "Payment + Delivery"),
    /**  */
    ORDER_TRACKING(1, "Order Tracking", "/help/order-tracking", "Order Tracking"),
    /**  */
    REFUNDS_AND_RETURNS(1, "Refunds + Returns", "/help/refunds-returns", "Refunds + Returns"),
    /**  */
    SIZE_CHARTS(1, "Size Charts", "/help/size-charts", "Size Charts"),
    /**  */
    CONTACT_US(1, "Contact Us", "/help/contact-us", "Contact Us"),
    /**  */
    SHOP_ONLINE_FAQS(1, "Shop Online FAQs", "/help/faqs", "Shop Online FAQs"),

    /**  */
    RETURNS_FAQS(2, "Returns FAQs", "/help/faqs/returns", "Returns FAQs"),
    /**  */
    CUSTOMER_SERVICE_FAQS(2, "Customer Service FAQs", "/help/faqs/customer-service", "Customer Service FAQs"),
    /**  */
    DELIVERY_FAQS(2, "Delivery FAQs", "/help/faqs/delivery", "Delivery FAQs"),
    /**  */
    STANDARD_LAY_BY_FAQS(2, "Standard Lay-By FAQs", "/help/faqs/standard-lay-by", "Standard Lay-By FAQs"),
    /**  */
    TOY_SALE_LAY_BY_FAQS(2, "Toysale Lay-By FAQs", "/help/faqs/toy-sale-lay-by", "Toysale Lay-By FAQs"),

    ARTICLES(1, "Shoe Fitting Tips", "/articles", "Articles");

    private int level;
    private String name;
    private String relativeURL;
    private String breadcrumbText;

    private ContentType(final int level, final String name, final String relativeURL, final String breadcrumbText)
    {
        this.level = level;
        this.name = name;
        this.relativeURL = relativeURL;
        this.breadcrumbText = breadcrumbText;
    }

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the relativeURL
     */
    public String getRelativeURL()
    {
        return relativeURL;
    }

    /**
     * @return the breadcrumbText
     */
    public String getBreadcrumbText()
    {
        return breadcrumbText;
    }
}
