package au.com.target.tgttest.selenium.data;

/**
 * 
 * enum datatype for facet sections.
 * 
 * @author maesi
 * 
 */
public enum FacetSectionType {

    /*FACET_SECTION_SHOP_BY_DELIVERY(0, "Shop by Delivery"),*/
    FACET_SECTION_SHOP_BY_CATEGORY(1, "Shop by Category"),
    FACET_SECTION_SHOP_BY_SIZE(2, "Shop by Size"),
    FACET_SECTION_SHOP_BY_COLOUR(3, "Shop by Colour"),
    FACET_SECTION_SHOP_BY_PRICE(4, "Shop by Price"),
    FACET_SECTION_SHOP_BY_BRAND(5, "Shop by Brand");



    private int menuPosition;
    private String sectionTitle;


    private FacetSectionType(final int pos, final String name) {
        this.menuPosition = pos;
        this.sectionTitle = name;
    }

    public int getMenuPosition() {
        return this.menuPosition;
    }

    public String getFacetSectionTitle() {
        return this.sectionTitle;
    }

}
