/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.data.SearchFilterType;
import au.com.target.tgttest.selenium.pageobjects.common.ProductInList;


/**
 * for pages that have the standard product list
 * 
 */
public abstract class BaseProductListPage extends TemplatePage {

    protected final By products = By.cssSelector("div.product-listing");
    private final By filter = By.cssSelector("div.main div.chosen-container");

    private final By viewAsList = By.cssSelector("div.main div.filter ul.view-as li.view-as-list");
    private final By viewAsGrid = By.cssSelector("div.main div.filter ul.view-as li.view-as-grid");
    private final By itemsPerPageOptionHolder = By.cssSelector("div.main ul.pager.pager-options");

    /* facets and left navigation... */
    private final By facetCategoryTitle = By.cssSelector("div.facets h2");
    private final By facetCategorySubTitle = By.cssSelector("div.facets h3");
    private final By pageNumberLabel = By.cssSelector("div.main ul.pager.pager-pages li.label");


    public BaseProductListPage(final BrowserDriver driver) {
        super(driver);
    }


    public final List<ProductInList> getProductList() {

        final List<FluentWebElement> productElements = fwd().div(products).lis(By.className("product"));

        final List<ProductInList> listProducts = new ArrayList<ProductInList>();
        for (final FluentWebElement elt : productElements)
        {
            final ProductInList product = new ProductInList(getBrowserDriver(), elt);
            listProducts.add(product);
        }

        return listProducts;
    }

    /**
     * Applies a filter on the resulting page.
     * 
     * @param type
     *            the sort by option to apply.
     */
    public final void filterResults(final SearchFilterType type) {

        fwd().div(filter).link().click();

        for (final FluentWebElement option : fwd().div(filter).lis()) {
            if (option.getText().toString().contains(type.getText())) {
                option.click();
                return;
            }
        }

        throw new RuntimeException("Could not activate filter: " + type);
    }

    public String getFacetCategoryTitle() {
        return getText(fwd().h2(facetCategoryTitle));
    }

    public String getFacetCategorySubTitle() {
        return getText(fwd().h3(facetCategorySubTitle));
    }

    public final ProductPage clickProduct(final int i) {
        return getProductList().get(i).clickProductImageLink();
    }


}
