/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.myaccount.MyAccountPage;


/**
 * Login page
 * 
 */
public class LoginPage extends TemplatePage {

    private final By heading = By.cssSelector("div.main h1");
    private final By loginName = By.cssSelector("input#j_username");
    private final By loginPassword = By.cssSelector("input#j_password");
    private final By loginErrorDiv = By.cssSelector("div.feedback-msg.error");
    private final By loginButton = By.cssSelector("form#loginForm button");
    private final By loggedinHeading = By.cssSelector("menu-item account-address h4");


    @Override
    public String getPageRelativeURL() {
        return "login";
    }


    @Override
    public boolean isCurrentPage() {

        if (getText(fwd().button(loginButton)).contains("I Agree, Login Now")) {
            return true;
        }
        else {
            return false;
        }
    }

    public LoginPage(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return getText(fwd().h1(heading)).equals("My Account");

    }

    public MyAccountPage logInExistingUserSuccessfully(final String email, final String password) {
        logInExistingUser(email, password);
        return new MyAccountPage(getBrowserDriver());
    }

    public LoginPage logInExistingUserWithError(final String email, final String password) {
        logInExistingUser(email, password);
        return this;
    }

    private void logInExistingUser(final String email, final String password) {
        fwd().input(loginName).clearField().sendKeys(email);
        fwd().input(loginPassword).clearField().sendKeys(password);
        fwd().button(loginButton).click();
    }

    public String getLoginErrorText() {

        try {
            final FluentWebElement errorDiv = fwd().div(loginErrorDiv).within(secs(3));
            final String errorText = getText(errorDiv.p());
            LOG.info("Login page login error: " + errorText);
            return errorText;
        }
        catch (final Exception e) {
            LOG.info("No error div");
        }

        return "";
    }



}
